/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot;

import com.skydream.coreot.pojos.*;

/**
 *
 * @author mcj
 */
public class CoreOtFactory {

    public ObjetoCoreOT getObjetoCoreOt(String nombreObjeto) throws Exception {
        ObjetoCoreOT objCoreOT = null;

        switch (nombreObjeto) {
            case "Accion":
                objCoreOT = new Acciones(new TipoAccion());
                break;
            case "Alarma":
                objCoreOT = new Alarmas(new TipoAlarma());
                break;
            case "Agencia":
                objCoreOT = new Agencias(new Regiones());
                break;
            case "Area":
                objCoreOT = new Areas();
                break;
            case "Central":
                objCoreOT = new Centrales(new Agencias());
                break;
            case "Contrato":
                objCoreOT = new Contratos(new Workflow(), new TipoPlanta());
                break;
            case "Cubicador":
                objCoreOT = new Cubicador(new Contratos(), new Ot(), new Regiones(), new Usuarios());
                break;
            case "CubicadorOrdinario":
                objCoreOT = new CubicadorOrdinario(new Cubicador(), new TipoMoneda(), new TipoUnidadMedida());
                break;                
            case "Decision":
                objCoreOT = new Decisiones();
                break;
            case "DetalleParametro":
                objCoreOT = new DetalleParametros(new Parametros());
                break;
            case "Evento":
                objCoreOT = new Eventos(new MensajeEvento(), new TipoEvento());
                break;
            case "Especialidad":
                objCoreOT = new Especialidades();
                break;
            case "Familia":
                objCoreOT = new Familias();
                break;
            case "EstadoWorkflow":
                objCoreOT = new EstadoWorkflow();
                break;
            case "LibroObra":
                objCoreOT = new LibroObras(new Ot(), new Usuarios());
                break;
            case "Adjunto":
                objCoreOT = new Adjuntos(new Ot(), new Repositorios());
                break;
            case "Login":
                objCoreOT = new Login();
                break;
            case "Material":
                objCoreOT = new Materiales(new Familias(), new TipoMoneda(), new TipoUnidadMedida());
                break;
            case "MensajeEvento":
                objCoreOT = new MensajeEvento();
                break;
            case "Opcion":
                objCoreOT = new Opciones();
                break;
            case "Ot":
                objCoreOT = new Ot(0,new Centrales(), new Contratos(), new Proveedores(), new TipoEstadoOt(), new TipoOt(), new TipoSolicitud(),
                                    new Usuarios(), new Usuarios(), new EstadoWorkflow(), null, null, null, null, null, null, null, null,null, 0, false);
                break;
            case "Parametro":
                objCoreOT = new Parametros(0, new TipoParametro());
                break;
            case "Perfil":
                objCoreOT = new Perfiles();
                break;
            case "Privilegio":
                objCoreOT = new Privilegios(new Eventos());
                break;
            case "Sitios":
                objCoreOT = new Sitios(new Contratos(), new Regiones());
                break;
            case "Proveedor":
                objCoreOT = new Proveedores(new TipoProveedor());
                break;
            case "Region":
                objCoreOT = new Regiones();
                break;
            case "Repositorio":
                objCoreOT = new Repositorios();
                break;
            case "Rol":
                objCoreOT = new Roles(new Perfiles());
                break;
            case "Servicio":
                objCoreOT = new Servicios(new TipoServicio(), new TipoUnidadMedida());
                break;
            case "TipoAccion":
                objCoreOT = new TipoAccion();
                break;
            case "TipoAlarma":
                objCoreOT = new TipoAlarma();
                break;
            case "TipoEvento":
                objCoreOT = new TipoEvento();
                break;
            case "TipoMoneda":
                objCoreOT = new TipoMoneda();
                break;
            case "TipoParametro":
                objCoreOT = new TipoParametro();
                break;
            case "TipoProveedor":
                objCoreOT = new TipoProveedor();
                break;
            case "TipoServicio":
                objCoreOT = new TipoServicio();
                break;
            case "TipoUnidadMedida":
                objCoreOT = new TipoUnidadMedida();
                break;
            case "TipoSolicitud":
                objCoreOT = new TipoSolicitud();
                break;
            case "TipoEstadoOt":
                objCoreOT = new TipoEstadoOt();
                break;
            case "TipoOt":
                objCoreOT = new TipoOt();
                break;
            case "TipoTramite":
                objCoreOT = new TipoTramite();
                break;
            case "Usuario":
                objCoreOT = new Usuarios(new Areas(), new Repositorios(), new Proveedores());
                break;
            case "Workflow":
                objCoreOT = new Workflow();
                break;
            case "WorkflowEventosEjecucion":
                objCoreOT = new WorkflowEventosEjecucion(new WorkflowEventosEjecucionId(), new Ot());
                break;
            case "PlanDeProyecto":
                objCoreOT = new PlanDeProyecto();
                break;
            case "CubicadorDetalle":
                objCoreOT = new CubicadorDetalle();
                break;
            case "EstadoTramite":
                objCoreOT = new EstadoTramite();
                break;
            case "Actividades":
                objCoreOT = new Actividades();
                break;
            case "Especialidades":
                objCoreOT = new Especialidades();
                break;
            case "ServiciosUnidad":
                objCoreOT = new ServiciosUnidad();
                break;
//            case "ServiciosUnidadMateriales":
//                objCoreOT = new ServiciosUnidadMateriales();
//                break;
            default:
                throw new Exception("Objeto no encontrado en Factory");
        }

        return objCoreOT;
    }

}
