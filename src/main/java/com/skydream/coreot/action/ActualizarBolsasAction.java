package com.skydream.coreot.action;

import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import org.apache.struts2.ServletActionContext;
import org.jboss.logging.Logger;

/**
 * Created by mcj on 05-06-16.
 */
public class ActualizarBolsasAction {

    private String parametros;
    private int usuarioId;
    private int rolId;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"),usuarioId,true);
        String cadenaRetorno = "";

        try {

            GenericDAO.getINSTANCE().actualizarBolsas(parametros, getUsuarioId(), getRolId());
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al actualizar Bolsa PMO. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getRolId() {
        return rolId;
    }

    public void setRolId(int rolId) {
        this.rolId = rolId;
    }

    private static final Logger log = Logger.getLogger(ActualizarBolsasAction.class);

}
