package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.AdjuntoDAO;
import com.skydream.coreot.dao.LibroObraDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.OtDAO;
import com.skydream.coreot.pojos.ArchivoAux;
import com.skydream.coreot.pojos.CartaAdjudicacion;
import com.skydream.coreot.pojos.CartaAdjudicacionAux;
import com.skydream.coreot.pojos.Ot;
import com.skydream.eventmanager.GatewayAcciones;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.*;
import java.util.Map;

/**
 * Created by mcj on 20-11-15.
 */
public class AdjuntarCartaAdjudicacionOTAction extends ActionSupport {

    private int otId;
    private int usuarioId;
    private File archivo;
    private String archivoFileName;
    private String archivoContentType;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"),usuarioId, true);
        String cadenaRetorno = "";
        ObjectMapper mapper = new ObjectMapper();
        try {

            ByteArrayOutputStream baos = leerArchivo(archivo);
            ArchivoAux archivoAux = new ArchivoAux();
            archivoAux.setBaos(baos);
            archivoAux.setNombre(archivoFileName);
            archivoAux.setExtension(retornarExtension(archivoFileName));

            Ot ot = OtDAO.getINSTANCE().obtenerDetalleOt(otId);
            String detalle = ot.getJsonDetalleOt();

            CartaAdjudicacionAux cartaAdjudicacionAux = mapper.readValue(detalle, CartaAdjudicacionAux.class);
            CartaAdjudicacion cartaAdjudicacion = new CartaAdjudicacion();
            cartaAdjudicacion.setMateria(cartaAdjudicacionAux.getMateria());
            cartaAdjudicacion.setNumero(cartaAdjudicacionAux.getNumero());
            cartaAdjudicacion.setNumeroCarta(String.valueOf(cartaAdjudicacionAux.getNumero()));
            cartaAdjudicacion.setNumeroPedido(cartaAdjudicacionAux.getNumeroPedido());

            String[] roles = new String[5];
            roles[0] = "2";
            roles[1] = "3";
            roles[2] = "6";
            roles[3] = "8";
            roles[4] = "9";

            LibroObraDAO.adjuntarCartaAdjudicacionOt(archivoAux, usuarioId, cartaAdjudicacionAux.getNumero(), otId, cartaAdjudicacion, roles);

            cadenaRetorno = "success";
        } catch (Exception ex) {
            System.out.println(ex);
            cadenaRetorno = "error";
            throw new Exception("No se pudo registrar el objeto");
        }
        return cadenaRetorno;
    }

    private ByteArrayOutputStream leerArchivo(File archivo) throws IOException, FileNotFoundException {
        ByteArrayOutputStream baos;
        InputStream is = new FileInputStream(archivo);
        try (BufferedInputStream bis = new BufferedInputStream(is)) {
            baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int readCount;
            while ((readCount = bis.read(buffer)) > 0) {
                baos.write(buffer, 0, readCount);
            }
        }
        return baos;
    }

    private static String retornarExtension(String nombreArhivo) {
        String fileName = nombreArhivo;
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    public File getArchivo() {
        return archivo;
    }

    public void setArchivo(File archivo) {
        this.archivo = archivo;
    }

    public String getArchivoFileName() {
        return archivoFileName;
    }

    public void setArchivoFileName(String archivoFileName) {
        this.archivoFileName = archivoFileName;
    }

    public String getArchivoContentType() {
        return archivoContentType;
    }

    public void setArchivoContentType(String archivoContentType) {
        this.archivoContentType = archivoContentType;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }


    public int getOtId() {
        return otId;
    }

    public void setOtId(int otId) {
        this.otId = otId;
    }

}
