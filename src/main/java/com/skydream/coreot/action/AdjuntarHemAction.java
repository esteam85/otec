package com.skydream.coreot.action;

import com.skydream.coreot.dao.*;
import com.skydream.coreot.pojos.*;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.logging.Logger;

import java.io.*;

/**
 * Created by mcj on 29-06-16.
 */
public class AdjuntarHemAction {

    private String hemId;
    private int usuarioId;
    private File archivo;
    private String archivoFileName;
    private String archivoContentType;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"),usuarioId, true);
        String cadenaRetorno = "";
        ObjectMapper mapper = new ObjectMapper();
        try {

            ByteArrayOutputStream baos = leerArchivo(archivo);
            ArchivoAux archivoAux = new ArchivoAux();
            archivoAux.setBaos(baos);
            archivoAux.setNombre(archivoFileName);
            archivoAux.setExtension(retornarExtension(archivoFileName));

            GenericDAO.getINSTANCE().adjuntarHem(archivoAux,getHemId(), getUsuarioId());

            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al adjuntar HEM.",ex);
            cadenaRetorno = "error";
            throw new Exception("No se pudo registrar el objeto");
        }
        return cadenaRetorno;
    }

    private ByteArrayOutputStream leerArchivo(File archivo) throws IOException, FileNotFoundException {
        ByteArrayOutputStream baos;
        InputStream is = new FileInputStream(archivo);
        try (BufferedInputStream bis = new BufferedInputStream(is)) {
            baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int readCount;
            while ((readCount = bis.read(buffer)) > 0) {
                baos.write(buffer, 0, readCount);
            }
        }
        return baos;
    }

    private static String retornarExtension(String nombreArhivo) {
        String fileName = nombreArhivo;
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    private static final Logger log = Logger.getLogger(AdjuntarHemAction.class);

    public File getArchivo() {
        return archivo;
    }

    public void setArchivo(File archivo) {
        this.archivo = archivo;
    }

    public String getArchivoFileName() {
        return archivoFileName;
    }

    public void setArchivoFileName(String archivoFileName) {
        this.archivoFileName = archivoFileName;
    }

    public String getArchivoContentType() {
        return archivoContentType;
    }

    public void setArchivoContentType(String archivoContentType) {
        this.archivoContentType = archivoContentType;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getHemId() {
        return hemId;
    }

    public void setHemId(String hemId) {
        this.hemId = hemId;
    }
}
