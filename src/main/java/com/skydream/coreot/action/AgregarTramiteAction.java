/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.OtDAO;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author Erbi
 */
public class AgregarTramiteAction extends ActionSupport {
    
    private int otId;
    private int tipoTramiteId;
    private String tramite;
    private boolean estadoOK;
    private int usuarioId;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(), true);
        estadoOK = false;
        String cadenaRetorno = "";
        try {
            OtDAO otDAO = OtDAO.getINSTANCE();
            Boolean respuesta = OtDAO.getINSTANCE().agregarTramite(getOtId(), getTipoTramiteId(), getTramite(), getUsuarioId());
            if (respuesta) {
                estadoOK = true;
            }
            cadenaRetorno = "success";
        } catch (Exception ex) {
            cadenaRetorno = "error";
            log.error("Error al agregar trámite. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public int getOtId() {
        return otId;
    }

    public void setOtId(int otId) {
        this.otId = otId;
    }
    
    public int getTipoTramiteId() {
        return tipoTramiteId;
    }

    public void setTipoTramiteId(int tipoTramiteId) {
        this.tipoTramiteId = tipoTramiteId;
    }

    public String getTramite() {
        return tramite;
    }

    public void setTramite(String tramite) {
        this.tramite = tramite;
    }

    public boolean isEstadoOK() {
        return estadoOK;
    }

    public void setEstadoOK(boolean estadoOK) {
        this.estadoOK = estadoOK;
    }

    private static final Logger log = Logger.getLogger(AgregarTramiteAction.class);

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
