/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.dao.BucleServices;
import com.skydream.coreot.dao.LoginLogoutDAO;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;
import java.util.Map;

/**
 *
 * @author mcj
 */
public class ApiBucleAction extends ActionSupport {
    
    private String servicio;
    private String recurso;
    private String retorno;
    private Integer regionId;
    private int usuarioId;

    public String execute() throws Exception{
        String cadenaRetorno = "";
        Map session = ActionContext.getContext().getSession();

        try {
//            LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), usuarioId,true);
            CoreOtFactory  coreOtFactory = new CoreOtFactory();

            BucleServices seriviciosBucle = new BucleServices();

            List<Object> listaRetorno = seriviciosBucle.procesaPeticion(servicio,recurso,validaNull(regionId));

            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(listaRetorno));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al listar Objeto. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private int validaNull(Integer regionId) {
        if(regionId != null){
            return regionId;
        }else {
            return 0;
        }
    }

    private static final Logger log = Logger.getLogger(ApiBucleAction.class);

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getRecurso() {
        return recurso;
    }

    public void setRecurso(String recurso) {
        this.recurso = recurso;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }
}