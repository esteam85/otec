/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.OtDAO;
import org.apache.struts2.ServletActionContext;
import org.jboss.logging.Logger;

/**
 *
 * @author Erbi
 */
public class ApruebaDetencionOtAction extends ActionSupport {

    private int usuarioId;
    private int tramiteId;
    
    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"),usuarioId,true);
        String cadenaRetorno = "success";
        try {
            Boolean respuesta = OtDAO.getINSTANCE().apruebaTramite(tramiteId, usuarioId);
        } catch (Exception ex) {
            log.error("Error al aprobar el tramite. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }
    
    private static final Logger log = Logger.getLogger(ApruebaDetencionOtAction.class);

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getTramiteId() {return tramiteId;}

    public void setTramiteId(int tramiteId) {this.tramiteId = tramiteId;}

}
