/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.Eventos;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.util.*;

/**
 *
 * @author Erbi
 */
public class AsignarResponsableWorkflowEventoEjecucionAction extends ActionSupport {

    private String parametros;
    private int usuarioId;
    private boolean estadoOK;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            Map map = mapper.readValue(parametros, new TypeReference<HashMap<String, Object>>() {});

            Iterator it = map.keySet().iterator();
            List<Eventos> listadoEventos = new ArrayList<>();
            int idOt = (Integer)map.get("idOt");
            List<Map> mapaEventos = (List<Map>) map.get("eventos");

            for(Map eve : mapaEventos){
                Eventos eventoAux = new Eventos();
                eventoAux.setId((Integer)eve.get("id"));
                eventoAux.setDuracion((Integer) eve.get("duracion"));
                listadoEventos.add(eventoAux);
                GenericDAO.getINSTANCE().asignarResponsableWorkflowEventoEjecucionAction(idOt, eventoAux);
            }


            cadenaRetorno = "success";
        } catch (Exception ex) {
            System.out.println(ex);
            cadenaRetorno = "error";
            throw new Exception("No se pudo registrar el objeto");
        }
        return cadenaRetorno;
    }



    public boolean isEstadoOK() {
        return estadoOK;
    }

    public void setEstadoOK(boolean estadoOK) {
        this.estadoOK = estadoOK;
    }

    private static final Logger log = Logger.getLogger(AsignarResponsableWorkflowEventoEjecucionAction.class);

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }


    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
