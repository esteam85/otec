/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.UsuarioDAO;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.type.TypeReference;

import java.util.*;

/**
 *
 * @author Erbi
 */
public class AsignarUsuarioPagosAction extends ActionSupport {

    private String parametros;
    private String retorno;
    private int usuarioId;


    public String execute() throws Exception {
        final String token = ServletActionContext.getRequest().getParameter("tk");
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(token,usuarioId,true);
        String cadenaRetorno = "";
        try {
            Login login = LoginLogoutDAO.getINSTANCE().retornarLoginSesionActual(token);

            ObjectMapper mapper = new ObjectMapper();
            Map mapaParams = mapper.readValue(parametros, new TypeReference<HashMap<String,Object>>() {
            });

            Map opcionItem = (Map) mapaParams.get("usuario");
            int idUsuarioAsignado = Integer.parseInt(opcionItem.get("id").toString());

            List<DetalleBolsasValidacionesPag> listadoOt = new ArrayList<>();

            String jsonActas = (String)mapaParams.get("jsonActas");

            ArrayList<LinkedHashMap> list =  (ArrayList<LinkedHashMap>) mapaParams.get("actas");
            Iterator iterator = list.iterator();

            while(iterator.hasNext()) {
                LinkedHashMap item = (LinkedHashMap)iterator.next();
                DetalleBolsasValidacionesPag detalle = new DetalleBolsasValidacionesPag();
                int idOt = Integer.parseInt(item.get("idOt").toString());
                int idActa = Integer.parseInt(item.get("idActa").toString());
                detalle.setActaId(idActa);
                detalle.setOtId(idOt);
                listadoOt.add(detalle);
            }

            boolean asigno = UsuarioDAO.getINSTANCE().asignarUsuarioPagos(usuarioId, idUsuarioAsignado, listadoOt);

            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(asigno));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            cadenaRetorno = "error";
            getLog().error("Error al obtener el detalle de la OT. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static Logger log = Logger.getLogger(AsignarUsuarioPagosAction.class);

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public static Logger getLog() {
        return log;
    }

    public static void setLog(Logger aLog) {
        log = aLog;
    }

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }
}
