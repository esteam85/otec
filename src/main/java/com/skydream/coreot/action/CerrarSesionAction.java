package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LoginLogoutDAO;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by mcj on 11-01-16.
 */
public class CerrarSesionAction extends ActionSupport{

    private int usuarioId;

    public String execute() throws Exception {

        String cadenaRetorno = "";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            String token = request.getParameter("tk");
            LoginLogoutDAO logout = LoginLogoutDAO.getINSTANCE();
            if(getUsuarioId() !=0){
                logout.validarSesionUsuario(token,usuarioId,true);
                logout.cerrarSesion(token);
                cadenaRetorno = "success";
            }else throw new Exception("Error al setear usuarioId");
        } catch (Exception ex) {
            cadenaRetorno = "error";
            getLog().error("Error al obtener el detalle de la OT. ", ex);
            throw new Exception("Error al intentar cerrar la sesion del usuario.");
        }
        return cadenaRetorno;
    }

    private static Logger log = Logger.getLogger(ObtenerDetalleOtAction.class);

    public static Logger getLog() {
        return log;
    }

    public static void setLog(Logger log) {
        CerrarSesionAction.log = log;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }


}