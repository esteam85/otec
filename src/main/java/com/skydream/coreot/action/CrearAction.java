/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import java.util.HashMap;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

/**
 *
 * @author mcj
 */
public class CrearAction extends ActionSupport {

    private String parametros;
    private String objCoreOT;
    private int usuarioId;
    
    public String execute() throws Exception{
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";
        Map session = ActionContext.getContext().getSession();

        ObjectMapper mapper = new ObjectMapper();
        try {
            Map map = mapper.readValue(parametros, new TypeReference<HashMap<String,Object>>() {
                    });
            CoreOtFactory  coreOtFactory = new CoreOtFactory();
            ObjetoCoreOT objetoCoreOT = coreOtFactory.getObjetoCoreOt(this.objCoreOT);
            if (objetoCoreOT != null) {
                objetoCoreOT.setearObjetoDesdeMap(map,0);
                objetoCoreOT.obtenerGatewayDAO().crear(objetoCoreOT, map);
            }
            cadenaRetorno = "success";
        } catch (Exception ex) {
            System.out.println(ex);
            cadenaRetorno = "error";
            throw new Exception("No se pudo registrar el objeto");
        }
        return cadenaRetorno;
    }

    /**
     * @return the objCoreOT
     */
    public String getObjCoreOT() {
        return objCoreOT;
    }

    /**
     * @param objCoreOT the objCoreOT to set
     */
    public void setObjCoreOT(String objCoreOT) {
        this.objCoreOT = objCoreOT;
    }

    /**
     * @return the parametros
     */
    public String getParametros() {
        return parametros;
    }

    /**
     * @param parametros the parametros to set
     */
    public void setParametros(String parametros) {
        this.parametros = parametros;
    }


    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
