/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.CubicadorDAO;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.jboss.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author christian
 */
public class CrearCubicacionBucleAction extends ActionSupport {

    private String parametros;
    private int idCubicacion;
    private int usuarioId;

    public String execute() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        String token = request.getParameter("tk");
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(token, getUsuarioId(),true);
        GenericDAO.getINSTANCE().registrarJournal(token, usuarioId, request.getServletPath(),
                URLDecoder.decode(request.getQueryString(), "UTF-8"), new Date());
        String cadenaRetorno = "";
        ObjectMapper mapper = new ObjectMapper();

        try {
            Map map = mapper.readValue(getParametros(), new TypeReference<HashMap<String, Object>>() {
            });
            Boolean status = CubicadorDAO.getINSTANCE().crearCubicacionBucle(map);
            if (status) {
                cadenaRetorno = "success";
            }

        } catch (Exception ex) {
            log.error("Error al crear cubicacion. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }

    private static final Logger log = Logger.getLogger(CrearCubicacionBucleAction.class);

    public int getIdCubicacion() {
        return idCubicacion;
    }

    public void setIdCubicacion(int idCubicacion) {
        this.idCubicacion = idCubicacion;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
