package com.skydream.coreot.action;

import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.pojos.MotivosRechazosGestionEconomica;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mcj on 03-06-16.
 */
public class CrearMotivoRechazoGestionEconomica {

    private int usuarioId;
    private String codigoAreaGestionEconomica;
    private String nombreRechazo;
    private String descripcionRechazo;


    public String execute() throws Exception {
//        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";
        try {
            if("PMO".equals(codigoAreaGestionEconomica) || "IMP".equals(codigoAreaGestionEconomica) || "PAG".equals(codigoAreaGestionEconomica)){
                GenericDAO dao = new GenericDAO();
                MotivosRechazosGestionEconomica motivosRechazosGestionEconomica = new MotivosRechazosGestionEconomica(this.nombreRechazo,this.descripcionRechazo,this.codigoAreaGestionEconomica);
                dao.crearMotivoRechazoGestionEconomica(motivosRechazosGestionEconomica);
            }else{
                throw new Exception("Codigo de area no corresponde.");
            }
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al registrar motivo de rechazo de gestion economica. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }



    private static final Logger log = Logger.getLogger(CrearMotivoRechazoGestionEconomica.class);

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getCodigoAreaGestionEconomica() {
        return codigoAreaGestionEconomica;
    }

    public void setCodigoAreaGestionEconomica(String codigoAreaGestionEconomica) {
        this.codigoAreaGestionEconomica = codigoAreaGestionEconomica;
    }

    public String getNombreRechazo() {
        return nombreRechazo;
    }

    public void setNombreRechazo(String nombreRechazo) {
        this.nombreRechazo = nombreRechazo;
    }

    public String getDescripcionRechazo() {
        return descripcionRechazo;
    }

    public void setDescripcionRechazo(String descripcionRechazo) {
        this.descripcionRechazo = descripcionRechazo;
    }
}
