/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.ObjetoCoreOT;

import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.skydream.coreot.pojos.Ot;
import com.skydream.coreot.pojos.TipoEstadoOt;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author mcj
 */
public class CrearOtAction extends ActionSupport {

    private String parametros;
    private String objCoreOT;
    private int id;
    private int usuarioId;

    public String execute() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(request.getParameter("tk"), getUsuarioId(),true);
        GenericDAO.getINSTANCE().registrarJournal(request.getParameter("tk"), usuarioId, request.getServletPath(),
                URLDecoder.decode(request.getQueryString(), "UTF-8"), new Date());
        String cadenaRetorno = "";

        //Map map = new HashMap();
        ObjectMapper mapper = new ObjectMapper();
        try {
            Map map = mapper.readValue(parametros, new TypeReference<HashMap<String, Object>>() {
            });
            CoreOtFactory coreOtFactory = new CoreOtFactory();
            ObjetoCoreOT objetoCoreOT = coreOtFactory.getObjetoCoreOt(this.objCoreOT);
            Ot ot = (Ot) objetoCoreOT;
            TipoEstadoOt tipoEstadoOt = new TipoEstadoOt();
            tipoEstadoOt.setId(1);
            ot.setTipoEstadoOt(tipoEstadoOt);
//            if(ot.getTipoOt().getId() == 2){
//                ot.setTipoEstadoOt(null);
//            }

            if (objetoCoreOT != null) {
                objetoCoreOT.setearObjetoDesdeMap(map, 0);
                id = objetoCoreOT.obtenerGatewayDAO().crearRetornarId(ot, map, getUsuarioId());
            }
            cadenaRetorno = "success";
        } catch (Exception ex) {
            System.out.println(ex);
            cadenaRetorno = "error";
            throw new Exception("No se pudo registrar el objeto");
        }
        return cadenaRetorno;
    }

    /**
     * @return the objCoreOT
     */
    public String getObjCoreOT() {
        return objCoreOT;
    }

    /**
     * @param objCoreOT the objCoreOT to set
     */
    public void setObjCoreOT(String objCoreOT) {
        this.objCoreOT = objCoreOT;
    }

    /**
     * @return the parametros
     */
    public String getParametros() {
        return parametros;
    }

    /**
     * @param parametros the parametros to set
     */
    public void setParametros(String parametros) {
        this.parametros = parametros;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

}
