/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.ProveedorDAO;
import com.skydream.coreot.pojos.*;
import com.skydream.coreot.util.SendMail;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.awt.geom.Area;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 *
 * @author mcj
 */
public class CrearProveedorCubicacionAction extends ActionSupport {

    private String parametros;
    private int usuarioId;

    
    public String execute() throws Exception{
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";
        
        //Map map = new HashMap();
        ObjectMapper mapper = new ObjectMapper();
        try {
            Map map = mapper.readValue(parametros, new TypeReference<HashMap<String,Object>>() {
                    });
            ProveedoresCubicacion proveedoresCubicacion = new ProveedoresCubicacion();
            proveedoresCubicacion = proveedoresCubicacion.setearObjetoDesdeMap(map);

            // primero creo el proveedor
            Proveedores proveedor = new Proveedores();
            TipoProveedor tipoProveedor = new TipoProveedor();
            tipoProveedor.setId(3);
            proveedor.setTipoProveedor(tipoProveedor);
            proveedor.setNombre(proveedoresCubicacion.getNombre());
            proveedor.setTelefono(proveedoresCubicacion.getTelefono());
            proveedor.setDireccion(proveedoresCubicacion.getDireccion());
            proveedor.setRut(proveedoresCubicacion.getRut());
            proveedor.setDv(proveedoresCubicacion.getDv());
            proveedor.setEmail(proveedoresCubicacion.getEmail());
            char estado = 'A';
            proveedor.setEstado(estado);

            int idProveedor = ProveedorDAO.getInstance().crearProveedorDesdeCubicacion(proveedor, proveedoresCubicacion);


            cadenaRetorno = "success";
        } catch (Exception ex) {
            System.out.println(ex);
            cadenaRetorno = "error";
            throw new Exception("No se pudo registrar el objeto");
        }
        return cadenaRetorno;
    }

    /**
     * @return the parametros
     */
    public String getParametros() {
        return parametros;
    }

    /**
     * @param parametros the parametros to set
     */
    public void setParametros(String parametros) {
        this.parametros = parametros;
    }


    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
