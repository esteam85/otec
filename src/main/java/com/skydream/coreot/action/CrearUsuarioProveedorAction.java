package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.UsuarioDAO;
import com.skydream.eventmanager.Facade;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import java.io.File;

/**
 * Created by mcj on 18-11-15.
 */
public class CrearUsuarioProveedorAction extends ActionSupport {

    private Integer idUsuario;
    private Integer idRolNuevoUsuario;
    private String nombreUsrNuevoUsuario;
    private String rutNuevoUsuario;
    private String nombresNuevoUsuario;
    private String apellidosNuevoUsuario;
    private String emailNuevoUsuario;
    private String celularNuevoUsuario;
    private File avatar;
    private String avatarContentType;
    private String avatarFileName;
    private int usuarioId;

    public static Logger getLog() {
        return log;
    }


    public String execute() throws Exception{
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";
        try {
            byte[] imgByteArr = Base64.decodeBase64(retornarImagenDePruebasEnBase64());
            UsuarioDAO.getINSTANCE().crearUsuarioDesdePerfilProveedor(getIdUsuario(), getIdRolNuevoUsuario(), getNombreUsrNuevoUsuario(),
                    getRutNuevoUsuario(), getNombresNuevoUsuario(), getApellidosNuevoUsuario(), getEmailNuevoUsuario(), getCelularNuevoUsuario(),imgByteArr);
            cadenaRetorno = "success";
        } catch (Exception ex) {
            getLog().error("Error al listar Objeto. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }


    private static final Logger log = Logger.getLogger(CrearUsuarioProveedorAction.class);

    private String  retornarImagenDePruebasEnBase64(){
        String imagen = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2MBERISGBUYLxoaL2NCOEJjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY//AABEIAWgB4AMBIgACEQEDEQH/xAAbAAEAAwEBAQEAAAAAAAAAAAAAAQIDBAUGB//EADkQAAICAQMCAggGAgEEAgMAAAABAhEDBBIhMUETUQUUFSJSYZHRMkJxcoGxBjShIyQzYhbhQ5Lx/8QAGQEBAQEBAQEAAAAAAAAAAAAAAAECAwQF/8QAKhEBAQEAAwAAAwYHAQAAAAAAAAERAhIhEyIxA0FRYZHBI0NxgaGx8AT/2gAMAwEAAhEDEQA/APz8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHor0LqWk9+Ln5v7D2NqfjxfV/YuDzgej7G1Hx4vq/sR7H1Hx4vq/sMHng9D2PqPjxfV/YeyNR8eL6v7DB54PR9jaj48X1f2C9C6l/nxfV/YmDzgel7E1Px4vq/sPYmp+PF9X9hg80HpP0LqV+fF9X9ivsfUfHi+r+wweeD0PY+o+PF9X9h7H1Hx4vq/sMHng7/ZOf48f1f2HsnP8eP6v7AcAO/2Tn+PH9X9h7Jz/AB4/q/sBwA9D2RqPjxfV/Yex9R8eL6v7AeeD0PY+o+PF9X9iy9C6l/nxfV/YDzQei/QupX58X1f2I9j6j48X1f2A88Hf7I1Hx4/q/sPZOf48f1f2A4Ad3srP8WP6v7EP0ZmX5sf1f2A4gdns3N8UPq/sR7OzfFD6v7AcgOv2dm+KH1f2Hs7N8UPq/sByA6/Z2b4ofV/Yezs3xQ+r+wHIDr9nZvih9X9h7OzfFD6v7AcgOv2fl+KH1f2Hs/L8UPq/sByA6/Z+X4ofVkeoZfih9WByg6vUcvxQ+rHqOXzh9WByg6vUcvxQ+rI9SyecfqBzA6fUsnnH6j1LL5x+oHMDp9SyfFD6kep5POP1A5wdHqeTzj9R6pk84/UDnBv6pk84j1WfnEDAG3q0/OI9Wn5xAxBt6tPziR6vPziBkDX1eXmh4EvNAZA18CXmiHhku6A+tiv+nH9CKNMcbxR/REONG2WbRGxvoi9cl4oKw2PuSoqjdxsKIRiok9DXaVaAoLLUVkQCrSZG4iwD4KMsyrQVABNARRIJSAmKstREUaAVUS3QIt2AoyriXoUBTaQ4mqRDQGDiUcUbyRlIDNpFWi7RFMgpRBfaxtAzRJbYSoAZg02oigM6ZKiaUQwM2iGi4aApSIovVEMKrRXaXKsCKIZJFoCpDLNlbAUQxZDYAqSQBBBYWBWiKLWCCjQLEMCpEuhJWT4A+v08LwxvyRMsdjTtPFC30SOjanymbZcmzklQVnRsrmijXPAGUolY9ao170Q+Aqko0Uao1l0Mm+wRSXBR8m3VFJRIMqFFytgRRBZlQqGgkTRNARRKiWii1AEhRJJBSiUWolQKIirJ2l0qAFVEhrgtZSTAzkZtGrRXbZBnSFGu0hoDHaHE0aKtAUFEtoq5xXcCKFEPLEq8oFmQZubZRzYGroq2jPcVcgNXJFdxk5EbgNdxFmVi2FaNlGRZFgAVckVcgL2RaKORWwNXIrZRMWQXsWUIsC+4hyKbkQ5AX3EORm5EOQF9xDlwZtkOQH3WDEpYY9ntRpGO1dS2nf8A0oL/ANUXeNPqbZZObRVO3yi2THXQx3NOgLSxpu0yk1XNi2LtUwrNybISRdwsJUEU20xRo0NpBhOJnRvMzaAzoOJehQVWiUi1FlECEidrLKJagMlGyyiaqHkKoCuwWkXKSlFdyBdirMpZoozlqijeSooznlqWZyzt9wOttLqUeWC7nHLI33KOTIOqWoiuhnLUHOyrA2edlHmb7mYCrObZVuyLIcgJBRyKuQGjkVcijZARZyI3EEOSCrJk2Z70Q5kGlkNme9kOTAu2yrZX3mRTAtZDK0xtYE2RZDS8xx5gTuRG8rwOPIBuIcmB/IENsgnjzIAECxaAEENh0B+hYYNYoNfCjoq48lsEU8MP2ozzOUXw+DbLHJ7vfk5Zys3lJvqjKUUwM1JounZVRLwxtgK54FGiSiUnwBHCIu0R1CRBWSKOJvtZGzzA56LbS7ikNr8gK7S6SLLGHtj3ClWTtS7mcs9dDGeo46gdLnGPcxlnS6HJPPbMnlYHVPO33MJZvmYuTZQgvKbZVtgWkA5YrzIcyjmFXdEWjJyfYryBq5pFHkKDgCzmyrkwQQRuZFsnn5D+QIsiw2vMWvICNzI5Jt+QdgRXmKXmQyLAtwRfyK2HICW38hfzKNkWBe/mVv5lSAL38yHXmVshsC1oi0VAE2iNzIdkfyBLZDIFACBQoCAKAAgAD9LWSePDj4X4UUlkeTqaYfewY1Ln3UUnjUZcG2XPKDRRRdnS0FADnUTWlFF9tEPngDNq+TKSN/kPD3EGCxt9i21RZ0KvwmUo8gQnZGxtl9qiveMp51FcAS8cY8sznmS6GOTO2YSnYGs9RJ9zGWV9ykpmMp2FaTymTm2UbIsCWyLKkkCyCWRYUIbDfkVbIDZHAIbQD9CKZDbI/VgS2kRu+RViwJbZF/MhlQLEbkVBRLkiNzBADcyG2SQAtkMkUBUhotQoCtAtRDQFQTQIKkUXoUUUoUWoEFaIaL0RQFKDLURQEFWXohoCgLUKAqQWoNAfpOJNYcaXwotfmbxjHwIdvdRm1G+p0ZZshIlzSf4SrytcKJBMlyZOrNIvdZVwbZBVRtmjagrFqC94wyzUgpJ73aD91deTN5FFdaM5Zd36BFcsmzmnI0y5ElRyTyWwqZTMpSIkyhAbZWySLCopiiWyGwIIFkORBJVtFXIq5AXbKuRW2RVhEuSIciNoooWQTQoCoLUNoFBRahQFaIoukTtCs6FF6JAzohouyKIK0Eie5JRXaKJsEEFWSyAIoULAChQ4IcgJogi2OQDIFEdAAbRFACLAIsBYIbG4CCX0IshtgfqcOMGJv4UZZ2kuKLaeaeDHF8raicuL3eOUbZcsWS7b4HhtM1UVHlgVhGuWRlm0uCMmVI4s2o8mQaOTb5ZhkydomEs0mUll2x+YF5SrmTMMmfsjOcnJ9SKIqJTbKlnRVsKjkq3ZLZW0QCrDZVyANlW2LbIKIfJFFqFAV2ii+0soBGNDabONFXSCs9ootdkxVhFdo20XlwUbsiodECmQEH1CIJTCpSEuA5fIpKQEXyRZFWTVAKsh2SmkhYERVktUQRYEkArygLRW6VPgv4ca6mKbsltsCzgl1ZTck6SG1kOLsCXRVtEvG/MjZ5sCNxG4s4oh433AjcRuJeNoeG2BVsbiXjonZx1Ap1IaZegBnQou6IYEKJFE2QwP0vT45eDCl+VHZBbcdyPTho4R0WOUVyoL+jxNXOak+eDbKMuWMW6XJw5NQ3LljJn4pnFknbA0y5r7nNJ2Q2yk5cECU6M22yUr6h0iKqQ2S2UdeYUbItENlWQJNFLZZ0QUVaILUSogUoUaqAryAook7TRqkTFWBSMRLgSkkzKU7IIlJlEm3yy1+ZNoA0kuCqlRLXmUsA5EWiVXchxQBN9i/h+bK7q6BzbAOKT6jhdiNz7DcyCG0yKj3Db8iH8uoE0mRSXcrTsNV0KLNJorXyIp9w3LswJ2vyI2/qSt1daG5rvZBUJtEuV9iY0gM3Jy7FeTe4t1QaS8gMtr8yKou4kbfMApeZFonahtAhuN2R4nyJpENgS5WRuoq2yrTb6gX3EORQi2UWbIbRApARZBbb5DawKckFqIaA/dMUd2hxrzxr+j5rXp45Siz6fTf6mL9i/o8H03hW9yjymaiV87kdyZi0bZY0zKSKjKRSrZdoiiKq+OhRsvIoRVGVZdlWgKkFqsAUoUWUWzSMOeQM1C+pdRSRpKPHBnKTqqIKt30J2OKJxqCdvqaOmuOgGHZqjKUmulm84uijxy2gZWurREpJ9ETKEiu1pgVdsJ0WcWQl8gIcmXUE4ptpFaLKNx46gJbVXdmU0Wkm2VpgVpENLsWpjbaAouCerIdoXXcC9beepDn8ijd9yUmyBuvsVD4ZBRL5JtLuVpF1FdiCN9drG5PsW6C0RVVT6Db8w2hce/IFXH50TtS7jfHyG5MBSIdE2RdlENryK7g4sbZPsEQ/1K2WeOfdMnYl+LgCFHcupV435k9W0kyHKVVXAEbfmQ0l8w5OiL80UG12RFk2vIRi5SoAlask6FBJJNURkxxT/EiaMKKSR0KEUuZoPFFr8XA0ftmn50eJf+i/o8rXR/FGSPW0v+ri/Yv6KajTQzRdrlmoj4rUwSk6OOaPoNd6NWOT5PLyadLuio89xIa4OieKimwK5pIo0byiUkiKyaIo0orXIFKpcEVybbCu2mAjGiWmmTNPihKVLkgiUlRkyZSso2rAVfYuu1Ff5F/Mgs5LzslTM277EKrugOjba5RlOFdjfTyhPG05VJeZaeKMqe5JE3FxwtchY+Lo63iwt0pKzSEIy91NKkOxjznilf4WUlGUezPVenpf8AlMvVd3MZ2TsvVyYsE5q6N/UpfJMtkxZktsZ/wVWPOl782kTauK+oO/ekqHqMY87+DGc80pbY7qNcUNQnzJ/oNp4yy6Zq3FJxMJaaTi3fK7HqKGRqpxjRFY4ulFDsdXmrT3VJ33LOEca5pvyPQeGMui/lMzyaaK5cb/RjsY8/JG1cYGccUn+WjtlNY+I4n/LMvWJydSx1H5F2s45nie7joR7y4XJtLxHfhwaj+hWOHM+aZdMZuddSryeSNZad9WVWHnhNjwZvJfZEbn5HXHSOXZlnpa4Y7QxxORW32O16W3STKSwKHVWNhjkqT7imvM6dr7RKuEu6LqYzT87Zbc+xooquSXFJcE1WW6T/ADCUW+nJOx2NrXcBFV2IlBefUdEVk2+wCWCXVLgzeKS7GqnNqkQ4zfmVGKRMU07XU1x46vcmabUuhNXGDnka7mb3PqmdcpRXSRHjLysaY5VGTfQu45Iqr4NvH+RSUk0/eGj9u0v+ri/Yv6LtFNL/AKuL9i/o0fQ0jzNblq4zimuzPE1UMck2lTPop6LxN2+Vpnl6/wBFTxrdjluj5GtZfP5OHRjLg7cunlF+9Ey8EK5JKzJxOyWKjGUOSDDbwUceTocaKNBUQ5XQpkSUrNYPbdqw4QkuOH8yDHddK6E266WWlgafyGxJVYGDjfYq4PyOhQ8uUS8dY6+YHJymWUbLuFdi0Y9OAMZY2lZVOup25cdQVnO8TpvsTVwxY97tM2jpW03JmOK4J8MmeZtUrM+rF4YXHJd3E6IQxpurb+ZzYnOT4dG+Sc4xS3cszbWpIiSxJpTj7z8mXjixwTlvaOZN3wrZE8m38cL/AJJg1lq8UY1zN+ZMdRgmlu3foZqOPIulFo6W3w/4Hi+r+LiXGPH/ACzOWfHGXRs0no8s3S4XyJxej8qXkvmTYeslqY3at/IZMk9trEjrh6PSak5KzSfuy2RimTYuOGEsklbil/BnLNNSfSj0pYpT4caRk9HjUncx2iY5cahnlTh/J0PDjXCgi6goLbH6kpcktXFVjivyoPHHyNaIaJo5M2mU4tJckYtNHGuls66KNF0xk4pdikkvI1kZyRRkzNwT6o2aK0VGG2O6qKzwv8qOnaiS6mPOlhm+iKPDPuepQeNSXKHYx5MsfzKeFb6npz0sX04KPSP4i9kxweAl1kPCp2mdnqaT5ZLwKH4VbHYxyeG/Kg4NG88Upq1wzGWLIu7Y0xRrjuUkl8RaUJ+TKLHOT4RUQ9i7WV/SJstPPujZY2lxQ0xyqM+0Q8Mmn7prkjkXcpHdJu+lDR+1aX/VxfsX9Ghnpf8AVxfsX9Gp0ZQcWvx5pR9x2juIA+Zzqd+9Gv1OSS+R9dPDjyfigmc2T0Zpp/kr9C6mPkp/tMZQPqsnoTDJe7Jo5MnoHIvwSTA+blAzlBnuZfROeHXG/wCDlnopx6xf0IrzFESgrpnXPTfKirxc8oDnXPBnNNPodMsPPumU8d/qBnFbmlXLNfD2KuTNJxdpuy0ss+zII8JTboh4skOyS+ZpHUNLmKZLzwydU4kutRlPJ8VMRkuyiTshL8//AAXWPH8ZjFRF88pP+CfDxO3KK+hK2J8ckN89qIuo2wXGOMU35lVpMkvxSSRpvXaiqyJ9ZA1K0cF1yfQn1LHJ252hcV+YePDH8yersaR9HQ67qReOPBidue75I55a3j3YmcdTzco2Z62mx3vPaeyNV0OPJqsspO3XyIWplK0lSM3FvkSYtqyyyck5SbN1nxr8u5nPGDNY4rLcF56mcn7vCM1FvlmixM0WOkZGUYltptHGyVj5JqqbSrib7SriFYuJRo2kjNlRjJGbRtJFGioycSNppRNFGVEOPBrtDiBkol1EtGJfaQZ7SHE1oJWBg4mc4+R1uA8NNDR5ziFfc7ngRWWFLsXTHHx3ihcFwkjq8BPsPVV5DUccowl+Yr4MF0f0O71K4tqDdFsWhlkXEKrzGjznHjp9SjhFRlUex60PR2Wc9qxujTN6IyQxyckklFjtDH6Jpf8AVxfsX9Gplpf9XF+xf0anocgAAAABAJAEUVlihJe9FP8AguAPP1fo/BKLahT+R5Wb0fBdHR9FOtrs8vO1ufBB4uTSODfRnPLAu6PTzK3wjmlDnoTVcTwJ/wD2c+TBTdHp+HzdCWGM+3BNXHjSxOhHFxyenLSrdwmRLAl2ZNMeZLE+yIUJHovC+kf6IemyVwv+CauOHw5dg4RS5s7PVZdyfVaXcauOF/8AquDPbJuj0Hid9GQsDa6Mmpjg2O+XwXlCL6M6npn5Mvj0sW+Wxq489YW2ax002/wnqrBHj3N38GsNM5fhhyZvNqcXl49JK+UarSvsmevDST+FHRHSV1RzvNrq8SGjk+xtHSNLoewtMT6vRjuuR5K0z8iXpmer4FFXh4HZceasFEvFR2yx0ZTgNMcM4pGUjfLxZzSZuM1R9SkizZXqUZTRRmsupmyoigiewKBVFgiAkWK9SaAJWXUSYRNVHgmjNQtlvDNIQ5NlCyDm8JslYG+x2RxnRjxKugHnR0mRq9peOklfKPWhCkTsV9Bo5semhjjyjRQjxSN9tqiHjoCIpdimrr1XJ+1luhjqn/2+T9rEH0ml/wBXF+xf0amWl/1cX7F/RqexwAAAAAAAgCQQSBDVmU9Njn1RsAOHJ6Pg+jObL6PlHlKz1g1Zm8V189LTNdivhNdj3cmnjLoYvRGLK1seP4TGxnsepD1FGcq7Hj+GyfCb6nr+oxD0SQymx5C06f5UXWmXken6rRPq9Gbq+PN9VvsvoStDB9UeisRZYjPq7Hnr0fjfWJrDQY0vwncsZpGAnC1LzccdDj+E2jpMcex0bSTpPsZ97N51j6vBO6J8KPkag38Lj+DPasXiXkPCXkbURZL9nxXtXNLGvIxnFI6pyOXLM8vOSOnC2uedeRzTrkvmzcHFkzWzMjsxz9zimzpyzs5MjOsc6q2RZVsruNC0nyVsiTK7giwSIvk0XQCnQNiVplLCtIljOLotuIOjErOhJUcuKZup8EGqRpBGMZG8HyB0QgbRSMISs3jLgguhyVZPYCV1LFUXQGUuphqV/wBvk/azoyGGb/XyftZYPotL/q4v2L+jUy0r/wC1xfsX9GtnsedIIAEgiwBJWUtvYsZ54qWDIpfhcWmBnp9Ss+PeotK2joOD0NCOP0ZhjBNRSdX+p3gACG6AkEEgCCQBBIAAgkARRFIsCYK7SaJAyCASCgAAIAspKdGbykFzOcqRSWWkcuXP15OHP7Wfc3x4Wr5cnU4dRmS4srl1HDPPz5jz+13k6py5evJyzyGeTKYSmdZE1rKZhOVlXMzcjWMrWUcqZVyKORcRdyI3GbkRuLg3Ujq0eGOryRxSz+Db/EedvCyuPRmbx2NcOd43Y1yKSzZovM34c3FPz5MnJvJW5pFFLmTfdkdZHPPlfYvOfG5S/Scfwn5Nscm5Sju4rqMeRqEpSbfkZwntdCPMHEjXLjx+a2fL8vv7t4RyyhuWSn2RrHUSnpJt8Tjwc8M+2NNO0TG46ed9Zcj+jHLjf5kk9mfq6dBqZbtmV/i5i2Tiy5Jw1f8A1JLbzHnp1OdR3aaFcSjyidNk24NS5dZL7k9dOvDlefLjPdkz+/7u/TRz5vRcskc0/Fttc+XYpHX59VjwabDKUczfvyXyM9NrPV/RtR/HbSXzKwhk0Dw6pNtv8a/UizhO3LtJ9b1/78HfrMmXHq8OnlqJYcO3nJ5v9TX0Vkz5/WcfiueOPGPK0cmpzY8muhk1UZz0u24UuL+Zt6NzPDk1OXFDJ6olcYvz+Q31y5cP4P09yfrv+1M2TPpdZghg1ktRKbqUOGfQVTPmdRPBl1GKXozFkhn3XJpNH0Hi0uetFjh/6+OTjv1/yvJKzDUJeBk/azz/AEr6W9Vw/wDQcZTk66/hPEfp/U48WeDfiva+Wuh048bXgtkfpGlf/a4v2L+jWz4vS/5qvAxw8BJxik+Tp/8AmMIpN44yvsj1vO+r3HP6Q1uPQ6WWWb6LhHzv/wA0wr8WnaVeZ8z6Y9Ny1udyk57Oyb4og+r0Hp7XanLtlhg3KSSjHsvM+mXQ/K3/AJDLSqEfR8pQ496UlyztX+da7wfDkobl+euSo/SDPUy26XK/KD/o+K0H+eJQUdVgcmvzR7nBrf8AMdbnlkjiahjlxXyIP0D0fHbocK/9bOk+W9Gf5dpMmPFiywljdJN9UdfpD/KdDooqt2ST7ID3rObVJOeFec0fNL/OtNt502RS/U5Z/wCXLU67BSljjGV/JhX2/QiORSuux5no/wBM4NXGTnkxpx8meXq/8h8P0tDBiyweJupf/wBGj6mxZxy1+lxY1KeoxxXS3I0w6vT547sWaE15pjTHRYKeJD4l9Rvj8S+o1GhBzT12lx7t+ohHb1uXQxXpj0e1xrMX/wCxdHeRuV1as4PbPo+UW4azE2v/AGPk9XrXL0nOeD0ltlKdr3vdSJeWLj7wHyOq/wAl9V0/hQ1Cy5Gv/IuiPIf+QelIZ8alqHNzd1HyJ3Xq/RbFnFo9bHVaWGW1Ftcxb6M1eaPxr6kvOHV0WVcqOaWqguPEj9TKWddpL6nPl9qs4V1TypHLlzpHHm12JRlLxY1Hq76Hnz9JYJzcI54OXlZxt5cnXjxkelk1Ndzizaq75OHNq4Ru8sV/JyS1cJK45Yv+RODWu3Jn46nJkzX3OeWdP86+pzZNXji/eyRX8nScWLXTPIZvIeZn9JVLbjSl8zF+k59PDRucWbyeq8hRzPKfpKfwIe0OOY8murPaPT3lHI832g/gLx12OXeh1XY7XMrvMt98lXOhhrfeRvOaOZSV8ol5Ki32Qw10bxvOCWuhFcW2Y+uzyS91NfIYmvV3l1kPGWpm8lKT57HVPVLHHqpSXYdTXorIXjkPIXpFU7jyF6Qld7Pd8ydauvajlN8eY+e9fybrSSiuqZfUa/xMCWKTi31J0Oz6jFmTr3kdHjY8UN+WahHzbPhcWvyY9sbbUXfU69d6QlrYLG1tjjVpvuyfDurOcfY6fWafPKsWeEn5WdEs8cd7pxVK3yfneGMlF58M2pw5M4y1OXJJb8jk173PYfC/Ne79ExekdNlaUM8G/Ky+py7MMmpxjJr3dzrk/PtLPHp9RGXvT288di/pbVZtVqt1yjjaW1brJ8L07+NvSWrlHLJT8Oc7duPQ8/Hlyvh5GovryWhhnPHcaW3v5m8Y446KSlCMXJfifLb+R2mRz21xxzyi+GarUT45fBzTlum5bVG+y6DczWMO31hyVNjxLXS0ji3snxJfImUdjnF8uNEcN24o5PFl8ifGl8iZR2JqKpJDckvwnJ48/kPWJ/IZR2RzbHatF563xGvEts4PHl5Ih5m+qiXKruWbE/NDet3us4PFfki3jyrpEmVHq4NbmwJ+G1z5MzyZ5ZMm/wDC7PP9Zn5RC1M0+iGVXp582TP1yNLyJ0+p1GmTWLNKKfWnR5q1k1+SH0ZWepnN8qK/QZT19d6K9O5cmSOKcpSjH8UrO/0hrcu+tLlk41362fEaf0jk08ax48X6tO/7Nvbmq+HF9H9ydWt8d08OaWScsmbc5O+WZyw5FK0rVdLOWfprPNVLDgvz2u/7M/auf4Mf0f3L1qOycM1c43/CMKk7cvd+Rl7Vz/Bj+j+49q5/hx/R/cZUa0n7scnK630IjKWCSm/frokZP0llfXFhf6x/+yj10n/+DD/Cf3GUevi1+SEajkcV+prD0pk/C8kv1s8F6uT6Qgv0v7lfWsnyM9F7V7/r9u3P/kjJ6Ql4ctuWSddVI8NazIndR+gesyN3UR0p2ruxPJkxS/60op9r6mawSct7l355OR6rI+yI9ZyVXBrKmvUkk/zqv1IUIv8AOl/J50dXOPSMfoHrMj7R+gymvS8CN/8AljX7is8MF+aLf6nnPV5H2j9CPWZvtH6DKa9NYce1vdG68zD1dVzNfU4/WZ/DEn1qXww/5GUdHhNXTTCxOuWjnWqmvyx/5HrU/hiX0avDK+vBDxNLhcmfrU/KJHrM/kPRupZ10k1x0J8TO+5z+sz8kPWZ+SGU2t3HI00pPnqVaz9HddDL1rJ8g9TkdW1wPRfwpVymI42r91/qU9ayfIes5PkPRZYpJN1ZXwp/Cx6zk+Q9Yn8i+i3hO7rtyHGWykinrE/kR48/kPRolXUidVUSnjS8kPGl5RJ6IUZLsXhKUevK7rzI8eXlEjx5eSL6OyGr20ljUY7aaX9muj1GDFkySz43k3KkzzvGl5IeNL5Geq9q6YZMUNTuUKhfKKRnw1dLda4MfGl5IeNLyRcNd2HU4oYZpprK3w64Mc+onklV3BdFVI5/Gl5IeNKq4GGswAaQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/Z";
        return imagen;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsrNuevoUsuario() {
        return nombreUsrNuevoUsuario;
    }

    public void setNombreUsrNuevoUsuario(String nombreUsrNuevoUsuario) {
        this.nombreUsrNuevoUsuario = nombreUsrNuevoUsuario;
    }

    public String getRutNuevoUsuario() {
        return rutNuevoUsuario;
    }

    public void setRutNuevoUsuario(String rutNuevoUsuario) {
        this.rutNuevoUsuario = rutNuevoUsuario;
    }

    public String getNombresNuevoUsuario() {
        return nombresNuevoUsuario;
    }

    public void setNombresNuevoUsuario(String nombresNuevoUsuario) {
        this.nombresNuevoUsuario = nombresNuevoUsuario;
    }

    public String getApellidosNuevoUsuario() {
        return apellidosNuevoUsuario;
    }

    public void setApellidosNuevoUsuario(String apellidosNuevoUsuario) {
        this.apellidosNuevoUsuario = apellidosNuevoUsuario;
    }

    public String getEmailNuevoUsuario() {
        return emailNuevoUsuario;
    }

    public void setEmailNuevoUsuario(String emailNuevoUsuario) {
        this.emailNuevoUsuario = emailNuevoUsuario;
    }

    public String getCelularNuevoUsuario() {
        return celularNuevoUsuario;
    }

    public void setCelularNuevoUsuario(String celularNuevoUsuario) {
        this.celularNuevoUsuario = celularNuevoUsuario;
    }

    public Integer getIdRolNuevoUsuario() {
        return idRolNuevoUsuario;
    }

    public void setIdRolNuevoUsuario(Integer idRolNuevoUsuario) {
        this.idRolNuevoUsuario = idRolNuevoUsuario;
    }

    public File getAvatar() {
        return avatar;
    }

    public void setAvatar(File avatar) {
        this.avatar = avatar;
    }

    public String getAvatarContentType() {
        return avatarContentType;
    }

    public void setAvatarContentType(String avatarContentType) {
        this.avatarContentType = avatarContentType;
    }

    public String getAvatarFileName() {
        return avatarFileName;
    }

    public void setAvatarFileName(String avatarFileName) {
        this.avatarFileName = avatarFileName;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
