/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.AdjuntoDAO;
import com.skydream.coreot.dao.LibroObraDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.UsuarioDAO;
import com.skydream.coreot.pojos.Adjuntos;
import com.skydream.coreot.pojos.AdjuntosLibroObras;
import com.skydream.coreot.pojos.LibroObras;
import com.skydream.coreot.pojos.Repositorios;
import com.skydream.coreot.util.SecureTransmission;

import java.io.*;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.jboss.logging.Logger;

/**
 *
 * @author rrr
 */
public class DescargarAdjuntosOTAction extends ActionSupport {

    private InputStream fileInputStream;
    private String nombreArchivo;
    private int usuarioId;
    private int otId;
    private Integer libroDeObraId;
    private Integer archivoId;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";

        try {
            if(libroDeObraId!=null){
                Repositorios rep = UsuarioDAO.getINSTANCE().obtenerRepositorioPorUsuarioId(getUsuarioId());
                LibroObras libroObras = (LibroObras)LibroObraDAO.getINSTANCE().retornarObjCoreOtPorId(getLibroDeObraId());
                List<AdjuntosLibroObras> adjuntosLibroObras = new ArrayList(libroObras.getAdjuntos());
                AdjuntosLibroObras adjLibroObras =  null;//(AdjuntosLibroObras)adjuntosLibroObras.get(0);
                for(AdjuntosLibroObras itemAdjLibroObras : adjuntosLibroObras){
                    if(itemAdjLibroObras.getNombre().equals(nombreArchivo)){
                        adjLibroObras = itemAdjLibroObras;
                    }
                }

                String url = adjLibroObras.getCarpeta();//rep.getUrl() + "/Ordenes_de_Trabajo/Ot-"+otId+"/Libro-"+libroDeObraId;
                log.debug("Ruta del adjunto que se solicitará --> " + url);
                byte[] byteArr = SecureTransmission.getFile(rep.getServidor(), rep.getUsuario(), rep.getPassword(), url, getNombreArchivo());
//                File file = byteArrayToFile(byteArr, url, getNombreArchivo());
//                setFileInputStream(new FileInputStream(file));
                setFileInputStream(new ByteArrayInputStream(byteArr));
            }else {
                Repositorios rep = UsuarioDAO.getINSTANCE().obtenerRepositorioPorUsuarioId(getUsuarioId());
                List<Adjuntos> adjuntosOt = AdjuntoDAO.getINSTANCE().obtenerAdjuntosPorOt(getOtId());
                Adjuntos adjunto =  null;//(Adjuntos)adjuntosOt.get(0);
                for(Adjuntos item : adjuntosOt){
//                    if(item.getNombre().equals(nombreArchivo)){
//                        adjunto = item;
//                        break;
//                    }
                    if(item.getId() == getArchivoId()){
                        adjunto = item;
                        break;
                    }
                }

                String url = adjunto.getCarpeta();//rep.getUrl() + "/Ordenes_de_Trabajo/Ot-"+otId+"/Libro-"+libroDeObraId;
                setNombreArchivo(adjunto.getNombre());
                byte[] byteArr = SecureTransmission.getFile(rep.getServidor(), rep.getUsuario(), rep.getPassword(), url, adjunto.getNombre());
//                File file = byteArrayToFile(byteArr, url, getNombreArchivo());
//                setFileInputStream(new FileInputStream(file));
                setFileInputStream(new ByteArrayInputStream(byteArr));
            }

            cadenaRetorno = "success";

        } catch (NumberFormatException | IOException ex) {
            log.error("Error al descargar archivo. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public static File byteArrayToFile(byte[] bytearray, String rutaArchivo, String nombreArchivo) throws IOException {
        File file = new File(rutaArchivo + "/" + nombreArchivo);
        log.debug("Path del archivo : " + file.getPath());
        file.createNewFile();
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bytearray);
        fos.close();
        return file;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getOtId() {
        return otId;
    }

    public void setOtId(int otId) {
        this.otId = otId;
    }

    public Integer getLibroDeObraId() {
        return libroDeObraId;
    }

    public void setLibroDeObraId(Integer libroDeObraId) {
        this.libroDeObraId = libroDeObraId;
    }

    public int getArchivoId(){
        return archivoId;
    }

    public void setArchivoId(int archivoId){
        this.archivoId = archivoId;
    }

    private static final Logger log = Logger.getLogger(DescargarAdjuntosOTAction.class);

}
