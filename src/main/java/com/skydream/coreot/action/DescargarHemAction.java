package com.skydream.coreot.action;

import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.AdjuntosPagos;
import com.skydream.coreot.pojos.Login;
import com.skydream.coreot.pojos.Pagos;
import com.skydream.coreot.pojos.Roles;
import com.skydream.coreot.util.SecureTransmission;
import org.apache.struts2.ServletActionContext;
import org.jboss.logging.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by mcj on 29-06-16.
 */
public class DescargarHemAction {

    private InputStream fileInputStream;
    private int usuarioId;
    private Integer actaId;
    private Integer id;
    private String namePdf;


    public String execute() throws Exception {

        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(), true);

        String cadenaRetorno = "";
        try {
            Login login = LoginLogoutDAO.getINSTANCE().retornarLoginSesionActual(ServletActionContext.getRequest().getParameter("tk"));

            if(login.getRoles().getId() == 4 || login.getRoles().getId() == 7){ // trabajador y coordinador
                throw new Exception("Usuario no permitido para realizar esta operacion.");
            }else {
                if(id!=null){
                    int pagoId = GenericDAO.getINSTANCE().obtenerIdPagoPorBolsa(getId());
                    AdjuntosPagos adjuntoPago = GenericDAO.getINSTANCE().obtenerArchivoHem(pagoId);
                    setFileInputStream(new ByteArrayInputStream(adjuntoPago.getArchivoHem()));
                    setNamePdf("HEM_" + adjuntoPago.getHemId() + ".pdf");
                }
            }

            cadenaRetorno = "success";
        } catch (NumberFormatException | IOException ex) {
            log.error("Error al descargar archivo. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Integer getActaId() {
        return actaId;
    }

    public void setActaId(Integer actaId) {
        this.actaId = actaId;
    }

    public String getNamePdf() {
        return namePdf;
    }

    public void setNamePdf(String namePdf) {
        this.namePdf = namePdf;
    }

    private static final Logger log = Logger.getLogger(DescargarHemAction.class);


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
