package com.skydream.coreot.action;

import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.AdjuntosPagos;
import com.skydream.coreot.pojos.Login;
import org.apache.struts2.ServletActionContext;
import org.jboss.logging.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by mcj on 29-06-16.
 */
public class DescargarHemActionPorActaId {

    private InputStream fileInputStream;
    private int usuarioId;
    private Integer actaId;
    private String namePdf;


    public String execute() throws Exception {

        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(), true);

        String cadenaRetorno = "";
        int pagoId;
        try {
            Login login = LoginLogoutDAO.getINSTANCE().retornarLoginSesionActual(ServletActionContext.getRequest().getParameter("tk"));

            if(login.getRoles().getId() == 4 || login.getRoles().getId() == 7){ // trabajador y coordinador
                throw new Exception("Usuario no permitido para realizar esta operacion.");
            }else {
                if(actaId!=null){
                    try{
                        pagoId = GenericDAO.getINSTANCE().obtenerIdPagoPorActa(getActaId());
                    }catch (NullPointerException e){
                        throw new Exception("Esta acta no registra pago asociado.");
                    }
                    AdjuntosPagos adjuntoPago = GenericDAO.getINSTANCE().obtenerArchivoHem(pagoId);
                    setFileInputStream(new ByteArrayInputStream(adjuntoPago.getArchivoHem()));
                    setNamePdf("HEM_" + adjuntoPago.getHemId() + ".pdf");
                }
            }

            cadenaRetorno = "success";
        } catch (NumberFormatException | IOException ex) {
            log.error("Error al descargar archivo. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Integer getActaId() {
        return actaId;
    }

    public void setActaId(Integer actaId) {
        this.actaId = actaId;
    }

    public String getNamePdf() {
        return namePdf;
    }

    public void setNamePdf(String namePdf) {
        this.namePdf = namePdf;
    }

    private static final Logger log = Logger.getLogger(DescargarHemActionPorActaId.class);

}
