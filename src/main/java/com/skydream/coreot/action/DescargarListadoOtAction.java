package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LibroObraDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.UsuarioDAO;
import com.skydream.coreot.pojos.AdjuntosLibroObras;
import com.skydream.coreot.pojos.LibroObras;
import com.skydream.coreot.pojos.Repositorios;
import com.skydream.coreot.util.SecureTransmission;
import com.skydream.eventmanager.Facade;
import org.apache.struts2.ServletActionContext;
import org.jboss.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mcj on 17-12-15.
 */
public class DescargarListadoOtAction extends ActionSupport {

    private InputStream fileInputStream;
    private int usuarioId;
    private int otId;
    private String fechaInicio;
    private String fechaTermino;


    public String execute() throws Exception {

//        HttpServletRequest request = ServletActionContext.getRequest();
//        String token = request.getParameter("tk");
//        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(token, getUsuarioId(),true);
        String cadenaRetorno = "";
        try {
            Facade facade = new Facade();
            byte[] bArray = facade.retornarArchivoDescargaEnExcel(usuarioId);
            setFileInputStream(new ByteArrayInputStream(bArray));
//            File file = new File("Arch_Temporal.xlsx");
//            file.createNewFile();
//            FileOutputStream fos = new FileOutputStream(file);
//            fos.write(bArray);
//            fos.close();
//            setFileInputStream(new FileInputStream(file));
            cadenaRetorno = "success";
        } catch (NumberFormatException | IOException ex) {
            log.error("Error al descargar archivo. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getOtId() {
        return otId;
    }

    public void setOtId(int otId) {
        this.otId = otId;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(String fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    private static final Logger log = Logger.getLogger(DescargarListadoOtAction.class);

}
