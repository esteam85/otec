package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.Login;
import com.skydream.coreot.pojos.Roles;
import com.skydream.eventmanager.Facade;
import org.apache.struts2.ServletActionContext;
import org.jboss.logging.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;


public class DescargarPDFActaAction extends ActionSupport {

    private InputStream fileInputStream;
    private int usuarioId;
    private int actaId;
    private int otId;
    private String namePdf;


    public String execute() throws Exception {

        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(), true);

        String cadenaRetorno = "";
        try {
            Login login = LoginLogoutDAO.getINSTANCE().retornarLoginSesionActual(ServletActionContext.getRequest().getParameter("tk"));

            if(login.getRoles().getId() == 4 || login.getRoles().getId() == 7){ // trabajador y coordinador
                throw new Exception("Usuario no permitido para realizar esta operacion.");
            }else {
                byte[] bArray = null;



                if(GenericDAO.getINSTANCE().validaPagoProbado(getActaId())){
                    if(GenericDAO.getINSTANCE().existePdfActaPagada(getActaId())){
                        bArray = GenericDAO.getINSTANCE().obtenerPdfActaPagada(getActaId(), getOtId());
                    }else{
                        bArray = GenericDAO.getINSTANCE().obtenerPdfActa(getActaId(), getOtId(), getUsuarioId());
                    }
                    setFileInputStream(new ByteArrayInputStream(bArray));
                    setNamePdf("pdf_acta_"+getActaId()+"_"+getOtId()+".pdf");
                }else {
                    throw new Exception("Esta acta aun no ha sido aprobada.");
                }


            }
            cadenaRetorno = "success";
        } catch (NumberFormatException | IOException ex) {
            log.error("Error al descargar archivo. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getActaId() {
        return actaId;
    }

    public void setActaId(int actaId) {
        this.actaId = actaId;
    }

    public int getOtId() {
        return otId;
    }

    public void setOtId(int otId) {
        this.otId = otId;
    }

    public String getNamePdf() {
        return namePdf;
    }

    public void setNamePdf(String namePdf) {
        this.namePdf = namePdf;
    }

    private static final Logger log = Logger.getLogger(DescargarPDFActaAction.class);

}
