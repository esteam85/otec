package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.Login;
import com.skydream.coreot.pojos.Roles;
import com.skydream.eventmanager.Facade;
import org.apache.struts2.ServletActionContext;
import org.jboss.logging.Logger;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;


public class DescargarReporteGeAction extends ActionSupport {

    private InputStream fileInputStream;
    private int usuarioId;
    private String fechaInicio;
    private String fechaTermino;
    private String estado;
    private int rolId;

    public String execute() throws Exception {

        //LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(), true);

        String cadenaRetorno = "";
        try {
            Facade facade = new Facade();
            //Login login = LoginLogoutDAO.getINSTANCE().retornarLoginSesionActual(ServletActionContext.getRequest().getParameter("tk"));

            if(getRolId() == 4 || getRolId() == 7){ // trabajador y coordinador
                throw new Exception("Usuario no permitido para realizar esta operacion.");
            }else {
                byte[] bArray = facade.retornarArchivoGestionEconomicaEnExcel(getUsuarioId(), getRolId(), getEstado());
                setFileInputStream(new ByteArrayInputStream(bArray));
            }
            cadenaRetorno = "success";
        } catch (NumberFormatException | IOException ex) {
            log.error("Error al descargar archivo. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(String fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getRolId() {
        return rolId;
    }

    public void setRolId(int rolId) {
        this.rolId = rolId;
    }

    private static final Logger log = Logger.getLogger(DescargarReporteGeAction.class);

}
