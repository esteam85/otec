package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.Login;
import com.skydream.eventmanager.Facade;
import org.apache.struts2.ServletActionContext;
import org.jboss.logging.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;


public class DescargarReporteOtAction extends ActionSupport {

    private InputStream fileInputStream;
    private int usuarioId;
    private String fechaInicio;
    private String fechaTermino;


    public String execute() throws Exception {

        //LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(), true);

        String cadenaRetorno = "";
        try {
            Login login = LoginLogoutDAO.getINSTANCE().retornarLoginSesionActual(ServletActionContext.getRequest().getParameter("tk"));
            Facade facade = new Facade();
            byte[] bArray = facade.retornarArchivoReporteEnExcel(usuarioId, login.getRoles().getId());
            setFileInputStream(new ByteArrayInputStream(bArray));
            cadenaRetorno = "success";
        } catch (NumberFormatException | IOException ex) {
            log.error("Error al descargar archivo. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(String fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    private static final Logger log = Logger.getLogger(DescargarReporteOtAction.class);

}
