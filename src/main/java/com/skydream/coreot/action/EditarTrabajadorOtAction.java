/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.OtDAO;
import com.skydream.coreot.dao.UsuarioDAO;
import com.skydream.coreot.pojos.Ot;
import com.skydream.coreot.pojos.Usuarios;
import com.skydream.eventmanager.Facade;
import com.skydream.eventmanager.GatewayAcciones;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mcj
 */
public class EditarTrabajadorOtAction extends ActionSupport {

    private String idEvento;
    private String parametrosEvento;
    private String jsonRetornoEjecucion;
    private int usuarioId;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";

        try {
            ObjectMapper mapper = new ObjectMapper();
            Map mapaParams = mapper.readValue(parametrosEvento, new TypeReference<HashMap<String,Object>>() {
            });

            Map trabajadorMap = (Map) mapaParams.get("trabajador");
            String trabajadorId = trabajadorMap.get("id").toString();
            int otId = (int) mapaParams.get("otId");
            int eventoId = (int) mapaParams.get("eventoId");

            boolean registro = UsuarioDAO.getINSTANCE().actualizarTrabajadorWorkfloEventoEjecucion(otId,eventoId, Integer.parseInt(trabajadorId));


            jsonRetornoEjecucion = "Ejecutado exitosamente";


            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al ejecutar Evento. ", ex);
            throw ex;
        }
        return cadenaRetorno;

    }

    private static final Logger log = Logger.getLogger(EditarTrabajadorOtAction.class);


    public String getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(String idEvento) {
        this.idEvento = idEvento;
    }

    public String getParametrosEvento() {
        return parametrosEvento;
    }

    public void setParametrosEvento(String parametrosEvento) {
        this.parametrosEvento = parametrosEvento;
    }

    public String getJsonRetornoEjecucion() {
        return jsonRetornoEjecucion;
    }

    public void setJsonRetornoEjecucion(String jsonRetornoEjecucion) {
        this.jsonRetornoEjecucion = jsonRetornoEjecucion;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
