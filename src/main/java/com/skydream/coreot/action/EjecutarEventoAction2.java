/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.eventmanager.Facade;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mcj
 */
public class EjecutarEventoAction2 extends ActionSupport {
    
    private String idEvento;
    private int usuarioId;
    private String parametrosEvento;
    private String jsonRetornoEjecucion;
    
    
    public String execute() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        String token = request.getParameter("tk");
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(token, getUsuarioId(),true);

        log.info(request.getQueryString());
        GenericDAO.getINSTANCE().registrarJournal(token, usuarioId, request.getServletPath(),
                URLDecoder.decode(request.getQueryString(), "UTF-8"), new Date());

        String cadenaRetorno = "";

        try {
            ObjectMapper mapper = new ObjectMapper();
            Map mapaParams = mapper.readValue(parametrosEvento, new TypeReference<HashMap<String,Object>>() {
                    });

            if(mapaParams.containsKey("datosOt")){
                Map datosOt = (Map)mapaParams.get("datosOt");
                if(datosOt.containsKey("id")){
                    int otId = Integer.parseInt(datosOt.get("id").toString());
                    int eventoId = Integer.parseInt(getIdEvento());
                    GenericDAO.getINSTANCE().validarEjecucionDeEvento(usuarioId,eventoId,otId);
                }
            }

            mapaParams.put("usuarioEjecutor", usuarioId);
            Facade facade = new Facade();
            facade.ejecutarEvento2(Integer.parseInt(idEvento), mapaParams);
            jsonRetornoEjecucion = "Ejecutado exitosamente";
//            setListadoObjCoreOT(ow.writeValueAsString(listaRetorno));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al ejecutar Evento. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }
    

    public String getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(String idEvento) {
        this.idEvento = idEvento;
    }

    public String getParametrosEvento() {
        return parametrosEvento;
    }

    public void setParametrosEvento(String parametrosEvento) {
        this.parametrosEvento = parametrosEvento;
    }

    public String getJsonRetornoEjecucion() {
        return jsonRetornoEjecucion;
    }

    public void setJsonRetornoEjecucion(String jsonRetornoEjecucion) {
        this.jsonRetornoEjecucion = jsonRetornoEjecucion;
    }
    
    private static final Logger log = Logger.getLogger(EjecutarEventoAction2.class);


    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
