/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.jboss.logging.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Christian
 */
public class EliminarAsBuiltAction extends ActionSupport {
    

    private int id;
    private int idForm;
    private String tipoAsBuilt;
    private String retorno;

    
    public String execute() throws Exception {

        String cadenaRetorno = "";
        try {

            String urlServicio = "http://10.232.139.111/api_asbuilts/api/";

            switch (tipoAsBuilt) {
                case "cercoAgricola":
                    urlServicio = urlServicio + "infra_cercoperimetral/";
                    break;
                case "cercoPerimetral":
                    urlServicio = urlServicio + "infra_cercoperimetral/";
                    break;
                case "escalerilla":
                    urlServicio = urlServicio + "infra_escalerilla/";
                    break;
                case "torres":
                    urlServicio = urlServicio + "infra_torre/";
                    break;
                default:
                    throw new Exception("Tipo AsBuilt no encontrado");
            }

            urlServicio = urlServicio + id + "/" + idForm;

            URL url = new URL(urlServicio);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Content-Type", "application/json");

            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }

            conn.disconnect();


            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al agregar cerco agricola. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(EliminarAsBuiltAction.class);



    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipoAsBuilt() {
        return tipoAsBuilt;
    }

    public void setTipoAsBuilt(String tipoAsBuilt) {
        this.tipoAsBuilt = tipoAsBuilt;
    }

    public int getIdForm() {
        return idForm;
    }

    public void setIdForm(int idForm) {
        this.idForm = idForm;
    }
}
