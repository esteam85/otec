package com.skydream.coreot.action;
/**
 *
 * @author mcj
 */
import java.io.File;
import javax.servlet.http.HttpServletRequest;

import com.skydream.coreot.dao.LoginLogoutDAO;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.UsuarioDAO;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;
 
public class EnviarImagenAction extends ActionSupport implements ServletRequestAware {
    
    private File image;
    private String nombre;
    private String imageContentType;
    private String imageFileName;
    private int usuarioId;
 
    private HttpServletRequest servletRequest;
 
    @Override
    public String execute() throws Exception{
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        try {
            String filePath = servletRequest.getSession().getServletContext().getRealPath("/");
            System.out.println("Server path:" + filePath);
            ByteArrayOutputStream baos = leerImagen();
            System.out.println("NOMBRE COMPLETO IMAGEN : " + this.imageFileName);
            
            gestionarImagen(baos);
        }catch (Exception e) {
            e.printStackTrace();
            addActionError(e.getMessage());
            return INPUT;
        }
        return "success";
    }

    private ByteArrayOutputStream leerImagen() throws IOException, FileNotFoundException {
        //            File fileToCreate = new File(filePath, this.imageFileName);
        ByteArrayOutputStream baos;
        InputStream is = new FileInputStream(getImage());
        try (BufferedInputStream bis = new BufferedInputStream(is)) {
            baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int readCount;
            while ((readCount  = bis.read(buffer)) > 0) {
                baos.write(buffer, 0, readCount);
            }
        }
        return baos;
    }

    private void gestionarImagen(ByteArrayOutputStream baos) throws Exception {
        //Se extrae cadena con identificadores
        //** Identificador 1 : Concepto
//        String[] varAux = getNombre().split(Pattern.quote("."));
        String[] varAux = getImageFileName().split(Pattern.quote("."));
        
        String[] cad = varAux[0].split("-");
        for(int i=0;i<cad.length;i++)
            System.out.println("Identificador " + cad[i]);
        
        if (cad.length == 2) {
            switch (cad[0]) {
                case "avatar":
                    UsuarioDAO.guardarAvatarUsuario(cad[1], baos.toByteArray());
                    break;
            }
        } else if (cad.length == 3) {
            switch (cad[0]) {
                case "documento":
                    UsuarioDAO.guardarDocumentoUsuario(cad[1], cad[2], baos.toByteArray());
                    break;
            }
        }
//            FileUtils.copyFile(this.image, fileToCreate);
    }


    public File getImage() {
        return image;
    }
 
    public void setImage(File image) {
        this.image = image;
    }
 
    public String getImageContentType() {
        return imageContentType;
    }
 
    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }
 
    public String getImageFileName() {
        return imageFileName;
    }
 
    public void setImageFileName(String imageFileName) {
        this.imageFileName = imageFileName;
    }
 
    @Override
    public void setServletRequest(HttpServletRequest servletRequest) {
        this.servletRequest = servletRequest;
 
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
