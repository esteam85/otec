/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.OtDAO;
import org.apache.struts2.ServletActionContext;
import org.jboss.logging.Logger;

/**
 *
 * @author mcj
 */
public class FinalizarTrabajoAction extends ActionSupport {
    
    private int evento;
    private int ot;
    private int usuarioEjecutor;
    private boolean estadoOK;
    private int usuarioId;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        setEstadoOK(false);
        String cadenaRetorno = "";
        try {
            Boolean respuesta = OtDAO.getINSTANCE().finalizaTrabajo(getEvento(), getOt(), getUsuarioEjecutor());
            if (respuesta) {
                setEstadoOK(true);
            }
            cadenaRetorno = "success";
        } catch (Exception ex) {
            cadenaRetorno = "error";
            log.error("Error al finalizar trabajo. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public int getEvento() {
        return evento;
    }

    public void setEvento(int evento) {
        this.evento = evento;
    }

    public int getOt() {
        return ot;
    }

    public void setOt(int ot) {
        this.ot = ot;
    }

    public int getUsuarioEjecutor() {
        return usuarioEjecutor;
    }

    public void setUsuarioEjecutor(int usuarioEjecutor) {
        this.usuarioEjecutor = usuarioEjecutor;
    }

    public boolean isEstadoOK() {
        return estadoOK;
    }

    public void setEstadoOK(boolean estadoOK) {
        this.estadoOK = estadoOK;
    }
    
    private static final Logger log = Logger.getLogger(FinalizarTrabajoAction.class);

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
