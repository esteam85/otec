/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.OtDAO;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author Erbi
 */
public class FinalizarTramiteAction extends ActionSupport {
    
    private int otTramiteId;
    private boolean estadoOK;
    private int usuarioId;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        estadoOK = false;
        String cadenaRetorno = "";
        try {
            OtDAO otDAO = OtDAO.getINSTANCE();
            Boolean respuesta = OtDAO.getINSTANCE().finalizarTramite(getOtTramiteId(),getUsuarioId());
            if (respuesta) {
                estadoOK = true;
            }
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al finalizar el trámite. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public int getOtTramiteId() {
        return otTramiteId;
    }

    public void setOtTramiteId(int otTramiteId) {
        this.otTramiteId = otTramiteId;
    }
    
    public boolean isEstadoOK() {
        return estadoOK;
    }

    public void setEstadoOK(boolean estadoOK) {
        this.estadoOK = estadoOK;
    }

    private static final Logger log = Logger.getLogger(FinalizarTramiteAction.class);

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
