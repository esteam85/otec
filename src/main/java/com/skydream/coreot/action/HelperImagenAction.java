/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.Result;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.ServletActionContext;

public class HelperImagenAction implements Result {

    @Override
    public void execute(ActionInvocation invocation) throws Exception {

        ObtenerImagenAction action = (ObtenerImagenAction) invocation.getAction();
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setContentType(action.getCustomContentType());
        response.getOutputStream().write(action.getCustomImageInBytes());
        response.getOutputStream().flush();

    }

}
