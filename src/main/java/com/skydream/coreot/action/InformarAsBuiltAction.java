/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.pojos.ArchivosAsBuilt;
import com.skydream.coreot.pojos.NombresArchivosAsBuilt;
import com.skydream.coreot.util.MultipartUtility;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.jboss.logging.Logger;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Christian
 */
public class InformarAsBuiltAction extends ActionSupport {
    
    private int id;
    private String parametros;
    private String tipoAsBuilt;
    private String retorno;
    private List<File> archivo = new ArrayList<>();
    private List<String> archivoFileName = new ArrayList<>();
    private List<String> archivoContentType = new ArrayList<>();


    
    public String execute() throws Exception {

        String cadenaRetorno = "";
        String usuarioEnviado = "";

        List<ArchivosAsBuilt> listadoArchivos = new ArrayList<>();

        if (getArchivo() != null && getArchivo().size() > 0) {
            for (int i = 0; i < getArchivo().size(); i++) {
                System.out.println(getArchivo().get(i));
                ByteArrayOutputStream baos = leerArchivo(getArchivo().get(i));
                InputStream is = new ByteArrayInputStream(baos.toByteArray());
                ArchivosAsBuilt archivoAsBuilt = new ArchivosAsBuilt();
                archivoAsBuilt.setIs(is);
                NombresArchivosAsBuilt nombreCompleto = obtieneNombreArchivo(getArchivoFileName().get(i));
                archivoAsBuilt.setNombre(nombreCompleto);
                archivoAsBuilt.setExtension(retornarExtension(getArchivoFileName().get(i)));
                archivoAsBuilt.setCodigoImagen(retornarCodigoImagen(nombreCompleto));
                archivoAsBuilt.setArchivo(getArchivo().get(i));
                listadoArchivos.add(archivoAsBuilt);
            }
        }


        try {

            String urlServicio = "http://10.232.139.111/api_asbuilts/api/";
            String body = "";

            ObjectMapper mapper = new ObjectMapper();
            Map map = mapper.readValue(parametros, new TypeReference<HashMap<String, String>>() {
            });

            switch (tipoAsBuilt) {
                case "cercoAgricola":
                    urlServicio = urlServicio + "infra_cercoagricola/";
                    try {

                        String instAnclaje = map.get("inst_anclaje").toString();
                        String cant_vientosanc = map.get("cant_vientosanc").toString();
                        String inst_rollizos = map.get("inst_rollizos").toString();
                        String cant_rollizos = map.get("cant_rollizos").toString();
                        String inst_cerag = map.get("inst_cerag").toString();
                        String cant_cerag = map.get("cant_cerag").toString();
                        String inst_ceragvi = map.get("inst_ceragvi").toString();
                        String inst_tensasdovi = map.get("inst_tensasdovi").toString();
                        String usuariodig = map.get("usuariodig").toString();
                        usuarioEnviado = usuariodig;
                        body = "{\n" +
                                "  \"inst_anclaje\": "  + "\"" + instAnclaje + "\"" + ",\n" +
                                "  \"cant_vientosanc\": "  + "\"" + cant_vientosanc + "\"" + ",\n" +
                                "  \"inst_rollizos\": "  + "\"" + inst_rollizos + "\"" + ",\n" +
                                "  \"cant_rollizos\": "  + "\"" + cant_rollizos + "\"" + ",\n" +
                                "  \"inst_cerag\": "  + "\"" + inst_cerag + "\"" + ",\n" +
                                "  \"cant_cerag\": "  + "\"" + cant_cerag + "\"" + ",\n" +
                                "  \"inst_ceragvi\": "  + "\"" + inst_ceragvi + "\"" + ",\n" +
                                "  \"inst_tensasdovi\": "  + "\"" + inst_tensasdovi + "\"" + ",\n" +
                                "  \"usuariodig\": "  + "\"" + usuariodig + "\"" + "\n" +
                                "}";

                    }catch (Exception ex) {
                        cadenaRetorno = "error";
                        throw new Exception("Error en el body del request");
                    }
                    break;
                case "cercoPerimetral":
                    urlServicio = urlServicio + "infra_cercoperimetral/";
                    try {

                        String cerco_perimetral = map.get("cerco_perimetral").toString();
                        String tipo_cer_per = map.get("tipo_cer_per").toString();
                        String largo_cer_per = map.get("largo_cer_per").toString();
                        String ancho_cer_per = map.get("ancho_cer_per").toString();
                        String pilar_galvanizado = map.get("pilar_galvanizado").toString();
                        String cant_pi_gal = map.get("cant_pi_gal").toString();
                        String const_horm = map.get("const_horm").toString();
                        String largo_const_horm = map.get("largo_const_horm").toString();
                        String ancho_const_horm = map.get("ancho_const_horm").toString();
                        String porton_acceso = map.get("porton_acceso").toString();
                        String largo_porton_acc = map.get("largo_porton_acc").toString();
                        String ancho_porton_acc = map.get("ancho_porton_acc").toString();
                        String tipo_porton_acc = map.get("tipo_porton_acc").toString();
                        String cant_porton_acc = map.get("cant_porton_acc").toString();
                        String material_porton_acc = map.get("material_porton_acc").toString();
                        String candado_acc_cerper = map.get("candado_acc_cerper").toString();
                        String clave_acc_per = map.get("clave_acc_per").toString();
                        String usuariodig = map.get("usuariodig").toString();
                        usuarioEnviado = usuariodig;

                        body = "{\n" +
                                "  \"cerco_perimetral\": "  + "\"" + cerco_perimetral + "\"" + ",\n" +
                                "  \"tipo_cer_per\": "  + "\"" + tipo_cer_per + "\"" + ",\n" +
                                "  \"largo_cer_per\": "  + "\"" + largo_cer_per + "\"" + ",\n" +
                                "  \"ancho_cer_per\": "  + "\"" + ancho_cer_per + "\"" + ",\n" +
                                "  \"pilar_galvanizado\": "  + "\"" + pilar_galvanizado + "\"" + ",\n" +
                                "  \"cant_pi_gal\": "  + "\"" + cant_pi_gal + "\"" + ",\n" +
                                "  \"const_horm\": "  + "\"" + const_horm + "\"" + ",\n" +
                                "  \"largo_const_horm\": "  + "\"" + largo_const_horm + "\"" + ",\n" +
                                "  \"ancho_const_horm\": "  + "\"" + ancho_const_horm + "\"" + ",\n" +
                                "  \"porton_acceso\": "  + "\"" + porton_acceso + "\"" + ",\n" +
                                "  \"largo_porton_acc\": "  + "\"" + largo_porton_acc + "\"" + ",\n" +
                                "  \"ancho_porton_acc\": "  + "\"" + ancho_porton_acc + "\"" + ",\n" +
                                "  \"tipo_porton_acc\": "  + "\"" + tipo_porton_acc + "\"" + ",\n" +
                                "  \"cant_porton_acc\": "  + "\"" + cant_porton_acc + "\"" + ",\n" +
                                "  \"material_porton_acc\": "  + "\"" + material_porton_acc + "\"" + ",\n" +
                                "  \"candado_acc_cerper\": "  + "\"" + candado_acc_cerper + "\"" + ",\n" +
                                "  \"clave_acc_per\": "  + "\"" + clave_acc_per + "\"" + ",\n" +
                                "  \"usuariodig\": "  + "\"" + usuariodig + "\"" + "\n" +
                                "}";


                    }catch (Exception ex) {
                        cadenaRetorno = "error";
                        throw new Exception("Error en el body del request");
                    }
                    break;

                case "escalerilla":
                    urlServicio = urlServicio + "infra_escalerilla/";

                    try {

                        String inst_esc = map.get("inst_esc").toString();
                        String tipo_esc = map.get("tipo_esc").toString();
                        String largo_esc = map.get("largo_esc").toString();
                        String ancho_esc = map.get("ancho_esc").toString();
                        String usuariodig = map.get("usuariodig").toString();
                        usuarioEnviado = usuariodig;

                        body = "{\n" +
                                "  \"inst_esc\": "  + "\"" + inst_esc + "\"" + ",\n" +
                                "  \"tipo_esc\": "  + "\"" + tipo_esc + "\"" + ",\n" +
                                "  \"largo_esc\": "  + "\"" + largo_esc + "\"" + ",\n" +
                                "  \"ancho_esc\": "  + "\"" + ancho_esc + "\"" + ",\n" +
                                "  \"usuariodig\": "  + "\"" + usuariodig + "\"" + "\n" +
                                "}";

                    }catch (Exception ex) {
                        cadenaRetorno = "error";
                        throw new Exception("Error en el body del request");
                    }


                    break;
                case "torres":
                    urlServicio = urlServicio + "infra_torre/";

                    String tipo_estructura = map.get("tipo_estructura").toString();
                    String altura_torre = map.get("altura_torre").toString();
                    String cant_total_sop = map.get("cant_total_sop").toString();
                    String cant_disp_sop = map.get("cant_disp_sop").toString();
                    String cant_inst_sop = map.get("cant_inst_sop").toString();
                    String altura_inst_sop = map.get("altura_inst_sop").toString();
                    String pararrayo = map.get("pararrayo").toString();
                    String inst_baliza = map.get("inst_baliza").toString();
                    String inst_aterr_esa = map.get("inst_aterr_esa").toString();
                    String estructura_sop_ant = map.get("estructura_sop_ant").toString();
                    String req_lives = map.get("req_lives").toString();
                    String usuariodig = map.get("usuariodig").toString();
                    usuarioEnviado = usuariodig;

                    body = "{\n" +
                            "  \"tipo_estructura\": "  + "\"" + tipo_estructura + "\"" + ",\n" +
                            "  \"altura_torre\": "  + "\"" + altura_torre + "\"" + ",\n" +
                            "  \"cant_total_sop\": "  + "\"" + cant_total_sop + "\"" + ",\n" +
                            "  \"cant_disp_sop\": "  + "\"" + cant_disp_sop + "\"" + ",\n" +
                            "  \"cant_inst_sop\": "  + "\"" + cant_inst_sop + "\"" + ",\n" +
                            "  \"altura_inst_sop\": "  + "\"" + altura_inst_sop + "\"" + ",\n" +
                            "  \"pararrayo\": "  + "\"" + pararrayo + "\"" + ",\n" +
                            "  \"inst_baliza\": "  + "\"" + inst_baliza + "\"" + ",\n" +
                            "  \"inst_aterr_esa\": "  + "\"" + inst_aterr_esa + "\"" + ",\n" +
                            "  \"estructura_sop_ant\": "  + "\"" + estructura_sop_ant + "\"" + ",\n" +
                            "  \"req_lives\": "  + "\"" + req_lives + "\"" + ",\n" +
                            "  \"usuariodig\": "  + "\"" + usuariodig + "\"" + "\n" +
                            "}";
                    break;

                default:
                    throw new Exception("Tipo AsBuilt no encontrado");
            }


            urlServicio = urlServicio + id;

            URL url = new URL(urlServicio);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStream os = conn.getOutputStream();
            os.write(body.getBytes());
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            String idForm = "";
            while ((output = br.readLine()) != null) {
                System.out.println("RESPUESTA SERVICIO REGISTRAR: " + output);
                Map map2 = mapper.readValue(output, new TypeReference<HashMap<String, String>>() {
                });
                idForm = map2.get("id_form").toString();
            }

            conn.disconnect();

            //llamo servicio de imagenes
            for (ArchivosAsBuilt archivo: listadoArchivos){
                enviarImagen("11",id, archivo, usuarioEnviado,idForm);
            }

            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al agregar cerco agricola. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private String retornarCodigoImagen(NombresArchivosAsBuilt nombreCompleto) {

        switch (nombreCompleto.getTipoElementoConstruccion()){

            // Cerco Agricola
            case "anclajeVientos":
                return "01040009";
            case "tensadoVientos":
                return "01040011";
            case "cercoAV":
                return "01040010";

            // Escalerilla
            case "escalerilla":
                return "01030005";

            // Torres
            case "pararrayo":
                return "01020012";
            case "baliza":
                return "01020013";
            case "aterramiento":
                return "01020014";
            case "area":
                return "01020015";
            case "nivelPiso":
                return "01020016";
            case "torre":
                return "01020017";

            // Cerco Perimetral
            case "cercoPerimetral":
                return "01010018";
            case "hormigonado":
                return "01010020";
            case "pilares":
                return "01010019";
            case "porton":
                return "01010021";
            case "portonAcceso":
                return "01010022";

            default:
                return null;
        }

    }

    private NombresArchivosAsBuilt obtieneNombreArchivo(String nombreArchivo) {

        NombresArchivosAsBuilt nombreArchivoCompleto = new NombresArchivosAsBuilt();

        String[] nombreCompleto = nombreArchivo.split("--");
        nombreArchivoCompleto.setNombre(nombreCompleto[0]);

        String tipoElemento = nombreCompleto[1];
        tipoElemento = tipoElemento.replace(".jpg","");
        nombreArchivoCompleto.setTipoElementoConstruccion(tipoElemento);

        return nombreArchivoCompleto;
    }

    private ByteArrayOutputStream leerArchivo(File archivo) throws IOException, FileNotFoundException {
        ByteArrayOutputStream baos;
        InputStream is = new FileInputStream(archivo);
        try (BufferedInputStream bis = new BufferedInputStream(is)) {
            baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int readCount;
            while ((readCount = bis.read(buffer)) > 0) {
                baos.write(buffer, 0, readCount);
            }
        }
        return baos;
    }
    private static String retornarExtension(String nombreArhivo) {
        String fileName = nombreArhivo;
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    private static Boolean enviarImagen(String tipoForm, int codigoProyecto, ArchivosAsBuilt file, String usuarioEnviado, String idForm) throws IOException {
    String charset = "UTF-8";
    String urlServicio = "http://10.232.139.111/api_asbuilts/api/formimagenes/"+codigoProyecto;
    String body = "{\n" +
            "  \"id_proyecto\": "  + "\"" + codigoProyecto + "\"" + ",\n" +
            "  \"id_form\": "  + "\"" + idForm + "\"" + ",\n" +
            "  \"tipo_form\": "  + "\"" + tipoForm + "\"" + ",\n" +
            "  \"tipo_imagen\": "  + "\"" + file.getCodigoImagen() + "\"" + ",\n" +
            "  \"usuariodig\": "  + "\"" + usuarioEnviado + "\"" + "\n" +
            "}";

    try {
        MultipartUtility multipart = new MultipartUtility(urlServicio, charset);
        multipart.addFormField("json", body);
        multipart.addArchivo(file.getNombre().getNombre(), file.getArchivo());
        List<String> response = multipart.finish();
        System.out.println("SERVER REPLIED:");
        for (String line : response) {
            System.out.println(line);
        }
    } catch (IOException ex) {
        System.err.println(ex);
    }
        return true;
    }



    private static final Logger log = Logger.getLogger(InformarAsBuiltAction.class);



    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }

    public String getTipoAsBuilt() {
        return tipoAsBuilt;
    }

    public void setTipoAsBuilt(String tipoAsBuilt) {
        this.tipoAsBuilt = tipoAsBuilt;
    }

    public List<File> getArchivo() {
        return archivo;
    }

    public void setArchivo(List<File> archivo) {
        this.archivo = archivo;
    }

    public List<String> getArchivoFileName() {
        return archivoFileName;
    }

    public void setArchivoFileName(List<String> archivoFileName) {
        this.archivoFileName = archivoFileName;
    }

    public List<String> getArchivoContentType() {
        return archivoContentType;
    }

    public void setArchivoContentType(List<String> archivoContentType) {
        this.archivoContentType = archivoContentType;
    }


}
