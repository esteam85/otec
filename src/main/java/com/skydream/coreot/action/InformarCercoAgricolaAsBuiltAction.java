/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.jboss.logging.Logger;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Christian
 */
public class InformarCercoAgricolaAsBuiltAction extends ActionSupport {
    

    private int id;
    private String parametros;
    private String retorno;

    
    public String execute() throws Exception {

        String cadenaRetorno = "";
        try {

            String urlServicio = "http://10.232.139.111/api_asbuilts/api/infra_cercoagricola/";
            urlServicio = urlServicio + id;

            URL url = new URL(urlServicio);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            ObjectMapper mapper = new ObjectMapper();

            Map map = mapper.readValue(parametros, new TypeReference<HashMap<String, String>>() {
            });

            String instAnclaje = "";
            String cant_vientosanc = "";
            String inst_rollizos = "";
            String cant_rollizos = "";
            String inst_cerag = "";
            String cant_cerag = "";
            String inst_ceragvi = "";
            String inst_tensasdovi = "";
            String usuariodig = "";

            try {

                instAnclaje = map.get("inst_anclaje").toString();
                cant_vientosanc = map.get("cant_vientosanc").toString();
                inst_rollizos = map.get("inst_rollizos").toString();
                cant_rollizos = map.get("cant_rollizos").toString();
                inst_cerag = map.get("inst_cerag").toString();
                cant_cerag = map.get("cant_cerag").toString();
                inst_ceragvi = map.get("inst_ceragvi").toString();
                inst_tensasdovi = map.get("inst_tensasdovi").toString();
                usuariodig = map.get("usuariodig").toString();

            }catch (Exception ex) {
                cadenaRetorno = "error";
                throw new Exception("Error en el body del request");
            }

            String body = "{\n" +
                    "  \"instAnclaje\": "  + "\"" + instAnclaje + "\"" + ",\n" +
                    "  \"cant_vientosanc\": "  + "\"" + cant_vientosanc + "\"" + ",\n" +
                    "  \"inst_rollizos\": "  + "\"" + inst_rollizos + "\"" + ",\n" +
                    "  \"cant_rollizos\": "  + "\"" + cant_rollizos + "\"" + ",\n" +
                    "  \"inst_cerag\": "  + "\"" + inst_cerag + "\"" + ",\n" +
                    "  \"cant_cerag\": "  + "\"" + cant_cerag + "\"" + ",\n" +
                    "  \"inst_ceragvi\": "  + "\"" + inst_ceragvi + "\"" + ",\n" +
                    "  \"inst_tensasdovi\": "  + "\"" + inst_tensasdovi + "\"" + ",\n" +
                    "  \"usuariodig\": "  + "\"" + usuariodig + "\"" + "\n" +
                    "}";

            OutputStream os = conn.getOutputStream();
            os.write(body.getBytes());
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }

            conn.disconnect();


            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al agregar cerco agricola. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(InformarCercoAgricolaAsBuiltAction.class);



    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }
}
