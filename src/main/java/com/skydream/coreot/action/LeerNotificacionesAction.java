package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import org.apache.struts2.ServletActionContext;
import org.jboss.logging.Logger;

import java.io.IOException;

/**
 * Created by christian on 26-10-15.
 */
public class LeerNotificacionesAction extends ActionSupport {

    private int idNotificacion;
    private int usuarioId;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),false);
        String cadenaRetorno = "";
        try {
            GenericDAO.getINSTANCE().leerNotificacion(getIdNotificacion());

            cadenaRetorno = "success";
        } catch (NumberFormatException | IOException ex) {
            log.error("Error al obtener notificaciones. ", ex);
            throw ex;
        }


        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(LeerNotificacionesAction.class);


    public int getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(int idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
