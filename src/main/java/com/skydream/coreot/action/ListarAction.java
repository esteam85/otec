/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author mcj
 */
public class ListarAction extends ActionSupport {
    
    private String objCoreOT;
    private String retorno;
    private int usuarioId;

    public String execute() throws Exception{
        String cadenaRetorno = "";
        Map session = ActionContext.getContext().getSession();

        try {
            LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), usuarioId,true);
            CoreOtFactory  coreOtFactory = new CoreOtFactory();
            ObjetoCoreOT objFactory = coreOtFactory.getObjetoCoreOt(this.getObjCoreOT());
            List listaRetorno = objFactory.obtenerGatewayDAO().listar(objFactory, session);
            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(listaRetorno));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al listar Objeto. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    /**
     * @return the objCoreOT
     */
    public String getObjCoreOT() {
        return objCoreOT;
    }

    /**
     * @param objCoreOT the objCoreOT to set
     */
    public void setObjCoreOT(String objCoreOT) {
        this.objCoreOT = objCoreOT;
    }



    private static final Logger log = Logger.getLogger(ListarAction.class);

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}