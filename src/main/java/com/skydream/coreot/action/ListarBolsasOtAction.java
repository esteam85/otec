/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.pojos.BolsaGestionEconomica;
import com.skydream.coreot.pojos.DetalleBolsaGestionEconomica;
import org.apache.struts2.components.Else;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mcj
 */
public class ListarBolsasOtAction extends ActionSupport {

    private String retorno;
    private int usuarioId;
    private int rolId;
    private int idCicloFacturacion;
    private String estado;


    public String execute() throws Exception {
//        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"),usuarioId,true);
        String cadenaRetorno = "";

        BolsaGestionEconomica bolsaGestionEconomica = new BolsaGestionEconomica();

        try {

            if(getRolId() == 6){  // pagos tiene logica de json
                retorno = GenericDAO.getINSTANCE().obtenerListaBolsasOtPagos(getUsuarioId(),estado);
            }else{
                List<DetalleBolsaGestionEconomica> listaBolsas = null;
                if(estado.compareTo("pagadas") == 0){
                    listaBolsas = GenericDAO.getINSTANCE().obtenerListaBolsasOtPagadas(getUsuarioId(), getRolId());
                }else{
                    listaBolsas = GenericDAO.getINSTANCE().obtenerListaBolsasOt(getUsuarioId(), getRolId(), getIdCicloFacturacion(),estado);
                }

                bolsaGestionEconomica.setListado(listaBolsas);
                bolsaGestionEconomica.setTotal(listaBolsas.size());

                ObjectMapper objMap = new ObjectMapper();
                objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
                ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
                retorno = ow.writeValueAsString(bolsaGestionEconomica);
            }

            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al listar Cubicaciones. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(ListarOtAction.class);

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }


    public int getRolId() {
        return rolId;
    }

    public void setRolId(int rolId) {
        this.rolId = rolId;
    }

    public int getIdCicloFacturacion() {
        return idCicloFacturacion;
    }

    public void setIdCicloFacturacion(int idCicloFacturacion) {
        this.idCicloFacturacion = idCicloFacturacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
