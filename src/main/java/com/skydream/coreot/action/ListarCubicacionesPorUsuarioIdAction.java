/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.CubicadorDAO;
import java.util.List;
import java.util.Map;

import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.RetornoListadoCubicacionAux;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

/**
 *
 * @author mcj
 */
public class ListarCubicacionesPorUsuarioIdAction extends ActionSupport {
    
    private int usuarioId;
    private String retorno;
    private String orderBy;
    private int pos;
    private int cantidad;
    
    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), usuarioId,true);
        String cadenaRetorno = "";
        Map session = ActionContext.getContext().getSession();
        RetornoListadoCubicacionAux retornoListadoCubicacionAux = new RetornoListadoCubicacionAux();
        try {

            String orden = orderBy.substring(0,1);
            int largo = orderBy.length();
            String filtro = orderBy.substring(1,largo);

            List listaRetorno = CubicadorDAO.getINSTANCE().listarCubicacionesPorUsuarioIdPaginado(usuarioId, getPos(), getCantidad(), filtro, orden);
            retornoListadoCubicacionAux.setListado(listaRetorno);
            int totalRegistros = CubicadorDAO.getINSTANCE().obtenerTotalCubicaciones(usuarioId);
            retornoListadoCubicacionAux.setTotal(totalRegistros);
            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(retornoListadoCubicacionAux));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al listar Cubicaciones. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
        

    private static final Logger log = Logger.getLogger(ListarCubicacionesPorUsuarioIdAction.class);


    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }


    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
}
