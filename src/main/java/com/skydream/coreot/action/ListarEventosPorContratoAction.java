/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.Contratos;
import com.skydream.coreot.pojos.Workflow;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Erbi
 */
public class ListarEventosPorContratoAction extends ActionSupport {
    
    private int contratoId;
    private String retorno;
    private int usuarioId;
    
    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";
        try {
            List<Contratos> listaContrato = GenericDAO.getINSTANCE().obtenerContratoPorId(getContratoId());
            List<Contratos> listaContrato2 = GenericDAO.getINSTANCE().obtenerContratoPorId(1);
            List<Workflow> listaWorkflow = new ArrayList<Workflow>();
            for(Contratos cont : listaContrato){
                int id = cont.getWorkflow().getId();
                listaWorkflow = GenericDAO.getINSTANCE().obtenerWorkflowPorId(id);
            }
            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(listaWorkflow));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al listar Roles. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }


        
        
    private static final Logger log = Logger.getLogger(ListarEventosPorContratoAction.class);



    public int getContratoId() {
        return contratoId;
    }

    public void setContratoId(int contratoId) {
        this.contratoId = contratoId;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
