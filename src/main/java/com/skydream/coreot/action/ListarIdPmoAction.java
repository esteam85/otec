/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.OtDAO;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.util.List;
import java.util.Map;

/**
 *
 * @author mcj
 */
public class ListarIdPmoAction extends ActionSupport {

    private String retorno;
    private int usuarioId;
    private int contratoId;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"),usuarioId,true);
        String cadenaRetorno = "";

        try {

            GenericDAO dao = new GenericDAO();
            List<Integer> listadoIdPmo = dao.listarIdPmo(contratoId, usuarioId);
            String jsonAux = "";
            int contIdPmo = 0;
            for(Integer idPmo : listadoIdPmo){
                if(contIdPmo != 0){
                    jsonAux = jsonAux + ",";
                }
                jsonAux = jsonAux + "{ \"id\": "  +  idPmo  + "}";
                contIdPmo ++;
            }
//            jsonAux = jsonAux + "]";
            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            retorno = "[" + jsonAux + "]";
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al listar Cubicaciones. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(ListarIdPmoAction.class);

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getContratoId() {
        return contratoId;
    }

    public void setContratoId(int contratoId) {
        this.contratoId = contratoId;
    }
}
