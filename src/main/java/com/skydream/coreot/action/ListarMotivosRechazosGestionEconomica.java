package com.skydream.coreot.action;

import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.pojos.MotivosRechazosGestionEconomica;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mcj on 03-06-16.
 */
public class ListarMotivosRechazosGestionEconomica {

    private int usuarioId;
    private String codigoAreaGestionEconomica;
    private String retorno;


    public String execute() throws Exception {
//        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";
        try {
            GenericDAO dao = new GenericDAO();
            Map<String,Object> filtro = new HashMap<>();
            filtro.put("codigoAreaRechazoGe", this.codigoAreaGestionEconomica);
            List<MotivosRechazosGestionEconomica> listadoMotivosRechazos = dao.listarGenerico(MotivosRechazosGestionEconomica.class,filtro,null);

            if (listadoMotivosRechazos==null && codigoAreaGestionEconomica.equals("PMO")){
                MotivosRechazosGestionEconomica mrge1 = new MotivosRechazosGestionEconomica(1,"LP sin fondos", "LP no posee fondos suficientes para cancelar la presente acta","PMO");
                MotivosRechazosGestionEconomica mrge2 = new MotivosRechazosGestionEconomica(1,"IDPMO mal asignado", "Acta no corresponde al idPmo asignado","PMO");
                listadoMotivosRechazos.add(mrge1);
                listadoMotivosRechazos.add(mrge2);
            }
            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(listadoMotivosRechazos));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al listar motivos de rechazos de gestion economica. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(ListarMotivosRechazosGestionEconomica.class);

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public String getCodigoAreaGestionEconomica() {
        return codigoAreaGestionEconomica;
    }

    public void setCodigoAreaGestionEconomica(String codigoAreaGestionEconomica) {
        this.codigoAreaGestionEconomica = codigoAreaGestionEconomica;
    }
}
