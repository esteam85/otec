/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.OtDAO;

import java.net.URLDecoder;
import java.util.*;

import com.skydream.coreot.pojos.OtAux;
import com.skydream.coreot.pojos.RetornoListadoOtAux;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.util.AttributeMap;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.type.TypeReference;
import org.jboss.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author mcj
 */
public class ListarOtAction extends ActionSupport {


    private int usuarioId;
    private String estado;
    private String orderBy;
    private RetornoListadoOtAux retorno2;
    private String retorno;
    private int pos;
    private int cantidad;
    private String filter;

    public String execute() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        String token = request.getParameter("tk");
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(token, getUsuarioId(),true);
        GenericDAO.getINSTANCE().registrarJournal(token, usuarioId, request.getServletPath(),
                URLDecoder.decode(request.getQueryString(), "UTF-8"), new Date());
        String cadenaRetorno = "";


        try {

            // leo los filtros que vienen
            ObjectMapper mapper = new ObjectMapper();
            Map mapaParams = null;
            if(filter != null){
                mapaParams = mapper.readValue(filter, new TypeReference<HashMap<String,Object>>() {
                });
            }

            // abierta cerrada play
            List<OtAux> listaRetorno = null;
            setRetorno2(new RetornoListadoOtAux());
            int total = 0;

            String orden = orderBy.substring(0,1);
            int largo = orderBy.length();
            String filtro = orderBy.substring(1,largo);

            switch (getEstado()){
                case "abierta":
                    listaRetorno = OtDAO.getINSTANCE().listarOtPorEstado(usuarioId, pos, cantidad, estado, filtro, orden, mapaParams);
                    total = OtDAO.getINSTANCE().obtenerTotalOtPorEstado(usuarioId,estado, mapaParams);
                    getRetorno2().setTotal(total);
                    break;
                case "cerrada":
                    listaRetorno = OtDAO.getINSTANCE().listarOtPorEstado(usuarioId, pos, cantidad, estado, filtro, orden, mapaParams);
                    total = OtDAO.getINSTANCE().obtenerTotalOtPorEstado(usuarioId,estado, mapaParams);
                    getRetorno2().setTotal(total);
                    break;
                case "tramite":
                    listaRetorno = OtDAO.getINSTANCE().listarOtPorEstado(usuarioId, pos, cantidad, estado, filtro, orden, mapaParams);
                    total = OtDAO.getINSTANCE().obtenerTotalOtPorEstado(usuarioId,estado, mapaParams);
                    getRetorno2().setTotal(total);
                    break;
                case "play":
                    listaRetorno = OtDAO.getINSTANCE().listarOtPorEstado(usuarioId, pos, cantidad, estado, filtro, orden, mapaParams);
                    total = OtDAO.getINSTANCE().obtenerTotalOtPorEstado(usuarioId,estado, mapaParams);
                    getRetorno2().setTotal(total);
                    break;
                default:
                    throw new Exception("Estado no encontrado");
            }

            getRetorno2().setListado(listaRetorno);

//            List listaRetorno = OtDAO.getINSTANCE().listarOtEjecucionPorUsuarioId(Integer.parseInt(usuarioId));
            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            retorno = ow.writeValueAsString(retorno2);
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al listar Ot's. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    private static final Logger log = Logger.getLogger(ListarOtAction.class);


    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }



    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public RetornoListadoOtAux getRetorno2() {
        return retorno2;
    }

    public void setRetorno2(RetornoListadoOtAux retorno2) {
        this.retorno2 = retorno2;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }
}
