/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.OtDAO;
import com.skydream.coreot.pojos.OtAux;
import com.skydream.coreot.pojos.RetornoListadoOtAux;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;

/**
 *
 * @author mcj
 */
public class ListarOtPorIdAction extends ActionSupport {


    private int usuarioId;
    private String otIdoNombre;
    private int opt;
    private String estado;
    private RetornoListadoOtAux retorno2;
    private String retorno;

    public String execute() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        String token = request.getParameter("tk");
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(token, getUsuarioId(),true);
        GenericDAO.getINSTANCE().registrarJournal(token, usuarioId, request.getServletPath(),
                URLDecoder.decode(request.getQueryString(), "UTF-8"), new Date());
        String cadenaRetorno = "";


        try {

            List<OtAux> listaRetorno = null;
            setRetorno2(new RetornoListadoOtAux());
            int total = 0;

            if(!getOtIdoNombre().isEmpty()){
                if(opt==0){
                    listaRetorno = OtDAO.getINSTANCE().listarOtPorId(usuarioId, Integer.parseInt(getOtIdoNombre()), getEstado());
                }else if(opt==1){
                    listaRetorno = OtDAO.getINSTANCE().listarOtPorNombre(usuarioId, getOtIdoNombre(), getEstado());
                }else throw new Exception("Parametro opt no valido.");

                total = listaRetorno.size();
                getRetorno2().setTotal(total);
            }else throw new Exception("Parametro otIdoNombre vacio.");

            getRetorno2().setListado(listaRetorno);

            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            retorno = ow.writeValueAsString(retorno2);
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al listar Cubicaciones. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(ListarOtPorIdAction.class);

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getOtIdoNombre() {
        return otIdoNombre;
    }

    public void setOtIdoNombre(String otId) {
        this.otIdoNombre = otId;
    }

    public int getOpt(){
        return opt;
    }

    public void setOpt(int opt){
        this.opt = opt;
    }

    public String getEstado() { return estado;}

    public void setEstado(String estado) { this.estado = estado;}

    public RetornoListadoOtAux getRetorno2() {
        return retorno2;
    }

    public void setRetorno2(RetornoListadoOtAux retorno2) {
        this.retorno2 = retorno2;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }


}
