/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.GestionEconomica;
import com.skydream.coreot.pojos.GestionEconomicaAux;
import com.skydream.coreot.pojos.LineasPresupuestarias;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mcj
 */
public class ListarPep2PorLpAction extends ActionSupport {

    private int lp;
    private int idPmo;
    private String retorno;
    private int usuarioId;
    private int contratoId;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";
        List<GestionEconomicaAux> listadoFinalLp = new ArrayList<>();
        try {
            GenericDAO dao = new GenericDAO();
            List<GestionEconomica> listadoLp = dao.listarPep2PorLp(idPmo,lp, contratoId);

            for(GestionEconomica gestionEconomica: listadoLp){
                GestionEconomicaAux gestionEconomicaAux = new GestionEconomicaAux();
                gestionEconomicaAux.setIdPmo(gestionEconomica.getId().getIdPmo());
                gestionEconomicaAux.setLp(gestionEconomica.getId().getLp());
                gestionEconomicaAux.setPep1(gestionEconomica.getId().getPep1());
                gestionEconomicaAux.setPep2(gestionEconomica.getId().getPep2());
                listadoFinalLp.add(gestionEconomicaAux);
            }

            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(listadoFinalLp));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al listar Cubicaciones. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(ListarPep2PorLpAction.class);

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getContratoId() {
        return contratoId;
    }

    public void setContratoId(int contratoId) {
        this.contratoId = contratoId;
    }

    public int getLp() {
        return lp;
    }

    public void setLp(int lp) {
        this.lp = lp;
    }

    public int getIdPmo() {
        return idPmo;
    }

    public void setIdPmo(int idPmo) {
        this.idPmo = idPmo;
    }
}
