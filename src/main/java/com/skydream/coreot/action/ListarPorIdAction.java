/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.dao.GatewayDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

/**
 *
 * @author mcj
 */
public class ListarPorIdAction extends ActionSupport {
    
    private String objCoreOT;
    private String atributoObj;
    private String id;
    private String retorno;
    private int usuarioId;
    
    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";

        try {
            CoreOtFactory  coreOtFactory = new CoreOtFactory();
            ObjetoCoreOT objetoCoreOT = coreOtFactory.getObjetoCoreOt(this.objCoreOT);
            GatewayDAO gatewayDAO = objetoCoreOT.obtenerGatewayDAO();
            List<ObjetoCoreOT> listado = 
                    gatewayDAO.listarPorClaveForanea(objetoCoreOT, this.atributoObj + ".id", Integer.parseInt(id));
            ObjectMapper objMap = new ObjectMapper();
//            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(listado));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            System.out.println(ex);
            throw new Exception("No se pudo obtener el listado de proveedores.");
        }
        return cadenaRetorno;
    }

    /**
     * @return the objCoreOT
     */
    public String getObjCoreOT() {
        return objCoreOT;
    }

    /**
     * @param objCoreOT the objCoreOT to set
     */
    public void setObjCoreOT(String objCoreOT) {
        this.objCoreOT = objCoreOT;
    }

    /**
     * @return the atributoObj
     */
    public String getAtributoObj() {
        return atributoObj;
    }

    /**
     * @param atributoObj the atributoObj to set
     */
    public void setAtributoObj(String atributoObj) {
        this.atributoObj = atributoObj;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }


    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
