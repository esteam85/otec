package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.ProveedorServiciosDAO;
import com.skydream.coreot.pojos.Login;
import com.skydream.coreot.pojos.Proveedores;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mcj on 16-11-15.
 */
public class ListarProveedoresPorAgenciaAction extends ActionSupport {


    private int agenciaId;
    private int usuarioId;
    private String retorno;



    public String execute() throws Exception{
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            String token = request.getParameter("tk");
            Login login = LoginLogoutDAO.getINSTANCE().retornarLoginSesionActual(token);
            List<Proveedores> proveedores = ProveedorServiciosDAO.getInstance().listarProveedoresPorAgencia(agenciaId);
            ObjectMapper objMap = new ObjectMapper();
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(proveedores));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            System.out.println(ex);
            throw new Exception("No se pudo registrar el objeto");
        }
        return cadenaRetorno;
    }


    public int getAgenciaId() {
        return agenciaId;
    }

    public void setAgenciaId(int agenciaId) {
        this.agenciaId = agenciaId;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }
}
