package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.Proveedores;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.io.IOException;
import java.util.List;

/**
 * Created by mcj on 01-02-16.
 */
public class ListarProveedoresPorContratoAction extends ActionSupport {

    private int usuarioId;
    private int contratoId;
    private String retorno;

    public String execute() throws Exception{
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";
        try {
            List<Proveedores> lista = GenericDAO.getINSTANCE().listarProveedoresPorContrato(contratoId);
            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(lista));
            cadenaRetorno = "success";
        } catch (NumberFormatException | IOException ex) {
            log.error("Error al obtener regiones por contrato. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }


    private static final Logger log = Logger.getLogger(ListarProveedoresPorRegionAction.class);

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getContratoId() {
        return contratoId;
    }

    public void setContratoId(int contratoId) {
        this.contratoId = contratoId;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

}
