package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.ProveedorServiciosDAO;
import com.skydream.coreot.pojos.Login;
import com.skydream.coreot.pojos.ServiciosAux;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by mcj on 16-11-15.
 */
public class ListarServiciosExtrasPorOtAction extends ActionSupport {

    private int otId;
    private int tipoServicioId;
    private int usuarioId;
    private String retorno;

    public String execute() throws Exception{
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"),usuarioId,true);
        String cadenaRetorno = "";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            String token = request.getParameter("tk");
            Login login = LoginLogoutDAO.getINSTANCE().retornarLoginSesionActual(token);
            List<ServiciosAux> servicios = ProveedorServiciosDAO.getInstance().listarServiciosExtrasPorOt(otId, tipoServicioId, login);

            ObjectMapper objMap = new ObjectMapper();
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(servicios));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            System.out.println(ex);
            throw new Exception("No se pudo registrar el objeto");
        }
        return cadenaRetorno;
    }



    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getTipoServicioId() {
        return tipoServicioId;
    }

    public void setTipoServicioId(int tipoServicioId) {
        this.tipoServicioId = tipoServicioId;
    }

    public int getOtId() {
        return otId;
    }

    public void setOtId(int otId) {
        this.otId = otId;
    }


    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
