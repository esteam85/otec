package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.ProveedorServiciosDAO;
import com.skydream.coreot.pojos.ServiciosAux;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import java.util.List;

/**
 * Created by mcj on 16-11-15.
 */
public class ListarServiciosPorContratoRegionProveedorAction extends ActionSupport {

    private int contratoId;
    private int regionId;
    private int proveedorId;
    private int tipoServicioId;
    private String retorno;
    private int usuarioId;


    public String execute() throws Exception{
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";

        try {
            List<ServiciosAux> servicios = ProveedorServiciosDAO.getInstance().listarServiciosPorProveedorRegionContrato2(getContratoId(), getRegionId(), getProveedorId(), getTipoServicioId());
            ObjectMapper objMap = new ObjectMapper();
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(servicios));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            System.out.println(ex);
            throw new Exception("No se pudo registrar el objeto");
        }
        return cadenaRetorno;
    }


    public int getContratoId() {
        return contratoId;
    }

    public void setContratoId(int contratoId) {
        this.contratoId = contratoId;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public int getProveedorId() {
        return proveedorId;
    }

    public void setProveedorId(int proveedorId) {
        this.proveedorId = proveedorId;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getTipoServicioId() {
        return tipoServicioId;
    }

    public void setTipoServicioId(int tipoServicioId) {
        this.tipoServicioId = tipoServicioId;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
