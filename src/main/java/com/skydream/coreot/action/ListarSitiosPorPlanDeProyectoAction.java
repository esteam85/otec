/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.Sitios;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mcj
 */
public class ListarSitiosPorPlanDeProyectoAction extends ActionSupport {

    private int planDeProyectoId;
    private String retorno;
    private int usuarioId;

    
    public String execute() throws Exception{
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"),usuarioId,true);
        String cadenaRetorno = "";
        List<Sitios> listaFinal = new ArrayList<>();
        try {
            List<Sitios> lista = GenericDAO.getINSTANCE().obtenerListadoDeSitiosPorPlanDeProyecto(planDeProyectoId);
            for(Sitios pro : lista){
                Sitios sitioFinal = new Sitios();
                sitioFinal.setId(pro.getId());
                sitioFinal.setNombre(pro.getNombre());
                sitioFinal.setDescripcion(pro.getDescripcion());
                sitioFinal.setCodigo(pro.getCodigo());
                sitioFinal.setLatitud(pro.getLatitud());
                sitioFinal.setLongitud(pro.getLongitud());
                sitioFinal.setDireccion(pro.getDireccion());

                listaFinal.add(sitioFinal);
            }
            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(listaFinal));
            cadenaRetorno = "success";
        } catch (NumberFormatException | IOException ex) {
            log.error("Error al obtener regiones por contrato. ", ex);
            throw ex;
        }


        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(ListarSitiosPorPlanDeProyectoAction.class);


    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }


    public int getPlanDeProyectoId() {
        return planDeProyectoId;
    }

    public void setPlanDeProyectoId(int planDeProyectoId) {
        this.planDeProyectoId = planDeProyectoId;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
