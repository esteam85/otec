/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.OtDAO;
import com.skydream.coreot.dao.UsuarioDAO;
import com.skydream.coreot.pojos.*;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.util.*;

/**
 *
 * @author mcj
 */
public class ListarTrabajadorOtAction extends ActionSupport {

    private String otId;
    private String eventoId;
    private String retorno;
    private int usuarioId;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";
        List<Usuarios> listadoUsuarios = new ArrayList<>();
        String usuarios = "[";
        Map parametros = new HashMap();
        Map mapAccion = new HashMap();
        try {
            mapAccion.put("codigo", "AC-AS-TR");
            Ot ot = OtDAO.getINSTANCE().obtenerDetalleOt(Integer.parseInt(getOtId()));
            int proveedorId = ot.getProveedor().getId();
            // el rol 4 es de trabajador de empresa colaboradora
            listadoUsuarios = UsuarioDAO.getINSTANCE().obtenerUsuariosSegunProveedorId(proveedorId, 4);
            usuarios = usuarios + armarJsonRespuestaUsuario(listadoUsuarios);
            parametros.put("asignar",usuarios);

            String boton = "";
            boton = boton + "{\n" +
                    "  \"id\": "  + "\"" + "" + "\"" + ",\n" +
                    "  \"opcion\": "  + "\"" + "" + "\"" + ",\n" +
                    "  \"eventoId\": "  + 3 + ",\n" +
                    "  \"tituloBoton\": "  + "\"" +  "Asignar" + "\"" + "\n" +
                    "}";

            String modal = "{\n" +
                    "  \"tituloModal\": \"Asignar Trabajador\",\n" +
                    "  \"modalSize\": \"lg\",\n" +
                    "  \"servicio\": \"editarTrabajadorOt\",\n" +
                    "  \"botones\": [\n" +  boton  + "  ]\n" +
                    "}";

            setRetorno(usuarios);

            parametros.put("modal",modal);

            mapAccion.put("parametros", parametros);

            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(mapAccion));

            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al listar Cubicaciones. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(ListarTrabajadorOtAction.class);

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public String getOtId() {
        return otId;
    }

    public void setOtId(String otId) {
        this.otId = otId;
    }

    public String getEventoId() {
        return eventoId;
    }

    public void setEventoId(String eventoId) {
        this.eventoId = eventoId;
    }

    private String armarJsonRespuestaUsuario(List<Usuarios> listadoUsuarios) {
        String usuarios = "";

        int contUsuarios = 0;
        for(Usuarios usuarioAux : listadoUsuarios){
            if(contUsuarios != 0){
                usuarios = usuarios + ",";
            }
            usuarios = usuarios + "{\n" +
                    "  \"id\": "  + "\"" + usuarioAux.getId() + "\"" + ",\n" +
                    "  \"nombre\": "  + "\"" + usuarioAux.getNombres() +" "+ usuarioAux.getApellidos() + "\"" + "\n" +
                    "}";
            contUsuarios++;
        }
        usuarios = usuarios + "]";

        return usuarios;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
