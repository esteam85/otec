/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.Roles;
import com.skydream.coreot.pojos.Usuarios;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Erbi
 */
public class ListarUsuariosPorRoleAction extends ActionSupport {
    
    private int rolId;
    private String retorno;
    private int usuarioId;
    
    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";
        Map session = ActionContext.getContext().getSession();

        try {
            List<Roles> listaPerfil = GenericDAO.getINSTANCE().obtenerRolPorId(rolId);
            int idPerfil = listaPerfil.get(0).getPerfil().getId();

            List<Roles> listaRetorno = GenericDAO.getINSTANCE().listarRolesPorPerfil(idPerfil);
            List<Usuarios> listaUsuarios = new ArrayList<Usuarios>();
            for(Roles rol: listaRetorno){
                if(rol.getId() == rolId) {
                    Set<Usuarios> user = rol.getUsuariosRoles();
                    for(Usuarios usuarioAux : user){
                        listaUsuarios.add(usuarioAux);
                    }
                }
            }

            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(listaUsuarios));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al listar Roles. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(ListarUsuariosPorRoleAction.class);


    public int getRolId() {
        return rolId;
    }

    public void setRolId(int rolId) {
        this.rolId = rolId;
    }


    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}



