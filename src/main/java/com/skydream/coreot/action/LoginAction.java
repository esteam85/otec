/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.Journal;
import com.skydream.coreot.pojos.Usuarios;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.type.TypeReference;
import sun.misc.Request;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mcj
 */
public class LoginAction extends ActionSupport implements ServletResponseAware, ServletRequestAware {
    
    private String usuario;
    private String clave;
    private boolean ingresoOK;
    private String datosUsuario;
    private String rol;
    private String tk;
    private boolean tokenApp;

    public LoginAction(){}
    
    public String execute() throws Exception{
        String cadenaRetorno = "";
        try {
            Usuarios usr = LoginLogoutDAO.validarLoginYRetornarUsuario(usuario, clave);
            setIngresoOK(usr != null);
            Cookie cookie = new Cookie("ck","nn");
            cookie.setHttpOnly(true);
            servletResponse.addCookie(cookie);
            if (isIngresoOK() && rol==null) {
                setearDatosUsuario(usr);
                cadenaRetorno = "success";
            }else if(isIngresoOK() && rol!=null){
                ObjectMapper mapper = new ObjectMapper();
                Map mapaObjRol = mapper.readValue(rol, new TypeReference<HashMap<String, Object>>() {
                });
                setearDatosSession(usr, (Integer) mapaObjRol.get("id"), tokenApp);

                HttpServletRequest request = ServletActionContext.getRequest();
                GenericDAO.getINSTANCE().registrarJournal(tk,usr.getId(), request.getServletPath(),
                        URLDecoder.decode(request.getQueryString(), "UTF-8"),new Date());
                cadenaRetorno = "success";
            } else {
                usr = null;
                cadenaRetorno = "error";
            }

        } catch (Exception ex) {
            log.error("Error al logear Usuario. ", ex);
            throw new Exception("Error al validar credenciales de usuario.");
        }
        return cadenaRetorno;
    }

    private void setearDatosUsuario(Usuarios usr) throws IOException {
        ObjectMapper objMap = new ObjectMapper();
        objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
        setDatosUsuario(ow.writeValueAsString(usr));
    }

    private void setearDatosSession(Usuarios usuario, int rolId, boolean tokenApp) throws Exception{
        Map session = ActionContext.getContext().getSession();
        session.clear();
//        String token = LoginLogoutDAO.retornarTokenSesionUsuario(usuario, rolId);
//        session.put("TKNSTEAM", token);
//        Cookie cookie = new Cookie("ID",LoginLogoutDAO.retornarTokenSesionUsuario(usuario, rolId));
//        servletResponse.addCookie(cookie);
        setTk(LoginLogoutDAO.retornarTokenSesionUsuario(usuario, rolId, tokenApp));
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDatosUsuario() {
        return datosUsuario;
    }

    public void setDatosUsuario(String datosUsuario) {
        this.datosUsuario = datosUsuario;
    }

    public boolean isIngresoOK() {
        return ingresoOK;
    }

    public void setIngresoOK(boolean ingresoOK) {
        this.ingresoOK = ingresoOK;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getTk() {
        return tk;
    }

    public void setTk(String tk) {
        this.tk = tk;
    }

    private static final Logger log = Logger.getLogger(LoginAction.class);

    @Override
    public void setServletRequest(HttpServletRequest servletRequest) {
        this.servletRequest = servletRequest;
    }

    @Override
    public void setServletResponse(HttpServletResponse servletResponse) {
        this.servletResponse = servletResponse;
    }

    protected HttpServletRequest servletRequest;
    protected HttpServletResponse servletResponse;


    public boolean isTokenApp() {
        return tokenApp;
    }

    public void setTokenApp(boolean tokenApp) {
        this.tokenApp = tokenApp;
    }
}
