/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.*;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.skydream.coreot.pojos.*;

import java.util.TreeSet;

import org.apache.catalina.Role;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.type.TypeReference;
import org.jboss.logging.Logger;

import javax.servlet.http.HttpServletRequest;


/**
 *
 * @author mcj
 */
public class ObtenerAccionesFrontendPorEventoAction extends ActionSupport {

    private String eventoId;
    private String otId;
    private String otIdAux;
    private String otIdAux2;
    private int contratoId;
    private int usuarioId;
    private String proveedorId;
    private String retorno;
    private int cubicadorId;
    private String parametrosEvento;

    public String execute() throws Exception {
        String cadenaRetorno = "";
        try {
            LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"),usuarioId,true);
            ObtenerAccionesFrontEndAux respuesta = null;
            if (otId != null) {
                respuesta = EventoDAO.getINSTANCE().listarAccionesFrontendPorOt(Integer.parseInt(otId));
            } else {
                respuesta = EventoDAO.getINSTANCE().listarAccionesFrontendPorEvento(Integer.parseInt(getEventoId()));
            }
            eventoId = String.valueOf(respuesta.getIdEvento());
            if (otId != null) {
                verificaUsuarioEvento(Integer.parseInt(eventoId), usuarioId, Integer.parseInt(otId));
            }
            List listaRetorno = prepararDataParaFrontEnd(respuesta.getEventosAcciones());
            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
//            objMap.configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(listaRetorno));
            cadenaRetorno = "success";
        } catch (NumberFormatException | IOException ex) {
            log.error("Error al listar Acciones. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private List prepararDataParaFrontEnd(TreeSet<EventosAcciones> listaAcciones) throws Exception{

        List<Map> retorno = new ArrayList();
        final String token = ServletActionContext.getRequest().getParameter("tk");
        Login login = LoginLogoutDAO.getINSTANCE().retornarLoginSesionActual(token);

        // recorro las accciones por eventos
        for (EventosAcciones eventosAcciones : listaAcciones){
            Map mapAccion = new HashMap();
            mapAccion.put("codigo", eventosAcciones.getAcciones().getCodigo());
            Set<AccionesParametros> listaParametros = eventosAcciones.getAcciones().getAccionesParametros();

            for (AccionesParametros accionesParametros : listaParametros){
                Set<DetalleParametros> listaDetalleParametros = accionesParametros.getParametros().getDetalleParametros();
                Map parametros = new HashMap();
                for(DetalleParametros detalle : listaDetalleParametros){

                    List<Usuarios> listadoUsuarios = new ArrayList<>();
                    String usuarios = "[";
                    String formularioActa = "";
                    String formularioGeoCubicador = "";
                    int contMateriales = 0;
                    int contServicios = 0;
                    double montoAPagar = 0;
                    String nombreServicio = "";
                    String descripcionServicio = "";
                    String servicios = "[";
                    String materiales = "[";
                    String detalleServiciosActa = "[";
                    String detalleServiciosAdicionalesActa = "[";
                    String detalleMaterialesActa = "[";
                    String detalleMaterialesAdicionalesActa = "[";
                    int contMaterialesAdicionales = 0;
                    int contServiciosAdicionales = 0;
                    List<Decisiones> listadoDecisiones = new ArrayList<>();
                    String decisiones = "";
                    int contDecisiones = 0;
                    String modalValidar = "";
                    switch (detalle.getLlave()) {
                        // el case formulario es cuando esta configurado en base de datos el formly completo para llegar y pintar en el front end
                        case "formulario":
                            parametros.put("formulario",detalle.getValor());
                            break;
                        // el case modal es cuando esta configurado en base de datos el modal para llegar y desplegar en el front end
                        case "modal":
                            switch (detalle.getValor()) {
                                // modal para generar diseño
                                case "modal-val-acta.llave":
                                    //obtener decisiones
                                    try {
                                        listadoDecisiones = GenericDAO.getINSTANCE().obtenerDecisionesPorEventoId(Integer.parseInt(getEventoId()), Integer.parseInt(otId));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    for(Decisiones decision : listadoDecisiones){
                                        if(contDecisiones != 0){
                                            decisiones = decisiones + ",";
                                        }
                                        int idEventoAux = 0;
                                        boolean opcionValidacion = false;
                                        if(decision.getId() == 2){
                                            idEventoAux = 53;
                                            opcionValidacion =  true;

                                        }else if(decision.getId() == 3){
                                            idEventoAux = 10;
                                            opcionValidacion = false;
                                        }
                                        decisiones = decisiones + "{\n" +
                                                "  \"id\": "  + "\"" + decision.getId() + "\"" + ",\n" +
                                                "  \"opcion\": "  + "\"" + decision.getRespuesta() + "\"" + ",\n" +
                                                "  \"eventoId\": "  + "\"" + idEventoAux + "\"" + ",\n" +
                                                "  \"opcionValidacion\": "  + opcionValidacion + ",\n" +
                                                "  \"tituloBoton\": "  + "\"" +  decision.getRespuesta() + "\"" + "\n" +
                                                "}";
                                        contDecisiones++;
                                    }

                                     modalValidar = "{\n" +
                                            "  \"tituloModal\": \"Validar Diseño\",\n" +
                                            "  \"modalSize\": \"xl\",\n" +
                                            "  \"servicio\": \"obtenerAccionesFrontendPorEvento\",\n" +
                                            "  \"botones\": [\n" +  decisiones  + "  ]\n" +
                                            "}";


                                    parametros.put("modal",modalValidar);
                                    break;
                                // modal custom para el evento de validar acta
                                case "modal-validar":
                                    //obtener decisiones
//                                    List<Decisiones> listadoDecisiones = new ArrayList<>();
                                    try {
                                        listadoDecisiones = GenericDAO.getINSTANCE().obtenerDecisionesPorEventoId(Integer.parseInt(getEventoId()), Integer.parseInt(otId));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
//                                    String decisiones = "";
//                                    int contDecisiones = 0;
                                    for(Decisiones decision : listadoDecisiones){
                                        if(contDecisiones != 0){
                                            decisiones = decisiones + ",";
                                        }
                                        int idEventoAux = 0;
                                        boolean opcionValidacion = false;
                                        if(decision.getId() == 2){
                                            idEventoAux = 9;
                                            opcionValidacion =  true;

                                        }else if(decision.getId() == 3){
                                            idEventoAux = 10;
                                            opcionValidacion = false;
                                        }
                                        decisiones = decisiones + "{\n" +
                                                "  \"id\": "  + "\"" + decision.getId() + "\"" + ",\n" +
                                                "  \"opcion\": "  + "\"" + decision.getRespuesta() + "\"" + ",\n" +
                                                "  \"eventoId\": "  + "\"" + idEventoAux + "\"" + ",\n" +
                                                "  \"opcionValidacion\": "  + opcionValidacion + ",\n" +
                                                "  \"tituloBoton\": "  + "\"" +  decision.getRespuesta() + "\"" + "\n" +
                                                "}";
                                        contDecisiones++;
                                    }

                                    modalValidar = "{\n" +
                                            "  \"tituloModal\": \"Validar Acta\",\n" +
                                            "  \"modalSize\": \"xl\",\n" +
                                            "  \"servicio\": \"obtenerAccionesFrontendPorEvento\",\n" +
                                            "  \"botones\": [\n" +  decisiones  + "  ]\n" +
                                            "}";


                                    parametros.put("modal",modalValidar);
                                    break;
                                case "modal-validar.sbe":
                                    //obtener decisiones
//                                    List<Decisiones> listadoDecisiones = new ArrayList<>();
                                    try {
                                        listadoDecisiones = GenericDAO.getINSTANCE().obtenerDecisionesPorEventoId(Integer.parseInt(getEventoId()), Integer.parseInt(otId));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
//                                    String decisiones = "";
//                                    int contDecisiones = 0;
                                    for(Decisiones decision : listadoDecisiones){
                                        if(contDecisiones != 0){
                                            decisiones = decisiones + ",";
                                        }
                                        int idEventoAux = 0;
                                        boolean opcionValidacion = false;
                                        if(decision.getId() == 2){
                                            idEventoAux = 58;
                                            opcionValidacion =  true;

                                        }else if(decision.getId() == 3){
                                            idEventoAux = 10;
                                            opcionValidacion = false;
                                        }
                                        decisiones = decisiones + "{\n" +
                                                "  \"id\": "  + "\"" + decision.getId() + "\"" + ",\n" +
                                                "  \"opcion\": "  + "\"" + decision.getRespuesta() + "\"" + ",\n" +
                                                "  \"eventoId\": "  + "\"" + idEventoAux + "\"" + ",\n" +
                                                "  \"opcionValidacion\": "  + opcionValidacion + ",\n" +
                                                "  \"tituloBoton\": "  + "\"" +  decision.getRespuesta() + "\"" + "\n" +
                                                "}";
                                        contDecisiones++;
                                    }

                                    modalValidar = "{\n" +
                                            "  \"tituloModal\": \"Validar Acta\",\n" +
                                            "  \"modalSize\": \"xl\",\n" +
                                            "  \"servicio\": \"obtenerAccionesFrontendPorEvento\",\n" +
                                            "  \"botones\": [\n" +  decisiones  + "  ]\n" +
                                            "}";


                                    parametros.put("modal",modalValidar);
                                break;
                                case "modal-validar.llave":
                                    //obtener decisiones
                                    try {
                                        listadoDecisiones = GenericDAO.getINSTANCE().obtenerDecisionesPorEventoId(Integer.parseInt(getEventoId()), Integer.parseInt(otId));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    // usuarios de ot de construccion
                                    if(usuarioId == 335 || usuarioId == 330){
                                        for(Decisiones decision : listadoDecisiones){
                                            if(contDecisiones != 0){
                                                decisiones = decisiones + ",";
                                            }
                                            int idEventoAux = 0;
                                            boolean opcionValidacion = false;
                                            if(decision.getId() == 2){
                                                idEventoAux = 53;
                                                opcionValidacion =  true;

                                            }else if(decision.getId() == 3){
                                                idEventoAux = 10;
                                                opcionValidacion = false;
                                            }
                                            decisiones = decisiones + "{\n" +
                                                    "  \"id\": "  + "\"" + decision.getId() + "\"" + ",\n" +
                                                    "  \"opcion\": "  + "\"" + decision.getRespuesta() + "\"" + ",\n" +
                                                    "  \"eventoId\": "  + "\"" + idEventoAux + "\"" + ",\n" +
                                                    "  \"opcionValidacion\": "  + opcionValidacion + ",\n" +
                                                    "  \"tituloBoton\": "  + "\"" +  decision.getRespuesta() + "\"" + "\n" +
                                                    "}";
                                            contDecisiones++;
                                        }

                                        modalValidar = "{\n" +
                                                "  \"tituloModal\": \"Validar Acta\",\n" +
                                                "  \"modalSize\": \"xl\",\n" +
                                                "  \"servicio\": \"obtenerAccionesFrontendPorEvento\",\n" +
                                                "  \"botones\": [\n" +  decisiones  + "  ]\n" +
                                                "}";
                                    }else{
                                        for(Decisiones decision : listadoDecisiones){
                                            if(contDecisiones != 0){
                                                decisiones = decisiones + ",";
                                            }
                                            int idEventoAux = 0;
                                            boolean opcionValidacion = false;
                                            if(decision.getId() == 2){
                                                idEventoAux = 52;
                                                opcionValidacion =  true;

                                            }else if(decision.getId() == 5){
                                                idEventoAux = 10;
                                                opcionValidacion = false;
                                            }
                                            else if(decision.getId() == 4){
                                                idEventoAux = 58;
                                                opcionValidacion = false;
                                            }
                                            decisiones = decisiones + "{\n" +
                                                    "  \"id\": "  + "\"" + decision.getId() + "\"" + ",\n" +
                                                    "  \"opcion\": "  + "\"" + decision.getRespuesta() + "\"" + ",\n" +
                                                    "  \"eventoId\": "  + "\"" + idEventoAux + "\"" + ",\n" +
                                                    "  \"opcionValidacion\": "  + opcionValidacion + ",\n" +
                                                    "  \"tituloBoton\": "  + "\"" +  decision.getRespuesta() + "\"" + "\n" +
                                                    "}";
                                            contDecisiones++;
                                        }

                                        modalValidar = "{\n" +
                                                "  \"tituloModal\": \"Validar Acta\",\n" +
                                                "  \"modalSize\": \"xl\",\n" +
                                                "  \"servicio\": \"obtenerAccionesFrontendPorEvento\",\n" +
                                                "  \"botones\": [\n" +  decisiones  + "  ]\n" +
                                                "}";
                                    }

                                    parametros.put("modal",modalValidar);
                                    break;
                                case "modal-prueba":
                                    //saber si es ot de diseño o construccion
                                    String modalLlave = "";
                                    try {

                                        Ot otRetorno = OtDAO.getINSTANCE().obtenerDetalleOtLLave(Integer.parseInt(otIdAux));
                                        String jsondetalle = otRetorno.getJsonDetalleOt();
                                        ObjectMapper mapper = new ObjectMapper();
                                        Map mapaParams = mapper.readValue(jsondetalle, new TypeReference<HashMap<String,Object>>() {
                                        });

                                        String idOtDiseno = String.valueOf(mapaParams.get("idOtDiseno"));

                                        if ("0".equalsIgnoreCase(idOtDiseno)){
                                             modalLlave = "{\n" +
                                                    "  \"tituloModal\": \"Validar Acta\",\n" +
                                                    "  \"tituloBoton\": \"Validar\",\n" +
                                                    "  \"label\": \"Esta seguro que desea validar el acta?\",\n" +
                                                    "  \"servicio\": \"ejecutarEvento2\",\n" +
                                                    "  \"esconderGestor\": false\n" +
                                                    "}";
                                        }else{
                                             modalLlave = "{\n" +
                                                    "  \"tituloModal\": \"Validar Acta\",\n" +
                                                    "  \"tituloBoton\": \"Validar\",\n" +
                                                    "  \"label\": \"Esta seguro que desea validar el acta?\",\n" +
                                                    "  \"servicio\": \"ejecutarEvento2\",\n" +
                                                    "  \"esconderGestor\": true\n" +
                                                    "}";
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                    parametros.put("modal",modalLlave);
                                    break;
                                case "modal-seleccion-diseno-construccion":
                                    //saber si es ot de diseño o construccion
                                    modalLlave = "";
                                    try {

                                        Ot otRetorno = OtDAO.getINSTANCE().obtenerDetalleOtLLave(Integer.parseInt(otIdAux));
                                        String jsondetalle = otRetorno.getJsonDetalleOt();
                                        ObjectMapper mapper = new ObjectMapper();
                                        Map mapaParams = mapper.readValue(jsondetalle, new TypeReference<HashMap<String,Object>>() {
                                        });
                                        SimpleDateFormat formatFecha = new SimpleDateFormat("yyyy-MM-dd");
//                                        Se asigna nueva fecha paso a produccion nuevo flujo SBE
                                        Date fechaNuevoFlujoSbe = formatFecha.parse("2016-08-28");

                                        decisiones = decisiones + "{\n" +
                                                "  \"id\": "  + 2 + ",\n" +
                                                "  \"opcion\": "  + "\" Crear  OT Construccion\"" + ",\n" +
                                                "  \"eventoId\": "  + 62 +  ",\n" +
                                                "  \"tituloBoton\": "  + "\"Crear  OT Construccion\"" + "\n" +
                                                "},";
                                        decisiones = decisiones + "{\n" +
                                                "  \"id\": "  + 3 + ",\n" +
                                                "  \"opcion\": "  + "\"Pagar AP\"" + ",\n" +
                                                "  \"eventoId\": "  + 63 + ",\n" +
                                                "  \"tituloBoton\": "  + "\"Pagar AP\"" + "\n" +
                                                "}";

                                        int fechaOt = otRetorno.getFechaCreacion().compareTo(fechaNuevoFlujoSbe);

                                        if (otRetorno.getTipoOt().getId() == 2 && fechaOt >= 0){
                                            modalLlave = "{\n" +
                                                    "  \"tituloModal\": \"Crear OT\",\n" +
                                                    "  \"label\": \"¿Desea crear la OT sobre esta AP?\",\n" +
                                                    "  \"servicio\": \"ejecutarEvento2\",\n" +
                                                    "  \"botones\": [\n" +  decisiones  + "  ]\n" +
                                                    "}";
                                            parametros.put("modal",modalLlave);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    break;
                                case "modal-aceptar-acta.sbe":
                                    //saber si es ot de diseño o construccion
                                    String modalSBE = "";
                                    try {


                                        modalSBE = "{\n" +
                                                    "  \"tituloModal\": \"Validar Acta\",\n" +
                                                    "  \"tituloBoton\": \"Validar\",\n" +
                                                    "  \"label\": \"Esta seguro que desea validar el acta?\",\n" +
                                                    "  \"servicio\": \"ejecutarEvento2\",\n" +
                                                    "  \"esconderGestor\": false\n" +
                                                    "}";

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                    parametros.put("modal",modalSBE);
                                    break;
                                case "modal-validar.ord":
                                    //obtener decisiones
                                    List<Decisiones> listadoDecisionesOrd = new ArrayList<>();
                                    try {
                                        listadoDecisionesOrd = GenericDAO.getINSTANCE().obtenerDecisionesPorEventoId(Integer.parseInt(getEventoId()), Integer.parseInt(otId));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    String decisionesOrd = "";
                                    int contDecisionesOrd = 0;
                                    for(Decisiones decision : listadoDecisionesOrd){
                                        if(contDecisionesOrd != 0){
                                            decisionesOrd = decisionesOrd + ",";
                                        }
                                        int idEventoAuxOrd = 0;
                                        boolean opcionValidacion = false;
                                        if(decision.getId() == 2){
                                            idEventoAuxOrd = 21;
                                            opcionValidacion =  true;

                                        }else if(decision.getId() == 3){
                                            idEventoAuxOrd = 22;
                                            opcionValidacion = true;
                                        }
                                        decisionesOrd = decisionesOrd + "{\n" +
                                                "  \"id\": "  + "\"" + decision.getId() + "\"" + ",\n" +
                                                "  \"opcion\": "  + "\"" + decision.getRespuesta() + "\"" + ",\n" +
                                                "  \"eventoId\": "  + "\"" + idEventoAuxOrd + "\"" + ",\n" +
                                                "  \"opcionValidacion\": "  + opcionValidacion + ",\n" +
                                                "  \"tituloBoton\": "  + "\"" +  decision.getRespuesta() + "\"" + "\n" +
                                                "}";
                                        contDecisionesOrd++;
                                    }

                                    String modalValidarOrd = "{\n" +
                                            "  \"tituloModal\": \"Validar Acta\",\n" +
                                            "  \"modalSize\": \"xl\",\n" +
                                            "  \"servicio\": \"obtenerAccionesFrontendPorEvento\",\n" +
                                            "  \"botones\": [\n" +  decisionesOrd  + "  ]\n" +
                                            "}";


                                    parametros.put("modal",modalValidarOrd);
                                    break;
                                case "modal-validar.uni":
                                    //obtener decisiones
                                    List<Decisiones> listadoDecisionesUni = new ArrayList<>();
                                    try {
                                        listadoDecisionesUni = GenericDAO.getINSTANCE().obtenerDecisionesPorEventoId(Integer.parseInt(getEventoId()), Integer.parseInt(otId));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    String decisionesUni = "";
                                    int contDecisionesUni = 0;
                                    for(Decisiones decision : listadoDecisionesUni){
                                        if(contDecisionesUni != 0){
                                            decisionesUni = decisionesUni + ",";
                                        }
                                        int idEventoAuxUni = 0;
                                        boolean opcionValidacion = false;
                                        if(decision.getId() == 2){
                                            idEventoAuxUni = 43;
                                            opcionValidacion =  true;

                                        }else if(decision.getId() == 3){
                                            idEventoAuxUni = 44;
                                            opcionValidacion = false;
                                        }
                                        decisionesUni = decisionesUni + "{\n" +
                                                "  \"id\": "  + "\"" + decision.getId() + "\"" + ",\n" +
                                                "  \"opcion\": "  + "\"" + decision.getRespuesta() + "\"" + ",\n" +
                                                "  \"eventoId\": "  + "\"" + idEventoAuxUni + "\"" + ",\n" +
                                                "  \"opcionValidacion\": "  + opcionValidacion + ",\n" +
                                                "  \"tituloBoton\": "  + "\"" +  decision.getRespuesta() + "\"" + "\n" +
                                                "}";
                                        contDecisionesUni++;
                                    }

                                    String modalValidarUni = "{\n" +
                                            "  \"tituloModal\": \"Validar Acta\",\n" +
                                            "  \"modalSize\": \"xl\",\n" +
                                            "  \"servicio\": \"obtenerAccionesFrontendPorEvento\",\n" +
                                            "  \"botones\": [\n" +  decisionesUni  + "  ]\n" +
                                            "}";


                                    parametros.put("modal",modalValidarUni);
                                    break;
                                case "modal-pagar":
                                    String decisionesPagar = "";
                                    Ot otRetorno = new Ot();
                                    if(otIdAux != null) {
                                        otRetorno = OtDAO.getINSTANCE().obtenerDetalleOtLLave(Integer.parseInt(otIdAux));
                                    }
                                    SimpleDateFormat formatFecha = new SimpleDateFormat("yyyy-MM-dd");
//                                    Se asigna fecha de paso a produccion nuevo flujo SBE
                                    Date fechaNuevoFlujoSbe = formatFecha.parse("2016-08-28");
                                    decisionesPagar = decisionesPagar + "{\n" +
                                            "  \"opcion\": "  + "\"" + "pagoTotal" + "\"" + ",\n" +
                                            "  \"opcionValidacion\": "  + true + ",\n" +
                                            "  \"tituloBoton\": "  + "\"" +  "Pago Total" + "\"" + "\n" +
                                            "},";

                                    decisionesPagar = decisionesPagar + "{\n" +
                                            "  \"opcion\": "  + "\"" + "pagoParcial" + "\"" + ",\n" +
                                            "  \"opcionValidacion\": "  + false + ",\n" +
                                            "  \"tituloBoton\": "  + "\"" +  "Pago Parcial" + "\"" + "\n" +
                                            "}";
                                    String modalPagar = "";
                                    int fechaOt = otRetorno.getFechaCreacion().compareTo(fechaNuevoFlujoSbe);
                                    if(contratoId != 1 || otRetorno.getTipoOt().getId() == 1){
                                        modalPagar = "{\n" +
                                                "  \"tituloModal\": \"Solicitar Pago\",\n" +
                                                "  \"servicio\": \"ejecutarEvento2\",\n" +
                                                "  \"hideRequiredMessage\": "  + true + ",\n" +
                                                "  \"botones\": [\n" +  decisionesPagar  + "  ]\n" +
                                                "}";
                                    } else if(fechaOt >= 0) {
                                         modalPagar = "{\n" +
                                                "  \"tituloModal\": \"Solicitar Pago\",\n" +
//                                            "  \"servicio\": \"ejecutarEvento2\",\n" +
                                                "  \"hideRequiredMessage\": "  + true + ",\n" +
                                                "  \"botones\": [\n" +  decisionesPagar  + "  ]\n" +
                                                "}";
                                    } else {
                                        modalPagar = "{\n" +
                                                "  \"tituloModal\": \"Solicitar Pago\",\n" +
                                                "  \"servicio\": \"ejecutarEvento2\",\n" +
                                                "  \"hideRequiredMessage\": "  + true + ",\n" +
                                                "  \"botones\": [\n" +  decisionesPagar  + "  ]\n" +
                                                "}";
                                    }

                                    parametros.put("modal",modalPagar);
                                    break;
                                case "modal-crear-ot":
                                    String decisionesCrearOt = "";
                                    decisionesCrearOt = decisionesCrearOt + "{\n" +
                                            "  \"opcion\": "  + "\"" + "crearOtContruccion" + "\"" + ",\n" +
                                            "  \"opcionValidacion\": "  + true + ",\n" +
                                            "  \"tituloBoton\": "  + "\"" +  "Crear Ot Construcción" + "\"" + "\n" +
                                            "}";


                                    String modalCrearOt = "{\n" +
                                            "  \"tituloModal\": \"Crear OT Construción\",\n" +
                                            "  \"servicio\": \"ejecutarEvento2\",\n" +
                                            "  \"hideRequiredMessage\": "  + true + ",\n" +
                                            "  \"botones\": [\n" +  decisionesCrearOt  + "  ]\n" +
                                            "}";
                                    parametros.put("modal",modalCrearOt);
                                    break;
                                case "modal-crear-ot-full.sbe":
                                    String decisionesCrearOtFull = "";
                                    decisionesCrearOtFull = decisionesCrearOtFull + "{\n" +
                                            "  \"opcion\": "  + "\"" + "crearOtContruccion" + "\"" + ",\n" +
                                            "  \"opcionValidacion\": "  + true + ",\n" +
                                            "  \"tituloBoton\": "  + "\"" +  "Crear Ot Construcción" + "\"" + "\n" +
                                            "}";


                                    String modalCrearOtFull = "{\n" +
                                            "  \"tituloModal\": \"Crear OT Construción\",\n" +
                                            "  \"servicio\": \"ejecutarEvento2\",\n" +
                                            "  \"hideRequiredMessage\": "  + true + ",\n" +
                                            "  \"botones\": [\n" +  decisionesCrearOtFull  + "  ]\n" +
                                            "}";
                                    parametros.put("modal",modalCrearOtFull);
                                    break;
                                case "modal-pagar.ord":
                                    String decisionesPagarOrd = "";
                                    decisionesPagarOrd = decisionesPagarOrd + "{\n" +
                                            "  \"opcion\": "  + "\"" + "pagoTotal" + "\"" + ",\n" +
                                            "  \"opcionValidacion\": "  + true + ",\n" +
                                            "  \"tituloBoton\": "  + "\"" +  "Pagar" + "\"" + "\n" +
                                            "}";

                                    String modalPagarOrd = "{\n" +
                                            "  \"tituloModal\": \"Solicitar Pago\",\n" +
                                            "  \"servicio\": \"ejecutarEvento2\",\n" +
                                            "  \"hideRequiredMessage\": "  + true + ",\n" +
                                            "  \"botones\": [\n" +  decisionesPagarOrd  + "  ]\n" +
                                            "}";
                                    parametros.put("modal",modalPagarOrd);
                                    break;
                                case "modal-pagar-llave":
                                    String decisionesPagarLlave = "";
                                    decisionesPagarLlave = decisionesPagarLlave + "{\n" +
                                            "  \"opcion\": "  + "\"" + "pagoTotal" + "\"" + ",\n" +
                                            "  \"opcionValidacion\": "  + true + ",\n" +
                                            "  \"tituloBoton\": "  + "\"" +  "Pagar" + "\"" + "\n" +
                                            "}";

                                    String modalPagarLLave = "{\n" +
                                            "  \"tituloModal\": \"Solicitar Pago\",\n" +
                                            "  \"servicio\": \"ejecutarEvento2\",\n" +
                                            "  \"hideRequiredMessage\": "  + true + ",\n" +
                                            "  \"botones\": [\n" +  decisionesPagarLlave  + "  ]\n" +
                                            "}";
                                    parametros.put("modal",modalPagarLLave);
                                    break;
                                case "modalCheckList":
                                    String decisionesCheckList = "";
                                    decisionesCheckList = decisionesCheckList + "{\n" +
                                            "  \"opcion\": "  + "\"" + "checkList" + "\"" + ",\n" +
                                            "  \"eventoId\": 60, \n" +
                                            "  \"opcionValidacion\": "  + true + ",\n" +
                                            "  \"tituloBoton\": "  + "\"" +  "Siguiente" + "\"" + "\n" +
                                            "}";


                                    String modalCheckList = "{\n" +
                                            "  \"tituloModal\": \"Ingresar CheckList\",\n" +
                                            "  \"servicio\": \"obtenerAccionesFrontendPorEvento\",\n" +
                                            "  \"modalSize\": \"xl\",\n" +
                                            "  \"hideRequiredMessage\": "  + true + ",\n" +
                                            "  \"botones\": [\n" +  decisionesCheckList  + "  ]\n" +
                                            "}";
                                    parametros.put("modal",modalCheckList);
                                    break;
                                case "modalCheckListFinalizar":
                                    String decisionesCheckListFinalizar = "";
                                    decisionesCheckListFinalizar = decisionesCheckListFinalizar + "{\n" +
                                            "  \"opcion\": "  + "\"" + "checkList" + "\"" + ",\n" +
                                            "  \"eventoId\": 57, \n" +
                                            "  \"opcionValidacion\": "  + true + ",\n" +
                                            "  \"tituloBoton\": "  + "\"" +  "Finalizar" + "\"" + "\n" +
                                            "}";


                                    String modalCheckListFinalizar = "{\n" +
                                            "  \"tituloModal\": \"Finalizar CheckList\",\n" +
                                            "  \"servicio\": \"ejecutarEvento2\",\n" +
                                            "  \"modalSize\": \"xl\",\n" +
                                            "  \"hideRequiredMessage\": "  + true + ",\n" +
                                            "  \"botones\": [\n" +  decisionesCheckListFinalizar  + "  ]\n" +
                                            "}";
                                    parametros.put("modal",modalCheckListFinalizar);
                                    break;
                                case "modal-autorizar":
                                    List<Decisiones> listadoDecisionesAutorizar = new ArrayList<>();
                                    try {
                                        listadoDecisionesAutorizar = GenericDAO.getINSTANCE().obtenerDecisionesPorEventoId(Integer.parseInt(getEventoId()), Integer.parseInt(otId));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    String decisionesAutorizar = "";
                                    int contDecisionesAutorizar = 0;
                                    for(Decisiones decisionAutorizar : listadoDecisionesAutorizar){
                                        if(contDecisionesAutorizar != 0){
                                            decisionesAutorizar = decisionesAutorizar + ",";
                                        }
                                        int idEventoAux = 0;
                                        boolean opcionValidacion = false;
                                        if(decisionAutorizar.getId() == 2){
                                            idEventoAux = 13;
                                            opcionValidacion =  true;

                                        }else if(decisionAutorizar.getId() == 3){
                                            idEventoAux = 14;
                                            opcionValidacion = false;
                                        }
                                        decisionesAutorizar = decisionesAutorizar + "{\n" +
                                                "  \"id\": "  + "\"" + decisionAutorizar.getId() + "\"" + ",\n" +
                                                "  \"opcion\": "  + "\"" + decisionAutorizar.getRespuesta() + "\"" + ",\n" +
                                                "  \"eventoId\": "  + "\"" + idEventoAux + "\"" + ",\n" +
                                                "  \"opcionAutorizar\": "  + opcionValidacion + ",\n" +
                                                "  \"tituloBoton\": "  + "\"" +  decisionAutorizar.getRespuesta() + "\"" + "\n" +
                                                "}";
                                        contDecisionesAutorizar++;
                                    }

                                    String modalAutorizar = "{\n" +
                                            "  \"tituloModal\": \"Autorizar Pago\",\n" +
                                            "  \"modalSize\": \"lg\",\n" +
                                            "  \"servicio\": \"ejecutarEvento2\",\n" +
                                            "  \"botones\": [\n" +  decisionesAutorizar  + "  ]\n" +
                                            "}";

                                    parametros.put("modal",modalAutorizar);
                                    break;
                                default:
                                    parametros.put("modal",detalle.getValor());
                            }

                            break;
                        // el case eventos es cuando se esta creando una ot y se listan los eventos para seleccionar a los responsables por perfil
                        case "eventos":
//                            String valor = "[";
                            List<OtUsuariosEventosAux> listadoEventosUsuariosOt = new ArrayList<>();
                            // obtenerEventosPorContrato
                            int orden = 0;
                            try {
                                List<Contratos> listaContrato = GenericDAO.getINSTANCE().obtenerContratoPorId(getContratoId());
                                List<Workflow> listaWorkflow = new ArrayList<Workflow>();
                                int id = listaContrato.get(0).getWorkflow().getId();
                                listaWorkflow = GenericDAO.getINSTANCE().obtenerWorkflowPorId(id);
                                Set<WorkflowEventos> workflowEventos = listaWorkflow.get(0).getWorkflowEventos();
                                int cont = 0;


                                for(WorkflowEventos we: workflowEventos){

                                    Eventos evento = we.getEvento();
                                    Eventos evento2 = GenericDAO.getINSTANCE().obtenerEventoPorId(evento.getId());
                                    TipoEvento tipoEvento = evento2.getTipoEvento();
                                    List<UsuariosEventosAux> usuariosEventos = new ArrayList<>();

                                    List<Usuarios> listadoUsuarioOrganigrama = new ArrayList<>();
                                    List<UsuariosEventosAux> usuariosEventosContratosFinal = new ArrayList<>();

                                    //listadoUsuarioOrganigrama = GenericDAO.getINSTANCE().obtenerUsuariosOrganigrama(we.getRol().getId(),usuarioId,contratoId);

                                    if(evento.getId() == 31 || evento.getId() == 32 || evento.getId() == 33 || evento.getId() == 6){
                                        listadoUsuarioOrganigrama = GenericDAO.getINSTANCE().obtenerUsuariosOrganigramaSinRol(usuarioId,contratoId);
                                    }else{
                                        // busco si tiene usuarios configurados en el organigrama por contrato
                                        listadoUsuarioOrganigrama = GenericDAO.getINSTANCE().obtenerUsuariosOrganigrama(we.getRol().getId(),usuarioId,contratoId);
                                    }

                                    if(listadoUsuarioOrganigrama.size()>0){

                                        for (Usuarios usuariosAux : listadoUsuarioOrganigrama){
                                            UsuariosEventosAux usuariosEventosAux = new UsuariosEventosAux();
                                            usuariosEventosAux.setId(usuariosAux.getId());
                                            usuariosEventosAux.setNombre(usuariosAux.getNombres() + " " + usuariosAux.getApellidos());
                                            usuariosEventosAux.setInactive(true);
                                            usuariosEventosContratosFinal.add(usuariosEventosAux);
                                        }


                                    }else{
                                        // obtengo los usuarios configurados por defecto para los eventos, si es que los tiene
                                        List<UsuariosEventosContratos> usuariosEventosContratos = GenericDAO.getINSTANCE().obtenerUsuariosEventosContratos(evento.getId(), contratoId);

                                        // si hay usuarios significa que el evento tiene configurados usuarios por defecto
                                        if(usuariosEventosContratos.size()>0){
                                            for (UsuariosEventosContratos usuarioEvento: usuariosEventosContratos){
                                                Usuarios usuario = GenericDAO.getINSTANCE().obtenerUsuario(usuarioEvento.getId().getUsuarioId());
                                                UsuariosEventosAux usuariosEventosAux = new UsuariosEventosAux();
                                                usuariosEventosAux.setId(usuario.getId());
                                                usuariosEventosAux.setNombre(usuario.getNombres() + " " + usuario.getApellidos());
                                                usuariosEventosAux.setInactive(true);
                                                usuariosEventosContratosFinal.add(usuariosEventosAux);
                                            }
                                        }
                                        // si no hay usuarios significa que no hay usuarios por defecto configurados para este evento
                                        else{
                                            // obtengo el rol asociado al evento
                                            Roles rol = new Roles();
                                            rol.setId(we.getRol().getId());
                                            Roles rolGestor = GenericDAO.getINSTANCE().obtenerRolPorUsuarioId(usuarioId);

                                            //si el rol del evento es igual al rol del gestor significa que este evento lo dejo a el encargado por defecto
                                            if(rol.getId() == rolGestor.getId()){
                                                Usuarios usuario = GenericDAO.getINSTANCE().obtenerUsuario(usuarioId);
                                                UsuariosEventosAux usuariosEventosAux = new UsuariosEventosAux();
                                                usuariosEventosAux.setId(usuario.getId());
                                                usuariosEventosAux.setNombre(usuario.getNombres() + " " + usuario.getApellidos());
                                                usuariosEventosAux.setInactive(true);
                                                usuariosEventosContratosFinal.add(usuariosEventosAux);

                                            }
                                            // si no es igual al del gestor obtengo el listado de usuarios asociados al rol y al contrato
                                            else{
                                                List<Usuarios> listadoUsuario = new ArrayList<>();

                                                // pregunto para saber si el evento corresponde a telefonica o a la empresa colaboradora
                                                if(evento.getEsTelefonica()){

                                                    // busco si tiene usuarios configurados en el organigrama por contrato
                                                    // este if es porque estos eventos son de 2 roles distitos
                                                    if(evento.getId() == 31 || evento.getId() == 32 || evento.getId() == 33 || evento.getId() == 6 || evento.getId() == 13 || evento.getId() == 14){
                                                        listadoUsuario = GenericDAO.getINSTANCE().obtenerUsuariosOrganigramaSinRol(usuarioId,contratoId);
                                                    }else{
                                                        // busco si tiene usuarios configurados en el organigrama por contrato
                                                        listadoUsuario = GenericDAO.getINSTANCE().obtenerUsuariosOrganigrama(rol.getId(),usuarioId,contratoId);
                                                        if(listadoUsuario.size() == 0){
                                                            listadoUsuario = GenericDAO.getINSTANCE().obtenerUsuariosPorRolContrato(rol.getId(),contratoId);
                                                        }
                                                    }
                                                }
                                                // aca es cuando el evento es de la empresa colaboradora
                                                else{

                                                    // primero valido si el gestor tiene configurado el administrador de contrato en el organigrama
                                                    listadoUsuario = GenericDAO.getINSTANCE().obtenerUsuariosOrganigramaEECC(usuarioId,Integer.parseInt(proveedorId),getContratoId());

                                                    if(listadoUsuario.size() <= 0){
                                                        listadoUsuario = GenericDAO.getINSTANCE().obtenerUsuariosPorRolProovedor(rol.getId(),Integer.parseInt(proveedorId),getContratoId());
                                                    }

                                                }
                                                for (Usuarios usuariosAux : listadoUsuario){
                                                    UsuariosEventosAux usuariosEventosAux = new UsuariosEventosAux();
                                                    usuariosEventosAux.setId(usuariosAux.getId());
                                                    usuariosEventosAux.setNombre(usuariosAux.getNombres() + " " + usuariosAux.getApellidos());
                                                    usuariosEventosAux.setInactive(true);
                                                    usuariosEventosContratosFinal.add(usuariosEventosAux);
                                                }
                                            }
                                        }
                                    }

                                    for (WorkflowEventos workflowEventos1: workflowEventos){
                                        if(evento2.getId() == workflowEventos1.getEvento().getId()){
                                            orden = we.getOrden();
                                            break;
                                        }
                                    }
                                    OtUsuariosEventosAux otUsuariosEventosAux = new OtUsuariosEventosAux();
                                    otUsuariosEventosAux.setOrden(orden);
                                    otUsuariosEventosAux.setId(evento2.getId());
                                    otUsuariosEventosAux.setDisabled(true);
                                    otUsuariosEventosAux.setDuracion(evento.getDuracion());
                                    otUsuariosEventosAux.setNombre(evento.getNombre());
                                    if(tipoEvento.getNombre().equals("Validacion")){
                                        otUsuariosEventosAux.setDecision(true);
                                    }else{
                                        otUsuariosEventosAux.setDecision(false);
                                    }

                                    otUsuariosEventosAux.setUsuarios(usuariosEventosContratosFinal);
                                    listadoEventosUsuariosOt.add(otUsuariosEventosAux);
                                }

//                                valor = valor + "]";

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            parametros.put("eventos",listadoEventosUsuariosOt);
                            break;
                        // el case asignar es cuando se asigna un trabajador para la ot
                        case "asignar" :
                            // obtener proveedor por la ot
                            try {
                                Ot ot = OtDAO.getINSTANCE().obtenerDetalleOt(Integer.parseInt(otId));
                                int proveedorId = ot.getProveedor().getId();
                                // el rol 4 es de trabajador de empresa colaboradora
                                listadoUsuarios = UsuarioDAO.getINSTANCE().obtenerUsuariosSegunProveedorId(proveedorId, 4);
                                usuarios = usuarios + armarJsonRespuestaUsuario(listadoUsuarios);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            parametros.put("asignar",usuarios);
                            break;
                        case "asignar-co" :
                            // obtener proveedor por la ot
                            try {

                                Ot ot = OtDAO.getINSTANCE().obtenerDetalleOt(Integer.parseInt(otId));
                                int proveedorId = ot.getProveedor().getId();

                                // el rol 7 es de coordinador de empresa colaboradora
                                listadoUsuarios = UsuarioDAO.getINSTANCE().obtenerUsuariosSegunProveedorId(proveedorId, 7);
                                usuarios = usuarios + armarJsonRespuestaUsuario(listadoUsuarios);
//                                int contUsuarios = 0;
//                                for(Usuarios usuarioAux : listadoUsuarios){
//                                    if(contUsuarios != 0){
//                                        usuarios = usuarios + ",";
//                                    }
//                                    usuarios = usuarios + "{\n" +
//                                            "  \"id\": "  + "\"" + usuarioAux.getId() + "\"" + ",\n" +
//                                            "  \"nombre\": "  + "\"" + usuarioAux.getNombres() +" "+ usuarioAux.getApellidos() + "\"" + "\n" +
//                                            "}";
//                                    contUsuarios++;
//                                }
//                                usuarios = usuarios + "]";

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            parametros.put("asignar",usuarios);
                            break;
//                        Formulario Contrato Unificado Asignar ubicacion Cubicacion
                        case "form-geo-uni":
                            List<CubicadorDetalle> cubicadorDetalle = new ArrayList<CubicadorDetalle>();
                            String detalleServiciosGeoCub = "[";

                            try{
                                cubicadorDetalle = CubicadorDAO.getINSTANCE().obtenerCubicadorDetalle(cubicadorId);
                                int contadorServGeo = 0;
                                for(CubicadorDetalle cubDetalle : cubicadorDetalle){
                                    nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon( cubDetalle.getServicios().getNombre(), "");
                                    descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(cubDetalle.getServicios().getDescripcion(), "");

                                    detalleServiciosGeoCub = detalleServiciosGeoCub + "{\n" +
                                            "  \"id\": "  + "\"" + cubDetalle.getId() + "\"" + ",\n" +
                                            "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                            "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                            "  \"regionId\": "   + cubDetalle.getRegiones().getId() + ",\n" +
                                            "  \"regionNombre\": "  + "\""  + cubDetalle.getRegiones().getNombre() + "\"" + ",\n" +
                                            "  \"cantidad\": "  +  cubDetalle.getCantidad()  + ",\n" +
                                            "  \"precio\": "  +  cubDetalle.getPrecio()  + ",\n" +
                                            "  \"tipoMonedaId\": "  +  cubDetalle.getTipoMoneda().getId()  + ",\n" +
                                            "  \"tipoMoneda\": "  + "\""  +  cubDetalle.getTipoMoneda().getNombre()  +"\"" + ",\n" +
                                            "  \"montoTotal\": "  +  (cubDetalle.getCantidad() * cubDetalle.getPrecio())  + "\n" +
                                            "}";
                                    if(contadorServGeo < (cubicadorDetalle.size() - 1)){
                                        detalleServiciosGeoCub = detalleServiciosGeoCub + ",";
                                    }
                                    contadorServGeo++;

                                }
                                detalleServiciosGeoCub = detalleServiciosGeoCub + "]";
                            } catch (Exception e){
                                log.error("************  Error al obtener cubicador detalle." + e.toString() +" ****************");
                            }
                            parametros.put("formularioCrearOtUnificado",detalleServiciosGeoCub);
                            break;
                        // el case formularioActa es para armar el formulario de acta
                        case "formularioActa" :

                            try {
                                // obtener actas anteriores
//                                String detalleServiciosActa = "[";
//                                String detalleServiciosAdicionalesActa = "[";
//                                String detalleMaterialesActa = "[";
//                                String detalleMaterialesAdicionalesActa = "[";
//                                int contMaterialesAdicionales = 0;
//                                int contServiciosAdicionales = 0;
                                boolean montoPendiente = false;
                                Actas ultimaActa = new Actas();
                                ultimaActa.setId(0);
                                try {
                                    ultimaActa = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otId));
                                    if((ultimaActa.getSaldoPendiente() != null) && (ultimaActa.getSaldoPendiente() != 0.0)){
                                        montoPendiente = true;
                                    }
                                }catch (Exception e) {
                                    e.printStackTrace();
                                }



                                // no hay actas anteriores else si hay actas
                                if(ultimaActa.getId() != 0){
                                    String observaciones = ultimaActa.getObservaciones();
                                    if(observaciones != null){
                                        observaciones = observaciones.replace("\n"," ");
                                        observaciones = observaciones.replace("\t"," ");
                                    } else {
                                        observaciones = "Sin observaciones";
                                    }
                                    formularioActa = formularioActa + "{  \n" +
                                            "  \"observaciones\": "   + observaciones +",\n" +
                                            "  \"saldoPagado\": "+ ultimaActa.getMontoTotalAPagar() + ",\n" +
                                            "  \"tieneSaldoPendiente\": "+ montoPendiente + ",\n" +
                                            "  \"saldoPendiente\": "+ ultimaActa.getSaldoPendiente() + ",\n" +
                                            "  \"validacionUsuario\": "  + "\"" + ultimaActa.getValidacionUsuarios() + "\"" + ",\n";

                                    Set<DetalleMaterialesActas> listadoDetalleMateriales = ultimaActa.getDetalleMaterialesActas();
                                    Set<DetalleMaterialesAdicionalesActas> listadoDetalleMaterialesAdicionales = ultimaActa.getDetalleMaterialesAdicionalesActas();
                                    Set<DetalleServiciosActas> listadoDetalleServicios = ultimaActa.getDetalleServiciosActas();
                                    Set<DetalleServiciosAdicionalesActas> listadoDetalleServiciosAdicionales = ultimaActa.getDetalleServiciosAdicionalesActas();

                                    if(listadoDetalleMateriales.size()>0){
                                        //
                                        for(DetalleMaterialesActas detalleMaterial : listadoDetalleMateriales){
                                            if(contMateriales != 0){
                                                detalleMaterialesActa = detalleMaterialesActa + ",";
                                            }
                                            nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterial.getMateriales().getNombre(), "");
                                            descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterial.getMateriales().getDescripcion(), "");
                                            detalleMaterialesActa = detalleMaterialesActa + "{\n" +
                                                    "  \"id\": "  + "\"" + detalleMaterial.getId().getMaterialId() + "\"" + ",\n" +
                                                    "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                    "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                    "  \"cantidad\": "   + detalleMaterial.getTotalUnidadesCubicadas() + ",\n" +
                                                    "  \"precio\": "   + detalleMaterial.getMateriales().getPrecio() + ",\n" +
                                                    "  \"montoTotalCubicado\": "  +  detalleMaterial.getTotalMontoCubicado()  + ",\n" +
                                                    "  \"cantidadInformada\": "   + detalleMaterial.getTotalUnidadesEjecucionOt() + ",\n" +
                                                    "  \"cantidadReal\": "  +  detalleMaterial.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                    "  \"montoTotalActa\": "  +  detalleMaterial.getTotalMontoEjecucionActa()  + "\n" +
                                                    "}";
                                            contMateriales++;
                                        }

                                    }
                                    if(listadoDetalleMaterialesAdicionales.size()>0){
                                        //
                                        for(DetalleMaterialesAdicionalesActas detalleMaterialAdicional : listadoDetalleMaterialesAdicionales){
                                            if(contMaterialesAdicionales != 0){
                                                detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + ",";
                                            }

                                            nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterialAdicional.getMateriales().getNombre(), "");
                                            descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterialAdicional.getMateriales().getDescripcion(), "");

                                            detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + "{\n" +
                                                    "  \"id\": "  + "\"" + detalleMaterialAdicional.getId().getMaterialId() + "\"" + ",\n" +
                                                    "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                    "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                    "  \"cantidad\": "   + "0" + ",\n" +
                                                    "  \"precio\": "   + detalleMaterialAdicional.getMateriales().getPrecio() + ",\n" +
                                                    "  \"montoTotalCubicado\": "  +  detalleMaterialAdicional.getTotalMontoCubicado()  + ",\n" +
                                                    "  \"cantidadInformada\": "   + detalleMaterialAdicional.getTotalUnidadesEjecucionOt() + ",\n" +
                                                    "  \"cantidadReal\": "  +  detalleMaterialAdicional.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                    "  \"validacionUsuario\": "   + detalleMaterialAdicional.getValidacionUsuarios() +  ",\n" +
                                                    "  \"montoTotalActa\": "  +  detalleMaterialAdicional.getTotalMontoEjecucionActa()  + "\n" +
                                                    "}";
                                            contMaterialesAdicionales++;
                                        }

                                    }
                                    if(listadoDetalleServicios.size()>0){
                                        //
                                        for(DetalleServiciosActas detalleServicios : listadoDetalleServicios){
                                            if(contServicios != 0){
                                                detalleServiciosActa = detalleServiciosActa + ",";
                                            }
                                            nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServicios.getServicio().getNombre(), "");
                                            descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServicios.getServicio().getDescripcion(), "");

                                            if(!montoPendiente){
                                                detalleServiciosActa = detalleServiciosActa + "{\n" +
                                                        "  \"id\": "  + "\"" + detalleServicios.getId().getServicioId() + "\"" + ",\n" +
                                                        "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                        "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                        "  \"cantidad\": "   + detalleServicios.getTotalUnidadesCubicadas() + ",\n" +
                                                        "  \"precio\": "  +  detalleServicios.getServicio().getPrecio()  + ",\n" +
                                                        "  \"montoTotalCubicado\": "  +  detalleServicios.getTotalMontoCubicado()  + ",\n" +
                                                        "  \"cantidadInformada\": "   + detalleServicios.getTotalUnidadesEjecucionOt() + ",\n" +
                                                        "  \"cantidadReal\": "  +  detalleServicios.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                        "  \"montoTotalActa\": "  +  detalleServicios.getTotalMontoEjecucionActa()  + "\n" +
                                                        "}";
                                            }else{
                                                detalleServiciosActa = detalleServiciosActa + "{\n" +
                                                        "  \"id\": "  + "\"" + detalleServicios.getId().getServicioId() + "\"" + ",\n" +
                                                        "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                        "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                        "  \"cantidad\": "   + detalleServicios.getTotalUnidadesCubicadas() + ",\n" +
                                                        "  \"precio\": "  +  detalleServicios.getServicio().getPrecio()  + ",\n" +
                                                        "  \"montoTotalCubicado\": "  +  detalleServicios.getTotalMontoCubicado()  + ",\n" +
                                                        "  \"cantidadInformada\": "   + detalleServicios.getTotalUnidadesEjecucionOt() + ",\n" +
                                                        "  \"cantidadHistorica\": "   + detalleServicios.getTotalUnidadesEjecucionOt() + ",\n" +
                                                        "  \"montoTotalHistorico\": "   + detalleServicios.getTotalHistoricoActa() + ",\n" +
                                                        //"  \"cantidadReal\": "  +  0  + ",\n" +
                                                        //"  \"montoTotalActa\": "  +  0  + "\n" +
                                                        "  \"cantidadReal\": "  +  detalleServicios.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                        "  \"montoTotalActa\": "  +  detalleServicios.getTotalMontoEjecucionActa()  + "\n" +
                                                        "}";

                                            }
                                            contServicios++;
                                        }

                                    }
                                    if(listadoDetalleServiciosAdicionales.size()>0){
                                        //
                                        for(DetalleServiciosAdicionalesActas detalleServiciosAdicionales : listadoDetalleServiciosAdicionales){
                                            if(contServiciosAdicionales != 0){
                                                detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + ",";
                                            }
                                            nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServiciosAdicionales.getServicio().getNombre(), "");
                                            descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServiciosAdicionales.getServicio().getDescripcion(), "");

                                            if(!montoPendiente){
                                                detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "{\n" +
                                                        "  \"id\": "  + "\"" + detalleServiciosAdicionales.getId().getServicioId() + "\"" + ",\n" +
                                                        "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                        "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                        "  \"cantidad\": "   + "0" + ",\n" +
                                                        "  \"precio\": "  +  detalleServiciosAdicionales.getServicio().getPrecio()  + ",\n" +
                                                        "  \"cantidadInformada\": "   + detalleServiciosAdicionales.getTotalUnidadesEjecucionOt() + ",\n" +
                                                        "  \"cantidadReal\": "  +  detalleServiciosAdicionales.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                        "  \"montoTotalCubicado\": "  +  detalleServiciosAdicionales.getTotalMontoCubicado()  + ",\n" +
                                                        "  \"validacionUsuario\": "   + detalleServiciosAdicionales.getValidacionUsuarios() + ",\n" +
                                                        "  \"montoTotalActa\": "  +  detalleServiciosAdicionales.getTotalMontoEjecucionActa()  + "\n" +
                                                        "}";
                                            }else{
                                                detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "{\n" +
                                                        "  \"id\": "  + "\"" + detalleServiciosAdicionales.getId().getServicioId() + "\"" + ",\n" +
                                                        "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                        "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                        "  \"cantidad\": "   + "0" + ",\n" +
                                                        "  \"precio\": "  +  detalleServiciosAdicionales.getServicio().getPrecio()  + ",\n" +
                                                        "  \"cantidadInformada\": "   + detalleServiciosAdicionales.getTotalUnidadesEjecucionOt() + ",\n" +
                                                        //"  \"cantidadReal\": "  +  0  + ",\n" +
                                                        "  \"cantidadReal\": "  +  detalleServiciosAdicionales.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                        "  \"montoTotalCubicado\": "  +  detalleServiciosAdicionales.getTotalMontoCubicado()  + ",\n" +
                                                        "  \"validacionUsuario\": "   + detalleServiciosAdicionales.getValidacionUsuarios() + ",\n" +
                                                        "  \"cantidadHistorica\": "   + detalleServiciosAdicionales.getTotalUnidadesEjecucionOt() + ",\n" +
                                                        "  \"montoTotalHistorico\": "   + detalleServiciosAdicionales.getTotalHistoricoActa() + ",\n" +
                                                        //"  \"montoTotalActa\": "  +  0  + "\n" +
                                                        "  \"montoTotalActa\": "  +  detalleServiciosAdicionales.getTotalMontoEjecucionActa()  + "\n" +
                                                        "}";
                                            }

                                            contServiciosAdicionales++;
                                        }

                                    }
                                    detalleMaterialesActa = detalleMaterialesActa + "]";
                                    detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + "]";
                                    detalleServiciosActa = detalleServiciosActa + "]";
                                    detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "]";

                                    formularioActa = formularioActa + "\"materiales\":" +detalleMaterialesActa + ",";
                                    formularioActa = formularioActa + "\"materialesAdicionales\":" +detalleMaterialesAdicionalesActa + ",";
                                    formularioActa = formularioActa + "\"servicios\":" +detalleServiciosActa + ",";
                                    formularioActa = formularioActa + "\"serviciosAdicionales\":" +detalleServiciosAdicionalesActa;
                                    formularioActa = formularioActa + "}";


                                }else{
                                    List<Cubicador> cubicaciones = CubicadorDAO.getINSTANCE().obtenerCubicacionPorIdOt(Integer.parseInt(otId));
                                    Cubicador cubicacion = cubicaciones.get(0);
                                    Set<CubicadorMateriales> listadoMateriales = cubicacion.getCubicadorMateriales();
                                    Set<CubicadorServicios> listadoServicios = cubicacion.getCubicadorServicios();

                                    for(CubicadorMateriales material : listadoMateriales){
                                        if(contMateriales != 0){
                                            materiales = materiales + ",";
                                        }
                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(material.getMaterial().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(material.getMaterial().getDescripcion(), "");

                                        materiales = materiales + "{\n" +
                                                "  \"id\": "  + "\"" + material.getMaterial().getId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"cantidad\": "  + material.getCantidad() + ",\n" +
                                                "  \"cantidadInformada\": "   + "0" + ",\n" +
                                                "  \"montoTotalCubicado\": "  +  material.getMontoTotal()  + ",\n" +
                                                "  \"cantidadReal\": "  + material.getCantidad() + "\n" +
                                                "}";
                                        contMateriales++;
                                    }


                                    if(listadoServicios.size()>0){
                                        materiales = materiales + "],";
                                    }else{
                                        materiales = materiales + "]";
                                    }


                                    for(CubicadorServicios servicio : listadoServicios){
                                        if(contServicios != 0){
                                            servicios = servicios + ",";
                                        }
                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(servicio.getServicio().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(servicio.getServicio().getDescripcion(), "");

                                        servicios = servicios + "{\n" +
                                                "  \"id\": "  + "\"" + servicio.getServicio().getId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"cantidad\": "  + servicio.getCantidad() + ",\n" +
                                                "  \"precio\": "  +  servicio.getPrecio()  + ",\n" +
                                                "  \"montoTotalCubicado\": "  +  servicio.getTotal()  + ",\n" +
                                                "  \"cantidadInformada\": "   + "0" + ",\n" +
                                                "  \"cantidadReal\": "  + servicio.getCantidad() + "\n" +
                                                "}";
                                        contServicios++;
                                    }

                                    servicios = servicios + "]";

                                    if(contServicios > 0 || contMateriales > 0){
                                        formularioActa = formularioActa + "{  \n" +
                                                "  \"id\":\"5\",\n" +"";

                                        if(contMateriales > 0){
                                            formularioActa = formularioActa + "\"materiales\":" +materiales;
                                        }
                                        if(contServicios > 0){
                                            formularioActa = formularioActa + "\"servicios\":" +servicios;
                                        }
                                        formularioActa = formularioActa + "}";
                                    }else{
                                        formularioActa = formularioActa + "{  \n" +
                                                "  \"id\":\"5\"\n" +
                                                "}";
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            parametros.put("formularioActa",formularioActa);
                            break;
                        case "form-gen-acta.llave" :

//                            String servicios = "[";
//                            String materiales = "[";
                            try {
                                // obtener actas anteriores
//                                String detalleServiciosActa = "[";
//                                String detalleServiciosAdicionalesActa = "[";
//                                String detalleMaterialesActa = "[";
//                                String detalleMaterialesAdicionalesActa = "[";
//                                int contMaterialesAdicionales = 0;
//                                int contServiciosAdicionales = 0;

                                Actas ultimaActa = new Actas();
                                ultimaActa.setId(0);
                                try {
                                    ultimaActa = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otId));
                                }catch (Exception e) {
                                    e.printStackTrace();
                                }


                                // no hay actas anteriores else si hay actas
                                if(ultimaActa.getId() != 0){
                                    String observaciones = ultimaActa.getObservaciones();
                                    if(observaciones != null){
                                        observaciones = observaciones.replace("\n"," ");
                                        observaciones = observaciones.replace("\t"," ");
                                    } else {
                                        observaciones = "Sin observaciones";
                                    }
                                    formularioActa = formularioActa + "{  \n" +
                                            "  \"observaciones\": "   + observaciones +",\n" +
                                            "  \"validacionUsuario\": "  + "\"" + ultimaActa.getValidacionUsuarios() + "\"" + ",\n";

                                    Set<DetalleMaterialesActas> listadoDetalleMateriales = ultimaActa.getDetalleMaterialesActas();
                                    Set<DetalleMaterialesAdicionalesActas> listadoDetalleMaterialesAdicionales = ultimaActa.getDetalleMaterialesAdicionalesActas();
                                    Set<DetalleServiciosActas> listadoDetalleServicios = ultimaActa.getDetalleServiciosActas();
                                    Set<DetalleServiciosAdicionalesActas> listadoDetalleServiciosAdicionales = ultimaActa.getDetalleServiciosAdicionalesActas();

                                    if(listadoDetalleMateriales.size()>0){
                                        //
                                        for(DetalleMaterialesActas detalleMaterial : listadoDetalleMateriales){
                                            if(contMateriales != 0){
                                                detalleMaterialesActa = detalleMaterialesActa + ",";
                                            }

                                            nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterial.getMateriales().getNombre(), "");
                                            descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterial.getMateriales().getDescripcion(), "");

                                            detalleMaterialesActa = detalleMaterialesActa + "{\n" +
                                                    "  \"id\": "  + "\"" + detalleMaterial.getId().getMaterialId() + "\"" + ",\n" +
                                                    "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                    "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                    "  \"cantidad\": "   + detalleMaterial.getTotalUnidadesCubicadas() + ",\n" +
                                                    "  \"precio\": "   + detalleMaterial.getMateriales().getPrecio() + ",\n" +
                                                    "  \"montoTotalCubicado\": "  +  detalleMaterial.getTotalMontoCubicado()  + ",\n" +
                                                    "  \"cantidadInformada\": "   + detalleMaterial.getTotalUnidadesEjecucionOt() + ",\n" +
                                                    "  \"cantidadReal\": "  +  detalleMaterial.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                    "  \"montoTotalActa\": "  +  detalleMaterial.getTotalMontoEjecucionActa()  + "\n" +
                                                    "}";
                                            contMateriales++;
                                        }

                                    }
                                    if(listadoDetalleMaterialesAdicionales.size()>0){
                                        //
                                        for(DetalleMaterialesAdicionalesActas detalleMaterialAdicional : listadoDetalleMaterialesAdicionales){
                                            if(contMaterialesAdicionales != 0){
                                                detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + ",";
                                            }

                                            nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterialAdicional.getMateriales().getNombre(), "");
                                            descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterialAdicional.getMateriales().getDescripcion(), "");

                                            detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + "{\n" +
                                                    "  \"id\": "  + "\"" + detalleMaterialAdicional.getId().getMaterialId() + "\"" + ",\n" +
                                                    "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                    "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                    "  \"cantidad\": "   + "0" + ",\n" +
                                                    "  \"precio\": "   + detalleMaterialAdicional.getMateriales().getPrecio() + ",\n" +
                                                    "  \"montoTotalCubicado\": "  +  detalleMaterialAdicional.getTotalMontoCubicado()  + ",\n" +
                                                    "  \"cantidadInformada\": "   + detalleMaterialAdicional.getTotalUnidadesEjecucionOt() + ",\n" +
                                                    "  \"cantidadReal\": "  +  detalleMaterialAdicional.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                    "  \"validacionUsuario\": "   + detalleMaterialAdicional.getValidacionUsuarios() +  ",\n" +
                                                    "  \"montoTotalActa\": "  +  detalleMaterialAdicional.getTotalMontoEjecucionActa()  + "\n" +
                                                    "}";
                                            contMaterialesAdicionales++;
                                        }

                                    }
                                    if(listadoDetalleServicios.size()>0){
                                        //
                                        for(DetalleServiciosActas detalleServicios : listadoDetalleServicios){
                                            if(contServicios != 0){
                                                detalleServiciosActa = detalleServiciosActa + ",";
                                            }
                                            nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServicios.getServicio().getNombre(), "");
                                            descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServicios.getServicio().getDescripcion(), "");

                                            detalleServiciosActa = detalleServiciosActa + "{\n" +
                                                    "  \"id\": "  + "\"" + detalleServicios.getId().getServicioId() + "\"" + ",\n" +
                                                    "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                    "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                    "  \"cantidad\": "   + detalleServicios.getTotalUnidadesCubicadas() + ",\n" +
                                                    "  \"precio\": "  +  detalleServicios.getServicio().getPrecio()  + ",\n" +
                                                    "  \"montoTotalCubicado\": "  +  detalleServicios.getTotalMontoCubicado()  + ",\n" +
                                                    "  \"cantidadInformada\": "   + detalleServicios.getTotalUnidadesEjecucionOt() + ",\n" +
                                                    "  \"cantidadReal\": "  +  detalleServicios.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                    "  \"montoTotalActa\": "  +  detalleServicios.getTotalMontoEjecucionActa()  + "\n" +
                                                    "}";
                                            contServicios++;
                                        }

                                    }
                                    if(listadoDetalleServiciosAdicionales.size()>0){
                                        //
                                        for(DetalleServiciosAdicionalesActas detalleServiciosAdicionales : listadoDetalleServiciosAdicionales){
                                            if(contServiciosAdicionales != 0){
                                                detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + ",";
                                            }

                                            nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServiciosAdicionales.getServicio().getNombre(), "");
                                            descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServiciosAdicionales.getServicio().getDescripcion(), "");

                                            detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "{\n" +
                                                    "  \"id\": "  + "\"" + detalleServiciosAdicionales.getId().getServicioId() + "\"" + ",\n" +
                                                    "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                    "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                    "  \"cantidad\": "   + "0" + ",\n" +
                                                    "  \"precio\": "  +  detalleServiciosAdicionales.getServicio().getPrecio()  + ",\n" +
                                                    "  \"cantidadInformada\": "   + detalleServiciosAdicionales.getTotalUnidadesEjecucionOt() + ",\n" +
                                                    "  \"cantidadReal\": "  +  detalleServiciosAdicionales.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                    "  \"montoTotalCubicado\": "  +  detalleServiciosAdicionales.getTotalMontoCubicado()  + ",\n" +
                                                    "  \"validacionUsuario\": "   + detalleServiciosAdicionales.getValidacionUsuarios() + ",\n" +
                                                    "  \"montoTotalActa\": "  +  detalleServiciosAdicionales.getTotalMontoEjecucionActa()  + "\n" +
                                                    "}";
                                            contServiciosAdicionales++;
                                        }

                                    }
                                    detalleMaterialesActa = detalleMaterialesActa + "]";
                                    detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + "]";
                                    detalleServiciosActa = detalleServiciosActa + "]";
                                    detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "]";

                                    formularioActa = formularioActa + "\"materiales\":" +detalleMaterialesActa + ",";
                                    formularioActa = formularioActa + "\"materialesAdicionales\":" +detalleMaterialesAdicionalesActa + ",";
                                    formularioActa = formularioActa + "\"servicios\":" +detalleServiciosActa + ",";
                                    formularioActa = formularioActa + "\"serviciosAdicionales\":" +detalleServiciosAdicionalesActa;
                                    formularioActa = formularioActa + "}";


                                }else{
                                    List<Cubicador> cubicaciones = CubicadorDAO.getINSTANCE().obtenerCubicacionPorIdOt(Integer.parseInt(otId));
                                    Cubicador cubicacion = cubicaciones.get(0);
                                    Set<CubicadorMateriales> listadoMateriales = cubicacion.getCubicadorMateriales();
                                    Set<CubicadorServicios> listadoServicios = cubicacion.getCubicadorServicios();

                                    for(CubicadorMateriales material : listadoMateriales){
                                        if(contMateriales != 0){
                                            materiales = materiales + ",";
                                        }

                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(material.getMaterial().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(material.getMaterial().getDescripcion(), "");

                                        materiales = materiales + "{\n" +
                                                "  \"id\": "  + "\"" + material.getMaterial().getId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"cantidad\": "  + material.getCantidad() + ",\n" +
                                                "  \"cantidadInformada\": "   + "0" + ",\n" +
                                                "  \"montoTotalCubicado\": "  +  material.getMontoTotal()  + ",\n" +
                                                "  \"cantidadReal\": "  + material.getCantidad() + "\n" +
                                                "}";
                                        contMateriales++;
                                    }


                                    if(listadoServicios.size()>0){
                                        materiales = materiales + "],";
                                    }else{
                                        materiales = materiales + "]";
                                    }


                                    for(CubicadorServicios servicio : listadoServicios){
                                        if(contServicios != 0){
                                            servicios = servicios + ",";
                                        }
                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(servicio.getServicio().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(servicio.getServicio().getDescripcion(), "");

                                        servicios = servicios + "{\n" +
                                                "  \"id\": "  + "\"" + servicio.getServicio().getId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"cantidad\": "  + servicio.getCantidad() + ",\n" +
                                                "  \"precio\": "  +  servicio.getPrecio()  + ",\n" +
                                                "  \"montoTotalCubicado\": "  +  servicio.getTotal()  + ",\n" +
                                                "  \"cantidadInformada\": "   + "0" + ",\n" +
                                                "  \"cantidadReal\": "  + servicio.getCantidad() + "\n" +
                                                "}";
                                        contServicios++;
                                    }

                                    servicios = servicios + "]";

                                    if(contServicios > 0 || contMateriales > 0){
                                        formularioActa = formularioActa + "{  \n" +
                                                "  \"id\":\"5\",\n" +"";

                                        if(contMateriales > 0){
                                            formularioActa = formularioActa + "\"materiales\":" +materiales;
                                        }
                                        if(contServicios > 0){
                                            formularioActa = formularioActa + "\"servicios\":" +servicios;
                                        }
                                        formularioActa = formularioActa + "}";
                                    }else{
                                        formularioActa = formularioActa + "{  \n" +
                                                "  \"id\":\"5\"\n" +
                                                "}";
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            parametros.put("formularioActa",formularioActa);
                            break;
                        case "formularioActaOrd" :
                            String materialesOr = "[";
                            int contMaterialesOr = 0;
                            int contadorTotalMateriales = 0;
                            try {
                                // obtener actas anteriores
                                String detalleMaterialesActaOr = "[";
                                Actas ultimaActa = new Actas();
                                ultimaActa.setId(0);
                                List<DetalleItemsOrdinario> detalleItemsOrdinario = new ArrayList<DetalleItemsOrdinario>();
                                String llaveAbierta = "{";
                                String llaveCerrada = "}";
                                try {
                                    ultimaActa = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otId));
                                    String observaciones = ultimaActa.getObservaciones();
                                    if(observaciones != null){
                                        observaciones = observaciones.replace("\n"," ");
                                        observaciones = observaciones.replace("\t"," ");
                                    } else {
                                        observaciones = "Sin observaciones";
                                    }
                                    Date fechaCreacion = ultimaActa.getOt().getFechaCreacion();
                                    Date fechaTerminoEstimada = ultimaActa.getOt().getFechaTerminoEstimada();
                                    boolean validacionSistema = ultimaActa.getValidacionSistema();

                                    formularioActa = formularioActa + "{  \n" +
                                            "  \"idActa\": "  + ultimaActa.getId()  + ",\n" +
                                            "  \"observaciones\": "   + observaciones + ",\n" +
                                            "  \"validacionSistema\": "  + "\"" + validacionSistema + "\"" + ",\n" +
                                            "  \"fechaCreacion\": "  + "\"" + fechaCreacion + "\"" + ",\n" +
                                            "  \"fechaTerminoEstimada\": "  + "\"" + fechaTerminoEstimada + "\"" + ",\n" ;
                                    llaveAbierta = "";
                                    llaveCerrada = "";
                                }catch (Exception e) {
                                    e.printStackTrace();
                                }
                                // obtener cubicaciones
                                detalleItemsOrdinario = CubicadorDAO.getINSTANCE().obtenerCubicacionOrdinarioPorIdOt(Integer.parseInt(otId), ultimaActa);
                                contadorTotalMateriales = detalleItemsOrdinario.size();
                                double montoInformadoItems = 0.0;
                                for(DetalleItemsOrdinario materialCub : detalleItemsOrdinario){
                                    montoInformadoItems = 0;
                                    try{
                                        if(materialCub.getDetalleCubicadorOrdinarioActas().getTotalUnidadesEjecucionOt() > 0){
                                            montoInformadoItems = materialCub.getDetalleCubicadorOrdinarioActas().getTotalUnidadesEjecucionOt();
                                        }
                                    } catch (Exception e){
                                        montoInformadoItems = 0;
                                    }

                                    nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(materialCub.getCubicadorOrdinario().getItem(), "");

                                    materialesOr = materialesOr + "{\n" +
                                            "  \"id\": "   + materialCub.getCubicadorOrdinario().getId() +  ",\n" +
                                            "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                            "  \"cantidad\": "  + materialCub.getCubicadorOrdinario().getCantidad() + ",\n" +
                                            "  \"unidadMedida\": " + "\"" +  materialCub.getCubicadorOrdinario().getTipoUnidadMedida().getNombre() + "\"" + ",\n" +
                                            "  \"precio\": "  + materialCub.getCubicadorOrdinario().getPrecio() + ",\n" +
                                            "  \"montoTotalCubicado\": " + materialCub.getCubicadorOrdinario().getTotal()+ ",\n" +
                                            "  \"tipoMonedaId\": " + materialCub.getCubicadorOrdinario().getTipoMoneda().getId() + ",\n" +
                                            "  \"tipoMoneda\": " + "\"" + materialCub.getCubicadorOrdinario().getTipoMoneda().getNombre() + "\""+ ",\n" +
                                            "  \"cantidadInformada\": "   + montoInformadoItems + ",\n" +
                                            "  \"cantidadReal\": "  + materialCub.getDetalleCubicadorOrdinarioActas().getTotalUnidadesEjecucionActa() + ",\n" +
                                            "  \"montoTotalActa\": " + materialCub.getDetalleCubicadorOrdinarioActas().getTotalMontoEjecucion() + "\n" +
                                            "}";
                                    if(contMaterialesOr < (contadorTotalMateriales - 1)){
                                        materialesOr = materialesOr + ",";
                                    }
                                    contMaterialesOr++;
                                }
                                materialesOr = materialesOr + "]";
                                if( contMaterialesOr > 0){
                                    formularioActa = formularioActa + llaveAbierta + "  \n" +
                                            "  \"id\":\"20\",\n" +"";

                                    if(contMaterialesOr > 0){
                                        formularioActa = formularioActa + "\"materiales\":" +materialesOr;
                                    }

                                    formularioActa = formularioActa + "}";
                                }else{
                                    formularioActa = formularioActa + "{  \n" +
                                            "  \"id\":\"5\"\n" +
                                            "}";
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                log.error("=================== Error al preparar objeto materiales acta ordinaria ======================");
                                log.error("Error: "+ e.toString());
                            }
                            parametros.put("formularioActa",formularioActa);
                            break;
                        case "formularioActaUni" :
                            Map mapFormActaUni = new HashMap();
                            List serviciosUniList = new ArrayList();
                            List detalleServiciosAdicionalesUnificadoList = new ArrayList();
                            Map mapaServiciosUni = new HashMap();
                            Map tipoUnidMedidaMap = new HashMap();
                            try {
                                // obtener actas anteriores
                                String detalleMaterialesActaOr = "[";
                                Actas ultimaActa = new Actas();
                                ultimaActa.setId(0);
                                List<DetalleServiciosUnificado> detalleServiciosUnificados = new ArrayList<DetalleServiciosUnificado>();
                                List<DetalleServiciosUnificado> detalleServiciosAdicionalesUnificados = new ArrayList<DetalleServiciosUnificado>();
                                List<Cubicador> cubicaciones = CubicadorDAO.getINSTANCE().obtenerCubicacionPorIdOt(Integer.parseInt(otId));
                                Cubicador cubicacion = cubicaciones.get(0);
                                LinkedHashMap serviciosAux = new LinkedHashMap();
                                try {
                                    ultimaActa = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otId));
                                    String observaciones = ultimaActa.getObservaciones();
                                    if(observaciones != null){
                                        observaciones = observaciones.replace("\n"," ");
                                        observaciones = observaciones.replace("\t"," ");
                                    } else {
                                        observaciones = "Sin observaciones";
                                    }
                                    Date fechaCreacion = ultimaActa.getOt().getFechaCreacion();
                                    Date fechaTerminoEstimada = ultimaActa.getOt().getFechaTerminoEstimada();
                                    boolean validacionSistema = ultimaActa.getValidacionSistema();

                                    mapFormActaUni.put("idActa", ultimaActa.getId());
                                    mapFormActaUni.put("observaciones",observaciones);
                                    mapFormActaUni.put("validacionSistema", validacionSistema);
                                    mapFormActaUni.put("fechaCreacion", fechaCreacion);
                                    mapFormActaUni.put("fechaTerminoEstimada", fechaTerminoEstimada);
                                }catch (Exception e) {
                                    e.printStackTrace();
                                }
                                // obtener cubicaciones
                                detalleServiciosUnificados = CubicadorDAO.getINSTANCE().obtenerCubicacionUnificadaPorIdOt(Integer.parseInt(otId), ultimaActa);
                                detalleServiciosAdicionalesUnificados = CubicadorDAO.getINSTANCE().obtenerCubicacionUnificadaAdicionalPorIdOtValidar(Integer.parseInt(otId), ultimaActa);
                                double montoInformadoServicios = 0.0;
                                for(DetalleServiciosUnificado serviciosCubDetalle : detalleServiciosUnificados){
                                    try{
                                        montoInformadoServicios = 0;
                                        if(serviciosCubDetalle.getCubicadorDetalleUnificadoActas().getTotalUnidadesEjecucionOt() > 0){
                                            montoInformadoServicios = serviciosCubDetalle.getCubicadorDetalleUnificadoActas().getTotalUnidadesEjecucionOt();
                                        }
                                    } catch (Exception e){
                                        montoInformadoServicios = 0;
                                    }

                                    mapaServiciosUni = new HashMap();
                                    serviciosAux = new LinkedHashMap();
                                    int servicioId = serviciosCubDetalle.getCubicadorDetalle().getServicios().getId();
                                    int regionId = serviciosCubDetalle.getCubicadorDetalle().getRegionId();
                                    int tipoServicio = serviciosCubDetalle.getCubicadorDetalle().getServicios().getTipoServicio().getId();
                                    int agenciaId = serviciosCubDetalle.getCubicadorDetalle().getAgencia().getId();
                                    int centralId = serviciosCubDetalle.getCubicadorDetalle().getCentrales().getId();
                                    double cantidad = serviciosCubDetalle.getCubicadorDetalle().getCantidad();
                                    serviciosAux = ProveedorServiciosDAO.getInstance().listarServicioPorProveedorRegionContratoServicioId(4, regionId, agenciaId, centralId, cubicacion.getProveedor().getId(), tipoServicio, servicioId, cantidad);

                                    tipoUnidMedidaMap = new HashMap();
                                    tipoUnidMedidaMap = (HashMap) serviciosAux.get("tipoUnidadMedida");




                                    nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon( serviciosCubDetalle.getCubicadorDetalle().getServicios().getNombre(), "");
                                    descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(serviciosCubDetalle.getCubicadorDetalle().getServicios().getDescripcion(), "");

                                    mapaServiciosUni.put("id", serviciosCubDetalle.getCubicadorDetalle().getId());
                                    mapaServiciosUni.put("nombre", nombreServicio);
                                    mapaServiciosUni.put("servicio", serviciosAux);
                                    mapaServiciosUni.put("descripcion", descripcionServicio);
                                    mapaServiciosUni.put("cantidad", serviciosCubDetalle.getCubicadorDetalle().getCantidad());
                                    mapaServiciosUni.put("cantidadReal", serviciosCubDetalle.getCubicadorDetalle().getCantidad());
                                    mapaServiciosUni.put("precio", serviciosCubDetalle.getCubicadorDetalle().getPrecio());
                                    mapaServiciosUni.put("montoTotalCubicado", serviciosCubDetalle.getCubicadorDetalle().getTotal());
                                    mapaServiciosUni.put("tipoMonedaId", serviciosCubDetalle.getCubicadorDetalle().getTipoMoneda().getId());
                                    mapaServiciosUni.put("tipoMoneda", serviciosCubDetalle.getCubicadorDetalle().getTipoMoneda().getNombre());
                                    mapaServiciosUni.put("cantidadInformada", montoInformadoServicios);
                                    mapaServiciosUni.put("cantidadReal", serviciosCubDetalle.getCubicadorDetalle().getCantidad());
                                    mapaServiciosUni.put("montoTotalActa", serviciosCubDetalle.getCubicadorDetalleUnificadoActas().getTotalMontoEjecucion());
                                    mapaServiciosUni.put("idRegion",serviciosCubDetalle.getCubicadorDetalle().getRegiones().getId());
                                    mapaServiciosUni.put("nombreRegion", serviciosCubDetalle.getCubicadorDetalle().getRegiones().getNombre());
                                    mapaServiciosUni.put("idAgencia", serviciosCubDetalle.getCubicadorDetalle().getAgencia().getId());
                                    mapaServiciosUni.put("nombreAgencia", serviciosCubDetalle.getCubicadorDetalle().getAgencia().getNombre());
                                    mapaServiciosUni.put("idCentral", serviciosCubDetalle.getCubicadorDetalle().getCentrales().getId());
                                    mapaServiciosUni.put("nombreCentral", serviciosCubDetalle.getCubicadorDetalle().getCentrales().getNombre());

                                    mapaServiciosUni.put("tipoUnidadMedidaId", tipoUnidMedidaMap.get("id"));
                                    mapaServiciosUni.put("tipoUnidadMedidaIdNombre", tipoUnidMedidaMap.get("nombre"));

                                    serviciosUniList.add(mapaServiciosUni);
                                }
                                if(detalleServiciosAdicionalesUnificados.size()>0){

                                    for(DetalleServiciosUnificado detalleServiciosUnificadoAdicionales : detalleServiciosAdicionalesUnificados){
                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon( detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getServicios().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getServicios().getDescripcion(), "");

                                        mapaServiciosUni = new HashMap();
                                        serviciosAux = new LinkedHashMap();
                                        int servicioId = detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getServicios().getId();
                                        int regionId = detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getRegionId();
                                        int tipoServicio = detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getServicios().getTipoServicio().getId();
                                        int agenciaId = detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getAgencia().getId();
                                        int centralId = detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getCentrales().getId();
                                        double cantidad = detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getCantidad();
                                        serviciosAux = ProveedorServiciosDAO.getInstance().listarServicioPorProveedorRegionContratoServicioId(4, regionId, agenciaId, centralId, cubicacion.getProveedor().getId(), tipoServicio, servicioId, cantidad);
                                        Map agenciaMap = new HashMap();
                                        Map centralMap = new HashMap();
                                        tipoUnidMedidaMap = new HashMap();

                                        agenciaMap = (HashMap) serviciosAux.get("agencia");
                                        centralMap = (HashMap) serviciosAux.get("central");
                                        tipoUnidMedidaMap = (HashMap) serviciosAux.get("tipoUnidadMedida");

                                        mapaServiciosUni.put("id", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getId());
                                        mapaServiciosUni.put("nombre", nombreServicio);
                                        mapaServiciosUni.put("servicio", serviciosAux);
                                        mapaServiciosUni.put("descripcion", descripcionServicio);
                                        mapaServiciosUni.put("precio", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getPrecio());
                                        mapaServiciosUni.put("cantidad", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getCantidad());
                                        mapaServiciosUni.put("montoTotal", (detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getPrecio() * detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getCantidad()));
                                        mapaServiciosUni.put("idRegion", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getRegionId());
                                        mapaServiciosUni.put("nombreRegion", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getRegiones().getNombre());
                                        mapaServiciosUni.put("agencia", agenciaMap);
                                        mapaServiciosUni.put("idAgencia", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getAgenciaId());
                                        mapaServiciosUni.put("nombreAgencia", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getAgencia().getNombre());
                                        mapaServiciosUni.put("idCentral", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getCentralId());
                                        mapaServiciosUni.put("central", centralMap);
                                        mapaServiciosUni.put("nombreCentral", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getCentrales().getNombre());
                                        mapaServiciosUni.put("validacionUsuario", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getValidacionSistema());
                                        mapaServiciosUni.put("codigoAlcance", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getAlcances().getCodigo());
                                        mapaServiciosUni.put("nombreAlcance", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getAlcances().getNombre());
                                        mapaServiciosUni.put("descripcionAlcance", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getAlcances().getDescripcion());

                                        mapaServiciosUni.put("tipoUnidadMedidaId", tipoUnidMedidaMap.get("id"));
                                        mapaServiciosUni.put("tipoUnidadMedidaIdNombre", tipoUnidMedidaMap.get("nombre"));

                                        detalleServiciosAdicionalesUnificadoList.add(mapaServiciosUni);
                                    }

                                }

                                if( detalleServiciosUnificados.size() > 0){
                                    mapFormActaUni.put("id", 42);
                                    if(detalleServiciosUnificados.size() > 0){
                                        mapFormActaUni.put("servicios", serviciosUniList);
                                    }
                                    if(detalleServiciosAdicionalesUnificados.size() > 0){
                                        mapFormActaUni.put("serviciosAdicionales", detalleServiciosAdicionalesUnificadoList);
                                    } else {
                                        mapFormActaUni.put("serviciosAdicionales", "");
                                    }
                                }else{
                                    mapFormActaUni.put("id", 42);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                log.error("=================== Error al preparar objeto materiales acta ordinaria ======================");
                                log.error("Error: "+ e.toString());
                            }
                            parametros.put("formularioActa",mapFormActaUni);
                            break;
                        case "formularioValidarActa" :

//                            String detalleServiciosActa = "[";
//                            String detalleServiciosAdicionalesActa = "[";
//                            String detalleMaterialesActa = "[";
//                            String detalleMaterialesAdicionalesActa = "[";
//                            int contMaterialesAdicionales = 0;
//                            int contServiciosAdicionales = 0;
                            try {

                                // obtener acta a validar
                                boolean montoPendiente = false;
                                Actas acta = new Actas();
                                try {
                                     acta = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otId));
                                    if((acta.getSaldoPendiente() != null) && (acta.getSaldoPendiente() != 0.0)){
                                        montoPendiente = true;
                                    }
                                } catch (Exception e){
                                     acta = null;
                                    e.printStackTrace();
                                }

                                String observaciones = acta.getObservaciones();
                                if(observaciones != null){
                                    observaciones = observaciones.replace("\n"," ");
                                    observaciones = observaciones.replace("\t"," ");
                                } else {
                                    observaciones = "Sin observaciones";
                                }

                                Date fechaCreacion = acta.getOt().getFechaCreacion();
                                Date fechaTerminoEstimada = acta.getOt().getFechaTerminoEstimada();
                                boolean validacionSistema = acta.getValidacionSistema();

                                formularioActa = formularioActa + "{  \n" +
                                        "  \"idActa\": "  + acta.getId()  + ",\n" +
                                        "  \"observaciones\": "   + observaciones + ",\n" +
                                        "  \"validacionSistema\": "  + "\"" + validacionSistema + "\"" + ",\n" +
                                        "  \"fechaCreacion\": "  + "\"" + fechaCreacion + "\"" + ",\n" +
                                        "  \"fechaTerminoEstimada\": "  + "\"" + fechaTerminoEstimada + "\"" + ",\n"+
                                        "  \"segundoPagoParcial\": "  + montoPendiente + ",\n";

                                Set<DetalleMaterialesActas> listadoDetalleMateriales = acta.getDetalleMaterialesActas();
                                Set<DetalleMaterialesAdicionalesActas> listadoDetalleMaterialesAdicionales = acta.getDetalleMaterialesAdicionalesActas();
                                Set<DetalleServiciosActas> listadoDetalleServicios = acta.getDetalleServiciosActas();
                                Set<DetalleServiciosAdicionalesActas> listadoDetalleServiciosAdicionales = acta.getDetalleServiciosAdicionalesActas();

                                if(listadoDetalleMateriales.size()>0){
                                    //
                                    for(DetalleMaterialesActas detalleMaterial : listadoDetalleMateriales){
                                        if(contMateriales != 0){
                                            detalleMaterialesActa = detalleMaterialesActa + ",";
                                        }

                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterial.getMateriales().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterial.getMateriales().getDescripcion(), "");

                                        detalleMaterialesActa = detalleMaterialesActa + "{\n" +
                                                "  \"id\": "  + "\"" + detalleMaterial.getId().getMaterialId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"cantidadCubicada\": "   + detalleMaterial.getTotalUnidadesCubicadas() + ",\n" +
                                                "  \"precio\": "   + detalleMaterial.getMateriales().getPrecio() + ",\n" +
                                                "  \"montoTotalCubicado\": " + detalleMaterial.getTotalMontoCubicado()+ ",\n" +
                                                "  \"cantidadAcumuladaInformada\": "   + detalleMaterial.getTotalUnidadesEjecucionOt() + ",\n" +
                                                "  \"validar\": "   + false + ",\n" +
                                                "  \"cantidadActualInformada\": "  +  detalleMaterial.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                "  \"montoTotalActa\": " + detalleMaterial.getTotalMontoEjecucionActa() + "\n" +
                                                "}";
                                        contMateriales++;
                                    }

                                }
                                if(listadoDetalleMaterialesAdicionales.size()>0){
                                    //
                                    for(DetalleMaterialesAdicionalesActas detalleMaterialAdicional : listadoDetalleMaterialesAdicionales){
                                        if(contMaterialesAdicionales != 0){
                                            detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + ",";
                                        }
                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterialAdicional.getMateriales().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterialAdicional.getMateriales().getDescripcion(), "");

                                        detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + "{\n" +
                                                "  \"id\": "  + "\"" + detalleMaterialAdicional.getId().getMaterialId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"montoTotalCubicado\": " + detalleMaterialAdicional.getTotalMontoCubicado()+ ",\n" +
                                                "  \"cantidadCubicada\": "   + "0" + ",\n" +
                                                "  \"precio\": "   + detalleMaterialAdicional.getMateriales().getPrecio() + ",\n" +
                                                "  \"cantidadAcumuladaInformada\": "   + detalleMaterialAdicional.getTotalUnidadesEjecucionOt() + ",\n" +
                                                "  \"validar\": "   + false + ",\n" +
                                                "  \"cantidadActualInformada\": "  +  detalleMaterialAdicional.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                "  \"montoTotalActa\": " + detalleMaterialAdicional.getTotalMontoEjecucionActa() +
                                                "}";
                                        contMaterialesAdicionales++;
                                    }

                                }
                                if(listadoDetalleServicios.size()>0){
                                    //
                                    for(DetalleServiciosActas detalleServicios : listadoDetalleServicios){
                                        if(contServicios != 0){
                                            detalleServiciosActa = detalleServiciosActa + ",";
                                        }
                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServicios.getServicio().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServicios.getServicio().getDescripcion(), "");

                                        detalleServiciosActa = detalleServiciosActa + "{\n" +
                                                "  \"id\": "  + "\"" + detalleServicios.getId().getServicioId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"cantidadCubicada\": "   + detalleServicios.getTotalUnidadesCubicadas() + ",\n" +
                                                "  \"precio\": "   + detalleServicios.getServicio().getPrecio() + ",\n" +
                                                "  \"montoTotalCubicado\": " + detalleServicios.getTotalMontoCubicado()+ ",\n" +
                                                "  \"cantidadAcumuladaInformada\": "   + detalleServicios.getTotalUnidadesEjecucionOt() + ",\n" +
                                                "  \"validar\": "   + false + ",\n" +
                                                "  \"cantidadActualInformada\": "  +  detalleServicios.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                "  \"montoTotalActa\": " + detalleServicios.getTotalMontoEjecucionActa() + "\n" +
                                                "}";
                                        contServicios++;
                                    }

                                }
                                if(listadoDetalleServiciosAdicionales.size()>0){
                                    //
                                    for(DetalleServiciosAdicionalesActas detalleServiciosAdicionales : listadoDetalleServiciosAdicionales){
                                        if(contServiciosAdicionales != 0){
                                            detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + ",";
                                        }

                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServiciosAdicionales.getServicio().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServiciosAdicionales.getServicio().getDescripcion(), "");

                                        detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "{\n" +
                                                "  \"id\": "  + "\"" + detalleServiciosAdicionales.getId().getServicioId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"cantidadCubicada\": "   + detalleServiciosAdicionales.getServicio().getCantidadDefault() + ",\n" +
                                                "  \"precio\": "   + detalleServiciosAdicionales.getServicio().getPrecio() + ",\n" +
                                                "  \"montoTotalCubicado\": " + detalleServiciosAdicionales.getTotalMontoCubicado()+ ",\n" +
                                                "  \"cantidadAcumuladaInformada\": "   + detalleServiciosAdicionales.getTotalUnidadesEjecucionOt() + ",\n" +
                                                "  \"validar\": "   + false + ",\n" +
                                                "  \"cantidadActualInformada\": "  +  detalleServiciosAdicionales.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                "  \"montoTotalActa\": " + detalleServiciosAdicionales.getTotalMontoEjecucionActa() + "\n" +
                                                "}";
                                        contServiciosAdicionales++;
                                    }

                                }
                                detalleMaterialesActa = detalleMaterialesActa + "]";
                                detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + "]";
                                detalleServiciosActa = detalleServiciosActa + "]";
                                detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "]";

                                formularioActa = formularioActa + "\"detalleMaterialesActa\":" +detalleMaterialesActa + ",";
                                formularioActa = formularioActa + "\"detalleMaterialesAdicionalesActa\":" +detalleMaterialesAdicionalesActa + ",";
                                formularioActa = formularioActa + "\"detalleServiciosActa\":" +detalleServiciosActa + ",";
                                formularioActa = formularioActa + "\"detalleServiciosAdicionalesActa\":" +detalleServiciosAdicionalesActa;
                                formularioActa = formularioActa + "}";


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            parametros.put("formularioActa",formularioActa);
                            break;
                        case "form-val-acta.sbe" :

//                            String detalleServiciosActa = "[";
//                            String detalleServiciosAdicionalesActa = "[";
//                            String detalleMaterialesActa = "[";
//                            String detalleMaterialesAdicionalesActa = "[";
//                            int contMaterialesAdicionales = 0;
//                            int contServiciosAdicionales = 0;
                            try {

                                // obtener acta a validar
                                boolean montoPendiente = false;
                                Actas acta = new Actas();
                                try {
                                    acta = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otId));
                                    if((acta.getSaldoPendiente() != null) && (acta.getSaldoPendiente() != 0.0)){
                                        montoPendiente = true;
                                    }
                                } catch (Exception e){
                                    acta = null;
                                    e.printStackTrace();
                                }

                                String observaciones = acta.getObservaciones();
                                if(observaciones != null){
                                    observaciones = observaciones.replace("\n"," ");
                                    observaciones = observaciones.replace("\t"," ");
                                } else {
                                    observaciones = "Sin observaciones";
                                }

                                Date fechaCreacion = acta.getOt().getFechaCreacion();
                                Date fechaTerminoEstimada = acta.getOt().getFechaTerminoEstimada();
                                boolean validacionSistema = acta.getValidacionSistema();

                                formularioActa = formularioActa + "{  \n" +
                                        "  \"idActa\": "  + acta.getId()  + ",\n" +
                                        "  \"observaciones\": "   + observaciones + ",\n" +
                                        "  \"validacionSistema\": "  + "\"" + validacionSistema + "\"" + ",\n" +
                                        "  \"fechaCreacion\": "  + "\"" + fechaCreacion + "\"" + ",\n" +
                                        "  \"fechaTerminoEstimada\": "  + "\"" + fechaTerminoEstimada + "\"" + ",\n"+
                                        "  \"segundoPagoParcial\": "  + montoPendiente + ",\n";

                                Set<DetalleMaterialesActas> listadoDetalleMateriales = acta.getDetalleMaterialesActas();
                                Set<DetalleMaterialesAdicionalesActas> listadoDetalleMaterialesAdicionales = acta.getDetalleMaterialesAdicionalesActas();
                                Set<DetalleServiciosActas> listadoDetalleServicios = acta.getDetalleServiciosActas();
                                Set<DetalleServiciosAdicionalesActas> listadoDetalleServiciosAdicionales = acta.getDetalleServiciosAdicionalesActas();

                                if(listadoDetalleMateriales.size()>0){
                                    //
                                    for(DetalleMaterialesActas detalleMaterial : listadoDetalleMateriales){
                                        if(contMateriales != 0){
                                            detalleMaterialesActa = detalleMaterialesActa + ",";
                                        }
                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterial.getMateriales().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterial.getMateriales().getDescripcion(), "");

                                        detalleMaterialesActa = detalleMaterialesActa + "{\n" +
                                                "  \"id\": "  + "\"" + detalleMaterial.getId().getMaterialId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"cantidadCubicada\": "   + detalleMaterial.getTotalUnidadesCubicadas() + ",\n" +
                                                "  \"precio\": "   + detalleMaterial.getMateriales().getPrecio() + ",\n" +
                                                "  \"montoTotalCubicado\": " + detalleMaterial.getTotalMontoCubicado()+ ",\n" +
                                                "  \"cantidadAcumuladaInformada\": "   + detalleMaterial.getTotalUnidadesEjecucionOt() + ",\n" +
                                                "  \"validar\": "   + false + ",\n" +
                                                "  \"cantidadActualInformada\": "  +  detalleMaterial.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                "  \"montoTotalActa\": " + detalleMaterial.getTotalMontoEjecucionActa() + "\n" +
                                                "}";
                                        contMateriales++;
                                    }

                                }
                                if(listadoDetalleMaterialesAdicionales.size()>0){
                                    //
                                    for(DetalleMaterialesAdicionalesActas detalleMaterialAdicional : listadoDetalleMaterialesAdicionales){
                                        if(contMaterialesAdicionales != 0){
                                            detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + ",";
                                        }

                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterialAdicional.getMateriales().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterialAdicional.getMateriales().getDescripcion(), "");

                                        detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + "{\n" +
                                                "  \"id\": "  + "\"" + detalleMaterialAdicional.getId().getMaterialId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"montoTotalCubicado\": " + detalleMaterialAdicional.getTotalMontoCubicado()+ ",\n" +
                                                "  \"cantidadCubicada\": "   + "0" + ",\n" +
                                                "  \"precio\": "   + detalleMaterialAdicional.getMateriales().getPrecio() + ",\n" +
                                                "  \"cantidadAcumuladaInformada\": "   + detalleMaterialAdicional.getTotalUnidadesEjecucionOt() + ",\n" +
                                                "  \"validar\": "   + false + ",\n" +
                                                "  \"cantidadActualInformada\": "  +  detalleMaterialAdicional.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                "  \"montoTotalActa\": " + detalleMaterialAdicional.getTotalMontoEjecucionActa() +
                                                "}";
                                        contMaterialesAdicionales++;
                                    }

                                }
                                if(listadoDetalleServicios.size()>0){
                                    //
                                    for(DetalleServiciosActas detalleServicios : listadoDetalleServicios){
                                        if(contServicios != 0){
                                            detalleServiciosActa = detalleServiciosActa + ",";
                                        }

                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServicios.getServicio().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServicios.getServicio().getDescripcion(), "");

                                        detalleServiciosActa = detalleServiciosActa + "{\n" +
                                                "  \"id\": "  + "\"" + detalleServicios.getId().getServicioId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"cantidadCubicada\": "   + detalleServicios.getTotalUnidadesCubicadas() + ",\n" +
                                                "  \"precio\": "   + detalleServicios.getServicio().getPrecio() + ",\n" +
                                                "  \"montoTotalCubicado\": " + detalleServicios.getTotalMontoCubicado()+ ",\n" +
                                                "  \"cantidadAcumuladaInformada\": "   + detalleServicios.getTotalUnidadesEjecucionOt() + ",\n" +
                                                "  \"validar\": "   + false + ",\n" +
                                                "  \"cantidadActualInformada\": "  +  detalleServicios.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                "  \"montoTotalActa\": " + detalleServicios.getTotalMontoEjecucionActa() + "\n" +
                                                "}";
                                        contServicios++;
                                    }

                                }
                                if(listadoDetalleServiciosAdicionales.size()>0){
                                    //
                                    for(DetalleServiciosAdicionalesActas detalleServiciosAdicionales : listadoDetalleServiciosAdicionales){
                                        if(contServiciosAdicionales != 0){
                                            detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + ",";
                                        }

                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServiciosAdicionales.getServicio().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServiciosAdicionales.getServicio().getDescripcion(), "");

                                        detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "{\n" +
                                                "  \"id\": "  + "\"" + detalleServiciosAdicionales.getId().getServicioId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"cantidadCubicada\": "   + detalleServiciosAdicionales.getServicio().getCantidadDefault() + ",\n" +
                                                "  \"precio\": "   + detalleServiciosAdicionales.getServicio().getPrecio() + ",\n" +
                                                "  \"montoTotalCubicado\": " + detalleServiciosAdicionales.getTotalMontoCubicado()+ ",\n" +
                                                "  \"cantidadAcumuladaInformada\": "   + detalleServiciosAdicionales.getTotalUnidadesEjecucionOt() + ",\n" +
                                                "  \"validar\": "   + false + ",\n" +
                                                "  \"cantidadActualInformada\": "  +  detalleServiciosAdicionales.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                "  \"montoTotalActa\": " + detalleServiciosAdicionales.getTotalMontoEjecucionActa() + "\n" +
                                                "}";
                                        contServiciosAdicionales++;
                                    }

                                }
                                detalleMaterialesActa = detalleMaterialesActa + "]";
                                detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + "]";
                                detalleServiciosActa = detalleServiciosActa + "]";
                                detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "]";

                                formularioActa = formularioActa + "\"detalleMaterialesActa\":" +detalleMaterialesActa + ",";
                                formularioActa = formularioActa + "\"detalleMaterialesAdicionalesActa\":" +detalleMaterialesAdicionalesActa + ",";
                                formularioActa = formularioActa + "\"detalleServiciosActa\":" +detalleServiciosActa + ",";
                                formularioActa = formularioActa + "\"detalleServiciosAdicionalesActa\":" +detalleServiciosAdicionalesActa;
                                formularioActa = formularioActa + "}";


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            parametros.put("formularioActa",formularioActa);
                        break;
                        case "form-val-acta.llave" :

//                            String detalleServiciosActa = "[";
//                            String detalleServiciosAdicionalesActa = "[";
//                            String detalleMaterialesActa = "[";
//                            String detalleMaterialesAdicionalesActa = "[";
//                            int contMaterialesAdicionales = 0;
//                            int contServiciosAdicionales = 0;
                            try {

                                // obtener acta a validar
                                Actas acta = new Actas();
                                try {
                                    acta = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otId));
                                } catch (Exception e){
                                    acta = null;
                                    e.printStackTrace();
                                }

                                String observaciones = acta.getObservaciones();
                                if(observaciones != null){
                                    observaciones = observaciones.replace("\n"," ");
                                    observaciones = observaciones.replace("\t"," ");
                                } else {
                                    observaciones = "Sin observaciones";
                                }
                                Date fechaCreacion = acta.getOt().getFechaCreacion();
                                Date fechaTerminoEstimada = acta.getOt().getFechaTerminoEstimada();
                                boolean validacionSistema = acta.getValidacionSistema();

                                formularioActa = formularioActa + "{  \n" +
                                        "  \"idActa\": "  + acta.getId()  + ",\n" +
                                        "  \"observaciones\": "   + observaciones + ",\n" +
                                        "  \"validacionSistema\": "  + "\"" + validacionSistema + "\"" + ",\n" +
                                        "  \"fechaCreacion\": "  + "\"" + fechaCreacion + "\"" + ",\n" +
                                        "  \"fechaTerminoEstimada\": "  + "\"" + fechaTerminoEstimada + "\"" + ",\n" ;


                                Set<DetalleMaterialesActas> listadoDetalleMateriales = acta.getDetalleMaterialesActas();
                                Set<DetalleMaterialesAdicionalesActas> listadoDetalleMaterialesAdicionales = acta.getDetalleMaterialesAdicionalesActas();
                                Set<DetalleServiciosActas> listadoDetalleServicios = acta.getDetalleServiciosActas();
                                Set<DetalleServiciosAdicionalesActas> listadoDetalleServiciosAdicionales = acta.getDetalleServiciosAdicionalesActas();

                                if(listadoDetalleMateriales.size()>0){
                                    //
                                    for(DetalleMaterialesActas detalleMaterial : listadoDetalleMateriales){
                                        if(contMateriales != 0){
                                            detalleMaterialesActa = detalleMaterialesActa + ",";
                                        }
                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon( detalleMaterial.getMateriales().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterial.getMateriales().getDescripcion(), "");

                                        detalleMaterialesActa = detalleMaterialesActa + "{\n" +
                                                "  \"id\": "  + "\"" + detalleMaterial.getId().getMaterialId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" +nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"cantidadCubicada\": "   + detalleMaterial.getTotalUnidadesCubicadas() + ",\n" +
                                                "  \"precio\": "   + detalleMaterial.getMateriales().getPrecio() + ",\n" +
                                                "  \"montoTotalCubicado\": " + detalleMaterial.getTotalMontoCubicado()+ ",\n" +
                                                "  \"cantidadAcumuladaInformada\": "   + detalleMaterial.getTotalUnidadesEjecucionOt() + ",\n" +
                                                "  \"validar\": "   + false + ",\n" +
                                                "  \"cantidadActualInformada\": "  +  detalleMaterial.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                "  \"montoTotalActa\": " + detalleMaterial.getTotalMontoEjecucionActa() + "\n" +
                                                "}";
                                        contMateriales++;
                                    }

                                }
                                if(listadoDetalleMaterialesAdicionales.size()>0){
                                    //
                                    for(DetalleMaterialesAdicionalesActas detalleMaterialAdicional : listadoDetalleMaterialesAdicionales){
                                        if(contMaterialesAdicionales != 0){
                                            detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + ",";
                                        }

                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon( detalleMaterialAdicional.getMateriales().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleMaterialAdicional.getMateriales().getDescripcion(), "");

                                        detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + "{\n" +
                                                "  \"id\": "  + "\"" + detalleMaterialAdicional.getId().getMaterialId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"montoTotalCubicado\": " + detalleMaterialAdicional.getTotalMontoCubicado()+ ",\n" +
                                                "  \"cantidadCubicada\": "   + "0" + ",\n" +
                                                "  \"precio\": "   + detalleMaterialAdicional.getMateriales().getPrecio() + ",\n" +
                                                "  \"cantidadAcumuladaInformada\": "   + detalleMaterialAdicional.getTotalUnidadesEjecucionOt() + ",\n" +
                                                "  \"validar\": "   + false + ",\n" +
                                                "  \"cantidadActualInformada\": "  +  detalleMaterialAdicional.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                "  \"montoTotalActa\": " + detalleMaterialAdicional.getTotalMontoEjecucionActa() +
                                                "}";
                                        contMaterialesAdicionales++;
                                    }

                                }
                                if(listadoDetalleServicios.size()>0){
                                    //
                                    for(DetalleServiciosActas detalleServicios : listadoDetalleServicios){
                                        if(contServicios != 0){
                                            detalleServiciosActa = detalleServiciosActa + ",";
                                        }

                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon( detalleServicios.getServicio().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServicios.getServicio().getDescripcion(), "");

                                        detalleServiciosActa = detalleServiciosActa + "{\n" +
                                                "  \"id\": "  + "\"" + detalleServicios.getId().getServicioId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"cantidadCubicada\": "   + detalleServicios.getTotalUnidadesCubicadas() + ",\n" +
                                                "  \"precio\": "   + detalleServicios.getServicio().getPrecio() + ",\n" +
                                                "  \"montoTotalCubicado\": " + detalleServicios.getTotalMontoCubicado()+ ",\n" +
                                                "  \"cantidadAcumuladaInformada\": "   + detalleServicios.getTotalUnidadesEjecucionOt() + ",\n" +
                                                "  \"validar\": "   + false + ",\n" +
                                                "  \"cantidadActualInformada\": "  +  detalleServicios.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                "  \"montoTotalActa\": " + detalleServicios.getTotalMontoEjecucionActa() + "\n" +
                                                "}";
                                        contServicios++;
                                    }

                                }
                                if(listadoDetalleServiciosAdicionales.size()>0){
                                    //
                                    for(DetalleServiciosAdicionalesActas detalleServiciosAdicionales : listadoDetalleServiciosAdicionales){
                                        if(contServiciosAdicionales != 0){
                                            detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + ",";
                                        }

                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon( detalleServiciosAdicionales.getServicio().getNombre() , "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServiciosAdicionales.getServicio().getDescripcion(), "");

                                        detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "{\n" +
                                                "  \"id\": "  + "\"" + detalleServiciosAdicionales.getId().getServicioId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"cantidadCubicada\": "   + detalleServiciosAdicionales.getServicio().getCantidadDefault() + ",\n" +
                                                "  \"precio\": "   + detalleServiciosAdicionales.getServicio().getPrecio() + ",\n" +
                                                "  \"montoTotalCubicado\": " + detalleServiciosAdicionales.getTotalMontoCubicado()+ ",\n" +
                                                "  \"cantidadAcumuladaInformada\": "   + detalleServiciosAdicionales.getTotalUnidadesEjecucionOt() + ",\n" +
                                                "  \"validar\": "   + false + ",\n" +
                                                "  \"cantidadActualInformada\": "  +  detalleServiciosAdicionales.getTotalUnidadesEjecucionActa()  + ",\n" +
                                                "  \"montoTotalActa\": " + detalleServiciosAdicionales.getTotalMontoEjecucionActa() + "\n" +
                                                "}";
                                        contServiciosAdicionales++;
                                    }

                                }
                                detalleMaterialesActa = detalleMaterialesActa + "]";
                                detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + "]";
                                detalleServiciosActa = detalleServiciosActa + "]";
                                detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "]";

                                formularioActa = formularioActa + "\"detalleMaterialesActa\":" +detalleMaterialesActa + ",";
                                formularioActa = formularioActa + "\"detalleMaterialesAdicionalesActa\":" +detalleMaterialesAdicionalesActa + ",";
                                formularioActa = formularioActa + "\"detalleServiciosActa\":" +detalleServiciosActa + ",";
                                formularioActa = formularioActa + "\"detalleServiciosAdicionalesActa\":" +detalleServiciosAdicionalesActa;
                                formularioActa = formularioActa + "}";


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            parametros.put("formularioActa",formularioActa);
                            break;
                        case "formularioValidarActaOrd" :

                            String detalleMaterialesActaOrd = "[";
                            int contMaterialesValidaOr = 0;
                            try {

                                // obtener acta a validar

                                Actas acta = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otId));
                                String observaciones = "";
                                Date fechaCreacion = null;
                                Date fechaTerminoEstimada = null;
                                boolean validacionSistema = false;
                                if(acta != null) {
                                     observaciones = acta.getObservaciones();
                                     fechaCreacion = acta.getOt().getFechaCreacion();
                                     fechaTerminoEstimada = acta.getOt().getFechaTerminoEstimada();
                                     validacionSistema = acta.getValidacionSistema();
                                }
                                if(observaciones != null){
                                    observaciones = observaciones.replace("\n"," ");
                                    observaciones = observaciones.replace("\t"," ");
                                } else {
                                    observaciones = "Sin observaciones";
                                }
                                formularioActa = formularioActa + "{  \n" +
                                        "  \"idActa\": " + acta.getId() + ",\n" +
                                        "  \"observaciones\": " + observaciones + ",\n" +
                                        "  \"validacionSistema\": " + "\"" + validacionSistema + "\"" + ",\n" +
                                        "  \"fechaCreacion\": " + "\"" + fechaCreacion + "\"" + ",\n" +
                                        "  \"fechaTerminoEstimada\": " + "\"" + fechaTerminoEstimada + "\"" + ",\n";

                                List<DetalleItemsOrdinario> detalleItemsOrdinario = new ArrayList<DetalleItemsOrdinario>();
                                detalleItemsOrdinario = CubicadorDAO.getINSTANCE().obtenerCubicacionOrdinarioPorIdOtValidar(Integer.parseInt(otId), acta);
                                contadorTotalMateriales = detalleItemsOrdinario.size();
                                double montoValidadoSistema = 0.0;
                                if(contadorTotalMateriales>0){
                                    //
                                    for(DetalleItemsOrdinario material : detalleItemsOrdinario){

                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon( material.getCubicadorOrdinario().getItem() , "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(material.getCubicadorOrdinario().getItem(), "");

                                        detalleMaterialesActaOrd = detalleMaterialesActaOrd + "{\n" +
                                                "  \"id\": "  + "\"" + material.getCubicadorOrdinario().getId() + "\"" + ",\n" +
                                                "  \"nombre\": "  + "\"" + nombreServicio + "\"" + ",\n" +
                                                "  \"descripcion\": "  + "\"" + descripcionServicio + "\"" + ",\n" +
                                                "  \"cantidadCubicada\": "   + material.getCubicadorOrdinario().getCantidad() + ",\n" +
                                                "  \"precio\": "   + material.getCubicadorOrdinario().getPrecio() + ",\n" +
                                                "  \"montoTotalCubicado\": " + material.getCubicadorOrdinario().getTotal()+ ",\n" +
                                                "  \"cantidadAcumuladaInformada\": "   + material.getDetalleCubicadorOrdinarioActas().getTotalUnidadesEjecucionOt()  + ",\n" +
                                                "  \"cantidadActualInformada\": "  +  material.getDetalleCubicadorOrdinarioActas().getTotalUnidadesEjecucionActa()  + ",\n" +
                                                "  \"montoTotalActa\": " + (material.getDetalleCubicadorOrdinarioActas().getTotalUnidadesEjecucionActa() *  material.getCubicadorOrdinario().getPrecio())+ "\n" +
                                                "}";
                                        if(contMaterialesValidaOr < (contadorTotalMateriales - 1)){
                                            detalleMaterialesActaOrd = detalleMaterialesActaOrd + ",";
                                        }
                                        contMaterialesValidaOr++;
                                    }

                                }
                                detalleMaterialesActaOrd = detalleMaterialesActaOrd + "]";
                                formularioActa = formularioActa + "\"detalleMaterialesActa\":" +detalleMaterialesActaOrd + "";
                                formularioActa = formularioActa + "}";


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            parametros.put("formularioActa",formularioActa);
                            break;
                        case "formularioValidarActaUni" :
                            Map formularioValidaActaMap = new HashMap();
                            List detalleServiciosActaUniList = new ArrayList();
                            List detalleServiciosAdicioActaUniList = new ArrayList();
                            Map servicioMap = new HashMap();
                            Map servicioAdicionalMap = new HashMap();
                            int contMaterialesValidaUni = 0;
                            int contAdicionales = 0;
                            int contMaterialesValidaUniAdicionales = 0;
                            try {
                                // obtener acta a validar
                                Actas acta = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otId));
                                String observaciones = "";
                                Date fechaCreacion = null;
                                Date fechaTerminoEstimada = null;
                                boolean validacionSistema = false;
                                if(acta != null) {
                                    observaciones = acta.getObservaciones();
                                    fechaCreacion = acta.getOt().getFechaCreacion();
                                    fechaTerminoEstimada = acta.getOt().getFechaTerminoEstimada();
                                    validacionSistema = acta.getValidacionSistema();
                                }
                                if(observaciones != null){
                                    observaciones = observaciones.replace("\n"," ");
                                    observaciones = observaciones.replace("\t"," ");
                                } else {
                                    observaciones = "Sin observaciones";
                                }
                                formularioValidaActaMap.put("idActa", acta.getId());
                                formularioValidaActaMap.put("observaciones", observaciones);
                                formularioValidaActaMap.put("validacionSistema", validacionSistema);
                                formularioValidaActaMap.put("fechaCreacion", fechaCreacion);
                                formularioValidaActaMap.put("fechaTerminoEstimada", fechaTerminoEstimada);


                                List<DetalleServiciosUnificado> detalleServiciosUnificados = new ArrayList<DetalleServiciosUnificado>();
                                List<DetalleServiciosUnificado> detalleServiciosUnificadosAdicionales = new ArrayList<DetalleServiciosUnificado>();
                                detalleServiciosUnificados = CubicadorDAO.getINSTANCE().obtenerCubicacionUnificadaPorIdOtValidar(Integer.parseInt(otId), acta);
                                detalleServiciosUnificadosAdicionales = CubicadorDAO.getINSTANCE().obtenerCubicacionUnificadaAdicionalPorIdOtValidar(Integer.parseInt(otId), acta);
                                contadorTotalMateriales = detalleServiciosUnificados.size();
                                contMaterialesValidaUniAdicionales = detalleServiciosUnificadosAdicionales.size();
                                double montoValidadoSistema = 0.0;
                                if(contadorTotalMateriales>0){
                                    //
                                    for(DetalleServiciosUnificado detalleServiciosUnificado : detalleServiciosUnificados){
                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon( detalleServiciosUnificado.getCubicadorDetalle().getServicios().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServiciosUnificado.getCubicadorDetalle().getServicios().getDescripcion(), "");

                                        servicioMap = new HashMap();
                                        servicioMap.put("id", detalleServiciosUnificado.getCubicadorDetalle().getId());
                                        servicioMap.put("nombre", nombreServicio);
                                        servicioMap.put("descripcion", descripcionServicio);
                                        servicioMap.put("cantidadCubicada", detalleServiciosUnificado.getCubicadorDetalleUnificadoActas().getTotalUnidadesCubicadas());
                                        servicioMap.put("precio", detalleServiciosUnificado.getCubicadorDetalle().getPrecio());
                                        servicioMap.put("montoTotalCubicado", detalleServiciosUnificado.getCubicadorDetalleUnificadoActas().getTotalMontoCubicado());
                                        servicioMap.put("cantidadActualInformada", detalleServiciosUnificado.getCubicadorDetalleUnificadoActas().getTotalUnidadesEjecucionActa());
                                        servicioMap.put("montoTotalActa", detalleServiciosUnificado.getCubicadorDetalleUnificadoActas().getTotalMontoEjecucion());
                                        servicioMap.put("idRegion", detalleServiciosUnificado.getCubicadorDetalle().getRegionId());
                                        servicioMap.put("nombreRegion", detalleServiciosUnificado.getCubicadorDetalle().getRegiones().getNombre());
                                        servicioMap.put("idAgencia", detalleServiciosUnificado.getCubicadorDetalle().getAgenciaId());
                                        servicioMap.put("nombreAgencia", detalleServiciosUnificado.getCubicadorDetalle().getAgencia().getNombre());
                                        servicioMap.put("idCentral", detalleServiciosUnificado.getCubicadorDetalle().getCentralId());
                                        servicioMap.put("nombreCentral", detalleServiciosUnificado.getCubicadorDetalle().getCentrales().getNombre());

                                        detalleServiciosActaUniList.add(servicioMap);
                                    }

                                }
                                if(contMaterialesValidaUniAdicionales>0){
                                    //
                                    for(DetalleServiciosUnificado detalleServiciosUnificadoAdicionales : detalleServiciosUnificadosAdicionales){
                                        nombreServicio = formateaStringConCaracteresNoaceptadosEnJSon( detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getServicios().getNombre(), "");
                                        descripcionServicio = formateaStringConCaracteresNoaceptadosEnJSon(detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getServicios().getDescripcion(), "");

                                        servicioAdicionalMap = new HashMap();
                                        servicioAdicionalMap.put("id", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getId());
                                        servicioAdicionalMap.put("nombre", nombreServicio);
                                        servicioAdicionalMap.put("descripcion", descripcionServicio);
                                        servicioAdicionalMap.put("precio", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getPrecio());
                                        servicioAdicionalMap.put("cantidad", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getCantidad());
                                        servicioAdicionalMap.put("montoTotal", (detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getPrecio() * detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getCantidad()));
                                        servicioAdicionalMap.put("idRegion", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getRegionId());
                                        servicioAdicionalMap.put("nombreRegion", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getRegiones().getNombre());
                                        servicioAdicionalMap.put("idAgencia", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getAgenciaId());
                                        servicioAdicionalMap.put("nombreAgencia", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getAgencia().getNombre());
                                        servicioAdicionalMap.put("idCentral", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getCentralId());
                                        servicioAdicionalMap.put("nombreCentral", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getCentrales().getNombre());

                                        detalleServiciosAdicioActaUniList.add(servicioAdicionalMap);
                                    }

                                }

                                formularioValidaActaMap.put("detalleServicios", detalleServiciosActaUniList);
                                formularioValidaActaMap.put("detalleServiciosAdicionales", detalleServiciosAdicioActaUniList);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            parametros.put("formularioActa",formularioValidaActaMap);

                            break;
                        case "form-sel-pago" :
                            // Obtener monto a cancelar del acta
                            String formularioSelPago = "[";
                            Actas actaSelPago = new Actas();
                            List<Actas> actasAnterioresValidadas = new ArrayList<Actas>();
                            boolean tieneActasAnterioresValidas = false;
                            double montoActasAnteriores = 0;
                            double montoPagadoActasAnteriores = 0;
                            double montoSaldoPendiente = 0;
                            double montoPendientePagar = 0;
                            try {
                                actaSelPago = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otIdAux));
                                montoAPagar = actaSelPago.getMontoTotalActa();
                                actasAnterioresValidadas = CubicadorDAO.getINSTANCE().obtenerActasPorOtValidadas(Integer.parseInt(otIdAux));

                                if(actasAnterioresValidadas.size() > 0){

                                    tieneActasAnterioresValidas = true;

                                    for(Actas actaValida : actasAnterioresValidadas){

                                        montoActasAnteriores = montoActasAnteriores + actaValida.getMontoTotalActa();
                                        montoPagadoActasAnteriores = montoPagadoActasAnteriores + actaValida.getMontoTotalAPagar();

                                    }
                                }

                                montoPendientePagar = montoActasAnteriores - montoPagadoActasAnteriores;

                                montoSaldoPendiente = Math.abs(montoActasAnteriores - (montoPagadoActasAnteriores + montoAPagar));

                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            String moneda = "$";
                            if(contratoId == 10 || contratoId==11){
                                moneda = "UF";
                            }

                            if(tieneActasAnterioresValidas){

                                formularioSelPago = formularioSelPago + "{\n" +
                                        "  \"key\": \"montoCubicado\",\n" +
                                        "  \"defaultValue\": "  + actaSelPago.getMontoCubicado() + ",\n" +
                                        "  \"type\": \"horizontalSimpleText\",\n" +
                                        "  \"templateOptions\": {\n" +
                                        "    \"placeholder\": \"Monto original Cubicado\",\n" +
                                        "    \"formatter\": \"currency\",\n" +
                                        "    \"formatter2\": "  + "\"" + moneda + "\"" + ",\n" +
                                        "    \"label\": \"Monto original Cubicado\"\n" +
                                        "  }"+
                                        "  }," +
                                        "  {\n" +
                                        "  \"key\": \"montoActa\",\n" +
                                        "  \"defaultValue\": "  + actaSelPago.getMontoTotalActaCubicado() + ",\n" +
                                        "  \"type\": \"horizontalSimpleText\",\n" +
                                        "  \"templateOptions\": {\n" +
                                        "    \"placeholder\": \"Monto total Acta sin adicionales\",\n" +
                                        "    \"formatter\": \"currency\",\n" +
                                        "    \"formatter2\": "  + "\"" + moneda + "\"" + ",\n" +
                                        "    \"label\": \"Monto total acta sin adicionales\"\n" +
                                        "  }"+
                                        "  },"+
                                        "  {\n" +
                                        "  \"key\": \"montoAdicionales\",\n" +
                                        "  \"defaultValue\": "  + actaSelPago.getMontoTotalActaAdicional() + ",\n" +
                                        "  \"type\": \"horizontalSimpleText\",\n" +
                                        "  \"templateOptions\": {\n" +
                                        "    \"placeholder\": \"Monto total Acta adicionales\",\n" +
                                        "    \"formatter\": \"currency\",\n" +
                                        "    \"formatter2\": "  + "\"" + moneda + "\"" + ",\n" +
                                        "    \"label\": \"Monto total Acta adicionales\"\n" +
                                        "  }"+
                                        "  },"+
                                        "  {\n" +
                                        "  \"key\": \"montoTotalActaConAdicionales\",\n" +
                                        "  \"defaultValue\": "  + montoAPagar + ",\n" +
                                        "  \"type\": \"horizontalSimpleText\",\n" +
                                        "  \"templateOptions\": {\n" +
                                        "    \"placeholder\": \"Monto total Acta con adicionales\",\n" +
                                        "    \"formatter\": \"currency\",\n" +
                                        "    \"formatter2\": "  + "\"" + moneda + "\"" + ",\n" +
                                        "    \"label\": \"Monto total Acta con adicionales\"\n" +
                                        "  }"+
                                        "  },"+
                                        "  {\n" +
                                        "  \"key\": \"montoActasAnteriores\",\n" +
                                        "  \"defaultValue\": "  + montoActasAnteriores + ",\n" +
                                        "  \"type\": \"horizontalSimpleText\",\n" +
                                        "  \"templateOptions\": {\n" +
                                        "    \"placeholder\": \"Monto Actas Anteriores\",\n" +
                                        "    \"formatter\": \"currency\",\n" +
                                        "    \"formatter2\": "  + "\"" + moneda + "\"" + ",\n" +
                                        "    \"label\": \"Monto Actas Anteriores\"\n" +
                                        "  }"+
                                        "  },"+
                                        "  {\n" +
                                        "  \"key\": \"montoPagadoActasAnteriores\",\n" +
                                        "  \"defaultValue\": "  + montoPagadoActasAnteriores + ",\n" +
                                        "  \"type\": \"horizontalSimpleText\",\n" +
                                        "  \"templateOptions\": {\n" +
                                        "    \"placeholder\": \"Monto Pagado Actas Anteriores\",\n" +
                                        "    \"formatter\": \"currency\",\n" +
                                        "    \"formatter2\": "  + "\"" + moneda + "\"" + ",\n" +
                                        "    \"label\": \"Monto Pagado Actas Anteriores\"\n" +
                                        "  }"+
                                        "  },"+
                                        "  {\n" +
                                        "  \"key\": \"montoPendientePagar\",\n" +
                                        "  \"defaultValue\": "  + montoPendientePagar + ",\n" +
                                        "  \"type\": \"horizontalSimpleText\",\n" +
                                        "  \"templateOptions\": {\n" +
                                        "    \"placeholder\": \"Monto Pendiente a Pagar\",\n" +
                                        "    \"formatter\": \"currency\",\n" +
                                        "    \"formatter2\": "  + "\"" + moneda + "\"" + ",\n" +
                                        "    \"label\": \"Monto Pendiente a Pagar\"\n" +
                                        "  }"+
                                        "  },"+
                                        "  {\n" +
                                        "  \"key\": \"montoSaldoPendiente\",\n" +
                                        "  \"defaultValue\": "  + montoSaldoPendiente + ",\n" +
                                        "  \"type\": \"horizontalSimpleText\",\n" +
                                        "  \"templateOptions\": {\n" +
                                        "    \"placeholder\": \"Saldo Pendiente\",\n" +
                                        "    \"formatter\": \"currency\",\n" +
                                        "    \"formatter2\": "  + "\"" + moneda + "\"" + ",\n" +
                                        "    \"label\": \"Saldo Pendiente\"\n" +
                                        "  }"+
                                        "  },"+
                                        "  {\n" +
                                        "  \"key\": \"montoDiponiblePago\",\n" +
                                        "  \"defaultValue\": "  + montoSaldoPendiente + ",\n" +
                                        "  \"type\": \"horizontalSimpleText\",\n" +
                                        "  \"templateOptions\": {\n" +
                                        "    \"placeholder\": \"Monto a pagar según porcentaje\",\n" +
                                        "    \"formatter\": \"currency\",\n" +
                                        "    \"formatter2\": "  + "\"" + moneda + "\"" + ",\n" +
                                        "    \"label\": \"Monto a pagar según porcentaje\"\n" +
                                        "  }"+
                                        "  }"+
                                        "]";

                            }else{

                                formularioSelPago = formularioSelPago + "{\n" +
                                        "  \"key\": \"montoCubicado\",\n" +
                                        "  \"defaultValue\": "  + actaSelPago.getMontoCubicado() + ",\n" +
                                        "  \"type\": \"horizontalSimpleText\",\n" +
                                        "  \"templateOptions\": {\n" +
                                        "    \"placeholder\": \"Monto original Cubicado\",\n" +
                                        "    \"formatter\": \"currency\",\n" +
                                        "    \"formatter2\": "  + "\"" + moneda + "\"" + ",\n" +
                                        "    \"label\": \"Monto original Cubicado\"\n" +
                                        "  }"+
                                        "  }," +
                                        "  {\n" +
                                        "  \"key\": \"montoActa\",\n" +
                                        "  \"defaultValue\": "  + actaSelPago.getMontoTotalActaCubicado() + ",\n" +
                                        "  \"type\": \"horizontalSimpleText\",\n" +
                                        "  \"templateOptions\": {\n" +
                                        "    \"placeholder\": \"Monto total Acta sin adicionales\",\n" +
                                        "    \"formatter\": \"currency\",\n" +
                                        "    \"formatter2\": "  + "\"" + moneda + "\"" + ",\n" +
                                        "    \"label\": \"Monto total acta sin adicionales\"\n" +
                                        "  }"+
                                        "  },"+
                                        "  {\n" +
                                        "  \"key\": \"montoAdicionales\",\n" +
                                        "  \"defaultValue\": "  + actaSelPago.getMontoTotalActaAdicional() + ",\n" +
                                        "  \"type\": \"horizontalSimpleText\",\n" +
                                        "  \"templateOptions\": {\n" +
                                        "    \"placeholder\": \"Monto total Acta adicionales\",\n" +
                                        "    \"formatter\": \"currency\",\n" +
                                        "    \"formatter2\": "  + "\"" + moneda + "\"" + ",\n" +
                                        "    \"label\": \"Monto total Acta adicionales\"\n" +
                                        "  }"+
                                        "  },"+
                                        "  {\n" +
                                        "  \"key\": \"montoTotalActaConAdicionales\",\n" +
                                        "  \"defaultValue\": "  + montoAPagar + ",\n" +
                                        "  \"type\": \"horizontalSimpleText\",\n" +
                                        "  \"templateOptions\": {\n" +
                                        "    \"placeholder\": \"Monto total Acta con adicionales\",\n" +
                                        "    \"formatter\": \"currency\",\n" +
                                        "    \"formatter2\": "  + "\"" + moneda + "\"" + ",\n" +
                                        "    \"label\": \"Monto total Acta con adicionales\"\n" +
                                        "  }"+
                                        "  },"+
                                        "  {\n" +
                                        "  \"key\": \"montoDiponiblePago\",\n" +
                                        "  \"defaultValue\": "  + montoAPagar + ",\n" +
                                        "  \"type\": \"horizontalSimpleText\",\n" +
                                        "  \"templateOptions\": {\n" +
                                        "    \"placeholder\": \"Monto a pagar según porcentaje\",\n" +
                                        "    \"formatter\": \"currency\",\n" +
                                        "    \"formatter2\": "  + "\"" + moneda + "\"" + ",\n" +
                                        "    \"label\": \"Monto a pagar según porcentaje\"\n" +
                                        "  }"+
                                        "  }"+
                                        "]";

                            }

                            parametros.put("formulario",formularioSelPago);
                            break;
                        case "form-crear-ot-construccion" :
                            // Obtener monto a cancelar del acta
                            String formularioCrearOT = "[";
                            OtAux otDiseño = new OtAux();
                            try {
                                // obtener la ot completa
                                otDiseño = OtDAO.getINSTANCE().obtenerDetalleOtCompleto(Integer.parseInt(otId));
                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            parametros.put("formulario",otDiseño);
                            break;
                        case "form-crear-ot-full.sbe" :
                            // Obtener monto a cancelar del acta
                            OtAux otFull = new OtAux();
                            try {
                                // obtener la ot completa
                                otFull = OtDAO.getINSTANCE().obtenerDetalleOtCompleto(Integer.parseInt(otId));

                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            parametros.put("formulario",otFull);
                            break;
                        case "form-sel-pago.ord" :
                            // Obtener monto a cancelar del acta
                            String formularioSelPagoOrd = "[";
                            double montoCubicadoPago = 0.0;
                            try {
                                Actas acta = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otIdAux));
                                montoAPagar = acta.getMontoTotalActa();
                                montoCubicadoPago = acta.getMontoCubicado();

                            }catch (Exception e) {
                                e.printStackTrace();
                            }
                            formularioSelPagoOrd = formularioSelPagoOrd + "{\n" +
                                    "  \"key\": \"montoCubicado\",\n" +
                                    "  \"defaultValue\": "  + montoCubicadoPago + ",\n" +
                                    "  \"type\": \"horizontalSimpleText\",\n" +
                                    "  \"templateOptions\": {\n" +
                                    "    \"formatter\": \"currency\",\n" +
                                    "    \"label\": \"Monto Cubicado\"\n" +
                                    "  }"+
                                    "  },";
                            formularioSelPagoOrd = formularioSelPagoOrd + "{\n" +
                                    "  \"key\": \"montoActa\",\n" +
                                    "  \"defaultValue\": "  + montoAPagar + ",\n" +
                                    "  \"type\": \"horizontalSimpleText\",\n" +
                                    "  \"templateOptions\": {\n" +
                                    "    \"formatter\": \"currency\",\n" +
                                    "    \"label\": \"Monto Acta\"\n" +
                                    "  }"+
                                    "  }]";
                            parametros.put("formulario",formularioSelPagoOrd);
                            break;
                        case "form-sel-pago.uni" :
                            // Obtener monto a cancelar del acta
                            String formularioSelPagoUni = "[";
                            double montoCubicadoPagoUni = 0.0;
                            double montoTotalAdicionalesUni = 0.0;
                            try {
                                Actas acta = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otIdAux));
                                montoAPagar = acta.getMontoTotalActa();
                                montoCubicadoPagoUni = acta.getMontoCubicado();
                                montoTotalAdicionalesUni = acta.getMontoTotalActaAdicional();

                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            formularioSelPagoUni = formularioSelPagoUni + "{\n" +
                                    "  \"key\": \"montoCubicado\",\n" +
                                    "  \"defaultValue\": "  + montoCubicadoPagoUni + ",\n" +
                                    "  \"type\": \"horizontalSimpleText\",\n" +
                                    "  \"templateOptions\": {\n" +
                                    "    \"formatter\": \"currency\",\n" +
                                    "    \"label\": \"Monto Cubicado\"\n" +
                                    "  }"+
                                    "  },";
                            formularioSelPagoUni = formularioSelPagoUni + "{\n" +
                                    "  \"key\": \"montoAdicionales\",\n" +
                                    "  \"defaultValue\": "  + montoTotalAdicionalesUni + ",\n" +
                                    "  \"type\": \"horizontalSimpleText\",\n" +
                                    "  \"templateOptions\": {\n" +
                                    "    \"formatter\": \"currency\",\n" +
                                    "    \"label\": \"Monto Adicionales\"\n" +
                                    "  }"+
                                    "  },";
                            formularioSelPagoUni = formularioSelPagoUni + "{\n" +
                                    "  \"key\": \"montoActa\",\n" +
                                    "  \"defaultValue\": "  + montoAPagar + ",\n" +
                                    "  \"type\": \"horizontalSimpleText\",\n" +
                                    "  \"templateOptions\": {\n" +
                                    "    \"formatter\": \"currency\",\n" +
                                    "    \"label\": \"Monto Acta\"\n" +
                                    "  }"+
                                    "  }]";
                            parametros.put("formulario",formularioSelPagoUni);
                            break;
                        case "formularioAutorizarPago.654" :

                            String formularioAutorizarPago = "[";
                            double montoCubicado = 0;
                            double montoAdicional = 0;

                            try {
                                // Obtener los datos de la OT
                                Actas acta = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otId));
                                montoCubicado = acta.getMontoCubicado();
                                try{
                                    montoAdicional = acta.getMontoTotalActaAdicional();
                                } catch (Exception e){
                                    montoAdicional = 0.0;
                                }
                                montoAPagar = acta.getMontoTotalActa();

                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            formularioAutorizarPago = formularioAutorizarPago + "{\n" +
                                    "  \"key\": \"montoCubicado\",\n" +
                                    "  \"defaultValue\": "  + montoCubicado + ",\n" +
                                    "  \"type\": \"horizontalText\",\n" +
                                    "  \"templateOptions\": {\n" +
                                    "    \"placeholder\": \"Monto disponible Para Pago\",\n" +
                                    "    \"label\": \"Monto Cubicado\",\n" +
                                    "    \"currency\": true\n" +
                                    "  }"+
                                    "  },";
                            if(montoAdicional > 0.0) {
                                formularioAutorizarPago = formularioAutorizarPago + "{\n" +
                                        "  \"key\": \"montoAdicional\",\n" +
                                        "  \"defaultValue\": " + montoAdicional + ",\n" +
                                        "  \"type\": \"horizontalText\",\n" +
                                        "  \"templateOptions\": {\n" +
                                        "    \"placeholder\": \"Monto disponible Para Pago\",\n" +
                                        "    \"label\": \"Monto Adicional\",\n" +
                                        "    \"currency\": true\n" +
                                        "  }" +
                                        "  },";
                            }
                            formularioAutorizarPago = formularioAutorizarPago + "{\n" +
                                    "  \"key\": \"montoAPagar\",\n" +
                                    "  \"defaultValue\": "  + montoAPagar + ",\n" +
                                    "  \"type\": \"horizontalText\",\n" +
                                    "  \"templateOptions\": {\n" +
                                    "    \"placeholder\": \"Monto disponible Para Pago\",\n" +
                                    "    \"label\": \"Monto a Pagar\",\n" +
                                    "    \"currency\": true\n" +
                                    "  }"+
                                    "  },";
                            formularioAutorizarPago = formularioAutorizarPago + "{\n" +
                                    "  \"key\": \"comentario\",\n" +
                                    "  \"type\": \"horizontalTextarea\",\n" +
                                    "  \"templateOptions\": {\n" +
                                    "    \"placeholder\": \"Comentario\",\n" +
                                    "    \"label\": \"Comentario\",\n" +
                                    "    \"required\": " + true +
                                    "  }"+
                                    "  }]";


                            parametros.put("formulario",formularioAutorizarPago);

                            break;
                        case "formularioAutorizarPago" :
                            Map mapaformActa = new HashMap();
                            try {
                                mapaformActa = CubicadorDAO.getINSTANCE().obtieneDetalleMaterialesServicios(Integer.parseInt(otId), login);
                            }catch (Exception e) {
                                e.printStackTrace();
                            }
                            parametros.put("formularioActa",mapaformActa);
                            break;
                        case "form-val-pmo" :
                            String formularioPmo = "";
                            try {
                                // Obtener el detalle de la OT
                                OtAux otRetorno = OtDAO.getINSTANCE().obtenerDetalleOtAux(Integer.parseInt(otId), login);

                                String detalleOt = otRetorno.getJsonDetalleOt();
                                Actas acta = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otId));
                                String observaciones = acta.getObservaciones();
                                if(observaciones != null){
                                    observaciones = observaciones.replace("\n"," ");
                                    observaciones = observaciones.replace("\t"," ");
                                } else {
                                    observaciones = "Sin observaciones";
                                }
                                Date fechaCreacion = acta.getOt().getFechaCreacion();
                                Date fechaTerminoEstimada = acta.getOt().getFechaTerminoEstimada();
                                boolean validacionSistema = acta.getValidacionSistema();

                                Contratos contrato = otRetorno.getContrato();
                                Proveedores proveedor = otRetorno.getProveedor();
                                String nombreProveedor = proveedor.getNombre();
                                Long numeroContrato = Long.parseLong("0");

                                if(contrato.getId() != 2 && contrato.getId() != 5){
                                    numeroContrato = ProveedorDAO.getInstance().obtenerCodigoAcuerdoPorProveedor(contrato.getId(), proveedor.getId());
                                }

                                String  fechaCreacionOt = obtienefechaSegunFormato(otRetorno.getFechaCreacion(), "dd-MM-yyyy");
                                String  fechaInicioOt = obtienefechaSegunFormato(otRetorno.getFechaInicioEstimada(), "dd-MM-yyyy");
                                String  fechaTerminoOt = obtienefechaSegunFormato(otRetorno.getFechaTerminoEstimada(), "dd-MM-yyyy");
                                String nombreContrato = revisaObjetoNulo(otRetorno.getContrato().getNombre());
                                Usuarios gestor = otRetorno.getGestor();

                                formularioPmo = formularioPmo + "{  \n" +
                                        "  \"idActa\": "  + acta.getId()  + ",\n" +
                                        "  \"montoTotalActa\": "  + acta.getMontoTotalActa()  + ",\n" +
                                        "  \"detalleOt\": "  + detalleOt  + ",\n"+
                                        "  \"proveedor\": "  + "\""   + nombreProveedor  + "\"" +",\n"+
                                        "  \"numeroContrato\": " + numeroContrato + ",\n" +
                                        "  \"gestor\": " + "\"" + gestor.getNombres() + " " + gestor.getApellidos() + "\"" + ",\n" +
                                        "  \"fechaCreacionOt\": " + "\"" + fechaCreacionOt  + "\"" +",\n"+
                                        "  \"fechaInicioOt\": " + "\"" + fechaInicioOt  + "\"" +",\n"+
                                        "  \"fechaTerminoOt\": " + "\"" + fechaTerminoOt  + "\"" +",\n"+
                                        "  \"nombreContrato\": " + "\"" + nombreContrato  + "\"" +"\n";

                                formularioPmo = formularioPmo + "}";

                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            parametros.put("formulario",formularioPmo);
                            break;
                        case "form-val-imp" :
                            String formularioImputacion = "";
                            try {
                                Ot ot = OtDAO.getINSTANCE().obtenerDetalleOt(Integer.parseInt(otId));
                                String detalleOt = ot.getJsonDetalleOt();

                                Actas acta = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otId));


                                String observaciones = acta.getObservaciones();
                                if(observaciones != null){
                                    observaciones = observaciones.replace("\n"," ");
                                    observaciones = observaciones.replace("\t"," ");
                                } else {
                                    observaciones = "Sin observaciones";
                                }
                                Date fechaCreacion = acta.getOt().getFechaCreacion();
                                Date fechaTerminoEstimada = acta.getOt().getFechaTerminoEstimada();
                                boolean validacionSistema = acta.getValidacionSistema();

                                formularioImputacion = formularioImputacion + "{  \n" +
                                        "  \"idActa\": "  + acta.getId()  + ",\n" +
                                        "  \"detalleOt\": "  + detalleOt  + "\n" ;

                                formularioImputacion = formularioImputacion + "}";
                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            parametros.put("formulario",formularioImputacion);
                            break;
                        case "modalFormularioValidaOt" :
                            String formularioValidaOt = "";
                            String decisionesValidaOT = "";
                            String montoTotal = "";
                            Ot ot = new Ot();
                            TipoMoneda tipoMoneda = new TipoMoneda();
                            String tipoMonedaIcon = "$";
                            try{
                                ot =  OtDAO.getINSTANCE().obtenerDetalleOt(Integer.parseInt(otId));
                                montoTotal  = CubicadorDAO.getINSTANCE().obtenerMontoCubicadorPorIdOt(ot.getId());
                                tipoMoneda = CubicadorDAO.getINSTANCE().obtieneTipoMonedaPorOt(ot.getId());
                                switch (tipoMoneda.getId()){
                                    case 1:
                                        tipoMonedaIcon = "USD$";
                                        break;
                                    case 3:
                                        tipoMonedaIcon = "UF";
                                        break;
                                    case 4:
                                        tipoMonedaIcon = "€";
                                        break;
                                }
                            } catch (Exception e){
                                log.error("******** Error al obtener monto para validar OT *******");
                                log.error("******** Error :" + e.toString());
                            }

                            decisionesValidaOT = "{\n" +
                                    "  \"id\": "  + "\"" + 0 + "\"" + ",\n" +
                                    "  \"opcion\": "  + "\"  Aceptar OT  \"" + ",\n" +
                                    "  \"eventoId\": "  +   32  + ",\n" +
                                    "  \"opcionValidacion\": "  + true + ",\n" +
                                    "  \"tituloBoton\": "  + "\"Aceptar\"" + "\n" +
                                    "},";
                            decisionesValidaOT = decisionesValidaOT + "{\n" +
                                    "  \"id\": "  + "\"" + 0 + "\"" + ",\n" +
                                    "  \"opcion\": "  + "\"  Rechazar OT  \"" + ",\n" +
                                    "  \"eventoId\": "  +   33  + ",\n" +
                                    "  \"opcionValidacion\": "  + true + ",\n" +
                                    "  \"tituloBoton\": "  + "\"Rechazar\"" + "\n" +
                                    "}";

                            String modalValidarOT = "{\n" +
                                    "  \"tituloModal\": \"Validar Orden de Trabajo\",\n" +
                                    "  \"modalSize\": \"lg\",\n" +
                                    "  \"servicio\": \"obtenerAccionesFrontendPorEvento\",\n" +
                                    "  \"label\": \"Se ha generado la orden de trabajo número "+ot.getId()+" por el monto "+tipoMonedaIcon+" "+montoTotal+" Desea aprobar esta OT?\",\n" +
                                    "  \"botones\": [\n" +  decisionesValidaOT  + "  ]\n" +
                                    "}";
                            try {

                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            parametros.put("modal",modalValidarOT);
                            break;
                        case "modalFormularioSolicitudTrabajo" :
                            String decisionesSolicitudTrabajo = "";
                            String modalSolicitudTrabajo = "";
                            try{


                            } catch (Exception e){
                                log.error("******** Error al obtener monto para validar OT *******");
                                log.error("******** Error :" + e.toString());
                            }

                            decisionesSolicitudTrabajo = "{\n" +
                                    "  \"id\": "  + "\"" + 0 + "\"" + ",\n" +
                                    "  \"opcion\": "  + "\"  Aceptar Solicitud de Trabajo \"" + ",\n" +
                                    "  \"eventoId\": "  +   46  + ",\n" +
                                    "  \"opcionValidacion\": "  + true + ",\n" +
                                    "  \"tituloBoton\": "  + "\"Aceptar\"" + "\n" +
                                    "},";
                            decisionesSolicitudTrabajo = decisionesSolicitudTrabajo + "{\n" +
                                    "  \"id\": "  + "\"" + 0 + "\"" + ",\n" +
                                    "  \"opcion\": "  + "\"  Rechazar Solicitud de Trabajo  \"" + ",\n" +
                                    "  \"eventoId\": "  +   47  + ",\n" +
                                    "  \"opcionValidacion\": "  + true + ",\n" +
                                    "  \"tituloBoton\": "  + "\"Rechazar\"" + "\n" +
                                    "}";

                            modalSolicitudTrabajo = "{\n" +
                                    "  \"tituloModal\": \"Nueva solicitud de trabajo\",\n" +
                                    "  \"modalSize\": \"lg\",\n" +
                                    "  \"servicio\": \"obtenerAccionesFrontendPorEvento\",\n" +
                                    "  \"label\": \"Se ha generado una nueva solicitud de trabajo nº "+otId+". ¿Desea aceptar esta solicitud?\",\n" +
                                    "  \"botones\": [\n" +  decisionesSolicitudTrabajo  + "  ]\n" +
                                    "}";
                            try {

                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            parametros.put("modal",modalSolicitudTrabajo);
                            break;
                        case "formularioCambioProveedor" :
                            Cubicador cubicador = new Cubicador();
                            Map formularioCambioProveedor = new HashMap();
                            List listaProveedores = new ArrayList();
                            Map proveedorRechazo = new HashMap();
                            Map proveedoresDisponibles = new HashMap();
                            List<Proveedores> lista = new ArrayList<Proveedores>();
                            String coma = "";
                            List<CubicadorServicios> cubicadorServicios = new ArrayList<CubicadorServicios>();
                            List<CubicadorDetalle> cubicadorDetalles = new ArrayList<CubicadorDetalle>();
                            List<CubicadorOrdinario> cubicadorOrdinarios = new ArrayList<CubicadorOrdinario>();
                            int j = 0;
                            try {
                                cubicador = CubicadorDAO.getINSTANCE().obtenerDetalleCubicacionPorOt(Integer.parseInt(otId));
                                if(cubicador.getContrato().getId() == 4){
                                    lista = GenericDAO.getINSTANCE().listarProveedoresPorContrato(cubicador.getContrato().getId());
                                } else {
                                lista = GenericDAO.getINSTANCE().obtenerListadoProveedoresPorRegion(cubicador.getRegion().getId(), cubicador.getContrato().getId());
                                }
                            }catch (Exception e) {
                                e.printStackTrace();
                            }
                            for(int i = 0; i < lista.size(); i++){
                                if(cubicador.getProveedor().getId() == lista.get(i).getId()){
                                    proveedorRechazo.put("id", lista.get(i).getId());
                                    proveedorRechazo.put("nombre", lista.get(i).getNombre());
                                } else {
                                    proveedoresDisponibles = new HashMap();
                                    proveedoresDisponibles.put("id", lista.get(i).getId());
                                    proveedoresDisponibles.put("nombreProveedor", lista.get(i).getNombre());
                                    listaProveedores.add(proveedoresDisponibles);

                                    j = j + 1;
                                }
                            }
                            formularioCambioProveedor = CubicadorDAO.getINSTANCE().obtieneJsonCubicadorConListProveedores(cubicador, listaProveedores, proveedorRechazo, Integer.parseInt(eventoId));

                            parametros.put("formulario",formularioCambioProveedor);
                            break;
                        case "formularioCheckList" :
                            CheckList checkList = new CheckList();
                            Ot otCheckLIst = new Ot();
                            String json = "";
                            try{
                                otCheckLIst =  OtDAO.getINSTANCE().obtenerDetalleOt(Integer.parseInt(otId));
                                checkList = GenericDAO.getINSTANCE().obtenerCheckList(otCheckLIst.getContrato().getId());
                                json = checkList.getJsonCheckList();
                            }catch (Exception e){
                                throw e;
                            }

                            parametros.put("formulario",json);

                            break;
                        case "formularioCheckListSeleccionados" :

                            ObjectMapper mapperCheck = new ObjectMapper();
                            Map mapaParamsCheck = mapperCheck.readValue(parametrosEvento, new TypeReference<HashMap<String,Object>>() {
                            });

                            CheckListOt checkListOt = new CheckListOt();
                            Usuarios usuario = new Usuarios();
                            usuario.setId(usuarioId);
                            checkListOt.setUsuario(usuario);
                            checkListOt.setCheckListDetalleOt((String)mapaParamsCheck.get("checkListString"));
                            Ot otCheckList = new Ot();
                            int otIdCheck = Integer.parseInt(mapaParamsCheck.get("otId").toString());
                            otCheckList.setId(otIdCheck);
                            checkListOt.setOt(otCheckList);
                            checkListOt.setFechaCreacion(new Date());
                            checkListOt.setEmpresacolaboradora(true);
                            String observaciones = mapaParamsCheck.get("observaciones").toString();
                            if(observaciones != null){
                                observaciones = observaciones.replace("\n"," ");
                                observaciones = observaciones.replace("\t"," ");
                            } else {
                                observaciones = "Sin observaciones";
                            }
                            checkListOt.setObservacion(observaciones);

                            int idCheckListOT = GenericDAO.getINSTANCE().registrarCheckListOt(checkListOt);
                            checkListOt.setId(idCheckListOT);

                            int puntuacionFinal = 0;
                            List<CheckListOtDetalleAux> checkListOtDetalleAuxList = new ArrayList<>();
                            if(mapaParamsCheck.containsKey("checkList")){

                                ArrayList<LinkedHashMap> list =  (ArrayList<LinkedHashMap>) mapaParamsCheck.get("checkList");
                                Iterator iterator = list.iterator();

                                int checkListDetelleId = 0;

                                while(iterator.hasNext()) {
                                    LinkedHashMap item = (LinkedHashMap)iterator.next();

                                    String nombreItem = item.get("item").toString();
                                    String observacionItem = item.get("observacion").toString();
                                    Map opcionItem = (Map)item.get("opcion");
                                    int idOpcion = Integer.parseInt(opcionItem.get("id").toString());
                                    String nombreOpcion = opcionItem.get("nombre").toString();
                                    int puntuacion = Integer.parseInt(item.get("puntuacion").toString());
                                    CheckListOtDetalle checkListOtDetalle = new CheckListOtDetalle();
                                    CheckListOtDetalleAux checkListOtDetalleAux = new CheckListOtDetalleAux();
                                    checkListOtDetalle.setCheckListOt(checkListOt);
                                    checkListOtDetalleAux.setCheckListOt(checkListOt);
                                    checkListOtDetalle.setItem(nombreItem);
                                    checkListOtDetalleAux.setItem(nombreItem);
                                    checkListOtDetalle.setObservacion(observacionItem);
                                    checkListOtDetalleAux.setObservacion(observacionItem);
                                    checkListOtDetalle.setOpcionId(idOpcion);
                                    checkListOtDetalleAux.setOpcionId(idOpcion);
                                    checkListOtDetalle.setOpcionNombre(nombreOpcion);
                                    checkListOtDetalleAux.setOpcionNombre(nombreOpcion);
                                    checkListOtDetalle.setPuntuacion(puntuacion);
                                    int idCheckListOtDetalle = GenericDAO.getINSTANCE().registrarCheckListOtDetalle(checkListOtDetalle);
                                    checkListOtDetalle.setId(idCheckListOtDetalle);
                                    checkListOtDetalleAux.setId(idCheckListOtDetalle);
                                    if(idOpcion == 1){
                                        puntuacionFinal = puntuacionFinal + puntuacion;
                                        checkListOtDetalleAuxList.add(checkListOtDetalleAux);
                                    }

                                }

                            }

                            GenericDAO.getINSTANCE().actualizarPuntuacionCheckListOt(idCheckListOT, puntuacionFinal);

                            CheckListOtAux checkListOtAux = new CheckListOtAux();
                            checkListOtAux.setId(checkListOt.getId());
                            checkListOtAux.setObservacion(checkListOt.getObservacion());
                            checkListOtAux.setEmpresacolaboradora(checkListOt.isEmpresacolaboradora());
                            checkListOtAux.setFechaCreacion(checkListOt.getFechaCreacion());
                            checkListOtAux.setCheckListOtDetalle(checkListOtDetalleAuxList);
                            checkListOtAux.setOt(otCheckList);


//                            parametros.put("formulario",checkListOtDetalleList);
                            parametros.put("formulario",checkListOtAux);

                            break;
                        case "form-agrega-ser-diseno-ap" :

                            Map mapaServiciosDiseno = new HashMap();
                                try{
                                    mapaServiciosDiseno = GenericDAO.getINSTANCE().obtieneServiciosDiseno(Integer.parseInt(otId));
                                    parametros.put("formulario",mapaServiciosDiseno);
                                }catch(Exception e){
                                    throw e;
                                }

                            break;
                        case "formularioActaBucle":
                            Actas ultimaActa = new Actas();
                            Map mapFormActaBucle = new HashMap();
                            ultimaActa.setId(0);

                            List<Cubicador> cubicaciones = CubicadorDAO.getINSTANCE().obtenerCubicacionPorIdOt(Integer.parseInt(otId));
                            Cubicador cubicacion = cubicaciones.get(0);
                            try {
                                ultimaActa = CubicadorDAO.getINSTANCE().obtenerActaParaValidar(Integer.parseInt(otId));
                                observaciones = ultimaActa.getObservaciones();
                                if(observaciones != null){
                                    observaciones = observaciones.replace("\n"," ");
                                    observaciones = observaciones.replace("\t"," ");
                                } else {
                                    observaciones = "Sin observaciones";
                                }
                                Date fechaCreacion = ultimaActa.getOt().getFechaCreacion();
                                Date fechaTerminoEstimada = ultimaActa.getOt().getFechaTerminoEstimada();
                                boolean validacionSistema = ultimaActa.getValidacionSistema();

                                mapFormActaBucle.put("idActa", ultimaActa.getId());
                                mapFormActaBucle.put("observaciones",observaciones);
                                mapFormActaBucle.put("validacionSistema", validacionSistema);
                                mapFormActaBucle.put("fechaCreacion", fechaCreacion);
                                mapFormActaBucle.put("fechaTerminoEstimada", fechaTerminoEstimada);
                            }catch (Exception e) {
                                e.printStackTrace();
                            }
                            if(ultimaActa.getId() == 0){
                                mapFormActaBucle = CubicadorDAO.getINSTANCE().obtieneDetalleBucle(cubicacion.getId(), mapFormActaBucle);
                                parametros.put("formularioActa", mapFormActaBucle);
                            } else {
                                parametros = GenericDAO.getINSTANCE().obtieneDetalleActaBucle();
                            }

                            break;

                    }
                }
                mapAccion.put("parametros", parametros);
            }
            retorno.add(mapAccion);
        }
        return retorno;
    }
    private boolean verificaUsuarioEvento (int idEvento, int usuarioId, int otId) throws Exception{
        boolean respuesta = false;
        List<UsuariosOt> lista = GenericDAO.getINSTANCE().obtenerUsuariosOt(otId, idEvento);

        for(UsuariosOt usuariosOt : lista){
            if(usuariosOt.getUsuarios().getId() == usuarioId){
                respuesta = true;
            }
        }
        if(respuesta){
            return  respuesta;
        } else {
            List<WorkflowEventosEjecucion> listaWe = GenericDAO.getINSTANCE().obtenerUsuariosOtWe(otId, idEvento);
            for(WorkflowEventosEjecucion usuariosOtWe : listaWe){
                if(usuariosOtWe.getUsuarioId() == usuarioId){
                    respuesta = true;
                }
            }
            if(respuesta){
                return respuesta;
            } else {
                throw new Exception("Error, Usuario ejecutor no corresponde al evento ejecutado, se recomienda actualizar la pagina y volver a intentar");
            }
        }
    }

    private String armarJsonRespuestaUsuario(List<Usuarios> listadoUsuarios) {
        String usuarios = "";

        int contUsuarios = 0;
        for(Usuarios usuarioAux : listadoUsuarios){
            if(contUsuarios != 0){
                usuarios = usuarios + ",";
            }
            usuarios = usuarios + "{\n" +
                    "  \"id\": "  + "\"" + usuarioAux.getId() + "\"" + ",\n" +
                    "  \"nombre\": "  + "\"" + usuarioAux.getNombres() +" "+ usuarioAux.getApellidos() + "\"" + "\n" +
                    "}";
            contUsuarios++;
        }
        usuarios = usuarios + "]";

        return usuarios;
    }
    private String obtienefechaSegunFormato(Date fecha, String formatoSalida) {
        String fechaString = "";
        if (fecha == null) {
            return fechaString;
        } else {
            try {
                DateFormat formato = new SimpleDateFormat(formatoSalida);
                fechaString = formato.format(fecha);
                //System.out.println("====================>  Fecha de salida :  "+fechaString);
                return fechaString;
            } catch (Exception e) {
                fechaString = "error: "+e;
                return fechaString;
            }
        }
    }
    private String formateaStringConCaracteresNoaceptadosEnJSon(String entrada, String remplazo)throws Exception{
        String respuesta = "";
        try{
            if(entrada != null){
                if(remplazo.equals("")){
                    respuesta = entrada.replace("\n"," ");
                    respuesta = respuesta.replace("\t"," ");
                    respuesta = respuesta.replace("\"", "''");
                } else {
                    respuesta = entrada.replace("\"", " "+remplazo+" ");
                    respuesta = respuesta.replace("\n"," ");
                    respuesta = respuesta.replace("\t"," ");
                }
            }
            return respuesta;
        }catch(Exception e){
            throw e;
        }
    }
    private String revisaObjetoNulo(String objeto) {
        String salida = "";
        if (objeto == null) {
            return salida;
        } else {
           salida = objeto.toString();
            return objeto;
        }
    }

    public String getEventoId() {
        return eventoId;
    }

    public void setEventoId(String eventoId) {
        this.eventoId = eventoId;
    }



    private static final Logger log = Logger.getLogger(ObtenerAccionesFrontendPorEventoAction.class);

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }



    public String getOtId() {
        return otId;
    }

    public void setOtId(String otId) {
        this.otId = otId;
    }

    public String getProveedorId() {
        return proveedorId;
    }

    public void setProveedorId(String proveedorId) {
        this.proveedorId = proveedorId;
    }

    public String getOtIdAux() {
        return otIdAux;
    }

    public void setOtIdAux(String otIdAux) {
        this.otIdAux = otIdAux;
    }

    public int getContratoId() {
        return contratoId;
    }

    public void setContratoId(int contratoId) {
        this.contratoId = contratoId;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getCubicadorId() {
        return cubicadorId;
    }

    public void setCubicadorId(int cubicadorId) {
        this.cubicadorId = cubicadorId;
    }

    public String getOtIdAux2() {
        return otIdAux2;
    }

    public void setOtIdAux2(String otIdAux2) {
        this.otIdAux2 = otIdAux2;
    }

    public String getParametrosEvento() {
        return parametrosEvento;
    }

    public void setParametrosEvento(String parametrosEvento) {
        this.parametrosEvento = parametrosEvento;
    }
}
