/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import org.jboss.logging.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author Christian
 */
public class ObtenerAsBuiltAction extends ActionSupport {
    

    private int idProyecto;
    private String tipoAsBuilt;
    private String retorno;

    
    public String execute() throws Exception {

        String cadenaRetorno = "";
        try {

            String urlServicio = "http://10.232.139.111/api_asbuilts/api/";

            switch (tipoAsBuilt) {
                case "cercoAgricola":
                    urlServicio = urlServicio + "infra_cercoagricola/";
                    break;
                case "cercoPerimetral":
                    urlServicio = urlServicio + "infra_cercoperimetral/";
                    break;
                case "escalerilla":
                    urlServicio = urlServicio + "infra_escalerilla/";
                    break;
                case "torres":
                    urlServicio = urlServicio + "infra_torre/";
                    break;
                default:
                    throw new Exception("Tipo AsBuilt no encontrado");
            }

            urlServicio = urlServicio + idProyecto;

            URL url = new URL(urlServicio);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
                setRetorno(output);
            }

            conn.disconnect();


            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al obtener combos asbuilt. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(ObtenerAsBuiltAction.class);



    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getTipoAsBuilt() {
        return tipoAsBuilt;
    }

    public void setTipoAsBuilt(String tipoAsBuilt) {
        this.tipoAsBuilt = tipoAsBuilt;
    }
}
