/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.type.TypeReference;

import java.util.*;

/**
 *
 * @author Christian
 */
public class ObtenerCheckListOtAction extends ActionSupport {

    private int idCheckListOt;
    private String retorno;
    private int usuarioId;


    public String execute() throws Exception {
        final String token = ServletActionContext.getRequest().getParameter("tk");
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(token,usuarioId,true);
        String cadenaRetorno = "";
        try {
            Login login = LoginLogoutDAO.getINSTANCE().retornarLoginSesionActual(token);

            List<CheckListOtDetalle> checkListOtDetalleList = GenericDAO.getINSTANCE().obtenerCheckListOtDetalle(idCheckListOt);

            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(checkListOtDetalleList));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            cadenaRetorno = "error";
            getLog().error("Error al obtener el detalle de la OT. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static Logger log = Logger.getLogger(ObtenerCheckListOtAction.class);

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public static Logger getLog() {
        return log;
    }

    public static void setLog(Logger aLog) {
        log = aLog;
    }


    public int getIdCheckListOt() {
        return idCheckListOt;
    }

    public void setIdCheckListOt(int idCheckListOt) {
        this.idCheckListOt = idCheckListOt;
    }
}
