/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.jboss.logging.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Christian
 */
public class ObtenerCodigoProyectoAsBuiltAction extends ActionSupport {
    

    private String parametros;
    private boolean infraestructura;
    private boolean energia;
    private boolean transmision;
    private boolean implementacion;
    private String retorno;

    
    public String execute() throws Exception {

        String cadenaRetorno = "";
        try {

            String urlServicio = "http://10.232.139.111/api_asbuilts/api/Cod_proyecto";

            String body = "";

            ObjectMapper mapper = new ObjectMapper();
            Map map = mapper.readValue(parametros, new TypeReference<HashMap<String, String>>() {
            });


            List<String> listadoTiposAsbuilt = new ArrayList<>();
            if(infraestructura){
                listadoTiposAsbuilt.add("11");
            }
            if(energia){
                listadoTiposAsbuilt.add("12");
            }
            if(transmision){
                listadoTiposAsbuilt.add("13");
            }
            if(implementacion){
                listadoTiposAsbuilt.add("14");
            }

            try {

                String id_otec = map.get("id_otec").toString();
                String codigo_proyecto = map.get("codigo_proyecto").toString();
                String nemonico = map.get("nemonico").toString();
                String usuario = map.get("usuario").toString();

                body = "{\n" +
                        "  \"id_otec\": "  + "\"" + id_otec + "\"" + ",\n";


                body = body + "\"tipo_asbuitls\":  ["  ;

                boolean primero = true;
                for(String tipoAsbuilt: listadoTiposAsbuilt){
                    if(primero){
                        body = body + "\"" + tipoAsbuilt + "\"" + "\n" ;
                    }else{
                        body = body + ",\"" + tipoAsbuilt + "\"" + "\n" ;
                    }
                    primero = false;
                }

                body = body + "],"  ;

                body = body +
                        "  \"codigo_proyecto\": "  + "\"" + codigo_proyecto + "\"" + ",\n" +
                        "  \"nemonico\": "  + "\"" + nemonico + "\"" + ",\n" +
                        "  \"usuario\": "  + "\"" + usuario + "\"" + "}\n";




            }catch (Exception ex) {
                cadenaRetorno = "error";
                throw new Exception("Error en el body del request");
            }

            URL url = new URL(urlServicio);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStream os = conn.getOutputStream();
            os.write(body.getBytes());
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                retorno = output;
            }

            conn.disconnect();


            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al agregar cerco agricola. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(ObtenerCodigoProyectoAsBuiltAction.class);



    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }


    public boolean isInfraestructura() {
        return infraestructura;
    }

    public void setInfraestructura(boolean infraestructura) {
        this.infraestructura = infraestructura;
    }

    public boolean isEnergia() {
        return energia;
    }

    public void setEnergia(boolean energia) {
        this.energia = energia;
    }

    public boolean isTransmision() {
        return transmision;
    }

    public void setTransmision(boolean transmision) {
        this.transmision = transmision;
    }

    public boolean isImplementacion() {
        return implementacion;
    }

    public void setImplementacion(boolean implementacion) {
        this.implementacion = implementacion;
    }
}
