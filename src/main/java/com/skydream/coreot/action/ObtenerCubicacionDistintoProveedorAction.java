/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.CubicadorDAO;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.type.TypeReference;
import org.jboss.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author rrr
 */
public class ObtenerCubicacionDistintoProveedorAction extends ActionSupport {

    private int usuarioId;
    private int otId;
    private int cubicadorId;
    private int proveedorId;
    private String retorno;

    public String execute() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        String token = request.getParameter("tk");
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(token, getUsuarioId(),true);
        GenericDAO.getINSTANCE().registrarJournal(token, usuarioId, request.getServletPath(),
                URLDecoder.decode(request.getQueryString(), "UTF-8"), new Date());
        String cadenaRetorno = "";
        ObjectMapper mapper = new ObjectMapper();

        try {
            List objeto = CubicadorDAO.getINSTANCE().obtenerCubicacionDistintoProveedorAction(otId, cubicadorId, proveedorId);

            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(objeto));
            cadenaRetorno = "success";

        } catch (Exception ex) {
            log.error("Error al crear cubicacion. ", ex);
            throw ex;
        }

        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(ObtenerCubicacionDistintoProveedorAction.class);

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }



    public int getOtId() {
        return otId;
    }

    public void setOtId(int otId) {
        this.otId = otId;
    }

    public int getCubicadorId() {
        return cubicadorId;
    }

    public void setCubicadorId(int cubicadorId) {
        this.cubicadorId = cubicadorId;
    }

    public int getProveedorId() {
        return proveedorId;
    }

    public void setProveedorId(int proveedorId) {
        this.proveedorId = proveedorId;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }
}
