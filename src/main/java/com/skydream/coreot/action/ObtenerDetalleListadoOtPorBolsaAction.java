/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.pojos.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.util.List;

/**
 *
 * @author mcj
 */
public class ObtenerDetalleListadoOtPorBolsaAction extends ActionSupport {

    private String retorno;
    private int usuarioId;
    private int rolId;
    private int idPmo;
    private Long numeroContrato;
    private String lp;
    private String pep2;
    private String subGerente;
    private String contrato;
    private double montoTotal;
    private String sitio;
    private int idSubgerente;
    private String estado;
    private String jsonActas;
    private int bolsaId;
    private String moneda;
    private String proveedor;

    public String execute() throws Exception {
//        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"),usuarioId,true);
        String cadenaRetorno = "";
        List<OtDetalleBolsasGE> listaDetalleOts = null;

        try {
            //DetalleBolsaGestionEconomica detalle = GenericDAO.getINSTANCE().obtenerBolsasPorOt(getOtId(),getRolId());
            DetalleBolsaGestionEconomica detalleBolsaGestionEconomica = new DetalleBolsaGestionEconomica();

            detalleBolsaGestionEconomica.setIdPmo(getIdPmo());
            detalleBolsaGestionEconomica.setNumeroContrato(getNumeroContrato());
            detalleBolsaGestionEconomica.setLp(getLp());
            detalleBolsaGestionEconomica.setPep2(getPep2());
            detalleBolsaGestionEconomica.setSubGerente(getSubGerente());
            detalleBolsaGestionEconomica.setContrato(getContrato());
            detalleBolsaGestionEconomica.setMontoTotal(getMontoTotal());
            detalleBolsaGestionEconomica.setSitio(getSitio());
            detalleBolsaGestionEconomica.setIdSubgerente(getIdSubgerente());
            detalleBolsaGestionEconomica.setBolsaId(getBolsaId());
            detalleBolsaGestionEconomica.setMoneda(getMoneda());
            detalleBolsaGestionEconomica.setProveedor(getProveedor());

            if(getRolId() == 6) { // pagos tiene logica de json
                listaDetalleOts = GenericDAO.getINSTANCE().obtenerListaDetalleOtsPorBolsaPagos(getUsuarioId(),getJsonActas());
            }else {
                if(estado.compareTo("pagadas") == 0){
                    listaDetalleOts = GenericDAO.getINSTANCE().obtenerListaDetalleOtsPorBolsaPagadas(getUsuarioId(), detalleBolsaGestionEconomica, getRolId());
                }else{
                    listaDetalleOts = GenericDAO.getINSTANCE().obtenerListaDetalleOtsPorBolsa(getUsuarioId(), detalleBolsaGestionEconomica, this.getEstado(), getRolId());
                }
            }

            detalleBolsaGestionEconomica.setActas(listaDetalleOts);

            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            retorno = ow.writeValueAsString(detalleBolsaGestionEconomica);
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al listar detalle de bolsas. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(ObtenerDetalleListadoOtPorBolsaAction.class);

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }


    public int getRolId() {
        return rolId;
    }

    public void setRolId(int rolId) {
        this.rolId = rolId;
    }

    public int getIdPmo() {
        return idPmo;
    }

    public void setIdPmo(int idPmo) {
        this.idPmo = idPmo;
    }

    public String getLp() {
        return lp;
    }

    public void setLp(String lp) {
        this.lp = lp;
    }

    public String getPep2() {
        return pep2;
    }

    public void setPep2(String pep2) {
        this.pep2 = pep2;
    }

    public Long getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(Long numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public String getSubGerente() {
        return subGerente;
    }

    public void setSubGerente(String subGerente) {
        this.subGerente = subGerente;
    }

    public String getContrato() {
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public double getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(double montoTotal) {
        this.montoTotal = montoTotal;
    }

    public String getSitio() {
        return sitio;
    }

    public void setSitio(String sitio) {
        this.sitio = sitio;
    }

    public int getIdSubgerente() {
        return idSubgerente;
    }

    public void setIdSubgerente(int idSubgerente) {
        this.idSubgerente = idSubgerente;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getJsonActas() {
        return jsonActas;
    }

    public void setJsonActas(String jsonActas) {
        this.jsonActas = jsonActas;
    }

    public int getBolsaId() {
        return bolsaId;
    }

    public void setBolsaId(int bolsaId) {
        this.bolsaId = bolsaId;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }
}
