/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.OtDAO;
import com.skydream.coreot.pojos.Login;
import com.skydream.coreot.pojos.OtAux;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 *
 * @author Erbi
 */
public class ObtenerDetalleOtAction extends ActionSupport {

    private int otId;
    private String retorno;
    private int usuarioId;

    public String execute() throws Exception {
        final String token = ServletActionContext.getRequest().getParameter("tk");
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(token,usuarioId,true);
        String cadenaRetorno = "";
        try {
            Login login = LoginLogoutDAO.getINSTANCE().retornarLoginSesionActual(token);
            OtAux otRetorno = OtDAO.getINSTANCE().obtenerDetalleOtAux(otId, login);

            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(otRetorno));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            cadenaRetorno = "error";
            getLog().error("Error al obtener el detalle de la OT. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static Logger log = Logger.getLogger(ObtenerDetalleOtAction.class);

    public int getOtId() {
        return otId;
    }

    public void setOtId(int otId) {
        this.otId = otId;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public static Logger getLog() {
        return log;
    }

    public static void setLog(Logger aLog) {
        log = aLog;
    }

}
