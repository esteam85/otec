/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.pojos.DetalleBolsaGestionEconomica;
import com.skydream.coreot.pojos.OtDetalleBolsasGE;
import com.skydream.coreot.pojos.ServiciosOt;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author mcj
 */
public class ObtenerDetalleServiciosPorActaAction extends ActionSupport {

    private String retorno;
    private int usuarioId;
    private int actaId;

    public String execute() throws Exception {
//        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"),usuarioId,true);
        String cadenaRetorno = "";

        try {
            //setActaId(100);
            List<ServiciosOt> listaDetalleServicios = GenericDAO.getINSTANCE().obtenerListaDetalleServiciosPorActa(getActaId());

            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            retorno = ow.writeValueAsString(listaDetalleServicios);
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al listar Cubicaciones. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(ListarOtAction.class);

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getActaId() {
        return actaId;
    }

    public void setActaId(int actaId) {
        this.actaId = actaId;
    }


}
