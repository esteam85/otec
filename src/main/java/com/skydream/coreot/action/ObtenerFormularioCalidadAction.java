/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.OtDAO;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Erbi
 */
public class ObtenerFormularioCalidadAction extends ActionSupport {

    private int otId;
    private int usuarioId;
    private String tipoFormulario;
    private List retorno;


    public String execute() throws Exception {
//        final String token = ServletActionContext.getRequest().getParameter("tk");
//        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(token,usuarioId,true);
        String cadenaRetorno = "";
        Map formularioCalidad = new HashMap();
        try {
//            Login login = LoginLogoutDAO.getINSTANCE().retornarLoginSesionActual(token);
//            retorno = OtDAO.getINSTANCE().obtieneFormularioCalidad(otId, usuarioId, tipoFormulario);
            cadenaRetorno = "success";
        } catch (Exception ex) {
            cadenaRetorno = "error";
            getLog().error("Error al obtener el detalle de la OT. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static Logger log = Logger.getLogger(ObtenerFormularioCalidadAction.class);

    public int getOtId() {
        return otId;
    }

    public void setOtId(int otId) {
        this.otId = otId;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public static Logger getLog() {
        return log;
    }

    public static void setLog(Logger aLog) {
        log = aLog;
    }


    public List getRetorno() {
        return retorno;
    }

    public void setRetorno(List retorno) {
        this.retorno = retorno;
    }

    public String getTipoFormulario() {
        return tipoFormulario;
    }

    public void setTipoFormulario(String tipoFormulario) {
        this.tipoFormulario = tipoFormulario;
    }
}
