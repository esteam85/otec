package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.MesCantidadAux;
import com.skydream.coreot.pojos.RetornoDashboard;
import com.skydream.eventmanager.FacadeDashboard;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by christian on 26-10-15.
 */
public class ObtenerGraficoDashboardAction extends ActionSupport {

    private String retorno;
    private int idUsuario;
    private int codigoGrafico;
    private int usuarioId;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";

        List listaRetorno = new ArrayList();
        RetornoDashboard retornoDashboard = new RetornoDashboard();
        try {
            switch (codigoGrafico){
                case 1:
                    List<MesCantidadAux> lista = facadeDashboard.retornarApFechaInicioRealVsFechaInicio(idUsuario);
                    List<Integer> listaMes = new ArrayList<>();
                    List<Integer> listaCantidad = new ArrayList<>();

                    for(MesCantidadAux mesCantidad: lista){
                        int mesAux = (Integer) mesCantidad.getMes();
                        int cantidadAux = mesCantidad.getCantidad();
                        listaMes.add(mesAux);
                        listaCantidad.add(cantidadAux);
                    }
                    retornoDashboard.setX(listaMes);
                    retornoDashboard.setY(listaCantidad);
                    break;
            }

            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(retornoDashboard));
            cadenaRetorno = "success";
        } catch (NumberFormatException | IOException ex) {
            log.error("Error al obtener notificaciones. ", ex);
            throw ex;
        }

        return cadenaRetorno;
    }

    private FacadeDashboard facadeDashboard = new FacadeDashboard();
    private static final Logger log = Logger.getLogger(ObtenerGraficoDashboardAction.class);




    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getCodigoGrafico() {
        return codigoGrafico;
    }

    public void setCodigoGrafico(int codigoGrafico) {
        this.codigoGrafico = codigoGrafico;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
