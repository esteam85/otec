/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.OtDAO;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.util.List;

/**
 *
 * @author Erbi
 */
public class ObtenerGraficoDetalleAction extends ActionSupport {

    private String retorno;
    private int usuarioId;
    private int graficoId;
    private String serie;
    private String categoria1;
    private String categoria2;
    private String categoria3;
    private int mes;
    
    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"),usuarioId,true);
        String cadenaRetorno = "";
        try {
            if(getCategoria2()== null)setCategoria2("");
            if(getCategoria3()== null)setCategoria3("");
            String retorno = GenericDAO.getINSTANCE().obtenerGraficoDetalle(getGraficoId(),getUsuarioId(),getSerie(),getCategoria1(),getCategoria2(),getCategoria3(),getMes());
            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(retorno);
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al obtener Objeto. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }
    
    private static final Logger log = Logger.getLogger(ObtenerGraficoDetalleAction.class);

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getGraficoId(){return this.graficoId;}

    public void setGraficoId(int graficoId){this.graficoId = graficoId;}

    public String getSerie(){return this.serie;}

    public void setSerie(String serie){this.serie = serie;}

    public String getCategoria1(){return this.categoria1;}

    public void setCategoria1(String categoria1){this.categoria1 = categoria1;}

    public String getCategoria2(){return this.categoria2;}

    public void setCategoria2(String categoria2){this.categoria2 = categoria2;}

    public String getCategoria3(){return this.categoria3;}

    public void setCategoria3(String categoria3){this.categoria3 = categoria3;}

    public int getMes() { return mes;}

    public void setMes(int ano) {
        this.mes = mes;
    }
}
