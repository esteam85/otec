/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.OtDAO;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.util.List;

/**
 *
 * @author Erbi
 */
public class ObtenerGraficoGlobalAction extends ActionSupport {

    private String retorno;
    private int usuarioId;
    private int graficoId;
    private int contratoId;
    private int proveedorId;
    private int ano;
    private int mes;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"),usuarioId,true);
        String cadenaRetorno = "";
        setMes(0);
        try {
            String retorno = GenericDAO.getINSTANCE().obtenerGraficoGlobal(getGraficoId(), getUsuarioId(), getContratoId(),getProveedorId(),getAno(), getMes());
            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(retorno));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al obtener Objeto. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }
    
    private static final Logger log = Logger.getLogger(ObtenerGraficoGlobalAction.class);

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getGraficoId(){return this.graficoId;}

    public void setGraficoId(int graficoId){this.graficoId = graficoId;}

    public int getContratoId() {
        return contratoId;
    }

    public void setContratoId(int contratoId) {
        this.contratoId = contratoId;
    }

    public int getProveedorId() {
        return proveedorId;
    }

    public void setProveedorId(int proveedorId) {
        this.proveedorId = proveedorId;
    }

    public int getAno() { return ano;}

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getMes() { return mes;}

    public void setMes(int ano) {
        this.mes = mes;
    }
}
