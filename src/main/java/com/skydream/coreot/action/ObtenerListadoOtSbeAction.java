package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.Notificaciones;
import com.skydream.coreot.pojos.Ot;
import com.skydream.coreot.pojos.OtAux;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by christian on 26-10-15.
 */
public class ObtenerListadoOtSbeAction extends ActionSupport {

    private int idContrato;
    private String retorno;
    private int usuarioId;

    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), getUsuarioId(),true);
        String cadenaRetorno = "";
        try {
            List<OtAux> lista = GenericDAO.getINSTANCE().obtenerListadoOtSbe(idContrato);
            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(lista));
            cadenaRetorno = "success";
        } catch (NumberFormatException | IOException ex) {
            log.error("Error al obtener notificaciones. ", ex);
            throw ex;
        }


        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(ObtenerListadoOtSbeAction.class);


    public int getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(int idContrato) {
        this.idContrato = idContrato;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
