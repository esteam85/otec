package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.Notificaciones;
import com.skydream.coreot.pojos.Ot;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by christian on 26-10-15.
 */
public class ObtenerNotificacionesAction extends ActionSupport {

    private int idUsuario;
    private String retorno;


    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"), idUsuario,true);
        String cadenaRetorno = "";
        try {
            List<Notificaciones> lista = GenericDAO.getINSTANCE().obtenerNotificacionesPorUsuario(idUsuario);
            List<Notificaciones> listaRetorno = new ArrayList<>();
            for(Notificaciones notificacion: lista){
                Notificaciones noti = new Notificaciones();
                noti.setId(notificacion.getId());
                noti.setEventoId(notificacion.getEventoId());
                noti.setMensaje(notificacion.getMensaje());
                noti.setLeido(notificacion.getLeido());
                noti.setFechaCreacion(notificacion.getFechaCreacion());
                noti.setOt(new Ot(notificacion.getOt().getId()));
                listaRetorno.add(noti);
            }
            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(listaRetorno));
            cadenaRetorno = "success";
        } catch (NumberFormatException | IOException ex) {
            log.error("Error al obtener notificaciones. ", ex);
            throw ex;
        }

//        }


        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(ObtenerNotificacionesAction.class);

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }


    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }
}
