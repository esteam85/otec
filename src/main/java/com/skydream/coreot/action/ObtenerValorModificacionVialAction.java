/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.CubicadorDAO;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;

/**
 *
 * @author christian
 */
public class ObtenerValorModificacionVialAction extends ActionSupport {


    private int usuarioId;
    private int agenciaId;
    private double puntoBaremos;
    private double valorModificacionVial;

    public String execute() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        String token = request.getParameter("tk");
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(token, getUsuarioId(),true);
        String cadenaRetorno = "";
        try {
            setValorModificacionVial(CubicadorDAO.getINSTANCE().obtenerValorModificacionVial(agenciaId,puntoBaremos));
            cadenaRetorno = "success";

        } catch (Exception ex) {
            throw ex;
        }
        return cadenaRetorno;
    }


    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getAgenciaId() {
        return agenciaId;
    }

    public void setAgenciaId(int agenciaId) {
        this.agenciaId = agenciaId;
    }


    public double getValorModificacionVial() {
        return valorModificacionVial;
    }

    public void setValorModificacionVial(double valorModificacionVial) {
        this.valorModificacionVial = valorModificacionVial;
    }

    public double getPuntoBaremos() {
        return puntoBaremos;
    }

    public void setPuntoBaremos(double puntoBaremos) {
        this.puntoBaremos = puntoBaremos;
    }
}
