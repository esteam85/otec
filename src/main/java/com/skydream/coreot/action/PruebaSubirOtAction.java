/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.dao.LibroObraDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.ArchivoAux;
import com.skydream.coreot.pojos.LibroObras;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rrr
 */
public class PruebaSubirOtAction extends ActionSupport {

    private String parametros;
    private String objCoreOT;
    private String latitud;
    private String longitud;
    private int id;
    private int usuarioId;
    private int otId;
    private String observaciones;
    private String[] roles;
    private List<File> archivo = new ArrayList<>();
    private List<String> archivoFileName = new ArrayList<>();    
    private List<String> archivoContentType = new ArrayList<>();


    public String execute() throws Exception {

        String cadenaRetorno = "";
        try {
            setearRoles();
            CoreOtFactory coreOtFactory = new CoreOtFactory();
            ObjetoCoreOT objetoCoreOT = coreOtFactory.getObjetoCoreOt(getObjCoreOT());
            Map map = new HashMap();
            Map mapAux = new HashMap();
            mapAux.put("id",otId);
            map.put("ot",mapAux);
            map.put("observaciones",observaciones);
            if(latitud!=null && !latitud.isEmpty()){
                map.put("latitud", latitud);
            }
            if(longitud!=null && !longitud.isEmpty()){
                map.put("longitud", longitud);
            }
            LibroObras libroObras = null;
            objetoCoreOT.setearObjetoDesdeMap(map, 0);
            libroObras = (LibroObras) objetoCoreOT;
            libroObras.setFechaCreacion(new Date());

            List<ArchivoAux> listadoArchivo = new ArrayList<>();

            if (getArchivo() != null && getArchivo().size() > 0) {
                for (int i = 0; i < getArchivo().size(); i++) {
                    System.out.println(getArchivo().get(i));
                    ByteArrayOutputStream baos = leerArchivo(getArchivo().get(i));
//                    gestionarArchivo(baos, i, id, usuarioId, otId);
                    ArchivoAux archivoAux = new ArchivoAux();
                    archivoAux.setBaos(baos);
                    archivoAux.setNombre(getArchivoFileName().get(i));
                    archivoAux.setExtension(retornarExtension(getArchivoFileName().get(i)));
                    listadoArchivo.add(archivoAux);
                }
            }

            LibroObraDAO.gestionarAdjuntoLibroObraOt2(listadoArchivo, libroObras,getRoles(), usuarioId, otId);
            cadenaRetorno = "success";
        } catch (Exception ex) {
            System.out.println(ex);
            cadenaRetorno = "error";
            throw new Exception("No se pudo registrar el objeto");
        }
        return cadenaRetorno;
    }

    private ByteArrayOutputStream leerArchivo(File archivo) throws IOException, FileNotFoundException {
        ByteArrayOutputStream baos;
        InputStream is = new FileInputStream(archivo);
        try (BufferedInputStream bis = new BufferedInputStream(is)) {
            baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int readCount;
            while ((readCount = bis.read(buffer)) > 0) {
                System.out.println("Writing: ");
                baos.write(buffer, 0, readCount);
            }
        }
        return baos;
    }

    private void gestionarArchivo(ByteArrayOutputStream baos, int contador, int idLibroDeObras, int usuarioId, int otId) throws Exception {
        int idLibroObra = getId();
        String nombreArchivo = getArchivoFileName().get(contador);
        String extensionArchivo = retornarExtension(getArchivoFileName().get(contador));

        LibroObraDAO.gestionarAdjuntoLibroObraOt(idLibroObra, nombreArchivo, extensionArchivo, baos.toByteArray(), idLibroDeObras, usuarioId, otId, contador);
    }

    private static String retornarExtension(String nombreArhivo) {
        String fileName = nombreArhivo;
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    public String getObjCoreOT() {
        return objCoreOT;
    }

    public void setObjCoreOT(String objCoreOT) {
        this.objCoreOT = objCoreOT;
    }

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public List<File> getArchivo() {
        return archivo;
    }

    public void setArchivo(List<File> archivo) {
        this.archivo = archivo;
    }

    public List<String> getArchivoContentType() {
        return archivoContentType;
    }

    public void setArchivoContentType(List<String> archivoContentType) {
        this.archivoContentType = archivoContentType;
    }

    public List<String> getArchivoFileName() {
        return archivoFileName;
    }

    public void setArchivoFileName(List<String> archivoFileName) {
        this.archivoFileName = archivoFileName;
    }

    public int getOtId() {
        return otId;
    }

    public void setOtId(int otId) {
        this.otId = otId;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String[] getRoles() {
        return roles;
    }

    private void setearRoles()throws Exception{
        if(roles==null)
            throw new Exception("Roles no encontrados.");

        ObjectMapper mapper = new ObjectMapper();
        String[] arrayAux = roles.clone();
        roles = new String[arrayAux.length];
        int pos = 0;
        for (String string : arrayAux) {
            try {
                Map mapaRol = mapper.readValue(string, new TypeReference<HashMap<String, Object>>() {
                });
                roles[pos] = mapaRol.get("id").toString();
                pos++;
            } catch (IOException ex) {
                Logger.getLogger(PruebaSubirOtAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }
}
