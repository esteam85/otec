/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.jboss.logging.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Christian
 */
public class RegistrarAsBuiltAction extends ActionSupport {
    

    private int id;
    private String parametros;
    private String retorno;

    
    public String execute() throws Exception {

        String cadenaRetorno = "";
        try {

            String urlServicio = "http://10.232.139.111/api_asbuilts/api/respasbuilts/";
            urlServicio = urlServicio + id;

            String body = "";

            ObjectMapper mapper = new ObjectMapper();
            Map map = mapper.readValue(parametros, new TypeReference<HashMap<String, String>>() {
            });

            try {

                String tipoasbuilts = map.get("tipoasbuilts").toString();
                String observacion = map.get("observacion").toString();
                String usuario = map.get("usuario").toString();

                body = "{\n" +
                        "  \"tipoasbuilts\": "  + "\"" + tipoasbuilts + "\"" + ",\n" +
                        "  \"observacion\": "  + "\"" + observacion + "\"" + ",\n" +
                        "  \"usuario\": "  + "\"" + usuario + "\"" + "\n" +
                        "}";

            }catch (Exception ex) {
                cadenaRetorno = "error";
                throw new Exception("Error en el body del request");
            }

            URL url = new URL(urlServicio);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStream os = conn.getOutputStream();
            os.write(body.getBytes());
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                retorno = output;
            }

            conn.disconnect();


            cadenaRetorno = "success";
        } catch (Exception ex) {
            log.error("Error al agregar cerco agricola. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static final Logger log = Logger.getLogger(RegistrarAsBuiltAction.class);



    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }


}
