/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.type.TypeReference;

import java.util.*;

/**
 *
 * @author Erbi
 */
public class RegistrarCheckListOtAction extends ActionSupport {

    private String parametros;
    private String retorno;
    private int usuarioId;


    public String execute() throws Exception {
        final String token = ServletActionContext.getRequest().getParameter("tk");
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(token,usuarioId,true);
        String cadenaRetorno = "";
        try {
            Login login = LoginLogoutDAO.getINSTANCE().retornarLoginSesionActual(token);

            ObjectMapper mapper = new ObjectMapper();
            Map mapaParams = mapper.readValue(parametros, new TypeReference<HashMap<String,Object>>() {
            });

            CheckListOt checkListOt = new CheckListOt();
            Usuarios usuario = new Usuarios();
            usuario.setId(usuarioId);
            checkListOt.setUsuario(usuario);
            checkListOt.setCheckListDetalleOt((String)mapaParams.get("checkListString"));
            Ot ot = new Ot();
            int otId = Integer.parseInt(mapaParams.get("otId").toString());
            ot.setId(otId);
            checkListOt.setOt(ot);
            checkListOt.setFechaCreacion(new Date());
            checkListOt.setEmpresacolaboradora(false);
            checkListOt.setObservacion(mapaParams.get("observaciones").toString());

            int idCheckListOT = GenericDAO.getINSTANCE().registrarCheckListOt(checkListOt);
            checkListOt.setId(idCheckListOT);

            int puntuacionFinal = 0;

            if(mapaParams.containsKey("checkList")){

                ArrayList<LinkedHashMap> list =  (ArrayList<LinkedHashMap>) mapaParams.get("checkList");
                Iterator iterator = list.iterator();


                while(iterator.hasNext()) {
                    LinkedHashMap item = (LinkedHashMap)iterator.next();

                    String nombreItem = item.get("item").toString();
                    String observacionItem = item.get("observacion").toString();
                    Map opcionItem = (Map)item.get("opcion");
                    int idOpcion = Integer.parseInt(opcionItem.get("id").toString());
                    String nombreOpcion = opcionItem.get("nombre").toString();
                    int puntuacion = Integer.parseInt(item.get("puntuacion").toString());
                    CheckListOtDetalle checkListOtDetalle = new CheckListOtDetalle();
                    checkListOtDetalle.setCheckListOt(checkListOt);
                    checkListOtDetalle.setItem(nombreItem);
                    checkListOtDetalle.setObservacion(observacionItem);
                    checkListOtDetalle.setOpcionId(idOpcion);
                    checkListOtDetalle.setOpcionNombre(nombreOpcion);
                    checkListOtDetalle.setPuntuacion(puntuacion);
                    if(idOpcion == 1){
                        puntuacionFinal = puntuacionFinal + puntuacion;
                    }
                    int idCheckListOtDetalle = GenericDAO.getINSTANCE().registrarCheckListOtDetalle(checkListOtDetalle);
                    checkListOtDetalle.setId(idCheckListOtDetalle);

                }

            }

            GenericDAO.getINSTANCE().actualizarPuntuacionCheckListOt(idCheckListOT, puntuacionFinal);

            ObjectMapper objMap = new ObjectMapper();
            objMap.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            ObjectWriter ow = objMap.writer().withDefaultPrettyPrinter();
            setRetorno(ow.writeValueAsString(checkListOt));
            cadenaRetorno = "success";
        } catch (Exception ex) {
            cadenaRetorno = "error";
            getLog().error("Error al obtener el detalle de la OT. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    private static Logger log = Logger.getLogger(RegistrarCheckListOtAction.class);

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public static Logger getLog() {
        return log;
    }

    public static void setLog(Logger aLog) {
        log = aLog;
    }

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }
}
