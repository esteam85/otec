/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.OtDAO;
import com.skydream.coreot.pojos.ArchivoAux;
import com.skydream.coreot.pojos.LibroObras;
import com.skydream.coreot.pojos.Login;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import org.apache.log4j.Logger;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.logging.Level;


import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

/**
 *
 * @author Erbi
 */
public class RegistrarFormularioSeguimientoCalidadAction extends ActionSupport{

    private int otId;
    private int usuarioId;
    private boolean respuesta;
    private String parametros;
    private List<File> archivo = new ArrayList<>();
    private List<String> archivoFileName = new ArrayList<>();
    private List<String> archivoContentType = new ArrayList<>();
    private String latitud;
    private String longitud;

    public String execute() throws Exception {
        final String token = ServletActionContext.getRequest().getParameter("tk");
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(token,usuarioId,true);
        String cadenaRetorno = "";
        Map formularioCalidad = new HashMap();
        try {
            Login login = LoginLogoutDAO.getINSTANCE().retornarLoginSesionActual(token);

            CoreOtFactory coreOtFactory = new CoreOtFactory();
            ObjetoCoreOT objetoCoreOT = coreOtFactory.getObjetoCoreOt("LibroObra");
            Map map = new HashMap();
            Map mapAux = new HashMap();
            mapAux.put("id",otId);
            map.put("ot",mapAux);
            List<ArchivoAux> listadoArchivo = new ArrayList<>();
            HashMap<String,Object> mapaParametros = new ObjectMapper().readValue(parametros, HashMap.class);
            Map mapaEntrada = (HashMap) mapaParametros.get("parametros");
            String latitudEntrada = mapaEntrada.get("latitud").toString();
            String longitudEntrada = mapaEntrada.get("longitud").toString();
            if(!latitudEntrada.equals("undefined") && !latitudEntrada.equals("0") && latitudEntrada !=null && !latitudEntrada.isEmpty()){
                map.put("latitud", latitudEntrada);
            }
            if(!longitudEntrada.equals("undefined")  && !longitudEntrada.equals("0")  && longitudEntrada !=null && !longitudEntrada.isEmpty()){
                map.put("longitud", longitudEntrada);
            }
            LibroObras libroObras = null;
            objetoCoreOT.setearObjetoDesdeMap(map, 0);
            libroObras = (LibroObras) objetoCoreOT;
            libroObras.setFechaCreacion(new Date());

            if (getArchivo() != null && getArchivo().size() > 0) {
                for (int i = 0; i < getArchivo().size(); i++) {
                    System.out.println(getArchivo().get(i));
                    ByteArrayOutputStream baos = leerArchivo(getArchivo().get(i));
//                    gestionarArchivo(baos, i, id, usuarioId, otId);
                    ArchivoAux archivoAux = new ArchivoAux();
                    archivoAux.setBaos(baos);
                    archivoAux.setNombre(getArchivoFileName().get(i));
                    archivoAux.setExtension(retornarExtension(getArchivoFileName().get(i)));
                    listadoArchivo.add(archivoAux);
                }
            }

            respuesta = OtDAO.getINSTANCE().registraFormularioSeguimientoCalidad(otId, usuarioId, mapaParametros, listadoArchivo, libroObras);
            cadenaRetorno = "success";
        } catch (Exception ex) {
            cadenaRetorno = "error";
            getLog().error("Error al obtener el detalle de la OT. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }
    private ByteArrayOutputStream leerArchivo(File archivo) throws IOException, FileNotFoundException {
        ByteArrayOutputStream baos;
        InputStream is = new FileInputStream(archivo);
        try (BufferedInputStream bis = new BufferedInputStream(is)) {
            baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int readCount;
            while ((readCount = bis.read(buffer)) > 0) {
                baos.write(buffer, 0, readCount);
            }
        }
        return baos;
    }
    private static String retornarExtension(String nombreArhivo) {
        String fileName = nombreArhivo;
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    private static Logger log = Logger.getLogger(RegistrarFormularioSeguimientoCalidadAction.class);

    public int getOtId() {
        return otId;
    }

    public void setOtId(int otId) {
        this.otId = otId;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public static Logger getLog() {
        return log;
    }

    public static void setLog(Logger aLog) {
        log = aLog;
    }


    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }


    public boolean isRespuesta() {
        return respuesta;
    }

    public void setRespuesta(boolean respuesta) {
        this.respuesta = respuesta;
    }

    public List<File> getArchivo() {
        return archivo;
    }

    public void setArchivo(List<File> archivo) {
        this.archivo = archivo;
    }

    public List<String> getArchivoFileName() {
        return archivoFileName;
    }

    public void setArchivoFileName(List<String> archivoFileName) {
        this.archivoFileName = archivoFileName;
    }

    public List<String> getArchivoContentType() {
        return archivoContentType;
    }

    public void setArchivoContentType(List<String> archivoContentType) {
        this.archivoContentType = archivoContentType;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }
}
