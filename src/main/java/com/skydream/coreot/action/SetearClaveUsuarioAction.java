/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.UsuarioDAO;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author mcj
 */

public class SetearClaveUsuarioAction {

    private String claveAntigua;
    private String claveNueva;
    private int usuarioId;
    private boolean estadoOK;
    
    public String execute() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        String token = request.getParameter("tk");
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(token, getUsuarioId(),true);
        estadoOK = false;
        String cadenaRetorno = "";
        try {

            UsuarioDAO usrDAO = UsuarioDAO.getINSTANCE();
            if(usrDAO.validarPasswordUsuario(usuarioId,claveAntigua)){
                usrDAO.setearPassword(usuarioId, claveNueva);
                estadoOK = true;
            }
            cadenaRetorno="success";
        } catch (Exception ex) {
            cadenaRetorno="error";
            log.error("Error al setear clave de Usuario. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }


    public boolean isEstadoOK() {
        return estadoOK;
    }

    public void setEstadoOK(boolean estadoOK) {
        this.estadoOK = estadoOK;
    }
    
    private static final Logger log = Logger.getLogger(SetearClaveUsuarioAction.class);

    public String getClaveAntigua() {
        return claveAntigua;
    }

    public void setClaveAntigua(String claveAntigua) {
        this.claveAntigua = claveAntigua;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getClaveNueva() {
        return claveNueva;
    }

    public void setClaveNueva(String claveNueva) {
        this.claveNueva = claveNueva;
    }

}
