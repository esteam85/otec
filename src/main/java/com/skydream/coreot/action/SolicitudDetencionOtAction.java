/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.action;

import com.opensymphony.xwork2.ActionSupport;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.dao.OtDAO;
import org.apache.struts2.ServletActionContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.jboss.logging.Logger;

import java.util.List;

/**
 *
 * @author Erbi
 */
public class SolicitudDetencionOtAction extends ActionSupport {
    
    private String otId;
    private int usuarioId;
    private int tipoTramiteId;
    private String tramite;
    
    public String execute() throws Exception {
        LoginLogoutDAO.getINSTANCE().validarSesionUsuario(ServletActionContext.getRequest().getParameter("tk"),usuarioId,true);
        String cadenaRetorno = "success";
        try {
            if(!OtDAO.getINSTANCE().isTramiteEnProcesoOt(Integer.parseInt(otId))){
                Boolean respuesta = OtDAO.getINSTANCE().agregarTramite(Integer.parseInt(otId), getTipoTramiteId(), getTramite(), usuarioId);
            }else throw new Exception("Ya existe un tramite en proceso.");

        } catch (Exception ex) {
            log.error("Error al ingresar el tramite. ", ex);
            throw ex;
        }
        return cadenaRetorno;
    }

    public String getOtId() {
        return otId;
    }

    public void setOtId(String otId) {
        this.otId = otId;
    }
    
    private static final Logger log = Logger.getLogger(SolicitudDetencionOtAction.class);

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getTipoTramiteId() {return tipoTramiteId;}

    public void setTipoTramiteId(int tipoTramiteId) {this.tipoTramiteId = tipoTramiteId;}

    public String getTramite(){return  tramite;}

    public void setTramite(String tramite){this.tramite = tramite;}
}
