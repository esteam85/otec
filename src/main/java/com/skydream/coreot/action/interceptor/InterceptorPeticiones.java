package com.skydream.coreot.action.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import org.apache.commons.io.output.TeeOutputStream;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.struts2.ServletActionContext;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

public class InterceptorPeticiones implements Interceptor{

    private static final long serialVersionUID = 1L;
    private static final List<CharSequence> PathList = new ArrayList<>();
    private static final List<CharSequence> IgnoredServices = new ArrayList<>();

    static {
        PathList.add("informarAvanceOt");
        PathList.add("adjuntarArchivosCheckListOt");
        PathList.add("adjuntarHem");
        PathList.add("adjuntarCartaAdjudicacion");
        PathList.add("adjuntarArchivoOt");
        IgnoredServices.add("obtenerNotificaciones");
    }


    public String intercept(ActionInvocation invocation) throws Exception {
        MDC.put("sessionId", ServletActionContext.getRequest().getSession().getId());
        MDC.put("remoteAddress", ServletActionContext.getRequest().getRemoteAddr());
        String className = null;
        long startTime = 0;
        String result;
        boolean ignoreService = false;
        for (CharSequence cad : IgnoredServices) {
            if (ServletActionContext.getRequest().getServletPath().contains(cad)) {
                ignoreService = true;
                break;
            }
        }

        try {
            if(!ignoreService){
                className = invocation.getAction().getClass().getName();
                startTime = System.currentTimeMillis();

                HttpServletRequest httpServletRequest = ServletActionContext.getRequest();
                HttpServletResponse httpServletResponse = ServletActionContext.getResponse();
                Map<String, String> requestMap = this.getTypesafeRequestMap(httpServletRequest);


                BufferedRequestWrapper bufferedReqest = new BufferedRequestWrapper(httpServletRequest);
                BufferedResponseWrapper bufferedResponse = new BufferedResponseWrapper(httpServletResponse);

                logger.info("Before calling action: " + className);

                final StringBuilder logMessage = new StringBuilder("REST Request - ")
                        .append("[HTTP METHOD:")
                        .append(httpServletRequest.getMethod())
                        .append("] \n [PATH INFO:")
                        .append(httpServletRequest.getServletPath())
                        .append("] \n [REQUEST PARAMETERS:")
                        .append(requestMap)
                        .append("] \n [REMOTE ADDRESS:")
                        .append(httpServletRequest.getRemoteAddr())
                        .append("]");
                boolean esServicioConAdjuntos = false;
                // SE EVALUA SI NO ES UN SERVICIO DE SUBIDA DE ARCHIVOS
                //En caso que lo sea, NO se escribira el body del request
                for (CharSequence cad : PathList) {
                    if (httpServletRequest.getServletPath().contains(cad)) {
                        esServicioConAdjuntos = true;
                        break;
                    }
                }
                if (!esServicioConAdjuntos) {
                    logMessage.append("\n [REQUEST BODY:");
                    logMessage.append(bufferedReqest.getRequestBody());
                    logMessage.append("]");
                }
//            logMessage.append(" [RESPONSE:").append( bufferedResponse.getContent() ).append("]");
                logger.info(logMessage.toString());
            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
                result = invocation.invoke();
                if(!ignoreService) logger.info("Response Code : " + invocation.getResultCode());
        }

        if(!ignoreService){
            long endTime = System.currentTimeMillis();
            logger.info("After calling action: " + className
                    + " Time taken: " + (endTime - startTime) + " ms");
        }

        return result;
    }

    private Map<String, String> getTypesafeRequestMap(HttpServletRequest request) {
        Map<String, String> typesafeRequestMap = new HashMap<String, String>();
        Enumeration<?> requestParamNames = request.getParameterNames();
        while (requestParamNames.hasMoreElements()) {
            String requestParamName = (String)requestParamNames.nextElement();
            String requestParamValue = request.getParameter(requestParamName);
            typesafeRequestMap.put(requestParamName, requestParamValue);
        }
        return typesafeRequestMap;
    }

    private static final class BufferedRequestWrapper extends HttpServletRequestWrapper {

        private ByteArrayInputStream bais = null;
        private ByteArrayOutputStream baos = null;
        private BufferedServletInputStream bsis = null;
        private byte[] buffer = null;


        public BufferedRequestWrapper(HttpServletRequest req) throws IOException {
            super(req);
            // Read InputStream and store its content in a buffer.
            InputStream is = req.getInputStream();
            this.baos = new ByteArrayOutputStream();
            byte buf[] = new byte[1024];
            int letti;
            while ((letti = is.read(buf)) > 0) {
                this.baos.write(buf, 0, letti);
            }
            this.buffer = this.baos.toByteArray();
        }


        @Override
        public ServletInputStream getInputStream() {
            this.bais = new ByteArrayInputStream(this.buffer);
            this.bsis = new BufferedServletInputStream(this.bais);
            return this.bsis;
        }



        String getRequestBody() throws IOException  {
            BufferedReader reader = new BufferedReader(new InputStreamReader(this.getInputStream()));
            String line = null;
            StringBuilder inputBuffer = new StringBuilder();
            do {
                line = reader.readLine();
                if (null != line) {
                    inputBuffer.append(line.trim());
                }
            } while (line != null);
            reader.close();
            return inputBuffer.toString().trim();
        }

    }


    private static final class BufferedServletInputStream extends ServletInputStream {

        private ByteArrayInputStream bais;

        public BufferedServletInputStream(ByteArrayInputStream bais) {
            this.bais = bais;
        }

        @Override
        public int available() {
            return this.bais.available();
        }

        @Override
        public int read() {
            return this.bais.read();
        }

        @Override
        public int read(byte[] buf, int off, int len) {
            return this.bais.read(buf, off, len);
        }


        @Override
        public boolean isFinished() {
            return false;
        }

        @Override
        public boolean isReady() {
            return false;
        }

        @Override
        public void setReadListener(ReadListener readListener) {

        }
    }

    public class TeeServletOutputStream extends ServletOutputStream {

        private final TeeOutputStream targetStream;

        public TeeServletOutputStream( OutputStream one, OutputStream two ) {
            targetStream = new TeeOutputStream( one, two);
        }

        @Override
        public void write(int arg0) throws IOException {
            this.targetStream.write(arg0);
        }

        public void flush() throws IOException {
            super.flush();
            this.targetStream.flush();
        }

        public void close() throws IOException {
            super.close();
            this.targetStream.close();
        }

        @Override
        public boolean isReady() {
            return false;
        }

        @Override
        public void setWriteListener(WriteListener writeListener) {

        }
    }



    public class BufferedResponseWrapper implements HttpServletResponse {

        HttpServletResponse original;
        TeeServletOutputStream tee;
        ByteArrayOutputStream bos;

        public BufferedResponseWrapper(HttpServletResponse response) {
            original = response;
        }

        public String getContent() {
            return bos.toString();
        }

        public PrintWriter getWriter() throws IOException {
            return original.getWriter();
        }

        public ServletOutputStream getOutputStream() throws IOException {
            if( tee == null ){
                bos = new ByteArrayOutputStream();
                tee = new TeeServletOutputStream( original.getOutputStream(), bos );
            }
            return tee;

        }

        @Override
        public String getCharacterEncoding() {
            return original.getCharacterEncoding();
        }

        @Override
        public String getContentType() {
            return original.getContentType();
        }

        @Override
        public void setCharacterEncoding(String charset) {
            original.setCharacterEncoding(charset);
        }

        @Override
        public void setContentLength(int len) {
            original.setContentLength(len);
        }

        @Override
        public void setContentLengthLong(long len) {

        }

        @Override
        public void setContentType(String type) {
            original.setContentType(type);
        }

        @Override
        public void setBufferSize(int size) {
            original.setBufferSize(size);
        }

        @Override
        public int getBufferSize() {
            return original.getBufferSize();
        }

        @Override
        public void flushBuffer() throws IOException {
            tee.flush();
        }

        @Override
        public void resetBuffer() {
            original.resetBuffer();
        }

        @Override
        public boolean isCommitted() {
            return original.isCommitted();
        }

        @Override
        public void reset() {
            original.reset();
        }

        @Override
        public void setLocale(Locale loc) {
            original.setLocale(loc);
        }

        @Override
        public Locale getLocale() {
            return original.getLocale();
        }

        @Override
        public void addCookie(Cookie cookie) {
            original.addCookie(cookie);
        }

        @Override
        public boolean containsHeader(String name) {
            return original.containsHeader(name);
        }

        @Override
        public String encodeURL(String url) {
            return original.encodeURL(url);
        }

        @Override
        public String encodeRedirectURL(String url) {
            return original.encodeRedirectURL(url);
        }

        @SuppressWarnings("deprecation")
        @Override
        public String encodeUrl(String url) {
            return original.encodeUrl(url);
        }

        @SuppressWarnings("deprecation")
        @Override
        public String encodeRedirectUrl(String url) {
            return original.encodeRedirectUrl(url);
        }

        @Override
        public void sendError(int sc, String msg) throws IOException {
            original.sendError(sc, msg);
        }

        @Override
        public void sendError(int sc) throws IOException {
            original.sendError(sc);
        }

        @Override
        public void sendRedirect(String location) throws IOException {
            original.sendRedirect(location);
        }

        @Override
        public void setDateHeader(String name, long date) {
            original.setDateHeader(name, date);
        }

        @Override
        public void addDateHeader(String name, long date) {
            original.addDateHeader(name, date);
        }

        @Override
        public void setHeader(String name, String value) {
            original.setHeader(name, value);
        }

        @Override
        public void addHeader(String name, String value) {
            original.addHeader(name, value);
        }

        @Override
        public void setIntHeader(String name, int value) {
            original.setIntHeader(name, value);
        }

        @Override
        public void addIntHeader(String name, int value) {
            original.addIntHeader(name, value);
        }

        @Override
        public void setStatus(int sc) {
            original.setStatus(sc);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void setStatus(int sc, String sm) {
            original.setStatus(sc, sm);
        }

        @Override
        public int getStatus() {
            return 0;
        }

        @Override
        public String getHeader(String name) {
            return null;
        }

        @Override
        public Collection<String> getHeaders(String name) {
            return null;
        }

        @Override
        public Collection<String> getHeaderNames() {
            return null;
        }

    }

    public void destroy() {
        logger.info("Destroying MyLoggingInterceptor...");
    }
    public void init() {
        logger.info("Initializing MyLoggingInterceptor...");
    }


    private static final Logger logger = Logger.getLogger("file");

}
