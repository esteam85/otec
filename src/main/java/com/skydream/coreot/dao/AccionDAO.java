/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.Acciones;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import com.skydream.coreot.pojos.Parametros;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

/**
 *
 * @author rrr
 */
public class AccionDAO implements GatewayDAO {

    private AccionDAO() {
    }

    public static AccionDAO getINSTANCE() {
        return INSTANCE;
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        Acciones accion = (Acciones) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearParametros(params, gateway, accion);
            gateway.iniciarTransaccion();
            try {
                gateway.registrar(obj);
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al crear Accion. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        Acciones accion = (Acciones) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearParametros(params, gateway, accion);
            gateway.iniciarTransaccion();
            try {
                gateway.actualizar2(obj);
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al actualizar Accion. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    @Override
    public List listar(ObjetoCoreOT obj, Map params) throws Exception {
        Acciones accion = (Acciones) obj;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = gate.listar(accion, join);
            for (ObjetoCoreOT objeto : listaRetorno) {
                Acciones acc = (Acciones) objeto;
                join.add("accionesParametros");
                gate.validarYSetearObjetoARetornar(acc, join);
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    private void setearParametros(Map params, Gateway gateway, Acciones accion) throws Exception {
        if (params.containsKey("accionesParametros")) {
            List<Map> listadoParametros = (ArrayList<Map>) params.get("accionesParametros");
            for (Map parametro : listadoParametros) {
                Integer idParametro = (Integer) parametro.get("id");
                Parametros para = (Parametros) gateway.obtenerDatoPorId(new Parametros(idParametro), null);
                accion.getAccionesParametros().add(para);
            }
        }
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static final Logger log = Logger.getLogger(AccionDAO.class);
    private static final AccionDAO INSTANCE = new AccionDAO();

}
