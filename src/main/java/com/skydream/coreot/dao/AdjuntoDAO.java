/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.Adjuntos;
import com.skydream.coreot.pojos.ArchivoAux;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import com.skydream.coreot.pojos.Ot;
import com.skydream.coreot.pojos.Repositorios;
import com.skydream.coreot.pojos.Roles;
import com.skydream.coreot.pojos.Usuarios;
import com.skydream.coreot.util.Config;
import com.skydream.coreot.util.SecureTransmission;
import com.skydream.coreot.util.SendMail;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;

/**
 *
 * @author Erbi
 */
public class AdjuntoDAO implements GatewayDAO {

    public static AdjuntoDAO getINSTANCE() {
        return INSTANCE;
    }

    private AdjuntoDAO() {
    }

    @Override
    public List listar(ObjetoCoreOT obj, Map params) throws Exception {
        Ot ot = (Ot) obj;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = gate.listar(ot, join);
            for (ObjetoCoreOT objeto : listaRetorno) {
                Ot ser = (Ot) objeto;

                gate.validarYSetearObjetoARetornar(ser, join);
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    private void enviarMail(Usuarios usuario, String asunto, String cuerpo) throws Exception {
        try {
            String mailDestino = usuario.getEmail();
            SendMail sendMail = SendMail.getINSTANCE();
            boolean envioOK = sendMail.enviarMail(mailDestino, asunto, cuerpo, null);
            if (!envioOK) {
                throw new Exception("Se produjo un error al enviar email.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public static void gestionarAdjuntoOt(List<ArchivoAux> listArchivos, String[] roles, int usuarioId, int otId, String observaciones) throws Exception {
        Gateway gateway = new Gateway();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                Usuarios usrEjecutor = (Usuarios) gateway.retornarObjetoCoreOT(Usuarios.class, usuarioId);
                Ot ot = (Ot) gateway.retornarObjetoCoreOT(Ot.class, otId);

                Map<String, Object> filtros = new HashMap<>();
                filtros.put("nombre", Config.getINSTANCE().getNombreRepositorioBase());
                filtros.put("estadoActivo", Boolean.TRUE);
                gateway.obtenerRegistroConFiltro(Repositorios.class, null, filtros);

                Repositorios repositorio = (Repositorios) gateway.obtenerRegistroConFiltro(Repositorios.class, null, filtros);

                String nombreCarpeta = "Ordenes_de_Trabajo";
                String rutaAlmacenar = "Ot-" + otId;

                for (ArchivoAux archivo : listArchivos) {
                    Adjuntos adjunto = new Adjuntos();
                    adjunto.setRepositorio(repositorio);
                    adjunto.setOt(ot);
                    adjunto.setNombre(archivo.getNombre());
                    adjunto.setCarpeta(repositorio.getUrl() + "/" + nombreCarpeta + "/" + rutaAlmacenar);
                    adjunto.setExtension(archivo.getExtension());
                    adjunto.setDescripcion(observaciones);

                    //Aqui se asocian los roles para cada archivo subido, esto se alamacena en la tabla adjuntos_roles.
                    Set<Roles> listadoRoles = new HashSet<>();
                    for(String rol: roles){
                        listadoRoles.add((Roles)gateway.retornarObjetoCoreOT(Roles.class,Integer.parseInt(rol)));
                    }
                    adjunto.setAdjuntosRoles(listadoRoles);

                    gateway.registrar((ObjetoCoreOT) adjunto);
                }

                guardarAdjuntoOt(repositorio, listArchivos, nombreCarpeta, rutaAlmacenar);

                gateway.commit();

            } catch (Exception e) {
                log.fatal("Error al crear Adjunto. ", e);
                gateway.rollback();
                throw e;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    public static void gestionarAdjuntoOt2(String nombreArchivo, String extensionArchivo, byte[] archivo, int usuarioId, int otId, int contador) throws Exception {
        Gateway gateway = new Gateway();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                Ot ot = (Ot) gateway.retornarObjetoCoreOT(Ot.class, otId);
                Hibernate.initialize(ot);

                List<Usuarios> usuarios = gateway.listarUsuariosPorId(usuarioId);

                Repositorios repositorio = (Repositorios) gateway.retornarObjetoCoreOT(Repositorios.class, usuarios.get(0).getRepositorio().getId());
                Hibernate.initialize(repositorio);

                Adjuntos adjunto = new Adjuntos();

                adjunto.setRepositorio(repositorio);
                adjunto.setOt(ot);
                adjunto.setNombre(nombreArchivo);
                //Es para cuando se cree un directorio para cada Ot
                //String carpetaOt = "ot-" + ot.getId() + "/";
                //adjLibroObra.setCarpeta(carpetaOt);

                String nombreCarpeta = "Ordenes de Trabajo";
                String rutaAlmacenar = "Ot-" + otId + "-Archivo-" + "_" + contador;
                adjunto.setCarpeta(repositorio.getUrl() + "/" + nombreCarpeta + "/" + rutaAlmacenar);
                adjunto.setExtension(extensionArchivo);
                gateway.registrar((ObjetoCoreOT) adjunto);
                gateway.commit();

                guardarAdjuntoOt2(repositorio, ot, nombreArchivo, archivo, nombreCarpeta, rutaAlmacenar);

            } catch (HibernateException e) {
                log.fatal("Error al crear LibroObra. ", e);
                gateway.rollback();
                throw e;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    private static void guardarAdjuntoOt2(Repositorios repo, Ot ot, String nombreArchivo, byte[] arrImagen, String nombreCarpeta, String rutaAlmacenar) throws Exception {
        String nombreFile = nombreArchivo;
        String SFTPHost = repo.getServidor();
        String SFTPUser = repo.getUsuario();
        String SFTPPassword = repo.getPassword();
        String SFTPDir = repo.getUrl() + "/" + nombreCarpeta + "/" + rutaAlmacenar;
        SecureTransmission.makeDirectory(SFTPHost, SFTPUser, SFTPPassword, SFTPDir);
        SecureTransmission.sendFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir, nombreFile, arrImagen);
    }

    private static void guardarAdjuntoOt(Repositorios repo, List<ArchivoAux> listArchivos, String nombreCarpeta, String rutaAlmacenar) throws Exception {

        String SFTPHost = repo.getServidor();
        String SFTPUser = repo.getUsuario();
        String SFTPPassword = repo.getPassword();
        String SFTPDir = repo.getUrl() + "/" + nombreCarpeta + "/" + rutaAlmacenar;
        SecureTransmission.makeDirectory(SFTPHost, SFTPUser, SFTPPassword, SFTPDir);

        for (ArchivoAux archivo : listArchivos) {
            String nombreFile = archivo.getNombre();
            SecureTransmission.sendFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir, nombreFile, archivo.getBaos().toByteArray());
        }
    } 
    
    public List<Adjuntos> obtenerAdjuntosPorOt(int otId) throws Exception {
        Gateway gateway = new Gateway();

        try {
            gateway.abrirSesion();
            try {                
                List<Adjuntos> listadoAdjuntos = gateway.obtenerAdjuntosPorOt(otId);            
                return listadoAdjuntos;
                
            } catch (HibernateException e) {
                log.fatal("Error al obtener adjuntos de OT "+otId, e);
                gateway.rollback();
                throw e;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }       

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static final Logger log = Logger.getLogger(AdjuntoDAO.class);
    private static final AdjuntoDAO INSTANCE = new AdjuntoDAO();

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
