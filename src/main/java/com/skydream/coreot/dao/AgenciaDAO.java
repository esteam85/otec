/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.Agencias;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

/**
 *
 * @author mcj
 */
public class AgenciaDAO implements GatewayDAO {

    private AgenciaDAO() {
    }

    public static AgenciaDAO getINSTANCE() {
        return INSTANCE;
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        Agencias agencia = (Agencias) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            //  setearEspecialidades(params, gateway, agencia);
            gateway.iniciarTransaccion();
            try {
                gateway.registrar(obj);
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al crear Agencia. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        Agencias agencia = (Agencias) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            //setearEspecialidades(params, gateway, agencia);
            gateway.iniciarTransaccion();
            try {
                gateway.actualizar2(obj);
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al actualizar Agencia. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    @Override
    public List listar(ObjetoCoreOT objetoCoreOT, Map params) throws Exception {
        Agencias agencia = (Agencias) objetoCoreOT;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = gate.listar(agencia, join);
            gate.validarYSetearObjetoARetornar(objetoCoreOT, join);
            for (ObjetoCoreOT objeto : listaRetorno) {
                Agencias age = (Agencias) objeto;
                //setearAgenciasEspecialidades(age);
                gate.validarYSetearObjetoARetornar(age, join);
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

//    private void setearEspecialidades(Map params, Gateway gateway, Agencias agencia) throws Exception {
//        if (params.containsKey("agenciasEspecialidadeses")) {
//            List<Map> listadoEspecialidades = (ArrayList<Map>) params.get("agenciasEspecialidadeses");
//            for (Map mapAgenEspe : listadoEspecialidades) {
//
//                Map especialidad = (HashMap) mapAgenEspe.get("especialidad");
//                int idEspecialidad = (Integer) especialidad.get("id");
//
//                Especialidades esp = (Especialidades) gateway.retornarObjetoCoreOT(Especialidades.class, idEspecialidad);
//                Hibernate.initialize(esp);
//
//                Integer precio = (Integer) mapAgenEspe.get("precio");
//
//                AgenciasEspecialidades ageEspe = new AgenciasEspecialidades(agencia, esp, precio);
//                agencia.getAgenciasEspecialidadeses().add(ageEspe);
//            }
//        } else {
//            throw new Exception("Para crear 1 agencia se debe enviar listado de 'agenciasEspecialidadeses'");
//        }
//    }
//    private void setearAgenciasEspecialidades(Agencias agencia) {
//        Set<AgenciasEspecialidades> agenEspe = agencia.getAgenciasEspecialidadeses();
//        Set<AgenciasEspecialidades> ageEsp = new HashSet<>();
//        for (AgenciasEspecialidades ae : agenEspe) {
//            Especialidades es = ae.getEspecialidad();
//            Especialidades especialidad = new Especialidades(es.getId(), es.getNombre(), es.getDescripcion(), es.getEstado());
//
//            AgenciasEspecialidades agenciaEspecialidad = new AgenciasEspecialidades(null, especialidad, ae.getPrecio());
//            ageEsp.add(agenciaEspecialidad);
//        }
//        agencia.setAgenciasEspecialidadeses(ageEsp);
//    }
    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static final Logger log = Logger.getLogger(AgenciaDAO.class);
    private static final AgenciaDAO INSTANCE = new AgenciaDAO();

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
