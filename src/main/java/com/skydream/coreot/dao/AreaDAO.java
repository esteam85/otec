/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.Areas;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import com.skydream.coreot.pojos.Usuarios;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

/**
 *
 * @author mcj
 */
public class AreaDAO implements GatewayDAO {

    private AreaDAO() {
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        boolean retornoOK = false;
        Areas area = (Areas) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                gateway.registrar(area);
                gateway.commit();
            } catch (HibernateException ex) {
                log.fatal("Error al crear Parametro. ", ex);
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return retornoOK;
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        boolean retornoOK = false;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                gateway.actualizar2(obj);
                gateway.commit();
            } catch (HibernateException ex) {
                log.fatal("Error al actualizar Parametro. ", ex);
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return retornoOK;
    }

    @Override
    public List listar(ObjetoCoreOT obj, Map params) throws Exception {
        Areas area = (Areas) obj;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = (List<ObjetoCoreOT>) gate.listar(area, join);
            join.add("usuarios");
            for (ObjetoCoreOT objeto : listaRetorno) {
                Areas areaAux = (Areas) objeto;
                gate.validarYSetearObjetoARetornar(areaAux, join);
                Set<Usuarios> listadoDetallesParametro = areaAux.getUsuarios();
                for (Usuarios usuario : listadoDetallesParametro) {
                    usuario.setArea(null);
                    usuario.setClave(null);
                }
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static AreaDAO getINSTANCE() {
        return INSTANCE;
    }

    private static final AreaDAO INSTANCE = new AreaDAO();
    private static final Logger log = Logger.getLogger(AreaDAO.class);

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
