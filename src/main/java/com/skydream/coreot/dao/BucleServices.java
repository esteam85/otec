/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mcj
 */
public class BucleServices {

    GatewayBucle gatewayBucle = new GatewayBucle();

    public List<Object> procesaPeticion(String servicio, String recurso, int regionId) throws Exception {

        List<Object> listado = new ArrayList<>();
        gatewayBucle.abrirSesion();

        switch (servicio) {
            case "Listar":

                switch (recurso) {
                    case "TipoOt":
                        listado = gatewayBucle.listarTipoOt();
                        break;

                    case "Comunas":
                        listado = gatewayBucle.listarComunasPorRegion(regionId);
                        break;
                }


        }

        return listado;
    }

}
