/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.Agencias;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import com.skydream.coreot.pojos.Contratos;
import com.skydream.coreot.pojos.ContratosProveedores;
import com.skydream.coreot.pojos.Proveedores;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;

/**
 *
 * @author mcj - rrr
 */
public class ContratoDAO implements GatewayDAO {

    private ContratoDAO() {
    }

    public static ContratoDAO getInstance() {
        return INSTANCE;
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        Contratos contrato = (Contratos) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearProveedores(params, gateway, contrato);
            setearAgencias(params, gateway, contrato);
            gateway.iniciarTransaccion();
            try {
                gateway.registrar(obj);
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al crear Contrato. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        Contratos contrato = (Contratos) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearProveedores(params, gateway, contrato);
            setearAgencias(params, gateway, contrato);
            gateway.iniciarTransaccion();
            try {
                gateway.actualizar2(obj);
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al actualizar Contrato. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    @Override
    public List listar(ObjetoCoreOT objetoCoreOT, Map params) throws Exception {
        Contratos contrato = (Contratos) objetoCoreOT;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = gate.listar(contrato, join);
            for (ObjetoCoreOT objeto : listaRetorno) {
                Contratos con = (Contratos) objeto;
                join.add("contratosAgencias");
                join.add("contratosProveedoreses");
                gate.validarYSetearObjetoARetornar(con, join);
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    private void setearProveedores(Map params, Gateway gateway, Contratos contrato) throws Exception {
        if (params.containsKey("contratosProveedoreses")) {
            List<Map> listadoProveedores = (ArrayList<Map>) params.get("contratosProveedoreses");
            for (Map mapProv : listadoProveedores) {

                Map proveedor = (HashMap) mapProv.get("proveedores");
                int idProveedor = (Integer) proveedor.get("id");

                Proveedores prov = (Proveedores) gateway.retornarObjetoCoreOT(Proveedores.class, idProveedor);
                Hibernate.initialize(prov);

                Long codigoAcuerdo = (Long) mapProv.get("codigoAcuerdo");
                Integer intcial1 = (Integer) mapProv.get("intcial1");
                Integer intcial2 = (Integer) mapProv.get("intcial2");
                Date fechaInicio = (Date) mapProv.get("fechaInicio");
                Date fechaTermino = (Date) mapProv.get("fechaTermino");
                Date fechaContable = (Date) mapProv.get("fechaContable");

                ContratosProveedores contProv = new ContratosProveedores(contrato, prov, codigoAcuerdo, intcial1, intcial2, fechaInicio, fechaTermino, fechaContable);
                contrato.getContratosProveedoreses().add(contProv);
            }
        }
    }

    private void setearAgencias(Map params, Gateway gateway, Contratos contrato) throws Exception {
        if (params.containsKey("contratosAgencias")) {
            List<Map> listadoAgencias = (ArrayList<Map>) params.get("contratosAgencias");
            for (Map agen : listadoAgencias) {
                Integer idAgen = (Integer) agen.get("id");
                Agencias age = (Agencias) gateway.obtenerDatoPorId(new Agencias(idAgen), null);
                contrato.getContratosAgencias().add(age);
            }
        }
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static final Logger log = Logger.getLogger(ContratoDAO.class);
    private static final ContratoDAO INSTANCE = new ContratoDAO();

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
