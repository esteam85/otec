/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.*;

import java.util.*;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;

/**
 *
 * @author rrr
 */
public class CubicadorDAO implements GatewayDAO {

    private CubicadorDAO() {
    }

    public static CubicadorDAO getINSTANCE() {
        return INSTANCE;
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        Cubicador cubicacion = (Cubicador) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                cubicacion.setOt(null);
                gateway.registrar(obj);
                Set<CubicadorServicios> cubicadorServiciosSet = retornarSetCubicadorServicios(params, gateway, cubicacion);

                for (CubicadorServicios servicio : cubicadorServiciosSet) {
                    gateway.registrar2(servicio);
                }

                Set<CubicadorMateriales> cubicadorMaterialesSet = retornarSetCubicadorMateriales(params, gateway, cubicacion);
                for (CubicadorMateriales material : cubicadorMaterialesSet) {
                    gateway.registrar2(material);
                }

                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al crear Cubicacion. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    public boolean crearCubicacion(Map params) throws Exception {
        Cubicador cubicacion = new Cubicador();
        String nombre = "";
        String descripcion = "";
        int idRegion = 0;
        int idProveedor = 0;
        int idContrato = 0;
        int idUsuario = 0;
        int idUnidadMedida = 0;

        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
                if (params.containsKey("nombre")) {
                    nombre = params.get("nombre").toString();
                }
                if (params.containsKey("descripcion")) {
                    descripcion = params.get("descripcion").toString();
                }

                if (params.containsKey("region")) {
                    Map objRegion = (Map) params.get("region");
                    idRegion = (int) objRegion.get("id");
                }

                if (params.containsKey("proveedor")) {
                    Map objProveedor = (Map) params.get("proveedor");
                    idProveedor = (int) objProveedor.get("id");
                }

                if (params.containsKey("contrato")) {
                    Map objContrato = (Map) params.get("contrato");
                    idContrato = (int) objContrato.get("id");
                }

                if (params.containsKey("usuario")) {
                    Map objUsuario = (Map) params.get("usuario");
                    idUsuario = (int) objUsuario.get("id");
                }

                Date fechaCreacion = new Date();

                Contratos contrato = new Contratos();
                contrato.setId(idContrato);

                Regiones region = null;
                if (idRegion > 0 && idContrato != 4) {
                    region = new Regiones();
                    region.setId(idRegion);
                }

                Usuarios usuario = new Usuarios();

                usuario.setId(idUsuario);

                Proveedores proveedor = new Proveedores();
                proveedor.setId(idProveedor);

                cubicacion.setProveedor(proveedor);

                cubicacion.setContrato(contrato);
                cubicacion.setRegion(region);
                cubicacion.setUsuario(usuario);
                cubicacion.setNombre(nombre);
                cubicacion.setDescripcion(descripcion);
                cubicacion.setFechaCreacion(fechaCreacion);
                cubicacion.setOt(null);

            try {
                gateway.registrar(cubicacion);

                if (idContrato == 2) {
                    Set<CubicadorOrdinario> cubicadorOrdinarioItemsSet = retornarSetCubicadorOrdinarioItems(params, gateway, cubicacion);
                    for (CubicadorOrdinario item : cubicadorOrdinarioItemsSet) {
                        gateway.registrar2(item);
                    }
                } else if(idContrato == 4){
                    List<CubicadorDetalle> cubicadorDetalles = retornarSetCubicadorDetalleItems(params, gateway, cubicacion);
                    for (CubicadorDetalle item : cubicadorDetalles) {
                        if(!gateway.registrarCubicadorDetalle(item)){
                            throw new Exception("Error al guarder CubicadorDetalle contrato unificado");
                        };
                    }
                } else if(idContrato == 5){
                    Set<CubicadorServicios> cubicadorServiciosSet = retornarSetCubicadorServicios(params, gateway, cubicacion);
                    for (CubicadorServicios servicio : cubicadorServiciosSet) {
                        gateway.registrar2(servicio);
                    }
                }
                // Contrato Bucle id = 9
                else if(idContrato == 9){
//                    List<CubicadorDetalleBucle> cubicadorDetalles = retornarSetCubicadorBucle(params, gateway, cubicacion);
//                    for (CubicadorDetalle item : cubicadorDetalles) {
//                        if(!gateway.registrarCubicadorDetalle(item)){
//                            throw new Exception("Error al guarder CubicadorDetalle contrato unificado");
//                        };
//                    }
                } else{
                    Set<CubicadorServicios> cubicadorServiciosSet = retornarSetCubicadorServicios(params, gateway, cubicacion);
                    for (CubicadorServicios servicio : cubicadorServiciosSet) {
                        gateway.registrar2(servicio);
                    }

                    Set<CubicadorMateriales> cubicadorMaterialesSet = retornarSetCubicadorMateriales(params, gateway, cubicacion);
                    for (CubicadorMateriales material : cubicadorMaterialesSet) {
                        gateway.registrar2(material);
                    }
                }

                gateway.commit();
            } catch (Exception e) {
                log.fatal("Error al crear cubicacion. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    public boolean crearCubicacionBucle(Map params) throws Exception {

        Cubicador cubicacion = new Cubicador();
        String nombre = "";
        String descripcion = "";
        int idRegion = 0;
        int idAgencia = 0;
        int idContrato = 0;
        int idUsuario = 0;
        int idUnidadMedida = 0;
        int idActividad = 0;
        int idEspecialidad = 0;
        int idProveedor = 0;
        List materialesContratistaList = new ArrayList<>();
        List<Map> listaMatrialesAux = new ArrayList<>();
        Map materialesContratistaMap = new HashMap();

        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            if (params.containsKey("nombre")) {
                nombre = params.get("nombre").toString();
            }
            if (params.containsKey("descripcion")) {
                descripcion = params.get("descripcion").toString();
            }

            if (params.containsKey("region")) {
                Map objRegion = (Map) params.get("region");
                idRegion = (int) objRegion.get("id");
            }

            Regiones region = null;
            if (idRegion > 0) {
                region = new Regiones();
                region.setId(idRegion);
            }


            if (params.containsKey("agencia")) {
                Map objAgencia = (Map) params.get("agencia");
                idAgencia = (int) objAgencia.get("id");
                Agencias agencia = new Agencias();
                agencia.setId(idAgencia);
                cubicacion.setAgencia(agencia);
            }


            if (params.containsKey("proveedor")) {
                Map objProveedor = (Map) params.get("proveedor");
                idProveedor = (int) objProveedor.get("id");
                Proveedores proveedor = new Proveedores();
                proveedor.setId(idProveedor);
                cubicacion.setProveedor(proveedor);
            }

            if (params.containsKey("contrato")) {
                Map objContrato = (Map) params.get("contrato");
                idContrato = (int) objContrato.get("id");
            }

            if (params.containsKey("usuario")) {
                Map objUsuario = (Map) params.get("usuario");
                idUsuario = (int) objUsuario.get("id");
            }

            //Bucle
            double subTotalPuntosBaremos = 0;
            List<ServicioBucle> listaDeServiciosBucleConfirmados = new ArrayList<ServicioBucle>();
            if (params.containsKey("cubicadorServicios")) {
                List<Map> listadoServicios = (ArrayList<Map>) params.get("cubicadorServicios");
                //Recorrer los servicios para comenzar validaciones.
                for (Map mapServ : listadoServicios) {
                    int idServicio = (Integer) mapServ.get("id");
                    //ver si este servicio le corresponde su agencia, su especialidad, su contrato.
                    // Si no arroja excepción es porque esta correcto
                    Servicios servicio = gateway.validarServicioAgenciaEspecialidadBucle(idServicio,idAgencia,idContrato);

                    if (mapServ.containsKey("actividad")) {
                        Map objActividad = (Map) mapServ.get("actividad");
                        idActividad = (int) objActividad.get("id");
                    }

                    if (mapServ.containsKey("especialidad")) {
                        Map objEspecialidad = (Map) mapServ.get("especialidad");
                        idEspecialidad = (int) objEspecialidad.get("id");
                    }
                    if (servicio.getEspecialidadId() != 9){

                        subTotalPuntosBaremos += servicio.getPuntosBaremos();
                    }

                    //Validar que los servicios Unidad correspondan a su Servicio.
                    List<Map> listadoServiciosUnidadSeleccionados = (ArrayList<Map>) mapServ.get("servicios_unidad_seleccionados");
                    List<ServiciosUnidadBucle> serviciosUnidadBucleConfirmados = new ArrayList<>();
                    if(listadoServiciosUnidadSeleccionados != null){

                    for (Map mapServUnidad : listadoServiciosUnidadSeleccionados) {
                        int idServicioUnidad = (Integer) mapServUnidad.get("id");
                        // Si no arroja excepción es porque esta correcto
                        ServiciosUnidad serviciosUnidad = gateway.validadarServicioConServiciosUnidadBucle(servicio,idServicioUnidad,idActividad);
                        serviciosUnidadBucleConfirmados.add(new ServiciosUnidadBucle(serviciosUnidad,(Integer) mapServUnidad.get("cantidad"),gateway.obtenerPrecioServicioUnidadBucle(serviciosUnidad)));
                    }
                    }
                    CubicadorDireccionesBucle direccion = new CubicadorDireccionesBucle((String) params.get("desdeNombre"), (String) params.get("hastaNombre"), (int) params.get("desdeAltura"), (int) params.get("hastaAltura"));
                    listaDeServiciosBucleConfirmados.add(new ServicioBucle((Integer) mapServ.get("cantidad"),idActividad,servicio,direccion,serviciosUnidadBucleConfirmados));

                }
            }


            Map otros = (Map) params.get("otros");

            boolean cubicadorExtrasBucle = false;
            CubicadorExtrasBucle cubicadorExtras = new CubicadorExtrasBucle();
            if (otros != null) {
                cubicadorExtrasBucle = true;
//                double subTotalPuntosBaremos = Double.parseDouble(otros.get("subTotalPuntosBaremos").toString());
                int precioOtros = 0;
                int precioViaticos = 0;
                int precioVehiculos = 0;
                int precioPermisos = 0;
                int sobreTiempo = 0;
                boolean modificacionVial = false;
                if (otros.containsKey("otros")) {
                    precioOtros = Integer.parseInt(otros.get("otros").toString());
                }
                if (otros.containsKey("vitiacos")) {
                    precioViaticos = Integer.parseInt(otros.get("vitiacos").toString());
                }
                if (otros.containsKey("sobreTiempo")) {
                    sobreTiempo = Integer.parseInt(otros.get("sobreTiempo").toString());
                }
                if (otros.containsKey("vehiculos")) {
                    precioVehiculos = Integer.parseInt(otros.get("vehiculos").toString());
                }
                if (otros.containsKey("permisos")) {
                    precioPermisos = Integer.parseInt(otros.get("permisos").toString());
                }
                if (otros.containsKey("modificacionVial")) {
                modificacionVial = (boolean)otros.get("modificacionVial");
                }
                cubicadorExtras.setOtros(precioOtros);
                cubicadorExtras.setPermisos(precioPermisos);
                cubicadorExtras.setVehiculos(precioVehiculos);
                cubicadorExtras.setSobreTiempo(sobreTiempo);
                cubicadorExtras.setViaticos(precioViaticos);
                if(modificacionVial){
                    cubicadorExtras.setModificacionVial(obtenerValorModificacionVial(idAgencia, subTotalPuntosBaremos));
                }else{
                    cubicadorExtras.setModificacionVial(0.0);
                }

            }


            Date fechaCreacion = new Date();
            Contratos contrato = new Contratos();
            contrato.setId(idContrato);
            Usuarios usuario = new Usuarios();
            usuario.setId(idUsuario);
            cubicacion.setContrato(contrato);
            cubicacion.setRegion(region);
            cubicacion.setUsuario(usuario);
            cubicacion.setNombre(nombre);
            cubicacion.setDescripcion(descripcion);
            cubicacion.setFechaCreacion(fechaCreacion);
            cubicacion.setOt(null);

            CubicadorBucle cubicadorBucle = new CubicadorBucle(cubicacion,listaDeServiciosBucleConfirmados);
            try {
                //Esta validado que los servicios le corresponden su agencia y especialidad. Por lo tanto se puede registrar la cubicación.
                //Registramos los servicios.
                boolean retorno = registrarCubicadorServiciosBucle(cubicadorBucle, gateway,cubicadorExtras,cubicadorExtrasBucle);

                gateway.commit();
//                throw new Exception("pruebas bucle");


//                    List<HashMap> serviciosCubicacionBucle = retornarServiciosUnidadesCubicacion(params, gateway, cubicacion, region, idEspecialidad, idContrato, idAgencia);
            } catch (Exception e) {
                log.fatal("Error al crear cubicacion. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }


    public int crearCubicacionLlaveEnMano(Map params) throws Exception {
        Cubicador cubicacion = new Cubicador();
        String nombre = "";
        String descripcion = "";
        int idRegion = 0;
        int idProveedor = 0;
        int idContrato = 0;
        int idUsuario = 0;
        int idUnidadMedida = 0;
        int idCubicacion = 0;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            if (params.containsKey("nombre")) {
                nombre = params.get("nombre").toString();
            }
            if (params.containsKey("descripcion")) {
                descripcion = params.get("descripcion").toString();
            }

            if (params.containsKey("region")) {
                Map objRegion = (Map) params.get("region");
                idRegion = (int) objRegion.get("id");
            }

            if (params.containsKey("proveedor")) {
                Map objProveedor = (Map) params.get("proveedor");
                idProveedor = (int) objProveedor.get("id");
            }

            if (params.containsKey("contrato")) {
                Map objContrato = (Map) params.get("contrato");
                idContrato = (int) objContrato.get("id");
            }

            if (params.containsKey("usuario")) {
                Map objUsuario = (Map) params.get("usuario");
                idUsuario = (int) objUsuario.get("id");
            }

            Date fechaCreacion = new Date();

            Contratos contrato = new Contratos();
            contrato.setId(idContrato);

            Regiones region = null;
            if (idRegion > 0 && idContrato != 4) {
                region = new Regiones();
                region.setId(idRegion);
            }

            Usuarios usuario = new Usuarios();

            usuario.setId(idUsuario);

            Proveedores proveedor = new Proveedores();
            proveedor.setId(idProveedor);

            cubicacion.setProveedor(proveedor);

            cubicacion.setContrato(contrato);
            cubicacion.setRegion(region);
            cubicacion.setUsuario(usuario);
            cubicacion.setNombre(nombre);
            cubicacion.setDescripcion(descripcion);
            cubicacion.setFechaCreacion(fechaCreacion);
            cubicacion.setOt(null);

            try {
                gateway.registrar(cubicacion);
                Set<CubicadorServicios> cubicadorServiciosSet = retornarSetCubicadorServicios(params, gateway, cubicacion);
                for (CubicadorServicios servicio : cubicadorServiciosSet) {
                    gateway.registrar2(servicio);
                }

                Set<CubicadorMateriales> cubicadorMaterialesSet = retornarSetCubicadorMaterialesLLave(params, gateway, cubicacion);
                for (CubicadorMateriales material : cubicadorMaterialesSet) {
                    gateway.registrar2(material);
                }

                // el commit lo hare despues de crear OT ya que se hacen las 2 cosas en 1 llamado
                gateway.commit();

                idCubicacion = cubicacion.getId();
            } catch (Exception e) {
                log.fatal("Error al crear cubicacion de llave en mano. ", e);
                gateway.rollback();
                throw e;
            }
            return idCubicacion;
        } catch (Exception e) {
            throw e;
        } finally {
//            gateway.cerrarSesion();
        }
    }


    private List<CubicadorDetalle> retornarSetCubicadorDetalleItems(Map params, Gateway gateway, Cubicador cubicador) throws Exception {
        List<CubicadorDetalle> setCubicadorDetalle = new ArrayList<CubicadorDetalle>();
        List<Map> listadoServicios = (ArrayList<Map>) params.get("cubicadorServicios");
        for (Map mapServ : listadoServicios) {
            Map servicioMap = (HashMap) mapServ.get("servicio");

            int idServicio = (Integer) servicioMap.get("id");
            double precio = 1.0 * ((Integer) servicioMap.get("precio"));
            boolean isPackBasico = (boolean) servicioMap.get("isPackBasico");
            Map tipoMonedaMap = (HashMap) servicioMap.get("tipoMoneda");

            // se comenta esta linea por incidencia ya que no todos los servicios tienen codigo de alcance
          //  Map alcancesMap = (HashMap) servicioMap.get("alcances");

            Map regionMap = (HashMap) servicioMap.get("region");
            double cantidad = 1.0 * ((Integer) mapServ.get("cantidad"));
            double total = 1.0 * ((Integer) mapServ.get("total"));

            int idTipoMoneda = (Integer) tipoMonedaMap.get("id");

            // se comenta esta linea por incidencia ya que no todos los servicios tienen codigo de alcance
           // String codAlcances = alcancesMap.get("codigo").toString();

            int idRegion = (Integer) regionMap.get("id");

            Servicios servicio = (Servicios) gateway.retornarObjetoCoreOT(Servicios.class, idServicio);
            Hibernate.initialize(servicio);

            CubicadorDetalle cubicadorDetalle = new CubicadorDetalle();
            cubicadorDetalle.setServicios(servicio);
            cubicadorDetalle.setCubicadorId(cubicador.getId());
            cubicadorDetalle.setRegionId(idRegion);
            cubicadorDetalle.setTipoMonedaId(idTipoMoneda);
            cubicadorDetalle.setCantidad(cantidad);
            cubicadorDetalle.setPrecio(precio);
            cubicadorDetalle.setTotal(total);
            Date date = new Date();
            cubicadorDetalle.setFechaCreacion(date);

            setCubicadorDetalle.add(cubicadorDetalle);

        }
        return setCubicadorDetalle;
    }



    private List<ServiciosUnidad> retornarServiciosUnidadesCubicacion(Map params, Gateway gateway, Cubicador cubicador, Regiones region, int idEspecialidad, int idContrato, int idAgencia) throws Exception {
        List<CubicadorDetalle> setCubicadorDetalle = new ArrayList<CubicadorDetalle>();
        List<Map> listadoServicios = (ArrayList<Map>) params.get("cubicadorServicios");
        for (Map mapServ : listadoServicios) {
            Map servicioMap = (HashMap) mapServ.get("servicio");

            int idServicio = (Integer) servicioMap.get("id");
            double cantidad = 1.0 * ((Integer) mapServ.get("cantidad"));

            Servicios servicio = (Servicios) gateway.retornarObjetoCoreOT(Servicios.class, idServicio);
            Hibernate.initialize(servicio);

            // obtengo el precio del servicio segun cantidad
            double precioServicio = gateway.obtenerPrecioServicioPorEspecialidad(servicio, idEspecialidad, idContrato, idAgencia);


            // obtengo el precio total de los materiales
            Map serviciosUnidadMap = (HashMap) servicioMap.get("servicios_unidad_seleccionados");
            int idMaterial = Integer.parseInt(serviciosUnidadMap.get("id").toString());




//
//
//            cubicadorDetalle.setPrecio(precioServicio);
//            cubicadorDetalle.setTotal(precioServicio * cantidad);
//            Date date = new Date();
//            cubicadorDetalle.setFechaCreacion(date);
//
//            setCubicadorDetalle.add(cubicadorDetalle);

        }
        return new List<ServiciosUnidad>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<ServiciosUnidad> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(ServiciosUnidad serviciosUnidad) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends ServiciosUnidad> c) {
                return false;
            }

            @Override
            public boolean addAll(int index, Collection<? extends ServiciosUnidad> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public ServiciosUnidad get(int index) {
                return null;
            }

            @Override
            public ServiciosUnidad set(int index, ServiciosUnidad element) {
                return null;
            }

            @Override
            public void add(int index, ServiciosUnidad element) {

            }

            @Override
            public ServiciosUnidad remove(int index) {
                return null;
            }

            @Override
            public int indexOf(Object o) {
                return 0;
            }

            @Override
            public int lastIndexOf(Object o) {
                return 0;
            }

            @Override
            public ListIterator<ServiciosUnidad> listIterator() {
                return null;
            }

            @Override
            public ListIterator<ServiciosUnidad> listIterator(int index) {
                return null;
            }

            @Override
            public List<ServiciosUnidad> subList(int fromIndex, int toIndex) {
                return null;
            }
        };
    }


    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        Cubicador cubicacion = (Cubicador) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            Set<CubicadorServicios> cubicadorServiciosSet = retornarSetCubicadorServicios(params, gateway, cubicacion);
            cubicacion.setCubicadorServicios(cubicadorServiciosSet);
            Set<CubicadorMateriales> cubicadorMaterialesSet = retornarSetCubicadorMateriales(params, gateway, cubicacion);
            cubicacion.setCubicadorMateriales(cubicadorMaterialesSet);
            gateway.iniciarTransaccion();
            try {
                gateway.actualizar2(obj);
//                for(CubicadorServicios cubicadorServicios : cubicadorServiciosSet){
//                    gateway.actualizar2(cubicadorServicios);
//                }
//                for(CubicadorMateriales cubicadorMateriales : cubicadorMaterialesSet){
//                    gateway.actualizar2(cubicadorMateriales);
//                }

                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al actualizar Cubicacion. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }



    @Override
    public List listar(ObjetoCoreOT obj, Map params) throws Exception {
        Cubicador cubicacion = (Cubicador) obj;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = gate.listar(cubicacion, join);
            for (ObjetoCoreOT objeto : listaRetorno) {
                Cubicador cub = (Cubicador) objeto;
                join.add("cubicadorServicios");
                join.add("cubicadorMateriales");
                gate.validarYSetearObjetoARetornar(cub, join);
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    private Set<CubicadorServicios> retornarSetCubicadorServicios(Map params, Gateway gateway, Cubicador cubicador) throws Exception {
        Set<CubicadorServicios> setCubicadorServicios = new HashSet<>();
        if (params.containsKey("cubicadorServicios")) {
            List<Map> listadoServicios = (ArrayList<Map>) params.get("cubicadorServicios");
            for (Map mapServ : listadoServicios) {

                Map servicio = (HashMap) mapServ.get("servicio");
                int idServicio = (Integer) servicio.get("id");

                Servicios serv = (Servicios) gateway.retornarObjetoCoreOT(Servicios.class, idServicio);
                Hibernate.initialize(servicio);

                Object  objCantidad = mapServ.get("cantidad");
                Double cantidad = 0d;
                if(objCantidad instanceof  Double){
                    cantidad = (Double)objCantidad;
                }else cantidad = ((Integer)objCantidad).doubleValue();

                Object objPrecio = servicio.get("precio");
                Double precio;
                if(objPrecio instanceof Double){
                    precio = (Double) objPrecio;
                }else precio = ((Integer)objPrecio).doubleValue();

                Double monto = precio * cantidad;

                CubicadorServiciosId cubicadorServicioId = new CubicadorServiciosId(cubicador.getId(), serv.getId());
                CubicadorServicios cubSer = new CubicadorServicios(cubicadorServicioId, cubicador, serv, cantidad, monto, precio);
                setCubicadorServicios.add(cubSer);
//                cubicador.getCubicadorServicios().add(cubSer);
            }
        }
        return setCubicadorServicios;
    }

//bucle
    private boolean registrarCubicadorServiciosBucle(CubicadorBucle cubicadorBucle, Gateway gateway, CubicadorExtrasBucle cubicadorExtras, boolean cubicadorExtrasBucle) throws Exception {

        gateway.registrar(cubicadorBucle.getCubicador());
        for (ServicioBucle servicioBucle : cubicadorBucle.getServiviosBucle()) {

            gateway.registrarCubicadorDireccionesBucle(servicioBucle.getDireccion());
            CubicadorServiciosBucle cubicadorServiciosBucle = new CubicadorServiciosBucle(cubicadorBucle.getCubicador().getId(), servicioBucle.getServicio().getId(), servicioBucle.getCantidad(), servicioBucle.getTotal(), servicioBucle.getServicio().getPrecio(), servicioBucle.getActividadId(),servicioBucle.getServicio().getEspecialidadId(),servicioBucle.getDireccion());

            gateway.registrarCubicadorServiciosBucle(cubicadorServiciosBucle);

            List<CubicadorServiciosServiciosUnidad> cubicadorServiciosServiciosUnidadList = new ArrayList<>();

            cubicadorServiciosServiciosUnidadList = registrarCubicadorServiciosUnidad(cubicadorServiciosBucle.getId(),servicioBucle.getServiciosUnidadBucle(),gateway);

            registrarCubicadorMaterialesBucle(cubicadorServiciosServiciosUnidadList,gateway);

        }

        if(cubicadorExtrasBucle){
            int idCubicacion = cubicadorBucle.getCubicador().getId();
            cubicadorExtras.setCubicadorId(idCubicacion);
            gateway.registrarCubicadorExtra(cubicadorExtras);
        }


        return true;
    }

    private List<CubicadorServiciosServiciosUnidad> registrarCubicadorServiciosUnidad(int id, List<ServiciosUnidadBucle> serviciosUnidadBucleList, Gateway gateway) throws Exception {
        List<CubicadorServiciosServiciosUnidad> cubicadorServiciosServiciosUnidadList = new ArrayList<>();
        for (ServiciosUnidadBucle servicioUnidadBucle : serviciosUnidadBucleList){

            CubicadorServiciosServiciosUnidad cubicadorServiciosServiciosUnidad = new CubicadorServiciosServiciosUnidad(id,servicioUnidadBucle.getId(),servicioUnidadBucle.getCantidad(),servicioUnidadBucle.getPrecio(),servicioUnidadBucle.getTotal());
            cubicadorServiciosServiciosUnidad = gateway.registrarCubicadorServiciosServiciosUnidad(cubicadorServiciosServiciosUnidad);
            cubicadorServiciosServiciosUnidadList.add(cubicadorServiciosServiciosUnidad);
        }
        return cubicadorServiciosServiciosUnidadList;

    }

    private void registrarCubicadorMaterialesBucle(List<CubicadorServiciosServiciosUnidad> cubicadorServiciosServiciosUnidadList, Gateway gateway) throws Exception {
        for(CubicadorServiciosServiciosUnidad cubicadorServiciosServiciosUnidad: cubicadorServiciosServiciosUnidadList){
            List<ServiciosUnidad> serviciosUnidadList = new ArrayList<>();
            String filtro = "codigoUnidadObra";
            serviciosUnidadList = gateway.obtieneServiciosUnidadList(cubicadorServiciosServiciosUnidad, filtro);

            for(ServiciosUnidad serviciosUnidad: serviciosUnidadList){
                List<ServiciosUnidadMateriales> serviciosUnidadMaterialesList = new ArrayList<>();
                if(!serviciosUnidad.getCodigo().equals("0")){
                    serviciosUnidadMaterialesList = gateway.obtieneMaterialesLis(serviciosUnidad.getCodigo());
                    gateway.guardaMaterialesBucleSegunServicioUnidad(serviciosUnidadMaterialesList, cubicadorServiciosServiciosUnidad.getId());

                }
            }
        }
    }


    private Set<CubicadorMateriales> retornarSetCubicadorMateriales(Map params, Gateway gateway, Cubicador cubicador) throws Exception {
        Set<CubicadorMateriales> setCubicadorMateriales = new HashSet<>();
        if (params.containsKey("cubicadorMateriales")) {
            List<Map> listadoMateriales = (ArrayList<Map>) params.get("cubicadorMateriales");
            for (Map mapMate : listadoMateriales) {

                Map material = (HashMap) mapMate.get("material");
                int idMaterial = (Integer) material.get("id");

                Materiales mate = (Materiales) gateway.retornarObjetoCoreOT(Materiales.class, idMaterial);
                Hibernate.initialize(material);

                Integer cantidad = (Integer) mapMate.get("cantidad");
                Integer montoTotal = (Integer) mapMate.get("montoTotal");

                CubicadorMaterialesId cubicadorMaterialId = new CubicadorMaterialesId(cubicador.getId(), mate.getId());
                CubicadorMateriales cubiMate = new CubicadorMateriales(cubicadorMaterialId, cubicador, mate, cantidad, montoTotal);
                setCubicadorMateriales.add(cubiMate);
//                cubicador.getCubicadorMateriales().add(cubiMate);
            }
        }
        return setCubicadorMateriales;
    }

    private Set<CubicadorMateriales> retornarSetCubicadorMaterialesLLave(Map params, Gateway gateway, Cubicador cubicador) throws Exception {
        Set<CubicadorMateriales> setCubicadorMateriales = new HashSet<>();
        if (params.containsKey("cubicadorMateriales")) {
            List<Map> listadoMateriales = (ArrayList<Map>) params.get("cubicadorMateriales");
            for (Map mapMate : listadoMateriales) {

                Map material = (HashMap) mapMate.get("material");
                int idMaterial = (Integer) material.get("id");

                Materiales mate = (Materiales) gateway.retornarObjetoCoreOT(Materiales.class, idMaterial);
                Hibernate.initialize(material);

                Double cantidad = (Double) mapMate.get("cantidad");

                CubicadorMaterialesId cubicadorMaterialId = new CubicadorMaterialesId(cubicador.getId(), mate.getId());
                CubicadorMateriales cubiMate = new CubicadorMateriales(cubicadorMaterialId, cubicador, mate, cantidad, 0.0);
                setCubicadorMateriales.add(cubiMate);
//                cubicador.getCubicadorMateriales().add(cubiMate);
            }
        }
        return setCubicadorMateriales;
    }

    private Set<CubicadorOrdinario> retornarSetCubicadorOrdinarioItems(Map params, Gateway gateway, Cubicador cubicador) throws Exception {
        Set<CubicadorOrdinario> setCubicadorOrdinarioItems = new HashSet<>();
        if (params.containsKey("cubicadorOrdinario")) {
            List<Map> listadoItems = (ArrayList<Map>) params.get("cubicadorOrdinario");
            for (Map mapItems : listadoItems) {

                Map tipoMoneda_ = (HashMap) mapItems.get("tipoMoneda");
                int idMoneda = (Integer) tipoMoneda_.get("id");

//                TipoMoneda tipoMoneda = (TipoMoneda) gateway.retornarObjetoCoreOT(TipoMoneda.class, idMoneda);
//                Hibernate.initialize(tipoMoneda);
                TipoMoneda tipoMoneda = new TipoMoneda();
                tipoMoneda.setId(idMoneda);

                String item = (String) mapItems.get("item");

                Object cantidadAux = mapItems.get("cantidad");
                Double cantidad;
                if(cantidadAux instanceof Integer){
                    cantidad = ((Integer)cantidadAux).doubleValue();
                }else cantidad = (Double)cantidadAux;

//                Integer precioAux = (Integer) mapItems.get("precio");
                Number precioAux = null;
                Object varAuxPrecio = mapItems.get("precio");
                if (varAuxPrecio instanceof Integer) {
                    precioAux = (Integer) mapItems.get("precio");
                } else if (varAuxPrecio instanceof Double) {
                    precioAux = (Double) mapItems.get("precio");
                }
                Double precio = precioAux.doubleValue();

                int totalAux = 0;
                Number totalAux2 = null;
                Double total = 0.0;
                Object varAuxTotal = mapItems.get("total");
                if (varAuxTotal instanceof Integer) {
                    totalAux = (Integer) mapItems.get("total");
                    total = 1.0 * totalAux;
                } else if (varAuxTotal instanceof Double) {
                    totalAux2 = (Double) mapItems.get("total");
                    total = (Double) totalAux2;
                }
                Map objUnidadMedida = (Map) mapItems.get("tipoUnidadMedida");
                int idUnidadMedida = (int) objUnidadMedida.get("id");

                TipoUnidadMedida tipoUnidadMedida = new TipoUnidadMedida();
                tipoUnidadMedida.setId(idUnidadMedida);

                CubicadorOrdinario cubOrdinario = new CubicadorOrdinario(cubicador, tipoMoneda, item, cantidad, precio, total, new Date(), tipoUnidadMedida);
                setCubicadorOrdinarioItems.add(cubOrdinario);
            }
        }
        return setCubicadorOrdinarioItems;
    }

    public List<Cubicador> listarCubicacionesPorUsuarioId(int usuarioId) throws Exception {
        List<Cubicador> listadoOts = null;
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();

            listadoOts = gate.listarCubicacionesPorUsuarioId(usuarioId);
            for (Cubicador cub : listadoOts) {
                Ot ot = new Ot();
                cub.setOt(ot);
                if (cub.getContrato().getId() == 2) {
                    join.add("cubicadorOrdinario");
                } else {
                    join.add("cubicadorServicios");
                    join.add("cubicadorMateriales");
                    join.add("region");
                }

                gate.validarYSetearObjetoARetornar(cub, join);
                if (cub.getContrato().getId() == 2) {

                } else {
                    Set<CubicadorServicios> cs = cub.getCubicadorServicios();
                    for (CubicadorServicios csAux : cs) {
                        csAux.setServicio(null);
                    }

                    Set<CubicadorMateriales> cm = new HashSet<>();
                    for (CubicadorMateriales cmAux : cm) {
                        cmAux.setMaterial(null);
                    }
                }

            }

        } finally {
            gate.cerrarSesion();
        }
        return listadoOts;
    }

    public List<Cubicador> listarCubicacionesPorUsuarioIdPaginado(int usuarioId, int pos, int cantidad, String filtro, String orden) throws Exception {
        List<Cubicador> listadoOts = null;
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();

            listadoOts = gate.listarCubicacionesPorUsuarioIdPaginado(usuarioId, pos, cantidad, filtro, orden);
            for (Cubicador cub : listadoOts) {
                Ot ot = new Ot();
                cub.setOt(ot);
                if (cub.getContrato().getId() == 2) {
                    join.add("cubicadorOrdinario");
                } else {
                    join.add("cubicadorServicios");
                    join.add("cubicadorMateriales");
                    join.add("region");
                }

                gate.validarYSetearObjetoARetornar(cub, join);
                if (cub.getContrato().getId() == 2) {

                } else {
                    Set<CubicadorServicios> cs = cub.getCubicadorServicios();
                    for (CubicadorServicios csAux : cs) {
                        csAux.setServicio(null);
                    }

                    Set<CubicadorMateriales> cm = new HashSet<>();
                    for (CubicadorMateriales cmAux : cm) {
                        cmAux.setMaterial(null);
                    }
                }

            }

        } finally {
            gate.cerrarSesion();
        }
        return listadoOts;
    }

    public int obtenerTotalCubicaciones(int usuarioEjecutorId) throws Exception {
        int total = 0;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            total = gate.obtenerTotalCubicaciones(usuarioEjecutorId);
        } finally {
            gate.cerrarSesion();
        }
        return total;
    }

    public Cubicador obtenerDetalleCubicacion(int cubicacionId) throws Exception {
        Cubicador cubicacion = new Cubicador();
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();
            cubicacion = gate.obtenerDetalleCubicacion(cubicacionId);
        } finally {
            gate.cerrarSesion();
        }
        return cubicacion;
    }

    public Cubicador obtenerDetalleCubicacionLLave(int cubicacionId, int otId) throws Exception {
        Cubicador cubicacion = new Cubicador();
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();
            cubicacion = gate.obtenerDetalleCubicacionLLave(cubicacionId, otId);
        } finally {
            gate.cerrarSesion();
        }
        return cubicacion;
    }

    public Cubicador obtenerDetalleCubicacionPorOt(int otId) throws Exception {
        Cubicador cubicacion = new Cubicador();
        Cubicador cubicacionId = new Cubicador();
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();
            cubicacionId = gate.obtenerCubicadorPorOtId(otId);
            cubicacion = gate.obtenerDetalleCubicacion(cubicacionId.getId());
        } finally {
            gate.cerrarSesion();
        }
        return cubicacion;
    }
    public List<CubicadorDetalle> obtenerCubicadorDetalle(int cubicadorId) throws Exception {
        List<CubicadorDetalle> cubicadorDetalle = new ArrayList<CubicadorDetalle>();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            cubicadorDetalle = gate.obtenerCubicadorDetallePorIdCubicador(cubicadorId);
        } finally {
            gate.cerrarSesion();
        }
        return cubicadorDetalle;
    }

    public List<Cubicador> obtenerCubicacionPorIdOt(int otId) throws Exception {
        List<Cubicador> listadoOts = null;
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();

            listadoOts = gate.obtenerCubicacionPorIdOt(otId);
            for (Cubicador cub : listadoOts) {
                Ot ot = new Ot();
                ot.setId(otId);
                cub.setOt(ot);
                join.add("cubicadorServicios");
                join.add("cubicadorMateriales");
                gate.validarYSetearObjetoARetornar(cub, join);
                //gate.validarYSetearObjetoARetornar(cub, null);
            }

        } finally {
            gate.cerrarSesion();
        }
        return listadoOts;
    }
    public List<DetalleItemsOrdinario> obtenerCubicacionOrdinarioPorIdOt(int otId, Actas ultimaActa) throws Exception {
        List<DetalleItemsOrdinario> listaDetalleItemsOrdinario = new ArrayList<DetalleItemsOrdinario>();
        DetalleItemsOrdinario detalleItemsOrdinario = new DetalleItemsOrdinario();
        DetalleCubicadorOrdinarioActas detalleCubicadorOrdinarioActas = null;
        Gateway gate = new Gateway();
        Cubicador cubicador = new Cubicador();
        List<CubicadorOrdinario> listaMaterialesCubicacionordinaria = new ArrayList<CubicadorOrdinario>();
        List<Actas> actasOld = new ArrayList<Actas>();
        boolean existe = false;
        try {
            if(ultimaActa.getId() > 0){
                existe = true;
            } else {
                existe = false;
            }

        } catch (Exception e){
            e.printStackTrace();
            existe = false;
        }

        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();
            cubicador = gate.obtenerCubicadorPorOtId(otId);
            listaMaterialesCubicacionordinaria = gate.obtenerListaCubicacionOrdinariaPorIdOt(cubicador.getId());

            for(CubicadorOrdinario listaCubOrd : listaMaterialesCubicacionordinaria){
                detalleItemsOrdinario = new DetalleItemsOrdinario();
                detalleCubicadorOrdinarioActas = new DetalleCubicadorOrdinarioActas();

                if(existe){
                    detalleCubicadorOrdinarioActas = gate.obtieneItemsOrdinario(listaCubOrd.getId(), ultimaActa.getId());
                } else {
                    detalleCubicadorOrdinarioActas.setTotalMontoCubicado(0.0);
                    detalleCubicadorOrdinarioActas.setTotalMontoEjecucion(0.0);
                    detalleCubicadorOrdinarioActas.setTotalUnidadesCubicadas(0.0);
                    detalleCubicadorOrdinarioActas.setTotalUnidadesEjecucionActa(0.0);
                    detalleCubicadorOrdinarioActas.setTotalUnidadesEjecucionOt(0.0);
                }
                detalleItemsOrdinario.setCubicadorOrdinario(listaCubOrd);
                detalleItemsOrdinario.setDetalleCubicadorOrdinarioActas(detalleCubicadorOrdinarioActas);
                listaDetalleItemsOrdinario.add(detalleItemsOrdinario);
            }
        } finally {
            gate.cerrarSesion();
        }
        return listaDetalleItemsOrdinario;
    }
    public List<DetalleServiciosUnificado> obtenerCubicacionUnificadaPorIdOt(int otId, Actas ultimaActa) throws Exception {
        List<DetalleServiciosUnificado> listaCubicacionDetalleUnificado = new ArrayList<DetalleServiciosUnificado>();
        DetalleServiciosUnificado detalleServiciosUnificado = new DetalleServiciosUnificado();
        CubicadorDetalleUnificadoActas cubicadorDetalleUnificadoActas = null;
        Gateway gate = new Gateway();
        Cubicador cubicador = new Cubicador();
        List<CubicadorDetalle> listaMaterialesCubicacionordinaria = new ArrayList<CubicadorDetalle>();
        List<Actas> actasOld = new ArrayList<Actas>();
        boolean existe = false;
        try {
            if(ultimaActa.getId() > 0){
                existe = true;
            } else {
                existe = false;
            }

        } catch (Exception e){
            e.printStackTrace();
            existe = false;
        }

        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();
            cubicador = gate.obtenerCubicadorPorOtId(otId);
            listaMaterialesCubicacionordinaria = gate.obtenerCubicadorDetallePorIdCubicador(cubicador.getId());

            for(CubicadorDetalle listaCubUni : listaMaterialesCubicacionordinaria){
                detalleServiciosUnificado = new DetalleServiciosUnificado();
                cubicadorDetalleUnificadoActas = new CubicadorDetalleUnificadoActas();

                if(existe){
                    cubicadorDetalleUnificadoActas = gate.obtieneCubicadorDetalleUnificadoActa(listaCubUni.getId(), ultimaActa.getId());
                } else {
                    cubicadorDetalleUnificadoActas.setTotalMontoCubicado(0.0);
                    cubicadorDetalleUnificadoActas.setTotalMontoEjecucion(0.0);
                    cubicadorDetalleUnificadoActas.setTotalUnidadesCubicadas(0.0);
                    cubicadorDetalleUnificadoActas.setTotalUnidadesEjecucionActa(0.0);
                    cubicadorDetalleUnificadoActas.setTotalUnidadesEjecucionOt(0.0);
                }
                detalleServiciosUnificado.setCubicadorDetalle(listaCubUni);
                detalleServiciosUnificado.setCubicadorDetalleUnificadoActas(cubicadorDetalleUnificadoActas);
                listaCubicacionDetalleUnificado.add(detalleServiciosUnificado);
            }
        } finally {
            gate.cerrarSesion();
        }
        return listaCubicacionDetalleUnificado;
    }
    public List<DetalleItemsOrdinario> obtenerCubicacionOrdinarioPorIdOtValidar(int otId, Actas acta) throws Exception {
        List<DetalleItemsOrdinario> listaDetalleItemsOrdinario = new ArrayList<DetalleItemsOrdinario>();
        DetalleItemsOrdinario detalleItemsOrdinario = new DetalleItemsOrdinario();
        DetalleCubicadorOrdinarioActas detalleCubicadorOrdinarioActas = null;
        Cubicador cubicador = new Cubicador();
        List<CubicadorOrdinario> listaMaterialesCubicacionordinaria = new ArrayList<CubicadorOrdinario>();
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();
            cubicador = gate.obtenerCubicadorPorOtId(otId);
            listaMaterialesCubicacionordinaria = gate.obtenerListaCubicacionOrdinariaPorIdOt(cubicador.getId());

            for(CubicadorOrdinario listaCubOrd : listaMaterialesCubicacionordinaria){
                detalleItemsOrdinario = new DetalleItemsOrdinario();
                detalleCubicadorOrdinarioActas = new DetalleCubicadorOrdinarioActas();
                detalleItemsOrdinario.setCubicadorOrdinario(listaCubOrd);
                detalleCubicadorOrdinarioActas = gate.obtenerListadetalleItemsOrdinarioActa(listaCubOrd.getId(), acta.getId());
                detalleItemsOrdinario.setDetalleCubicadorOrdinarioActas(detalleCubicadorOrdinarioActas);
                listaDetalleItemsOrdinario.add(detalleItemsOrdinario);
            }
        } finally {
            gate.cerrarSesion();
        }
        return listaDetalleItemsOrdinario;
    }
    public List<DetalleServiciosUnificado> obtenerCubicacionUnificadaPorIdOtValidar(int otId, Actas acta) throws Exception {
        List<DetalleServiciosUnificado> listaDetalleServiciosUnificado = new ArrayList<DetalleServiciosUnificado>();
        DetalleServiciosUnificado detalleServiciosUnificado = new DetalleServiciosUnificado();
        CubicadorDetalleUnificadoActas cubicadorDetalleUnificadoActas = null;
        Cubicador cubicador = new Cubicador();
        List<CubicadorDetalle> listaMaterialesCubicacionUnificada = new ArrayList<CubicadorDetalle>();
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();
            cubicador = gate.obtenerCubicadorPorOtId(otId);
            listaMaterialesCubicacionUnificada = gate.obtenerListaCubicacionUnificadaPorIdOt(cubicador.getId());
            CubicadorDetalle cubicadorDetalle = null;
            for(CubicadorDetalle listaCubdetalle : listaMaterialesCubicacionUnificada){
                detalleServiciosUnificado = new DetalleServiciosUnificado();
                cubicadorDetalleUnificadoActas = new CubicadorDetalleUnificadoActas();
                cubicadorDetalle = new CubicadorDetalle();
                cubicadorDetalle = gate.obtenerUnicoCubicadorDetallePorIdCubicador(listaCubdetalle.getId());
                detalleServiciosUnificado.setCubicadorDetalle(cubicadorDetalle);
                cubicadorDetalleUnificadoActas = gate.obtenerListaCubicacionDetalleActa(listaCubdetalle.getId(), acta.getId());
                detalleServiciosUnificado.setCubicadorDetalleUnificadoActas(cubicadorDetalleUnificadoActas);
                listaDetalleServiciosUnificado.add(detalleServiciosUnificado);
            }
        } finally {
            gate.cerrarSesion();
        }
        return listaDetalleServiciosUnificado;
    }
    public List<DetalleServiciosUnificado> obtenerCubicacionUnificadaAdicionalPorIdOtValidar(int otId, Actas acta) throws Exception {
        List<DetalleServiciosUnificado> listaDetalleServiciosUnificado = new ArrayList<DetalleServiciosUnificado>();
        DetalleServiciosUnificado detalleServiciosUnificado = new DetalleServiciosUnificado();
        Cubicador cubicador = new Cubicador();
        List<CubicadorDetalleAdicionales> listaMaterialesCubicacionUnificada = new ArrayList<CubicadorDetalleAdicionales>();
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();
            cubicador = gate.obtenerCubicadorPorOtId(otId);
            listaMaterialesCubicacionUnificada = gate.obtenerListaCubicacionAdicionalUnificadaPorIdOt(cubicador.getId());
            CubicadorDetalleAdicionales cubicadorDetalle = null;
            for(CubicadorDetalleAdicionales listaCubdetalle : listaMaterialesCubicacionUnificada){
                detalleServiciosUnificado = new DetalleServiciosUnificado();
                cubicadorDetalle = new CubicadorDetalleAdicionales();
                cubicadorDetalle = gate.obtenerUnicoCubicadorDetalleAdicionalesPorIdCubicador(listaCubdetalle.getId());
                detalleServiciosUnificado.setCubicadorDetalleAdicionales(cubicadorDetalle);
                listaDetalleServiciosUnificado.add(detalleServiciosUnificado);
            }
        } finally {
            gate.cerrarSesion();
        }
        return listaDetalleServiciosUnificado;
    }

    public Actas obtenerActaParaValidar(int otId) throws Exception {
        Actas actaFinal = new Actas();
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();

            actaFinal = gate.obtenerActaParaValidar(otId);

        } finally {
            gate.cerrarSesion();
        }
        return actaFinal;
    }

    public List<Actas> obtenerActasPorOtValidadas(int otId) throws Exception {
        List<Actas> actasValidadas = new ArrayList<Actas>();
        Gateway gate = new Gateway();
        try {

            gate.abrirSesion();

            actasValidadas = gate.obtenerActasPorOtValidadas(otId);

        } finally {
            gate.cerrarSesion();
        }
        return actasValidadas;
    }

    public Actas obtenerUltimaActaPorOt(int otId) throws Exception {
        Actas actas = new Actas();
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();

            actas = gate.obtenerUltimaActaPorOt(otId);

        } finally {
            gate.cerrarSesion();
        }
        return actas;
    }

    public List<Cubicador> listarCubicacionesSinOtPorUsuarioId(int usuarioId, int contratoId) throws Exception {
        List<Cubicador> listadoOts = null;
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();

            listadoOts = gate.listarCubicacionesSinOtPorUsuarioId(usuarioId, contratoId);
            for (Cubicador cub : listadoOts) {
                Ot ot = new Ot();
                cub.setOt(ot);
                if(contratoId != 9) {
                    Proveedores proveedores = new Proveedores();
                    proveedores.setId(cub.getProveedor().getId());
                    proveedores.setNombre(cub.getProveedor().getNombre());
                    cub.setProveedor(proveedores);
                }
                join.add("cubicadorServicios");
                join.add("cubicadorMateriales");
                join.add("cubicadorOrdinario");
                gate.validarYSetearObjetoARetornar(cub, join);
                //gate.validarYSetearObjetoARetornar(cub, null);
            }
        } finally {
            gate.cerrarSesion();
        }
        return listadoOts;
    }

    public TipoMoneda obtieneTipoMonedaPorOt(int otId) throws Exception {
        double montoTotal = 0;
        TipoMoneda tipoMoneda = new TipoMoneda();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            Cubicador cubicador = new Cubicador();
            Ot ot = new Ot();
            ot = gate.obtenerDetalleBasicoOt(otId);
            cubicador = gate.obtenerCubicadorPorOtId(ot.getId());
            int servicioId = 0;
            int proveedorId = 0;
            int contratoId = 0;
            int regionId = 0;
            switch (ot.getContrato().getId()){
                case 1:
                    List<CubicadorServicios> lista = gate.obtenerCubicacionServiciosPorIdCubicador(cubicador.getId());
                     servicioId = lista.get(0).getServicio().getId();
                     proveedorId = ot.getProveedor().getId();
                     contratoId = ot.getContrato().getId();
                     regionId = cubicador.getRegion().getId();
                    List<ProveedoresServicios> proveedoresServicios = new ArrayList<ProveedoresServicios>();
                    proveedoresServicios = gate.obtieneProveedorServicios(contratoId, regionId, proveedorId, servicioId);
                    tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();
                    break;
                case 2:
                    CubicadorOrdinario cubicadorOrdinario = new CubicadorOrdinario();
                    cubicadorOrdinario = gate.obtenCubicadorOrdinario(cubicador.getId());
                    tipoMoneda = cubicadorOrdinario.getTipoMoneda();
                    break;
                case 3:
                    List<CubicadorServicios> listaRan = gate.obtenerCubicacionServiciosPorIdCubicador(cubicador.getId());
                     servicioId = listaRan.get(0).getServicio().getId();
                     proveedorId = ot.getProveedor().getId();
                     contratoId = ot.getContrato().getId();
                     regionId = cubicador.getRegion().getId();
                    List<ProveedoresServicios> proveedoresServiciosRan = new ArrayList<ProveedoresServicios>();
                    proveedoresServiciosRan = gate.obtieneProveedorServicios(contratoId, regionId, proveedorId, servicioId);
                    tipoMoneda = proveedoresServiciosRan.get(0).getTipoMoneda();
                    break;
                case 4:
                    CubicadorDetalle cubicadorDetalle = new CubicadorDetalle();
                    cubicadorDetalle = gate.obtenerUnicoCubicadorDetallePorIdCubicador(cubicador.getId());
                    tipoMoneda = cubicadorDetalle.getTipoMoneda();
                    break;
                case 5:
                    List<CubicadorServicios> listaLLave = gate.obtenerCubicacionServiciosPorIdCubicador(cubicador.getId());
                    servicioId = listaLLave.get(0).getServicio().getId();
                    proveedorId = ot.getProveedor().getId();
                    contratoId = ot.getContrato().getId();
                    regionId = cubicador.getRegion().getId();
                    List<ProveedoresServicios> proveedoresServiciosLlave = new ArrayList<ProveedoresServicios>();
                    proveedoresServiciosLlave = gate.obtieneProveedorServicios(contratoId, regionId, proveedorId, servicioId);
                    tipoMoneda = proveedoresServiciosLlave.get(0).getTipoMoneda();
                    montoTotal = obtieneMontoServicios(cubicador.getId());
                case 6:
                    List<CubicadorServicios> listaSalas = gate.obtenerCubicacionServiciosPorIdCubicador(cubicador.getId());
                    servicioId = listaSalas.get(0).getServicio().getId();
                    proveedorId = ot.getProveedor().getId();
                    contratoId = ot.getContrato().getId();
                    regionId = cubicador.getRegion().getId();
                    List<ProveedoresServicios> proveedoresServiciosSalas = new ArrayList<ProveedoresServicios>();
                    proveedoresServiciosSalas = gate.obtieneProveedorServicios(contratoId, regionId, proveedorId, servicioId);
                    tipoMoneda = proveedoresServiciosSalas.get(0).getTipoMoneda();
                    montoTotal = obtieneMontoServicios(cubicador.getId());
                case 8:
                    List<CubicadorServicios> listaSat = gate.obtenerCubicacionServiciosPorIdCubicador(cubicador.getId());
                    servicioId = listaSat.get(0).getServicio().getId();
                    proveedorId = ot.getProveedor().getId();
                    contratoId = ot.getContrato().getId();
                    regionId = cubicador.getRegion().getId();
                    List<ProveedoresServicios> proveedoresServiciosSat = new ArrayList<ProveedoresServicios>();
                    proveedoresServiciosSat = gate.obtieneProveedorServicios(contratoId, regionId, proveedorId, servicioId);
                    tipoMoneda = proveedoresServiciosSat.get(0).getTipoMoneda();
                    montoTotal = obtieneMontoServicios(cubicador.getId());
                    break;
                default:
                    break;
            }

        } catch (Exception e) {
            throw e;
        }finally {
            gate.cerrarSesion();
        }
        return tipoMoneda;
    }

    public String obtenerMontoCubicadorPorIdOt(int otId) throws Exception {
        double montoTotal = 0;
        String montoTotalParciado = "";
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            Cubicador cubicador = new Cubicador();
            Ot ot = new Ot();
            ot = gate.obtenerDetalleBasicoOt(otId);
            cubicador = gate.obtenerCubicadorPorOtId(ot.getId());
            switch (ot.getContrato().getId()){
                case 1:
                    montoTotal = obtieneMontoServicios(cubicador.getId());
                    break;
                case 2:
                    montoTotal = obtieneMontoCubicadorOrdinario(cubicador.getId());
                    break;
                case 3:
                    montoTotal = obtieneMontoServicios(cubicador.getId());
                    break;
                case 4:
                    montoTotal = obtieneMontoCubicadorDetalle(cubicador.getId());
                    break;
                case 5:
                    montoTotal = obtieneMontoServicios(cubicador.getId());
                    break;
                case 6:
                    montoTotal = obtieneMontoServicios(cubicador.getId());
                    break;
                case 8:
                    montoTotal = obtieneMontoServicios(cubicador.getId());
                    break;
                default:
                    break;
            }
            montoTotalParciado = parseaMonto(montoTotal);

        } catch (Exception e) {
            throw e;
        }finally {
            gate.cerrarSesion();
        }
        return montoTotalParciado;
    }
    private double obtieneMontoServicios(int cubicadorId) throws Exception {
        List<CubicadorServicios> cubicadorServicios = new ArrayList<CubicadorServicios>();
        Gateway gate = new Gateway();
        double montoTotal = 0;
        try {
            gate.abrirSesion();
            cubicadorServicios = gate.obtenerCubicacionServiciosPorIdCubicador(cubicadorId);
            for(CubicadorServicios cubicadorServiciosAux : cubicadorServicios){
                montoTotal =montoTotal + cubicadorServiciosAux.getTotal();
            }

        } finally {
            gate.cerrarSesion();
        }
        return montoTotal;
    }
    private double obtieneMontoCubicadorDetalle(int cubicadorId) throws Exception {
        List<CubicadorDetalle> cubicadorDetalles = new ArrayList<CubicadorDetalle>();
        Gateway gate = new Gateway();
        double montoTotal = 0;
        try {
            gate.abrirSesion();
            cubicadorDetalles = gate.obtenerCubicacionDetallePorIdCubicador(cubicadorId);
            for(CubicadorDetalle cubicadorDetalleAux : cubicadorDetalles){
                montoTotal = montoTotal + cubicadorDetalleAux.getTotal();
            }

        } finally {
            gate.cerrarSesion();
        }
        return montoTotal;
    }
    public Map obtieneDetalleMaterialesServicios(int otId, Login login) throws Exception {
        Map mapaformActa = new HashMap();
        Map serviciosMaterialesMap = new HashMap();
        List listaMaterialesServicios = new ArrayList();
        Gateway gate = new Gateway();

        try {
            gate.abrirSesion();
            List listaDetalleMateriales = new ArrayList();
            List<Cubicador> cubicaciones = obtenerCubicacionPorIdOt(otId);
            Cubicador cubicador = cubicaciones.get(0);
            Map material = new HashMap();
            int contMaterialesValidaOrVal= 0;
            int contServUnificado = 0;
            int countServiciosGenerales = 0;
            int countMateriales = 0;
            int contratoId = 0;
            int actaId = 0;


                // obtener acta a validar
                Actas acta = obtenerActaParaValidar(otId);
                List observaciones = new ArrayList();
                observaciones = OtDAO.getINSTANCE().obtieneObservacionesGerencia(login, otId);
                Date fechaCreacion = null;
                Date fechaTerminoEstimada = null;
                boolean validacionSistema = false;
                double montoTotalActa = 0.0;
                double montoTotalCubicado = 0.0;
                if (acta != null) {
                    fechaCreacion = acta.getOt().getFechaCreacion();
                    fechaTerminoEstimada = acta.getOt().getFechaTerminoEstimada();
                    validacionSistema = acta.getValidacionSistema();
                }
                actaId = acta.getId();
                mapaformActa.put("idActa", acta.getId());
                mapaformActa.put("contratoId", acta.getOt().getContrato().getId());
                mapaformActa.put("observaciones", observaciones);
                mapaformActa.put("validacionSistema", validacionSistema);
                mapaformActa.put("fechaCreacion", fechaCreacion);
                mapaformActa.put("fechaTerminoEstimada", fechaTerminoEstimada);
                List<DetalleItemsOrdinario> detalleItemsOrdinario = new ArrayList<DetalleItemsOrdinario>();
                Set<DetalleMaterialesActas> listadoDetalleMateriales = acta.getDetalleMaterialesActas();
                Set<DetalleMaterialesAdicionalesActas> listadoDetalleMaterialesAdicionales = acta.getDetalleMaterialesAdicionalesActas();
                Set<DetalleServiciosActas> listadoDetalleServicios = acta.getDetalleServiciosActas();
                Set<DetalleServiciosAdicionalesActas> listadoDetalleServiciosAdicionales = acta.getDetalleServiciosAdicionalesActas();
                List<CubicadorDetalle> cubicadorDetallesList = new ArrayList<CubicadorDetalle>();
                List<ProveedoresServicios> proveedoresServicios = new ArrayList<ProveedoresServicios>();
                Materiales materiales = new Materiales();
                contratoId = acta.getOt().getContrato().getId();

            Servicios servicios = new Servicios();
                switch (contratoId) {
                    case 1:
                        listaMaterialesServicios = new ArrayList();
                        List<DetalleServiciosActas> detalleServiciosActas = new ArrayList<DetalleServiciosActas>();
                        List<DetalleServiciosAdicionalesActas> detalleServiciosAdicionalesActas = new ArrayList<DetalleServiciosAdicionalesActas>();
                        detalleServiciosActas = gate.obtieneDetalleCosteoPorContratoYtipoDetalle(otId, contratoId, "materiales");

                            if (listadoDetalleMateriales.size() > 0) {
                                for (DetalleMaterialesActas detalleMaterial : listadoDetalleMateriales) {
                                    materiales = new Materiales();
                                    materiales = gate.obtieneMaterial(detalleMaterial.getId().getMaterialId());
                                    serviciosMaterialesMap = new HashMap();
                                    serviciosMaterialesMap.put("id", detalleMaterial.getId().getMaterialId());
                                    serviciosMaterialesMap.put("nombre", detalleMaterial.getMateriales().getNombre());
                                    serviciosMaterialesMap.put("descripcion", detalleMaterial.getMateriales().getDescripcion());
                                    serviciosMaterialesMap.put("cantidadCubicada", detalleMaterial.getTotalUnidadesCubicadas());
                                    serviciosMaterialesMap.put("precio", materiales.getPrecio());
                                    serviciosMaterialesMap.put("tipoMonedaId", materiales.getTipoMoneda().getId());
                                    serviciosMaterialesMap.put("montoTotalCubicado", 0);
                                    serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleMaterial.getTotalUnidadesEjecucionOt());
                                    serviciosMaterialesMap.put("validar", false);
                                    serviciosMaterialesMap.put("cantidadActualInformada", detalleMaterial.getTotalUnidadesEjecucionActa());
                                    serviciosMaterialesMap.put("montoTotalMaterial", 0);
                                    montoTotalActa = montoTotalActa + detalleMaterial.getTotalMontoEjecucionActa();
                                    montoTotalCubicado = montoTotalCubicado + (detalleMaterial.getTotalUnidadesCubicadas() * materiales.getPrecio());
                                    listaMaterialesServicios.add(serviciosMaterialesMap);
                                }
                            }
                            mapaformActa.put("detalleMaterialesActa", listaMaterialesServicios);
                            listaMaterialesServicios = new ArrayList();
                            if (listadoDetalleMaterialesAdicionales.size() > 0) {
                                for (DetalleMaterialesAdicionalesActas detalleMaterialAdicional : listadoDetalleMaterialesAdicionales) {
                                    serviciosMaterialesMap = new HashMap();
                                    materiales = new Materiales();
                                    materiales = gate.obtieneMaterial(detalleMaterialAdicional.getId().getMaterialId());
                                    serviciosMaterialesMap.put("id", detalleMaterialAdicional.getId().getMaterialId());
                                    serviciosMaterialesMap.put("nombre", detalleMaterialAdicional.getMateriales().getNombre());
                                    serviciosMaterialesMap.put("descripcion", detalleMaterialAdicional.getMateriales().getDescripcion());
                                    serviciosMaterialesMap.put("montoTotalCubicado", 0);
                                    serviciosMaterialesMap.put("cantidadCubicada", 0);
                                    serviciosMaterialesMap.put("precio", materiales.getPrecio());
                                    serviciosMaterialesMap.put("tipoMonedaId", materiales.getTipoMoneda().getId());
                                    serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleMaterialAdicional.getTotalUnidadesEjecucionOt());
                                    serviciosMaterialesMap.put("validar", false);
                                    serviciosMaterialesMap.put("cantidadActualInformada", detalleMaterialAdicional.getTotalUnidadesEjecucionActa());
                                    serviciosMaterialesMap.put("montoTotalMaterialAdicional", 0);
                                    montoTotalActa = montoTotalActa + detalleMaterialAdicional.getTotalMontoEjecucionActa();
                                    listaMaterialesServicios.add(serviciosMaterialesMap);
                                }
                            }
                            mapaformActa.put("detalleMaterialesAdicionalesActa", listaMaterialesServicios);
                            listaMaterialesServicios = new ArrayList();
                            if (listadoDetalleServicios.size() > 0) {
                                for (DetalleServiciosActas detalleServicios : listadoDetalleServicios) {
                                    proveedoresServicios = new ArrayList<ProveedoresServicios>();
                                    proveedoresServicios = gate.obtieneProveedorServicios(cubicador.getContrato().getId(), cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  detalleServicios.getId().getServicioId());
                                    serviciosMaterialesMap = new HashMap();
                                    serviciosMaterialesMap.put("id", detalleServicios.getId().getServicioId());
                                    serviciosMaterialesMap.put("nombre", detalleServicios.getServicio().getNombre());
                                    serviciosMaterialesMap.put("descripcion", detalleServicios.getServicio().getDescripcion());
                                    serviciosMaterialesMap.put("cantidadCubicada", detalleServicios.getTotalUnidadesCubicadas());
                                    serviciosMaterialesMap.put("precio", detalleServicios.getServicio().getPrecio());
                                    serviciosMaterialesMap.put("tipoMonedaId", proveedoresServicios.get(0).getTipoMoneda().getId());
                                    serviciosMaterialesMap.put("montoTotalCubicado", detalleServicios.getTotalMontoCubicado());
                                    serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleServicios.getTotalUnidadesEjecucionOt());
                                    serviciosMaterialesMap.put("validar", false);
                                    serviciosMaterialesMap.put("cantidadActualInformada", detalleServicios.getTotalUnidadesEjecucionActa());
                                    serviciosMaterialesMap.put("montoTotalServicio", detalleServicios.getTotalMontoEjecucionActa());
                                    montoTotalActa = montoTotalActa + detalleServicios.getTotalMontoEjecucionActa();
                                    montoTotalCubicado = montoTotalCubicado + (detalleServicios.getTotalUnidadesEjecucionActa() * detalleServicios.getServicio().getPrecio());
                                    listaMaterialesServicios.add(serviciosMaterialesMap);
                                }
                            }
                            mapaformActa.put("detalleServiciosActa", listaMaterialesServicios);
                            listaMaterialesServicios = new ArrayList();
                            if (listadoDetalleServiciosAdicionales.size() > 0) {
                                for (DetalleServiciosAdicionalesActas detalleServiciosAdicionales : listadoDetalleServiciosAdicionales) {
                                    proveedoresServicios = new ArrayList<ProveedoresServicios>();
                                    proveedoresServicios = gate.obtieneProveedorServicios(cubicador.getContrato().getId(), cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  detalleServiciosAdicionales.getId().getServicioId());
                                    serviciosMaterialesMap = new HashMap();
                                    serviciosMaterialesMap.put("id", detalleServiciosAdicionales.getId().getServicioId());
                                    serviciosMaterialesMap.put("nombre", detalleServiciosAdicionales.getServicio().getNombre());
                                    serviciosMaterialesMap.put("descripcion", detalleServiciosAdicionales.getServicio().getDescripcion());
                                    serviciosMaterialesMap.put("cantidadCubicada", detalleServiciosAdicionales.getServicio().getCantidadDefault());
                                    serviciosMaterialesMap.put("precio", detalleServiciosAdicionales.getServicio().getPrecio());
                                    serviciosMaterialesMap.put("tipoMonedaId", proveedoresServicios.get(0).getTipoMoneda().getId());
                                    serviciosMaterialesMap.put("montoTotalCubicado", detalleServiciosAdicionales.getTotalMontoCubicado());
                                    serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleServiciosAdicionales.getTotalUnidadesEjecucionOt());
                                    serviciosMaterialesMap.put("validar", false);
                                    serviciosMaterialesMap.put("cantidadActualInformada", detalleServiciosAdicionales.getTotalUnidadesEjecucionActa());
                                    serviciosMaterialesMap.put("montoTotalServiciosAdicionales", detalleServiciosAdicionales.getTotalMontoEjecucionActa());
                                    montoTotalActa = montoTotalActa + detalleServiciosAdicionales.getTotalMontoEjecucionActa();
                                    listaMaterialesServicios.add(serviciosMaterialesMap);
                                }
                            }
                            mapaformActa.put("detalleServiciosAdicionalesActa", listaMaterialesServicios);
                        break;
                    case 11:
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleMateriales.size() > 0) {
                            for (DetalleMaterialesActas detalleMaterial : listadoDetalleMateriales) {
                                materiales = new Materiales();
                                materiales = gate.obtieneMaterial(detalleMaterial.getId().getMaterialId());
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleMaterial.getId().getMaterialId());
                                serviciosMaterialesMap.put("nombre", detalleMaterial.getMateriales().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleMaterial.getMateriales().getDescripcion());
                                serviciosMaterialesMap.put("cantidadCubicada", detalleMaterial.getTotalUnidadesCubicadas());
                                serviciosMaterialesMap.put("precio", materiales.getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", materiales.getTipoMoneda().getId());
                                serviciosMaterialesMap.put("montoTotalCubicado", 0);
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleMaterial.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleMaterial.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalMaterial", 0);
                                montoTotalActa = montoTotalActa + detalleMaterial.getTotalMontoEjecucionActa();
                                montoTotalCubicado = montoTotalCubicado + (detalleMaterial.getTotalUnidadesCubicadas() * materiales.getPrecio());
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleMaterialesActa", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleMaterialesAdicionales.size() > 0) {
                            for (DetalleMaterialesAdicionalesActas detalleMaterialAdicional : listadoDetalleMaterialesAdicionales) {
                                serviciosMaterialesMap = new HashMap();
                                materiales = new Materiales();
                                materiales = gate.obtieneMaterial(detalleMaterialAdicional.getId().getMaterialId());
                                serviciosMaterialesMap.put("id", detalleMaterialAdicional.getId().getMaterialId());
                                serviciosMaterialesMap.put("nombre", detalleMaterialAdicional.getMateriales().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleMaterialAdicional.getMateriales().getDescripcion());
                                serviciosMaterialesMap.put("montoTotalCubicado", 0);
                                serviciosMaterialesMap.put("cantidadCubicada", 0);
                                serviciosMaterialesMap.put("precio", materiales.getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", materiales.getTipoMoneda().getId());
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleMaterialAdicional.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleMaterialAdicional.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalMaterialAdicional", 0);
                                montoTotalActa = montoTotalActa + detalleMaterialAdicional.getTotalMontoEjecucionActa();
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleMaterialesAdicionalesActa", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleServicios.size() > 0) {
                            for (DetalleServiciosActas detalleServicios : listadoDetalleServicios) {
                                proveedoresServicios = new ArrayList<ProveedoresServicios>();
                                proveedoresServicios = gate.obtieneProveedorServicios(cubicador.getContrato().getId(), cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  detalleServicios.getId().getServicioId());
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleServicios.getId().getServicioId());
                                serviciosMaterialesMap.put("nombre", detalleServicios.getServicio().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleServicios.getServicio().getDescripcion());
                                serviciosMaterialesMap.put("cantidadCubicada", detalleServicios.getTotalUnidadesCubicadas());
                                serviciosMaterialesMap.put("precio", detalleServicios.getServicio().getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", proveedoresServicios.get(0).getTipoMoneda().getId());
                                serviciosMaterialesMap.put("montoTotalCubicado", detalleServicios.getTotalMontoCubicado());
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleServicios.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleServicios.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalServicio", detalleServicios.getTotalMontoEjecucionActa());
                                montoTotalActa = montoTotalActa + detalleServicios.getTotalMontoEjecucionActa();
                                montoTotalCubicado = montoTotalCubicado + (detalleServicios.getTotalUnidadesEjecucionActa() * detalleServicios.getServicio().getPrecio());
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleServiciosActa", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleServiciosAdicionales.size() > 0) {
                            for (DetalleServiciosAdicionalesActas detalleServiciosAdicionales : listadoDetalleServiciosAdicionales) {
                                proveedoresServicios = new ArrayList<ProveedoresServicios>();
                                proveedoresServicios = gate.obtieneProveedorServicios(cubicador.getContrato().getId(), cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  detalleServiciosAdicionales.getId().getServicioId());
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleServiciosAdicionales.getId().getServicioId());
                                serviciosMaterialesMap.put("nombre", detalleServiciosAdicionales.getServicio().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleServiciosAdicionales.getServicio().getDescripcion());
                                serviciosMaterialesMap.put("cantidadCubicada", detalleServiciosAdicionales.getServicio().getCantidadDefault());
                                serviciosMaterialesMap.put("precio", detalleServiciosAdicionales.getServicio().getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", proveedoresServicios.get(0).getTipoMoneda().getId());
                                serviciosMaterialesMap.put("montoTotalCubicado", detalleServiciosAdicionales.getTotalMontoCubicado());
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleServiciosAdicionales.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleServiciosAdicionales.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalServiciosAdicionales", detalleServiciosAdicionales.getTotalMontoEjecucionActa());
                                montoTotalActa = montoTotalActa + detalleServiciosAdicionales.getTotalMontoEjecucionActa();
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleServiciosAdicionalesActa", listaMaterialesServicios);
                        break;
                    case 10:
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleMateriales.size() > 0) {
                            for (DetalleMaterialesActas detalleMaterial : listadoDetalleMateriales) {
                                materiales = new Materiales();
                                materiales = gate.obtieneMaterial(detalleMaterial.getId().getMaterialId());
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleMaterial.getId().getMaterialId());
                                serviciosMaterialesMap.put("nombre", detalleMaterial.getMateriales().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleMaterial.getMateriales().getDescripcion());
                                serviciosMaterialesMap.put("cantidadCubicada", detalleMaterial.getTotalUnidadesCubicadas());
                                serviciosMaterialesMap.put("precio", materiales.getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", materiales.getTipoMoneda().getId());
                                serviciosMaterialesMap.put("montoTotalCubicado", 0);
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleMaterial.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleMaterial.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalMaterial", 0);
                                montoTotalActa = montoTotalActa + detalleMaterial.getTotalMontoEjecucionActa();
                                montoTotalCubicado = montoTotalCubicado + (detalleMaterial.getTotalUnidadesCubicadas() * materiales.getPrecio());
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleMaterialesActa", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleMaterialesAdicionales.size() > 0) {
                            for (DetalleMaterialesAdicionalesActas detalleMaterialAdicional : listadoDetalleMaterialesAdicionales) {
                                serviciosMaterialesMap = new HashMap();
                                materiales = new Materiales();
                                materiales = gate.obtieneMaterial(detalleMaterialAdicional.getId().getMaterialId());
                                serviciosMaterialesMap.put("id", detalleMaterialAdicional.getId().getMaterialId());
                                serviciosMaterialesMap.put("nombre", detalleMaterialAdicional.getMateriales().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleMaterialAdicional.getMateriales().getDescripcion());
                                serviciosMaterialesMap.put("montoTotalCubicado", 0);
                                serviciosMaterialesMap.put("cantidadCubicada", 0);
                                serviciosMaterialesMap.put("precio", materiales.getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", materiales.getTipoMoneda().getId());
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleMaterialAdicional.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleMaterialAdicional.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalMaterialAdicional", 0);
                                montoTotalActa = montoTotalActa + detalleMaterialAdicional.getTotalMontoEjecucionActa();
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleMaterialesAdicionalesActa", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleServicios.size() > 0) {
                            for (DetalleServiciosActas detalleServicios : listadoDetalleServicios) {
                                proveedoresServicios = new ArrayList<ProveedoresServicios>();
                                proveedoresServicios = gate.obtieneProveedorServicios(cubicador.getContrato().getId(), cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  detalleServicios.getId().getServicioId());
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleServicios.getId().getServicioId());
                                serviciosMaterialesMap.put("nombre", detalleServicios.getServicio().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleServicios.getServicio().getDescripcion());
                                serviciosMaterialesMap.put("cantidadCubicada", detalleServicios.getTotalUnidadesCubicadas());
                                serviciosMaterialesMap.put("precio", detalleServicios.getServicio().getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", proveedoresServicios.get(0).getTipoMoneda().getId());
                                serviciosMaterialesMap.put("montoTotalCubicado", detalleServicios.getTotalMontoCubicado());
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleServicios.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleServicios.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalServicio", detalleServicios.getTotalUnidadesCubicadas());
                                montoTotalActa = montoTotalActa + detalleServicios.getTotalMontoEjecucionActa();
                                montoTotalCubicado = montoTotalCubicado + (detalleServicios.getTotalUnidadesEjecucionActa() * detalleServicios.getServicio().getPrecio());
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleServiciosActa", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleServiciosAdicionales.size() > 0) {
                            for (DetalleServiciosAdicionalesActas detalleServiciosAdicionales : listadoDetalleServiciosAdicionales) {
                                proveedoresServicios = new ArrayList<ProveedoresServicios>();
                                proveedoresServicios = gate.obtieneProveedorServicios(cubicador.getContrato().getId(), cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  detalleServiciosAdicionales.getId().getServicioId());
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleServiciosAdicionales.getId().getServicioId());
                                serviciosMaterialesMap.put("nombre", detalleServiciosAdicionales.getServicio().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleServiciosAdicionales.getServicio().getDescripcion());
                                serviciosMaterialesMap.put("cantidadCubicada", detalleServiciosAdicionales.getServicio().getCantidadDefault());
                                serviciosMaterialesMap.put("precio", detalleServiciosAdicionales.getServicio().getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", proveedoresServicios.get(0).getTipoMoneda().getId());
                                serviciosMaterialesMap.put("montoTotalCubicado", detalleServiciosAdicionales.getTotalMontoCubicado());
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleServiciosAdicionales.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleServiciosAdicionales.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalServiciosAdicionales", detalleServiciosAdicionales.getTotalMontoEjecucionActa());
                                montoTotalActa = montoTotalActa + detalleServiciosAdicionales.getTotalMontoEjecucionActa();
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleServiciosAdicionalesActa", listaMaterialesServicios);
                        break;
                    case 2:
                        detalleItemsOrdinario = CubicadorDAO.getINSTANCE().obtenerCubicacionOrdinarioPorIdOtValidar(otId, acta);
                        contMaterialesValidaOrVal = detalleItemsOrdinario.size();
                        if (contMaterialesValidaOrVal > 0) {
                            //
                            for (DetalleItemsOrdinario itemsord : detalleItemsOrdinario) {
                                material = new HashMap();
                                material.put("id", itemsord.getCubicadorOrdinario().getId());
                                material.put("nombre", itemsord.getCubicadorOrdinario().getItem());
                                material.put("descripcion", itemsord.getCubicadorOrdinario().getItem());
                                material.put("cantidadCubicada", itemsord.getCubicadorOrdinario().getCantidad());
                                material.put("tipoMonedaId", itemsord.getCubicadorOrdinario().getTipoMoneda().getId());
                                material.put("precio", itemsord.getCubicadorOrdinario().getPrecio());
                                material.put("montoTotalCubicado", itemsord.getCubicadorOrdinario().getTotal());
                                material.put("cantidadAcumuladaInformada", (itemsord.getDetalleCubicadorOrdinarioActas().getTotalUnidadesEjecucionOt() - itemsord.getDetalleCubicadorOrdinarioActas().getTotalUnidadesEjecucionActa()));
                                material.put("cantidadActualInformada", itemsord.getDetalleCubicadorOrdinarioActas().getTotalUnidadesEjecucionActa());
                                material.put("montoTotalItems", (itemsord.getDetalleCubicadorOrdinarioActas().getTotalUnidadesEjecucionActa() * itemsord.getCubicadorOrdinario().getPrecio()));

                                listaDetalleMateriales.add(material);
                                montoTotalActa = montoTotalActa + (itemsord.getDetalleCubicadorOrdinarioActas().getTotalUnidadesEjecucionActa() * itemsord.getCubicadorOrdinario().getPrecio());
                                montoTotalCubicado = montoTotalCubicado + (itemsord.getCubicadorOrdinario().getCantidad() * itemsord.getCubicadorOrdinario().getPrecio());
                            }
                            mapaformActa.put("detalleMaterialesActa", listaDetalleMateriales);
                        }
                        break;
                    case 3:
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleMateriales.size() > 0) {
                            for (DetalleMaterialesActas detalleMaterial : listadoDetalleMateriales) {
                                materiales = new Materiales();
                                materiales = gate.obtieneMaterial(detalleMaterial.getId().getMaterialId());
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleMaterial.getId().getMaterialId());
                                serviciosMaterialesMap.put("nombre", detalleMaterial.getMateriales().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleMaterial.getMateriales().getDescripcion());
                                serviciosMaterialesMap.put("cantidadCubicada", detalleMaterial.getTotalUnidadesCubicadas());
                                serviciosMaterialesMap.put("precio", materiales.getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", materiales.getTipoMoneda().getId());
                                serviciosMaterialesMap.put("montoTotalCubicado", 0);
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleMaterial.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleMaterial.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalMaterial", 0);
                                montoTotalActa = montoTotalActa + detalleMaterial.getTotalMontoEjecucionActa();
                                montoTotalCubicado = montoTotalCubicado + (detalleMaterial.getTotalUnidadesCubicadas() * materiales.getPrecio());
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleMaterialesActa", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleMaterialesAdicionales.size() > 0) {
                            for (DetalleMaterialesAdicionalesActas detalleMaterialAdicional : listadoDetalleMaterialesAdicionales) {
                                serviciosMaterialesMap = new HashMap();
                                materiales = new Materiales();
                                materiales = gate.obtieneMaterial(detalleMaterialAdicional.getId().getMaterialId());
                                serviciosMaterialesMap.put("id", detalleMaterialAdicional.getId().getMaterialId());
                                serviciosMaterialesMap.put("nombre", detalleMaterialAdicional.getMateriales().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleMaterialAdicional.getMateriales().getDescripcion());
                                serviciosMaterialesMap.put("montoTotalCubicado", 0);
                                serviciosMaterialesMap.put("cantidadCubicada", 0);
                                serviciosMaterialesMap.put("precio", materiales.getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", materiales.getTipoMoneda().getId());
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleMaterialAdicional.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleMaterialAdicional.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalMaterialAdicional", 0);
                                montoTotalActa = montoTotalActa + detalleMaterialAdicional.getTotalMontoEjecucionActa();
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleMaterialesAdicionalesActa", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleServicios.size() > 0) {
                            for (DetalleServiciosActas detalleServicios : listadoDetalleServicios) {
                                proveedoresServicios = new ArrayList<ProveedoresServicios>();
                                proveedoresServicios = gate.obtieneProveedorServicios(cubicador.getContrato().getId(), cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  detalleServicios.getId().getServicioId());
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleServicios.getId().getServicioId());
                                serviciosMaterialesMap.put("nombre", detalleServicios.getServicio().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleServicios.getServicio().getDescripcion());
                                serviciosMaterialesMap.put("cantidadCubicada", detalleServicios.getTotalUnidadesCubicadas());
                                serviciosMaterialesMap.put("precio", detalleServicios.getServicio().getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", proveedoresServicios.get(0).getTipoMoneda().getId());
                                serviciosMaterialesMap.put("montoTotalCubicado", detalleServicios.getTotalMontoCubicado());
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleServicios.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleServicios.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalServicio", detalleServicios.getTotalMontoEjecucionActa());
                                montoTotalActa = montoTotalActa + detalleServicios.getTotalMontoEjecucionActa();
                                montoTotalCubicado = montoTotalCubicado + (detalleServicios.getTotalUnidadesCubicadas() * detalleServicios.getServicio().getPrecio());
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleServiciosActa", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleServiciosAdicionales.size() > 0) {
                            for (DetalleServiciosAdicionalesActas detalleServiciosAdicionales : listadoDetalleServiciosAdicionales) {
                                proveedoresServicios = new ArrayList<ProveedoresServicios>();
                                proveedoresServicios = gate.obtieneProveedorServicios(cubicador.getContrato().getId(), cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  detalleServiciosAdicionales.getId().getServicioId());
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleServiciosAdicionales.getId().getServicioId());
                                serviciosMaterialesMap.put("nombre", detalleServiciosAdicionales.getServicio().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleServiciosAdicionales.getServicio().getDescripcion());
                                serviciosMaterialesMap.put("cantidadCubicada", detalleServiciosAdicionales.getServicio().getCantidadDefault());
                                serviciosMaterialesMap.put("precio", detalleServiciosAdicionales.getServicio().getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", proveedoresServicios.get(0).getTipoMoneda().getId());
                                serviciosMaterialesMap.put("montoTotalCubicado", detalleServiciosAdicionales.getTotalMontoCubicado());
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleServiciosAdicionales.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleServiciosAdicionales.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalServiciosAdicionales", detalleServiciosAdicionales.getTotalMontoEjecucionActa());
                                montoTotalActa = montoTotalActa + detalleServiciosAdicionales.getTotalMontoEjecucionActa();
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleServiciosAdicionalesActa", listaMaterialesServicios);
                        break;
                    case 4:
                        List<DetalleServiciosUnificado> detalleServiciosUnificados = new ArrayList<DetalleServiciosUnificado>();
                        List<DetalleServiciosUnificado> detalleServiciosUnificadosAdicionales = new ArrayList<DetalleServiciosUnificado>();
                        detalleServiciosUnificados = CubicadorDAO.getINSTANCE().obtenerCubicacionUnificadaPorIdOtValidar(otId, acta);
                        detalleServiciosUnificadosAdicionales = CubicadorDAO.getINSTANCE().obtenerCubicacionUnificadaAdicionalPorIdOtValidar(otId, acta);
                        double montoValidadoSistema = 0.0;
                        String nombreServicio = "";
                        String descripcionServicio = "";
                        listaMaterialesServicios = new ArrayList();
                        if (detalleServiciosUnificados.size() > 0) {
                            for (DetalleServiciosUnificado detalleServiciosUnificado : detalleServiciosUnificados) {
                                nombreServicio = detalleServiciosUnificado.getCubicadorDetalle().getServicios().getNombre().replace("\"", " pulg. ");
                                descripcionServicio = detalleServiciosUnificado.getCubicadorDetalle().getServicios().getDescripcion().replace("\"", " pulg. ");
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleServiciosUnificado.getCubicadorDetalle().getId());
                                serviciosMaterialesMap.put("nombre", nombreServicio);
                                serviciosMaterialesMap.put("descripcion", descripcionServicio);
                                serviciosMaterialesMap.put("cantidadCubicada", detalleServiciosUnificado.getCubicadorDetalleUnificadoActas().getTotalUnidadesCubicadas());
                                serviciosMaterialesMap.put("precio", detalleServiciosUnificado.getCubicadorDetalle().getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", detalleServiciosUnificado.getCubicadorDetalle().getTipoMonedaId());
                                serviciosMaterialesMap.put("montoTotalCubicado", detalleServiciosUnificado.getCubicadorDetalleUnificadoActas().getTotalMontoCubicado());
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleServiciosUnificado.getCubicadorDetalleUnificadoActas().getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalServicio", detalleServiciosUnificado.getCubicadorDetalleUnificadoActas().getTotalMontoEjecucion());
                                serviciosMaterialesMap.put("idRegion", detalleServiciosUnificado.getCubicadorDetalle().getRegionId());
                                serviciosMaterialesMap.put("nombreRegion", detalleServiciosUnificado.getCubicadorDetalle().getRegiones().getNombre());
                                serviciosMaterialesMap.put("idAgencia", detalleServiciosUnificado.getCubicadorDetalle().getAgenciaId());
                                serviciosMaterialesMap.put("nombreAgencia", detalleServiciosUnificado.getCubicadorDetalle().getAgencia().getNombre());
                                serviciosMaterialesMap.put("idCentral", detalleServiciosUnificado.getCubicadorDetalle().getCentralId());
                                serviciosMaterialesMap.put("nombreCentral", detalleServiciosUnificado.getCubicadorDetalle().getCentrales().getNombre());
                                montoTotalActa = montoTotalActa + detalleServiciosUnificado.getCubicadorDetalleUnificadoActas().getTotalMontoEjecucion();
                                montoTotalCubicado = montoTotalCubicado + (detalleServiciosUnificado.getCubicadorDetalle().getPrecio() * detalleServiciosUnificado.getCubicadorDetalleUnificadoActas().getTotalUnidadesCubicadas());
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleServiciosUnificado", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (detalleServiciosUnificadosAdicionales.size() > 0) {
                            for (DetalleServiciosUnificado detalleServiciosUnificadoAdicionales : detalleServiciosUnificadosAdicionales) {
                                nombreServicio = detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getServicios().getNombre().replace("\"", " pulg. ");
                                descripcionServicio = detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getServicios().getDescripcion().replace("\"", " pulg. ");
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getId());
                                serviciosMaterialesMap.put("nombre", nombreServicio);
                                serviciosMaterialesMap.put("descripcion", descripcionServicio);
                                serviciosMaterialesMap.put("precio", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getTipoMonedaId());
                                serviciosMaterialesMap.put("cantidad", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getCantidad());
                                serviciosMaterialesMap.put("montoTotalServicio", (detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getPrecio() * detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getCantidad()));
                                serviciosMaterialesMap.put("idRegion", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getRegionId());
                                serviciosMaterialesMap.put("nombreRegion", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getRegiones().getNombre());
                                serviciosMaterialesMap.put("idAgencia", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getAgenciaId());
                                serviciosMaterialesMap.put("nombreAgencia", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getAgencia().getNombre());
                                serviciosMaterialesMap.put("idCentral", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getCentralId());
                                serviciosMaterialesMap.put("nombreCentral", detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getCentrales().getNombre());
                                montoTotalActa = montoTotalActa + (detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getPrecio() * detalleServiciosUnificadoAdicionales.getCubicadorDetalleAdicionales().getCantidad());
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleServiciosAdicionalesUnificado", listaMaterialesServicios);
                        break;
                    case 5:
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleMateriales.size() > 0) {
                            for (DetalleMaterialesActas detalleMaterial : listadoDetalleMateriales) {
                                materiales = new Materiales();
                                materiales = gate.obtieneMaterial(detalleMaterial.getId().getMaterialId());
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleMaterial.getId().getMaterialId());
                                serviciosMaterialesMap.put("nombre", detalleMaterial.getMateriales().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleMaterial.getMateriales().getDescripcion());
                                serviciosMaterialesMap.put("cantidadCubicada", detalleMaterial.getTotalUnidadesCubicadas());
                                serviciosMaterialesMap.put("precio", materiales.getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", materiales.getTipoMoneda().getId());
                                serviciosMaterialesMap.put("montoTotalCubicado", 0);
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleMaterial.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleMaterial.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalMaterial", 0);
//                                En contrato llave en mano no se suman los materiales
//                                montoTotalActa = montoTotalActa + detalleMaterial.getTotalMontoEjecucionActa();
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleMaterialesActa", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleMaterialesAdicionales.size() > 0) {
                            for (DetalleMaterialesAdicionalesActas detalleMaterialAdicional : listadoDetalleMaterialesAdicionales) {
                                serviciosMaterialesMap = new HashMap();
                                materiales = new Materiales();
                                materiales = gate.obtieneMaterial(detalleMaterialAdicional.getId().getMaterialId());
                                serviciosMaterialesMap.put("id", detalleMaterialAdicional.getId().getMaterialId());
                                serviciosMaterialesMap.put("nombre", detalleMaterialAdicional.getMateriales().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleMaterialAdicional.getMateriales().getDescripcion());
                                serviciosMaterialesMap.put("montoTotalCubicado", 0);
                                serviciosMaterialesMap.put("cantidadCubicada", 0);
                                serviciosMaterialesMap.put("precio", materiales.getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", materiales.getTipoMoneda().getId());
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleMaterialAdicional.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleMaterialAdicional.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalMaterialAdicional", 0);
//                                En contrato llave en mano no se suman los materiales
//                                montoTotalActa = montoTotalActa + detalleMaterial.getTotalMontoEjecucionActa();
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleMaterialesAdicionalesActa", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleServicios.size() > 0) {
                            for (DetalleServiciosActas detalleServicios : listadoDetalleServicios) {
                                proveedoresServicios = new ArrayList<ProveedoresServicios>();
                                proveedoresServicios = gate.obtieneProveedorServicios(cubicador.getContrato().getId(), cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  detalleServicios.getId().getServicioId());
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleServicios.getId().getServicioId());
                                serviciosMaterialesMap.put("nombre", detalleServicios.getServicio().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleServicios.getServicio().getDescripcion());
                                serviciosMaterialesMap.put("cantidadCubicada", detalleServicios.getTotalUnidadesCubicadas());
                                serviciosMaterialesMap.put("precio", detalleServicios.getServicio().getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", proveedoresServicios.get(0).getTipoMoneda().getId());
                                serviciosMaterialesMap.put("montoTotalCubicado", detalleServicios.getTotalMontoCubicado());
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleServicios.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleServicios.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalServicio", detalleServicios.getTotalMontoEjecucionActa());
                                montoTotalActa = montoTotalActa + detalleServicios.getTotalMontoEjecucionActa();
                                montoTotalCubicado = montoTotalCubicado + (detalleServicios.getServicio().getPrecio() * detalleServicios.getTotalUnidadesCubicadas());
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleServiciosActa", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleServiciosAdicionales.size() > 0) {
                            for (DetalleServiciosAdicionalesActas detalleServiciosAdicionales : listadoDetalleServiciosAdicionales) {
                                proveedoresServicios = new ArrayList<ProveedoresServicios>();
                                proveedoresServicios = gate.obtieneProveedorServicios(cubicador.getContrato().getId(), cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  detalleServiciosAdicionales.getId().getServicioId());
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleServiciosAdicionales.getId().getServicioId());
                                serviciosMaterialesMap.put("nombre", detalleServiciosAdicionales.getServicio().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleServiciosAdicionales.getServicio().getDescripcion());
                                serviciosMaterialesMap.put("cantidadCubicada", detalleServiciosAdicionales.getServicio().getCantidadDefault());
                                serviciosMaterialesMap.put("precio", detalleServiciosAdicionales.getServicio().getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", proveedoresServicios.get(0).getTipoMoneda().getId());
                                serviciosMaterialesMap.put("montoTotalCubicado", detalleServiciosAdicionales.getTotalMontoCubicado());
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleServiciosAdicionales.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleServiciosAdicionales.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalServiciosAdicionales", detalleServiciosAdicionales.getTotalMontoEjecucionActa());
                                montoTotalActa = montoTotalActa + detalleServiciosAdicionales.getTotalMontoEjecucionActa();
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleServiciosAdicionalesActa", listaMaterialesServicios);
                        break;
                    case 8:
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleMateriales.size() > 0) {
                            for (DetalleMaterialesActas detalleMaterial : listadoDetalleMateriales) {
                                materiales = new Materiales();
                                materiales = gate.obtieneMaterial(detalleMaterial.getId().getMaterialId());
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleMaterial.getId().getMaterialId());
                                serviciosMaterialesMap.put("nombre", detalleMaterial.getMateriales().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleMaterial.getMateriales().getDescripcion());
                                serviciosMaterialesMap.put("cantidadCubicada", detalleMaterial.getTotalUnidadesCubicadas());
                                serviciosMaterialesMap.put("precio", materiales.getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", materiales.getTipoMoneda().getId());
                                serviciosMaterialesMap.put("montoTotalCubicado", 0);
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleMaterial.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleMaterial.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalMaterial", 0);
                                montoTotalActa = montoTotalActa + detalleMaterial.getTotalMontoEjecucionActa();
                                montoTotalCubicado = montoTotalCubicado + (detalleMaterial.getTotalUnidadesCubicadas() * materiales.getPrecio());
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleMaterialesActa", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleMaterialesAdicionales.size() > 0) {
                            for (DetalleMaterialesAdicionalesActas detalleMaterialAdicional : listadoDetalleMaterialesAdicionales) {
                                serviciosMaterialesMap = new HashMap();
                                materiales = new Materiales();
                                materiales = gate.obtieneMaterial(detalleMaterialAdicional.getId().getMaterialId());
                                serviciosMaterialesMap.put("id", detalleMaterialAdicional.getId().getMaterialId());
                                serviciosMaterialesMap.put("nombre", detalleMaterialAdicional.getMateriales().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleMaterialAdicional.getMateriales().getDescripcion());
                                serviciosMaterialesMap.put("montoTotalCubicado", 0);
                                serviciosMaterialesMap.put("cantidadCubicada", 0);
                                serviciosMaterialesMap.put("precio", materiales.getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", materiales.getTipoMoneda().getId());
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleMaterialAdicional.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleMaterialAdicional.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalMaterialAdicional", 0);
                                montoTotalActa = montoTotalActa + detalleMaterialAdicional.getTotalMontoEjecucionActa();
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleMaterialesAdicionalesActa", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleServicios.size() > 0) {
                            for (DetalleServiciosActas detalleServicios : listadoDetalleServicios) {
                                proveedoresServicios = new ArrayList<ProveedoresServicios>();
                                proveedoresServicios = gate.obtieneProveedorServicios(cubicador.getContrato().getId(), cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  detalleServicios.getId().getServicioId());
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleServicios.getId().getServicioId());
                                serviciosMaterialesMap.put("nombre", detalleServicios.getServicio().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleServicios.getServicio().getDescripcion());
                                serviciosMaterialesMap.put("cantidadCubicada", detalleServicios.getTotalUnidadesCubicadas());
                                serviciosMaterialesMap.put("precio", detalleServicios.getServicio().getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", proveedoresServicios.get(0).getTipoMoneda().getId());
                                serviciosMaterialesMap.put("montoTotalCubicado", detalleServicios.getTotalMontoCubicado());
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleServicios.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleServicios.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalServicio", detalleServicios.getTotalMontoEjecucionActa());
                                montoTotalActa = montoTotalActa + detalleServicios.getTotalMontoEjecucionActa();
                                montoTotalCubicado = montoTotalCubicado + (detalleServicios.getTotalUnidadesCubicadas() * detalleServicios.getServicio().getPrecio());
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleServiciosActa", listaMaterialesServicios);
                        listaMaterialesServicios = new ArrayList();
                        if (listadoDetalleServiciosAdicionales.size() > 0) {
                            for (DetalleServiciosAdicionalesActas detalleServiciosAdicionales : listadoDetalleServiciosAdicionales) {
                                proveedoresServicios = new ArrayList<ProveedoresServicios>();
                                proveedoresServicios = gate.obtieneProveedorServicios(cubicador.getContrato().getId(), cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  detalleServiciosAdicionales.getId().getServicioId());
                                serviciosMaterialesMap = new HashMap();
                                serviciosMaterialesMap.put("id", detalleServiciosAdicionales.getId().getServicioId());
                                serviciosMaterialesMap.put("nombre", detalleServiciosAdicionales.getServicio().getNombre());
                                serviciosMaterialesMap.put("descripcion", detalleServiciosAdicionales.getServicio().getDescripcion());
                                serviciosMaterialesMap.put("cantidadCubicada", detalleServiciosAdicionales.getServicio().getCantidadDefault());
                                serviciosMaterialesMap.put("precio", detalleServiciosAdicionales.getServicio().getPrecio());
                                serviciosMaterialesMap.put("tipoMonedaId", proveedoresServicios.get(0).getTipoMoneda().getId());
                                serviciosMaterialesMap.put("montoTotalCubicado", detalleServiciosAdicionales.getTotalMontoCubicado());
                                serviciosMaterialesMap.put("cantidadAcumuladaInformada", detalleServiciosAdicionales.getTotalUnidadesEjecucionOt());
                                serviciosMaterialesMap.put("validar", false);
                                serviciosMaterialesMap.put("cantidadActualInformada", detalleServiciosAdicionales.getTotalUnidadesEjecucionActa());
                                serviciosMaterialesMap.put("montoTotalServiciosAdicionales", detalleServiciosAdicionales.getTotalMontoEjecucionActa());
                                montoTotalActa = montoTotalActa + detalleServiciosAdicionales.getTotalMontoEjecucionActa();
                                listaMaterialesServicios.add(serviciosMaterialesMap);
                            }
                        }
                        mapaformActa.put("detalleServiciosAdicionalesActa", listaMaterialesServicios);
                        break;
                }
                double montoTotalApagar = 0;
                if(acta.getMontoTotalAPagar() == null){
                    montoTotalApagar = montoTotalActa;
                } else {
                    montoTotalApagar = acta.getMontoTotalAPagar();
                }
                mapaformActa.put("montoTotalActa", montoTotalActa);
                mapaformActa.put("montoTotalAPagarActa", montoTotalApagar   );
                mapaformActa.put("saldoPendiente", acta.getSaldoPendiente());
                mapaformActa.put("montoTotalCubicado", acta.getMontoCubicado());

                if(contratoId == 1){
                    double montoPagadoActasAnteriores = 0.0;
                    boolean existenActasAnteriores = false;
                    List<Actas> actasValidas = gate.obtenerActasPorOtValidadas(otId);
                    for(Actas actaAnt : actasValidas){
                        if (actaAnt.getId() != acta.getId()){
                            existenActasAnteriores = true;
                            montoPagadoActasAnteriores = montoPagadoActasAnteriores + actaAnt.getMontoTotalAPagar();
                        }
                    }
                    mapaformActa.put("montoPagadoActasAnteriores", montoPagadoActasAnteriores);
                    mapaformActa.put("existenActasAnterioresValidas", existenActasAnteriores);
                }


        }catch (Exception e){
            log.error("Error al obtener detalle de pago para autorizacion, metodo obtieneDetalleMaterialesServicios, error : " + e.toString());
            throw e;
        }finally {
            gate.cerrarSesion();
        }
        return mapaformActa;
    }
    private double obtieneMontoCubicadorOrdinario(int cubicadorId) throws Exception {
        List<CubicadorOrdinario> cubicadorOrdinarios = new ArrayList<CubicadorOrdinario>();
        Gateway gate = new Gateway();
        double montoTotal = 0;
        try {
            gate.abrirSesion();
            cubicadorOrdinarios = gate.obtenerCubicadorOrdinarioPorIdCubicador(cubicadorId);
            for(CubicadorOrdinario cubicadorDetalleAux : cubicadorOrdinarios){
                montoTotal = montoTotal + cubicadorDetalleAux.getTotal();
            }

        } finally {
            gate.cerrarSesion();
        }
        return montoTotal;
    }
    private String parseaMonto(double monto){
        int montoConvertidoInt = (int) monto;
        String montoParaPArsear = Integer.toString(montoConvertidoInt);
        String [] montoAux = montoParaPArsear.split("");
        String parseoTotal = "";
        String parseoTotalInvertido = "";
        int contadorPuntos = 0;
        int contadorCaracteres = montoAux.length;
        for(int i = 1; montoAux.length > i; i++){
            contadorCaracteres = contadorCaracteres - 1;
            parseoTotal = parseoTotal + montoAux[contadorCaracteres];
        }

        String [] montoAuxReverso = parseoTotal.split("");
        for(int i = 0; montoAuxReverso.length > i; i++){
            if(contadorPuntos == 2){
                contadorPuntos = 0;
                parseoTotalInvertido = parseoTotalInvertido + ".";
            } else if(i>1){
                contadorPuntos++;
            }
            parseoTotalInvertido = parseoTotalInvertido + montoAuxReverso[i];
        }

        String[] parseoFinal = parseoTotalInvertido.split("");
        contadorCaracteres = parseoFinal.length;
        String montoFinalParsiado = "";
        for(int i = 1; parseoFinal.length > i; i++){
            contadorCaracteres = contadorCaracteres - 1;
            montoFinalParsiado = montoFinalParsiado + parseoFinal[contadorCaracteres];
        }
        return montoFinalParsiado;
    }
    public Map obtieneJsonCubicadorConListProveedores(Cubicador cubicador, List jsonProveedores, Map jsonProveedorReschazo, int eventoId) {
        Map formularioCambioProveedor = new HashMap();
        List listaServiciosMateriales = new ArrayList();
        Map servicioMaterial = new HashMap();
        Set<CubicadorMateriales> listadoMateriales = cubicador.getCubicadorMateriales();
        Set<CubicadorServicios> listadoServicios = cubicador.getCubicadorServicios();
        Set<CubicadorDetalle> listadoServiciosUnificado = cubicador.getCubicadorServiciosUnificado();
        Set<CubicadorOrdinario> listadoMaterialesOrdinarios = cubicador.getCubicadorOrdinario();
        String coma = "";
        int i = 0;
        double totalCosteo = 0;
        TipoMoneda tipoMoneda = new TipoMoneda();
        Map contrato = new HashMap();

        Gateway gate = new Gateway();

        try{
            gate.abrirSesion();
            switch (cubicador.getContrato().getId()){
                case 1:
//                contrato SBE

                    for(CubicadorServicios servicios : listadoServicios){
                        if(i != 0){
                            coma = ",";
                        } else {
                            List<ProveedoresServicios> proveedoresServicios = gate.obtieneProveedorServicios(cubicador.getContrato().getId(),  cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  servicios.getServicio().getId());
                            tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();
                        }
                        servicioMaterial = new HashMap();
                        servicioMaterial.put("id", servicios.getServicio().getId());
                        servicioMaterial.put("nombreServicio", servicios.getServicio().getNombre());
                        servicioMaterial.put("descripcion", servicios.getServicio().getDescripcion());
                        servicioMaterial.put("cantidad", servicios.getCantidad());
                        servicioMaterial.put("precio", servicios.getPrecio());
                        listaServiciosMateriales.add(servicioMaterial);
                        totalCosteo = totalCosteo + servicios.getTotal();
                        i = i + 1;

                    }
                     contrato.put("id", cubicador.getContrato().getId());
                    contrato.put("nombre", cubicador.getContrato().getNombre());

                    formularioCambioProveedor.put("eventoId", eventoId);
                    formularioCambioProveedor.put("cubicacionId", cubicador.getId());
                    formularioCambioProveedor.put("nombre", cubicador.getNombre());
                    formularioCambioProveedor.put("descripcion", cubicador.getDescripcion());
                    formularioCambioProveedor.put("contrato", contrato);
                    formularioCambioProveedor.put("regionId", cubicador.getRegion().getId());
                    formularioCambioProveedor.put("region", cubicador.getRegion().getNombre());
                    formularioCambioProveedor.put("montoTotal", totalCosteo);
                    formularioCambioProveedor.put("tipoMonedaId", tipoMoneda.getId());
                    formularioCambioProveedor.put("tipoMonedaNombre", tipoMoneda.getNombre());
                    formularioCambioProveedor.put("listaServiciosMateriales", listaServiciosMateriales);
                    formularioCambioProveedor.put("proveedorRechazo", jsonProveedorReschazo);
                    formularioCambioProveedor.put("listaProveedores", jsonProveedores);

                    break;
                case 3:
//                contrato RAN
                    for(CubicadorServicios servicios : listadoServicios){
                        if(i != 0){
                            coma = ",";
                        } else {
                            List<ProveedoresServicios> proveedoresServicios = gate.obtieneProveedorServicios(cubicador.getContrato().getId(),  cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  servicios.getServicio().getId());
                            tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();
                        }
                        servicioMaterial = new HashMap();
                        servicioMaterial.put("id", servicios.getServicio().getId());
                        servicioMaterial.put("nombreServicio", servicios.getServicio().getNombre());
                        servicioMaterial.put("descripcion", servicios.getServicio().getDescripcion());
                        servicioMaterial.put("cantidad", servicios.getCantidad());
                        servicioMaterial.put("precio", servicios.getPrecio());
                        listaServiciosMateriales.add(servicioMaterial);
                        totalCosteo = totalCosteo + servicios.getTotal();
                        i = i + 1;

                    }
                    contrato.put("id", cubicador.getContrato().getId());
                    contrato.put("nombre", cubicador.getContrato().getNombre());

                    formularioCambioProveedor.put("eventoId", eventoId);
                    formularioCambioProveedor.put("cubicacionId", cubicador.getId());
                    formularioCambioProveedor.put("nombre", cubicador.getNombre());
                    formularioCambioProveedor.put("descripcion", cubicador.getDescripcion());
                    formularioCambioProveedor.put("contrato", contrato);
                    formularioCambioProveedor.put("regionId", cubicador.getRegion().getId());
                    formularioCambioProveedor.put("region", cubicador.getRegion().getNombre());
                    formularioCambioProveedor.put("montoTotal", totalCosteo);
                    formularioCambioProveedor.put("tipoMonedaId", tipoMoneda.getId());
                    formularioCambioProveedor.put("tipoMonedaNombre", tipoMoneda.getNombre());
                    formularioCambioProveedor.put("listaServiciosMateriales", listaServiciosMateriales);
                    formularioCambioProveedor.put("proveedorRechazo", jsonProveedorReschazo);
                    formularioCambioProveedor.put("listaProveedores", jsonProveedores);

                    break;
                case 4:
//                contrato UNIFICADO
                    for(CubicadorDetalle servicios : listadoServiciosUnificado){
                        if(i != 0){
                            coma = ",";
                        } else {
                            tipoMoneda = gate.obtenertipoMonedaPorId(servicios.getTipoMoneda().getId());
                        }
                        servicioMaterial = new HashMap();
                        servicioMaterial.put("id", servicios.getServicios().getId());
                        servicioMaterial.put("nombreServicio", servicios.getServicios().getNombre());
                        servicioMaterial.put("descripcion", servicios.getServicios().getDescripcion());
                        servicioMaterial.put("regionId", servicios.getRegionId());
                        servicioMaterial.put("nombreRegion", servicios.getRegiones().getNombre());
                        servicioMaterial.put("agenciaId", servicios.getAgenciaId());
                        servicioMaterial.put("nombreAgencia", servicios.getAgencia().getNombre());
                        servicioMaterial.put("centralId", servicios.getCentralId());
                        servicioMaterial.put("nombreCentral", servicios.getCentrales().getNombre());
                        servicioMaterial.put("cantidad", servicios.getCantidad());
                        servicioMaterial.put("precio", servicios.getPrecio());
                        listaServiciosMateriales.add(servicioMaterial);

                        totalCosteo = totalCosteo + servicios.getTotal();
                        i = i+1;
                    }

                    contrato.put("id", cubicador.getContrato().getId());
                    contrato.put("nombre", cubicador.getContrato().getNombre());

                    formularioCambioProveedor.put("eventoId", eventoId);
                    formularioCambioProveedor.put("cubicacionId", cubicador.getId());
                    formularioCambioProveedor.put("nombre", cubicador.getNombre());
                    formularioCambioProveedor.put("descripcion", cubicador.getDescripcion());
                    formularioCambioProveedor.put("contrato", contrato);
                    formularioCambioProveedor.put("montoTotal", totalCosteo);
                    formularioCambioProveedor.put("tipoMonedaId", tipoMoneda.getId());
                    formularioCambioProveedor.put("tipoMonedaNombre", tipoMoneda.getNombre());
                    formularioCambioProveedor.put("listaServiciosMateriales", listaServiciosMateriales);
                    formularioCambioProveedor.put("proveedorRechazo", jsonProveedorReschazo);
                    formularioCambioProveedor.put("listaProveedores", jsonProveedores);
                    break;
                case 5:
//                Contrato LLave en Mano
                    for(CubicadorServicios servicios : listadoServicios){
                        if(i != 0){
                            coma = ",";
                        } else {
                            List<ProveedoresServicios> proveedoresServicios = gate.obtieneProveedorServicios(cubicador.getContrato().getId(),  cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  servicios.getServicio().getId());
                            tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();
                        }
                        servicioMaterial = new HashMap();
                        servicioMaterial.put("id", servicios.getServicio().getId());
                        servicioMaterial.put("nombreServicio", servicios.getServicio().getNombre());
                        servicioMaterial.put("descripcion", servicios.getServicio().getDescripcion());
                        servicioMaterial.put("cantidad", servicios.getCantidad());
                        servicioMaterial.put("precio", servicios.getPrecio());
                        listaServiciosMateriales.add(servicioMaterial);
                        totalCosteo = totalCosteo + servicios.getTotal();
                        i = i + 1;

                    }
                    contrato.put("id", cubicador.getContrato().getId());
                    contrato.put("nombre", cubicador.getContrato().getNombre());

                    formularioCambioProveedor.put("eventoId", eventoId);
                    formularioCambioProveedor.put("cubicacionId", cubicador.getId());
                    formularioCambioProveedor.put("nombre", cubicador.getNombre());
                    formularioCambioProveedor.put("descripcion", cubicador.getDescripcion());
                    formularioCambioProveedor.put("contrato", contrato);
                    formularioCambioProveedor.put("regionId", cubicador.getRegion().getId());
                    formularioCambioProveedor.put("region", cubicador.getRegion().getNombre());
                    formularioCambioProveedor.put("montoTotal", totalCosteo);
                    formularioCambioProveedor.put("tipoMonedaId", tipoMoneda.getId());
                    formularioCambioProveedor.put("tipoMonedaNombre", tipoMoneda.getNombre());
                    formularioCambioProveedor.put("listaServiciosMateriales", listaServiciosMateriales);
                    formularioCambioProveedor.put("proveedorRechazo", jsonProveedorReschazo);
                    formularioCambioProveedor.put("listaProveedores", jsonProveedores);

                    break;


            }

        } catch (Exception e){
            log.error("error al reasignar proveedor: "+ e.toString());
        } finally {
            gate.cerrarSesion();
        }

        return formularioCambioProveedor;
    }
    public List obtenerCubicacionDistintoProveedorAction(int otId, int cubicadorId, int nuevoProveedorId) throws Exception {
        List respuesta = new ArrayList();
        Map nuevosServiciosMateriales = new HashMap();
        Gateway gateway = new Gateway();

        try {
            gateway.abrirSesion();
            Cubicador cubicador = new Cubicador();
            cubicador = gateway.obtenerDetalleCubicacion(cubicadorId);
            Set<CubicadorMateriales> listadoMateriales = cubicador.getCubicadorMateriales();
            Set<CubicadorServicios> listadoServicios = cubicador.getCubicadorServicios();
            Set<CubicadorDetalle> listadoServiciosUnificado = cubicador.getCubicadorServiciosUnificado();
            Set<CubicadorOrdinario> listadoMaterialesOrdinarios = cubicador.getCubicadorOrdinario();
            String coma = "";
            int i = 0;
            double totalCosteo = 0;
            TipoMoneda tipoMoneda = new TipoMoneda();
            String contrato = "";
            List<ProveedoresServicios> proveedoresServicios = new ArrayList<ProveedoresServicios>();
            switch (cubicador.getContrato().getId()){
                case 1:
//                contrato SBE

                    for(CubicadorServicios servicios : listadoServicios){

                        nuevosServiciosMateriales = new HashMap();

                        proveedoresServicios = gateway.obtieneProveedorServicios(cubicador.getContrato().getId(),  cubicador.getRegion().getId(),  nuevoProveedorId,  servicios.getServicio().getId());
                        if(proveedoresServicios.size() > 0) {
                            tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();

                            nuevosServiciosMateriales.put("id", servicios.getServicio().getId());
                            nuevosServiciosMateriales.put("nombreServicio", servicios.getServicio().getNombre());
                            nuevosServiciosMateriales.put("descripcion", servicios.getServicio().getDescripcion());
                            nuevosServiciosMateriales.put("cantidad", servicios.getCantidad());
                            nuevosServiciosMateriales.put("precio", proveedoresServicios.get(0).getPrecio());

                            respuesta.add(nuevosServiciosMateriales);
                            totalCosteo = totalCosteo + (servicios.getCantidad() * proveedoresServicios.get(0).getPrecio());
                        }
                    }

                    break;
                case 3:
//                contrato RAN
                    for(CubicadorServicios servicios : listadoServicios){

                        nuevosServiciosMateriales = new HashMap();

                        proveedoresServicios = gateway.obtieneProveedorServicios(cubicador.getContrato().getId(),  cubicador.getRegion().getId(),  nuevoProveedorId,  servicios.getServicio().getId());
                        if(proveedoresServicios.size() > 0) {
                            tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();

                            nuevosServiciosMateriales.put("id", servicios.getServicio().getId());
                            nuevosServiciosMateriales.put("nombreServicio", servicios.getServicio().getNombre());
                            nuevosServiciosMateriales.put("descripcion", servicios.getServicio().getDescripcion());
                            nuevosServiciosMateriales.put("cantidad", servicios.getCantidad());
                            nuevosServiciosMateriales.put("precio", proveedoresServicios.get(0).getPrecio());

                            respuesta.add(nuevosServiciosMateriales);
                            totalCosteo = totalCosteo + (servicios.getCantidad() * proveedoresServicios.get(0).getPrecio());
                        }
                    }
//

                    break;
                case 4:
//                contrato UNIFICADO
                    for(CubicadorDetalle servicios : listadoServiciosUnificado){

                        nuevosServiciosMateriales = new HashMap();

                        proveedoresServicios = gateway.obtieneProveedorServicios(cubicador.getContrato().getId(),  servicios.getRegionId(),  nuevoProveedorId,  servicios.getServicios().getId());

                        if(proveedoresServicios.size() > 0) {
                            tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();
                            nuevosServiciosMateriales.put("id", servicios.getServicios().getId());
                            nuevosServiciosMateriales.put("nombreServicio", servicios.getServicios().getNombre());
                            nuevosServiciosMateriales.put("descripcion", servicios.getServicios().getDescripcion());
                            nuevosServiciosMateriales.put("regionId", servicios.getRegiones().getId());
                            nuevosServiciosMateriales.put("nombreRegion", servicios.getRegiones().getNombre());
                            nuevosServiciosMateriales.put("agenciaId", servicios.getAgencia().getId());
                            nuevosServiciosMateriales.put("nombreAgencia", servicios.getAgencia().getNombre());
                            nuevosServiciosMateriales.put("centralId", servicios.getCentrales().getId());
                            nuevosServiciosMateriales.put("nombreCentral", servicios.getCentrales().getNombre());
                            nuevosServiciosMateriales.put("nombreServicio", servicios.getServicios().getNombre());
                            nuevosServiciosMateriales.put("cantidad", servicios.getCantidad());
                            nuevosServiciosMateriales.put("precio", proveedoresServicios.get(0).getPrecio());

                            respuesta.add(nuevosServiciosMateriales);
                            totalCosteo = totalCosteo + (servicios.getCantidad() * proveedoresServicios.get(0).getPrecio());
                        }
                    }

                    break;
                case 5:
//                Contrato LLave en Mano
                    for(CubicadorServicios servicios : listadoServicios){

                        nuevosServiciosMateriales = new HashMap();

                        proveedoresServicios = gateway.obtieneProveedorServicios(cubicador.getContrato().getId(),  cubicador.getRegion().getId(),  nuevoProveedorId,  servicios.getServicio().getId());
                        if(proveedoresServicios.size() > 0) {
                            tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();

                            nuevosServiciosMateriales.put("id", servicios.getServicio().getId());
                            nuevosServiciosMateriales.put("nombreServicio", servicios.getServicio().getNombre());
                            nuevosServiciosMateriales.put("descripcion", servicios.getServicio().getDescripcion());
                            nuevosServiciosMateriales.put("cantidad", servicios.getCantidad());
                            nuevosServiciosMateriales.put("precio", proveedoresServicios.get(0).getPrecio());

                            respuesta.add(nuevosServiciosMateriales);
                            totalCosteo = totalCosteo + (servicios.getCantidad() * proveedoresServicios.get(0).getPrecio());
                        }
                    }
                    break;
            }
        } catch (Exception e){
            log.error("Error al obtener cubicacion al cambiar proveedor: "+e.toString());
            throw e;

        } finally {
            gateway.cerrarSesion();
        }

        return respuesta;
    }

//    Inicio Cubicador para Bucle:

    public List<Regiones> listarRegiones()throws Exception {
        List<Regiones> regionesList = new ArrayList<Regiones>();
        Gateway gateway = new Gateway();
        try{
            gateway.abrirSesion();
            regionesList = gateway.obtieneRegiones();
            for (Regiones actividad : regionesList){
                gateway.validarYSetearObjetoARetornar(actividad, null);
            }
        }catch (Exception e){
            throw e;
        }finally {
            gateway.cerrarSesion();
        }
        return regionesList;
    }

    public List<Actividades> listarActividades() throws Exception {
        List<Actividades> actividadesList = new ArrayList<Actividades>();
        Gateway gateway = new Gateway();
        try{
            gateway.abrirSesion();
            actividadesList = gateway.obtieneActividades();
            for (Actividades actividad : actividadesList){
                gateway.validarYSetearObjetoARetornar(actividad, null);
            }
        }catch (Exception e){
            throw e;
        }finally {
            gateway.cerrarSesion();
        }
        return actividadesList;
    }


    public List<Especialidades> listarEspecialidades(int actividadId)throws Exception {
        List<Especialidades> especialidadesList = new ArrayList<Especialidades>();
        Gateway gateway = new Gateway();
        try{
            gateway.abrirSesion();
            especialidadesList = gateway.obtieneEspecialidades(actividadId);
            for (Especialidades especialidad : especialidadesList){
                gateway.validarYSetearObjetoARetornar(especialidad, null);
            }
        }catch (Exception e){
            throw e;
        }finally {
            gateway.cerrarSesion();
        }
        return especialidadesList;
    }

    public boolean eliminaCubicacion(int cubicadorId, int usuarioId) throws Exception{
        boolean respuesta = false;
        Cubicador cubicador = new Cubicador();
        Gateway gateway = new Gateway();
        try{
            gateway.abrirSesion();
            cubicador = gateway.obtenerCubicadorPorUsuarioIdyPorId(cubicadorId, usuarioId);
            if(cubicador == null){
                throw new Exception("Esta intentando eliminar un costeo con un id inexistente, numero ID de costeo : "+ cubicadorId);
            } else if(cubicador.getOt() == null){
                respuesta = gateway.eliminarCosteo(cubicador);
            } else if(cubicador.getOt() != null){
                //System.out.println("Esta intentando eliminar un costeo que esta asociado a una OT, el numero de la OT es: "+ cubicador.getOt().getId());
                throw new Exception("Esta intentando eliminar un costeo que esta asociado a una OT, el numero de la OT es: "+ cubicador.getOt().getId());
            }

        }catch (Exception e){
            throw e;
        }finally {
            gateway.cerrarSesion();
        }
        return respuesta;
    }



    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static final Logger log = Logger.getLogger(CubicadorDAO.class);
    private static final CubicadorDAO INSTANCE = new CubicadorDAO();

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public double obtenerValorModificacionVial(int agenciaId, double puntoBaremos) throws Exception{
        double valor = 0;

        Gateway gateway = new Gateway();
        try{
            gateway.abrirSesion();
            valor = gateway.obtenerValorModificacionVial(agenciaId,puntoBaremos);
        }catch (Exception e){
            throw e;
        }finally {
            gateway.cerrarSesion();
        }

        return valor;

    }

    public Map obtieneDetalleBucle(int cubicadorId, Map mapFormActaBucle) throws Exception {
        Gateway gateway = new Gateway();
        Cubicador cubicador = new Cubicador();
        try{
            gateway.abrirSesion();
            cubicador = gateway.obtenerDetalleCubicacion(cubicadorId);
            mapFormActaBucle.put("servicios", cubicador);
            mapFormActaBucle.put("serviciosAdicionales", "");

            return mapFormActaBucle;
        }catch (Exception e){
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }
}
