package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.CubicadorDireccionesBucle;
import com.skydream.coreot.pojos.Materiales;

import java.util.List;

/**
 * Created by Esteam on 03-10-16.
 */
public class CubicadorServiciosServiciosUnidadBucleResponse {

    private double numero;
    private String actividadNombre;
    private String especialidadNombre;
    //Datos Servicio
    private String descripcionServicio;
    private String unidadMedidaServicio;
    private int cantidadServicio;
    private double precioServicio;
    private double puntosBaremos;
    private double totalServicio;

    //Datos ServicioUnidad;
    private String descripcionServicioUnidad;
    private String unidadMedidaServicioUnidad;
    private int cantidadServicioUnidad;
    private double precioServicioUnidad;
    private double totalServicioUnidad;
    //Datos Finales
    private double totalFinal;
    private CubicadorDireccionesBucle direccion;
    private List<MaterialesBucleResponse> materialesCTCResponse;
    private List<MaterialesBucleResponse> materialesEECCResponse;

    public void setPuntosBaremos(Double puntosBaremos) {
        this.puntosBaremos = puntosBaremos;
    }

    public int getCantidadServicio() {
        return cantidadServicio;
    }

    public void setCantidadServicio(int cantidadServicio) {
        this.cantidadServicio = cantidadServicio;
    }

    public double getPuntosBaremos() {
        return puntosBaremos;
    }

    public void setTotalServicio(double totalServicio) {
        this.totalServicio = totalServicio;
    }

    public void setPrecioServicio(double precioServicio) {
        this.precioServicio = precioServicio;
    }

    public double getPrecioServicio() {
        return precioServicio;
    }

    public double getTotalServicio() {
        return totalServicio;
    }

    public void setActividadNombre(String actividadNombre) {
        this.actividadNombre = actividadNombre;
    }

    public String getActividadNombre() {
        return actividadNombre;
    }

    public String getEspecialidadNombre() {
        return especialidadNombre;
    }

    public void setEspecialidadNombre(String especialidadNombre) {
        this.especialidadNombre = especialidadNombre;
    }

    public double getPrecioServicioUnidad() {
        return precioServicioUnidad;
    }

    public void setPrecioServicioUnidad(double precioServicioUnidad) {
        this.precioServicioUnidad = precioServicioUnidad;
    }

    public double getTotalServicioUnidad() {
        return totalServicioUnidad;
    }

    public void setTotalServicioUnidad(double totalServicioUnidad) {
        this.totalServicioUnidad = totalServicioUnidad;
    }

    public double getTotalFinal() {
        return totalFinal;
    }

    public void setTotalFinal(double totalFinal) {
        this.totalFinal = totalFinal;
    }

    public int getCantidadServicioUnidad() {
        return cantidadServicioUnidad;
    }

    public void setCantidadServicioUnidad(int cantidadServicioUnidad) {
        this.cantidadServicioUnidad = cantidadServicioUnidad;
    }

    public String getDescripcionServicio() {
        return descripcionServicio;
    }

    public void setDescripcionServicio(String descripcionServicio) {
        this.descripcionServicio = descripcionServicio;
    }

    public String getDescripcionServicioUnidad() {
        return descripcionServicioUnidad;
    }

    public void setDescripcionServicioUnidad(String descripcionServicioUnidad) {
        this.descripcionServicioUnidad = descripcionServicioUnidad;
    }

    public double getNumero() {
        return numero;
    }

    public void setNumero(double numero) {
        this.numero = numero;
    }

    public String getUnidadMedidaServicio() {
        return unidadMedidaServicio;
    }

    public void setUnidadMedidaServicio(String unidadMedidaServicio) {
        this.unidadMedidaServicio = unidadMedidaServicio;
    }

    public String getUnidadMedidaServicioUnidad() {
        return unidadMedidaServicioUnidad;
    }

    public void setUnidadMedidaServicioUnidad(String unidadMedidaServicioUnidad) {
        this.unidadMedidaServicioUnidad = unidadMedidaServicioUnidad;
    }

    public void setDireccion(CubicadorDireccionesBucle direccion) {
        this.direccion = direccion;
    }

    public CubicadorDireccionesBucle getDireccion() {
        return direccion;
    }

    public List<MaterialesBucleResponse> getMaterialesCTCResponse() {
        return materialesCTCResponse;
    }

    public void setMaterialesCTCResponse(List<MaterialesBucleResponse> materialesCTCResponse) {
        this.materialesCTCResponse = materialesCTCResponse;
    }

    public List<MaterialesBucleResponse> getMaterialesEECCResponse() {
        return materialesEECCResponse;
    }

    public void setMaterialesEECCResponse(List<MaterialesBucleResponse> materialesEECCResponse) {
        this.materialesEECCResponse = materialesEECCResponse;
    }
}
