/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;

/**
 *
 * @author mcj - RRR
 */
public class EventoDAO implements GatewayDAO {

    private EventoDAO() {
    }

    public static EventoDAO getINSTANCE() {
        return INSTANCE;
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        Eventos evento = (Eventos) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearAcciones(params, gateway, evento);
//            setearAlarmas(params, gateway, evento);
            gateway.iniciarTransaccion();
            try {
                gateway.registrar(obj);
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al crear Evento. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        Eventos evento = (Eventos) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearAcciones(params, gateway, evento);
//            setearAlarmas(params, gateway, evento);
            gateway.iniciarTransaccion();
            try {
                gateway.actualizar2(obj);
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al actualizar Evento. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    @Override
    public List listar(ObjetoCoreOT objetoCoreOT, Map params) throws Exception {
        Eventos evento = (Eventos) objetoCoreOT;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = gate.listar(evento, join);
            for (ObjetoCoreOT objeto : listaRetorno) {
                Eventos eve = (Eventos) objeto;
                join.add("eventosAcciones");
                join.add("eventosAlarmas");
                gate.validarYSetearObjetoARetornar(eve, join);
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    private void setearAcciones(Map params, Gateway gateway, Eventos evento) throws Exception {
        if (params.containsKey("eventosAcciones")) {
            String[] join = {"tipoAccion"};
            List<Map> listaAcciones = (List<Map>) params.get("eventosAcciones");
            for (Map acc : listaAcciones) {
                Integer id = (Integer) acc.get("id");
                Acciones accion = (Acciones) gateway.obtenerDatoPorId(new Acciones(id), join);
                evento.getEventosAcciones().add(accion);
            }
        }
    }

    private void setearAlarmas(Map params, Gateway gateway, Eventos evento) throws Exception {
        if (params.containsKey("eventosAlarmas")) {
            List<Map> listadoAlarmas = (ArrayList<Map>) params.get("eventosAlarmas");
            for (Map mapEveAla : listadoAlarmas) {

                Map alarma = (HashMap) mapEveAla.get("alarmas");
                int idAlarma = (Integer) alarma.get("id");

                Alarmas ala = (Alarmas) gateway.retornarObjetoCoreOT(Alarmas.class, idAlarma);
                Hibernate.initialize(ala);

                String mensaje = mapEveAla.get("mensaje").toString();
                //Integer estadoAlarma = (Integer) mapEveAla.get("estadoAlarma");

                EventosAlarmas eveAla = new EventosAlarmas(evento, ala, mensaje);
                evento.getEventosAlarmas().add(eveAla);
            }
        } else {
            throw new Exception("Para crear 1 evento se debe enviar listado de 'eventosAlarmas'");
        }
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static final Logger log = Logger.getLogger(EventoDAO.class);
    private static final EventoDAO INSTANCE = new EventoDAO();

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    // Metodos implementados para el ejecutor dinamico
    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idEvento) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            Eventos evento = (Eventos) gate.retornarObjetoCoreOT2(Eventos.class, idEvento);
            if (evento != null) {

                Set<EventosAcciones> listadoAcciones = setearEventosAcciones(evento);
                evento.setEventosAcciones(listadoAcciones);
            }
            return evento;
        } finally {
            gate.cerrarSesion();
        }
    }

    private TreeSet<EventosAcciones> setearEventosAcciones(Eventos evento) {
        TreeSet<EventosAcciones> listadoAcciones = new TreeSet<>();
        Set<EventosAcciones> acciones = evento.getEventosAcciones();
        for (EventosAcciones acc : acciones) {
            Acciones accionObjAux = acc.getAcciones();
            // hay q ir a base de datos a buscar el id segun el nombre Back-End
            if (accionObjAux.getTipoAccion().getNombre().equals("Back-End") ) {
                TipoAccion tipoAccion = new TipoAccion(accionObjAux.getTipoAccion().getId());
                Acciones newAccion = new Acciones(accionObjAux.getId(), tipoAccion, accionObjAux.getNombre(), 
                                accionObjAux.getDescripcion(), accionObjAux.getCodigo(), accionObjAux.getTipoEjecucionBackEnd(), null, null);
                Set<AccionesParametros> newListadoParametros = retornarAccionesParametros(accionObjAux);
                newAccion.setAccionesParametros(newListadoParametros);
                EventosAcciones eveAcciones = new EventosAcciones(newAccion, null, acc.getOrden());
                listadoAcciones.add(eveAcciones);
            }
        }
        return listadoAcciones;
    }

    private TreeSet<AccionesParametros> retornarAccionesParametros(Acciones accionObjAux) {
        TreeSet<AccionesParametros> newListadoParametros = new TreeSet<>();
        Set<AccionesParametros> parametros = accionObjAux.getAccionesParametros();
        for (AccionesParametros accionParam : parametros) {
            Parametros param = accionParam.getParametros();
            TipoParametro tipoParametro = new TipoParametro(param.getTipoParametro().getId());
            Parametros newParametro = new Parametros(param.getId(), tipoParametro, param.getNombre(), param.getDescripcion(), null, null);
            Set<DetalleParametros> newSetDetalleParametros = new HashSet<>();
            Set<DetalleParametros> detParams = param.getDetalleParametros();
            for (DetalleParametros detalle : detParams) {
                DetalleParametros newDetalleParametros = new DetalleParametros(detalle.getId(), newParametro, detalle.getLlave(), detalle.getValor());
                newSetDetalleParametros.add(newDetalleParametros);
            }
            newParametro.setDetalleParametros(newSetDetalleParametros);
            AccionesParametros newAccionParametro = new AccionesParametros(null, newParametro, accionParam.getOrden());
            newListadoParametros.add(newAccionParametro);
        }
        return newListadoParametros;
    }

    public ObtenerAccionesFrontEndAux listarAccionesFrontendPorEvento(int eventoId) throws Exception {
        ObtenerAccionesFrontEndAux respuesta = new ObtenerAccionesFrontEndAux();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            Eventos evento = (Eventos) gate.retornarObjetoCoreOT2(Eventos.class, eventoId);
            if (evento != null) {
                Set<EventosAcciones> listadoAcciones = GenericDAO.getINSTANCE().setearEventosAccionesPorTipoAccion(evento);
                evento.setEventosAcciones(listadoAcciones);
            }
            TreeSet listadoAcciones = (TreeSet) evento.getEventosAcciones();
            respuesta.setEventosAcciones(listadoAcciones);
            respuesta.setIdEvento(eventoId);
            return respuesta;
        } finally {
            gate.cerrarSesion();
        }
    }
//
//    public int obtenerPrimerEventoDelFlujoSegunWorkflow(int contratoId) throws Exception {
//        int idEvento = 0;
//        Gateway gate = new Gateway();
//        try {
//            gate.abrirSesion();
//            idEvento =  gate.obtenerPrimerEventoDelFlujoSegunWorkflow(contratoId);
//
//            return idEvento;
//        } finally {
//            gate.cerrarSesion();
//        }
//    }

    public ObtenerAccionesFrontEndAux listarAccionesFrontendPorOt(int otId) throws Exception {
        ObtenerAccionesFrontEndAux respuesta = new ObtenerAccionesFrontEndAux();
        Gateway gate = new Gateway();
        int eventoId = 0;
        try {
            gate.abrirSesion();
            // obtener el eventoId proximo
            eventoId = gate.obtenerEventoProximoEjecutarPorOt(otId);
            Eventos evento = (Eventos) gate.retornarObjetoCoreOT2(Eventos.class, eventoId);
            if (evento != null) {
                Set<EventosAcciones> listadoAcciones = GenericDAO.getINSTANCE().setearEventosAccionesPorTipoAccion(evento);
                evento.setEventosAcciones(listadoAcciones);

            }
            TreeSet listadoAcciones = (TreeSet) evento.getEventosAcciones();
            respuesta.setEventosAcciones(listadoAcciones);
            respuesta.setIdEvento(eventoId);
            return respuesta;
        } finally {
            gate.cerrarSesion();
        }
    }

}
