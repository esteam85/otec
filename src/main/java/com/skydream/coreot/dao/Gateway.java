/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.*;
import com.skydream.coreot.util.Config;
import com.skydream.coreot.util.NewHibernateUtil;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.*;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.LinkedHashMap;
import java.util.List;

import com.skydream.eventmanager.GatewayAcciones;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.*;
import org.hibernate.InstantiationException;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import org.hibernate.criterion.Order;


/**
 * @author mcj
 */
public class Gateway {

    protected void abrirSesion() throws Exception {
        try {
            SessionFactory sesionFactory = NewHibernateUtil.getSessionFactory();
            this.sesion = sesionFactory.openSession();
        } catch (Exception e) {
            System.out.println("No se pudo iniciar sesión. " + e.getStackTrace().toString());
            log.error("===========================================================");
            log.error("Error al abrir session Gateways, error: "+e.toString());
            throw e;
        }
    }

    protected void cerrarSesion() {
        this.sesion.close();
    }

    protected void rollback() {
        this.trx.rollback();
    }

    protected void commit() {
        this.trx.commit();
    }

    protected void iniciarTransaccion() {
        this.trx = sesion.beginTransaction();
    }

    protected Integer registrar(ObjetoCoreOT obj) throws Exception {
        int registro = 0;
        try {
            registro = (Integer) sesion.save(obj);
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + obj.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return registro;
    }

    protected void registrarObjCoreOT(ObjetoCoreOT obj) throws Exception {
        Serializable registro = null;
        try {
            registro = sesion.save(obj);
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + obj.getClass().toString());
            System.out.println(e);
            throw e;
        }
    }

    protected ObjetoCoreOT obtenerRegistroConFiltro(Class claseObjeto, List<String> joinObjCoreOT, Map<String, Object> filtros) throws Exception {
        ObjetoCoreOT registroRetorno = null;
        try {
            Criteria criteria = sesion.createCriteria(claseObjeto);
            if (joinObjCoreOT != null) {
                for (String join : joinObjCoreOT) {
                    criteria.setFetchMode(join, FetchMode.JOIN);
                }
            }
            for (Map.Entry<String, Object> item : filtros.entrySet()) {
                criteria.add(Restrictions.eq(item.getKey(), item.getValue()));
            }
//            registroRetorno = (ObjetoCoreOT)criteria.uniqueResult();
            List<ObjetoCoreOT> registros = criteria.list();
            int cantRegistros = registros.size();
            if (cantRegistros > 1) {
                StringBuffer msjeError = new StringBuffer("Se encontró mas de 1 registro en consulta. ");
                setearMensajeErrorParaConsultaGenericaConFiltros(msjeError, claseObjeto, filtros);
                throw new Exception("Error al consultar registro único. ");
            } else if (cantRegistros == 0) {
                StringBuffer msjeError = new StringBuffer("NO se encontraron registros en consulta. ");
                setearMensajeErrorParaConsultaGenericaConFiltros(msjeError, claseObjeto, filtros);
                log.error(msjeError);
            } else {
                registroRetorno = registros.get(0);
            }

        } catch (HibernateException e) {
            throw e;
        }
        return registroRetorno;
    }

    private void setearMensajeErrorParaConsultaGenericaConFiltros(StringBuffer msjeError, Class claseObjeto, Map<String, Object> filtros) {
        msjeError.append("Objeto: ");
        msjeError.append(claseObjeto);
        msjeError.append(". Filtros : [");
        int contAux = 0;
        for (Map.Entry<String, Object> item : filtros.entrySet()) {
            if (contAux > 0) {
                msjeError.append(",");
            }
            msjeError.append("{nombreFiltro: ");
            msjeError.append(item.getKey());
            msjeError.append(", ");
            msjeError.append("valorFiltro: ");
            msjeError.append(item.getValue());
            msjeError.append("}\n");
            contAux++;
        }
        msjeError.append("]");
    }

    protected List<ProveedoresServicios> listarServiciosPorProveedorRegionContrato(int contratoId, int regionId, int proveedorId, int tipoServicioId) throws Exception {

        try {

            Query query = sesion.createSQLQuery(
                    "SELECT PS.PROVEEDOR_ID, PS.SERVICIO_ID, PS.REGION_ID, PS.PRECIO, PS.TIPO_MONEDA_ID, PS.CONTRATO_ID, PS.ID "
                    + "FROM PROVEEDORES_SERVICIOS PS "
                    + "INNER JOIN SERVICIOS SERV ON PS.SERVICIO_ID = SERV.ID \n"
                    + "WHERE PS.CONTRATO_ID = :contratoId AND PS.REGION_ID = :regionId AND PS.PROVEEDOR_ID = :proveedorId AND SERV.TIPO_SERVICIO_ID = :tipoServicioId"
            )
                    .addEntity(ProveedoresServicios.class)
                    .setParameter("contratoId", contratoId)
                    .setParameter("regionId", regionId)
                    .setParameter("proveedorId", proveedorId)
                    .setParameter("tipoServicioId", tipoServicioId);

            List<ProveedoresServicios> result = query.list();
            return result;
        } catch (HibernateException hExcep) {
            log.error("Error al obtener roles desde la entidad workflowEventosEjecucion");
            throw hExcep;
        }

    }

    protected List<ServiciosAux> listarServiciosPorProveedorRegionContrato2(int contratoId, int regionId, int proveedorId, int idTipoServicio, Cubicador cubicador) throws Exception {

        List<ServiciosAux> listadoServicios = new ArrayList<>();
        TipoMoneda tipoMonedaProxy = null;

        try {
            Criteria criteria = sesion.createCriteria(ProveedoresServicios.class)
                    .setFetchMode("tipoMoneda", FetchMode.JOIN)
                    .setFetchMode("servicios", FetchMode.JOIN);
            criteria.add(
                    Restrictions.and(

                            Restrictions.eq("proveedores.id", proveedorId),
                            Restrictions.eq("contratos.id", contratoId),
                            Restrictions.eq("regiones.id", regionId)
                    )
            );
            List<ProveedoresServicios> lista = criteria.list();
            Date fechaCubi = new Date();
            Date fechaTerminoServicio = new Date();
            Date fechaInicioServicio = new Date();
            Date fechaServi = new Date();

            List<Integer> listaOt = Arrays.asList(172,259,261,262,299,332,344,353,379,427,428,432,433,455,469,473,488,492,494,524,562,563,628,632,657,665,713,714,717,991,1002,1053,1087,1116,1121,1136,1170,1205,1221,1226,1227,1229,1273,1278,1309,1469,1486,1517,1528,1543,1559,1611,1619,1622,1725,1726,1737,1748,1749,1787,1832,1860,2007,2169,2236,2714,2748,3001,3010,3111,3147,3157,3268,3306,3311,3314,3429,3451,3595,3899,4173,4176,4177,4351,4352,4370,4560,4642,4652,4653,4763,4765,4813,5780,6204,6228,6309,6489,6768,
                    304,317,318,341,349,529,621,835,986,1285,1701,1703,1720,2671,3295,3491,3621,4025,4047,4285,4289,4291,4374,4783,4829,4830,4831,6315);

            if(cubicador != null && listaOt.contains(cubicador.getOt().getId())){
                for (ProveedoresServicios ps : lista) {
                    tipoMonedaProxy = new TipoMoneda();
                    if (idTipoServicio == ps.getServicios().getTipoServicio().getId()) {
                        ServiciosAux servicio = new ServiciosAux();
                        char estado = ps.getServicios().getEstado();

                        if(estado == 'A'){
                            servicio.setNombre(ps.getServicios().getNombre());
                            servicio.setDescripcion(ps.getServicios().getDescripcion());
                            servicio.setPrecio(ps.getPrecio());

                            tipoMonedaProxy = (TipoMoneda)retornarObjetoCoreOT2(TipoMoneda.class,ps.getTipoMoneda().getId());
                            TipoMoneda tipoMoneda = new TipoMoneda(tipoMonedaProxy.getId(),tipoMonedaProxy.getNombre(),tipoMonedaProxy.getDescripcion(),tipoMonedaProxy.getEstado());

                            servicio.setTipoMoneda(tipoMoneda);
                            servicio.setId(ps.getServicios().getId());
                            servicio.setIsPackBasico(ps.getServicios().getIsPackBasico());
                            Alcances alcances = new Alcances();

                            Criteria criteria3 = sesion.createCriteria(Alcances.class)
                                    .add(Restrictions.eq("codigo", ps.getServicios().getCodAlcance()));
                            alcances = (Alcances) criteria3.uniqueResult();

                            TipoUnidadMedida tipoUnidadMedida = new TipoUnidadMedida();
                            TipoUnidadMedida tipoUnidadMedidaProx = new TipoUnidadMedida();
                            tipoUnidadMedidaProx = (TipoUnidadMedida) retornarObjetoCoreOT2(TipoUnidadMedida.class,ps.getServicios().getTipoUnidadMedida().getId());
                            tipoUnidadMedida = new TipoUnidadMedida(tipoUnidadMedidaProx.getId(),tipoUnidadMedidaProx.getNombre(),tipoUnidadMedidaProx.getCodigo(),tipoUnidadMedidaProx.getEstado());

                            servicio.setTipoUnidadMedida(tipoUnidadMedida);


                            if(alcances != null){
                                servicio.setAlcances(alcances);
                            }
                            listadoServicios.add(servicio);
                        }

                    }
                }
            }else{
                for (ProveedoresServicios ps : lista) {
                    tipoMonedaProxy = new TipoMoneda();
                    if (idTipoServicio == ps.getServicios().getTipoServicio().getId()) {
                        ServiciosAux servicio = new ServiciosAux();
                        char estado = ps.getServicios().getEstado();
                        boolean validacionFechaCreacion = false;
                        if(cubicador != null) {
                            fechaCubi = cubicador.getFechaCreacion();
                            log.debug("fecha cubicacion : " + fechaCubi);
                            if(ps.getServicios().getFechaTerminoVigencia() != null){
                                fechaTerminoServicio = ps.getServicios().getFechaTerminoVigencia();
                                fechaInicioServicio = cubicador.getFechaCreacion();

                                log.debug("fecha cubicacion : " + fechaCubi);
                                log.debug("fecha termino vigencia servicio : " + fechaTerminoServicio);
                            } else {
                                fechaInicioServicio = ps.getServicios().getFechaInicioVigencia();
                                fechaTerminoServicio = cubicador.getFechaCreacion();

                                log.debug("fecha cubicacion : " + fechaCubi);
                                log.debug("fecha inicio vigencia servicio : " + fechaInicioServicio);
                            }
                            if (cubicador.getFechaCreacion().after(fechaTerminoServicio) || cubicador.getFechaCreacion().before(fechaInicioServicio)) {
                                validacionFechaCreacion = true;

                                fechaServi = ps.getServicios().getFechaTerminoVigencia();

                            }
                        } else if(estado == 'I'){
                            validacionFechaCreacion = true;
                        }
                        log.debug("servicio id : "+ ps.getServicios().getId());
                        log.debug("estado servicio : " + estado);
                        log.debug("fecha servicio : " + fechaServi);
                        log.debug("fecha cubicacion : " + fechaCubi);
                        log.debug("fecha termino vigencia servicio : " + fechaTerminoServicio);
                        log.debug("fecha inicio vigencia servicio : " + fechaInicioServicio);
                        if(!validacionFechaCreacion){
                            servicio.setNombre(ps.getServicios().getNombre());
                            servicio.setDescripcion(ps.getServicios().getDescripcion());
                            servicio.setPrecio(ps.getPrecio());

                            tipoMonedaProxy = (TipoMoneda)retornarObjetoCoreOT2(TipoMoneda.class,ps.getTipoMoneda().getId());
                            TipoMoneda tipoMoneda = new TipoMoneda(tipoMonedaProxy.getId(),tipoMonedaProxy.getNombre(),tipoMonedaProxy.getDescripcion(),tipoMonedaProxy.getEstado());

                            servicio.setTipoMoneda(tipoMoneda);
                            servicio.setId(ps.getServicios().getId());
                            servicio.setIsPackBasico(ps.getServicios().getIsPackBasico());
//                    servicio.setPackBasico(true);
                            Alcances alcances = new Alcances();

                            Criteria criteria3 = sesion.createCriteria(Alcances.class)
                                    .add(Restrictions.eq("codigo", ps.getServicios().getCodAlcance()));
                            alcances = (Alcances) criteria3.uniqueResult();

                            TipoUnidadMedida tipoUnidadMedida = new TipoUnidadMedida();
                            TipoUnidadMedida tipoUnidadMedidaProx = new TipoUnidadMedida();
                            tipoUnidadMedidaProx = (TipoUnidadMedida) retornarObjetoCoreOT2(TipoUnidadMedida.class,ps.getServicios().getTipoUnidadMedida().getId());
                            tipoUnidadMedida = new TipoUnidadMedida(tipoUnidadMedidaProx.getId(),tipoUnidadMedidaProx.getNombre(),tipoUnidadMedidaProx.getCodigo(),tipoUnidadMedidaProx.getEstado());

                            servicio.setTipoUnidadMedida(tipoUnidadMedida);


                            if(alcances != null){
                                servicio.setAlcances(alcances);
                            }
                            listadoServicios.add(servicio);
                        }

                    }
                }
            }


        } catch (HibernateException he) {
            log.error(he);
            throw he;
        }
        return listadoServicios;
    }

//
//    protected List<ServiciosAux> listarServiciosPorProveedorRegionContrato2(int contratoId, int regionId, int proveedorId, int idTipoServicio, Cubicador cubicador) throws Exception {
//
//        List<ServiciosAux> listadoServicios = new ArrayList<>();
//        TipoMoneda tipoMonedaProxy = null;
//
//        try {
//            Criteria criteria = sesion.createCriteria(ProveedoresServicios.class)
//                    .setFetchMode("tipoMoneda", FetchMode.JOIN)
//                    .setFetchMode("servicios", FetchMode.JOIN);
//            criteria.add(
//                    Restrictions.and(
//
//                            Restrictions.eq("proveedores.id", proveedorId),
//                            Restrictions.eq("contratos.id", contratoId),
//                            Restrictions.eq("regiones.id", regionId)
//                    )
//            );
//            List<ProveedoresServicios> lista = criteria.list();
//            Date fechaCubi = new Date();
//            Date fechaTerminoServicio = new Date();
//            Date fechaInicioServicio = new Date();
//            Date fechaServi = new Date();
//            for (ProveedoresServicios ps : lista) {
//                tipoMonedaProxy = new TipoMoneda();
//                if (idTipoServicio == ps.getServicios().getTipoServicio().getId()) {
//                    ServiciosAux servicio = new ServiciosAux();
//                    char estado = ps.getServicios().getEstado();
//                    boolean validacionFechaCreacion = false;
//                    if(cubicador != null) {
//                        fechaCubi = cubicador.getFechaCreacion();
//                        log.debug("fecha cubicacion : " + fechaCubi);
//                        if(ps.getServicios().getFechaTerminoVigencia() != null){
//                            fechaTerminoServicio = ps.getServicios().getFechaTerminoVigencia();
//                            fechaInicioServicio = cubicador.getFechaCreacion();
//
//                            log.debug("fecha cubicacion : " + fechaCubi);
//                            log.debug("fecha termino vigencia servicio : " + fechaTerminoServicio);
//                        } else {
//                            fechaInicioServicio = ps.getServicios().getFechaInicioVigencia();
//                            fechaTerminoServicio = cubicador.getFechaCreacion();
//
//                            log.debug("fecha cubicacion : " + fechaCubi);
//                            log.debug("fecha inicio vigencia servicio : " + fechaInicioServicio);
//                        }
//                        if (cubicador.getFechaCreacion().after(fechaTerminoServicio) || cubicador.getFechaCreacion().before(fechaInicioServicio)) {
//                            validacionFechaCreacion = true;
//
//                            fechaServi = ps.getServicios().getFechaTerminoVigencia();
//
//                        }
//                    } else if(estado == 'I'){
//                        validacionFechaCreacion = true;
//                    }
//                    log.debug("servicio id : "+ ps.getServicios().getId());
//                    log.debug("estado servicio : " + estado);
//                    log.debug("fecha servicio : " + fechaServi);
//                    log.debug("fecha cubicacion : " + fechaCubi);
//                    log.debug("fecha termino vigencia servicio : " + fechaTerminoServicio);
//                    log.debug("fecha inicio vigencia servicio : " + fechaInicioServicio);
//                    if(!validacionFechaCreacion){
//                        servicio.setNombre(ps.getServicios().getNombre());
//                        servicio.setDescripcion(ps.getServicios().getDescripcion());
//                        servicio.setPrecio(ps.getPrecio());
//
//                        tipoMonedaProxy = (TipoMoneda)retornarObjetoCoreOT2(TipoMoneda.class,ps.getTipoMoneda().getId());
//                        TipoMoneda tipoMoneda = new TipoMoneda(tipoMonedaProxy.getId(),tipoMonedaProxy.getNombre(),tipoMonedaProxy.getDescripcion(),tipoMonedaProxy.getEstado());
//
//                        servicio.setTipoMoneda(tipoMoneda);
//                        servicio.setId(ps.getServicios().getId());
//                        servicio.setIsPackBasico(ps.getServicios().getIsPackBasico());
////                    servicio.setPackBasico(true);
//                        Alcances alcances = new Alcances();
//
//                        Criteria criteria3 = sesion.createCriteria(Alcances.class)
//                                .add(Restrictions.eq("codigo", ps.getServicios().getCodAlcance()));
//                        alcances = (Alcances) criteria3.uniqueResult();
//
//                        TipoUnidadMedida tipoUnidadMedida = new TipoUnidadMedida();
//                        TipoUnidadMedida tipoUnidadMedidaProx = new TipoUnidadMedida();
//                        tipoUnidadMedidaProx = (TipoUnidadMedida) retornarObjetoCoreOT2(TipoUnidadMedida.class,ps.getServicios().getTipoUnidadMedida().getId());
//                        tipoUnidadMedida = new TipoUnidadMedida(tipoUnidadMedidaProx.getId(),tipoUnidadMedidaProx.getNombre(),tipoUnidadMedidaProx.getCodigo(),tipoUnidadMedidaProx.getEstado());
//
//                        servicio.setTipoUnidadMedida(tipoUnidadMedida);
//
//
//                        if(alcances != null){
//                            servicio.setAlcances(alcances);
//                        }
//                        listadoServicios.add(servicio);
//                    }
//
//                }
//            }
//
//
//        } catch (HibernateException he) {
//            log.error(he);
//            throw he;
//        }
//        return listadoServicios;
//    }
    protected List<Servicios> listarServiciosPorEspecialidadGateways(int especialidadId, int contratoId, int agenciaId) throws Exception {
        List<ServiciosAux> serviciosAuxList = new ArrayList<>();
        List<Servicios> serviciosList = new ArrayList<>();

        boolean precio = false;

        try {

            // obtengo los servicios segun los filtros
            Criteria criteria = sesion.createCriteria(Servicios.class)
                    .setFetchMode("tipoMoneda", FetchMode.JOIN);
            criteria.add(
                    Restrictions.and(
                            Restrictions.eq("especialidadId", especialidadId)
                            ,Restrictions.eq("contratoId", contratoId)
                    )
            );
            serviciosList = criteria.list();


            if(serviciosList.size() > 0){
                // obtengo el precio segun agencia contrato y especialidad
                Criteria criteria2 = sesion.createCriteria(AgenciasContratosEspecialidades.class)
                        .add(Restrictions.eq("id.especialidadId", especialidadId))
                        .add(Restrictions.eq("id.agenciaId", agenciaId))
                        .add(Restrictions.eq("id.contratoId", contratoId)
                        );

                AgenciasContratosEspecialidades agenciasContratosEspecialidades = (AgenciasContratosEspecialidades)criteria2.uniqueResult();

                if(agenciasContratosEspecialidades != null){
                    // precio
                    double precioPorEspecilidad = agenciasContratosEspecialidades.getId().getPrecio();

                    for (Servicios servicioObtenido: serviciosList){
                        double precioServicio = precioPorEspecilidad * servicioObtenido.getPuntosBaremos();
                        servicioObtenido.setPrecio(precioServicio);
                    }
                    precio = true;

                }else{
                    serviciosList = new ArrayList<>();
                }


            }

        } catch (HibernateException he) {
            log.error(he);
            throw he;
        }
        if(precio){
            return serviciosList;
        }else{
            return serviciosList;
        }


    }

    protected double obtenerPrecioServicioPorEspecialidad(Servicios servicio, int especialidadId, int idContrato, int idAgencia) throws Exception {
        List<ServiciosAux> serviciosAuxList = new ArrayList<>();

        double precio = 0;

        try {
            // obtengo el precio segun agencia contrato y especialidad
            Criteria criteria2 = sesion.createCriteria(AgenciasContratosEspecialidades.class)
                    .add(Restrictions.eq("id.especialidadId", especialidadId))
                    .add(Restrictions.eq("id.agenciaId", idAgencia))
                    .add(Restrictions.eq("id.contratoId", idContrato)
                    );

            AgenciasContratosEspecialidades agenciasContratosEspecialidades = (AgenciasContratosEspecialidades) criteria2.uniqueResult();

            if (agenciasContratosEspecialidades != null) {
                // precio
                double precioPorEspecilidad = agenciasContratosEspecialidades.getId().getPrecio();
                precio = precioPorEspecilidad * servicio.getPuntosBaremos();
            }

        } catch (HibernateException he) {
            log.error(he);
            throw he;
        }

        return precio;

    }


    protected LinkedHashMap listarServicioPorProveedorRegionContratoServicioId(int contratoId, int regionId, int agenciaId, int centralId, int proveedorId, int idTipoServicio, int servicioId, double cantidadServ) throws Exception {
        LinkedHashMap servicioLinkHashMap = new LinkedHashMap();
        List<ServiciosAux> listadoServicios = new ArrayList<>();
        TipoMoneda tipoMonedaProxy = null;

        try {
            Criteria criteria = sesion.createCriteria(ProveedoresServicios.class)
                    .setFetchMode("tipoMoneda", FetchMode.JOIN);
            criteria.add(
                    Restrictions.and(

                            Restrictions.eq("proveedores.id", proveedorId),
                            Restrictions.eq("contratos.id", contratoId),
                            Restrictions.eq("regiones.id", regionId),
                            Restrictions.eq("servicios.id", servicioId)
                    )
            );
            List<ProveedoresServicios> lista = criteria.list();
            for (ProveedoresServicios ps : lista) {
                tipoMonedaProxy = new TipoMoneda();
                if (idTipoServicio == ps.getServicios().getTipoServicio().getId()) {

                    char estado = ps.getServicios().getEstado();

                    if(estado == 'A'){
                        ServiciosAux servicio = new ServiciosAux();
                        servicio.setNombre(ps.getServicios().getNombre());
                        servicio.setDescripcion(ps.getServicios().getDescripcion());
                        servicio.setPrecio(ps.getPrecio());

                        tipoMonedaProxy = (TipoMoneda)retornarObjetoCoreOT2(TipoMoneda.class,ps.getTipoMoneda().getId());
                        TipoMoneda tipoMoneda = new TipoMoneda(tipoMonedaProxy.getId(),tipoMonedaProxy.getNombre(),tipoMonedaProxy.getDescripcion(),tipoMonedaProxy.getEstado());

                        servicio.setTipoMoneda(tipoMoneda);
                        servicio.setId(ps.getServicios().getId());
                        servicio.setIsPackBasico(ps.getServicios().getIsPackBasico());
//                    servicio.setPackBasico(true);
                        Alcances alcances = new Alcances();


                        Criteria criteria3 = sesion.createCriteria(Alcances.class)
                                .add(Restrictions.eq("codigo", ps.getServicios().getCodAlcance()));
                        alcances = (Alcances) criteria3.uniqueResult();
                        if(alcances != null){
                            servicio.setAlcances(alcances);
                        }

                        TipoUnidadMedida tipoUnidadMedida = new TipoUnidadMedida();
                        TipoUnidadMedida tipoUnidadMedidaProx = new TipoUnidadMedida();
                        tipoUnidadMedidaProx = (TipoUnidadMedida) retornarObjetoCoreOT2(TipoUnidadMedida.class,ps.getServicios().getTipoUnidadMedida().getId());
                        tipoUnidadMedida = new TipoUnidadMedida(tipoUnidadMedidaProx.getId(),tipoUnidadMedidaProx.getNombre(),tipoUnidadMedidaProx.getCodigo(),tipoUnidadMedidaProx.getEstado());

                        LinkedHashMap tipuUnidadMedidaMap = new LinkedHashMap();
                        tipuUnidadMedidaMap.put("id", tipoUnidadMedida.getId());
                        tipuUnidadMedidaMap.put("nombre", tipoUnidadMedida.getNombre());
                        tipuUnidadMedidaMap.put("codigo", tipoUnidadMedida.getCodigo());


                        LinkedHashMap servicioMap = new LinkedHashMap();
                        LinkedHashMap servicioMapOInt = new LinkedHashMap();
                        servicioMapOInt.put("id", servicio.getId());
                        servicioMapOInt.put("precio", servicio.getPrecio());

                        LinkedHashMap regionLinkedMap = new LinkedHashMap();
                        regionLinkedMap.put("id", regionId);
                        servicioMapOInt.put("region", regionLinkedMap);

                        LinkedHashMap tipoMonedaLinkedMap = new LinkedHashMap();
                        tipoMonedaLinkedMap.put("id", ps.getTipoMoneda().getId());
                        servicioMapOInt.put("tipoMoneda", tipoMonedaLinkedMap);

                        LinkedHashMap tipoServicioLinkedMap = new LinkedHashMap();
                        tipoServicioLinkedMap.put("id", idTipoServicio);
                        servicioMapOInt.put("tipoServicio", tipoServicioLinkedMap);

                        servicioMap.put("servicio", servicioMapOInt);


                        LinkedHashMap agenciaLinkedMap = new LinkedHashMap();
                        agenciaLinkedMap.put("id", agenciaId);
                        servicioMap.put("agencia", agenciaLinkedMap);

                        LinkedHashMap centralLinkedMap = new LinkedHashMap();
                        centralLinkedMap.put("id", centralId);
                        servicioMap.put("central", centralLinkedMap);


                        int cantidad = (int) cantidadServ;
                        servicioMap.put("cantidad", cantidad);

                        LinkedHashMap cantidadMap = new LinkedHashMap();
                        cantidadMap.put("cantidad", cantidad);


//                        servicioLinkHashMap.add(servicioMap);
                        servicioLinkHashMap.put("id", servicio.getId());
                        servicioLinkHashMap.put("servicio", servicioMapOInt);
                        servicioLinkHashMap.put("region", regionLinkedMap);
                        servicioLinkHashMap.put("tipoUnidadMedida", tipuUnidadMedidaMap);
                        servicioLinkHashMap.put("tipoMoneda", tipoMonedaLinkedMap);
                        servicioLinkHashMap.put("tipoServicio", tipoServicioLinkedMap);
                        servicioLinkHashMap.put("agencia", agenciaLinkedMap);
                        servicioLinkHashMap.put("central", centralLinkedMap);
                        servicioLinkHashMap.put("cantidad", cantidadMap);

                    }

                }
            }


        } catch (HibernateException he) {
            log.error(he);
            throw he;
        }
        return servicioLinkHashMap;
    }

    protected List<ProveedoresServicios> obtieneProveedorServicios(int contratoId, int regionId, int proveedorId, int servicioId) {
        List<ProveedoresServicios> lista = new ArrayList<ProveedoresServicios>();
        try {

            try {
                Criteria criteria = sesion.createCriteria(ProveedoresServicios.class);
                criteria.add(Restrictions.eq("contratos.id", contratoId));
                criteria.add(Restrictions.eq("regiones.id", regionId));
                criteria.add(Restrictions.eq("proveedores.id", proveedorId));
                criteria.add(Restrictions.eq("servicios.id", servicioId));
                lista = (List<ProveedoresServicios>) criteria.list();

            } catch (HibernateException he) {
                log.error(he);
                throw he;
            }

            return lista;
        } catch (HibernateException hExcep) {
            log.error("Error al obtener roles desde la entidad workflowEventosEjecucion");
            throw hExcep;
        }

    }

    protected List<Actas> listadoHistoricoActaPorOt(int otId) throws Exception {

        List<Actas> actas = new ArrayList<Actas>();

        try {
            Criteria criteria = sesion.createCriteria(Actas.class);
            criteria.add(
                    Restrictions.and(
                            Restrictions.eq("ot.id", otId)
                    )
            );
            List<Actas> lista = (List<Actas>) criteria.list();


        } catch (HibernateException he) {
            log.error(he);
            throw he;
        }

        return actas;
    }

    //    protected Integer registrarRetornaId(Proveedores proveedor) throws Exception {
//        int registro = 0;
//        try {
//            sesion.save(proveedor);
//            registro = proveedor.getId();
//        } catch (HibernateException e) {
//            System.out.println("No se pudo crear el objeto " + obj.getClass().toString());
//            System.out.println(e);
//            throw e;
//        }
//        return registro;
//    }
    protected Integer registrarAdjunto(Adjuntos adjunto) throws Exception {
        int registro = 0;
        try {
            sesion.save(adjunto);
            registro = adjunto.getId();
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + adjunto.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return registro;
    }

    protected Integer registrarCartaAdjudicacion(CartaAdjudicacion cartaAdjudicacion) throws Exception {
        int registro = 0;
        try {
            registro = (Integer) sesion.save(cartaAdjudicacion);
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + cartaAdjudicacion.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return registro;
    }

    protected Serializable registrar2(ObjetoCoreOT obj) throws Exception {
        Serializable registro = 0;
        try {
            registro = sesion.save(obj);
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + obj.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return registro;
    }

    protected void registrarCubicadorServiciosBucle(CubicadorServiciosBucle cubicadorServiciosBucle) throws Exception {
        Serializable registro = 0;
        try {

            registro = sesion.save(cubicadorServiciosBucle);

        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + cubicadorServiciosBucle.getClass().toString());
            System.out.println(e);
            throw e;
        }
    }

    protected void registrarCubicadorDireccionesBucle(CubicadorDireccionesBucle direccion) throws Exception {
        Serializable registro = 0;
        try {

            registro = sesion.save(direccion);

        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + direccion.getClass().toString());
            System.out.println(e);
            throw e;
        }
    }


    protected CubicadorServiciosServiciosUnidad registrarCubicadorServiciosServiciosUnidad(CubicadorServiciosServiciosUnidad cubicadorServiciosServiciosUnidad) throws Exception {
        Serializable registro = 0;
        try {
//            CubicadorServiciosServiciosUnidad cubicadorServiciosServiciosUnidadNew = new CubicadorServiciosServiciosUnidad();
//            int cubicadorServiciosServiciosUnidadId = persistirObjeto(cubicadorServiciosServiciosUnidadNew);
//            cubicadorServiciosServiciosUnidad.setId(cubicadorServiciosServiciosUnidadId);
            registro = sesion.save(cubicadorServiciosServiciosUnidad);
            return cubicadorServiciosServiciosUnidad;

        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + cubicadorServiciosServiciosUnidad.getClass().toString());
            System.out.println(e);
            throw e;
        }
    }


    protected boolean registrarCubicadorDetalle(CubicadorDetalle cubicadorDetalle) throws Exception {
        boolean registro = false;
        try {
            sesion.save(cubicadorDetalle);
            sesion.flush();
            registro = true;
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + cubicadorDetalle.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return registro;
    }

    protected Integer registrarConId(ObjetoCoreOT obj, String claseObjeto) throws Exception {
        int registro = 0;
        try {
            sesion.persist(obj);
            switch (claseObjeto) {
                case "Ot":
                    Ot ot = (Ot) obj;
                    registro = ot.getId();
                    break;
                case "LibroObra":
                    LibroObras libroObras = (LibroObras) obj;
                    registro = libroObras.getId();
                    break;
                case "Notificacion":
                    Notificaciones notificacion = (Notificaciones) obj;
                    registro = notificacion.getId();
                    break;
                default:
                    throw new Exception("Error en registrarConId");
            }

        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + obj.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return registro;
    }

    protected Integer persistirObjeto(ObjetoCoreOT obj) throws Exception {
        Integer idRegistro = 0;
        try {
            sesion.persist(obj);
            sesion.flush();
        } catch (HibernateException e) {
            log.error("No se pudo crear el objeto " + obj.getClass().toString(), e);
            throw e;
        }
        return idRegistro;
    }

    protected boolean registrarWee(WorkflowEventosEjecucion obj) throws Exception {
        boolean respuesta = false;
        try {
            sesion.save(obj);
            respuesta = true;
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + obj.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return respuesta;
    }

    protected boolean registrarOtTramite(OtTramites obj) throws Exception {
        boolean respuesta = false;
        try {
            sesion.save(obj);
            respuesta = true;
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + obj.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return respuesta;
    }

    protected boolean actualizarOtTramite(OtTramites obj) throws Exception {
        try {
            sesion.update(obj);
        } catch (HibernateException e) {
            System.out.println("No se pudo actualizar el objeto ");
            throw e;
        }
        return true;
    }

    protected int obtenerTotalTramitesPendientes(int otId) throws Exception {

        boolean valorBool = false;
        int retorno = 0;
        BigInteger total;
        Object respuesta = new Object();
        List<BigInteger> listadoTramitesPendientes = new ArrayList();
        try {
            Query query = sesion.createSQLQuery("SELECT count(ot_tramites.id) FROM ot_tramites WHERE ot_id =:otId AND terminado =:valorBool")
                    .setParameter("otId", otId)
                    .setParameter("valorBool", valorBool);
            listadoTramitesPendientes = query.list();
            respuesta = listadoTramitesPendientes.get(0);
            total = (BigInteger)respuesta;
            retorno = total.intValue();
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }

        return retorno;
    }

    protected List<OtTramites> listarTramitesPorOt(int otId) throws Exception {
        try {

            Query query = sesion.createSQLQuery(
                    "SELECT * "
                    + "FROM ot_tramites otr "
                    + "INNER JOIN tipo_tramite tt ON tt.id = otr.tipo_tramite_id \n"
                    + "INNER JOIN ot orden ON orden.id = otr.ot_id \n"
                    + "WHERE otr.ot_id = :otId"
            )
                    .addEntity(OtTramites.class)
                    .setParameter("otId", otId);

            List<OtTramites> result = query.list();
            return result;
        } catch (HibernateException hExcep) {
            log.error("Error al obtener listado de OtTramites. ", hExcep);
            throw hExcep;
        }
    }

        protected List<OtTramites> listarTramitesPorOt2(int otId) throws Exception {
        List<OtTramites> listado = new ArrayList<>();
        try {
            Criteria criteria = sesion.createCriteria(OtTramites.class)
                    .setFetchMode("ot", FetchMode.JOIN)
                    .add(Restrictions.eq("ot.id", otId))
                    .addOrder(Order.asc("estadoTramite.id"));

            listado = (List<OtTramites>) criteria.list();

            for (OtTramites tramites: listado){

                validarYSetearObjetoARetornar(tramites, null);
            }

            return listado;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<OtTramites> listarTramitesPorOtyEstado(int otId, int estadoTramite) throws Exception {
        List<OtTramites> listado = new ArrayList<>();
        try {
            Criteria criteria = sesion.createCriteria(OtTramites.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .add(Restrictions.eq("estadoTramite.id", estadoTramite))
                    .setFetchMode("estadoTramite", FetchMode.JOIN);

            listado = (List<OtTramites>) criteria.list();

            for (OtTramites tramites: listado){

                validarYSetearObjetoARetornar(tramites, null);
            }
            return listado;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<TipoTramite> listarTipoTramite() throws Exception {
        List<TipoTramite> listado = new ArrayList<>();
        try {
            Criteria criteria = sesion.createCriteria(TipoTramite.class)
                    .setFetchMode("tipo_tramite", FetchMode.JOIN);

            listado = (List<TipoTramite>) criteria.list();
            return listado;
        } catch (Exception e) {
            throw e;
        }
    }

    protected int obtenerTotalTramitesEnProcesoOt(int otId) throws Exception {

        int retorno = 0;
        BigInteger total;
        Object respuesta = new Object();
        List<BigInteger> listadoOtEnProceso = new ArrayList();
        try {
            Query query = sesion.createSQLQuery("SELECT COUNT (1) FROM ot_tramites WHERE ot_id =:otId AND estado_tramite_id in (1,2)")
                    .setParameter("otId", otId);
            listadoOtEnProceso = query.list();
            respuesta = listadoOtEnProceso.get(0);
            total = (BigInteger)respuesta;
            retorno = total.intValue();
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }

        return retorno;
    }

    protected List<ObjetoCoreOT> listarGenerico(Class claseObj, Map<String,Object> filtros, List<String> joinObjCoreOT) throws Exception {
        List<ObjetoCoreOT> lista = new ArrayList();
        try {
            Criteria criteria = sesion.createCriteria(claseObj);
            if (joinObjCoreOT != null) {
                for (String join : joinObjCoreOT) {
                    criteria.setFetchMode(join, FetchMode.JOIN);
                }
            }
            if(filtros!=null){
                for(Map.Entry filtro : filtros.entrySet()){
                    criteria.add(Restrictions.eq((String)filtro.getKey(), filtro.getValue()));
                }
            }
            lista = criteria.list();
            if (lista != null) {
                for (ObjetoCoreOT objeto : lista) {
                    validarYSetearObjetoARetornar(objeto, joinObjCoreOT);
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return lista;
    }

    protected List<ObjetoCoreOT> listar(ObjetoCoreOT obj, List<String> joinObjCoreOT) throws Exception {
        try {
            Criteria criteria = sesion.createCriteria(obj.getClass());
            for (String join : joinObjCoreOT) {
                criteria.setFetchMode(join, FetchMode.JOIN);
            }
            List<ObjetoCoreOT> lista = criteria.list();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    protected List<Usuarios> listarUsuarios(int idProveedor) throws Exception {
        List<Usuarios> listaUsuarios = new ArrayList();
        try {

            Criteria criteria = sesion.createCriteria(Usuarios.class);
            criteria.setFetchMode("proveedor", FetchMode.JOIN);
            criteria.add(Restrictions.eq("proveedor.id", idProveedor));
            listaUsuarios = criteria.list();
        } catch (HibernateException e) {
            throw e;
        }
        return listaUsuarios;
    }

    protected ObjetoCoreOT obtenerDatoPorId(ObjetoCoreOT obj, String[] joinObjCoreOT) throws Exception {
        ObjetoCoreOT objetoRetorno = null;
        Class claseObj = obj.getClass();
        Criteria criteria = sesion.createCriteria(claseObj);
        if (joinObjCoreOT != null) {
            for (String join : joinObjCoreOT) {
                criteria.setFetchMode(join, FetchMode.JOIN);
            }
        }
        Method method = claseObj.getMethod("getId");
        Integer id = (Integer) method.invoke(obj);
        criteria.add(Restrictions.eq("id", id));
        objetoRetorno = (ObjetoCoreOT) criteria.uniqueResult();
        if (objetoRetorno != null) {
            validarYSetearObjetoARetornar(objetoRetorno, null);
        }
        return objetoRetorno;
    }

    protected boolean actualizar(ObjetoCoreOT objetoCoreOT) throws Exception {
        boolean actualizar = false;
        try {
            Class claseObj = objetoCoreOT.getClass();
            Method method = claseObj.getMethod("getId");
            Integer id = (Integer) method.invoke(objetoCoreOT);
            ObjetoCoreOT registroActual = (ObjetoCoreOT) sesion.get(claseObj, id);
            if (registroActual != null) {
                validarYSetearObjetoAPersistir(objetoCoreOT, registroActual);
                sesion.update(registroActual);
                actualizar = true;
            } else {
                throw new Exception("No se encontró el registro a actualizar");
            }
        } catch (Exception e) {
            System.out.println("No se pudo actualizar el objeto " + objetoCoreOT.getClass().toString());
            throw e;
        }
        return actualizar;
    }

    protected ObjetoCoreOT retornarObjetoCoreOT(Class claseObjetoCoreOT, int id) throws Exception {
        try {
            ObjetoCoreOT objetoRetorno = (ObjetoCoreOT) sesion.load(claseObjetoCoreOT, id);
            return objetoRetorno;
        } catch (Exception e) {
            System.out.println("No se pudo cargar el objeto " + claseObjetoCoreOT.toString());
            throw e;
        }
    }

    protected ObjetoCoreOT retornarObjetoCoreOT2(Class claseObjetoCoreOT, int id) throws Exception {
        try {
            ObjetoCoreOT objetoRetorno = (ObjetoCoreOT) sesion.get(claseObjetoCoreOT, id);
            return objetoRetorno;
        } catch (Exception e) {
            System.out.println("No se pudo cargar el objeto " + claseObjetoCoreOT.toString());
            throw e;
        }
    }

    protected boolean actualizar2(ObjetoCoreOT objetoCoreOT) throws Exception {
        try {
            sesion.update(objetoCoreOT);
        } catch (HibernateException e) {
            System.out.println("No se pudo actualizar el objeto " + objetoCoreOT.getClass().toString());
            throw e;
        }
        return true;
    }

    protected boolean actualizarCubicador(Cubicador cubicador) throws Exception {
        try {
            sesion.update(cubicador);
        } catch (HibernateException e) {
            System.out.println("No se pudo actualizar el objeto ");
            throw e;
        }
        return true;
    }

    private void validarYSetearObjetoAPersistir(ObjetoCoreOT objetoEnviado, ObjetoCoreOT objetoBBDD) throws IllegalAccessException {
        String nombreClaseObjeto = objetoBBDD.getClass().getName();
        System.out.println("Clase : " + nombreClaseObjeto);
        Field[] atributos = objetoEnviado.getClass().getDeclaredFields();

        for (Field campo : atributos) {
            String nombreCampo = campo.getName();
            System.out.println("Nombre atributo : " + nombreCampo);
            Class claseCampo = campo.getType();

            if (!Set.class.isAssignableFrom(claseCampo)) {
                campo.setAccessible(true);
                Object valor = campo.get(objetoEnviado);
                campo.set(objetoBBDD, valor);
            }
        }
    }

    protected List<ObjetoCoreOT> listarConFiltro(Class claseObjeto, String filtro, int valorFiltro) throws Exception {
        List<ObjetoCoreOT> lista = null;
        try {
            Criteria criteria = sesion.createCriteria(claseObjeto);
            criteria.setFetchMode(filtro, FetchMode.JOIN);
            criteria.add(Restrictions.eq(filtro, valorFiltro));
            lista = criteria.list();
        } catch (HibernateException e) {
            throw e;
        }
        return lista;
    }

//    protected int obtenerPrimerEventoDelFlujoSegunWorkflow(int idContrato) throws Exception {
//        int idEvento = 0;
//        try {
//            Criteria criteria = sesion.createCriteria(Contratos.class);
//            criteria.add(Restrictions.eq("id", idContrato));
//            Contratos contrato = (Contratos) criteria.uniqueResult();
//
//            Criteria criteria2 = sesion.createCriteria(WorkflowEventos.class);
//            criteria2.add(Restrictions.eq("workflow.id", contrato.getWorkflow().getId()));
//
//            List<WorkflowEventos> listaWorkflowEventos = criteria2.list();
//
//            for(WorkflowEventos we: listaWorkflowEventos){
//
//            }
//
//        } catch (HibernateException e) {
//            throw e;
//        }
//        return idEvento;
//    }

    protected List<EventosAcciones> listarAcciones(String filtro, int valorFiltro) throws Exception {
        List<EventosAcciones> lista = null;
        try {
            Criteria criteria = sesion.createCriteria(EventosAcciones.class);
//            criteria.setFetchMode(filtro, FetchMode.JOIN);
//            criteria.add(Restrictions.eq(filtro, valorFiltro));
            lista = criteria.list();
        } catch (HibernateException e) {
            throw e;
        }
        return lista;
    }

    protected List<Perfiles> listarRolesPorPerfil() throws Exception {
        List<Perfiles> listaRetorno = new ArrayList<>();
        try {
            Criteria criteria = sesion.createCriteria(Roles.class);
            criteria.setProjection(Projections.projectionList()
                    .add(Projections.groupProperty("perfiles"))
            );
            List<Perfiles> lista = criteria.list();
            for (Perfiles p : lista) {
                Perfiles perfil = new Perfiles(p.getId(), p.getNombre(), p.getDescripcion(), p.getEstado(), null);
                perfil.setId(p.getId());
                perfil.setNombre(p.getNombre());
                perfil.setEstado(p.getEstado());
                perfil.setDescripcion(p.getDescripcion());
                Set<Roles> roles = new HashSet<>(p.getRoleses());
                for (Roles r : roles) {
                    r.setPerfil(null);
                    r.setWorkflowEventosRoleses(null);
                }
                perfil.setRoleses(roles);
                listaRetorno.add(perfil);
            }
        } catch (Exception e) {
            throw e;
        }
        return listaRetorno;
    }

    protected void validarYSetearObjetoARetornar(ObjetoCoreOT objCoreOT, List<String> camposJoin) throws java.lang.InstantiationException {
        String nombreClaseObjeto = objCoreOT.getClass().getName();
        log.debug("Clase : " + nombreClaseObjeto);
        Field[] atributos = objCoreOT.getClass().getDeclaredFields();

        try {
            for (Field campo : atributos) {
                log.debug("Nombre atributo : " + campo.getName());
                Class claseCampo = campo.getType();
                campo.setAccessible(true);
                boolean isSet = Set.class.isAssignableFrom(claseCampo);
                boolean isJoinField = camposJoin != null ? camposJoin.contains(campo.getName()) : false;
                if (isSet && !isJoinField) {
                    campo.set(objCoreOT, null);
                } else if (ObjetoCoreOT.class.isAssignableFrom(claseCampo) && campo != null) {
                    ObjetoCoreOT attrObjCoreOT = (ObjetoCoreOT) campo.get(objCoreOT);
                    if (attrObjCoreOT != null) {
                        ObjetoCoreOT nuevoAttrObjCoreOT = setearYRetornarAtributoObjCoreOT(attrObjCoreOT, claseCampo);
                        campo.set(objCoreOT, nuevoAttrObjCoreOT);
                    }
                } else if (isSet && isJoinField) {
                    Set setObjetos = (Set) campo.get(objCoreOT);
                    Set setAux = new HashSet();
                    for (Object objSet : setObjetos) {
                        int cont = 0;
                        log.debug("**** Seteando coleccion del objeto ****");
                        log.debug("**** Seteando campo JOIN ****" + " " + cont++);
                        ObjetoCoreOT attrObjCoreOT = (ObjetoCoreOT) objSet;
                        validarYSetearObjetoARetornar(attrObjCoreOT, null);
                        setAux.add(attrObjCoreOT);
                    }
                    campo.set(objCoreOT, setAux);
                }
            }
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | InstantiationException ex) {
            log.error(ex);
        }
    }

    private ObjetoCoreOT setearYRetornarAtributoObjCoreOT(ObjetoCoreOT objCoreOT, Class claseTipoObjeto) throws NoSuchMethodException, InvocationTargetException, SecurityException, IllegalArgumentException, IllegalAccessException, InstantiationException, java.lang.InstantiationException {
        String nombreClaseObjeto = claseTipoObjeto.toString();

        log.debug("Clase : " + nombreClaseObjeto);
        Field[] atributos = claseTipoObjeto.getDeclaredFields();

        Map<String, Method> mapaGets = retornarMapaGet(claseTipoObjeto.getMethods(), atributos);
        try {
            Constructor constructor = claseTipoObjeto.getConstructor();
            ObjetoCoreOT objetoRetorno = (ObjetoCoreOT) constructor.newInstance();
            for (Field campo : atributos) {
                log.debug("Nombre atributo : " + campo.getName());
                Class claseCampo = campo.getType();
                Method metodo = mapaGets.get(campo.getName());
                campo.setAccessible(true);
                if (Set.class.isAssignableFrom(claseCampo) || ObjetoCoreOT.class.isAssignableFrom(claseCampo)) {
                    campo.set(objetoRetorno, null);
                } else {
                    Object objetoAux = metodo.invoke(objCoreOT);
                    campo.set(objetoRetorno, objetoAux);
                }
            }
            return objetoRetorno;
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | InstantiationException ex) {
            log.error("Error al setear nuevo atributo de objeto" + ex);
            throw ex;
        }
    }

    private Map<String, Method> retornarMapaGet(Method[] metodos, Field[] campos) {
        Map<String, Method> mapaRetorno = new HashMap<>();
        for (Method metodo : metodos) {
            String nombreMetodo = metodo.getName();
            if (isGetter(metodo)) {
                for (Field obj : campos) {
                    int largoEsperadoCampoGet = "get".length() + obj.getName().length();
                    boolean contieneCampo = nombreMetodo.toLowerCase().contains(obj.getName().toLowerCase());
                    if (contieneCampo && (nombreMetodo.length() == largoEsperadoCampoGet)) {
                        mapaRetorno.put(obj.getName(), metodo);
                        break;
                    }
                }
            }
        }
        return mapaRetorno;
    }

    private static boolean isGetter(Method method) {
        if (!method.getName().startsWith("get")) {
            return false;
        }
        if (method.getParameterTypes().length != 0) {
            return false;
        }
        if (void.class.equals(method.getReturnType())) {
            return false;
        }
        return true;
    }

    protected Usuarios obtenerUsuarioPorRut(int rut, char dv) throws Exception {
        try {
            Criteria criteria = sesion.createCriteria(Usuarios.class)
                    .add(Restrictions.eq("rut", rut))
                    .add(Restrictions.eq("dv", dv));
            Usuarios usr = (Usuarios) criteria.uniqueResult();
            if (usr == null) {
                throw new Exception("Usuario NO encontrado");
            }
            return usr;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected Usuarios obtenerUsuarioPorId(int usuarioId) throws Exception {
        try {
            Criteria criteria = sesion.createCriteria(Usuarios.class)
                    .add(Restrictions.eq("id", usuarioId));
            Usuarios usr = (Usuarios) criteria.uniqueResult();
            if (usr == null) {
                throw new Exception("Usuario NO encontrado");
            }
            return usr;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }


    protected List<Usuarios> obtenerUsuariosPorRolContrato(int rolId, int contratoId) throws Exception {
        List<Usuarios> usuarios = new ArrayList<>();
        try {
            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from usuarios WHERE id in (SELECT usuario_id from usuarios_roles WHERE rol_id = :rol) AND id in (SELECT usuario_id from usuarios_contratos WHERE contrato_id = :contrato)");
            sqlQuery.setParameter("rol", rolId);
            sqlQuery.setParameter("contrato", contratoId);
            List<Object[]> usuariosAux = sqlQuery.list();
            for (Object[] obj : usuariosAux) {
                Usuarios usr = new Usuarios();
                int idAux = (Integer) obj[0];
                String nombreAux = (String) obj[8];
                String apellidoAux = (String) obj[9];
                usr.setId(idAux);
                usr.setNombres(nombreAux);
                usr.setApellidos(apellidoAux);
                usuarios.add(usr);
            }
            return usuarios;
        } catch (Exception e) {
            System.out.println("No se pudo obtener listado de usuarios." + e);
            throw e;
        }
    }

    protected List<Usuarios> obtenerOrganigrama(int rolId, int usuarioId, int contratoId) throws Exception {
        List<Usuarios> usuarios = new ArrayList<>();
        try {
//            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from usuarios WHERE\n" +
//                    "    id in (SELECT sub_gerente_id  from organigrama WHERE gestor_id = :gestor and contrato_id = :contrato and sub_gerente_id in\n" +
//                    "      (SELECT usuario_id from usuarios_roles WHERE rol_id = :rol))\n" +
//                    "  OR\n" +
//                    "    id in (SELECT jefe_de_area_id  from organigrama WHERE gestor_id = :gestor and contrato_id = :contrato and jefe_de_area_id in\n" +
//                    "      (SELECT usuario_id from usuarios_roles WHERE rol_id = :rol))");

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from usuarios WHERE\n" +
                    "    id in (SELECT sub_gerente_id  from organigrama WHERE gestor_id = :gestor and contrato_id = :contrato and sub_gerente_id in\n" +
                    "      (SELECT usuario_id from usuarios_roles WHERE rol_id = :rol))\n" +
                    "  OR\n" +
                    "    id in (SELECT jefe_de_area_id  from organigrama WHERE gestor_id = :gestor and contrato_id = :contrato and jefe_de_area_id in\n" +
                    "      (SELECT usuario_id from usuarios_roles WHERE rol_id = :rol))\n" +
                    "  OR\n" +
                    "    id in (SELECT pmo_id  from organigrama WHERE gestor_id = :gestor and contrato_id = :contrato and pmo_id in\n" +
                    "      (SELECT usuario_id from usuarios_roles WHERE rol_id = :rol))\n" +
                    "  OR\n" +
                    "    id in (SELECT imputacion_id  from organigrama WHERE gestor_id = :gestor and contrato_id = :contrato and imputacion_id in\n" +
                    "       (SELECT usuario_id from usuarios_roles WHERE rol_id = :rol))\n" +
                    "  OR\n" +
                    "    id in (SELECT pagos_id  from organigrama WHERE gestor_id = :gestor and contrato_id = :contrato and pagos_id in\n" +
                    "       (SELECT usuario_id from usuarios_roles WHERE rol_id = :rol))");

            sqlQuery.setParameter("contrato", contratoId);
            sqlQuery.setParameter("gestor", usuarioId);
            sqlQuery.setParameter("rol", rolId);

            List<Object[]> usuariosAux = sqlQuery.list();
            for (Object[] obj : usuariosAux) {
                Usuarios usr = new Usuarios();
                int idAux = (Integer) obj[0];
                String nombreAux = (String) obj[8];
                String apellidoAux = (String) obj[9];
                usr.setId(idAux);
                usr.setNombres(nombreAux);
                usr.setApellidos(apellidoAux);
                usuarios.add(usr);
            }
            return usuarios;
        } catch (Exception e) {
            System.out.println("No se pudo obtener listado de usuarios." + e);
            return usuarios;
        }
    }

    protected List<Usuarios> obtenerOrganigramaSinRol(int usuarioId, int contratoId) throws Exception {
        List<Usuarios> usuarios = new ArrayList<>();
        List<Usuarios> usuariosFinal = new ArrayList<>();
        try {
            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from usuarios WHERE\n" +
                    "id in (SELECT sub_gerente_id  from organigrama WHERE gestor_id = :gestor and contrato_id = :contrato)\n" +
                    "OR\n" +
                    "id in (SELECT jefe_de_area_id  from organigrama WHERE gestor_id = :gestor and contrato_id = :contrato)");

            sqlQuery.setParameter("contrato",contratoId);
            sqlQuery.setParameter("gestor",usuarioId);

            List<Object[]> usuariosAux = sqlQuery.list();
            for(Object[] obj: usuariosAux){
                Usuarios usr = new Usuarios();
                int idAux = (Integer) obj[0];
                String nombreAux = (String) obj[8];
                String apellidoAux = (String) obj[9];
                usr.setId(idAux);
                usr.setNombres(nombreAux);
                usr.setApellidos(apellidoAux);
                // voy a usar el rut para guardar el rol O
                SQLQuery sqlQuery2 = sesion.createSQLQuery("SELECT rol_id FROM usuarios_roles WHERE usuario_id = :idAux2");
                sqlQuery2.setParameter("idAux2",idAux);
                List<Integer> rolAux = sqlQuery2.list();
                int rolIUsuario = 0;
                for(Integer obj2: rolAux){
                    rolIUsuario = obj2;
                }
                usr.setRut(rolIUsuario);
                usuarios.add(usr);
            }

            for (Usuarios usuariosDesordenados: usuarios){
                if(usuariosDesordenados.getRut() == 10){
                    usuariosFinal.add(usuariosDesordenados);
                }
            }

            for (Usuarios usuariosDesordenados: usuarios){
                if(usuariosDesordenados.getRut() == 5 || usuariosDesordenados.getRut() == 14){
                    usuariosFinal.add(usuariosDesordenados);
                }
            }

            return usuariosFinal;
        } catch (Exception e) {
            System.out.println("No se pudo obtener listado de usuarios." + e);
            throw e;
        }
    }



    protected List<Usuarios> obtenerUsuariosPorRolProovedor(int rolId, int proveedorId, int contratoId) throws Exception {
        List<Usuarios> usuarios = new ArrayList<>();
        try {

//            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from usuarios WHERE id in (SELECT usuario_id from usuarios_roles WHERE rol_id = :rol) AND usuarios.proveedor_id = :proveedor");
            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from usuarios WHERE id in (SELECT usuario_id from usuarios_roles WHERE rol_id = :rol) AND  id in (SELECT usuario_id from usuarios_contratos WHERE contrato_id = :contrato) AND usuarios.proveedor_id = :proveedor");

            sqlQuery.setParameter("rol",rolId);
            sqlQuery.setParameter("proveedor",proveedorId);
            sqlQuery.setParameter("contrato",contratoId);
            List<Object[]> usuariosAux = sqlQuery.list();
            for(Object[] obj: usuariosAux){
                Usuarios usr = new Usuarios();
                int idAux = (Integer) obj[0];
                String nombreAux = (String) obj[8];
                String apellidoAux = (String) obj[9];
                usr.setId(idAux);
                usr.setNombres(nombreAux);
                usr.setApellidos(apellidoAux);
                usuarios.add(usr);
            }
            return usuarios;
        } catch (Exception e) {
            System.out.println("No se pudo obtener listado de usuarios." + e);
            throw e;
        }
    }


    protected List<Usuarios> obtenerUsuariosOrganigramaEECC(int usuarioId, int proveedorId, int contratoId) throws Exception {
        List<Usuarios> usuarios = new ArrayList<>();
        try {

            Criteria criteria = sesion.createCriteria(OrganigramaEECC.class)
                    .add(Restrictions.eq("id.contratoId", contratoId))
                    .add(Restrictions.eq("id.proveedorId", proveedorId))
                    .add(Restrictions.eq("id.gestorId", usuarioId));

            List<OrganigramaEECC> listadoOrganigramaEECC = (List<OrganigramaEECC>)criteria.list();

            for(OrganigramaEECC organigramaEECC: listadoOrganigramaEECC){

                Criteria criteriaUsr = sesion.createCriteria(Usuarios.class)
                        .add(Restrictions.eq("id", organigramaEECC.getId().getAdministradorId()));

                Usuarios administradorContrato = (Usuarios) criteriaUsr.uniqueResult();
                usuarios.add(administradorContrato);

            }

            return usuarios;
        } catch (Exception e) {
            System.out.println("No se pudo obtener listado de usuarios." + e);
            throw e;
        }
    }



    protected List<GestionEconomica> listarPep2PorLp(int idPmo, int lp, int contratoId) throws Exception {
        List<GestionEconomica> listado = new ArrayList<>();

        Criteria criteria1 = sesion.createCriteria(GestionEconomica.class);
        criteria1.add(Restrictions.eq("id.lp", String.valueOf(lp)));
        criteria1.add(Restrictions.eq("id.idPmo", String.valueOf(idPmo)));

//        .setProjection(Projections.distinct(Projections.property("id.pep2")));
        listado = (List<GestionEconomica>) criteria1.list();

        try {
//            switch (contratoId){
//
//                case 1: // SBE
//                    Criteria criteria1 = sesion.createCriteria(LineasPresupuestarias.class)
//                            .add(Restrictions.eq("id", lp));
//                    listado = (List<LineasPresupuestarias>) criteria1.list();
//                    break;
//
//                case 2: // RAN
//                    Criteria criteria2 = sesion.createCriteria(LineasPresupuestarias.class)
//                            .add(Restrictions.eq("id", lp));
//                    listado = (List<LineasPresupuestarias>) criteria2.list();
//                    break;
//
//                case 4: // Unificado
//                    Criteria criteria4 = sesion.createCriteria(LineasPresupuestarias.class)
//                            .add(Restrictions.eq("id", lp));
//                    listado = (List<LineasPresupuestarias>) criteria4.list();
//                    break;
//
//                case 5: // LLave en Mano
//                    Criteria criteria5 = sesion.createCriteria(LineasPresupuestarias.class)
//                            .add(Restrictions.eq("id", lp));
//                    listado = (List<LineasPresupuestarias>) criteria5.list();
//                    break;
//            }
            return listado;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<Usuarios> obtenerGestoresConstruccion(int proveedorId) throws Exception {
        List<Usuarios> listado = new ArrayList<>();
        try {

            // cuando se huawei
            if(proveedorId == 6){
                Criteria criteria2 = sesion.createCriteria(Usuarios.class)
                        .add(Restrictions.eq("nombreUsuario", "luisjp"));
                Usuarios user2 = (Usuarios) criteria2.list().get(0);
                listado.add(user2);
            }else{
                Criteria criteria1 = sesion.createCriteria(Usuarios.class)
                        .add(Restrictions.eq("nombreUsuario", "javierlh"));
                Usuarios user1 = (Usuarios) criteria1.list().get(0);
                listado.add(user1);
            }

            return listado;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<GestionEconomica> listarLpIdPmo(int pmoId, int contratoId) throws Exception {

        List<GestionEconomica> listado = new ArrayList<>();
        List listado2 = new ArrayList<>();
        Criteria criteria1 = sesion.createCriteria(GestionEconomica.class)
            .add(Restrictions.eq("id.idPmo", String.valueOf(pmoId)))
            .setProjection(Projections.distinct(Projections.property("id.lp")));
//        listado = (List<GestionEconomica>) criteria1.list();
        listado2 = (List)criteria1.list();

        for (int i = 0; i<listado2.size(); i++){

            GestionEconomica gestionEconomicaAux = new GestionEconomica();
            GestionEconomicaId gestionEconomicaId = new GestionEconomicaId();
            String lp = listado2.get(i).toString();
            gestionEconomicaId.setLp(lp);
            gestionEconomicaAux.setId(gestionEconomicaId);
            listado.add(gestionEconomicaAux);
        }

        try {
//            switch (contratoId){
//
//                case 1: // SBE
//                    Criteria criteria1 = sesion.createCriteria(GestionEconomica.class)
//                            .add(Restrictions.eq("idPmo", pmoId));
//                    listado = (List<LineasPresupuestarias>) criteria1.list();
//                    break;
//
//                case 2: // RAN
//                    Criteria criteria2 = sesion.createCriteria(GestionEconomica.class)
//                            .add(Restrictions.eq("idPmoNuevo", pmoId))
//                            .add(Restrictions.eq("subgerente", "Reinaldo Pérez"));
//                    listado = (List<LineasPresupuestarias>) criteria2.list();
//                    break;
//
//                case 4: // Unificado
//                    Criteria criteria4 = sesion.createCriteria(LineasPresupuestarias.class)
//                            .add(Restrictions.eq("idPmoNuevo", pmoId))
//                            .add(Restrictions.eq("subgerente", "Reinaldo Pérez"));
//                    listado = (List<LineasPresupuestarias>) criteria4.list();
//                    break;
//
//                case 5: // LLave en Mano
//                    Criteria criteria5 = sesion.createCriteria(LineasPresupuestarias.class)
//                            .add(Restrictions.eq("idPmoNuevo", pmoId))
//                            .add(Restrictions.eq("gerencia", "LLAVE"));
//                    listado = (List<LineasPresupuestarias>) criteria5.list();
//                    break;
//            }

            return listado;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<Contratos> listarContratosPorUsuario(int usuarioId) throws Exception {
        List<Contratos> listado = new ArrayList<>();
        try {

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from contratos WHERE id IN (SELECT contrato_id from usuarios_contratos WHERE usuario_id =:usuarioId)");
            sqlQuery.setParameter("usuarioId",usuarioId);
            List<Object[]> contratosAux = sqlQuery.list();
            for(Object[] obj: contratosAux){
                Contratos contrato = new Contratos();
                int idAux = (Integer) obj[0];
                int workflowIdAux = (Integer) obj[1];
                int tipoPlantaIdAux = (Integer) obj[2];
                String nombreAux = (String) obj[3];
                contrato.setId(idAux);
                contrato.setNombre(nombreAux);
                listado.add(contrato);
            }
            return listado;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<TipoServicio> listarTipoServiciosPorContrato(int contratoId) throws Exception {
        List<TipoServicio> listado = new ArrayList<>();
        List<TipoServicio> respuesta = new ArrayList<>();
        try {

            Criteria criteria = sesion.createCriteria(TipoServicio.class)
                    .add(Restrictions.eq("contrato.id", contratoId));
            listado = (List<TipoServicio>) criteria.list();

            for(TipoServicio tipo: listado){
                Contratos contrato = new Contratos();
                contrato.setId(tipo.getContrato().getId());
                tipo.setContrato(contrato);
                respuesta.add(tipo);
            }

            return respuesta;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<Agencias> listarAgenciasPorRegion(int regionId) throws Exception {
        List<Agencias> listado = new ArrayList<>();
        try {
            char estado = 'A';
            Criteria criteria = sesion.createCriteria(Agencias.class)
                    .add(Restrictions.eq("regiones.id", regionId))
                    .add(Restrictions.eq("estado", estado));
            listado = (List<Agencias>) criteria.list();

            return listado;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<Centrales> listarCentralesPorAgencia(int agenciaId) throws Exception {
        List<Centrales> listado = new ArrayList<>();
        try {

            Criteria criteria = sesion.createCriteria(Centrales.class)
                    .add(Restrictions.eq("agencias.id", agenciaId));
            listado = (List<Centrales>) criteria.list();

            return listado;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<Materiales> listarMaterialesPorFamilia(int familiaId) throws Exception {
        List<Materiales> listado = new ArrayList<>();
        try {

            Criteria criteria = sesion.createCriteria(Materiales.class)
                    .add(Restrictions.eq("familia.id", familiaId));
            listado = (List<Materiales>) criteria.list();

            return listado;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<Familias> listarFamiliasMateriales() throws Exception {
        List<Familias> listado = new ArrayList<>();
        try {
            Criteria criteria = sesion.createCriteria(Familias.class);
            listado = (List<Familias>) criteria.list();
            return listado;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<Usuarios> listarUsuarios() throws Exception {
        try {
            String hql = "SELECT NEW Usuarios(id, nombreUsuario, rut, dv, nombres, apellidos, email, celular, estado) "
                    + "FROM Usuarios";
            Query query = sesion.createQuery(hql);
            List<Usuarios> listaRetorno = query.list();
            return listaRetorno;
        } catch (Exception e) {
            System.out.println("No se pudo obtener listado de usuarios." + e);
            throw e;
        }
    }
    protected Map obtienegerenciaOrganigramaGateways(int usuarioId) {
        Map dependenciaMapa = new HashMap();
        try {
            Organigrama organigrama = (Organigrama) sesion.createCriteria(Organigrama.class)
                    .add(Restrictions.eq("id.gestorId", usuarioId))
                    .uniqueResult();
            if(organigrama != null){
                Usuarios gerente = (Usuarios) sesion.createCriteria(Usuarios.class)
                        .add(Restrictions.eq("id", organigrama.getId().getGerenteId()))
                        .uniqueResult();
                String[] nombreApellido = gerente.getNombres().split(" ");
                Map gerenteMap = new HashMap();
                gerenteMap.put("nombre", nombreApellido[0].toString());
                nombreApellido = gerente.getApellidos().split(" ");
                gerenteMap.put("apellido", nombreApellido[0].toString());
                dependenciaMapa.put("gerente", gerenteMap);

                Usuarios subGerente = (Usuarios) sesion.createCriteria(Usuarios.class)
                        .add(Restrictions.eq("id", organigrama.getId().getSubGerenteId()))
                        .uniqueResult();
                nombreApellido = subGerente.getNombres().split(" ");
                Map subGerenteMap = new HashMap();
                subGerenteMap.put("nombre", nombreApellido[0].toString());
                nombreApellido = subGerente.getApellidos().split(" ");
                subGerenteMap.put("apellido", nombreApellido[0].toString());
                dependenciaMapa.put("subGerente", subGerenteMap);
            }

            return dependenciaMapa;
        } catch (HibernateException hibEx) {
            log.error("Error al validar credenciales de usuario. ", hibEx);
            throw hibEx;
        }
    }
    protected List<Integer> listarIdPmo(int contratoId, int  usuarioId) throws Exception {

        List listaRetorno2 = new ArrayList<>();
        List<Integer> listaRetorno = new ArrayList<>();

        try {

            if(contratoId == 5){

                Criteria criteria1 = sesion.createCriteria(GestionEconomica.class)
                .add(Restrictions.eq("id.contratoId", contratoId))
                .setProjection(Projections.distinct(Projections.property("id.idPmo")));
                listaRetorno2 = (List) criteria1.list();
                for (int i = 0; i<listaRetorno2.size(); i++){
                    String idPmo = listaRetorno2.get(i).toString();
                    listaRetorno.add(Integer.parseInt(idPmo));
                }
            }else{

                Criteria criteria1 = sesion.createCriteria(GestionEconomica.class);
                Disjunction or = Restrictions.disjunction();
                or.add(Restrictions.eq("id.contratoId", contratoId));
                or.add(Restrictions.eq("id.contratoId", 0));

                criteria1.setProjection(Projections.distinct(Projections.property("id.idPmo")))
                        .addOrder(Order.asc("id.idPmo"));
                listaRetorno2 = (List) criteria1.list();
                for (int i = 0; i<listaRetorno2.size(); i++){
                    String idPmo = listaRetorno2.get(i).toString();
                    listaRetorno.add(Integer.parseInt(idPmo));
                }
            }

        } catch (Exception e) {
            System.out.println("No se pudo obtener listado de usuarios." + e);
            throw e;
        }

        return listaRetorno;
    }





//    protected List<Eventos> listarEventos() {
//        try {
//            Criteria criteria = sesion.createCriteria(Eventos.class);
//            List<Eventos> lista = criteria.list();
//            for (Eventos evento : lista) {
//                MensajeEvento msjeEvento = new MensajeEvento();
//                msjeEvento.setId(evento.getMensajeEvento().getId());
//                evento.setMensajeEvento(msjeEvento);
//                TipoEvento tipoEvento = new TipoEvento();
//                tipoEvento.setId(evento.getTipoEvento().getId());
//                evento.setTipoEvento(tipoEvento);
//            }
//            return lista;
//        } catch (Exception e) {
//            System.out.println(e);
//            throw e;
//        }
//    }
//    protected List<Privilegios> listarOpciones() throws Exception {
//        List<Privilegios> listaRetorno = null;
//        try {
//            Criteria crit = sesion.createCriteria(Privilegios.class);
//            listaRetorno = crit.list();
//            for (Privilegios obj : listaRetorno) {
//                validarYSetearObjetoARetornar(obj, null);
//            }
//        } catch (HibernateException e) {
//            System.out.println(e);
//            throw e;
//        }
//        return listaRetorno;
//    }
    protected int validarUsuarioYRetornarId(String usuario, String clave) {
        try {
            Usuarios usr = (Usuarios) sesion.createCriteria(Usuarios.class)
                    .add(Restrictions.eq("nombreUsuario", usuario))
                    .add(Restrictions.eq("estado", 'A'))
                    .uniqueResult();
            return usr.getId();
        } catch (HibernateException hibEx) {
            log.error("Error al validar credenciales de usuario. ", hibEx);
            throw hibEx;
        }
    }

    protected List<WorkflowEventosEjecucion> retornarWrkFlwEvEjecPorUsuario(int idUsuario) {
        try {
            List<WorkflowEventosEjecucion> listado = (List<WorkflowEventosEjecucion>) sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .setFetchMode("ot", FetchMode.JOIN)
                    .add(Restrictions.eq("usuarioId", idUsuario))
                    .add(Restrictions.eq("ejecutado", false))
                    .list();
            return listado;
        } catch (HibernateException hibEx) {
            log.error("Error al obtener pendientes de usuario. ", hibEx);
            throw hibEx;
        }
    }

    protected List<UsuariosOt> obtieneUsuariosOtEventos(int otId, int eventoId) {

        try {
            List<UsuariosOt> lista = new ArrayList<UsuariosOt>();
            lista = (List<UsuariosOt>) sesion.createCriteria(UsuariosOt.class)
                    .setFetchMode("ot", FetchMode.JOIN)
                    .add(Restrictions.eq("eventos.id", eventoId))
                    .add(Restrictions.eq("ot.id", otId))
//                    .add(Restrictions.eq("usuarios.id", usuarioId))
                    .list();

            return lista;

        } catch (HibernateException hibEx) {
            log.error("Error al obtener lista de usuarios ot. ", hibEx);
            throw hibEx;
        }
    }
    protected List<UsuariosOt> obtieneUsuariosOt(int otId) {

        try {
            List<UsuariosOt> lista = new ArrayList<UsuariosOt>();
            lista = (List<UsuariosOt>) sesion.createCriteria(UsuariosOt.class)
                    .setFetchMode("ot", FetchMode.JOIN)
                    .add(Restrictions.eq("ot.id", otId))
//                    .add(Restrictions.eq("usuarios.id", usuarioId))
                    .list();

            return lista;

        } catch (HibernateException hibEx) {
            log.error("Error al obtener lista de usuarios ot. ", hibEx);
            throw hibEx;
        }
    }

    protected List<Usuarios> obtieneUsuariosOtRol(int otId, int rolId) {

        try {
            List<Integer> listaIds = new ArrayList<Integer>();
            List<Usuarios> listaUsuarios = new ArrayList<Usuarios>();

            Query query = sesion.createSQLQuery("SELECT distinct(usuario_id) FROM usuarios_ot WHERE ot_id = :otId and rol_id = :rolId")
                    .setParameter("otId", otId)
                    .setParameter("rolId", rolId);
            listaIds = query.list();

            for(Integer usuarioId: listaIds){
                listaUsuarios.add((Usuarios) sesion.createCriteria(Usuarios.class)
                        .add(Restrictions.eq("id", usuarioId))
                        .uniqueResult());
            }


            return listaUsuarios;

        } catch (HibernateException hibEx) {
            log.error("Error al obtener lista de usuarios ot rol. ", hibEx);
            throw hibEx;
        }
    }

    protected List<Usuarios> obtieneUsuariosOtRolSinEmisor(int otId, int rolId, int idEmisor) {

        try {
            List<Integer> listaIds = new ArrayList<Integer>();
            List<Usuarios> listaUsuarios = new ArrayList<Usuarios>();

            Query query = sesion.createSQLQuery("SELECT distinct(usuario_id) FROM usuarios_ot WHERE ot_id = :otId and rol_id = :rolId")
                    .setParameter("otId", otId)
                    .setParameter("rolId", rolId);
            listaIds = query.list();

            for(Integer usuarioId: listaIds){
                Usuarios usuario = (Usuarios) sesion.createCriteria(Usuarios.class)
                        .add(Restrictions.eq("id", usuarioId))
                        .add(Restrictions.not(Restrictions.eq("id", idEmisor)))
                        .uniqueResult();
                if(usuario!=null)
                    listaUsuarios.add(usuario);
            }


            return listaUsuarios;

        } catch (HibernateException hibEx) {
            log.error("Error al obtener lista de usuarios ot rol. ", hibEx);
            throw hibEx;
        }
    }
    protected List<WorkflowEventosEjecucion> obtieneUsuariosOtEventosWE(int otId, int eventoId) {

        try {
            List<WorkflowEventosEjecucion> lista = new ArrayList<WorkflowEventosEjecucion>();
            lista = (List<WorkflowEventosEjecucion>) sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .add(Restrictions.eq("id.eventoId", eventoId))
//                    .add(Restrictions.eq("usuarios.id", usuarioId))
                    .list();

            return lista;

        } catch (HibernateException hibEx) {
            log.error("Error al obtener lista de usuarios ot. ", hibEx);
            throw hibEx;
        }
    }
    protected List<WorkflowEventosEjecucion> obtieneUsuariosOtIdEventosWE(int otId) {

        try {
            List<WorkflowEventosEjecucion> lista = new ArrayList<WorkflowEventosEjecucion>();
            lista = (List<WorkflowEventosEjecucion>) sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .add(Restrictions.eq("ot.id", otId))
//                    .add(Restrictions.eq("usuarios.id", usuarioId))
                    .list();

            return lista;

        } catch (HibernateException hibEx) {
            log.error("Error al obtener lista de usuarios ot. ", hibEx);
            throw hibEx;
        }
    }

    protected List<Decisiones> obtenerDecisionesPorEventoId(int eventoId, int otId) {

        List<Decisiones> listadoDecisiones = new ArrayList<>();
        try {
            List<Decisiones> listadoDecisionesWee = new ArrayList<>();
            List<WorkflowEventosEjecucion> listado = (List<WorkflowEventosEjecucion>) sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .setFetchMode("ot", FetchMode.JOIN)
                    .add(Restrictions.eq("ot.id", otId))
                    .add(Restrictions.eq("id.eventoId", eventoId))
                    .list();
            WorkflowEventosEjecucion wee = listado.get(0);
            if (wee.getId().getDecisionId() == 1) {
                List<WorkflowEventosEjecucion> listado2 = (List<WorkflowEventosEjecucion>) sesion.createCriteria(WorkflowEventosEjecucion.class)
                        .setFetchMode("ot", FetchMode.JOIN)
                        .add(Restrictions.eq("ot.id", otId))
                        .add(Restrictions.eq("eventoPadre", eventoId))
                        .list();
                for (WorkflowEventosEjecucion weeAux : listado2) {
                    Decisiones decision = new Decisiones();
                    decision.setId(weeAux.getId().getDecisionId());
                    listadoDecisionesWee.add(decision);
                }
            }

            for (Decisiones decisiones : listadoDecisionesWee) {
                List<Decisiones> listadoDecisionesAux = (List<Decisiones>) sesion.createCriteria(Decisiones.class)
                        .add(Restrictions.eq("id", decisiones.getId()))
                        .list();
                Decisiones decision = listadoDecisionesAux.get(0);
                listadoDecisiones.add(decision);
            }
            return listadoDecisiones;

        } catch (HibernateException hibEx) {
            log.error("Error al obtener pendientes de usuario. ", hibEx);
            throw hibEx;
        }
    }

    protected List<Ot> retornarOtPendientesPorUsuario(String idUsuario, List<Ot> listadoOts) {
        try {
            Criteria crit = sesion.createCriteria(Ot.class);
            crit.add(Restrictions.eq("usuarioId", idUsuario));
            for (Ot ot : listadoOts) {
                crit.add(Restrictions.eq("usuarioId", idUsuario));
            }
            List<Ot> listado = crit.list();
            return listado;
        } catch (HibernateException hibEx) {
            log.error("Error al obtener pendientes de usuario. ", hibEx);
            throw hibEx;
        }
    }

    protected boolean validarToken(String token, char estado) {
        try {
            long rows = (Long) sesion.createCriteria(Login.class)
                    .add(Restrictions.eq("token", token))
                    .add(Restrictions.eq("estado", estado))
                    .setProjection(Projections.rowCount())
                    //Pendiente agregar filtro de tiempo
                    .uniqueResult();
            return rows == 1L;
        } catch (HibernateException hibEx) {
            log.error("Error al validar credenciales de usuario. ", hibEx);
            throw hibEx;
        }
    }

    protected boolean validarTokenSesion(String token, int usuarioId , char estado) {
        try {
            long rows = (Long) sesion.createCriteria(Login.class)
                    .add(Restrictions.eq("usuarios.id", usuarioId))
                    .add(Restrictions.eq("token", token))
                    .add(Restrictions.eq("estado", estado))
                    .setProjection(Projections.rowCount())
                    //Pendiente agregar filtro de tiempo
                    .uniqueResult();
            return rows == 1L;
        } catch (HibernateException hibEx) {
            log.error("Error al validar credenciales de usuario. ", hibEx);
            throw hibEx;
        }
    }

    protected Login retornarLogin(String token, int usuarioId) {
        try {
            Login login = (Login) sesion.createCriteria(Login.class)
                    .add(Restrictions.eq("token", token))
                    .add(Restrictions.eq("usuarios.id", usuarioId))
                    //Pendiente agregar filtro de tiempo
                    .uniqueResult();
            return login;
        } catch (HibernateException hibEx) {
            log.error("Error al validar credenciales de usuario. ", hibEx);
            throw hibEx;
        }
    }

//    protected List<WorkflowEventos> listarWorkflowEventosPorWorkflow(int workflowId) throws Exception {
//        List<WorkflowEventos> lista = new ArrayList();
//        try {
//            Criteria criteria = sesion.createCriteria(WorkflowEventos.class);
//            criteria.setFetchMode("workflow_id", FetchMode.JOIN);
//            criteria.add(Restrictions.eq("workflow_id", workflowId));
//            lista = criteria.list();
//        } catch (HibernateException e) {
//            throw e;
//        }
//        return lista;
//    }
    protected WorkflowEventosEjecucion obtenerWorkflowEventosEjecucionPorEventoYOt(int eventoId, int otId) throws Exception {
        try {
            WorkflowEventosEjecucion wee = (WorkflowEventosEjecucion) sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .add(Restrictions.eq("id.eventoId", eventoId))
                    .add(Restrictions.eq("ot.id", otId))
                    .add(Restrictions.eq("ejecutado", false))
                    .uniqueResult();
            return wee;

        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected WorkflowEventosEjecucion obtenerWorkflowEventosEjecucionNNN(int eventoId, int otId, int usuarioEjecutorId) throws Exception {
        try {
            WorkflowEventosEjecucion wee = (WorkflowEventosEjecucion) sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .add(Restrictions.eq("id.eventoId", eventoId))
                    .add(Restrictions.eq("ot.id", otId))
                    .add(Restrictions.eq("usuarioEjecutorId", usuarioEjecutorId))
                    .add(Restrictions.eq("ejecutado", false))
                    .uniqueResult();
            return wee;

        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected WorkflowEventosEjecucion obtenerWorkflowEventosEjecucionPorEventoYOt2(int eventoId, int otId) throws Exception {
        try {
            WorkflowEventosEjecucion wee = (WorkflowEventosEjecucion) sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .add(Restrictions.eq("id.eventoId", eventoId))
                    .add(Restrictions.eq("ot.id", otId))
                    .uniqueResult();
            return wee;

        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected Ot obtenerDetalleOt2(int otId) throws Exception {
        try {
            Ot otDetalle = (Ot) sesion.createCriteria(Ot.class)
                    .setFetchMode("tipoOt", FetchMode.JOIN)
                    .setFetchMode("contrato", FetchMode.JOIN)
                    .setFetchMode("proveedor", FetchMode.JOIN)
                    .setFetchMode("gestor", FetchMode.JOIN)
                    .setFetchMode("tipoEstadoOt", FetchMode.JOIN)
                    .setFetchMode("estadoWorkflow", FetchMode.JOIN)
                    .add(Restrictions.eq("id", otId))
                    .uniqueResult();

            Ot retorno = new Ot();
            retorno.setId(otDetalle.getId());
            retorno.setJsonDetalleOt(otDetalle.getJsonDetalleOt());

            Proveedores proveedor = new Proveedores();
            proveedor.setId(otDetalle.getProveedor().getId());
            proveedor.setNombre(otDetalle.getProveedor().getNombre());
            retorno.setProveedor(proveedor);

            TipoOt tipoOt = new TipoOt();
            tipoOt.setId(otDetalle.getTipoOt().getId());
            tipoOt.setNombre(otDetalle.getTipoOt().getNombre());
            retorno.setTipoOt(tipoOt);

            Contratos contrato = new Contratos();
            contrato.setId(otDetalle.getContrato().getId());
            contrato.setNombre(otDetalle.getContrato().getNombre());
            retorno.setContrato(contrato);

            Usuarios gestor = new Usuarios();
            gestor.setId(otDetalle.getGestor().getId());
            gestor.setNombres(otDetalle.getGestor().getNombres());
            gestor.setApellidos(otDetalle.getGestor().getApellidos());
            retorno.setGestor(gestor);

            if (retorno.getTipoOt().getId() != 2) {
                TipoEstadoOt tipoEstadoOt = new TipoEstadoOt();
                tipoEstadoOt.setId(otDetalle.getTipoEstadoOt().getId());
                tipoEstadoOt.setNombre(otDetalle.getTipoEstadoOt().getNombre());
                retorno.setTipoEstadoOt(tipoEstadoOt);
            }

            retorno.setFechaCreacion(otDetalle.getFechaCreacion());
            retorno.setFechaInicioEstimada(otDetalle.getFechaInicioEstimada());
            retorno.setFechaTerminoEstimada(otDetalle.getFechaTerminoEstimada());

            retorno.setCubicadors(otDetalle.getCubicadors());
            retorno.setOtCubicadors(otDetalle.getOtCubicadors());

            return retorno;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected OtAux obtenerDetalleOtAux(int otId, Login login) throws Exception {
        OtAux retorno = new OtAux();
        try {
            Ot otDetalle = (Ot) sesion.createCriteria(Ot.class)
                    .add(Restrictions.eq("id", otId))
                    .uniqueResult();

            retorno.setJsonDetalleOt(otDetalle.getJsonDetalleOt());
            // agrego detalle de la Ot: Cubicacion -- Libro de Obras -- Actas -- Pagos
            Map detalleOt = new HashMap<>();

            // detalle OT
            detalleOt.put("detalleOt", otDetalle.getJsonDetalleOt());

            detalleOt.put("observaciones", otDetalle.getObservaciones());
            detalleOt.put("idDiseno", otDetalle.getIdAp());
            // Cubicacion
            List<Cubicador> cubicaciones = obtenerCubicacionPorIdOt(otDetalle.getId());
            // Actas
            List<Actas> actas = obtenerTodasLasActasPorOt(otDetalle.getId());
            if (actas.size() > 0) {
                String jsonActas = generarJsonActas(actas, cubicaciones,  otDetalle.getContrato().getId(), login);
                detalleOt.put("actas", jsonActas);
            } else {
                detalleOt.put("actas", "[]");
            }

            if(otDetalle.getContrato().getId() == 2){
                try{
                    List listaMaterialesOrdi = new ArrayList();
                    List<CubicadorOrdinario> cubicadorOrdinario = new ArrayList<CubicadorOrdinario>();
                    for (Cubicador cubicado : cubicaciones){

                        cubicadorOrdinario = obtenerListaCubicacionOrdinariaPorIdOt(cubicado.getId());
                    }
                    listaMaterialesOrdi =  generarJsonMaterialesDetalle(cubicadorOrdinario);
                    detalleOt.put("cubicacion", listaMaterialesOrdi);
                }catch (Exception e){
                    throw e;
                }
            } else  if(otDetalle.getContrato().getId() == 4){
                try{
                    List listaServiciosUnificado = new ArrayList();
//                    Map listaServiciosUnificado = new HashMap<>();
                    List<CubicadorDetalle> cubicadorDetalles = new ArrayList<CubicadorDetalle>();
                    for (Cubicador cubicado : cubicaciones){

                        cubicadorDetalles = obtenerCubicadorDetallePorIdCubicador(cubicado.getId());
                    }
                    listaServiciosUnificado =  generarJsonServiciosUnificado(cubicadorDetalles);
                    detalleOt.put("cubicacion", listaServiciosUnificado);
                }catch (Exception e){
                    throw e;
                }
            } else  if(otDetalle.getContrato().getId() == 9){
                try{
                    List listaServiciosUnificado = new ArrayList();
//                    Map listaServiciosUnificado = new HashMap<>();
                    List<CubicadorDetalle> cubicadorDetalles = new ArrayList<CubicadorDetalle>();
                    for (Cubicador cubicado : cubicaciones){
                    detalleOt.put("cubicacionId", cubicado.getId());
                    }
                }catch (Exception e){
                    throw e;
                }
            } else {
                Map mapaCubicacion = null;
                mapaCubicacion = generarJsonCubicacion(cubicaciones.get(0));
                detalleOt.put("cubicacion", mapaCubicacion);
            }
            // Libro de obras
            List<LibroObras> librosDeObras = obtenerLibrosDeObras(otDetalle.getId());
            if (librosDeObras.size() > 0) {
                List listaLibroObras = new ArrayList();
                listaLibroObras = generarJsonLibrosDeObras(librosDeObras);
                detalleOt.put("librosDeObras", listaLibroObras);
            } else {
                detalleOt.put("librosDeObras", "[]");
            }

            List<LibroObras> librosDeObrasCalidad = obtenerLibrosDeObrasCalidad(otDetalle.getId());
            if (librosDeObrasCalidad.size() > 0) {
                List listaLibroObras = new ArrayList();
                listaLibroObras = generarJsonLibrosDeObrasCalidad(librosDeObrasCalidad);
                detalleOt.put("libroDeObraCalidad", listaLibroObras);
            } else {
                detalleOt.put("libroDeObraCalidad", "[]");
            }


            // Adjuntos
            List<Adjuntos> listadoAdjuntos = obtenerAdjuntosPorOtRol(otDetalle.getId(), login.getRoles().getId());
            if(listadoAdjuntos.size()>0){
                String jsonAdjuntos = generarJsonAdjuntos(listadoAdjuntos);
                detalleOt.put("adjuntos", jsonAdjuntos);
            }else{
                detalleOt.put("adjuntos", "[]");
            }

            // Christian: cada ot tendra un check-list de calidad por lo que cuando este este completado es necesario desplegarlo en el detalle de la ot, de todas maneras queda registrado en el
            // libro de obras. Es decir esto es para una mejor visibilidad del checklist
            // Calidad
            List<CheckListOtAux> checkListOt = GenericDAO.getINSTANCE().obtenerCheckListOtPorOtID(otDetalle.getId());


            for (CheckListOtAux check: checkListOt){

                validarYSetearObjetoARetornar(check, null);

                for (CheckListOtDetalleAux checkDetalle: check.getCheckListOtDetalle()){
                    validarYSetearObjetoARetornar(checkDetalle, null);
                }

            }


            if (checkListOt.size() > 0) {
                detalleOt.put("librosDeObrasCalidad", checkListOt);
            } else {
                detalleOt.put("librosDeObrasCalidad", "[]");
            }



            // Usuarios Validadores
            List<UsuariosValidadores> listadoUsuariosValidadores = obtenerUsuariosValidadores(otDetalle.getId());

            List<ValidacionJerarquicaOt> validacionJerarquicaOts = new ArrayList<>();
            if(listadoUsuariosValidadores.size()>0){

//                ValidacionJerarquicaOt validacionJerarquicaOt = new ValidacionJerarquicaOt();
//                validacionJerarquicaOt.setOt_id(otDetalle.getId());

                List<ValidadoresOt> listadoValidadoresOt = new ArrayList();

                for (UsuariosValidadores usuarioValidador: listadoUsuariosValidadores){

                    ValidadoresOt validadorOt = new ValidadoresOt();

                    Usuarios usuario = obtenerUsuarioPorId(usuarioValidador.getUsuarios().getId());
                    validadorOt.setId(usuario.getId());
                    validadorOt.setNombreUsuario(usuario.getNombres() + " " + usuario.getApellidos());
                    Eventos evento = (Eventos) sesion.createCriteria(Eventos.class)
                            .add(Restrictions.eq("id", usuarioValidador.getEventos().getId())).uniqueResult();

                    validadorOt.setConcepto(evento.getNombre());
                    List<ValidacionesOtec> listadoValidacionesOt = obtenerValidacionesUsuarioOt(usuarioValidador.getId());
                    List<ValidacionesOt> validacionesOts = new ArrayList<>();
                    for(ValidacionesOtec validacion: listadoValidacionesOt){
                        ValidacionesOt validacionesOt = new ValidacionesOt();
                        validacionesOt.setFecha(validacion.getFecha());
                        validacionesOt.setComentario(validacion.getComentario());
                        validacionesOt.setValidacion(validacion.getValidacion());
                        validacionesOts.add(validacionesOt);

                    }
                    validadorOt.setValidaciones(validacionesOts);
                    listadoValidadoresOt.add(validadorOt);
                }

//                validacionJerarquicaOt.setValidadoresOt(listadoValidadoresOt);
//                validacionJerarquicaOts.add(validacionJerarquicaOt);
                detalleOt.put("usuariosValidadores", listadoValidadoresOt);
            }else{
                detalleOt.put("usuariosValidadores", "[]");
            }

//            retorno.setEjecutar(true);
            retorno.setDetalleOt(detalleOt);

            retorno.setId(otDetalle.getId());
            retorno.setNombre(otDetalle.getNombre());

            Proveedores proveedor = new Proveedores();
            proveedor.setId(otDetalle.getProveedor().getId());
            proveedor.setNombre(otDetalle.getProveedor().getNombre());
            retorno.setProveedor(proveedor);

            TipoOt tipoOt = new TipoOt();
            tipoOt.setId(otDetalle.getTipoOt().getId());
            tipoOt.setNombre(otDetalle.getTipoOt().getNombre());
            retorno.setTipoOt(tipoOt);

            Contratos contrato = new Contratos();
            contrato.setId(otDetalle.getContrato().getId());
            contrato.setNombre(otDetalle.getContrato().getNombre());
            retorno.setContrato(contrato);

            Usuarios gestor = new Usuarios();
            gestor.setId(otDetalle.getGestor().getId());
            gestor.setNombres(otDetalle.getGestor().getNombres());
            gestor.setApellidos(otDetalle.getGestor().getApellidos());
            retorno.setGestor(gestor);

            if (retorno.getTipoOt().getId() != 2) {
                TipoEstadoOt tipoEstadoOt = new TipoEstadoOt();
                tipoEstadoOt.setId(otDetalle.getTipoEstadoOt().getId());
                tipoEstadoOt.setNombre(otDetalle.getTipoEstadoOt().getNombre());
                retorno.setTipoEstadoOt(tipoEstadoOt);
            }
            retorno.setNumeroAp(otDetalle.getNumeroAp());
            retorno.setFechaCreacion(otDetalle.getFechaCreacion());
            retorno.setFechaInicioEstimada(otDetalle.getFechaInicioEstimada());
            retorno.setFechaTerminoEstimada(otDetalle.getFechaTerminoEstimada());
            retorno.setIdPmo(otDetalle.getIdPmo());
            retorno.setLp(otDetalle.getLp());
            retorno.setPep2(otDetalle.getPep2());

        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }

        return retorno;
    }


    protected Ot obtenerDetalleOtLLave(int otId) throws Exception {
        Ot retorno = new Ot();
        try {
            retorno = (Ot) sesion.createCriteria(Ot.class)
                    .add(Restrictions.eq("id", otId))
                    .setFetchMode("tipoOt", FetchMode.JOIN)
                    .uniqueResult();

        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }

        return retorno;
    }



    protected OtAux obtenerDetalleOtCompleto(int otId) throws Exception {
        OtAux retorno = new OtAux();
        try {
            Ot otDetalle = (Ot) sesion.createCriteria(Ot.class)
                    .add(Restrictions.eq("id", otId))
                    .uniqueResult();

            retorno.setJsonDetalleOt(otDetalle.getJsonDetalleOt());
            // agrego detalle de la Ot: Cubicacion -- Libro de Obras -- Actas -- Pagos
            Map detalleOt = new HashMap<>();

            // detalle OT
            detalleOt.put("detalleOt", otDetalle.getJsonDetalleOt());

            detalleOt.put("observaciones", otDetalle.getObservaciones());

            // Actas
            Actas actas = obtenerActaParaValidar(otDetalle.getId());
            String jsonActas = "";
            jsonActas = generarJsonActasLlaveEnMano(actas, otDetalle.getContrato().getId());
            detalleOt.put("actas", jsonActas);
            detalleOt.put("actaId", actas.getId());

            // Cubicacion
            List<Cubicador> cubicaciones = obtenerCubicacionPorIdOt(otDetalle.getId());
            if(otDetalle.getContrato().getId() == 2){
                try{
                    List listaMaterialesOrdi = new ArrayList();
                    List<CubicadorOrdinario> cubicadorOrdinario = new ArrayList<CubicadorOrdinario>();
                    for (Cubicador cubicado : cubicaciones){

                        cubicadorOrdinario = obtenerListaCubicacionOrdinariaPorIdOt(cubicado.getId());
                    }
                    listaMaterialesOrdi =  generarJsonMaterialesDetalle(cubicadorOrdinario);
                    detalleOt.put("cubicacion", listaMaterialesOrdi);
                }catch (Exception e){
                    throw e;
                }
            } else  if(otDetalle.getContrato().getId() == 4){
                try{
                    List listaServiciosUnificado = new ArrayList();
//                    Map listaServiciosUnificado = new HashMap<>();
                    List<CubicadorDetalle> cubicadorDetalles = new ArrayList<CubicadorDetalle>();
                    for (Cubicador cubicado : cubicaciones){

                        cubicadorDetalles = obtenerCubicadorDetallePorIdCubicador(cubicado.getId());
                    }
                    listaServiciosUnificado =  generarJsonServiciosUnificado(cubicadorDetalles);
                    detalleOt.put("cubicacion", listaServiciosUnificado);
                }catch (Exception e){
                    throw e;
                }
            } else {
                Map mapaCubicacion = new HashMap();
                mapaCubicacion = generarJsonCubicacion(cubicaciones.get(0));
                detalleOt.put("cubicacion", mapaCubicacion);
            }
            // Libro de obras
            List<LibroObras> librosDeObras = obtenerLibrosDeObras(otDetalle.getId());
            if (librosDeObras.size() > 0) {
                List listaLibroObras = new ArrayList();
                listaLibroObras = generarJsonLibrosDeObras(librosDeObras);
                detalleOt.put("librosDeObras", listaLibroObras);
            } else {
                detalleOt.put("librosDeObras", "[]");
            }

            retorno.setDetalleOt(detalleOt);

            retorno.setId(otDetalle.getId());
            retorno.setNombre(otDetalle.getNombre());

            Proveedores proveedor = new Proveedores();
            proveedor.setId(otDetalle.getProveedor().getId());
            proveedor.setNombre(otDetalle.getProveedor().getNombre());
            retorno.setProveedor(proveedor);

            TipoOt tipoOt = new TipoOt();
            tipoOt.setId(otDetalle.getTipoOt().getId());
            tipoOt.setNombre(otDetalle.getTipoOt().getNombre());
            retorno.setTipoOt(tipoOt);

            Contratos contrato = new Contratos();
            contrato.setId(otDetalle.getContrato().getId());
            contrato.setNombre(otDetalle.getContrato().getNombre());
            retorno.setContrato(contrato);

            Usuarios gestor = new Usuarios();
            gestor.setId(otDetalle.getGestor().getId());
            gestor.setNombres(otDetalle.getGestor().getNombres());
            gestor.setApellidos(otDetalle.getGestor().getApellidos());
            retorno.setGestor(gestor);

            if (retorno.getTipoOt().getId() != 2) {
                TipoEstadoOt tipoEstadoOt = new TipoEstadoOt();
                tipoEstadoOt.setId(otDetalle.getTipoEstadoOt().getId());
                tipoEstadoOt.setNombre(otDetalle.getTipoEstadoOt().getNombre());
                retorno.setTipoEstadoOt(tipoEstadoOt);
            }

            retorno.setFechaCreacion(otDetalle.getFechaCreacion());
            retorno.setFechaInicioEstimada(otDetalle.getFechaInicioEstimada());
            retorno.setFechaTerminoEstimada(otDetalle.getFechaTerminoEstimada());

            retorno.setPep2(otDetalle.getPep2());
            retorno.setLp(otDetalle.getLp());
            retorno.setIdPmo(otDetalle.getIdPmo());

            if(otDetalle.getCentral() != null) {
                Centrales central = new Centrales();
                central.setId(otDetalle.getCentral().getId());
                retorno.setCentral(central);
            }

        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }

        return retorno;
    }

    private String generarJsonActas(List<Actas> listadoActas, List<Cubicador>cubicaciones,  int tipoContrato, Login login) throws Exception {

        String jsonActas = "[";
        int contadorActas = 0;
        for (Actas acta : listadoActas) {

            if (contadorActas != 0) {
                jsonActas = jsonActas + ",";
            }

            String formularioActa = "";
            String detalleServiciosActa = "[";
            String detalleServiciosAdicionalesActa = "[";
            String detalleMaterialesActa = "[";
            String detalleMaterialesAdicionalesActa = "[";
            int contMateriales = 0;
            int contMaterialesAdicionales = 0;
            int contServicios = 0;
            int contServiciosAdicionales = 0;
            String observaciones = acta.getObservaciones();
            observaciones = formateaStringConCaracteresNoaceptadosEnJSon(observaciones, "");
            String fechaCreacion = obtienefechaSegunFormato(acta.getFecha(), "yyyy-MM-dd hh:mm:ss");
            Date fechaTerminoEstimada = acta.getOt().getFechaTerminoEstimada();
            List<ValidacionesOtec> listaValidacionesOtec = new ArrayList<ValidacionesOtec>();
            List<Map> listaObs = new ArrayList<>();
            Map mapaObservacionesGerencia = new HashMap();
            String obsgerencia = "[";
            int i = 0;
            double montoTotalActa = 0;
            String coma = "";
            if(login.getRoles().getId() == 5 || login.getRoles().getId() == 11 || login.getRoles().getId() == 2 || login.getRoles().getId() == 10) {
                List<UsuariosValidadores> listUsuariosValidadores = new ArrayList<UsuariosValidadores>();
                Criteria criteria3 = sesion.createCriteria(UsuariosValidadores.class)
                        .add(Restrictions.eq("ot.id", acta.getOt().getId()));
                listUsuariosValidadores = (List<UsuariosValidadores>) criteria3.list();

                for (UsuariosValidadores usuariosValidadores : listUsuariosValidadores){
                    listaValidacionesOtec = new ArrayList<ValidacionesOtec>();
                    Criteria criteria4 = sesion.createCriteria(ValidacionesOtec.class)
                            .add(Restrictions.eq("usuariosValidadores.id", usuariosValidadores.getId()));
                    listaValidacionesOtec = (List<ValidacionesOtec>) criteria4.list();
                    for (ValidacionesOtec validacionesOtecAux : listaValidacionesOtec) {
                        mapaObservacionesGerencia = new HashMap();
                        String nombreUser = usuariosValidadores.getUsuarios().getNombres() + " " + usuariosValidadores.getUsuarios().getApellidos();
                        if(i > 0){
                            coma = ",";
                        } else {
                            coma = "";
                        }
                        obsgerencia = obsgerencia + coma + "{  \n" +
                                "  \"usuario\": "   + "\"" + nombreUser + "\""+  ",\n" +
                                "  \"fecha\": "   + "\"" + validacionesOtecAux.getFecha() + "\""+  ",\n" +
                                "  \"observaciones\": "  + "\"" + formateaStringConCaracteresNoaceptadosEnJSon(validacionesOtecAux.getComentario(), "") + "\""+  "\n" +
                                "}";
//                        mapaObservacionesGerencia.put("usuario", nombreUser);
//                        mapaObservacionesGerencia.put("fecha", validacionesOtecAux.getFecha());
//                        mapaObservacionesGerencia.put("observaciones", validacionesOtecAux.getComentario());

                        listaObs.add(mapaObservacionesGerencia);
                        i = i +1;
                    }
                }
                if(acta.getMontoTotalActa() != null){
                    montoTotalActa = acta.getMontoTotalActa();
                }
            }
            obsgerencia = obsgerencia + "]";

            boolean tieneHem = false;
            Long numeroHem = new Long(0);

            DetalleBolsasValidacionesPag detalleBolsasValidacionesPag = new DetalleBolsasValidacionesPag();
            detalleBolsasValidacionesPag = validaHemActa(acta.getId());
            if(detalleBolsasValidacionesPag != null &&  detalleBolsasValidacionesPag.getPagosId() != null){
                numeroHem = obtieneNumeroHem(detalleBolsasValidacionesPag.getPagosId());
                tieneHem = true;
            };
            int tipoMonedaId = 0;
            tipoMonedaId = obtieneTipoMoneda(acta, tipoContrato, cubicaciones.get(0) );
            formularioActa = formularioActa + "{  \n" +
                    "  \"observaciones\": "   + observaciones +  ",\n" +
                    "  \"observacionesGerencia\": "   + obsgerencia +  ",\n" +
                    "  \"id\": "   + acta.getId() +  ",\n" +
                    "  \"validacionSistema\": "  + "\"" + acta.getValidacionSistema() + "\"" + ",\n" +
                    "  \"montoCubicado\": "  + "\"" + acta.getMontoTotalActaCubicado() + "\"" + ",\n" +
                    "  \"montoAdicional\": "  + "\"" + acta.getMontoTotalActaAdicional() + "\"" + ",\n" +
                    "  \"montoTotal\": "  + "\"" + acta.getMontoTotalAPagar() + "\"" + ",\n" +
                    "  \"tipoMoneda\": " + tipoMonedaId + ",\n"+
                    "  \"validacionUsuario\": "  + "\"" + acta.getValidacionUsuarios() + "\"" + ",\n" +
                    "  \"fechaCreacion\": "  + "\"" + fechaCreacion + "\"" + ",\n" +
                    "  \"fechaTerminoEstimada\": "  + "\"" + fechaTerminoEstimada + "\"" + ",\n"+
                    "  \"montoTotal\": "  + "\"" + montoTotalActa + "\"" + ",\n"+
                    "  \"tieneHem\": " + tieneHem + ",\n"+
                    "  \"descargarActa\": " + acta.getPagoAprobado() + ",\n"+
                    "  \"numeroHem\": " + numeroHem + ",\n";
            if(tipoContrato != 2 && tipoContrato !=4) {
                Set<DetalleMaterialesActas> listadoDetalleMateriales = acta.getDetalleMaterialesActas();
                Set<DetalleMaterialesAdicionalesActas> listadoDetalleMaterialesAdicionales = acta.getDetalleMaterialesAdicionalesActas();
                Set<DetalleServiciosActas> listadoDetalleServicios = acta.getDetalleServiciosActas();
                Set<DetalleServiciosAdicionalesActas> listadoDetalleServiciosAdicionales = acta.getDetalleServiciosAdicionalesActas();

                if (listadoDetalleMateriales.size() > 0) {
                    //
                    for (DetalleMaterialesActas detalleMaterial : listadoDetalleMateriales) {
                        if (contMateriales != 0) {
                            detalleMaterialesActa = detalleMaterialesActa + ",";
                        }
                        detalleMaterialesActa = detalleMaterialesActa + "{\n" +
                                "  \"nombre\": " + "\"" + detalleMaterial.getMateriales().getNombre() + "\"" + ",\n" +
                                "  \"descripcion\": " + "\"" + detalleMaterial.getMateriales().getDescripcion() + "\"" + ",\n" +
                                "  \"cantidadCubicada\": " + detalleMaterial.getTotalUnidadesCubicadas() + ",\n" +
                                "  \"cantidadAcumuladaInformada\": " + detalleMaterial.getTotalUnidadesEjecucionOt() + ",\n" +
                                "  \"cantidadActualInformada\": " + detalleMaterial.getTotalUnidadesEjecucionActa() + "\n" +
                                "}";
                        contMateriales++;
                    }

                }

                if (listadoDetalleMaterialesAdicionales.size() > 0) {
                    //
                    for (DetalleMaterialesAdicionalesActas detalleMaterialAdicional : listadoDetalleMaterialesAdicionales) {
                        if (contMaterialesAdicionales != 0) {
                            detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + ",";
                        }
                        detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + "{\n" +
                                "  \"nombre\": " + "\"" + detalleMaterialAdicional.getMateriales().getNombre() + "\"" + ",\n" +
                                "  \"descripcion\": " + "\"" + detalleMaterialAdicional.getMateriales().getDescripcion() + "\"" + ",\n" +
                                "  \"cantidadCubicada\": " + "0" + ",\n" +
                                "  \"cantidadAcumuladaInformada\": " + detalleMaterialAdicional.getTotalUnidadesEjecucionOt() + ",\n" +
                                "  \"cantidadActualInformada\": " + detalleMaterialAdicional.getTotalUnidadesEjecucionActa() + "\n" +
                                "}";
                        contMaterialesAdicionales++;
                    }

                }
//
//                if (listadoDetalleServicios.size() > 0) {
//                    //
//                    for (DetalleServiciosActas detalleServicios : listadoDetalleServicios) {
//                        if (contServicios != 0) {
//                            detalleServiciosActa = detalleServiciosActa + ",";
//                        }
//                        detalleServiciosActa = detalleServiciosActa + "{\n" +
//                                "  \"nombre\": " + "\"" + detalleServicios.getServicio().getNombre() + "\"" + ",\n" +
//                                "  \"descripcion\": " + "\"" + detalleServicios.getServicio().getDescripcion() + "\"" + ",\n" +
//                                "  \"cantidadCubicada\": " + detalleServicios.getTotalUnidadesCubicadas() + ",\n" +
//                                "  \"cantidadAcumuladaInformada\": " + detalleServicios.getTotalUnidadesEjecucionOt() + ",\n" +
//                                "  \"cantidadActualInformada\": " + detalleServicios.getTotalUnidadesEjecucionActa() + "\n" +
//                                "}";
//                        contServicios++;
//                    }
//
//                }
                if (listadoDetalleServicios.size() > 0) {
                    //
                    for (DetalleServiciosActas detalleServicios : listadoDetalleServicios) {
                        if (contServicios != 0) {
                            detalleServiciosActa = detalleServiciosActa + ",";
                        }
                        Servicios servicios = new Servicios();
                        Criteria criteria = sesion.createCriteria(Servicios.class)
                                .add(Restrictions.eq("id", detalleServicios.getServicio().getId()));
                        servicios = (Servicios) criteria.uniqueResult();
                        detalleServiciosActa = detalleServiciosActa + "{\n" +
                                "  \"nombre\": " + "\"" + servicios.getNombre() + "\"" + ",\n" +
                                "  \"descripcion\": " + "\"" + servicios.getDescripcion() + "\"" + ",\n" +
                                "  \"cantidadCubicada\": " + detalleServicios.getTotalUnidadesCubicadas() + ",\n" +
                                "  \"cantidadAcumuladaInformada\": " + detalleServicios.getTotalUnidadesEjecucionOt() + ",\n" +
                                "  \"cantidadActualInformada\": " + detalleServicios.getTotalUnidadesEjecucionActa() + "\n" +
                                "}";
                        contServicios++;
                    }

                }
//                if (listadoDetalleServiciosAdicionales.size() > 0) {
//                    //
//                    for (DetalleServiciosAdicionalesActas detalleServiciosAdicionales : listadoDetalleServiciosAdicionales) {
//                        if (contServiciosAdicionales != 0) {
//                            detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + ",";
//                        }
//                        detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "{\n" +
////                                            "  \"id\": "  + "\"" + detalleServiciosAdicionales.getId().getServicioId() + "\"" + ",\n" +
//                                "  \"nombre\": " + "\"" + detalleServiciosAdicionales.getServicio().getNombre() + "\"" + ",\n" +
//                                "  \"descripcion\": " + "\"" + detalleServiciosAdicionales.getServicio().getDescripcion() + "\"" + ",\n" +
//                                "  \"cantidadCubicada\": " + "0" + ",\n" +
//                                "  \"cantidadAcumuladaInformada\": " + detalleServiciosAdicionales.getTotalUnidadesEjecucionOt() + ",\n" +
//                                "  \"cantidadActualInformada\": " + detalleServiciosAdicionales.getTotalUnidadesEjecucionActa() + "\n" +
//                                "}";
//                        contServiciosAdicionales++;
//                    }
//
//                }
                if (listadoDetalleServiciosAdicionales.size() > 0) {
                    //
                    for (DetalleServiciosAdicionalesActas detalleServiciosAdicionales : listadoDetalleServiciosAdicionales) {
                        if (contServiciosAdicionales != 0) {
                            detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + ",";
                        }
                        Servicios serviciosAdicionalesAux = new Servicios();
                        Criteria criteria = sesion.createCriteria(Servicios.class)
                                .add(Restrictions.eq("id", detalleServiciosAdicionales.getServicio().getId()));
                        serviciosAdicionalesAux = (Servicios) criteria.uniqueResult();
                        detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "{\n" +
//                                            "  \"id\": "  + "\"" + detalleServiciosAdicionales.getId().getServicioId() + "\"" + ",\n" +
                                "  \"nombre\": " + "\"" + serviciosAdicionalesAux.getNombre() + "\"" + ",\n" +
                                "  \"descripcion\": " + "\"" + serviciosAdicionalesAux.getDescripcion() + "\"" + ",\n" +
                                "  \"cantidadCubicada\": " + "0" + ",\n" +
                                "  \"cantidadAcumuladaInformada\": " + detalleServiciosAdicionales.getTotalUnidadesEjecucionOt() + ",\n" +
                                "  \"cantidadActualInformada\": " + detalleServiciosAdicionales.getTotalUnidadesEjecucionActa() + "\n" +
                                "}";
                        contServiciosAdicionales++;
                    }

                }

                detalleMaterialesActa = detalleMaterialesActa + "]";
                detalleMaterialesAdicionalesActa = detalleMaterialesAdicionalesActa + "]";
                detalleServiciosActa = detalleServiciosActa + "]";
                detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "]";

                formularioActa = formularioActa + "\"detalleMaterialesActa\":" + detalleMaterialesActa + ",";
                formularioActa = formularioActa + "\"detalleMaterialesAdicionalesActa\":" + detalleMaterialesAdicionalesActa + ",";
                formularioActa = formularioActa + "\"detalleServiciosActa\":" + detalleServiciosActa + ",";
                formularioActa = formularioActa + "\"detalleServiciosAdicionalesActa\":" + detalleServiciosAdicionalesActa;
                formularioActa = formularioActa + "}";
            } else if(tipoContrato == 2) {
                int contadorObjeto = 0;
                List<DetalleCubicadorOrdinarioActas> detalleMaterialesOrdinariosActas = new ArrayList<DetalleCubicadorOrdinarioActas>();
                detalleMaterialesOrdinariosActas = obtieneListaDetalleMaterialesOrdinarios(acta.getId());
                if (detalleMaterialesOrdinariosActas.size() > 0) {
                    //
                    for (DetalleCubicadorOrdinarioActas cubOrd : detalleMaterialesOrdinariosActas) {
                        CubicadorOrdinario cubicadorOrdinario = new CubicadorOrdinario();

                        cubicadorOrdinario = obtenCubicadorOrdinario(cubOrd.getId().getCubicadorOrdinarioId());
                        detalleMaterialesActa = detalleMaterialesActa + "{\n" +
                                "  \"nombre\": " + "\"" + cubicadorOrdinario.getItem() + "\"" + ",\n" +
                                "  \"descripcion\": " + "\"" + cubicadorOrdinario.getItem() + "\"" + ",\n" +
                                "  \"cantidadCubicada\": " + cubicadorOrdinario.getCantidad() + ",\n" +
                                "  \"cantidadAcumuladaInformada\": " + cubOrd.getTotalUnidadesEjecucionOt() + ",\n" +
                                "  \"cantidadActualInformada\": " + cubOrd.getTotalUnidadesEjecucionActa() + "\n" +
                                "}";
                        if((detalleMaterialesOrdinariosActas.size() - 1) > contadorObjeto){
                            detalleMaterialesActa = detalleMaterialesActa + ",";
                        }
                        contadorObjeto ++;
                        contMateriales++;
                    }

                }
                detalleMaterialesActa = detalleMaterialesActa + "]";
                formularioActa = formularioActa + "\"detalleMaterialesActa\":" + detalleMaterialesActa + "";
                formularioActa = formularioActa + "}";
            } else if(tipoContrato == 4){
                int cubicadorId = 0;
                int contadorObjeto = 0;
                List<CubicadorDetalleUnificadoActas> detalleActasServiciosUnificados = new ArrayList<CubicadorDetalleUnificadoActas>();
                detalleActasServiciosUnificados = obtieneListaDetalleActasServiciosUnificado(acta.getId());

                if (detalleActasServiciosUnificados.size() > 0) {
                    //
                    for (CubicadorDetalleUnificadoActas actaDetalle : detalleActasServiciosUnificados) {
                        CubicadorDetalle cubicadorDetalle = new CubicadorDetalle();

                        cubicadorDetalle = obtenerUnicoCubicadorDetallePorIdCubicador(actaDetalle.getId().getCubicadorDetalleId());
                        detalleMaterialesActa = detalleMaterialesActa + "{\n" +
                                "  \"nombre\": " + "\"" + cubicadorDetalle.getServicios().getNombre() + "\"" + ",\n" +
                                "  \"descripcion\": " + "\"" + cubicadorDetalle.getServicios().getDescripcion() + "\"" + ",\n" +
                                "  \"cantidadCubicada\": " + cubicadorDetalle.getCantidad() + ",\n" +
                                "  \"cantidadActualInformada\": " + actaDetalle.getTotalUnidadesEjecucionActa() + ",\n" +
                                "  \"total\": " + cubicadorDetalle.getTotal() + "\n" +
                                "}";
                        if((detalleActasServiciosUnificados.size() - 1) > contadorObjeto){
                            detalleMaterialesActa = detalleMaterialesActa + ",";
                        }
                        contadorObjeto ++;
                        contMateriales++;
                        cubicadorId = cubicadorDetalle.getCubicadorId();
                    }

                }

                List<CubicadorDetalleAdicionales> detalleServiciosAdicionalesUnificados = new ArrayList<CubicadorDetalleAdicionales>();
                detalleServiciosAdicionalesUnificados = obtenerUnicoCubicadorDetalleAdicionalPorCubicadorIdValidacion(cubicadorId);
                contadorObjeto = 0;
                if (detalleServiciosAdicionalesUnificados.size() > 0) {
                    //
                    for (CubicadorDetalleAdicionales detalleAdicionales : detalleServiciosAdicionalesUnificados) {

                        detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "{\n" +
                                "  \"nombre\": " + "\"" + detalleAdicionales.getServicios().getNombre() + "\"" + ",\n" +
                                "  \"descripcion\": " + "\"" + detalleAdicionales.getServicios().getDescripcion() + "\"" + ",\n" +
                                "  \"cantidadCubicada\": " + detalleAdicionales.getCantidad() + ",\n" +
                                "  \"cantidadActualInformada\": " + detalleAdicionales.getCantidad() + ",\n" +
                                "  \"total\": " + detalleAdicionales.getTotal() + "\n" +
                                "}";
                        if((detalleServiciosAdicionalesUnificados.size() - 1) > contadorObjeto){
                            detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + ",";
                        }
                        contadorObjeto ++;
                        contMateriales++;
                    }

                }

                detalleMaterialesActa = detalleMaterialesActa + "],";
                detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + "]";
                formularioActa = formularioActa + "\"detalleServiciosActa\":" + detalleMaterialesActa + "";
                formularioActa = formularioActa + "\"detalleServiciosAdicionalesActa\":" + detalleServiciosAdicionalesActa;
                formularioActa = formularioActa + "}";

            }
            jsonActas = jsonActas + formularioActa;
            contadorActas++;
        }
        jsonActas = jsonActas + "]";
        return jsonActas;
    }

private int obtieneTipoMoneda(Actas acta, int tipoContrato, Cubicador cubicador){
    int tipoMoneda = 0;
    try{
        switch (tipoContrato){
            case 2:
                List<CubicadorOrdinario> cubicadorOrdinario = new ArrayList<CubicadorOrdinario>();
                cubicadorOrdinario = obtenerCubicadorOrdinarioPorIdCubicador(cubicador.getId());
                tipoMoneda = cubicadorOrdinario.get(0).getTipoMoneda().getId();
                break;
            case 4:
                List<CubicadorDetalleUnificadoActas> detalleActasServiciosUnificados = new ArrayList<CubicadorDetalleUnificadoActas>();
                detalleActasServiciosUnificados = obtieneListaDetalleActasServiciosUnificado(acta.getId());
                for(CubicadorDetalleUnificadoActas serviciosDetellalle: detalleActasServiciosUnificados ){
                    List<ProveedoresServicios> proveedoresServicios = null;
                    CubicadorDetalle cubicadorDetalle = new CubicadorDetalle();
                    cubicadorDetalle = obtenerUnicoCubicadorDetallePorIdCubicador(serviciosDetellalle.getId().getCubicadorDetalleId());
                    proveedoresServicios = obtieneProveedorServicios(tipoContrato, cubicadorDetalle.getRegionId(),  cubicador.getProveedor().getId(),  cubicadorDetalle.getServicios().getId());
                    if(proveedoresServicios.size() > 0){
                        tipoMoneda = proveedoresServicios.get(0).getTipoMoneda().getId();
                        break;
                    }
                }
                break;

            default:
                Set<DetalleServiciosActas> listadoDetalleServicios = acta.getDetalleServiciosActas();
                for (DetalleServiciosActas detalleServicios : listadoDetalleServicios) {
                    List<ProveedoresServicios> proveedoresServicios = null;
                    proveedoresServicios = obtieneProveedorServicios(tipoContrato, cubicador.getRegion().getId(),  cubicador.getProveedor().getId(),  detalleServicios.getServicio().getId());
                    tipoMoneda = proveedoresServicios.get(0).getTipoMoneda().getId();
                    break;
                }
                break;

        }
    }catch (Exception e){

    }
    return tipoMoneda;
}
    private DetalleBolsasValidacionesPag validaHemActa (int actaId) throws  Exception{
        DetalleBolsasValidacionesPag detalleBolsasValidacionesPag = new DetalleBolsasValidacionesPag();
        try{
            Criteria criteria = sesion.createCriteria(DetalleBolsasValidacionesPag.class)
                    .add(Restrictions.eq("actaId", actaId));
            detalleBolsasValidacionesPag = (DetalleBolsasValidacionesPag) criteria.uniqueResult();
        }catch (Exception e){
            throw e;
        }
        return detalleBolsasValidacionesPag;
    };
    private Long obtieneNumeroHem (int pagoId) throws  Exception{
        Pagos pagos = new Pagos();
        Long numeroHem = new Long(0);
        try{
            Criteria criteria = sesion.createCriteria(Pagos.class)
                    .add(Restrictions.eq("id", pagoId));
            pagos = (Pagos) criteria.uniqueResult();
            numeroHem = pagos.getHemId();
        }catch (Exception e){
            throw e;
        }
        return numeroHem;
    };

    private String generarJsonActasLlaveEnMano(Actas acta, int tipoContrato) throws Exception {

        String jsonActas = "[";

        String formularioActa = "";
        String detalleServiciosActa = "[";
        String detalleServiciosAdicionalesActa = "[";
        int contServicios = 0;
        int contServiciosAdicionales = 0;
        String observaciones = acta.getObservaciones();
        if(observaciones != null){
            observaciones = observaciones.replace("\n"," ");
            observaciones = observaciones.replace("\t"," ");
        } else {
            observaciones = "Sin observaciones";
        }
        Date fechaCreacion = acta.getOt().getFechaCreacion();
        Date fechaTerminoEstimada = acta.getOt().getFechaTerminoEstimada();

        formularioActa = formularioActa + "{  \n" +
                "  \"observaciones\": "   + observaciones +  ",\n" +
                "  \"id\": "   + acta.getId() +  ",\n" +
                "  \"validacionSistema\": "  + "\"" + acta.getValidacionSistema() + "\"" + ",\n" +
                "  \"validacionUsuario\": "  + "\"" + acta.getValidacionUsuarios() + "\"" + ",\n" +
                "  \"fechaCreacion\": "  + "\"" + fechaCreacion + "\"" + ",\n" +
                "  \"fechaTerminoEstimada\": "  + "\"" + fechaTerminoEstimada + "\"" + ",\n" ;


            Set<DetalleServiciosActas> listadoDetalleServicios = acta.getDetalleServiciosActas();
            Set<DetalleServiciosAdicionalesActas> listadoDetalleServiciosAdicionales = acta.getDetalleServiciosAdicionalesActas();

            Servicios servicioCubicado = new Servicios();
            DetalleServiciosActas detalleServiciosActasCubicado = new DetalleServiciosActas();
            for (DetalleServiciosActas detalleServicios : listadoDetalleServicios) {
                Criteria criteria = sesion.createCriteria(Servicios.class)
                        .add(Restrictions.eq("id", detalleServicios.getServicio().getId()));
                servicioCubicado = (Servicios) criteria.uniqueResult();
                detalleServiciosActasCubicado = detalleServicios;
            }

            Servicios servicioAdicional = new Servicios();
            DetalleServiciosAdicionalesActas detalleServiciosActasAdicionalAux = new DetalleServiciosAdicionalesActas();

            if (listadoDetalleServiciosAdicionales.size() > 0) {
                for (DetalleServiciosAdicionalesActas detalleServiciosAdicionales : listadoDetalleServiciosAdicionales) {
                    if (contServiciosAdicionales != 0) {
                        detalleServiciosAdicionalesActa = detalleServiciosAdicionalesActa + ",";
                    }
                    Criteria criteria = sesion.createCriteria(Servicios.class)
                            .add(Restrictions.eq("id", detalleServiciosAdicionales.getServicio().getId()));
                    servicioAdicional = (Servicios) criteria.uniqueResult();
                    detalleServiciosActasAdicionalAux = detalleServiciosAdicionales;
                }

            }

            double cantidadFinal = 0;

            cantidadFinal = detalleServiciosActasCubicado.getTotalUnidadesEjecucionActa();

            if(servicioCubicado.getId() == servicioAdicional.getId()){
                cantidadFinal = cantidadFinal + detalleServiciosActasAdicionalAux.getTotalUnidadesEjecucionActa();
            }

//            formularioActa = formularioActa + "\"servicio\":" + servicioCubicado.getNombre() + ",";
            formularioActa = formularioActa + "\"servicioId\":" + servicioCubicado.getId() + ",";
            formularioActa = formularioActa + "  \"servicio\": "  + "\"" + servicioCubicado.getNombre() + "\"" + ",\n";
            formularioActa = formularioActa + "\"cantidadFinal\":" + cantidadFinal;
            formularioActa = formularioActa + "}";

        jsonActas = jsonActas + formularioActa;

        jsonActas = jsonActas + "]";
        return jsonActas;
    }







    private List generarJsonMaterialesDetalle(List<CubicadorOrdinario> listadoItems) {
        int contadorTotalMateriales = listadoItems.size();
        List listaItemsCubicados = new ArrayList();
        Map mapaItemsCubicados = new HashMap();
        for (CubicadorOrdinario items : listadoItems) {
            mapaItemsCubicados = new HashMap();
            mapaItemsCubicados.put("idItems", items.getId());
            mapaItemsCubicados.put("items", items.getItem());
            mapaItemsCubicados.put("cantidadCubicada", items.getCantidad());
            mapaItemsCubicados.put("precio", items.getPrecio());
            mapaItemsCubicados.put("unidadMedida", items.getTipoUnidadMedida().getNombre());
            mapaItemsCubicados.put("precioTotalCubicado", items.getTotal());
            mapaItemsCubicados.put("tipoMoneda", items.getTipoMoneda().getNombre());
            mapaItemsCubicados.put("tipoMonedaId", items.getTipoMoneda().getId());

            listaItemsCubicados.add(mapaItemsCubicados);

        }

        return listaItemsCubicados;
    }
    private List generarJsonServiciosUnificado(List<CubicadorDetalle> listadoServicios) {
        List mapaServicios = new ArrayList();
//        Map mapaServicios = new HashMap();
        Map detalleItems = new HashMap();
        String nombre = "";
        String descripcion = "";
        for (CubicadorDetalle servicio : listadoServicios) {
            detalleItems = new HashMap();
//            nombre = servicio.getServicios().getNombre().replace("\"", " pulg. ");
//            descripcion = servicio.getServicios().getDescripcion().replace("\"", " pulg. ");

            detalleItems.put("idServicio", servicio.getId());
            detalleItems.put("nombre", servicio.getServicios().getNombre());
            detalleItems.put("descripcion", servicio.getServicios().getDescripcion());
            detalleItems.put("cantidadCubicada", servicio.getCantidad());
            detalleItems.put("precio", servicio.getPrecio());
            detalleItems.put("precioTotalCubicado", servicio.getTotal());
            detalleItems.put("tipoMonedaId", servicio.getTipoMoneda().getId());
            detalleItems.put("tipoMoneda", servicio.getTipoMoneda().getNombre());
            detalleItems.put("regionId", servicio.getRegiones().getId());
            detalleItems.put("nombreRegion", servicio.getRegiones().getNombre());
            detalleItems.put("agenciaId", servicio.getAgencia().getId());
            detalleItems.put("nombreAgencia", servicio.getAgencia().getNombre());
            detalleItems.put("centralId", servicio.getCentrales().getId());
            detalleItems.put("nombreCentral", servicio.getCentrales().getNombre());

            mapaServicios.add(detalleItems);

        }

        return mapaServicios;
    }
    protected List<DetalleCubicadorOrdinarioActas> obtieneListaDetalleMaterialesOrdinarios(int actaId) throws Exception {
        List<DetalleCubicadorOrdinarioActas> respuesta = new ArrayList<DetalleCubicadorOrdinarioActas>();
        try {
            Criteria criteria = sesion.createCriteria(DetalleCubicadorOrdinarioActas.class)
                    .add(Restrictions.eq("id.actaId", actaId))
                    .setFetchMode("cubicador_ordinario", FetchMode.JOIN);
            respuesta = (List<DetalleCubicadorOrdinarioActas>) criteria.list();

        } catch (HibernateException hExcep) {
            log.error("Error al obtener detalle materiales ordinario Gateways ln 1320 metodo obtieneListaDetalleMaterialesOrdinarios");
            throw hExcep;
        }
        return respuesta;
    }
    protected List<CubicadorDetalleUnificadoActas> obtieneListaDetalleActasServiciosUnificado(int actaId) throws Exception {
        List<CubicadorDetalleUnificadoActas> respuesta = new ArrayList<CubicadorDetalleUnificadoActas>();
        try {
            Criteria criteria = sesion.createCriteria(CubicadorDetalleUnificadoActas.class)
                    .add(Restrictions.eq("id.actaId", actaId))
                    .setFetchMode("id.cubicadorDetalleId", FetchMode.JOIN);
            respuesta = (List<CubicadorDetalleUnificadoActas>) criteria.list();

        } catch (HibernateException hExcep) {
            log.error("Error al obtener detalle materiales ordinario Gateways ln 1320 metodo obtieneListaDetalleMaterialesOrdinarios");
            throw hExcep;
        }
        return respuesta;
    }
    protected CubicadorOrdinario  obtenCubicadorOrdinario(int cubicadorId) {
        try {
            CubicadorOrdinario cubicadorOrdinario = new CubicadorOrdinario();
            cubicadorOrdinario = (CubicadorOrdinario) sesion.createCriteria(CubicadorOrdinario.class)
                    .add(Restrictions.eq("id", cubicadorId))
                    .uniqueResult();
            return cubicadorOrdinario;
        } catch (HibernateException ex) {
            log.error("Error al obtener cubicador ordinario. ", ex);
            throw ex;
        }
    }
    protected DetalleCubicadorOrdinarioActas obtenerListadetalleItemsOrdinarioActa(int cubicadorId, int idActa) {
        DetalleCubicadorOrdinarioActasId detalleCubicadorOrdinarioActasId = new DetalleCubicadorOrdinarioActasId();
        DetalleCubicadorOrdinarioActas detalleCubicadorOrdinarioActas = new DetalleCubicadorOrdinarioActas();
        try {

            detalleCubicadorOrdinarioActasId.setActaId(idActa);
            detalleCubicadorOrdinarioActasId.setCubicadorOrdinarioId(cubicadorId);
            detalleCubicadorOrdinarioActas = (DetalleCubicadorOrdinarioActas) sesion.createCriteria(DetalleCubicadorOrdinarioActas.class)
                    .add(Restrictions.eq("id", detalleCubicadorOrdinarioActasId))
                    .uniqueResult();
            if (detalleCubicadorOrdinarioActas == null){
                detalleCubicadorOrdinarioActas.setTotalMontoCubicado(0.0);
                detalleCubicadorOrdinarioActas.setTotalMontoEjecucion(0.0);
                detalleCubicadorOrdinarioActas.setTotalUnidadesCubicadas(0.0);
                detalleCubicadorOrdinarioActas.setTotalUnidadesEjecucionActa(0.0);
                detalleCubicadorOrdinarioActas.setTotalUnidadesEjecucionOt(0.0);
            }
        } catch (HibernateException ex) {

            log.error("Error al obtener DetalleCubicadorOrdinarioActas. ", ex);
        }
        return detalleCubicadorOrdinarioActas;
    }
    protected CubicadorDetalleUnificadoActas obtenerListaCubicacionDetalleActa(int cubicadorId, int idActa) {
        CubicadorDetalleUnificadoActasId cubicadorDetalleUnificadoActasId = new CubicadorDetalleUnificadoActasId();
        CubicadorDetalleUnificadoActas cubicadorDetalleUnificadoActas = new CubicadorDetalleUnificadoActas();
        try {

            cubicadorDetalleUnificadoActasId.setActaId(idActa);
            cubicadorDetalleUnificadoActasId.setCubicadorDetalleId(cubicadorId);
            cubicadorDetalleUnificadoActas = (CubicadorDetalleUnificadoActas) sesion.createCriteria(CubicadorDetalleUnificadoActas.class)
                    .add(Restrictions.eq("id", cubicadorDetalleUnificadoActasId))
                    .uniqueResult();
            if (cubicadorDetalleUnificadoActas == null){
                cubicadorDetalleUnificadoActas.setTotalMontoCubicado(0.0);
                cubicadorDetalleUnificadoActas.setTotalMontoEjecucion(0.0);
                cubicadorDetalleUnificadoActas.setTotalUnidadesCubicadas(0.0);
                cubicadorDetalleUnificadoActas.setTotalUnidadesEjecucionActa(0.0);
                cubicadorDetalleUnificadoActas.setTotalUnidadesEjecucionOt(0.0);
            }
        } catch (HibernateException ex) {

            log.error("Error al obtener DetalleCubicadorOrdinarioActas. ", ex);
        }
        return cubicadorDetalleUnificadoActas;
    }
    protected OtAux obtenerDetalleOt(OtAux ot) throws Exception {
        try {
            Ot otDetalle = (Ot) sesion.createCriteria(Ot.class)
                    //                .setFetchMode("tipoOt", FetchMode.JOIN)
                    //                .setFetchMode("contrato", FetchMode.JOIN)
                    //                .setFetchMode("proveedor", FetchMode.JOIN)
                    //                .setFetchMode("gestor", FetchMode.JOIN)
                    //                .setFetchMode("tipoEstadoOt", FetchMode.JOIN)
                    //                .setFetchMode("estadoWorkflow", FetchMode.JOIN)
                    .add(Restrictions.eq("id", ot.getId()))
                    .uniqueResult();

            OtAux retorno = new OtAux();

//            retorno.setJsonDetalleOt(otDetalle.getJsonDetalleOt());
            // agrego detalle de la Ot: Cubicacion -- Libro de Obras -- Actas -- Pagos
//            Map<String,String> detalleOt = new HashMap<>();
            // detalle OT
//            detalleOt.put("detalleOt", otDetalle.getJsonDetalleOt());
            // Cubicacion
//            List<Cubicador> cubicaciones = obtenerCubicacionPorIdOt(otDetalle.getId());
//            String jsonCubicaciones = generarJsonCubicacio(cubicaciones.get(0));
//            detalleOt.put("cubicacion", jsonCubicaciones);
            // Actas
//            List<Actas> actas = obtenerActasPorOt(otDetalle.getId());
//            if(actas.size()>0){
//                String jsonActas = generarJsonActas(actas);
//                detalleOt.put("actas", jsonActas);
//            }else{
//                detalleOt.put("actas", "[]");
//            }
            // Libro de obras
//            List<LibroObras> librosDeObras = obtenerLibrosDeObras(otDetalle.getId());
//            if(librosDeObras.size()>0){
//                String jsonLibroDeObras = generarJsonLibrosDeObras(librosDeObras);
//                detalleOt.put("librosDeObras", jsonLibroDeObras);
//            }else{
//                detalleOt.put("librosDeObras", "[]");
//            }
//            retorno.setDetalleOt(detalleOt);
            retorno.setId(otDetalle.getId());
            retorno.setParticipacion(ot.getParticipacion());
            retorno.setEjecutar(ot.isEjecutar());
            retorno.setEditar(ot.isEditar());
            retorno.setCurrent(ot.getCurrent());
            retorno.setNombre(otDetalle.getNombre());
            retorno.setTieneTramites(otDetalle.getTieneTramites());

            Proveedores proveedor = new Proveedores();
            proveedor.setId(otDetalle.getProveedor().getId());
            proveedor.setNombre(otDetalle.getProveedor().getNombre());
            retorno.setProveedor(proveedor);

            TipoOt tipoOt = new TipoOt();
            tipoOt.setId(otDetalle.getTipoOt().getId());
            tipoOt.setNombre(otDetalle.getTipoOt().getNombre());
            retorno.setTipoOt(tipoOt);

            Contratos contrato = new Contratos();
            contrato.setId(otDetalle.getContrato().getId());
            contrato.setNombre(otDetalle.getContrato().getNombre());
            retorno.setContrato(contrato);

            Usuarios gestor = new Usuarios();
            gestor.setId(otDetalle.getGestor().getId());
            gestor.setNombres(otDetalle.getGestor().getNombres());
            gestor.setApellidos(otDetalle.getGestor().getApellidos());
            retorno.setGestor(gestor);

            if (otDetalle.getTipoOt().getId() != 2) {
                TipoEstadoOt tipoEstadoOt = new TipoEstadoOt();
                tipoEstadoOt.setId(otDetalle.getTipoEstadoOt().getId());
                tipoEstadoOt.setNombre(otDetalle.getTipoEstadoOt().getNombre());
                retorno.setTipoEstadoOt(tipoEstadoOt);
            }

            retorno.setFechaCreacion(otDetalle.getFechaCreacion());
            retorno.setFechaInicioEstimada(otDetalle.getFechaInicioEstimada());
            retorno.setFechaTerminoEstimada(otDetalle.getFechaTerminoEstimada());

            return retorno;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected Ot obtenerDetalleBasicoOt(int idOt) throws Exception {
        try {
            Ot otDetalle = (Ot) sesion.createCriteria(Ot.class)
                    .add(Restrictions.eq("id", idOt))
                    .uniqueResult();

            Ot retorno = new Ot();
            retorno.setId(otDetalle.getId());
            retorno.setNombre(otDetalle.getNombre());

            Proveedores proveedor = new Proveedores();
            proveedor.setId(otDetalle.getProveedor().getId());
            proveedor.setNombre(otDetalle.getProveedor().getNombre());
            retorno.setProveedor(proveedor);

            TipoOt tipoOt = new TipoOt();
            tipoOt.setId(otDetalle.getTipoOt().getId());
            tipoOt.setNombre(otDetalle.getTipoOt().getNombre());
            retorno.setTipoOt(tipoOt);

            Contratos contrato = new Contratos();
            contrato.setId(otDetalle.getContrato().getId());
            contrato.setNombre(otDetalle.getContrato().getNombre());
            retorno.setContrato(contrato);

            Usuarios gestor = new Usuarios();
            gestor.setId(otDetalle.getGestor().getId());
            gestor.setNombres(otDetalle.getGestor().getNombres());
            gestor.setApellidos(otDetalle.getGestor().getApellidos());
            retorno.setGestor(gestor);

            if (otDetalle.getTipoOt().getId() != 2) {
                TipoEstadoOt tipoEstadoOt = new TipoEstadoOt();
                tipoEstadoOt.setId(otDetalle.getTipoEstadoOt().getId());
                tipoEstadoOt.setNombre(otDetalle.getTipoEstadoOt().getNombre());
                retorno.setTipoEstadoOt(tipoEstadoOt);
            }

            retorno.setFechaCreacion(otDetalle.getFechaCreacion());
            retorno.setFechaInicioEstimada(otDetalle.getFechaInicioEstimada());
            retorno.setFechaTerminoEstimada(otDetalle.getFechaTerminoEstimada());

            return retorno;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<Roles> obtenerRolesPorOtWEE(int otId) throws Exception {

        try {
            Query query = sesion.createSQLQuery(
                    "SELECT * FROM ROLES WHERE ID IN"
                            + "(SELECT DISTINCT(ROL_ID) FROM WORKFLOW_EVENTOS_EJECUCION WHERE OT_ID = :idOt)"
            )
                    .addEntity(Roles.class)
                    .setParameter("idOt", otId);
            List<Roles> result = query.list();
            return result;
        } catch (HibernateException hExcep) {
            log.error("Error al obtener roles desde la entidad workflowEventosEjecucion");
            throw hExcep;
        }
    }

    private String retornarConsultaListarTodos(ObjetoCoreOT objCoreOT) {

        String nombreClaseObjeto = objCoreOT.getClass().getName();
        String cadena1 = "SELECT ";
        String alias = "objCoreOT";
        String cadena2 = "FROM " + nombreClaseObjeto + " as " + alias;
        Field[] atributos = objCoreOT.getClass().getDeclaredFields();
        int contador = 0;

        for (Field campo : atributos) {
            Class claseCampo = campo.getType();

            if (!Set.class.isAssignableFrom(claseCampo)) {
                if (contador > 0) {
                    cadena1 = cadena1 + ",";
                }
                System.out.println("nombreCampo : " + campo.getName());
                String nombreCampo = alias + "." + campo.getName();
                if (ObjetoCoreOT.class.isAssignableFrom(claseCampo)) {
                    nombreCampo = nombreCampo + ".id";
                    cadena2 = cadena2 + " LEFT JOIN FETCH " + alias + "." + campo.getName();
//                System.out.println("El atributo " + campo.getName() + " es un objetoRetorno de tipo ObjetoCoreOT !");
                }
                cadena1 = cadena1 + nombreCampo;
            }
            contador++;
        }
        String cadenaRetorno = cadena2;
        System.out.println(cadenaRetorno);

        return cadenaRetorno;
    }

//    protected List<Roles> listarUsuariosPorRolesProveedor(String idProveedor){
//        List<Roles> listaRetorno = null;
//        String hql = "SELECT usr.usuariosRoles FROM Usuarios usr WHERE usr.id IN"
//                + "(SELECT id FROM Usuarios usr2 WHERE usr2.proveedor.id = " + idProveedor + ")"
//                + " GROUP BY usr.usuariosRoles.id";
////
//        try {
//            Query query = sesion.createQuery(hql);
//            listaRetorno = query.list();
//        } catch (HibernateException hibEx) {
//            log.error("Error al obtener roles por proveedor. ", hibEx);
//        }
//        return listaRetorno;
//    }
    private String retornarConsulta(ObjetoCoreOT objCoreOT) {

        String nombreClaseObjeto = objCoreOT.getClass().getName();
        String cadena1 = "SELECT ";
        String alias = "objCoreOT";
        String cadena2 = "FROM " + nombreClaseObjeto + " as " + alias;
        Field[] atributos = objCoreOT.getClass().getDeclaredFields();
        int contador = 0;

        for (Field campo : atributos) {
            Class claseCampo = campo.getType();

            if (!Set.class.isAssignableFrom(claseCampo)) {
                if (contador > 0) {
                    cadena1 = cadena1 + ",";
                }
                System.out.println("nombreCampo : " + campo.getName());
                String nombreCampo = alias + "." + campo.getName();
                if (ObjetoCoreOT.class.isAssignableFrom(claseCampo)) {
                    nombreCampo = nombreCampo + ".id";
                    cadena2 = cadena2 + " LEFT JOIN FETCH " + alias + "." + campo.getName();
//                System.out.println("El atributo " + campo.getName() + " es un objetoRetorno de tipo ObjetoCoreOT !");
                }
                cadena1 = cadena1 + nombreCampo;
            }
            contador++;
        }
        String cadenaRetorno = cadena2;
        System.out.println(cadenaRetorno);

        return cadenaRetorno;
    }

    protected List<Cubicador> listarCubicacionesPorUsuarioId(int usuarioId) {
        try {
            List<Cubicador> listado = (List<Cubicador>) sesion.createCriteria(Cubicador.class)
                    .add(Restrictions.eq("usuario.id", usuarioId))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones. ", ex);
            throw ex;
        }
    }


    protected List<Cubicador> listarCubicacionesPorUsuarioIdPaginado(int usuarioId, int pos, int cantidad, String filtro, String orden) {
        try {

//            List<Cubicador> listado = (List<Cubicador>) sesion.createCriteria(Cubicador.class)
            Criteria cr = sesion.createCriteria(Cubicador.class);
            cr.add(Restrictions.eq("usuario.id", usuarioId));
            cr.setFirstResult(pos);
            cr.setMaxResults(cantidad);
            if(orden.equals("+")){
                cr.addOrder(Order.asc(filtro));
            }else{
                cr.addOrder(Order.desc(filtro));
            }

            List<Cubicador> listado = cr.list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones. ", ex);
            throw ex;
        }
    }

    protected Cubicador obtenerDetalleCubicacion(int cubicacionId) {
        Cubicador cubicacion = new Cubicador();
        try {
            Cubicador cubi = (Cubicador) sesion.createCriteria(Cubicador.class)
                    .add(Restrictions.eq("id", cubicacionId))
                    .setFetchMode("proveedor", FetchMode.JOIN)
                    .setFetchMode("contrato", FetchMode.JOIN)
                    .setFetchMode("cubicadorServicios", FetchMode.JOIN)
                    .setFetchMode("cubicadorMateriales", FetchMode.JOIN)
                    .setFetchMode("cubicadorOrdinario", FetchMode.JOIN)
                    .setFetchMode("cubicadorUnificado", FetchMode.JOIN)
                    .uniqueResult();

            cubicacion.setId(cubi.getId());
            cubicacion.setNombre(cubi.getNombre());
            if (cubi.getContrato().getId() != 9) {
                Proveedores proveAux = new Proveedores();
                Proveedores prove = (Proveedores) retornarObjetoCoreOT(Proveedores.class, cubi.getProveedor().getId());
                proveAux.setId(prove.getId());
                proveAux.setNombre(prove.getNombre());
                cubicacion.setProveedor(proveAux);
            }
            cubicacion.setDescripcion(cubi.getDescripcion());
            Contratos contratoAux = new Contratos();
            Contratos contrato = (Contratos) retornarObjetoCoreOT(Contratos.class, cubi.getContrato().getId());
            contratoAux.setId(contrato.getId());
            contratoAux.setNombre(contrato.getNombre());
            cubicacion.setContrato(contratoAux);

            Set<CubicadorOrdinario> cubicacionOrdinario = new HashSet<>();
            Set<CubicadorServicios> cubicacionServicios = new HashSet<>();
            Set<CubicadorMateriales> cubicacionMateriales = new HashSet<>();

            if(cubi.getContrato().getId() == 1 || cubi.getContrato().getId() == 3 || cubi.getContrato().getId() == 5 || cubi.getContrato().getId() == 9 || cubi.getContrato().getId() == 10 || cubi.getContrato().getId() == 11){
                Regiones region = (Regiones)retornarObjetoCoreOT(Regiones.class, cubi.getRegion().getId());
                Regiones regionAxu = new Regiones(region.getId(),region.getCodigo(),region.getNombre());
                cubicacion.setRegion(regionAxu);
            }

            if (cubi.getContrato().getId() == 2) {
                Set<CubicadorOrdinario> cubiOrdinario = cubi.getCubicadorOrdinario();
                for (CubicadorOrdinario cubAuxOrdinario : cubiOrdinario) {
                    CubicadorOrdinario cubicacionOrdinaria = new CubicadorOrdinario();
                    cubicacionOrdinaria.setId(cubAuxOrdinario.getId());
                    cubicacionOrdinaria.setPrecio(cubAuxOrdinario.getPrecio());
                    cubicacionOrdinaria.setCantidad(cubAuxOrdinario.getCantidad());
                    cubicacionOrdinaria.setItem(cubAuxOrdinario.getItem());
                    TipoMoneda tipoMonedaAux = cubAuxOrdinario.getTipoMoneda();
                    TipoMoneda tipoMoneda = new TipoMoneda(tipoMonedaAux.getId(), tipoMonedaAux.getNombre(), tipoMonedaAux.getDescripcion(), tipoMonedaAux.getEstado());
                    cubicacionOrdinaria.setTipoMoneda(tipoMoneda);
                    cubicacionOrdinaria.setTotal(cubAuxOrdinario.getTotal());
                    cubicacionOrdinario.add(cubicacionOrdinaria);
                }
                cubicacion.setCubicadorOrdinario(cubicacionOrdinario);

            } else if (cubi.getContrato().getId() == 4){
                Set<CubicadorDetalle> cubicadorDetalleLista = new HashSet<>();
//                cubicadorDetalleLista = obtenerCubicadorDetallePorIdCubicador(cubicacionId);
                for (CubicadorDetalle cubicadorDetalle : cubi.getCubicadorServiciosUnificado()){
                    TipoMoneda tipoMoneda = (TipoMoneda) sesion.createCriteria(TipoMoneda.class)
                            .add(Restrictions.eq("id", cubicadorDetalle.getTipoMonedaId()))
                            .uniqueResult();
                    tipoMoneda.setMaterialeses(null);

                    Servicios servicios = (Servicios) sesion.createCriteria(Servicios.class)
                            .add(Restrictions.eq("id", cubicadorDetalle.getServicios().getId()))
                            .setFetchMode("tipoServicio", FetchMode.JOIN)
                            .setFetchMode("tipoUnidadMedida", FetchMode.JOIN)
                            .uniqueResult();

                    Regiones region = (Regiones) sesion.createCriteria(Regiones.class)
                            .add(Restrictions.eq("id", cubicadorDetalle.getRegionId()))
                            .uniqueResult();

                    Centrales centrales = (Centrales) sesion.createCriteria(Centrales.class)
                            .add(Restrictions.eq("id", cubicadorDetalle.getCentralId()))
                            .uniqueResult();

                    Agencias agencias = (Agencias) sesion.createCriteria(Agencias.class)
                            .add(Restrictions.eq("id", cubicadorDetalle.getAgenciaId()))
                            .uniqueResult();

                    TipoUnidadMedida tipoUnidadMedida = new TipoUnidadMedida();
                    TipoUnidadMedida tipoUnidadMedidaProx = new TipoUnidadMedida();
                    tipoUnidadMedidaProx = (TipoUnidadMedida) retornarObjetoCoreOT2(TipoUnidadMedida.class, servicios.getTipoUnidadMedida().getId());
                    tipoUnidadMedida = new TipoUnidadMedida(tipoUnidadMedidaProx.getId(), tipoUnidadMedidaProx.getNombre(), tipoUnidadMedidaProx.getCodigo(), tipoUnidadMedidaProx.getEstado());

                    cubicadorDetalle.setTipoUnidadMedida(tipoUnidadMedida);
                    cubicadorDetalle.setRegiones(region);
                    cubicadorDetalle.setCentrales(centrales);
                    cubicadorDetalle.setAgencia(agencias);
                    cubicadorDetalle.setServicios(servicios);
                    cubicadorDetalle.setTipoMoneda(tipoMoneda);

                    if (cubicadorDetalle != null) {
                        validarYSetearObjetoARetornar(cubicadorDetalle, null);
                    }

                    cubicadorDetalleLista.add(cubicadorDetalle);
                }
                cubicacion.setCubicadorServiciosUnificado(cubicadorDetalleLista);
            } else if (cubi.getContrato().getId() == 9) {

                CubicadorExtrasBucle cubicadorExtrasBucle = (CubicadorExtrasBucle) sesion.createCriteria(CubicadorExtrasBucle.class)
                        .add(Restrictions.eq("cubicadorId", cubi.getId()))
                        .uniqueResult();

                cubicacion.setOtros(cubicadorExtrasBucle);

                double numeroServicio = 1;
                List<CubicadorServiciosBucle> cubicadorServiciosBucle = (List<CubicadorServiciosBucle>) sesion.createCriteria(CubicadorServiciosBucle.class)
                        .add(Restrictions.eq("cubicadorId", cubi.getId()))
                        .setFetchMode("direccion", FetchMode.JOIN).list();

                Set<CubicadorServiciosServiciosUnidadBucleResponse> cubicadorServiciosBucleResponse = new HashSet<>();

                for (CubicadorServiciosBucle cServicioB : cubicadorServiciosBucle) {

                    List<CubicadorServiciosServiciosUnidad> cubicadorServiciosServiciosUnidad = (List<CubicadorServiciosServiciosUnidad>) sesion.createCriteria(CubicadorServiciosServiciosUnidad.class)
                            .add(Restrictions.eq("cubicadorServicioBucleId", cServicioB.getId())).list();

                    Servicios servicio = (Servicios) sesion.createCriteria(Servicios.class)
                            .add(Restrictions.eq("id", cServicioB.getServicioId()))
                            .uniqueResult();

                    TipoUnidadMedida tipoUnidadMedida = (TipoUnidadMedida) sesion.createCriteria(TipoUnidadMedida.class)
                            .add(Restrictions.eq("id", servicio.getTipoUnidadMedida().getId()))
                            .uniqueResult();

                    Actividades actividad = (Actividades) sesion.createCriteria(Actividades.class)
                            .add(Restrictions.eq("id", cServicioB.getActividadId()))
                            .uniqueResult();

                    Especialidades especialidad = (Especialidades) sesion.createCriteria(Especialidades.class)
                            .add(Restrictions.eq("id", cServicioB.getEspecialidadId()))
                            .uniqueResult();


                    if (cubicadorServiciosServiciosUnidad.isEmpty()) {
                        CubicadorServiciosServiciosUnidadBucleResponse cServicioBReponse = new CubicadorServiciosServiciosUnidadBucleResponse();

                        //numero
                        cServicioBReponse.setNumero(numeroServicio);
                        numeroServicio++;

                        //Datos Servicios
                        cServicioBReponse.setDescripcionServicio(servicio.getCodigo() + " - " + servicio.getDescripcion());
                        cServicioBReponse.setPuntosBaremos(servicio.getPuntosBaremos());
                        cServicioBReponse.setCantidadServicio(cServicioB.getCantidad());
                        cServicioBReponse.setTotalServicio(cServicioB.getTotal());
                        cServicioBReponse.setPrecioServicio(cServicioB.getPrecio() / servicio.getPuntosBaremos());
                        cServicioBReponse.setUnidadMedidaServicio(tipoUnidadMedida.getNombre());

                        //Dirección
                        cServicioBReponse.setDireccion(cServicioB.getDireccion());

                        //Actividad y Especialidad
                        cServicioBReponse.setActividadNombre(actividad.getNombre());
                        cServicioBReponse.setEspecialidadNombre(especialidad.getNombre());
                        //Datos Finales
                        cServicioBReponse.setTotalFinal(cServicioBReponse.getTotalServicio());
                        //Datos Servicio Unidad
                        cServicioBReponse.setDescripcionServicioUnidad("Sin S.U");
                        cServicioBReponse.setUnidadMedidaServicioUnidad("Sin S.U");
                        cubicadorServiciosBucleResponse.add(cServicioBReponse);


                    } else if (cubicadorServiciosServiciosUnidad.size() > 0) {

                        double numeroServicioUnidad = 1;
                        for (CubicadorServiciosServiciosUnidad cServicioUnidadB : cubicadorServiciosServiciosUnidad) {
                            CubicadorServiciosServiciosUnidadBucleResponse cServicioBReponse = new CubicadorServiciosServiciosUnidadBucleResponse();
                            //numero
                            cServicioBReponse.setNumero(numeroServicio + numeroServicioUnidad / 10);
                            numeroServicioUnidad++;

                            ServiciosUnidad servicioUnidad = (ServiciosUnidad) sesion.createCriteria(ServiciosUnidad.class)
                                    .add(Restrictions.eq("id", cServicioUnidadB.getServicioUnidadId()))
                                    .uniqueResult();

                            TipoUnidadMedida tipoUnidadMedidaServicioUnidad = (TipoUnidadMedida) sesion.createCriteria(TipoUnidadMedida.class)
                                    .add(Restrictions.eq("id", servicioUnidad.getTipoUnidadMedidaId()))
                                    .uniqueResult();

                            //Datos Servicios
                            cServicioBReponse.setDescripcionServicio(servicio.getCodigo() + " - " + servicio.getDescripcion());

                            //Significa que es el primero.
                            if (cServicioBReponse.getNumero() == 1.1) {
                                cServicioBReponse.setPuntosBaremos(servicio.getPuntosBaremos());
                                cServicioBReponse.setCantidadServicio(cServicioB.getCantidad());
                                cServicioBReponse.setTotalServicio(cServicioB.getTotal());
                                cServicioBReponse.setPrecioServicio(cServicioB.getPrecio() / servicio.getPuntosBaremos());
                            }
                            cServicioBReponse.setUnidadMedidaServicio(tipoUnidadMedida.getNombre());

                            //Actividad y Especialidad
                            cServicioBReponse.setActividadNombre(actividad.getNombre());
                            cServicioBReponse.setEspecialidadNombre(especialidad.getNombre());

                            //Dirección
                            cServicioBReponse.setDireccion(cServicioB.getDireccion());

                            //Datos ServiciosUnidad
                            cServicioBReponse.setDescripcionServicioUnidad(servicioUnidad.getCodigo() + " - " + servicioUnidad.getDescripcion());
                            cServicioBReponse.setCantidadServicioUnidad(cServicioUnidadB.getCantidad());
                            cServicioBReponse.setPrecioServicioUnidad(cServicioUnidadB.getPrecio());
                            cServicioBReponse.setTotalServicioUnidad(cServicioUnidadB.getTotal());
                            cServicioBReponse.setUnidadMedidaServicioUnidad(tipoUnidadMedidaServicioUnidad.getNombre());

//                            Datos Materiales

                            List<MaterialesBucleResponse> materialesBucleCTC = new ArrayList<MaterialesBucleResponse>();
                            List<MaterialesBucleResponse> materialesBucleEECC = new ArrayList<MaterialesBucleResponse>();

                            List<CubicadorMaterialesBucle> cubicadorMaterialesBucle = (List<CubicadorMaterialesBucle>) sesion.createCriteria(CubicadorMaterialesBucle.class)
                                    .add(Restrictions.eq("cubicadorServiciosServiciosUnidadId", cServicioUnidadB.getId())).list();

                            for (CubicadorMaterialesBucle cMaterailesB : cubicadorMaterialesBucle) {
                                MaterialesBucleResponse materialB = new MaterialesBucleResponse();
                                Materiales materialesRaw = new Materiales();
                                CubicadorMaterialesBucle materialesCubicado = new CubicadorMaterialesBucle();

                                Criteria criteriaMaterial = sesion.createCriteria(Materiales.class)
                                        .setFetchMode("tipoMoneda", FetchMode.JOIN)
                                        .setFetchMode("tipoUnidadMedida", FetchMode.JOIN);
                                criteriaMaterial.setFetchMode("tipoUnidadMedida", FetchMode.JOIN);
                                criteriaMaterial.add(Restrictions.eq("codigo", cMaterailesB.getCodigoMaterial()));
                                materialesRaw = (Materiales) criteriaMaterial.uniqueResult();

                                Criteria criteriaMaterialBucle = sesion.createCriteria(CubicadorMaterialesBucle.class);
                                criteriaMaterialBucle.add(Restrictions.eq("codigoMaterial", cMaterailesB.getCodigoMaterial()));
                                criteriaMaterialBucle.add(Restrictions.eq("cubicadorServiciosServiciosUnidadId", cMaterailesB.getCubicadorServiciosServiciosUnidadId()));
                                materialesCubicado = (CubicadorMaterialesBucle) criteriaMaterialBucle.uniqueResult();

                                materialesRaw.setDistribuidor(cMaterailesB.getDistribuidor());

                                materialB.setId(materialesRaw.getId());
                                materialB.setDistribuidor(cMaterailesB.getDistribuidor());
                                materialB.setCodigo(cMaterailesB.getCodigoMaterial());
                                materialB.setCodigoUnidadDeObra(cMaterailesB.getCodigoUnidadObra());
                                materialB.setCodigoSap(materialesRaw.getCodigoSap());
                                materialB.setDescripcion(materialesRaw.getDescripcion());
                                materialB.setNombre(materialesRaw.getNombre());
                                materialB.setPrecio((double) Math.round(cMaterailesB.getPrecio()*materialesCubicado.getCantidadMaterial() * 100) / 100);
                                materialB.setPrecioPesos(retornarPrecioEnPesosChilenos(materialB.getPrecio(),cMaterailesB.getTipoMonedaId()));


                                TipoMoneda tipoMoneda = (TipoMoneda) sesion.createCriteria(TipoMoneda.class)
                                        .add(Restrictions.eq("id", cMaterailesB.getTipoMonedaId()))
                                        .uniqueResult();
                                materialB.setTipoMoneda(tipoMoneda.getNombre());
                                materialB.setTipoMonedaId(cMaterailesB.getTipoMonedaId());

                                materialB.setTipoCambio(retornarValorMonedaEnPesos(cMaterailesB.getTipoMonedaId()));

                                materialB.setUnidadMedida(materialesRaw.getTipoUnidadMedida().getNombre());
                                if ("CTC".equalsIgnoreCase(materialB.getDistribuidor().trim())) {

                                    materialesBucleCTC.add(materialB);

                                } else {

                                    materialesBucleEECC.add(materialB);
                                }
                            }
                            cServicioBReponse.setMaterialesCTCResponse(materialesBucleCTC);
                            cServicioBReponse.setMaterialesEECCResponse(materialesBucleEECC);
                            //Datos Finales
                            cServicioBReponse.setTotalFinal(cServicioBReponse.getTotalServicio() + cServicioBReponse.getTotalServicioUnidad());


                            cubicadorServiciosBucleResponse.add(cServicioBReponse);
                        }
                        numeroServicioUnidad = 1;
                        numeroServicio++;
                    }

                }

                cubicacion.setCubicadorServiciosServiciosUnidadBucleResponse(cubicadorServiciosBucleResponse);
            } else {
                Set<CubicadorServicios> cubiServicios = cubi.getCubicadorServicios();
                for (CubicadorServicios cubServiciosAux : cubiServicios) {
                    CubicadorServicios cubicacionServicio = new CubicadorServicios();
                    cubicacionServicio.setId(cubServiciosAux.getId());
                    cubicacionServicio.setCantidad(cubServiciosAux.getCantidad());
                    cubicacionServicio.setTotal(cubServiciosAux.getTotal());
                    cubicacionServicio.setPrecio(cubServiciosAux.getPrecio());
                    Servicios servicioAux = new Servicios();
                    Servicios servicio = (Servicios) retornarObjetoCoreOT(Servicios.class, cubServiciosAux.getId().getServicioId());
                    servicioAux.setId(servicio.getId());
                    servicioAux.setNombre(servicio.getNombre());
                    servicioAux.setPrecio(cubServiciosAux.getPrecio());
                    servicioAux.setDescripcion(servicio.getDescripcion());
                    cubicacionServicio.setServicio(servicioAux);
                    cubicacionServicios.add(cubicacionServicio);
                }
                cubicacion.setCubicadorServicios(cubicacionServicios);

                Set<CubicadorMateriales> cubiMateriales = cubi.getCubicadorMateriales();
                for (CubicadorMateriales cubMaterialAux : cubiMateriales) {
                    CubicadorMateriales cubicacionMaterial = new CubicadorMateriales();
                    cubicacionMaterial.setId(cubMaterialAux.getId());
                    cubicacionMaterial.setCantidad(cubMaterialAux.getCantidad());
                    cubicacionMaterial.setMontoTotal(cubMaterialAux.getMontoTotal());
                    Materiales materialesAux = new Materiales();
                    Materiales materiales = (Materiales) retornarObjetoCoreOT(Servicios.class, cubMaterialAux.getId().getMaterialId());
                    materialesAux.setId(materiales.getId());
                    materialesAux.setNombre(materiales.getNombre());
                    cubicacionMaterial.setMaterial(materialesAux);
                    cubicacionMateriales.add(cubicacionMaterial);
                }
                cubicacion.setCubicadorMateriales(cubicacionMateriales);

            }

        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones. ", ex);
            throw ex;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cubicacion;
    }


    protected Cubicador obtenerDetalleCubicacionLLave(int cubicacionId, int otId) {
        Cubicador cubicacion = new Cubicador();
        try {
            Cubicador cubi = (Cubicador) sesion.createCriteria(Cubicador.class)
                    .add(Restrictions.eq("id", cubicacionId))
                    .setFetchMode("proveedor", FetchMode.JOIN)
                    .setFetchMode("contrato", FetchMode.JOIN)
                    .setFetchMode("cubicadorServicios", FetchMode.JOIN)
                    .setFetchMode("cubicadorMateriales", FetchMode.JOIN)
                    .setFetchMode("cubicadorOrdinario", FetchMode.JOIN)
                    .setFetchMode("cubicadorUnificado", FetchMode.JOIN)
                    .uniqueResult();

            cubicacion.setId(cubi.getId());
            cubicacion.setNombre(cubi.getNombre());
            Proveedores proveAux = new Proveedores();
            Proveedores prove = (Proveedores) retornarObjetoCoreOT(Proveedores.class, cubi.getProveedor().getId());
            proveAux.setId(prove.getId());
            proveAux.setNombre(prove.getNombre());
            cubicacion.setProveedor(proveAux);
            cubicacion.setDescripcion(cubi.getDescripcion());
            Contratos contratoAux = new Contratos();
            Contratos contrato = (Contratos) retornarObjetoCoreOT(Contratos.class, cubi.getContrato().getId());
            contratoAux.setId(contrato.getId());
            contratoAux.setNombre(contrato.getNombre());
            cubicacion.setContrato(contratoAux);

            Set<CubicadorOrdinario> cubicacionOrdinario = new HashSet<>();
            Set<CubicadorServicios> cubicacionServicios = new HashSet<>();
            Set<CubicadorMateriales> cubicacionMateriales = new HashSet<>();

            Regiones region = (Regiones) retornarObjetoCoreOT(Regiones.class, cubi.getRegion().getId());
            Regiones regionAxu = new Regiones(region.getId(), region.getCodigo(), region.getNombre());
            cubicacion.setRegion(regionAxu);

            Set<CubicadorServicios> cubiServicios = cubi.getCubicadorServicios();
            for (CubicadorServicios cubServiciosAux : cubiServicios) {
                CubicadorServicios cubicacionServicio = new CubicadorServicios();
                cubicacionServicio.setId(cubServiciosAux.getId());
                cubicacionServicio.setCantidad(cubServiciosAux.getCantidad());
                cubicacionServicio.setTotal(cubServiciosAux.getTotal());
                cubicacionServicio.setPrecio(cubServiciosAux.getPrecio());
                Servicios servicioAux = new Servicios();
                Servicios servicio = (Servicios) retornarObjetoCoreOT(Servicios.class, cubServiciosAux.getId().getServicioId());
                servicioAux.setId(servicio.getId());
                servicioAux.setNombre(servicio.getNombre());
                servicioAux.setPrecio(cubServiciosAux.getPrecio());
                servicioAux.setDescripcion(servicio.getDescripcion());
                cubicacionServicio.setServicio(servicioAux);
                cubicacionServicios.add(cubicacionServicio);
            }
            cubicacion.setCubicadorServicios(cubicacionServicios);

            Actas actaFinal = new Actas();
            List<Actas> listado = (List<Actas>) sesion.createCriteria(Actas.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .setFetchMode("ot", FetchMode.JOIN)
                    .setFetchMode("detalleMaterialesActas", FetchMode.JOIN)
                    .setFetchMode("detalleServiciosActas", FetchMode.JOIN)
                    .setFetchMode("detalleMaterialesAdicionalesActas", FetchMode.JOIN)
                    .setFetchMode("detalleServiciosAdicionalesActas", FetchMode.JOIN)
                    .list();
            int idActaMayor = 0;
            for (Actas acta : listado) {
                int idActa = acta.getId();
                if (idActa > idActaMayor) {
                    idActaMayor = idActa;
                    Set<DetalleMaterialesAdicionalesActas> setDetalleMaterialesAdicionalesActas = acta.getDetalleMaterialesAdicionalesActas();
                    if (setDetalleMaterialesAdicionalesActas.size() > 0) {
                        Set<DetalleMaterialesAdicionalesActas> listaDetalleMaterialesAdicionalesActas = new HashSet<>();
                        for (DetalleMaterialesAdicionalesActas detalle : setDetalleMaterialesAdicionalesActas) {
                            DetalleMaterialesAdicionalesActas detalleMaterialesActasAdicionalesFinal = new DetalleMaterialesAdicionalesActas();
                            Materiales material = new Materiales();
                            material.setId(detalle.getMateriales().getId());
                            material.setNombre(detalle.getMateriales().getNombre());
                            detalleMaterialesActasAdicionalesFinal.setMateriales(material);
                            detalleMaterialesActasAdicionalesFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                            detalleMaterialesActasAdicionalesFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());
                            listaDetalleMaterialesAdicionalesActas.add(detalleMaterialesActasAdicionalesFinal);
                        }
                        actaFinal.setDetalleMaterialesAdicionalesActas(listaDetalleMaterialesAdicionalesActas);
                    }
                }

            }

            Set<DetalleMaterialesAdicionalesActas> cubiMateriales = actaFinal.getDetalleMaterialesAdicionalesActas();
            for (DetalleMaterialesAdicionalesActas cubMaterialAux : cubiMateriales) {
                CubicadorMateriales cubicacionMaterial = new CubicadorMateriales();
                CubicadorMaterialesId cubicadorMaterialesId = new CubicadorMaterialesId();
                cubicadorMaterialesId.setMaterialId(cubMaterialAux.getMateriales().getId());
                cubicacionMaterial.setId(cubicadorMaterialesId);
                cubicacionMaterial.setCantidad(cubMaterialAux.getTotalUnidadesEjecucionActa());
                Materiales materialesAux = new Materiales();
                Materiales materiales = (Materiales) retornarObjetoCoreOT(Materiales.class, cubMaterialAux.getMateriales().getId());
                materialesAux.setId(materiales.getId());
                materialesAux.setNombre(materiales.getNombre());
                cubicacionMaterial.setMaterial(materialesAux);
                cubicacionMateriales.add(cubicacionMaterial);
            }
            cubicacion.setCubicadorMateriales(cubicacionMateriales);


        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones. ", ex);
            throw ex;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cubicacion;
    }


    protected List<CubicadorDetalle> obtenerCubicadorDetallePorIdCubicador(int idCubicacion) {
        try {
            List<CubicadorDetalle> cubicadorDetalleListFinal = new ArrayList<CubicadorDetalle>();
            List<CubicadorDetalle> cubicadorDetalleList = (List<CubicadorDetalle>) sesion.createCriteria(CubicadorDetalle.class)
                    .add(Restrictions.eq("cubicadorId", idCubicacion))
                    .list();
            for (CubicadorDetalle cubicadorDetalleAux : cubicadorDetalleList) {
                Regiones regiones = (Regiones) sesion.createCriteria(Regiones.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getRegionId()))
                        .uniqueResult();
                Agencias agencias = (Agencias) sesion.createCriteria(Agencias.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getAgenciaId()))
                        .uniqueResult();
                Centrales centrales = (Centrales) sesion.createCriteria(Centrales.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getCentralId()))
                        .uniqueResult();
                TipoMoneda tipoMoneda = (TipoMoneda) sesion.createCriteria(TipoMoneda.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getTipoMonedaId()))
                        .uniqueResult();

                tipoMoneda.setMaterialeses(null);

                Servicios servicios = (Servicios) sesion.createCriteria(Servicios.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getServicios().getId()))
                        .uniqueResult();
                servicios.setManosDeObrasServicioses(null);
                servicios.setProveedoresServicioses(null);
                servicios.setServiciosMaterialeses(null);

                TipoServicio tipoServicio = (TipoServicio) sesion.createCriteria(TipoServicio.class)
                        .add(Restrictions.eq("id", servicios.getTipoServicio().getId()))
                        .uniqueResult();
                tipoServicio.setServicioses(null);

                TipoUnidadMedida tipoUnidadMedida = (TipoUnidadMedida) sesion.createCriteria(TipoUnidadMedida.class)
                        .add(Restrictions.eq("id", servicios.getTipoUnidadMedida().getId()))
                        .uniqueResult();
                tipoUnidadMedida.setMaterialeses(null);
                tipoUnidadMedida.setServicioses(null);

                servicios.setTipoServicio(tipoServicio);
                servicios.setTipoUnidadMedida(tipoUnidadMedida);

                cubicadorDetalleAux.setServicios(servicios);

                cubicadorDetalleAux.setRegiones(regiones);
                cubicadorDetalleAux.setAgencia(agencias);
                cubicadorDetalleAux.setCentrales(centrales);
                cubicadorDetalleAux.setTipoMoneda(tipoMoneda);
                cubicadorDetalleListFinal.add(cubicadorDetalleAux);
            }
            return cubicadorDetalleList;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicador detalle. ", ex);
            throw ex;
        }
    }
    protected CubicadorDetalle obtenerUnicoCubicadorDetallePorIdCubicador(int idCubicacion) {
        try {
            CubicadorDetalle cubicadorDetalle = (CubicadorDetalle) sesion.createCriteria(CubicadorDetalle.class)
                    .add(Restrictions.eq("id", idCubicacion))
                    .setFetchMode("servicios", FetchMode.JOIN)
                    .uniqueResult();
                Regiones regiones = (Regiones) sesion.createCriteria(Regiones.class)
                        .add(Restrictions.eq("id", cubicadorDetalle.getRegionId()))
                        .uniqueResult();
                Agencias agencias = (Agencias) sesion.createCriteria(Agencias.class)
                        .add(Restrictions.eq("id", cubicadorDetalle.getAgenciaId()))
                        .uniqueResult();
                Centrales centrales = (Centrales) sesion.createCriteria(Centrales.class)
                        .add(Restrictions.eq("id", cubicadorDetalle.getCentralId()))
                        .uniqueResult();
                TipoMoneda tipoMoneda = (TipoMoneda) sesion.createCriteria(TipoMoneda.class)
                        .add(Restrictions.eq("id", cubicadorDetalle.getTipoMonedaId()))
                        .uniqueResult();
                tipoMoneda.setMaterialeses(null);
            Servicios servicios = cubicadorDetalle.getServicios();
            servicios.setManosDeObrasServicioses(null);
            servicios.setProveedoresServicioses(null);
            servicios.setServiciosMaterialeses(null);

            TipoServicio tipoServicio = (TipoServicio) sesion.createCriteria(TipoServicio.class)
                    .add(Restrictions.eq("id", servicios.getTipoServicio().getId()))
                    .uniqueResult();
            tipoServicio.setServicioses(null);

            TipoUnidadMedida tipoUnidadMedida = (TipoUnidadMedida) sesion.createCriteria(TipoUnidadMedida.class)
                    .add(Restrictions.eq("id", servicios.getTipoUnidadMedida().getId()))
                    .uniqueResult();
            tipoUnidadMedida.setMaterialeses(null);

            servicios.setTipoServicio(tipoServicio);
            servicios.setTipoUnidadMedida(tipoUnidadMedida);

            cubicadorDetalle.setServicios(servicios);
            cubicadorDetalle.setRegiones(regiones);
            cubicadorDetalle.setAgencia(agencias);
            cubicadorDetalle.setCentrales(centrales);
            cubicadorDetalle.setTipoMoneda(tipoMoneda);
            return cubicadorDetalle;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicador detalle. ", ex);
            throw ex;
        }
    }

    protected List<CubicadorDetalleAdicionales> obtenerUnicoCubicadorDetalleAdicionalPorCubicadorIdValidacion(int idCubicacion) {
        try {
            List<CubicadorDetalleAdicionales> respuesta = new ArrayList<CubicadorDetalleAdicionales>();
            List<CubicadorDetalleAdicionales> cubicadorDetalleAdicionalesLista = (List<CubicadorDetalleAdicionales>) sesion.createCriteria(CubicadorDetalleAdicionales.class)
                    .add(Restrictions.eq("cubicadorId", idCubicacion))
//                    .add(Restrictions.eq("validacionSistema", validacion))
                    .setFetchMode("servicios", FetchMode.JOIN)
                    .list();
            for(CubicadorDetalleAdicionales cubicadorDetalleAdicionales : cubicadorDetalleAdicionalesLista ) {
                Regiones regiones = (Regiones) sesion.createCriteria(Regiones.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAdicionales.getRegionId()))
                        .uniqueResult();
                Agencias agencias = (Agencias) sesion.createCriteria(Agencias.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAdicionales.getAgenciaId()))
                        .uniqueResult();
                Centrales centrales = (Centrales) sesion.createCriteria(Centrales.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAdicionales.getCentralId()))
                        .uniqueResult();
                TipoMoneda tipoMoneda = (TipoMoneda) sesion.createCriteria(TipoMoneda.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAdicionales.getTipoMonedaId()))
                        .uniqueResult();
                tipoMoneda.setMaterialeses(null);

                Servicios servicios = (Servicios) sesion.createCriteria(Servicios.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAdicionales.getServicioId()))
                        .setFetchMode("tipoServicio", FetchMode.JOIN)
                        .setFetchMode("tipoUnidadMedida", FetchMode.JOIN)
                        .uniqueResult();

                servicios.setManosDeObrasServicioses(null);
                servicios.setProveedoresServicioses(null);
                servicios.setServiciosMaterialeses(null);

                TipoServicio tipoServicio = (TipoServicio) sesion.createCriteria(TipoServicio.class)
                        .add(Restrictions.eq("id", servicios.getTipoServicio().getId()))
                        .uniqueResult();
                tipoServicio.setServicioses(null);

                TipoUnidadMedida tipoUnidadMedida = (TipoUnidadMedida) sesion.createCriteria(TipoUnidadMedida.class)
                        .add(Restrictions.eq("id", servicios.getTipoUnidadMedida().getId()))
                        .uniqueResult();
                tipoUnidadMedida.setMaterialeses(null);

                servicios.setTipoServicio(tipoServicio);
                servicios.setTipoUnidadMedida(tipoUnidadMedida);

                cubicadorDetalleAdicionales.setServicios(servicios);
                cubicadorDetalleAdicionales.setRegiones(regiones);
                cubicadorDetalleAdicionales.setAgencia(agencias);
                cubicadorDetalleAdicionales.setCentrales(centrales);
                cubicadorDetalleAdicionales.setTipoMoneda(tipoMoneda);

                respuesta.add(cubicadorDetalleAdicionales);
            }
            return respuesta;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicador detalle. ", ex);
            throw ex;
        }
    }
    protected CubicadorDetalleAdicionales obtenerUnicoCubicadorDetalleAdicionalesPorIdCubicador(int idCubicacion) {
        try {
            CubicadorDetalleAdicionales cubicadorDetalle = (CubicadorDetalleAdicionales) sesion.createCriteria(CubicadorDetalleAdicionales.class)
                    .add(Restrictions.eq("id", idCubicacion))
                    .uniqueResult();
            Servicios servicios = (Servicios) sesion.createCriteria(Servicios.class)
                    .add(Restrictions.eq("id", cubicadorDetalle.getServicioId()))
                    .uniqueResult();
            Regiones regiones = (Regiones) sesion.createCriteria(Regiones.class)
                    .add(Restrictions.eq("id", cubicadorDetalle.getRegionId()))
                    .uniqueResult();
            Agencias agencias = (Agencias) sesion.createCriteria(Agencias.class)
                    .add(Restrictions.eq("id", cubicadorDetalle.getAgenciaId()))
                    .uniqueResult();
            Centrales centrales = (Centrales) sesion.createCriteria(Centrales.class)
                    .add(Restrictions.eq("id", cubicadorDetalle.getCentralId()))
                    .uniqueResult();
            TipoMoneda tipoMoneda = (TipoMoneda) sesion.createCriteria(TipoMoneda.class)
                    .add(Restrictions.eq("id", cubicadorDetalle.getTipoMonedaId()))
                    .uniqueResult();
            Alcances alcances = (Alcances) sesion.createCriteria(Alcances.class)
                    .add(Restrictions.eq("codigo", servicios.getCodAlcance()))
                    .uniqueResult();

            cubicadorDetalle.setServicios(servicios);
            cubicadorDetalle.setRegiones(regiones);
            cubicadorDetalle.setAgencia(agencias);
            cubicadorDetalle.setCentrales(centrales);
            cubicadorDetalle.setTipoMoneda(tipoMoneda);
            cubicadorDetalle.setAlcances(alcances);
            return cubicadorDetalle;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicador detalle. ", ex);
            throw ex;
        }
    }
    protected CubicadorDetalleUnificadoActas obtieneCubicadorDetalleUnificadoActa(int cubDetalleServicio, int idActa) throws Exception {
        Serializable idRegistro = 0;
        CubicadorDetalleUnificadoActas respuesta = new CubicadorDetalleUnificadoActas();
        try {
            CubicadorDetalleUnificadoActas detalleCubicadorOrdinarioActasAux = new CubicadorDetalleUnificadoActas();
            CubicadorDetalleUnificadoActasId cubicadorDetalleUnificadoActasId = new CubicadorDetalleUnificadoActasId();
            cubicadorDetalleUnificadoActasId.setActaId(idActa);
            cubicadorDetalleUnificadoActasId.setCubicadorDetalleId(cubDetalleServicio);

            Criteria criteria1 = sesion.createCriteria(CubicadorDetalleUnificadoActas.class)
                    .add(Restrictions.eq("id", cubicadorDetalleUnificadoActasId));
            detalleCubicadorOrdinarioActasAux = (CubicadorDetalleUnificadoActas) criteria1.uniqueResult();
            respuesta = detalleCubicadorOrdinarioActasAux;
        } catch (HibernateException e) {
            log.error("No se pudo crear el detalleCubicadorOrdinarioActas");
            log.error("Error: " + e.toString());
            throw e;
        }
        return respuesta;
    }
    protected List<Cubicador> obtenerCubicacionPorIdOt(int otId) {
        try {
            List<Cubicador> listado = (List<Cubicador>) sesion.createCriteria(Cubicador.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .setFetchMode("region", FetchMode.JOIN)
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones. ", ex);
            throw ex;
        }
    }

    protected Cubicador obtenerCubicadorPorOtId(int otId) {
        try {
            Cubicador cubicador = (Cubicador) sesion.createCriteria(Cubicador.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .uniqueResult();

            return cubicador;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones. ", ex);
            throw ex;
        }
    }
    protected Cubicador obtenerCubicadorPorId(int cubicadoId) {
        try {
            Cubicador cubicador = (Cubicador) sesion.createCriteria(Cubicador.class)
                    .add(Restrictions.eq("id", cubicadoId))
                    .uniqueResult();

            return cubicador;
        } catch (HibernateException ex) {
            log.error("Error al obtener cubicacion. ", ex);
            throw ex;
        }
    }
    protected Cubicador obtenerCubicadorPorUsuarioIdyPorId(int cubicadoId, int usuarioId) {
        try {
            Cubicador cubicador = (Cubicador) sesion.createCriteria(Cubicador.class)
                    .add(Restrictions.eq("id", cubicadoId))
                    .add(Restrictions.eq("usuario.id", usuarioId))
                    .uniqueResult();

            return cubicador;
        } catch (HibernateException ex) {
            log.error("Error al obtener cubicacion por id y por usuario id. ", ex);
            throw ex;
        }
    }
    protected boolean existeDetalleCubicacionActasCubicador(int idCubicadoOrd) throws Exception {
        Serializable idRegistro = 0;
        boolean existe = false;
        try {
            DetalleCubicadorOrdinarioActas detalleCubicadorOrdinarioActas = new DetalleCubicadorOrdinarioActas();
            Criteria criteria1 = sesion.createCriteria(DetalleCubicadorOrdinarioActas.class)
                    .add(Restrictions.eq("cubicadorOrdinarioId", idCubicadoOrd));
            detalleCubicadorOrdinarioActas = (DetalleCubicadorOrdinarioActas) criteria1.uniqueResult();
            if(detalleCubicadorOrdinarioActas != null ){
                existe = true;
            }

        } catch (HibernateException e) {
            log.error("No se pudo crear el pago");
            throw e;
        }
        return existe;
    }
    protected DetalleCubicadorOrdinarioActas obtieneItemsOrdinario(int idItems, int idActa) throws Exception {
        Serializable idRegistro = 0;
        DetalleCubicadorOrdinarioActas respuesta = new DetalleCubicadorOrdinarioActas();
        try {
            DetalleCubicadorOrdinarioActas detalleCubicadorOrdinarioActasAux = new DetalleCubicadorOrdinarioActas();
            DetalleCubicadorOrdinarioActasId detalleCubicadorOrdinarioActasAuxId = new DetalleCubicadorOrdinarioActasId();
            detalleCubicadorOrdinarioActasAuxId.setActaId(idActa);
            detalleCubicadorOrdinarioActasAuxId.setCubicadorOrdinarioId(idItems);

            Criteria criteria1 = sesion.createCriteria(DetalleCubicadorOrdinarioActas.class)
                    .add(Restrictions.eq("id", detalleCubicadorOrdinarioActasAuxId));
            detalleCubicadorOrdinarioActasAux = (DetalleCubicadorOrdinarioActas) criteria1.uniqueResult();
            respuesta = detalleCubicadorOrdinarioActasAux;
        } catch (HibernateException e) {
            log.error("No se pudo crear el detalleCubicadorOrdinarioActas");
            log.error("Error: "+ e.toString());
            throw e;
        }
        return respuesta;
    }
    protected List<CubicadorOrdinario>  obtenerListaCubicacionOrdinariaPorIdOt(int cubicadorId) {
        try {
            List<CubicadorOrdinario> listado = new ArrayList<CubicadorOrdinario>();
            List<CubicadorOrdinario> listadoAux = (List<CubicadorOrdinario>) sesion.createCriteria(CubicadorOrdinario.class)
                    .add(Restrictions.eq("cubicador.id", cubicadorId))
                    .list();
            for(CubicadorOrdinario cubicadorOrdinario : listadoAux){
                CubicadorOrdinario cubicadorOrdinarioAux = new CubicadorOrdinario();
                TipoMoneda tipoMoneda = new TipoMoneda();
                TipoUnidadMedida tipoUnidadMedida = new TipoUnidadMedida();
                tipoMoneda = (TipoMoneda) sesion.createCriteria(TipoMoneda.class)
                        .add(Restrictions.eq("id", cubicadorOrdinario.getTipoMoneda().getId()))
                        .uniqueResult();
                tipoUnidadMedida = (TipoUnidadMedida) sesion.createCriteria(TipoUnidadMedida.class)
                        .add(Restrictions.eq("id", cubicadorOrdinario.getTipoUnidadMedida().getId()))
                        .uniqueResult();
                cubicadorOrdinarioAux = cubicadorOrdinario;
                cubicadorOrdinarioAux.setTipoMoneda(tipoMoneda);
                cubicadorOrdinarioAux.setTipoUnidadMedida(tipoUnidadMedida);
                listado.add(cubicadorOrdinarioAux);
            }
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones ordinarias. ", ex);
            throw ex;
        }
    }
    protected List<CubicadorDetalle>  obtenerListaCubicacionUnificadaPorIdOt(int cubicadorId) {
        try {
            List<CubicadorDetalle> listado = new ArrayList<CubicadorDetalle>();
            List<CubicadorDetalle> listadoAux = (List<CubicadorDetalle>) sesion.createCriteria(CubicadorDetalle.class)
                    .add(Restrictions.eq("cubicadorId", cubicadorId))
                    .list();
            for(CubicadorDetalle cubicadorDetalle : listadoAux){
                CubicadorDetalle cubicadorDetalleAux = new CubicadorDetalle();

                Regiones regiones = new Regiones();
                Agencias agencia = new Agencias();
                TipoMoneda tipoMoneda = new TipoMoneda();
                Sitios sitios = new Sitios();
                Centrales centrales = new Centrales();

                regiones = (Regiones) sesion.createCriteria(Regiones.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getRegionId()))
                        .uniqueResult();
                agencia = (Agencias) sesion.createCriteria(Agencias.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getAgenciaId()))
                        .uniqueResult();
                tipoMoneda = (TipoMoneda) sesion.createCriteria(TipoMoneda.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getTipoMonedaId()))
                        .uniqueResult();
                sitios = (Sitios) sesion.createCriteria(Sitios.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getSitioId()))
                        .uniqueResult();
                centrales = (Centrales) sesion.createCriteria(Centrales.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getCentralId()))
                        .uniqueResult();

                cubicadorDetalleAux = cubicadorDetalle;
                cubicadorDetalleAux.setRegiones(regiones);
                cubicadorDetalleAux.setAgencia(agencia);
                cubicadorDetalleAux.setTipoMoneda(tipoMoneda);
                cubicadorDetalleAux.setSitios(sitios);
                cubicadorDetalleAux.setCentrales(centrales);
                listado.add(cubicadorDetalleAux);
            }
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones ordinarias. ", ex);
            throw ex;
        }
    }
    protected List<CubicadorDetalleAdicionales>  obtenerListaCubicacionAdicionalUnificadaPorIdOt(int cubicadorId) {
        try {
            List<CubicadorDetalleAdicionales> listado = new ArrayList<CubicadorDetalleAdicionales>();
            List<CubicadorDetalleAdicionales> listadoAux = (List<CubicadorDetalleAdicionales>) sesion.createCriteria(CubicadorDetalleAdicionales.class)
                    .add(Restrictions.eq("cubicadorId", cubicadorId))
                    .list();
            for(CubicadorDetalleAdicionales cubicadorDetalle : listadoAux){
                CubicadorDetalleAdicionales cubicadorDetalleAux = new CubicadorDetalleAdicionales();

                Regiones regiones = new Regiones();
                Agencias agencia = new Agencias();
                TipoMoneda tipoMoneda = new TipoMoneda();
                Sitios sitios = new Sitios();
                Centrales centrales = new Centrales();

                regiones = (Regiones) sesion.createCriteria(Regiones.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getRegionId()))
                        .uniqueResult();
                agencia = (Agencias) sesion.createCriteria(Agencias.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getAgenciaId()))
                        .uniqueResult();
                tipoMoneda = (TipoMoneda) sesion.createCriteria(TipoMoneda.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getTipoMonedaId()))
                        .uniqueResult();
                sitios = (Sitios) sesion.createCriteria(Sitios.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getSitioId()))
                        .uniqueResult();
                centrales = (Centrales) sesion.createCriteria(Centrales.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getCentralId()))
                        .uniqueResult();

                cubicadorDetalleAux = cubicadorDetalle;
                cubicadorDetalleAux.setRegiones(regiones);
                cubicadorDetalleAux.setAgencia(agencia);
                cubicadorDetalleAux.setTipoMoneda(tipoMoneda);
                cubicadorDetalleAux.setSitios(sitios);
                cubicadorDetalleAux.setCentrales(centrales);
                listado.add(cubicadorDetalleAux);
            }
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones ordinarias. ", ex);
            throw ex;
        }
    }
    protected Actas obtenerUltimaActaPorOt(int otId) {
        Actas actaFinal = new Actas();
        actaFinal.setId(0);
        try {
            List<Actas> listado = (List<Actas>) sesion.createCriteria(Actas.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .setFetchMode("ot", FetchMode.JOIN)
                    .setFetchMode("detalleMaterialesActas", FetchMode.JOIN)
                    .setFetchMode("detalleServiciosActas", FetchMode.JOIN)
                    .setFetchMode("detalleMaterialesAdicionalesActas", FetchMode.JOIN)
                    .setFetchMode("detalleServiciosAdicionalesActas", FetchMode.JOIN)
                    .add(Restrictions.isNull("validacionUsuarios"))
                    .list();
            int idActaMayor = 0;
            for (Actas acta : listado) {
                int idActa = acta.getId();
                if (idActa > idActaMayor) {
                    idActaMayor = idActa;
                    actaFinal.setId(acta.getId());
                    actaFinal.setObservaciones(acta.getObservaciones());
                    Ot ot = new Ot();
                    ot.setId(acta.getOt().getId());
                    ot.setFechaCreacion(acta.getOt().getFechaCreacion());
                    ot.setFechaTerminoEstimada(acta.getOt().getFechaTerminoEstimada());
                    actaFinal.setOt(ot);
                    actaFinal.setValidacionSistema(acta.getValidacionSistema());
                    Set<DetalleMaterialesActas> setDetalleMaterialesActas = acta.getDetalleMaterialesActas();
                    if (setDetalleMaterialesActas.size() > 0) {
                        Set<DetalleMaterialesActas> listaDetalleMaterialesActas = new HashSet<>();
                        for (DetalleMaterialesActas detalle : setDetalleMaterialesActas) {
                            DetalleMaterialesActas detalleMaterialesActasFinal = new DetalleMaterialesActas();
                            Materiales material = new Materiales();
                            material.setId(detalle.getMateriales().getId());
                            material.setNombre(detalle.getMateriales().getNombre());
                            detalleMaterialesActasFinal.setMateriales(material);
                            detalleMaterialesActasFinal.setTotalUnidadesCubicadas(detalle.getTotalUnidadesCubicadas());
                            detalleMaterialesActasFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                            detalleMaterialesActasFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());
                            detalleMaterialesActasFinal.setValidacionSistema(detalle.getValidacionSistema());
                            listaDetalleMaterialesActas.add(detalleMaterialesActasFinal);
                        }
                        actaFinal.setDetalleMaterialesActas(listaDetalleMaterialesActas);
                    }
                    Set<DetalleMaterialesAdicionalesActas> setDetalleMaterialesAdicionalesActas = acta.getDetalleMaterialesAdicionalesActas();
                    if (setDetalleMaterialesAdicionalesActas.size() > 0) {
                        Set<DetalleMaterialesAdicionalesActas> listaDetalleMaterialesAdicionalesActas = new HashSet<>();
                        for (DetalleMaterialesAdicionalesActas detalle : setDetalleMaterialesAdicionalesActas) {
                            DetalleMaterialesAdicionalesActas detalleMaterialesActasAdicionalesFinal = new DetalleMaterialesAdicionalesActas();
                            Materiales material = new Materiales();
                            material.setId(detalle.getMateriales().getId());
                            material.setNombre(detalle.getMateriales().getNombre());
                            detalleMaterialesActasAdicionalesFinal.setMateriales(material);
                            detalleMaterialesActasAdicionalesFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                            detalleMaterialesActasAdicionalesFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());
                            listaDetalleMaterialesAdicionalesActas.add(detalleMaterialesActasAdicionalesFinal);
                        }
                        actaFinal.setDetalleMaterialesAdicionalesActas(listaDetalleMaterialesAdicionalesActas);
                    }
                    Set<DetalleServiciosActas> setDetalleServiciosActas = acta.getDetalleServiciosActas();
                    if (setDetalleServiciosActas.size() > 0) {
                        Set<DetalleServiciosActas> listaDetalleServiciosActas = new HashSet<>();
                        for (DetalleServiciosActas detalle : setDetalleServiciosActas) {
                            DetalleServiciosActas detalleServiciosActaFinal = new DetalleServiciosActas();
                            Servicios servicio = new Servicios();
                            servicio.setId(detalle.getServicio().getId());
                            servicio.setNombre(detalle.getServicio().getNombre());
                            detalleServiciosActaFinal.setServicio(servicio);
                            detalleServiciosActaFinal.setTotalUnidadesCubicadas(detalle.getTotalUnidadesCubicadas());
                            detalleServiciosActaFinal.setValidacionSistema(detalle.getValidacionSistema());
                            detalleServiciosActaFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                            detalleServiciosActaFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());

                            listaDetalleServiciosActas.add(detalleServiciosActaFinal);
                        }
                        actaFinal.setDetalleServiciosActas(listaDetalleServiciosActas);
                    }
                    Set<DetalleServiciosAdicionalesActas> setDetalleServiciosAdicionalesActas = acta.getDetalleServiciosAdicionalesActas();
                    if (setDetalleServiciosAdicionalesActas.size() > 0) {
                        Set<DetalleServiciosAdicionalesActas> listaDetalleServiciosAdicionalesActas = new HashSet<>();
                        for (DetalleServiciosAdicionalesActas detalle : setDetalleServiciosAdicionalesActas) {
                            DetalleServiciosAdicionalesActas detalleServiciosAdicionalesActaFinal = new DetalleServiciosAdicionalesActas();
                            Servicios servicio = new Servicios();
                            servicio.setId(detalle.getServicio().getId());
                            servicio.setNombre(detalle.getServicio().getNombre());
                            detalleServiciosAdicionalesActaFinal.setServicio(servicio);
                            detalleServiciosAdicionalesActaFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                            detalleServiciosAdicionalesActaFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());

                            listaDetalleServiciosAdicionalesActas.add(detalleServiciosAdicionalesActaFinal);
                        }
                        actaFinal.setDetalleServiciosAdicionalesActas(listaDetalleServiciosAdicionalesActas);
                    }
                }
            }
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones. ", ex);
            throw ex;
        }

        return actaFinal;
    }

    protected List<Actas> obtenerActasPorOt(int otId) {
        List<Actas> listadoFinal = new ArrayList<>();
        List<Actas> listado = new ArrayList<Actas>();
        try {
            listado = (List<Actas>) sesion.createCriteria(Actas.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .setFetchMode("ot", FetchMode.JOIN)
                    //                    .setFetchMode("detalleMaterialesActas", FetchMode.JOIN)
                    //                    .setFetchMode("detalleServiciosActas", FetchMode.JOIN)
                    //                    .setFetchMode("detalleMaterialesAdicionalesActas", FetchMode.JOIN)
                    //                    .setFetchMode("detalleServiciosAdicionalesActas", FetchMode.JOIN)
                    .add(Restrictions.isNull("validacionUsuarios"))
                    .list();
            int contadorActas = listado.size();
            if (contadorActas > 0) {
                for (Actas acta : listado) {
                    Actas actaFinal = new Actas();

                    actaFinal.setId(acta.getId());
                    actaFinal.setObservaciones(acta.getObservaciones());
                    Ot ot = new Ot();
                    ot.setId(acta.getOt().getId());
                    ot.setFechaCreacion(acta.getOt().getFechaCreacion());
                    ot.setFechaTerminoEstimada(acta.getOt().getFechaTerminoEstimada());
                    actaFinal.setOt(ot);
                    actaFinal.setValidacionSistema(acta.getValidacionSistema());
                    Set<DetalleMaterialesActas> setDetalleMaterialesActas = acta.getDetalleMaterialesActas();
                    if (setDetalleMaterialesActas.size() > 0) {
                        Set<DetalleMaterialesActas> listaDetalleMaterialesActas = new HashSet<>();
                        for (DetalleMaterialesActas detalle : setDetalleMaterialesActas) {
                            DetalleMaterialesActas detalleMaterialesActasFinal = new DetalleMaterialesActas();
                            Materiales material = new Materiales();
                            material.setId(detalle.getMateriales().getId());
                            material.setNombre(detalle.getMateriales().getNombre());
                            detalleMaterialesActasFinal.setMateriales(material);
                            detalleMaterialesActasFinal.setTotalUnidadesCubicadas(detalle.getTotalUnidadesCubicadas());
                            detalleMaterialesActasFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                            detalleMaterialesActasFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());
                            detalleMaterialesActasFinal.setValidacionSistema(detalle.getValidacionSistema());
                            listaDetalleMaterialesActas.add(detalleMaterialesActasFinal);
                        }
                        actaFinal.setDetalleMaterialesActas(listaDetalleMaterialesActas);
                    }
                    Set<DetalleMaterialesAdicionalesActas> setDetalleMaterialesAdicionalesActas = acta.getDetalleMaterialesAdicionalesActas();
                    if (setDetalleMaterialesAdicionalesActas.size() > 0) {
                        Set<DetalleMaterialesAdicionalesActas> listaDetalleMaterialesAdicionalesActas = new HashSet<>();
                        for (DetalleMaterialesAdicionalesActas detalle : setDetalleMaterialesAdicionalesActas) {
                            DetalleMaterialesAdicionalesActas detalleMaterialesActasAdicionalesFinal = new DetalleMaterialesAdicionalesActas();
                            Materiales material = new Materiales();
                            material.setId(detalle.getMateriales().getId());
                            material.setNombre(detalle.getMateriales().getNombre());
                            detalleMaterialesActasAdicionalesFinal.setMateriales(material);
                            detalleMaterialesActasAdicionalesFinal.setTotalUnidadesCubicadas(0d);
                            detalleMaterialesActasAdicionalesFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                            detalleMaterialesActasAdicionalesFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());

                            listaDetalleMaterialesAdicionalesActas.add(detalleMaterialesActasAdicionalesFinal);
                        }
                        actaFinal.setDetalleMaterialesAdicionalesActas(listaDetalleMaterialesAdicionalesActas);
                    }
                    Set<DetalleServiciosActas> setDetalleServiciosActas = acta.getDetalleServiciosActas();
                    if (setDetalleServiciosActas.size() > 0) {
                        Set<DetalleServiciosActas> listaDetalleServiciosActas = new HashSet<>();
                        for (DetalleServiciosActas detalle : setDetalleServiciosActas) {
                            DetalleServiciosActas detalleServiciosActaFinal = new DetalleServiciosActas();
                            Servicios servicio = new Servicios();
                            servicio.setId(detalle.getServicio().getId());
                            servicio.setNombre(detalle.getServicio().getNombre());
                            detalleServiciosActaFinal.setServicio(servicio);
                            detalleServiciosActaFinal.setTotalUnidadesCubicadas(detalle.getTotalUnidadesCubicadas());
                            detalleServiciosActaFinal.setValidacionSistema(detalle.getValidacionSistema());
                            detalleServiciosActaFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                            detalleServiciosActaFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());

                            listaDetalleServiciosActas.add(detalleServiciosActaFinal);
                        }
                        actaFinal.setDetalleServiciosActas(listaDetalleServiciosActas);
                    }
                    Set<DetalleServiciosAdicionalesActas> setDetalleServiciosAdicionalesActas = acta.getDetalleServiciosAdicionalesActas();
                    if (setDetalleServiciosAdicionalesActas.size() > 0) {
                        Set<DetalleServiciosAdicionalesActas> listaDetalleServiciosAdicionalesActas = new HashSet<>();
                        for (DetalleServiciosAdicionalesActas detalle : setDetalleServiciosAdicionalesActas) {
                            DetalleServiciosAdicionalesActas detalleServiciosAdicionalesActaFinal = new DetalleServiciosAdicionalesActas();
                            Servicios servicio = new Servicios();
                            servicio.setId(detalle.getServicio().getId());
                            servicio.setNombre(detalle.getServicio().getNombre());
                            detalleServiciosAdicionalesActaFinal.setServicio(servicio);
                            detalleServiciosAdicionalesActaFinal.setTotalUnidadesCubicadas(0d);
                            detalleServiciosAdicionalesActaFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                            detalleServiciosAdicionalesActaFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());

                            listaDetalleServiciosAdicionalesActas.add(detalleServiciosAdicionalesActaFinal);
                        }
                        actaFinal.setDetalleServiciosAdicionalesActas(listaDetalleServiciosAdicionalesActas);
                    }
                    listadoFinal.add(actaFinal);
                }
            }
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones. ", ex);
            throw ex;
        }
        return listadoFinal;
    }

    protected List<Actas> obtenerActasPorOtIdValidada(int otId) {
        List<Actas> actasValidadas = new ArrayList<Actas>();
        Ot ot = new Ot();
        try {
            Criteria criteria = sesion.createCriteria(Ot.class)
                    .add(Restrictions.eq("id", otId));
            ot = (Ot) criteria.uniqueResult();

            Criteria criteria1 = sesion.createCriteria(Actas.class)
                    .add(Restrictions.eq("ot", ot));
            actasValidadas = (List<Actas>) criteria1.list();

        } catch (Exception e) {
            log.error("Error al obtener listado de cubicaciones. " + e.toString());
            throw e;
        }
        return actasValidadas;
    }


    protected List<Actas> obtenerActasPorOtValidadas(int otId) {
        List<Actas> actasValidadas = new ArrayList<Actas>();
        Ot ot = new Ot();
        try{
            Criteria criteria = sesion.createCriteria(Ot.class)
                    .add(Restrictions.eq("id", otId));
            ot = (Ot) criteria.uniqueResult();

            Criteria criteria1 = sesion.createCriteria(Actas.class)
                    .add(Restrictions.eq("ot", ot))
                    .add(Restrictions.eq("validacionUsuarios", true));
            actasValidadas = (List<Actas>) criteria1.list();

        } catch (Exception e){
            log.error("Error al obtener listado de cubicaciones. "+ e.toString());
            throw e;
        }
        return actasValidadas;
    }

    protected List<Actas> obtenerTodasLasActasPorOt(int otId) {
        List<Actas> listadoFinal = new ArrayList<>();

        try {
            List<Actas> listado = (List<Actas>) sesion.createCriteria(Actas.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .setFetchMode("ot", FetchMode.JOIN)
                    .list();

            for (Actas acta : listado) {
                Actas actaFinal = new Actas();

                actaFinal.setId(acta.getId());
                //Modificacion por errores e frontend cuando existen observaciones con salto de linea
                String obs = acta.getObservaciones().replace("\n", "");
                actaFinal.setObservaciones(obs);
                Ot ot = new Ot();
                ot.setId(acta.getOt().getId());
                ot.setFechaCreacion(acta.getOt().getFechaCreacion());
                ot.setFechaTerminoEstimada(acta.getOt().getFechaTerminoEstimada());
                actaFinal.setOt(ot);
                actaFinal.setValidacionSistema(acta.getValidacionSistema());
                actaFinal.setValidacionUsuarios(acta.getValidacionUsuarios());
                actaFinal.setPagoAprobado(acta.getPagoAprobado());

                actaFinal.setMontoTotalActaCubicado(acta.getMontoTotalActaCubicado());
                actaFinal.setMontoTotalActaAdicional(acta.getMontoTotalActaAdicional());
                actaFinal.setMontoTotalActa(acta.getMontoTotalActa());
                actaFinal.setMontoTotalAPagar(acta.getMontoTotalAPagar());
                actaFinal.setFecha(acta.getFecha());


                Set<DetalleMaterialesActas> setDetalleMaterialesActas = acta.getDetalleMaterialesActas();
                if (setDetalleMaterialesActas.size() > 0) {
                    Set<DetalleMaterialesActas> listaDetalleMaterialesActas = new HashSet<>();
                    for (DetalleMaterialesActas detalle : setDetalleMaterialesActas) {
                        DetalleMaterialesActas detalleMaterialesActasFinal = new DetalleMaterialesActas();
                        Materiales material = new Materiales();
                        material.setId(detalle.getMateriales().getId());
                        material.setNombre(detalle.getMateriales().getNombre());
                        detalleMaterialesActasFinal.setMateriales(material);
                        detalleMaterialesActasFinal.setTotalUnidadesCubicadas(detalle.getTotalUnidadesCubicadas());
                        detalleMaterialesActasFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                        detalleMaterialesActasFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());
                        detalleMaterialesActasFinal.setValidacionSistema(detalle.getValidacionSistema());
                        listaDetalleMaterialesActas.add(detalleMaterialesActasFinal);
                    }
                    actaFinal.setDetalleMaterialesActas(listaDetalleMaterialesActas);
                }
                Set<DetalleMaterialesAdicionalesActas> setDetalleMaterialesAdicionalesActas = acta.getDetalleMaterialesAdicionalesActas();
                if (setDetalleMaterialesAdicionalesActas.size() > 0) {
                    Set<DetalleMaterialesAdicionalesActas> listaDetalleMaterialesAdicionalesActas = new HashSet<>();
                    for (DetalleMaterialesAdicionalesActas detalle : setDetalleMaterialesAdicionalesActas) {
                        DetalleMaterialesAdicionalesActas detalleMaterialesActasAdicionalesFinal = new DetalleMaterialesAdicionalesActas();
                        Materiales material = new Materiales();
                        material.setId(detalle.getMateriales().getId());
                        material.setNombre(detalle.getMateriales().getNombre());
                        material.setDescripcion(detalle.getMateriales().getDescripcion());
                        detalleMaterialesActasAdicionalesFinal.setMateriales(material);
                        detalleMaterialesActasAdicionalesFinal.setTotalUnidadesCubicadas(0d);
                        detalleMaterialesActasAdicionalesFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                        detalleMaterialesActasAdicionalesFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());

                        listaDetalleMaterialesAdicionalesActas.add(detalleMaterialesActasAdicionalesFinal);
                    }
                    actaFinal.setDetalleMaterialesAdicionalesActas(listaDetalleMaterialesAdicionalesActas);
                }
                Set<DetalleServiciosActas> setDetalleServiciosActas = acta.getDetalleServiciosActas();
                if (setDetalleServiciosActas.size() > 0) {
                    Set<DetalleServiciosActas> listaDetalleServiciosActas = new HashSet<>();
                    for (DetalleServiciosActas detalle : setDetalleServiciosActas) {
                        DetalleServiciosActas detalleServiciosActaFinal = new DetalleServiciosActas();
                        Servicios servicio = new Servicios();
                        servicio.setId(detalle.getServicio().getId());
                        servicio.setNombre(detalle.getServicio().getNombre());
                        detalleServiciosActaFinal.setServicio(servicio);
                        detalleServiciosActaFinal.setTotalUnidadesCubicadas(detalle.getTotalUnidadesCubicadas());
                        detalleServiciosActaFinal.setValidacionSistema(detalle.getValidacionSistema());
                        detalleServiciosActaFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                        detalleServiciosActaFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());

                        listaDetalleServiciosActas.add(detalleServiciosActaFinal);
                    }
                    actaFinal.setDetalleServiciosActas(listaDetalleServiciosActas);
                }
                Set<DetalleServiciosAdicionalesActas> setDetalleServiciosAdicionalesActas = acta.getDetalleServiciosAdicionalesActas();
                if (setDetalleServiciosAdicionalesActas.size() > 0) {
                    Set<DetalleServiciosAdicionalesActas> listaDetalleServiciosAdicionalesActas = new HashSet<>();
                    for (DetalleServiciosAdicionalesActas detalle : setDetalleServiciosAdicionalesActas) {
                        DetalleServiciosAdicionalesActas detalleServiciosAdicionalesActaFinal = new DetalleServiciosAdicionalesActas();
                        Servicios servicio = new Servicios();
                        servicio.setId(detalle.getServicio().getId());
                        servicio.setNombre(detalle.getServicio().getNombre());
                        detalleServiciosAdicionalesActaFinal.setServicio(servicio);
                        detalleServiciosAdicionalesActaFinal.setTotalUnidadesCubicadas(0d);
                        detalleServiciosAdicionalesActaFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                        detalleServiciosAdicionalesActaFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());

                        listaDetalleServiciosAdicionalesActas.add(detalleServiciosAdicionalesActaFinal);
                    }
                    actaFinal.setDetalleServiciosAdicionalesActas(listaDetalleServiciosAdicionalesActas);
                }
                listadoFinal.add(actaFinal);
            }

        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones. ", ex);
            throw ex;
        }
        return listadoFinal;
    }

    protected Actas obtenerActaParaValidar(int otId) {
        Actas actaFinal = new Actas();
        try {

            Criteria criteria = sesion
                    .createCriteria(Actas.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .setProjection(Projections.max("fecha"));

            java.sql.Timestamp fechaReciente = (java.sql.Timestamp) criteria.uniqueResult();

            List<Actas> listado = (List<Actas>) sesion.createCriteria(Actas.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .add(Restrictions.eq("fecha", fechaReciente))
                    .setFetchMode("ot", FetchMode.JOIN)
                    .setFetchMode("detalleMaterialesActas", FetchMode.JOIN)
                    .setFetchMode("detalleServiciosActas", FetchMode.JOIN)
                    .setFetchMode("detalleMaterialesAdicionalesActas", FetchMode.JOIN)
                    .setFetchMode("detalleServiciosAdicionalesActas", FetchMode.JOIN)
                    .list();
            int totalActas = listado.size();

//            Obtiene cubicador
            Cubicador cubicador = new Cubicador();
            cubicador = (Cubicador) sesion
                    .createCriteria(Cubicador.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .setFetchMode("contrato", FetchMode.JOIN)
                    .setFetchMode("proveedor", FetchMode.JOIN)
                    .setFetchMode("region", FetchMode.JOIN)
                    .uniqueResult();

            if (totalActas > 0) {
                Actas acta = listado.get(0);
                actaFinal.setId(acta.getId());
                actaFinal.setObservaciones(acta.getObservaciones());
                Ot ot = new Ot();
                ot.setId(acta.getOt().getId());
                ot.setContrato(acta.getOt().getContrato());
                ot.setFechaCreacion(acta.getOt().getFechaCreacion());
                ot.setFechaTerminoEstimada(acta.getOt().getFechaTerminoEstimada());
                actaFinal.setOt(ot);
                actaFinal.setValidacionSistema(acta.getValidacionSistema());
                Set<DetalleMaterialesActas> setDetalleMaterialesActas = acta.getDetalleMaterialesActas();
                if (setDetalleMaterialesActas.size() > 0) {
                    Set<DetalleMaterialesActas> listaDetalleMaterialesActas = new HashSet<>();
                    for (DetalleMaterialesActas detalle : setDetalleMaterialesActas) {
                        DetalleMaterialesActas detalleMaterialesActasFinal = new DetalleMaterialesActas();
                        Materiales material = new Materiales();
                        material.setId(detalle.getMateriales().getId());
                        material.setNombre(detalle.getMateriales().getNombre());
                        material.setDescripcion(detalle.getMateriales().getDescripcion());
                        DetalleMaterialesActasId id = new DetalleMaterialesActasId();
                        id.setMaterialId(detalle.getId().getMaterialId());
                        detalleMaterialesActasFinal.setId(id);
                        detalleMaterialesActasFinal.setMateriales(material);
                        detalleMaterialesActasFinal.setTotalUnidadesCubicadas(detalle.getTotalUnidadesCubicadas());
                        detalleMaterialesActasFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                        detalleMaterialesActasFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());
                        detalleMaterialesActasFinal.setValidacionSistema(detalle.getValidacionSistema());
                        listaDetalleMaterialesActas.add(detalleMaterialesActasFinal);
                    }
                    actaFinal.setDetalleMaterialesActas(listaDetalleMaterialesActas);
                }
                Set<DetalleMaterialesAdicionalesActas> setDetalleMaterialesAdicionalesActas = acta.getDetalleMaterialesAdicionalesActas();
                if (setDetalleMaterialesAdicionalesActas.size() > 0) {
                    Set<DetalleMaterialesAdicionalesActas> listaDetalleMaterialesAdicionalesActas = new HashSet<>();
                    for (DetalleMaterialesAdicionalesActas detalle : setDetalleMaterialesAdicionalesActas) {
                        DetalleMaterialesAdicionalesActas detalleMaterialesActasAdicionalesFinal = new DetalleMaterialesAdicionalesActas();
                        Materiales material = new Materiales();
                        material.setId(detalle.getMateriales().getId());
                        material.setNombre(detalle.getMateriales().getNombre());
                        material.setDescripcion(detalle.getMateriales().getDescripcion());

                        DetalleMaterialesAdicionalesActasId id = new DetalleMaterialesAdicionalesActasId();
                        id.setMaterialId(detalle.getId().getMaterialId());
                        detalleMaterialesActasAdicionalesFinal.setId(id);

                        detalleMaterialesActasAdicionalesFinal.setMateriales(material);

                        detalleMaterialesActasAdicionalesFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                        detalleMaterialesActasAdicionalesFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());
                        detalleMaterialesActasAdicionalesFinal.setValidacionUsuarios(detalle.getValidacionUsuarios());

                        listaDetalleMaterialesAdicionalesActas.add(detalleMaterialesActasAdicionalesFinal);
                    }
                    actaFinal.setDetalleMaterialesAdicionalesActas(listaDetalleMaterialesAdicionalesActas);
                }
                Set<DetalleServiciosActas> setDetalleServiciosActas = acta.getDetalleServiciosActas();
                if (setDetalleServiciosActas.size() > 0) {
                    Set<DetalleServiciosActas> listaDetalleServiciosActas = new HashSet<>();
                    for (DetalleServiciosActas detalle : setDetalleServiciosActas) {

                        CubicadorServicios cubicadorServicios = new CubicadorServicios();
                        cubicadorServicios =  (CubicadorServicios) sesion
                                .createCriteria(CubicadorServicios.class)
                                .add(Restrictions.eq("cubicador.id", cubicador.getId()))
                                .add(Restrictions.eq("servicio.id", detalle.getId().getServicioId()))
                                .uniqueResult();

                        DetalleServiciosActas detalleServiciosActaFinal = new DetalleServiciosActas();
                        Servicios servicio = new Servicios();
                        servicio.setId(detalle.getServicio().getId());
                        servicio.setNombre(detalle.getServicio().getNombre());
                        servicio.setDescripcion(detalle.getServicio().getDescripcion());
                        servicio.setPrecio(cubicadorServicios.getPrecio());
                        detalleServiciosActaFinal.setServicio(servicio);

                        DetalleServiciosActasId id = new DetalleServiciosActasId();
                        id.setServicioId(detalle.getId().getServicioId());
                        detalleServiciosActaFinal.setId(id);

                        detalleServiciosActaFinal.setTotalMontoCubicado(detalle.getTotalMontoCubicado());
                        detalleServiciosActaFinal.setTotalMontoEjecucionActa(detalle.getTotalMontoEjecucionActa());
                        detalleServiciosActaFinal.setTotalUnidadesCubicadas(detalle.getTotalUnidadesCubicadas());
                        detalleServiciosActaFinal.setValidacionSistema(detalle.getValidacionSistema());
                        detalleServiciosActaFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                        detalleServiciosActaFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());
                        detalleServiciosActaFinal.setTotalHistoricoActa(detalle.getTotalHistoricoActa());

                        listaDetalleServiciosActas.add(detalleServiciosActaFinal);
                    }
                    actaFinal.setDetalleServiciosActas(listaDetalleServiciosActas);
                }
                Set<DetalleServiciosAdicionalesActas> setDetalleServiciosAdicionalesActas = acta.getDetalleServiciosAdicionalesActas();
                if (setDetalleServiciosAdicionalesActas.size() > 0) {
                    Set<DetalleServiciosAdicionalesActas> listaDetalleServiciosAdicionalesActas = new HashSet<>();
                    for (DetalleServiciosAdicionalesActas detalle : setDetalleServiciosAdicionalesActas) {

                        CubicadorServicios cubicadorServicios = new CubicadorServicios();
                        cubicadorServicios =  (CubicadorServicios) sesion
                                .createCriteria(CubicadorServicios.class)
                                .add(Restrictions.eq("cubicador.id", cubicador.getId()))
                                .add(Restrictions.eq("servicio.id", detalle.getId().getServicioId()))
                                .uniqueResult();

                        DetalleServiciosAdicionalesActas detalleServiciosAdicionalesActaFinal = new DetalleServiciosAdicionalesActas();
                        Servicios servicio = new Servicios();
                        servicio.setId(detalle.getServicio().getId());
                        servicio.setNombre(detalle.getServicio().getNombre());
                        servicio.setDescripcion(detalle.getServicio().getDescripcion());
                        if(cubicadorServicios != null){
                            servicio.setCantidadDefault(cubicadorServicios.getServicio().getCantidadDefault());
                            servicio.setPrecio(cubicadorServicios.getPrecio());
                        } else {
                            ProveedoresServicios proveedoresServicios = new ProveedoresServicios();
                            proveedoresServicios =  (ProveedoresServicios) sesion
                                    .createCriteria(ProveedoresServicios.class)
                                    .add(Restrictions.eq("contratos.id", cubicador.getContrato().getId()))
                                    .add(Restrictions.eq("proveedores.id", cubicador.getProveedor().getId()))
                                    .add(Restrictions.eq("regiones.id", cubicador.getRegion().getId()))
                                    .add(Restrictions.eq("servicios.id", detalle.getId().getServicioId()))
                                    .uniqueResult();
                            servicio.setCantidadDefault(0);
                            servicio.setPrecio(proveedoresServicios.getPrecio());
                        }
                        detalleServiciosAdicionalesActaFinal.setServicio(servicio);

                        DetalleServiciosAdicionalesActasId id = new DetalleServiciosAdicionalesActasId();
                        id.setServicioId(detalle.getId().getServicioId());
                        detalleServiciosAdicionalesActaFinal.setId(id);

//                    detalleServiciosAdicionalesActaFinal.setTotalUnidadesCubicadas(detalle.getTotalUnidadesCubicadas());
                        detalleServiciosAdicionalesActaFinal.setTotalMontoCubicado(detalle.getTotalMontoCubicado());
                        detalleServiciosAdicionalesActaFinal.setTotalMontoEjecucionActa(detalle.getTotalMontoEjecucionActa());
                        detalleServiciosAdicionalesActaFinal.setTotalUnidadesEjecucionActa(detalle.getTotalUnidadesEjecucionActa());
                        detalleServiciosAdicionalesActaFinal.setTotalUnidadesEjecucionOt(detalle.getTotalUnidadesEjecucionOt());
                        detalleServiciosAdicionalesActaFinal.setValidacionUsuarios(detalle.getValidacionUsuarios());
                        detalleServiciosAdicionalesActaFinal.setTotalHistoricoActa(detalle.getTotalHistoricoActa());

                        listaDetalleServiciosAdicionalesActas.add(detalleServiciosAdicionalesActaFinal);
                    }
                    actaFinal.setDetalleServiciosAdicionalesActas(listaDetalleServiciosAdicionalesActas);
                }

                actaFinal.setMontoCubicado(acta.getMontoCubicado());
                actaFinal.setMontoHistorico(acta.getMontoHistorico());
                actaFinal.setMontoTotalActaAdicional(acta.getMontoTotalActaAdicional());
                actaFinal.setMontoTotalActa(acta.getMontoTotalActa());
                actaFinal.setMontoTotalActaCubicado(acta.getMontoTotalActaCubicado());
                actaFinal.setSaldoPendiente(acta.getSaldoPendiente());
                actaFinal.setMontoTotalAPagar(acta.getMontoTotalAPagar());

//            actaFinal.setDetalleMaterialesActas(acta.getDetalleMaterialesActas());
//            actaFinal.setDetalleMaterialesAdicionalesActas(acta.getDetalleMaterialesAdicionalesActas());
//            actaFinal.setDetalleServiciosActas(acta.getDetalleMaterialesActas());
//            actaFinal.setDetalleServiciosAdicionalesActas(acta.getDetalleServiciosAdicionalesActas());
            }
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones. ", ex);
            throw ex;
        }
        return actaFinal;
    }

    protected List obtieneObservacionesGerencia(Login login, int otId) {
        List<ValidacionesOtec> listaValidacionesOtec = new ArrayList<ValidacionesOtec>();
        Map mapaObservacionesGerencia = new HashMap();
        List listaObs = new ArrayList();

        try {

            List<UsuariosValidadores> listUsuariosValidadores = new ArrayList<UsuariosValidadores>();
            Criteria criteria3 = sesion.createCriteria(UsuariosValidadores.class)
                    .add(Restrictions.eq("ot.id", otId));
            listUsuariosValidadores = (List<UsuariosValidadores>) criteria3.list();

            for (UsuariosValidadores usuariosValidadores : listUsuariosValidadores) {
                listaValidacionesOtec = new ArrayList<ValidacionesOtec>();
                Criteria criteria4 = sesion.createCriteria(ValidacionesOtec.class)
                        .add(Restrictions.eq("usuariosValidadores.id", usuariosValidadores.getId()));
                listaValidacionesOtec = (List<ValidacionesOtec>) criteria4.list();
                for (ValidacionesOtec validacionesOtecAux : listaValidacionesOtec) {
                    mapaObservacionesGerencia = new HashMap();
                    String nombreUser = usuariosValidadores.getUsuarios().getNombres() + " " + usuariosValidadores.getUsuarios().getApellidos();
//                    if (i > 0) {
//                        coma = ",";
//                    } else {
//                        coma = "";
//                    }
//                    obsgerencia = obsgerencia + coma + "{  \n" +
//                            "  \"usuario\": " + "\"" + nombreUser + "\"" + ",\n" +
//                            "  \"fecha\": " + "\"" + validacionesOtecAux.getFecha() + "\"" + ",\n" +
//                            "  \"observaciones\": " + "\"" + validacionesOtecAux.getComentario() + "\"" + "\n" +
//                            "}";
                    mapaObservacionesGerencia.put("usuario", nombreUser);
                    mapaObservacionesGerencia.put("fecha", validacionesOtecAux.getFecha());
                    mapaObservacionesGerencia.put("observaciones", validacionesOtecAux.getComentario());

                    listaObs.add(mapaObservacionesGerencia);
//                    i = i + 1;
                }
            }
        } catch (Exception e) {

        }
        return listaObs;
    }

    protected List<Cubicador> listarCubicacionesSinOtPorUsuarioId(int usuarioId, int contratoId) {
        try {
            List<Cubicador> listado = (List<Cubicador>) sesion.createCriteria(Cubicador.class)
                    .add(Restrictions.eq("usuario.id", usuarioId))
                    .add(Restrictions.eq("contrato.id", contratoId))
                    .add(Restrictions.isNull("ot.id"))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones. ", ex);
            throw ex;
        }
    }

    protected List<Cubicador> obtenerMontoCubicadorPorIdOt(int usuarioId, int contratoId) {
        try {
            List<Cubicador> listado = (List<Cubicador>) sesion.createCriteria(Cubicador.class)
                    .add(Restrictions.eq("usuario.id", usuarioId))
                    .add(Restrictions.eq("contrato.id", contratoId))
                    .add(Restrictions.isNull("ot.id"))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones. ", ex);
            throw ex;
        }
    }

    protected List<Roles> listarRolesPorPerfil(int perfilId) {
        try {
            List<Roles> listado = (List<Roles>) sesion.createCriteria(Roles.class)
                    .add(Restrictions.eq("perfil.id", perfilId))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Roles. ", ex);
            throw ex;
        }
    }

    protected List<Contratos> listarContratosPorId(int contratoId) {
        try {
            List<Contratos> listado = (List<Contratos>) sesion.createCriteria(Contratos.class)
                    .setFetchMode("tipoEvento", FetchMode.JOIN)
                    .add(Restrictions.eq("id", contratoId))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Contratos. ", ex);
            throw ex;
        }
    }


    protected List<UsuariosEventosContratos> obtenerUsuariosEventosContratos(int eventoId, int contratoId) {
        List<UsuariosEventosContratos> listado = new ArrayList<>();
        try {
            listado = (List<UsuariosEventosContratos>) sesion.createCriteria(UsuariosEventosContratos.class)
                    .add(Restrictions.eq("id.eventoId", eventoId))
                    .add(Restrictions.eq("id.contratoId", contratoId))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Contratos. ", ex);
            throw ex;
        }
    }

    protected List<UsuariosValidadores> obtenerUsuariosValidadores(int otId) throws Exception{
        List<UsuariosValidadores> listadoFinal = new ArrayList<>();
        try {
            List<UsuariosValidadores> listadoEvento9 = (List<UsuariosValidadores>) sesion.createCriteria(UsuariosValidadores.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .list();

            for (UsuariosValidadores usuariosValidadores: listadoEvento9){

                if(usuariosValidadores.getEventos().getId() != 16 && usuariosValidadores.getEventos().getId() != 17){
                    validarYSetearObjetoARetornar(usuariosValidadores, null);
                    listadoFinal.add(usuariosValidadores);
                }

            }


            return listadoFinal;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Contratos. ", ex);
            throw ex;
        }
    }

    protected List<ValidacionesOtec> obtenerValidacionesUsuarioOt(int usuarioValidador) throws Exception{
        List<ValidacionesOtec> listadoFinal = new ArrayList<>();
        try {
            listadoFinal = (List<ValidacionesOtec>) sesion.createCriteria(ValidacionesOtec.class)
                    .add(Restrictions.eq("usuariosValidadores.id", usuarioValidador))
                    .list();

            for (ValidacionesOtec validacionesUsuario: listadoFinal){

                validarYSetearObjetoARetornar(validacionesUsuario, null);
            }

            return listadoFinal;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Contratos. ", ex);
            throw ex;
        }
    }

    protected Eventos listarEventosPorId(int eventoId) {
        try {
            List<Eventos> listado = (List<Eventos>) sesion.createCriteria(Eventos.class)
                    .setFetchMode("tipoEvento", FetchMode.JOIN)
                    .add(Restrictions.eq("id", eventoId))
                    .list();
            Eventos eventoRetorno = listado.get(0);
            Eventos evento = new Eventos();
            evento.setId(eventoRetorno.getId());
            TipoEvento tipoEvento = new TipoEvento();
            tipoEvento.setId(eventoRetorno.getTipoEvento().getId());
            tipoEvento.setNombre(eventoRetorno.getTipoEvento().getNombre());
            evento.setTipoEvento(tipoEvento);

            return evento;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Contratos. ", ex);
            throw ex;
        }
    }

    protected List<Workflow> listarWorkflowPorId(int workflowId) {
        try {
            List<Workflow> listado = (List<Workflow>) sesion.createCriteria(Workflow.class)
                    .add(Restrictions.eq("id", workflowId))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Contratos. ", ex);
            throw ex;
        }
    }

    protected List<Roles> listarRolPorId(int rolId) {
        try {
            List<Roles> listado = (List<Roles>) sesion.createCriteria(Roles.class)
                    .add(Restrictions.eq("id", rolId))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Contratos. ", ex);
            throw ex;
        }
    }

    protected Roles obtenerRolPorUsuarioId(int usuarioId) {
        Roles rolUsuario = new Roles();
        try {
            Usuarios usuario = (Usuarios) sesion.createCriteria(Usuarios.class)
                    .add(Restrictions.eq("id", usuarioId))
                    .uniqueResult();
            Set<Roles> listadoRoles = usuario.getUsuariosRoles();
            for (Roles rol : listadoRoles) {
                rolUsuario.setId(rol.getId());
            }
            return rolUsuario;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Contratos. ", ex);
            throw ex;
        }
    }

    protected List<Notificaciones> obtenerNotificacionesNOLeidasPorUsuario(int idUsuario, int limite) throws Exception{
        try {
            Criteria criteria = sesion.createCriteria(Notificaciones.class);
            criteria.add(Restrictions.eq("leido", false));
            criteria.setMaxResults(limite);
            criteria.setFirstResult(0);
            criteria.add(Restrictions.eq("usuarioId", idUsuario));
            criteria.addOrder(Order.asc("id"));
            List<Notificaciones> listadoNoLeidas = criteria.list();
            for(Notificaciones notificacion : listadoNoLeidas){
                validarYSetearObjetoARetornar(notificacion, null);
            }
            return listadoNoLeidas;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Notificaciones. ", ex);
            throw ex;
        } catch(Exception exc){
            log.error("Error al obtener listado de Notificaciones. ", exc);
            throw exc;
        }
    }

    protected List<Notificaciones> obtenerNotificacionesLeidasPorUsuario(int idUsuario, int limite) throws Exception{
        try {
            Criteria criteria = sesion.createCriteria(Notificaciones.class);
            criteria.add(Restrictions.eq("leido", true));
            criteria.setMaxResults(limite);
            criteria.setFirstResult(0);
            criteria.add(Restrictions.eq("usuarioId", idUsuario));
            criteria.addOrder(Order.asc("id"));
            List<Notificaciones> listadoLeidas = criteria.list();
            for(Notificaciones notificacion : listadoLeidas){
                validarYSetearObjetoARetornar(notificacion, null);
            }
            return listadoLeidas;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Notificaciones. ", ex);
            throw ex;
        } catch(Exception exc){
            log.error("Error al obtener listado de Notificaciones. ", exc);
            throw exc;
        }
    }

    protected List<Ot> obtenerListadoOtSbe(int idContrato) {
        try {

            List<Ot> listado = (List<Ot>) sesion.createCriteria(Ot.class)
                    .add(Restrictions.eq("contrato.id", idContrato))
                    .add(Restrictions.eq("tipoOt.id", 2))
                    .add(Restrictions.eq("numeroAp", 0))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Contratos. ", ex);
            throw ex;
        }
    }

    protected List<Integer> obtenerListadoRegionesPorContrato(int idContrato) {
        try {

            List<Integer> listado = (List<Integer>) sesion.createCriteria(ProveedoresServicios.class)
                    .add(Restrictions.eq("contratos.id", idContrato))
                    .setProjection(Projections.distinct(Projections.property("regiones.id")))
                    .list();
            return listado;

        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Contratos. ", ex);
            throw ex;
        }
    }

    protected List<Integer> obtenerListadoProveedoresPorRegion(int idRegion, int idContrato) {
        try {
            List<Integer> listado = (List<Integer>) sesion.createCriteria(ProveedoresServicios.class)
                    .add(Restrictions.eq("contratos.id", idContrato))
                    .add(Restrictions.eq("regiones.id", idRegion))
                    .setProjection(Projections.distinct(Projections.property("proveedores.id")))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Contratos. ", ex);
            throw ex;
        }
    }

    protected List<ContratosProveedores> obtenerListadoProveedoresPorContrato(int idContrato) {
        List<ContratosProveedores> listadoRetorno = null;
        try {
            listadoRetorno = (List<ContratosProveedores>) sesion.createCriteria(ContratosProveedores.class)
                    .setFetchMode("proveedores", FetchMode.JOIN)
                    .add(Restrictions.eq("contratos.id", idContrato))
                    .list();
            return listadoRetorno;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Contratos. ", ex);
            throw ex;
        }
    }

    protected List<Proveedores> obtenerListadoProveedoresPorContratoUnificado(int idContrato) {
        List listadoRetorno = null;
        List<Proveedores> proveedores = new ArrayList<Proveedores>();
        try {
            listadoRetorno = (List<ContratosProveedores>) sesion.createCriteria(ContratosProveedores.class)
                    .setFetchMode("proveedores", FetchMode.JOIN)
                    .add(Restrictions.eq("contratos.id", idContrato))
                    .setProjection(Projections.projectionList()
                            .add(Projections.distinct(Projections.property("proveedores.id"))))
                    .list();
            for (int i = 0; i < listadoRetorno.size(); i++) {
                int idProveedor = Integer.parseInt(listadoRetorno.get(i).toString());
                Proveedores proveedores2 = new Proveedores();
                proveedores2 = (Proveedores) sesion.createCriteria(Proveedores.class)
                        .add(Restrictions.eq("id", idProveedor))
                        .uniqueResult();
                proveedores.add(proveedores2);
            }

            return proveedores;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Contratos. ", ex);
            throw ex;
        }
    }

    protected List<Sitios> obtenerListadoDeSitiosPorPlanDeProyecto(int planDeProyectoId) {
        try {
            List<Sitios> listado = (List<Sitios>) sesion.createCriteria(Sitios.class)
                    .add(Restrictions.eq("planDeProyecto.id", planDeProyectoId))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Contratos. ", ex);
            throw ex;
        }
    }

    protected void leerNotificacion(int idNotificacion) {
        try {
            List<Notificaciones> listado = (List<Notificaciones>) sesion.createCriteria(Notificaciones.class)
                    .add(Restrictions.eq("id", idNotificacion))
                    .list();
            Notificaciones notificacion = listado.get(0);
            notificacion.setLeido(true);
            sesion.update(notificacion);
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Notificaciones. ", ex);
            throw ex;
        }
    }

    // este es el nuevo
    protected List<OtAux> listarWorkflowEventosEjecucionPorUsuarioId(int usuarioEjecutorId) throws Exception {
        try {
            // Obtengo todas las ot donde participa el usuario
            List<WorkflowEventosEjecucion> wee = (List<WorkflowEventosEjecucion>) sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .setFetchMode("ot", FetchMode.JOIN)
                    .list();

            List<OtAux> listadoOt = new ArrayList();
            List<Integer> particpacionOt = new ArrayList<>();
            for (WorkflowEventosEjecucion workee : wee) {
                if (workee.getUsuarioId() == usuarioEjecutorId) {
                    boolean esta = false;
                    int idOt = workee.getOt().getId();
                    for (Integer listadoParticipacion : particpacionOt) {
                        if (idOt == listadoParticipacion) {
                            esta = true;
                        }
                    }
                    if (!esta) {
                        particpacionOt.add(workee.getOt().getId());
                    }
                }
            }

            for (WorkflowEventosEjecucion workee : wee) {
                int idOt = workee.getOt().getId();
                for (Integer listadoParticipacion : particpacionOt) {
                    if (idOt == listadoParticipacion) {
                        int ejecutado = workee.getEjecutado();
                        String estado = "";
                        if (ejecutado > 0 && ejecutado != 9999) {
                            estado = "ejecutado";
                        } else if (ejecutado == 9999) {
                            estado = "en ejecucion";
                        } else if (ejecutado == 0) {
                            estado = "por ejecutar";
                        }
                        boolean usuarioEjecutaEvento = false;
                        if (usuarioEjecutorId == workee.getUsuarioId() && estado.equals("en ejecucion")) {
                            usuarioEjecutaEvento = true;
                        }

                        List<Eventos> evento = (List<Eventos>) sesion.createCriteria(Eventos.class)
                                .add(Restrictions.eq("id", workee.getId().getEventoId())).list();
                        String nombreEvento = evento.get(0).getNombre();
                        listadoOt = validaPushOt2(listadoOt, idOt, estado, nombreEvento, usuarioEjecutaEvento);
                    }
                }
            }

            // obtengo el rol del usuario que me invoco
            Roles rol = GenericDAO.getINSTANCE().obtenerRolPorUsuarioId(usuarioEjecutorId);
            if (rol.getId() == 7) {
                for (OtAux ot : listadoOt) {
                    if (ot.getCurrent().equalsIgnoreCase("Informar Avance")) {
                        ot.setEditar(true);
                    }

                }
            }

            return listadoOt;

        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<String> obtenerAccionesOt(int otId) throws Exception {

        List<String> listadoAccionesOt = new ArrayList<String>();
        List<String> listadoAccionesOtFinal = new ArrayList<String>();
        try {

            List<WorkflowEventosEjecucion> wee = (List<WorkflowEventosEjecucion>) sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .list();

            Ot ot = (Ot) sesion.createCriteria(Ot.class)
                    .add(Restrictions.eq("id", otId))
                    .setFetchMode("contrato", FetchMode.JOIN)
                    .uniqueResult();
            String pasoFlujo = "";
            int orden = 0;
            int pos = 0;
            List<WorkflowEventos> workflowEventos = (List<WorkflowEventos>) sesion.createCriteria(WorkflowEventos.class)
                    .add(Restrictions.eq("id.workflowId", ot.getContrato().getWorkflow().getId()))
                    .addOrder(Order.asc("orden"))
                    .list();
            for (int i = 0; i< workflowEventos.size(); i++){
                listadoAccionesOt.add(Integer.toString(pos));
                pos++;
            }
            int eventosBorrados = 0;
            for (WorkflowEventosEjecucion weeAux: wee){
                boolean encontro = false;
                int rolId = 0;
                Eventos evento = (Eventos) sesion.createCriteria(Eventos.class)
                        .add(Restrictions.eq("id", weeAux.getId().getEventoId())).uniqueResult();
                rolId = weeAux.getId().getRolId();
                for (WorkflowEventos we: workflowEventos){
                    if(evento.getId() == we.getEvento().getId()){
                        orden = we.getOrden() - 1;
                        encontro = true;
                        break;
                    }
                }
                if(!encontro){
                    eventosBorrados = (eventosBorrados + 1);
                    orden = wee.size() + eventosBorrados;
                    listadoAccionesOt.add(Integer.toString(orden - 1));
                    orden = orden - 1;
                }
                Usuarios usuarios = null;
                int idUser = weeAux.getUsuarioId();
                Criteria criteria = sesion.createCriteria(Usuarios.class);
                criteria.add(Restrictions.eq("id", idUser));
                criteria.setFetchMode("usuariosRoles", FetchMode.JOIN);
                usuarios = (Usuarios) criteria.uniqueResult();

                int rolUsuarioEjecutor = 0;
                String sql1 = "select rol_id from usuarios_roles where usuario_id = "+usuarios.getId()+" LIMIT (1) ";
                Query query1 = sesion.createSQLQuery(sql1);
                rolUsuarioEjecutor = (int) query1.uniqueResult();

                Roles roles = new Roles();
                String sql = "select * from roles where id = "+rolUsuarioEjecutor;
                Query query = sesion.createSQLQuery(sql).addEntity(Roles.class);
                roles = (Roles) query.uniqueResult();

                String usuarioEjecutor = "";

                if(evento.getId() == 16){
                    usuarioEjecutor = "Encargado de PMO";
                }else if(evento.getId() == 17){
                    usuarioEjecutor = "Encargado de Imputación";
                }else if(evento.getId() == 7 || evento.getId() == 11 || evento.getId() == 12){
                    usuarioEjecutor = "Encargado de Pagos";
                }else {
                    usuarioEjecutor = usuarios.getNombres() + " " + usuarios.getApellidos();
                }

                if(weeAux.getEjecutado() > 0 && weeAux.getEjecutado() !=9999){

                    pasoFlujo = "{\n" +
                            "  \"nombre\": \""+ evento.getNombre() + "\"" + ",\n" +
                            "  \"usuarioEjecutor\": \""+ usuarioEjecutor + "\"" + ",\n" +
                            "  \"usuarioId\": "+ usuarios.getId()+ ",\n" +
                            "  \"rolNombre\": \""+ roles.getNombre() + "\"" + ",\n" +
                            "  \"rolId\": "+ roles.getId()+ ",\n" +
                            "  \"current\": \" "+ "ejecutado" + "\"" + ",\n" +
                            "  \"orden\": "+ (orden + 1)  +
                            "}";
                }else if(weeAux.getEjecutado() > 0 && weeAux.getEjecutado() ==9999){
                    pasoFlujo = "{\n" +
                            "  \"nombre\": \""+ evento.getNombre() + "\"" + ",\n" +
                            "  \"usuarioEjecutor\": \""+ usuarioEjecutor + "\"" + ",\n" +
                            "  \"usuarioId\": "+ usuarios.getId()+ ",\n" +
                            "  \"rolNombre\": \""+ roles.getNombre() + "\"" + ",\n" +
                            "  \"rolId\": "+ roles.getId()+ ",\n" +
                            "  \"current\": \" "+ "en ejecucion" + "\"" + ",\n" +
                            "  \"orden\": "+ (orden + 1)  +
                            "}";
                }else{
                    pasoFlujo = "{\n" +
                            "  \"nombre\": \""+ evento.getNombre() + "\"" + ",\n" +
                            "  \"usuarioEjecutor\": \""+ usuarioEjecutor + "\"" + ",\n" +
                            "  \"usuarioId\": "+ usuarios.getId()+ ",\n" +
                            "  \"rolNombre\": \""+ roles.getNombre() + "\"" + ",\n" +
                            "  \"rolId\": "+ roles.getId()+ ",\n" +
                            "  \"current\": \" "+ "por ejecutar" + "\"" + ",\n" +
                            "  \"orden\": "+ (orden + 1)  +
                            "}";
                }



                listadoAccionesOt.set(orden, pasoFlujo);
            }

            int i = 0;
            for (String cont: listadoAccionesOt){
                if(validaListaConCamposVacios(cont, listadoAccionesOt.size())){
                    listadoAccionesOtFinal.add(i, cont);
                    i = i + 1;
                }

            }

        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }

        return listadoAccionesOtFinal;
    }

    protected boolean validaListaConCamposVacios(String contenido,int total) throws Exception {
        boolean respuesta = true;
        String numPos = "";
        for (int u = 0; u< total; u++){
            numPos = Integer.toString(u);
            if(contenido.equals(numPos)){
                respuesta = false;
            }
        }
        return respuesta;
    }

    protected boolean validaEditar(int otId, int usuarioEjecutorId) throws Exception {

        boolean respuesta = false;

        Eventos eventoAux = new Eventos();
        eventoAux.setNombre("");

        try {

            List<WorkflowEventosEjecucion> wee = (List<WorkflowEventosEjecucion>) sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .list();

            for (WorkflowEventosEjecucion weeAux: wee){
                if(weeAux.getEjecutado() == 9999){
                    eventoAux =  (Eventos) sesion.createCriteria(Eventos.class)
                            .add(Restrictions.eq("id", weeAux.getId().getEventoId())).uniqueResult();
                    break;
                }
            }
            if(eventoAux.getNombre().equalsIgnoreCase("Informar Avance")){
                Roles rol = GenericDAO.getINSTANCE().obtenerRolPorUsuarioId(usuarioEjecutorId);
                if(rol.getId() == 7){
                    respuesta = true;
                }
            }

        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }

        return respuesta;
    }


    private List<OtAux> validaPushOt2(List<OtAux> listaRetorno, int idOt, String estado, String nombreEvento, boolean usuarioEjecutaEvento) {
        boolean esta = false;
        int cont = 0;
        for (OtAux otAux : listaRetorno) {
            if (otAux.getId() == idOt) {
                esta = true;
                String pasoFlujo = "{\n"
                        + "  \"nombre\": \" " + nombreEvento + "\"" + ",\n"
                        + "  \"current\": \" " + estado + "\""
                        + "}";
                listaRetorno.get(cont).getParticipacion().add(pasoFlujo);
                if (usuarioEjecutaEvento) {
                    listaRetorno.get(cont).setEjecutar(true);
                }
                if (estado.equals("en ejecucion")) {
                    listaRetorno.get(cont).setCurrent(nombreEvento);
                }
            }
            cont++;
        }
        if (!esta) {
            OtAux ot = new OtAux();
            ot.setId(idOt);
            if (estado.equals("en ejecucion")) {
                ot.setCurrent(nombreEvento);
            }
            if (usuarioEjecutaEvento) {
                ot.setEjecutar(true);
            } else {
                ot.setEjecutar(false);
            }
            String pasoFlujo = "{\n"
                    + "  \"nombre\": \" " + nombreEvento + "\"" + ",\n"
                    + "  \"current\": \" " + estado + "\""
                    + "}";

            List<String> participacionAux = new ArrayList<>();
            participacionAux.add(pasoFlujo);
            ot.setParticipacion(participacionAux);
            ot.setEditar(false);
            listaRetorno.add(ot);
        }
        return listaRetorno;
    }


    protected int obtenerTotalOtPorEstado(int usuarioEjecutorId, String estado, Map<String,String> listadoFiltros) throws Exception {
        int retorno = 0;
        BigInteger total;
        Object respuesta = new Object();
        List<BigInteger> listadoOtAbiertas = new ArrayList();
        try {

            String condicionWhere = obtenerFiltrosDesdeMap(listadoFiltros);

            switch (estado) {
                case "abierta":
                    Query query = sesion.createSQLQuery("SELECT count(ot.id) FROM ot WHERE tipo_estado_ot_id = 1" + condicionWhere +" AND ID IN (SELECT DISTINCT(usuarios_ot.ot_id) FROM usuarios_ot WHERE usuario_id = :usuarioId)")
                            .setParameter("usuarioId", usuarioEjecutorId);
                    listadoOtAbiertas = query.list();
                    respuesta = listadoOtAbiertas.get(0);
                    total = (BigInteger)respuesta;
                    retorno = total.intValue();
                    break;
                case "cerrada":
                    Query query2 = sesion.createSQLQuery("SELECT count(ot.id) FROM ot WHERE tipo_estado_ot_id = 2" + condicionWhere +" AND ID IN (SELECT DISTINCT(usuarios_ot.ot_id) FROM usuarios_ot WHERE usuario_id = :usuarioId)")
                            .setParameter("usuarioId", usuarioEjecutorId);
                    listadoOtAbiertas = query2.list();
                    respuesta = listadoOtAbiertas.get(0);
                    total = (BigInteger)respuesta;
                    retorno = total.intValue();
                    break;
                case "play":
                    Query query3 = sesion.createSQLQuery("SELECT count(ot.id) FROM ot WHERE tipo_estado_ot_id = 1" + condicionWhere +" AND ID IN (SELECT DISTINCT(usuarios_ot.ot_id) FROM usuarios_ot WHERE usuario_id = :usuarioId) AND ID IN (SELECT DISTINCT(workflow_eventos_ejecucion.ot_id) FROM workflow_eventos_ejecucion WHERE usuario_id = :usuarioId and ejecutado = 9999)")
                            .setParameter("usuarioId", usuarioEjecutorId);
                    listadoOtAbiertas = query3.list();
                    respuesta = listadoOtAbiertas.get(0);
                    total = (BigInteger)respuesta;
                    retorno = total.intValue();
                    break;
                case "tramite":
                    Query query4 = sesion.createSQLQuery("SELECT count(ot.id) FROM ot WHERE tipo_estado_ot_id = 5" + condicionWhere +" AND ID IN (SELECT DISTINCT(usuarios_ot.ot_id) FROM usuarios_ot WHERE usuario_id = :usuarioId)")
                            .setParameter("usuarioId", usuarioEjecutorId);
                    listadoOtAbiertas = query4.list();
                    respuesta = listadoOtAbiertas.get(0);
                    total = (BigInteger)respuesta;
                    retorno = total.intValue();
                    break;
            }

        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }

        return retorno;
    }

    private String obtenerFiltrosDesdeMap(Map<String, String> listadoFiltros) {
        String condicionWhere = "";

        if(listadoFiltros != null && listadoFiltros.size() > 0){
            if(listadoFiltros.containsKey("id")){
                String valor = String.valueOf(listadoFiltros.get("id"));
                if(valor != null && !("null").equalsIgnoreCase(valor)){
                    condicionWhere = condicionWhere + "and id = " + String.valueOf(listadoFiltros.get("id"));
                }
            }
            if(listadoFiltros.containsKey("contratoId")){
                if(!String.valueOf(listadoFiltros.get("contratoId")).equals("0")){
                    condicionWhere = condicionWhere + "and contrato_id = " + String.valueOf(listadoFiltros.get("contratoId"));
                }
            }
            if(listadoFiltros.containsKey("nombre")){
                if(!String.valueOf(listadoFiltros.get("nombre")).isEmpty()){
                    condicionWhere = condicionWhere + "and LOWER(nombre) like LOWER('%" + String.valueOf(listadoFiltros.get("nombre")+"%')");
                }
            }
            if(listadoFiltros.containsKey("fechaCreacion")){
                if(listadoFiltros.get("fechaCreacion") != null){
                    String[] fechaCompleta = String.valueOf(listadoFiltros.get("fechaCreacion")).split("T");
                    condicionWhere = condicionWhere + "and DATE(fecha_creacion) = '" + fechaCompleta[0]+ "'";
                }
            }
            if(listadoFiltros.containsKey("fechaInicioEstimada")){
                if(listadoFiltros.get("fechaInicioEstimada") != null) {
                    String[] fechaCompleta = String.valueOf(listadoFiltros.get("fechaInicioEstimada")).split("T");
                    condicionWhere = condicionWhere + "and DATE(fecha_inicio_estimada) = '" + fechaCompleta[0] + "'";
                }
            }
            if(listadoFiltros.containsKey("fechaTerminoEstimada")){
                if(listadoFiltros.get("fechaTerminoEstimada") != null) {
                    String[] fechaCompleta = String.valueOf(listadoFiltros.get("fechaTerminoEstimada")).split("T");
                    condicionWhere = condicionWhere + "and DATE(fecha_termino_estimada) = '" + fechaCompleta[0] + "'";
                }
            }
            if(listadoFiltros.containsKey("tipoOt")){
                if(!String.valueOf(listadoFiltros.get("tipoOt")).equals("0")){
                    condicionWhere = condicionWhere + "and tipo_ot_id = " + String.valueOf(listadoFiltros.get("tipoOt"));
                }
            }
            if(listadoFiltros.containsKey("gestor")){
                if(!String.valueOf(listadoFiltros.get("gestor")).isEmpty()){
                    condicionWhere = condicionWhere + " and usuario_id in (select id from usuarios where (nombres ||' ' ||  apellidos) ilike '%" + String.valueOf(listadoFiltros.get("gestor")+"%')");
                }
            }
            if(listadoFiltros.containsKey("proveedor")){
                if(!String.valueOf(listadoFiltros.get("proveedor")).isEmpty()){
                    condicionWhere = condicionWhere + "and proveedor_id in (SELECT id from proveedores WHERE nombre ilike '%" + String.valueOf(listadoFiltros.get("proveedor")+"%')");
                }
            }

        }

        return condicionWhere;
    }

    protected List<Ot> listarOtPorEstado(int usuarioEjecutorId, int pos, int cantidadRegistro, String estado, String filtro, String orden, Map<String,String> listadoFiltros) throws Exception {

        List<Ot> listadoOtAbiertas = new ArrayList<>();
        String filtroReal = "";

        try {

            String condicionWhere = obtenerFiltrosDesdeMap(listadoFiltros);

            switch (filtro) {
                case "fechaCreacion":
                    filtroReal = "fecha_creacion";
                    break;
                case "fechaInicioEstimada":
                    filtroReal = "fecha_inicio_estimada";
                    break;
                case "fechaTerminoEstimada":
                    filtroReal = "fecha_termino_estimada";
                    break;
                case "tipoOt":
                    filtroReal = "tipo_ot_id";
                    break;
                case "proveedor":
                    filtroReal = "proveedor_id";
                    break;
                case "gestor":
                    filtroReal = "usuario_id";
                    break;
                case "contrato":
                    filtroReal = "contrato_id";
                    break;
                default:
                    filtroReal = filtro;
            }
            String consulta = "";
            String ordenReal = "";
            switch (estado) {
                case "abierta":
                    consulta = "SELECT * FROM ot WHERE tipo_estado_ot_id = 1 "
                    + "AND ID IN (SELECT DISTINCT(usuarios_ot.ot_id) FROM usuarios_ot WHERE usuario_id = :usuarioId)" + condicionWhere
//                    + "AND ID IN (SELECT DISTINCT(workflow_eventos_ejecucion.ot_id) FROM workflow_eventos_ejecucion WHERE usuario_id = :usuarioId and ejecutado <> 9999)"
                    + " ordenamiento"
                    + " LIMIT :limite"
                    + " OFFSET :pos";

                    if(orden.equals("+")){
                        ordenReal = "ORDER BY " +  filtroReal + " ASC";
                        consulta = consulta.replace("ordenamiento", ordenReal);
                    }else{
                        ordenReal = "ORDER BY " +  filtroReal + " DESC";
                        consulta = consulta.replace("ordenamiento", ordenReal);
                    }

                    Query query = sesion.createSQLQuery(consulta)
                    .addEntity(Ot.class)
                    .setParameter("usuarioId", usuarioEjecutorId)
                    .setParameter("limite", cantidadRegistro)
                    .setParameter("pos", pos);
                    listadoOtAbiertas = query.list();
                    break;
                case "cerrada":
                    consulta = "SELECT * FROM ot WHERE tipo_estado_ot_id = 2 "
                    +" condicionWhere "
                    + "AND ID IN (SELECT DISTINCT(usuarios_ot.ot_id) FROM usuarios_ot WHERE usuario_id = :usuarioId)"
                    + " ordenamiento"
                    + " LIMIT :limite"
                    + " OFFSET :pos";

                    if(orden.equals("+")){
                        ordenReal = "ORDER BY " +  filtroReal + " ASC";
                        consulta = consulta.replace("ordenamiento", ordenReal);
                    }else{
                        ordenReal = "ORDER BY " +  filtroReal + " DESC";
                        consulta = consulta.replace("ordenamiento", ordenReal);
                    }

                    consulta = consulta.replace("condicionWhere", condicionWhere);

                    Query query2 = sesion.createSQLQuery(consulta)
                    .addEntity(Ot.class)
                    .setParameter("usuarioId", usuarioEjecutorId)
                    .setParameter("limite", cantidadRegistro)
                    .setParameter("pos", pos);
                    listadoOtAbiertas = query2.list();
                    break;
                case "play":

//                    consulta = "SELECT * FROM ot WHERE tipo_estado_ot_id = 1 "
//                    + "AND ID IN (SELECT DISTINCT(usuarios_ot.ot_id) FROM usuarios_ot WHERE usuario_id = :usuarioId)"
//                    + "AND ID IN (SELECT DISTINCT(workflow_eventos_ejecucion.ot_id) FROM workflow_eventos_ejecucion WHERE usuario_id = :usuarioId and ejecutado = 9999)"
//                    + " ORDER BY ot.id ASC"
//                    + " LIMIT :limite"
//                    + " OFFSET :pos";

                    // esta nueva query busca desplegar el play en todas las bandejas de los coordinadores del unificado
                    consulta = "SELECT * FROM ot WHERE tipo_estado_ot_id = 1\n" +
                            " condicionWhere "+
                            "AND ID IN (SELECT DISTINCT(usuarios_ot.ot_id) FROM usuarios_ot WHERE usuario_id = :usuarioId)\n" +
                            "AND (ID IN (SELECT DISTINCT(workflow_eventos_ejecucion.ot_id) FROM workflow_eventos_ejecucion WHERE usuario_id = :usuarioId and ejecutado = 9999)\n"+
                            "OR (ID IN (SELECT DISTINCT(usuarios_ot.ot_id) FROM usuarios_ot WHERE usuario_id = :usuarioId\n" +
                            "                                                                     AND ot_id in (SELECT DISTINCT(workflow_eventos_ejecucion.ot_id) FROM workflow_eventos_ejecucion WHERE ejecutado = 9999 and evento_id = 45 OR ejecutado = 9999 and evento_id = 15 OR ejecutado = 9999 and evento_id = 41)\n" +
                            "                                                                     AND usuario_id in (SELECT id from usuarios WHERE id = :usuarioId and proveedor_id <> 9999))))"
                            + " ORDER BY ot.id DESC"
                            + " LIMIT :limite"
                            + " OFFSET :pos";


                    if(orden.equals("+")){
                        ordenReal = "ORDER BY " +  filtroReal + " ASC";
                        consulta = consulta.replace("ordenamiento", ordenReal);
                    }else{
                        ordenReal = "ORDER BY " +  filtroReal + " DESC";
                        consulta = consulta.replace("ordenamiento", ordenReal);
                    }


                    consulta = consulta.replace("condicionWhere", condicionWhere);

                    Query query3 = sesion.createSQLQuery(consulta)
                    .addEntity(Ot.class)
                    .setParameter("usuarioId", usuarioEjecutorId)
                    .setParameter("limite", cantidadRegistro)
                    .setParameter("pos", pos);
                    listadoOtAbiertas = query3.list();
                    break;
                case "tramite":
                    consulta = "SELECT * FROM ot WHERE tipo_estado_ot_id = 5 "
                            + "AND ID IN (SELECT DISTINCT(usuarios_ot.ot_id) FROM usuarios_ot WHERE usuario_id = :usuarioId)"
//                    + "AND ID IN (SELECT DISTINCT(workflow_eventos_ejecucion.ot_id) FROM workflow_eventos_ejecucion WHERE usuario_id = :usuarioId and ejecutado <> 9999)"
                            + " ordenamiento"
                            + " LIMIT :limite"
                            + " OFFSET :pos";

                    if(orden.equals("+")){
                        ordenReal = "ORDER BY " +  filtroReal + " ASC";
                        consulta = consulta.replace("ordenamiento", ordenReal);
                    }else{
                        ordenReal = "ORDER BY " +  filtroReal + " DESC";
                        consulta = consulta.replace("ordenamiento", ordenReal);
                    }

                    Query query4 = sesion.createSQLQuery(consulta)
                            .addEntity(Ot.class)
                            .setParameter("usuarioId", usuarioEjecutorId)
                            .setParameter("limite", cantidadRegistro)
                            .setParameter("pos", pos);
                    listadoOtAbiertas = query4.list();
                    break;
            }

        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }

        return listadoOtAbiertas;
    }

    protected List<Ot> listarOtPorId(int usuarioEjecutorId, int otId, String estado) throws Exception {

        List<Ot> listadoOtPorId = new ArrayList<>();

        try {
            String consulta = "";
            int tipo_estado_ot_id = 0;
            String consulta_ad = "";

            switch (estado) {
                case "abierta": tipo_estado_ot_id = 1; break;
                case "cerrada": tipo_estado_ot_id = 2; break;
                case "play": tipo_estado_ot_id = 1; consulta_ad = " AND ID IN (SELECT DISTINCT(workflow_eventos_ejecucion.ot_id) FROM workflow_eventos_ejecucion WHERE usuario_id = :usuarioId and ejecutado = 9999)"; break;
                case "tramite": tipo_estado_ot_id = 5; break;

            }

            consulta = "SELECT * FROM ot WHERE id = :otId AND tipo_estado_ot_id = :otEstado  "
                    + "AND ID IN (SELECT DISTINCT(usuarios_ot.ot_id) FROM usuarios_ot WHERE usuario_id = :usuarioId) " + consulta_ad;

            Query query = sesion.createSQLQuery(consulta)
                    .addEntity(Ot.class)
                    .setParameter("otId", otId)
                    .setParameter("otEstado", tipo_estado_ot_id)
                    .setParameter("usuarioId", usuarioEjecutorId);
            listadoOtPorId = query.list();

        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }

        return listadoOtPorId;
    }

    protected List<Ot> listarOtPorNombre(int usuarioEjecutorId, String otNombre, String estado) throws Exception {

        List<Ot> listadoOtPorId = new ArrayList<>();

        try {
            String consulta = "";
            int tipo_estado_ot_id = 0;
            String consulta_ad = "";

            switch (estado) {
                case "abierta": tipo_estado_ot_id = 1; break;
                case "cerrada": tipo_estado_ot_id = 2; break;
                case "play": tipo_estado_ot_id = 1; consulta_ad = " AND ID IN (SELECT DISTINCT(workflow_eventos_ejecucion.ot_id) FROM workflow_eventos_ejecucion WHERE usuario_id = :usuarioId and ejecutado = 9999)"; break;
                case "tramite": tipo_estado_ot_id = 5; break;

            }

            consulta = "SELECT * FROM ot WHERE LOWER(nombre) LIKE LOWER(:nombre) AND tipo_estado_ot_id = :otEstado "
                    + "AND ID IN (SELECT DISTINCT(usuarios_ot.ot_id) FROM usuarios_ot WHERE usuario_id = :usuarioId)";

            Query query = sesion.createSQLQuery(consulta)
                    .addEntity(Ot.class)
                    .setParameter("nombre", "%" + otNombre + "%")
                    .setParameter("otEstado", tipo_estado_ot_id)
                    .setParameter("usuarioId", usuarioEjecutorId);
            listadoOtPorId = query.list();

        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }

        return listadoOtPorId;
    }

    protected int obtenerTotalCubicaciones(int usuarioEjecutorId) throws Exception {

        int retorno = 0;
        BigInteger total;
        Object respuesta = new Object();
        List<BigInteger> listadoOtAbiertas = new ArrayList();
        try {
            Query query = sesion.createSQLQuery("SELECT count(cubicador.id) FROM cubicador WHERE usuario_id =:usuarioId")
                    .setParameter("usuarioId", usuarioEjecutorId);
            listadoOtAbiertas = query.list();
            respuesta = listadoOtAbiertas.get(0);
            total = (BigInteger) respuesta;
            retorno = total.intValue();
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }

        return retorno;
    }


    protected int obtenerEventoProximoEjecutar(int usuarioEjecutorId) throws Exception {
        int idEvento = 0;
        try {
            List<WorkflowEventosEjecucion> wee = (List<WorkflowEventosEjecucion>) sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .setFetchMode("ot", FetchMode.JOIN)
                    .add(Restrictions.eq("usuarioId", usuarioEjecutorId))
                    .add(Restrictions.eq("ejecutado", 9999))
                    .list();
            idEvento = wee.get(0).getId().getEventoId();
            return idEvento;

        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected int obtenerEventoProximoEjecutarPorOt(int otId) throws Exception {
        int idEvento = 0;
        try {
            List<WorkflowEventosEjecucion> wee = (List<WorkflowEventosEjecucion>) sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .setFetchMode("ot", FetchMode.JOIN)
                    .add(Restrictions.eq("ot.id", otId))
                    .add(Restrictions.eq("ejecutado", 9999))
                    .list();
            idEvento = wee.get(0).getId().getEventoId();
            return idEvento;

        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<Usuarios> listarUsuariosPorId(int usuarioId) {
        try {
            List<Usuarios> listado = (List<Usuarios>) sesion.createCriteria(Usuarios.class)
                    .add(Restrictions.eq("id", usuarioId))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Usuarios. ", ex);
            throw ex;
        }
    }

    private Map generarJsonCubicacion(Cubicador cubicacion) {
        Map formularioActa = new HashMap();
        List listaServicios = new ArrayList();
        List listaMateriales = new ArrayList();
        Map mapaMateriales = null;
        Map mapaServicios = null;


        Set<CubicadorMateriales> listadoMateriales = cubicacion.getCubicadorMateriales();
        Set<CubicadorServicios> listadoServicios = cubicacion.getCubicadorServicios();

        for (CubicadorMateriales material : listadoMateriales) {
            mapaMateriales = new HashMap();
            mapaMateriales.put("id", material.getMaterial().getId());
            mapaMateriales.put("nombre", material.getMaterial().getNombre());
            mapaMateriales.put("cantidad", material.getCantidad());
            mapaMateriales.put("montoTotal", material.getMontoTotal());
//            mapaMateriales.put("tipoMoneda", material.getMaterial().getTipoMoneda());
            listaMateriales.add(mapaMateriales);
        }

        for (CubicadorServicios servicio : listadoServicios) {

            List<ProveedoresServicios> proveedoresServicios = null;
            proveedoresServicios = obtieneProveedorServicios(cubicacion.getContrato().getId(), cubicacion.getRegion().getId(),  cubicacion.getProveedor().getId(),  servicio.getServicio().getId());
            int tipoMonedaId = proveedoresServicios.get(0).getTipoMoneda().getId();
//            double valorCLP = 0;
//            if(tipoMonedaId!=2){
//                valorCLP = retornarPrecioEnPesosChilenos(servicio.getPrecio(),tipoMonedaId);
//            }
            mapaServicios = new HashMap();
            mapaServicios.put("id", servicio.getServicio().getId());
            mapaServicios.put("numeroProducto", proveedoresServicios.get(0).getNumeroProducto());
            mapaServicios.put("nombre", servicio.getServicio().getNombre());
            mapaServicios.put("descripcion", servicio.getServicio().getDescripcion());
            mapaServicios.put("cantidad", servicio.getCantidad());
            mapaServicios.put("precio", servicio.getPrecio());
            mapaServicios.put("montoTotal", servicio.getTotal());
            mapaServicios.put("tipoMonedaId", tipoMonedaId);
//            mapaServicios.put("valorCLP",valorCLP);
            listaServicios.add(mapaServicios);

        }


        if (listadoServicios.size() > 0 || listadoMateriales.size() > 0) {
            formularioActa.put("id", cubicacion.getId());
            if (listadoMateriales.size() > 0) {
                formularioActa.put("materiales", listaMateriales);
            }
            if (listadoServicios.size() > 0) {
                formularioActa.put("servicios", listaServicios);
            }
        } else {
            formularioActa.put("id", cubicacion.getId());
        }

        return formularioActa;
    }

    protected double retornarPrecioEnPesosChilenos(double valor, int codigoTipoMoneda) {
        if (codigoTipoMoneda == 2) {
            return valor;
        }
        double valorEnPesos = 0d;
        Object tipoMonedaValores = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String fechaActual = sdf.format(new Date());
            Date fecha = sdf.parse(fechaActual);
            tipoMonedaValores = (Object) sesion.createCriteria(TipoMonedaValores.class)
                    .setProjection(Projections.property("valor"))
                    .add(Restrictions.ge("id.fecha", fecha))
                    .add(Restrictions.eq("id.tipoMonedaId", codigoTipoMoneda))
                    .uniqueResult();
        } catch (HibernateException ex) {
            log.error("Error al obtener tipoMoneda. ", ex);
            throw ex;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return valor * (Double) tipoMonedaValores;
    }

    protected double retornarValorMonedaEnPesos(int codigoTipoMoneda) {
        if (codigoTipoMoneda == 2) {
            return 1;
        }
        double valorEnPesos = 0d;
        Object tipoMonedaValores = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String fechaActual = sdf.format(new Date());
            Date fecha = sdf.parse(fechaActual);
            tipoMonedaValores = (Object) sesion.createCriteria(TipoMonedaValores.class)
                    .setProjection(Projections.property("valor"))
                    .add(Restrictions.ge("id.fecha", fecha))
                    .add(Restrictions.eq("id.tipoMonedaId", codigoTipoMoneda))
                    .uniqueResult();
        } catch (HibernateException ex) {
            log.error("Error al obtener tipoMoneda. ", ex);
            throw ex;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return (Double) tipoMonedaValores;
    }

    protected List<Adjuntos> obtenerAdjuntosPorOtRol(int otId, int rolId) throws Exception {
        try {

            Query query = sesion.createSQLQuery(
                    "SELECT * "
                            + "FROM ADJUNTOS A "
                            + "INNER JOIN ADJUNTOS_ROLES AR ON A.ID = AR.ADJUNTO_ID \n"
                            + "WHERE A.OT_ID = :otId AND AR.ROL_ID = :rolId"
            )
                    .addEntity(Adjuntos.class)
                    .setParameter("otId", otId)
                    .setParameter("rolId", rolId);

            List<Adjuntos> result = query.list();
            return result;
        } catch (HibernateException hExcep) {
            log.error("Error al obtener listado de Adjuntos. ", hExcep);
            throw hExcep;
        }
    }

    protected List<Adjuntos> obtenerAdjuntosPorOt(int otId) throws Exception {
        try {

            Query query = sesion.createSQLQuery(
                    "SELECT * "
                    + "FROM ADJUNTOS A "
                    + "INNER JOIN ADJUNTOS_ROLES AR ON A.ID = AR.ADJUNTO_ID \n"
                    + "WHERE A.OT_ID = :otId"
            )
                    .addEntity(Adjuntos.class)
                    .setParameter("otId", otId);

            List<Adjuntos> result = query.list();
            return result;
        } catch (HibernateException hExcep) {
            log.error("Error al obtener listado de Adjuntos. ", hExcep);
            throw hExcep;
        }
    }


    private List<LibroObras> obtenerLibrosDeObras(int otId) {
        List<LibroObras> listadoFinalLibrosDeObras = new ArrayList<>();
        try {
            List<LibroObras> listadoLibrosDeObras = (List<LibroObras>) sesion.createCriteria(LibroObras.class)
                    .setFetchMode("usuarioEjecutor", FetchMode.JOIN)
                    .add(Restrictions.eq("ot.id", otId))
                    .list();

            for (LibroObras libroDeObra : listadoLibrosDeObras) {
                Usuarios usuario = new Usuarios();
                usuario.setId(libroDeObra.getUsuarioEjecutor().getId());
                usuario.setNombres(libroDeObra.getUsuarioEjecutor().getNombres());
                libroDeObra.setUsuarioEjecutor(usuario);
                List<AdjuntosLibroObras> listadoAdjuntosLibroObras = new ArrayList<>();
                listadoAdjuntosLibroObras = (List<AdjuntosLibroObras>) sesion.createCriteria(AdjuntosLibroObras.class)
                        .add(Restrictions.eq("libroObra.id", libroDeObra.getId()))
                        .list();

                Set<AdjuntosLibroObras> listadoAdjuntosLibroObrasFinal = new HashSet<>();
                for (AdjuntosLibroObras adjuntosLibroObras : listadoAdjuntosLibroObras) {
                    listadoAdjuntosLibroObrasFinal.add(adjuntosLibroObras);
                }

                libroDeObra.setAdjuntos(listadoAdjuntosLibroObrasFinal);
                //Modificacion por error de parseo en frontend cuando existen saltos de lineas en las observaciones
                String obs = libroDeObra.getObservaciones().replace("\r", "").replace("\n", "");
                libroDeObra.setObservaciones(obs);
                listadoFinalLibrosDeObras.add(libroDeObra);
            }

        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Usuarios. ", ex);
            throw ex;
        }
        return listadoFinalLibrosDeObras;
    }

    private List<LibroObras> obtenerLibrosDeObrasCalidad(int otId) {
        List<LibroObras> listadoFinalLibrosDeObras = new ArrayList<>();
        try {
            List<LibroObras> listadoLibrosDeObras = (List<LibroObras>) sesion.createCriteria(LibroObras.class)
                    .setFetchMode("usuarioEjecutor", FetchMode.JOIN)
                    .add(Restrictions.eq("ot.id", otId))
                    .list();

            for (LibroObras libroDeObra : listadoLibrosDeObras) {
                String[] observacion = libroDeObra.getObservaciones().split("Calidad - Obs");
                if(observacion.length > 1){
                    Usuarios usuario = new Usuarios();
                    usuario.setId(libroDeObra.getUsuarioEjecutor().getId());
                    usuario.setNombres(libroDeObra.getUsuarioEjecutor().getNombres());
                    libroDeObra.setUsuarioEjecutor(usuario);
                    List<AdjuntosLibroObras> listadoAdjuntosLibroObras = new ArrayList<>();
                    listadoAdjuntosLibroObras = (List<AdjuntosLibroObras>) sesion.createCriteria(AdjuntosLibroObras.class)
                            .add(Restrictions.eq("libroObra.id", libroDeObra.getId()))
                            .list();

                    Set<AdjuntosLibroObras> listadoAdjuntosLibroObrasFinal = new HashSet<>();
                    for (AdjuntosLibroObras adjuntosLibroObras : listadoAdjuntosLibroObras) {
                        listadoAdjuntosLibroObrasFinal.add(adjuntosLibroObras);
                    }

                    libroDeObra.setAdjuntos(listadoAdjuntosLibroObrasFinal);
                    //Modificacion por error de parseo en frontend cuando existen saltos de lineas en las observaciones
                    String obs = libroDeObra.getObservaciones().replace("\r","").replace("\n","");
                    libroDeObra.setObservaciones(obs);
                    listadoFinalLibrosDeObras.add(libroDeObra);
                }
            }

        } catch (HibernateException ex) {
            log.error("Error al obtener listado de Usuarios. ", ex);
            throw ex;
        }
        return listadoFinalLibrosDeObras;
    }



    public int actualizarTrabajadorWorkfloEventoEjecucion(int otId, int eventoId, int usuarioId) throws Exception {
        List<WorkflowEventosEjecucion> lista = new ArrayList();
        int retorno = 0;
        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            lista = criteria.list();

            int idEventoProximo = 0;
            // Acualizo el registro
            for (WorkflowEventosEjecucion weeActual : lista) {
                if (weeActual.getId().getEventoId() == eventoId) {
                    weeActual.setUsuarioEjecutorId(usuarioId);
                    weeActual.setUsuarioId(usuarioId);
                    sesion.update(weeActual);
                    break;
                }
            }
            //Actualiza registro de usuario en tabla usuarios_ot
            SQLQuery sqlQuery = sesion.createSQLQuery("UPDATE usuarios_ot set usuario_id = :newUser WHERE evento_id = :eventoId and ot_id = :otId");
            sqlQuery.setParameter("newUser", usuarioId);
            sqlQuery.setParameter("eventoId", eventoId);
            sqlQuery.setParameter("otId", otId);
            sqlQuery.executeUpdate();
            sesion.flush();

            retorno = 1;

        } catch (HibernateException e) {
            throw e;
        }
        return retorno;
    }

    private List generarJsonLibrosDeObras(List<LibroObras> listadoLibroDeObras) {
        List listaLibrosDeObra = new ArrayList();
        Map mapaDetalleLibroObras = new HashMap();
        List listaAdjuntos = new ArrayList();
        Map mapaAdjuntos = new HashMap();
        int contLibrosDeObras = 0;
        for (LibroObras librosDeObras : listadoLibroDeObras) {
            mapaDetalleLibroObras = new HashMap();
            mapaDetalleLibroObras.put("id", librosDeObras.getId());
            mapaDetalleLibroObras.put("fechaCreacion", librosDeObras.getFechaCreacion());
            mapaDetalleLibroObras.put("observaciones", librosDeObras.getObservaciones());
            mapaDetalleLibroObras.put("creador", librosDeObras.getUsuarioEjecutor().getNombres());
            mapaDetalleLibroObras.put("latitud", librosDeObras.getLatitud());
            mapaDetalleLibroObras.put("longitud", librosDeObras.getLongitud());

            FormulariosSeguimientosCalidad formularioCalidad = obtenerSeguimientoCalidadPorLibroObraId(librosDeObras.getId());

            if(formularioCalidad != null){
                mapaDetalleLibroObras.put("estado", formularioCalidad.getEstado());
                mapaDetalleLibroObras.put("porcentaje", formularioCalidad.getPorcentaje());
                mapaDetalleLibroObras.put("categoria", formularioCalidad.getCategoria());
                mapaDetalleLibroObras.put("gantt", formularioCalidad.getGantt());
                mapaDetalleLibroObras.put("libroObra", formularioCalidad.getLibroObra());
                mapaDetalleLibroObras.put("documentoLegal", formularioCalidad.getDocumentoLegal());
                mapaDetalleLibroObras.put("materialAcopio", formularioCalidad.getMaterialAcopio());
                mapaDetalleLibroObras.put("cantidadPersonal", formularioCalidad.getCantidadPersonal());
                mapaDetalleLibroObras.put("cantidadAcreditado", formularioCalidad.getCantidadAcreditados());
                mapaDetalleLibroObras.put("nombresAcreditados", formularioCalidad.getNombresAcreditados());
                mapaDetalleLibroObras.put("region", formularioCalidad.getRegion());
                mapaDetalleLibroObras.put("tipoFormulario", formularioCalidad.getFormulariosCalidad().getTipoFormulario());
            }

            Set<AdjuntosLibroObras> listadoAdjuntosLibroObras = librosDeObras.getAdjuntos();
            if (listadoAdjuntosLibroObras.size() > 0) {
                listaAdjuntos = new ArrayList();
                for (AdjuntosLibroObras adjuntosLibroObras : listadoAdjuntosLibroObras) {
                    mapaAdjuntos = new HashMap();
                    mapaAdjuntos.put("nombreArchivo", adjuntosLibroObras.getNombre());
                    mapaAdjuntos.put("extension", adjuntosLibroObras.getExtension());
                    mapaAdjuntos.put("carpeta", adjuntosLibroObras.getCarpeta());
                    listaAdjuntos.add(mapaAdjuntos);
                }
                mapaDetalleLibroObras.put("librosDeObras", listaAdjuntos);
            }


            listaLibrosDeObra.add(mapaDetalleLibroObras);
        }
        return listaLibrosDeObra;
    }

    protected FormulariosSeguimientosCalidad obtenerSeguimientoCalidadPorLibroObraId(int libroObraId){

        FormulariosSeguimientosCalidad formulario = null;

        try{

            Criteria criteriaSeguimiento = sesion.createCriteria(FormulariosSeguimientosCalidad.class);
            criteriaSeguimiento.add(Restrictions.eq("libroObraId", libroObraId));
            formulario = (FormulariosSeguimientosCalidad) criteriaSeguimiento.uniqueResult();

        }catch(Exception e){
            log.error("Error al obtener seguimiento calidad",e);
            throw e;
        }
        return formulario;
    }

    private List generarJsonLibrosDeObrasCalidad(List<LibroObras> listadoLibroDeObras) {
        List listaLibrosDeObra = new ArrayList();
        Map mapaDetalleLibroObras = new HashMap();
        List listaAdjuntos = new ArrayList();
        Map mapaAdjuntos = new HashMap();
        int contLibrosDeObras = 0;
        for (LibroObras librosDeObras : listadoLibroDeObras) {
            mapaDetalleLibroObras = new HashMap();
            mapaDetalleLibroObras.put("id", librosDeObras.getId());
            mapaDetalleLibroObras.put("fechaCreacion", librosDeObras.getFechaCreacion());
            mapaDetalleLibroObras.put("observaciones", librosDeObras.getObservaciones());
            mapaDetalleLibroObras.put("creador", librosDeObras.getUsuarioEjecutor().getNombres());
            mapaDetalleLibroObras.put("latitud", librosDeObras.getLatitud());
            mapaDetalleLibroObras.put("longitud", librosDeObras.getLongitud());
            Set<AdjuntosLibroObras> listadoAdjuntosLibroObras = librosDeObras.getAdjuntos();
            if (listadoAdjuntosLibroObras.size() > 0) {
                listaAdjuntos = new ArrayList();
                for (AdjuntosLibroObras adjuntosLibroObras : listadoAdjuntosLibroObras) {
                    mapaAdjuntos = new HashMap();
                    mapaAdjuntos.put("nombreArchivo", adjuntosLibroObras.getNombre());
                    mapaAdjuntos.put("extension", adjuntosLibroObras.getExtension());
                    mapaAdjuntos.put("carpeta", adjuntosLibroObras.getCarpeta());
                    listaAdjuntos.add(mapaAdjuntos);
                }
                mapaDetalleLibroObras.put("librosDeObras", listaAdjuntos);
            }
            listaLibrosDeObra.add(mapaDetalleLibroObras);
        }
        return listaLibrosDeObra;
    }


    private String generarJsonAdjuntos(List<Adjuntos> listadoAdjuntos) {

        String jsonAdjuntos = "[";
        int contAdjuntos = 0;
        for (Adjuntos adjuntos : listadoAdjuntos) {
            String detalleAdjunto = "";
            if (contAdjuntos != 0) {
                detalleAdjunto = detalleAdjunto + ",";
            }
            detalleAdjunto = detalleAdjunto + "{\n" +
                    "  \"id\": "  + adjuntos.getId() + ",\n" +
                    "  \"nombreArchivo\": "  + "\"" + adjuntos.getNombre() + "\"" + ",\n" +
                    "  \"estension\": "   + "\"" + adjuntos.getExtension() + "\"" + ",\n" +
                    "  \"carpeta\": "   + "\"" + adjuntos.getCarpeta() + "\"" + "\n";


            detalleAdjunto = detalleAdjunto + "}";
            jsonAdjuntos = jsonAdjuntos + detalleAdjunto;
            contAdjuntos++;
        }
        jsonAdjuntos = jsonAdjuntos + "]";

        return jsonAdjuntos;
    }

    private String generarJsonUsuariosValidadores(List<UsuariosValidadores> listadoUsuariosValidadores) {

        String jsonAdjuntos = "[";
        int contAdjuntos = 0;
        for (UsuariosValidadores usuariosValidadores : listadoUsuariosValidadores) {
            String detalleAdjunto = "";
            if (contAdjuntos != 0) {
                detalleAdjunto = detalleAdjunto + ",";
            }
            detalleAdjunto = detalleAdjunto + "{\n" +
                    "  \"id\": "  + usuariosValidadores.getId() + ",\n" +
                    "  \"usuarios\": "  + "\"" + usuariosValidadores.getUsuarios().getId() + "\"" + ",\n" +
                    "  \"evento\": "   + "\"" + usuariosValidadores.getEventos().getId() + "\"" + ",\n" +
                    "  \"concepto\": "   + "\"" + usuariosValidadores.getConceptos().getId() + "\"" + "\n";


            detalleAdjunto = detalleAdjunto + "}";
            jsonAdjuntos = jsonAdjuntos + detalleAdjunto;
            contAdjuntos++;
        }
        jsonAdjuntos = jsonAdjuntos + "]";

        return jsonAdjuntos;
    }


    protected List<UsuariosValidadores> listarUsuariosValidadoresPorOt(int otId) {
        try {
            List<UsuariosValidadores> listado = (List<UsuariosValidadores>) sesion.createCriteria(UsuariosValidadores.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de UsuariosValidadores. ", ex);
            throw ex;
        }
    }

    protected Login retornarLoginSesionActual(String token) {
        try {
            Login login = (Login) sesion.createCriteria(Login.class)
                    .add(Restrictions.eq("token", token))
                    .uniqueResult();
            return login;
        } catch (HibernateException hibEx) {
            log.error("Error al validar credenciales de usuario. ", hibEx);
            throw hibEx;
        }
    }

    protected void registrarJournal(Journal journal) throws Exception {
//        RedisConnection<String, String> conn = (RedisConnection<String, String>) NewRedisUtil.getRedisClient().connect();
//        try{
//            ObjectMapper mapper = new ObjectMapper();
//            ObjectWriter ow = mapper.writer();
//            String valor = ow.writeValueAsString(journal);
//            //Se incrementa el valor del indice y se setea
//            SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//            String fecha = formatoDelTexto.format(journal.getFecha());
//            Long index = conn.hincrby("indexes","journal",1L);
//            conn.hsetnx("journal:traza","id:" + index,"fecha:" + fecha + "," + "token:" + journal.getToken() + ","
//                    + "path:" + journal.getPath());
//            conn.hsetnx("journal:detalle","id:" + index,valor);
//            conn.hsetnx("journal:lookup", "lookup:usuarioId:" + journal.getUsuarioId() + "token:" + journal.getToken()
//                    + ":date:" + fecha + ":path:" + journal.getPath(), String.valueOf(index));
//        }catch(Exception ex){
//            log.error("Error al registrar accion en journal.");
//            throw ex;
//        }finally{
//            conn.close();
//        }
    }

//    protected List obtenerReporteLineasPresupuestariasPorIDPMO(){
//        List listadoRetorno = null;
//        try{
//
//        }catch{
//
//        }finally{
//
//        }
//    }


    protected List<CubicadorServicios> obtenerCubicacionServiciosPorIdCubicador(int cubicadorId) {
        try {
            List<CubicadorServicios> listado = (List<CubicadorServicios>) sesion.createCriteria(CubicadorServicios.class)
                    .add(Restrictions.eq("cubicador.id", cubicadorId))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicacion servicios. ", ex);
            throw ex;
        }
    }

    protected List<CubicadorOrdinario> obtenerCubicadorOrdinarioPorIdCubicador(int cubicadorId) {
        try {
            List<CubicadorOrdinario> listado = (List<CubicadorOrdinario>) sesion.createCriteria(CubicadorOrdinario.class)
                    .add(Restrictions.eq("cubicador.id", cubicadorId))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicacion servicios. ", ex);
            throw ex;
        }
    }

    protected List<CubicadorDetalle> obtenerCubicacionDetallePorIdCubicador(int cubicadorId) {
        try {
            List<CubicadorDetalle> listado = (List<CubicadorDetalle>) sesion.createCriteria(CubicadorDetalle.class)
                    .add(Restrictions.eq("cubicadorId", cubicadorId))
                    .list();
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicacion servicios. ", ex);
            throw ex;
        }
    }
    protected TipoMoneda obtenertipoMonedaPorId(int tipoMonedaId) {
        try {
            TipoMoneda objeto = (TipoMoneda) sesion.createCriteria(TipoMoneda.class)
                    .add(Restrictions.eq("id", tipoMonedaId))
                    .uniqueResult();
            return objeto;
        } catch (HibernateException ex) {
            log.error("Error al obtener tipoMoneda. ", ex);
            throw ex;
        }
    }

    protected Long obtenerCodigoAcuerdoPorProveedor(int contratoID, int proveedorID)throws Exception{
        try{
            String consulta = "SELECT * FROM contratos_proveedores WHERE " +
                    "contrato_id = :contratoID AND proveedor_id = :proveedorID LIMIT 1";
            Query query = sesion.createSQLQuery(consulta)
                    .addScalar("codigo_acuerdo");
            query.setParameter("contratoID",contratoID);
            query.setParameter("proveedorID", proveedorID);
//            query.setMaxResults(1);
            Number codigoAcuerdo = (Number)query.uniqueResult();
            return codigoAcuerdo.longValue();
        }catch(HibernateException hibernateEx){
            log.error("Error al obtener codigoAcuerdoPorProveedor. ",hibernateEx);
            throw new Exception("Error al obtener codigoAcuerdoPorProveedor.");
        }
    }

    protected String obtenerGraficoGlobal(int graficoId, int usuarioId, int contratoId, int proveedorId, int anio, int mes)throws Exception{

        String respJSON ="";
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from retornar_json_global(:id_grafico, :id_usuario, :id_contrato, :id_proveedor, :ano, :mes)");
            sqlQuery.setParameter("id_grafico",graficoId);
            sqlQuery.setParameter("id_usuario",usuarioId);
            sqlQuery.setParameter("id_contrato",contratoId);
            sqlQuery.setParameter("id_proveedor",proveedorId);
            sqlQuery.setParameter("ano",anio);
            sqlQuery.setParameter("mes",mes);

            respJSON = (String)sqlQuery.uniqueResult();

        }catch(Exception e){
            log.error("Error al obtener data del grafico detalle",e);
        }
        return respJSON;
    }

    protected String obtenerGraficoDetalle(int graficoId, int usuarioId, String serie, String categoria1, String categoria2, String categoria3, int mes)throws Exception{

        String respJSON ="";
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from retornar_json_detalle(:id_grafico,:id_usuario,:serie,:categoria1,:categoria2,:categoria3,:mes)");
            sqlQuery.setParameter("id_grafico",graficoId);
            sqlQuery.setParameter("id_usuario",usuarioId);
            sqlQuery.setParameter("serie",serie);
            sqlQuery.setParameter("categoria1",categoria1);
            sqlQuery.setParameter("categoria2",categoria2);
            sqlQuery.setParameter("categoria3",categoria3);
            sqlQuery.setParameter("mes",mes);

            respJSON = (String)sqlQuery.uniqueResult();

        }catch(Exception e){
            log.error("Error al obtener data del grafico detalle",e);
        }
        return respJSON;
    }
    public Materiales obtieneMaterial(int idMaterial)throws Exception {
        Materiales materiales = new Materiales();
        try{
            materiales = (Materiales) sesion.createCriteria(Materiales.class)
                    .add(Restrictions.eq("id", idMaterial))
                    .uniqueResult();
            return materiales;
        }catch (Exception e){
            log.error("Error al obtener material con id "+ idMaterial +"  , error: "+ e.toString());
            throw e;
        }
    }
    public Servicios obtieneServicios(int idServicio)throws Exception {
        Servicios servicios = new Servicios();
        try{
            servicios = (Servicios) sesion.createCriteria(Servicios.class)
                    .add(Restrictions.eq("id", idServicio))
                    .uniqueResult();
            return servicios;
        }catch (Exception e){
            log.error("Error al obtener servicio con id "+ idServicio +"  , error: "+ e.toString());
            throw e;
        }
    }

    public boolean validarEjecucionDeEvento(int usuarioId, int eventoId, int otId)throws Exception{
        boolean ejecucionAutorizada = false;
        try{
            List<WorkflowEventosEjecucion> wee = new ArrayList<>();
            List<UsuariosOt> usuariosOtList = new ArrayList();

            Ot ot = (Ot) sesion.createCriteria(Ot.class)
                    .add(Restrictions.eq("id", otId)).uniqueResult();



            if(ot.getContrato().getId() == 4 && eventoId == 46){

                //Validar si usuario esta autorizado para ejecutar el evento
                usuariosOtList = (List<UsuariosOt>)sesion.createCriteria(UsuariosOt.class)
                        .add(Restrictions.eq("id.otId", otId))
                        .add(Restrictions.eq("id.eventoId",eventoId))
                        .add(Restrictions.eq("id.usuarioId",usuarioId))
                        .list();

                ejecucionAutorizada = (usuariosOtList.size() > 0)?true:false;

            } else {
                //Validar si evento requerido se encuentra en estado de ejecucion
                wee = (List)sesion.createCriteria(WorkflowEventosEjecucion.class)
                        .add(Restrictions.eq("ot.id", otId))
                        .add(Restrictions.eq("usuarioId",usuarioId))
                        .add(Restrictions.eq("ejecutado",9999))
                        .list();

                //Validar si usuario esta autorizado para ejecutar el evento
                usuariosOtList = (List<UsuariosOt>)sesion.createCriteria(UsuariosOt.class)
                        .add(Restrictions.eq("id.otId", otId))
                        .add(Restrictions.eq("id.eventoId",eventoId))
                        .add(Restrictions.eq("id.usuarioId",usuarioId))
                        .list();
                ejecucionAutorizada = (wee.size()>0 && usuariosOtList.size() > 0)?true:false;
            }






        } catch (Exception ex) {
            log.error("Error al validar ejecución de evento", ex);
            throw ex;
        }

        return ejecucionAutorizada;
    }

    private Session sesion;
    private Transaction trx;
    private static final Logger log = Logger.getLogger(Gateway.class);
    private static Config config = Config.getINSTANCE();

    public List<Login> obtieneLogueosAbiertosMovil(Usuarios usuario, int rolId, int tipoToken) {
        List<Login> listaLogin = new ArrayList<Login>();

        try {
            String estadoNuevo = "A";
            char estado = estadoNuevo.charAt(0);
            listaLogin = (List<Login>) sesion.createCriteria(Login.class)
                    .add(Restrictions.eq("usuarios.id", usuario.getId()))
                    .add(Restrictions.eq("roles.id", rolId))
                    .add(Restrictions.eq("estado", estado))
                    .add(Restrictions.eq("tipoToken", tipoToken))
                    .list();
            return listaLogin;
        } catch (Exception e) {
            log.error("Error al listar login activos moviles, error: " + e.toString());
            throw e;
        }
    }

    public void eliminaSesionMovilAbierta(Login login) {
        Login loginCerrado = new Login();
        Date date = new Date();
        try {
            String estadoNuevo = "C";
            char estado = estadoNuevo.charAt(0);
            loginCerrado.setId(login.getId());
            loginCerrado.setUsuarios(login.getUsuarios());
            loginCerrado.setRoles(login.getRoles());
            loginCerrado.setToken(login.getToken());
            loginCerrado.setFecha(login.getFecha());
            loginCerrado.setFechaUltimaAccion(date);
            loginCerrado.setEstado(estado);
            loginCerrado.setTipoToken(login.getTipoToken());

            login.setEstado(estado);

            sesion.update(login);
            sesion.flush();

        } catch (Exception e) {
            log.error("Error al cambiar de estado sesion movil, error: " + e.toString());
        }
    }

    protected String obtenerXmlOt(int otId) throws Exception {

        String respXML = "";
        try {

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from retornar_xml_otec(:id_ot)");
            sqlQuery.setParameter("id_ot", otId);

            respXML = (String)sqlQuery.uniqueResult();

        }catch(Exception e){
            log.error("Error al obtener XML ot",e);
        }
        return respXML;
    }

    protected String obtenerXmlActa(int actaId)throws Exception{

        String respXML ="";
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from retornar_xml_acta(:id_acta)");
            sqlQuery.setParameter("id_acta",actaId);

            respXML = (String)sqlQuery.uniqueResult();

        }catch(Exception e){
            log.error("Error al obtener XML acta",e);
        }
        return respXML;
    }

    protected Repositorios obtenerRepositorio(int idRepositorio) {
        try {
            Repositorios repo = (Repositorios) sesion.createCriteria(Repositorios.class)
                    .add(Restrictions.eq("id", idRepositorio))
                    .uniqueResult();
            return repo;
        } catch (HibernateException hibEx) {
            log.error("Error al obtener Repositorio. ", hibEx);
            throw hibEx;
        }
    }

    private String obtenerFiltrosGeBolsas(Map<String, String> listadoFiltros) {
        String condicionWhere = "WHERE 1 ";

        if(listadoFiltros != null && listadoFiltros.size() > 0){
            if(listadoFiltros.containsKey("contrato")){
                String valor = String.valueOf(listadoFiltros.get("contrato"));
                if(valor != null && !("null").equalsIgnoreCase(valor)){
                    condicionWhere = condicionWhere + "AND contrato ILIKE %" + valor + "%";
                }
            }
            if(listadoFiltros.containsKey("numeroContrato")){
                String valor = String.valueOf(listadoFiltros.get("numeroContrato"));
                condicionWhere = condicionWhere + "and numero_contrato = " + valor;
            }
            if(listadoFiltros.containsKey("idPmo")){
                String valor = String.valueOf(listadoFiltros.get("idPmo"));
                condicionWhere = condicionWhere + "and id_pmo = " + valor;
            }
            if(listadoFiltros.containsKey("lp")){
                String valor = String.valueOf(listadoFiltros.get("lp"));
                condicionWhere = condicionWhere + "and lp = " + valor;
            }
            if(listadoFiltros.containsKey("fechaIngreso")){
                String[] fechaCompleta = String.valueOf(listadoFiltros.get("fechaIngreso")).split("T");
                if(fechaCompleta != null){
                    condicionWhere = condicionWhere + "and DATE(fecha_ingreso) = '" + fechaCompleta[0]+ "'";
                }
            }
            if(listadoFiltros.containsKey("hemId")){
                String valor = String.valueOf(listadoFiltros.get("hemId"));
                condicionWhere = condicionWhere + "and hem_id = " + valor;
            }
            if(listadoFiltros.containsKey("derivadaId")){
                String valor = String.valueOf(listadoFiltros.get("derivadaId"));
                condicionWhere = condicionWhere + "and derivada_id = " + valor;
            }
            if(listadoFiltros.containsKey("fechaContable")){
                String[] fechaCompleta = String.valueOf(listadoFiltros.get("fechaContable")).split("T");
                if(fechaCompleta != null){
                    condicionWhere = condicionWhere + "and DATE(fecha_contable) = '" + fechaCompleta[0]+ "'";
                }
            }

        }

        return condicionWhere;
    }

    private String obtenerFiltrosGeDetalle(Map<String, String> listadoFiltros) {
        String condicionWhere = "WHERE 1 ";

        if(listadoFiltros != null && listadoFiltros.size() > 0){

            if(listadoFiltros.containsKey("id")){ // id ot
                String valor = String.valueOf(listadoFiltros.get("id"));
                condicionWhere = condicionWhere + "and id = " + valor;
            }
            if(listadoFiltros.containsKey("idActa")){
                String valor = String.valueOf(listadoFiltros.get("idActa"));
                condicionWhere = condicionWhere + "and id_acta = " + valor;
            }
            if(listadoFiltros.containsKey("validar")){
                String valor = String.valueOf(listadoFiltros.get("validar"));
                condicionWhere = condicionWhere + "and validar = " + valor;
            }
            if(listadoFiltros.containsKey("idMotivoRechazo")){
                String valor = String.valueOf(listadoFiltros.get("idMotivoRechazo"));
                condicionWhere = condicionWhere + "and id_motivo_rechazo = " + valor;
            }
            if(listadoFiltros.containsKey("fechaIngreso")){
                String[] fechaCompleta = String.valueOf(listadoFiltros.get("fechaIngreso")).split("T");
                if(fechaCompleta != null){
                    condicionWhere = condicionWhere + "and DATE(fecha_creacion) = '" + fechaCompleta[0]+ "'";
                }
            }
        }

        return condicionWhere;
    }

    protected List<DetalleBolsaGestionEconomica> obtenerBolsaPorOt(int otId, int usuarioId, int rolId, String estado)throws Exception{

        List<DetalleBolsaGestionEconomica> resultado = new ArrayList<DetalleBolsaGestionEconomica>();
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from buscar_bolsa_por_ot(:otid, :usuarioid, :estado_bolsa, :rolid)");
            sqlQuery.setParameter("otid",otId);
            sqlQuery.setParameter("usuarioid",usuarioId);
            sqlQuery.setParameter("estado_bolsa",estado);
            sqlQuery.setParameter("rolid",rolId);

            List<Object[]> listObject = (List<Object[]>) sqlQuery.list();
            if (listObject != null && !listObject.isEmpty()) {
                for(Object[] obj: listObject){
                    DetalleBolsaGestionEconomica bolsa = new DetalleBolsaGestionEconomica();
                    switch (rolId){
                        case 6:  //Pagos
                            /*bolsa.setIdPmo((obj[0]==null) ? 0 :(Integer) obj[0]);
                            bolsa.setNumeroContrato((obj[1]==null) ? new Long(0) : ((BigInteger) obj[1]).longValue());
                            bolsa.setLp((String) obj[2]);
                            bolsa.setPep2((String) obj[3]);
                            bolsa.setSubGerente((String) obj[4]);
                            bolsa.setContrato((String) obj[5]);
                            bolsa.setSitio((String) obj[6]);
                            bolsa.setMontoTotal((Double) obj[7]);
                            bolsa.setMontoActasAceptadas((Double) obj[8]);
                            bolsa.setMontoActasRechazadas((Double) obj[9]);
//                            bolsa.setIdSubgerente((obj[8]==null) ? 0 :(Integer) obj[8]);*/
                            break;

                        case 8: //PMO
                            bolsa.setIdPmo((obj[0]==null) ? 0 :(Integer) obj[0]);
                            bolsa.setNumeroContrato((obj[1]==null) ? new Long(0) : ((BigInteger) obj[1]).longValue());
                            bolsa.setLp((String) obj[2]);
                            bolsa.setContrato((String) obj[3]);
                            bolsa.setMontoTotal((Double) obj[4]);
                            bolsa.setMontoActasAceptadas((Double)obj[5]);
                            bolsa.setMontoActasRechazadas((Double)obj[6]);
                            break;

                        case 9: //Imputacion
                            bolsa.setIdPmo((obj[0]==null) ? 0 :(Integer) obj[0]);
                            bolsa.setNumeroContrato((obj[1]==null) ? new Long(0) : ((BigInteger) obj[1]).longValue());
                            bolsa.setLp((String) obj[2]);
                            bolsa.setContrato((String) obj[3]);
                            bolsa.setMontoTotal((Double) obj[4]);
                            bolsa.setMontoActasAceptadas((Double)obj[5]);
                            bolsa.setMontoActasRechazadas((Double)obj[6]);
                            break;
                    }

                    resultado.add(bolsa);
                }
            }

        }catch(Exception e){
            log.error("Error al obtener bolsa por id de ot",e);
            e.printStackTrace();
        }
        return resultado;
    }


    protected List<DetalleBolsaGestionEconomica> obtenerListaBolsasOt(int usuarioId, int rolId, int cicloFacturacionId, String estado)throws Exception{

        List<DetalleBolsaGestionEconomica> resultado = new ArrayList<DetalleBolsaGestionEconomica>();
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_bolsas_con_estado(:usuarioid, :ciclofacturacionid, :estado_bolsa, :rolid)");
            sqlQuery.setParameter("usuarioid",usuarioId);
            sqlQuery.setParameter("ciclofacturacionid",cicloFacturacionId);
            sqlQuery.setParameter("estado_bolsa",estado);
            sqlQuery.setParameter("rolid",rolId);

            List<Object[]> listObject = (List<Object[]>) sqlQuery.list();
            if (listObject != null && !listObject.isEmpty()) {
                for(Object[] obj: listObject){
                    DetalleBolsaGestionEconomica bolsa = new DetalleBolsaGestionEconomica();

                    switch (rolId){
                        case 6:  //Pagos
                            /*bolsa.setIdPmo((obj[0]==null) ? 0 :(Integer) obj[0]);
                            bolsa.setNumeroContrato((obj[1]==null) ? new Long(0) : ((BigInteger) obj[1]).longValue());
                            bolsa.setLp((String) obj[2]);
                            bolsa.setPep2((String) obj[3]);
                            bolsa.setSubGerente((String) obj[4]);
                            bolsa.setContrato((String) obj[5]);
                            bolsa.setSitio((String) obj[6]);
                            bolsa.setMontoTotal((Double) obj[7]);
                            bolsa.setMontoActasAceptadas((Double) obj[8]);
                            bolsa.setMontoActasRechazadas((Double) obj[9]);
//                            bolsa.setIdSubgerente((obj[8]==null) ? 0 :(Integer) obj[8]);*/
                            break;

                        case 8: //PMO
                            bolsa.setIdPmo((obj[0]==null) ? 0 :(Integer) obj[0]);
                            bolsa.setNumeroContrato((obj[1]==null) ? new Long(0) : ((BigInteger) obj[1]).longValue());
                            bolsa.setLp((String) obj[2]);
                            /*bolsa.setPep2((String) obj[3]);
                            bolsa.setSubGerente((String) obj[4]);*/
                            bolsa.setContrato((String) obj[5]);
                            /*bolsa.setSitio((String) obj[6]);*/
                            bolsa.setMontoTotal((Double) obj[7]);
                            bolsa.setMontoActasAceptadas((Double)obj[8]);
                            bolsa.setMontoActasRechazadas((Double)obj[9]);
                            //bolsa.setIdSubgerente((obj[8]==null) ? 0 :(Integer) obj[8]);
                            bolsa.setFechaIngreso((obj[11]==null) ? null :(Date) obj[11]);
                            bolsa.setMoneda((String) obj[12]);
                            bolsa.setProveedor((String) obj[13]);
                            break;

                        case 9: //Imputacion
                            bolsa.setIdPmo((obj[0]==null) ? 0 :(Integer) obj[0]);
                            bolsa.setNumeroContrato((obj[1]==null) ? new Long(0) : ((BigInteger) obj[1]).longValue());
                            bolsa.setLp((String) obj[2]);
                            bolsa.setPep2((String) obj[3]);
                            /*bolsa.setSubGerente((String) obj[4]);*/
                            bolsa.setContrato((String) obj[5]);
                            /*bolsa.setSitio((String) obj[6]);*/
                            bolsa.setMontoTotal((Double) obj[7]);
                            bolsa.setMontoActasAceptadas((Double) obj[8]);
                            bolsa.setMontoActasRechazadas((Double) obj[9]);
                            //bolsa.setIdSubgerente((obj[8]==null) ? 0 :(Integer) obj[8]);
                            bolsa.setFechaIngreso((obj[11]==null) ? null :(Date) obj[11]);
                            bolsa.setMoneda((String) obj[12]);
                            bolsa.setProveedor((String) obj[13]);
                            break;
                    }

                    resultado.add(bolsa);
                }
            }

        }catch(Exception e){
            log.error("Error al obtener listado de bolsas ot",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected List<DetalleBolsaGestionEconomica> obtenerListaBolsasOtPagadas(int usuarioId, int rolId)throws Exception{

        List<DetalleBolsaGestionEconomica> resultado = new ArrayList<DetalleBolsaGestionEconomica>();
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_bolsas_pagadas(:usuarioid, :rolid)");
            sqlQuery.setParameter("usuarioid",usuarioId);
            sqlQuery.setParameter("rolid",rolId);

            List<Object[]> listObject = (List<Object[]>) sqlQuery.list();
            if (listObject != null && !listObject.isEmpty()) {
                for(Object[] obj: listObject){
                    DetalleBolsaGestionEconomica bolsa = new DetalleBolsaGestionEconomica();
                    Pagos pagoBolsa = new Pagos();
                    switch (rolId){

                        case 8: //PMO
                            bolsa.setContrato((String) obj[0]);
                            bolsa.setNumeroContrato((obj[1]==null) ? new Long(0) : ((BigInteger) obj[1]).longValue());
                            bolsa.setIdPmo((obj[2]==null) ? 0 :(Integer) obj[2]);
                            bolsa.setLp((String) obj[3]);
                            bolsa.setMontoTotal((Double) obj[4]);
                            pagoBolsa.setMonto((obj[5]==null) ? 0 :(Double) obj[5]);
                            bolsa.setFechaIngreso((obj[6]==null) ? null :(Date) obj[6]);
                            bolsa.setBolsaId((obj[7]==null) ? 0 :(Integer) obj[7]);
                            pagoBolsa.setFechaPago((obj[8]==null) ? null :(Date) obj[8]);
                            pagoBolsa.setHemId((obj[9]==null) ? new Long(0) : ((BigInteger) obj[9]).longValue());
                            pagoBolsa.setDerivadaId((obj[10]==null) ? new Long(0) : ((BigInteger) obj[10]).longValue());
                            pagoBolsa.setFechaContable((obj[11]==null) ? null :(Date) obj[11]);
                            bolsa.setMoneda((String) obj[12]);
                            bolsa.setProveedor((String) obj[13]);
                            break;

                        case 9: //Imputacion
                            bolsa.setContrato((String) obj[0]);
                            bolsa.setNumeroContrato((obj[1]==null) ? new Long(0) : ((BigInteger) obj[1]).longValue());
                            bolsa.setIdPmo((obj[2]==null) ? 0 :(Integer) obj[2]);
                            bolsa.setLp((String) obj[3]);
                            bolsa.setMontoTotal((Double) obj[4]);
                            pagoBolsa.setMonto((obj[5]==null) ? 0 :(Double) obj[5]);
                            bolsa.setFechaIngreso((obj[6]==null) ? null :(Date) obj[6]);
                            bolsa.setBolsaId((obj[7]==null) ? 0 :(Integer) obj[7]);
                            pagoBolsa.setFechaPago((obj[8]==null) ? null :(Date) obj[8]);
                            pagoBolsa.setHemId((obj[9]==null) ? new Long(0) : ((BigInteger) obj[9]).longValue());
                            pagoBolsa.setDerivadaId((obj[10]==null) ? new Long(0) : ((BigInteger) obj[10]).longValue());
                            pagoBolsa.setFechaContable((obj[11]==null) ? null :(Date) obj[11]);
                            bolsa.setMoneda((String) obj[12]);
                            bolsa.setProveedor((String) obj[13]);
                            break;
                    }
                    bolsa.setPago(pagoBolsa);
                    resultado.add(bolsa);
                }
            }

        }catch(Exception e){
            log.error("Error al obtener listado de bolsas pagadas",e);
            e.printStackTrace();
        }
        return resultado;
    }

    //Retorna json solo para pagos gestion economica
    protected String obtenerListaBolsasOtPagos(int usuarioId, String estado)throws Exception{

        String resultado = null;
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from retornar_json_bolsas_pagos(:usuarioid, :estado_bolsa)");
            sqlQuery.setParameter("usuarioid",usuarioId);
            sqlQuery.setParameter("estado_bolsa",estado);
            resultado = (String) sqlQuery.uniqueResult();

        }catch(Exception e){
            log.error("Error al obtener listado de bolsas ot",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected List<DetalleOtPorBolsa> obtenerListaOtsPorBolsa(int usuarioId, DetalleBolsaGestionEconomica bolsa, int rolId)throws Exception{

        List<DetalleOtPorBolsa> resultado = new ArrayList<DetalleOtPorBolsa>();
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_ots_por_bolsa(:usuarioid, :id_pmo_, :numero_contrato_, :lp_, :pep2_, :subgerente_, :contrato_, :sitio_, :rolid)");
            sqlQuery.setParameter("usuarioid",usuarioId);
            sqlQuery.setParameter("id_pmo_",bolsa.getIdPmo());
            sqlQuery.setParameter("numero_contrato_",(bolsa.getNumeroContrato()==null) ? 0L :bolsa.getNumeroContrato());
            sqlQuery.setParameter("lp_",(bolsa.getLp()==null) ? "" : bolsa.getLp());
            sqlQuery.setParameter("pep2_",(bolsa.getPep2()==null) ? "" :bolsa.getPep2());
            sqlQuery.setParameter("subgerente_",bolsa.getIdSubgerente());
            sqlQuery.setParameter("contrato_",(bolsa.getContrato()==null) ? "" :bolsa.getContrato());
            sqlQuery.setParameter("sitio_",(bolsa.getSitio()==null) ? "" :bolsa.getSitio());
            sqlQuery.setParameter("rolid", rolId);

            List<Object[]> listObject = (List<Object[]>) sqlQuery.list();
            if (listObject != null && !listObject.isEmpty()) {
                for(Object[] obj: listObject){
                    DetalleOtPorBolsa otPorBolsa = new DetalleOtPorBolsa();

                    otPorBolsa.setIdPmo((Integer) obj[0]);
                    otPorBolsa.setLp((String) obj[1]);
                    otPorBolsa.setMonto((Double) obj[2]);
                    otPorBolsa.setOt((Integer) obj[3]);

                    resultado.add(otPorBolsa);
                }
            }

        }catch(Exception e){
            log.error("Error al obtener listado de bolsas ot",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected List<DetalleOtPorBolsa> obtenerListaOtsPorBolsaPagos(int usuarioId, String jsonActas)throws Exception{

        List<DetalleOtPorBolsa> resultado = new ArrayList<DetalleOtPorBolsa>();
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_ots_por_bolsa_pag(:usuario_id_, :json_actas_)");
            sqlQuery.setParameter("usuario_id_",usuarioId);
            sqlQuery.setParameter("json_actas_",jsonActas);

            List<Object[]> listObject = (List<Object[]>) sqlQuery.list();
            if (listObject != null && !listObject.isEmpty()) {
                for(Object[] obj: listObject){
                    DetalleOtPorBolsa otPorBolsa = new DetalleOtPorBolsa();

                    otPorBolsa.setIdPmo((Integer) obj[0]);
                    otPorBolsa.setLp((String) obj[1]);
                    otPorBolsa.setMonto((Double) obj[2]);
                    otPorBolsa.setOt((Integer) obj[3]);

                    resultado.add(otPorBolsa);
                }
            }

        }catch(Exception e){
            log.error("Error al obtener listado de bolsas ot",e);
            e.printStackTrace();
        }
        return resultado;
    }

    /*protected DetalleBolsaGestionEconomica obtenerBolsaPorId(int bolsaId, int usuarioId, int rolId)throws Exception{

        DetalleBolsaGestionEconomica resultado = new DetalleBolsaGestionEconomica();

        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from obtener_bolsa_por_id(:usuarioid, :bolsaid)");
            sqlQuery.setParameter("usuarioid",usuarioId);
            sqlQuery.setParameter("bolsaid",bolsaId);

            Object[] obj = (Object[]) sqlQuery.uniqueResult();
            if (obj != null) {
                log.debug("salida*******");
                log.debug(obj[0]);
                log.debug(obj[1]);

                log.debug(obj[2]);
                log.debug(obj[3]);

                log.debug(obj[4]);
                log.debug(obj[5]);
                log.debug(obj[6]);

                switch (rolId){
                    case 6:  //Pagos
                        resultado.setIdBolsa((obj[0]==null) ? 0 :(Integer) obj[0]);
                        resultado.setCodigoCicloFacturacion((String) obj[1]);
                        resultado.setIdPmo((obj[2]==null) ? 0 :(Integer) obj[2]);
                        resultado.setNumeroContrato((obj[3]==null) ? new Long(0) : ((BigInteger) obj[3]).longValue());
                        resultado.setLp((String) obj[4]);
                        resultado.setPep2((String) obj[5]);
                        resultado.setSubGerente((String) obj[6]);
                        resultado.setContrato((String) obj[7]);
                        resultado.setSitio((String) obj[8]);
                        resultado.setMontoTotal((Double) obj[9]);
                        break;

                    case 8: //PMO
                        resultado.setIdBolsa((obj[0]==null) ? 0 :(Integer) obj[0]);
                        resultado.setCodigoCicloFacturacion((String) obj[1]);
                        resultado.setIdPmo((obj[2]==null) ? 0 :(Integer) obj[2]);
                        //resultado.setNumeroContrato((obj[3]==null) ? new Long(0) : ((BigInteger) obj[3]).longValue());
                        resultado.setLp((String) obj[4]);
                        //resultado.setPep2((String) obj[5]);
                        //resultado.setSubGerente((String) obj[6]);
                        //resultado.setContrato((String) obj[7]);
                        //resultado.setSitio((String) obj[8]);
                        resultado.setMontoTotal((Double) obj[9]);
                        break;

                    case 9: //Imputacion
                        resultado.setIdBolsa((obj[0]==null) ? 0 :(Integer) obj[0]);
                        resultado.setCodigoCicloFacturacion((String) obj[1]);
                        resultado.setIdPmo((obj[2]==null) ? 0 :(Integer) obj[2]);
                        //resultado.setNumeroContrato((obj[3]==null) ? new Long(0) : ((BigInteger) obj[3]).longValue());
                        resultado.setLp((String) obj[4]);
                        resultado.setPep2((String) obj[5]);
                        //resultado.setSubGerente((String) obj[6]);
                        //resultado.setContrato((String) obj[7]);
                        //resultado.setSitio((String) obj[8]);
                        resultado.setMontoTotal((Double) obj[9]);
                        break;
                }
            }

        }catch(Exception e){
            log.error("Error al obtener listado de bolsas ot",e);
            e.printStackTrace();
        }
        return resultado;
    }*/

    protected List<OtDetalleBolsasGE> obtenerListaDetalleOtsPorBolsa(int usuarioId, DetalleBolsaGestionEconomica bolsa, String estado, int rolId)throws Exception{

        List<OtDetalleBolsasGE> resultado = new ArrayList<OtDetalleBolsasGE>();
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_detalle_ots_por_bolsa(:usuarioid, :id_pmo_, :numero_contrato_, :lp_, :pep2_, :moneda_, :estado_, :rolid)");
            sqlQuery.setParameter("usuarioid",usuarioId);
            sqlQuery.setParameter("id_pmo_",bolsa.getIdPmo());
            sqlQuery.setParameter("numero_contrato_",(bolsa.getNumeroContrato()==null) ? 0L :bolsa.getNumeroContrato());
            sqlQuery.setParameter("lp_",(bolsa.getLp()==null) ? "" : bolsa.getLp());
            sqlQuery.setParameter("pep2_",(bolsa.getPep2()==null) ? "" :bolsa.getPep2());
            sqlQuery.setParameter("moneda_",bolsa.getMoneda());
            sqlQuery.setParameter("estado_",estado);
            sqlQuery.setParameter("rolid",rolId);

            List<Object[]> listObject = (List<Object[]>) sqlQuery.list();
            if (listObject != null && !listObject.isEmpty()) {
                for(Object[] obj: listObject){
                    OtDetalleBolsasGE detalleOtPorBolsa = new OtDetalleBolsasGE();

                    if(obj.length>8){
                        Integer idMotivoRechazo = (Integer)obj[8];
                        if(idMotivoRechazo!=null){
                            MotivosRechazosGestionEconomica mr = (MotivosRechazosGestionEconomica)this.retornarObjetoCoreOT(MotivosRechazosGestionEconomica.class, idMotivoRechazo);
                            MotivosRechazosGestionEconomica motivoRechazoGestionEconomica = new MotivosRechazosGestionEconomica(mr.getId(),mr.getNombre(),mr.getDescripcion(),mr.getCodigoAreaRechazoGe());
                            detalleOtPorBolsa.setMotivoRechazo(motivoRechazoGestionEconomica);
                        }
                    }

                    detalleOtPorBolsa.setId((Integer) obj[0]);
                    detalleOtPorBolsa.setIdActa((Integer) obj[1]);
                    detalleOtPorBolsa.setNombre((String) obj[2]);
                    detalleOtPorBolsa.setFechaInicio((Date) obj[3]);
                    detalleOtPorBolsa.setFechaFin((Date) obj[4]);
                    detalleOtPorBolsa.setMonto((obj[5] == null) ? 0 : (Double) obj[5]);
                    detalleOtPorBolsa.setValidar((obj[6] == null) ? false : (Boolean) obj[6]);
                    detalleOtPorBolsa.setCantidadServicios((Integer) obj[7]);
                    detalleOtPorBolsa.setMontoPagar((obj[9] == null) ? 0 : (Double) obj[9]);
                    detalleOtPorBolsa.setPorcentajePagar((String) obj[10]);

                    if(rolId!=6){
                        detalleOtPorBolsa.setUsuarioPagadorId(usuarioId);
                    }

                    resultado.add(detalleOtPorBolsa);
                }
            }

        }catch(Exception e){
            log.error("Error al obtener listado de bolsas ot",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected List<OtDetalleBolsasGE> obtenerListaDetalleOtsPorBolsaPagadas(int usuarioId, DetalleBolsaGestionEconomica bolsa, int rolId)throws Exception{

        List<OtDetalleBolsasGE> resultado = new ArrayList<OtDetalleBolsasGE>();
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_detalle_ots_por_bolsa_pagadas(:usuarioid, :id_pmo_, :lp_, :bolsa_id_, :rolid)");
            sqlQuery.setParameter("usuarioid",usuarioId);
            sqlQuery.setParameter("id_pmo_",bolsa.getIdPmo());
            sqlQuery.setParameter("lp_",(bolsa.getLp()==null) ? "" : bolsa.getLp());
            sqlQuery.setParameter("bolsa_id_",bolsa.getBolsaId());
            sqlQuery.setParameter("rolid",rolId);

            List<Object[]> listObject = (List<Object[]>) sqlQuery.list();
            if (listObject != null && !listObject.isEmpty()) {
                for(Object[] obj: listObject){
                    OtDetalleBolsasGE detalleOtPorBolsa = new OtDetalleBolsasGE();

                    detalleOtPorBolsa.setId((Integer) obj[0]);
                    detalleOtPorBolsa.setIdActa((Integer) obj[1]);
                    detalleOtPorBolsa.setNombre((String) obj[2]);
                    detalleOtPorBolsa.setFechaInicio((Date) obj[3]);
                    detalleOtPorBolsa.setFechaFin((Date) obj[4]);
                    detalleOtPorBolsa.setMonto((obj[5] == null) ? 0 : (Double) obj[5]);
                    detalleOtPorBolsa.setValidar((obj[6] == null) ? false : (Boolean) obj[6]);
                    detalleOtPorBolsa.setCantidadServicios((Integer) obj[7]);
                    detalleOtPorBolsa.setMontoPagar((obj[9] == null) ? 0 : (Double) obj[9]);
                    detalleOtPorBolsa.setPorcentajePagar((String) obj[10]);

                    resultado.add(detalleOtPorBolsa);
                }
            }

        }catch(Exception e){
            log.error("Error al obtener listado de bolsas ot pagadas",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected List<OtDetalleBolsasGE> obtenerListaDetalleOtsPorBolsaPagos(int usuarioId, String jsonActas)throws Exception{

        List<OtDetalleBolsasGE> resultado = new ArrayList<OtDetalleBolsasGE>();
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_detalle_ots_por_bolsa_pag(:usuario_id_, :json_actas_)");
            sqlQuery.setParameter("usuario_id_",usuarioId);
            sqlQuery.setParameter("json_actas_",jsonActas);

            List<Object[]> listObject = (List<Object[]>) sqlQuery.list();
            if (listObject != null && !listObject.isEmpty()) {
                for(Object[] obj: listObject){
                    OtDetalleBolsasGE detalleOtPorBolsa = new OtDetalleBolsasGE();

                    if(obj.length>8){
                        Integer idMotivoRechazo = (Integer)obj[8];
                        if(idMotivoRechazo!=null){
                            MotivosRechazosGestionEconomica mr = (MotivosRechazosGestionEconomica)this.retornarObjetoCoreOT(MotivosRechazosGestionEconomica.class, idMotivoRechazo);
                            MotivosRechazosGestionEconomica motivoRechazoGestionEconomica = new MotivosRechazosGestionEconomica(mr.getId(),mr.getNombre(),mr.getDescripcion(),mr.getCodigoAreaRechazoGe());
                            detalleOtPorBolsa.setMotivoRechazo(motivoRechazoGestionEconomica);
                        }
                    }
                    detalleOtPorBolsa.setId((Integer) obj[0]);
                    detalleOtPorBolsa.setIdActa((Integer) obj[1]);
                    detalleOtPorBolsa.setNombre((String) obj[2]);
                    detalleOtPorBolsa.setFechaInicio((Date) obj[3]);
                    detalleOtPorBolsa.setFechaFin((Date) obj[4]);
                    detalleOtPorBolsa.setMonto((obj[5]==null) ? 0 :(Double) obj[5]);
                    detalleOtPorBolsa.setValidar((obj[6]==null) ? false :(Boolean) obj[6]);
                    detalleOtPorBolsa.setCantidadServicios((Integer) obj[7]);
                    detalleOtPorBolsa.setMontoPagar((obj[9]==null) ? 0 :(Double) obj[9]);
                    detalleOtPorBolsa.setPorcentajePagar((String) obj[10]);
                    detalleOtPorBolsa.setUsuarioPagadorId((obj[11]==null) ? 0 :(Integer) obj[11]);
                    detalleOtPorBolsa.setUsuarioPagadorNombre((String) obj[12]);

                    resultado.add(detalleOtPorBolsa);
                }
            }

        }catch(Exception e){
            log.error("Error al obtener listado de bolsas ot",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected List<ServiciosOt> obtenerListaDetalleServiciosPorActa(int actaId)throws Exception{

        List<ServiciosOt> resultado = new ArrayList<ServiciosOt>();
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_detalle_servicios_por_acta(:actaid)");
            sqlQuery.setParameter("actaid",actaId);

            List<Object[]> listObject = (List<Object[]>) sqlQuery.list();
            if (listObject != null && !listObject.isEmpty()) {
                for(Object[] obj: listObject){
                    ServiciosOt detalleServicioOt = new ServiciosOt();

                    detalleServicioOt.setId((Integer) obj[0]);
                    detalleServicioOt.setNombre((String) obj[1]);
                    detalleServicioOt.setMontoUnitario((obj[2] == null) ? 0 : (Double) obj[2]);
                    detalleServicioOt.setMontoTotal((obj[3] == null) ? 0 : (Double) obj[3]);
                    detalleServicioOt.setCantidad((obj[4] == null) ? 0 : (Double) obj[4]);

                    resultado.add(detalleServicioOt);
                }
            }

        }catch(Exception e){
            log.error("Error al obtener listado de bolsas ot",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected List<Object[]> obtenerListaReporteOtExcel(int usuarioId)throws Exception{
        List<Object[]> resultado = null;
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_reporte_ot_excel(:usuario_id_)");
            sqlQuery.setParameter("usuario_id_",usuarioId);

            resultado = (List<Object[]>) sqlQuery.list();

        }catch(Exception e){
            log.error("Error al obtener listado reporte ot",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected List<Object[]> obtenerHojasPorReporte(int reporteId)throws Exception{
        List<Object[]> listaHojas = new ArrayList();
        try{

            Query query = sesion.createSQLQuery("SELECT rh.rep_hoja_id, h.nombre, h.columnas, h.query FROM rep_reporte_hoja rh, rep_hoja h WHERE rh.rep_reporte_id = :reporteid AND rh.rep_hoja_id = h.id ORDER BY rh.rep_hoja_id ASC");
            query.setParameter("reporteid",reporteId);

            listaHojas = query.list();

        }catch(Exception e){
            log.error("Error al obtener hojas por reporte ot",e);
            e.printStackTrace();
        }
        return listaHojas;
    }

    protected List<Object[]> obtenerHojaReporte(int idHoja, int usuarioId)throws Exception{
        List<Object[]> resultado = new ArrayList();

        try{
            String queryHoja = obtenerQueryHojaReporte(idHoja);

            SQLQuery sqlQuery = sesion.createSQLQuery(queryHoja);
            sqlQuery.setParameter("usuarioid",usuarioId);

            resultado = (List<Object[]>) sqlQuery.list();

        }catch(Exception e){
            log.error("Error al obtener hoja reporte ot",e);
            e.printStackTrace();
//            throw e;
        }
        return resultado;
    }

    protected Object[] obtenerReportePorRol (int rolId) throws Exception{
        Object[] reporteIdRol = null;
        try{
            Query query = sesion.createSQLQuery("SELECT re.id, re.nombre FROM rep_reporte_rol rr, rep_reporte re WHERE rr.rep_reporte_id = re.id AND rr.rol_id = :rol_id");
            query.setParameter("rol_id", rolId);
            reporteIdRol = (Object[])query.uniqueResult();

        }catch (HibernateException e){
            log.error("Error al obtener reporte por rol",e);
            e.printStackTrace();
        }
        return  reporteIdRol;
    }

    protected boolean tieneRol (int rolId, int usuarioId) throws Exception{
        boolean salida = false;
        try{
            Query query = sesion.createSQLQuery("SELECT exists(SELECT 1 FROM usuarios_roles ur WHERE ur.rol_id = :rolid AND ur.usuario_id = :usuarioid)");
            query.setParameter("rolid", rolId);
            query.setParameter("usuarioid", usuarioId);
            salida = (Boolean) query.uniqueResult();

        }catch (HibernateException e){
            log.error("Error al validar rol",e);
            e.printStackTrace();
        }
        return  salida;
    }

    protected String obtenerQueryHojaReporte(int idHoja)throws Exception{
        String resultado = null;
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT query FROM rep_hoja WHERE id = :idhoja");
            sqlQuery.setParameter("idhoja",idHoja);

            resultado = sqlQuery.uniqueResult().toString();

        }catch(Exception e){
            log.error("Error al obtener Query hoja reporte ot",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected List<Object[]> obtenerListaReporteOtTipoServicioExcel(int usuarioId)throws Exception{
        List<Object[]> resultado = null;
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_reporte_ot_tipo_servicio_excel(:usuario_id_)");
            sqlQuery.setParameter("usuario_id_",usuarioId);

            resultado = (List<Object[]>) sqlQuery.list();

        }catch(Exception e){
            log.error("Error al obtener listado reporte tipo servicio ot",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected List<Object[]> obtenerListaReporteOtServicioExcel(int usuarioId)throws Exception{
        List<Object[]> resultado = null;
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_reporte_ot_servicios_excel(:usuario_id_)");
            sqlQuery.setParameter("usuario_id_",usuarioId);

            resultado = (List<Object[]>) sqlQuery.list();

        }catch(Exception e){
            log.error("Error al obtener listado reporte servicio ot",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected List<Object[]> obtenerListaReporteOtActividadesPlazosExcel(int usuarioId)throws Exception{
        List<Object[]> resultado = null;
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_reporte_ot_actividades_plazos_excel(:usuario_id_)");
            sqlQuery.setParameter("usuario_id_",usuarioId);

            resultado = (List<Object[]>) sqlQuery.list();

        }catch(Exception e){
            log.error("Error al obtener listado reporte actividades plazos ot",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected List<Object[]> obtenerListaReporteOtContratoActividadExcel(int usuarioId)throws Exception{
        List<Object[]> resultado = null;
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_reporte_ot_contrato_actividad_excel(:usuario_id_)");
            sqlQuery.setParameter("usuario_id_",usuarioId);

            resultado = (List<Object[]>) sqlQuery.list();

        }catch(Exception e){
            log.error("Error al obtener listado reporte contrato actividad ot",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected List<Object[]> obtenerListaReporteGestionEconomicaExcel(int usuarioId, String estado, int rolId)throws Exception{
        List<Object[]> resultado = null;
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_reporte_gestion_economica_excel(:usuario_id_, :estado, :rol_id_)");
            sqlQuery.setParameter("usuario_id_",usuarioId);
            sqlQuery.setParameter("estado",estado);
            sqlQuery.setParameter("rol_id_",rolId);

            resultado = (List<Object[]>) sqlQuery.list();

        }catch(Exception e){
            log.error("Error al obtener listado reporte Gestion Economica",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected List<Object[]> obtenerListaReporteGestionEconomicaExcelPagadas(int usuarioId, int rolId)throws Exception{
        List<Object[]> resultado = null;
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_reporte_gestion_economica_excel_pagadas(:usuario_id_, :rol_id_)");
            sqlQuery.setParameter("usuario_id_",usuarioId);
            sqlQuery.setParameter("rol_id_",rolId);

            resultado = (List<Object[]>) sqlQuery.list();

        }catch(Exception e){
            log.error("Error al obtener listado reporte Gestion Economica Pagadas",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected List<Object[]> obtenerListaReporteGestionEconomicaExcelDash(int usuarioId, int rolId, boolean flagAprobadas, boolean flagGlobal)throws Exception{
        List<Object[]> resultado = null;
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from listar_reporte_gestion_economica_excel_dash(:usuario_id_, :rol_id_, :flag_aprobadas, :flag_global)");
            sqlQuery.setParameter("usuario_id_",usuarioId);
            sqlQuery.setParameter("rol_id_",rolId);
            sqlQuery.setParameter("flag_aprobadas",flagAprobadas);
            sqlQuery.setParameter("flag_global",flagGlobal);

            resultado = (List<Object[]>) sqlQuery.list();

        }catch(Exception e){
            log.error("Error al obtener listado reporte Gestion Economica Dash",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected void actualizarBolsas(String jsonBolsa, int usuarioId, int rolId)throws Exception{
        try{
            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from actualizar_temporal_detalle_bolsas_validaciones(:json_detalle_bolsa, :usuarioid, :rolid)");
            sqlQuery.setParameter("json_detalle_bolsa",jsonBolsa);
            sqlQuery.setParameter("usuarioid",usuarioId);
            sqlQuery.setParameter("rolid",rolId);
            String resultado = (String)sqlQuery.uniqueResult();

        }catch(Exception e){
            log.error("Error al actualizar bolsa de validacion PMO", e);
            throw new Exception("Error al actualizar bolsa de validacion PMO");
        }
    }

    protected void validarBolsas(String jsonBolsas, int usuarioId, int rolId)throws Exception{
        try{
            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from validar_bolsas(:json_detalle_bolsas, :usuarioid, :rolid)");
            sqlQuery.setParameter("json_detalle_bolsas",jsonBolsas);
            sqlQuery.setParameter("usuarioid",usuarioId);
            sqlQuery.setParameter("rolid",rolId);
            String resultado = (String)sqlQuery.uniqueResult();

        }catch(Exception e){
            log.error("Error al validar bolsas",e);
            throw new Exception("Error al validar bolsas");
        }
    }

    public String obtenerXmlActaDummy(int actaId)throws Exception{
        abrirSesion();
        String respXML ="";
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from retornar_xml_acta(:id_acta)");
            sqlQuery.setParameter("id_acta",actaId);

            respXML = (String)sqlQuery.uniqueResult();

        }catch(Exception e){
            log.error("Error al obtener XML acta",e);
        }
        return respXML;
    }
    public List<Regiones> obtieneRegiones() {
        List<Regiones> regionesList = new ArrayList<Regiones>();
        try{
            regionesList = (List)sesion.createCriteria(Regiones.class).list();
            return regionesList;
        }catch(Exception e){
            log.error("Error al obtener Regiones, error: ",e);
            throw e;
        }
    }
    public List<Actividades> obtieneActividades() {
        List<Actividades> actividadesList = new ArrayList<Actividades>();
        try{
            actividadesList = (List)sesion.createCriteria(Actividades.class).list();
            return actividadesList;
        }catch(Exception e){
            log.error("Error al obtener actividades, error:  ",e);
            throw e;
        }
    }

    protected CheckList obtenerCheckList(int contratoId) {
        try {
            CheckList checkListConfiguracion = (CheckList) sesion.createCriteria(CheckList.class)
                    .add(Restrictions.eq("contratos.id", contratoId))
                    .uniqueResult();
            return checkListConfiguracion;
        } catch (HibernateException hibEx) {
            log.error("Error al obtener checkListConfiguracion. ", hibEx);
            throw hibEx;
        }
    }


    protected CheckListOt obtenerCheckListOtPorId(int checkListOtId) {
        try {
            CheckListOt checkListOt = (CheckListOt) sesion.createCriteria(CheckListOt.class)
                    .add(Restrictions.eq("id", checkListOtId))
                    .uniqueResult();
            return checkListOt;
        } catch (HibernateException hibEx) {
            log.error("Error al obtener checkListConfiguracion. ", hibEx);
            throw hibEx;
        }
    }


    protected List<CheckListOtDetalle> obtenerCheckListOtDetalle(int checkListOtId) {

        List<CheckListOtDetalle> listado = new ArrayList<>();
        List<CheckListOtDetalle> listadoFinal = new ArrayList<>();

        try {
            listado = (List<CheckListOtDetalle>) sesion.createCriteria(CheckListOtDetalle.class)
                    .add(Restrictions.eq("checkListOt.id", checkListOtId)).list();


            for (CheckListOtDetalle checkListOtDetalle: listado){

                if(checkListOtDetalle.getOpcionId() == 1){
                    CheckListOt checkListOt = new CheckListOt();
                    checkListOt.setId(checkListOtDetalle.getCheckListOt().getId());
                    checkListOtDetalle.setCheckListOt(checkListOt);
                    listadoFinal.add(checkListOtDetalle);
                }
            }



            return listadoFinal;
        } catch (HibernateException hibEx) {
            log.error("Error al obtener checkListConfiguracion. ", hibEx);
            throw hibEx;
        }
    }


    protected List<CheckListOtAux> obtenerCheckListOtPorOtID(int otId) {

        List<CheckListOt> listado = new ArrayList<>();
        List<CheckListOtAux> listadoFinal = new ArrayList<>();
        try {

            listado = (List<CheckListOt>) sesion.createCriteria(CheckListOt.class)
                    .add(Restrictions.eq("empresacolaboradora", false))
                    .add(Restrictions.eq("ot.id", otId)).list();

            for (CheckListOt checkListOt: listado){
                List<CheckListOtDetalleAux> listadoCheckListOtDetalle = new ArrayList<>();
                CheckListOtAux checkListOtAux = new CheckListOtAux();
                Ot ot = new Ot();
                ot.setId(checkListOt.getOt().getId());
                checkListOtAux.setOt(ot);
                checkListOtAux.setId(checkListOt.getId());
                checkListOtAux.setFechaCreacion(checkListOt.getFechaCreacion());
                Usuarios usuario = new Usuarios();
                usuario.setId(checkListOt.getUsuario().getId());
                try {
                    usuario = obtenerUsuarioPorId(usuario.getId());
                }catch (Exception ex){
                    log.error("No se pudo obtener los datos del usuario. ", ex);
                }

                checkListOtAux.setUsuario(usuario);
                checkListOtAux.setObservacion(checkListOt.getObservacion());
                checkListOtAux.setEmpresacolaboradora(checkListOt.isEmpresacolaboradora());

                List<CheckListOtDetalle> listadoCheckListOtDetalleAux = (List<CheckListOtDetalle>) sesion.createCriteria(CheckListOtDetalle.class)
                        .add(Restrictions.eq("checkListOt.id", checkListOt.getId())).list();

                List<LibroObras> listadoFinalLibrosDeObras = new ArrayList<>();

                for (CheckListOtDetalle checkListOtDetalle: listadoCheckListOtDetalleAux){
                    CheckListOtDetalleAux checkListOtDetalleAux = new CheckListOtDetalleAux();
                    checkListOtDetalleAux.setId(checkListOtDetalle.getId());
                    checkListOtDetalleAux.setItem(checkListOtDetalle.getItem());
                    String jsonAdjuntos = "[";
                    if(checkListOtDetalle.getLibroObra() != null){

                        checkListOtDetalleAux.setLibroObraId(checkListOtDetalle.getLibroObra().getId());
                        List<AdjuntosLibroObras> listadoAdjuntosLibroObras = new ArrayList<>();

                        listadoAdjuntosLibroObras = (List<AdjuntosLibroObras>) sesion.createCriteria(AdjuntosLibroObras.class)
                                .add(Restrictions.eq("libroObra.id", checkListOtDetalle.getLibroObra().getId()))
                                .list();

                        for (AdjuntosLibroObras adjuntosLibroObras : listadoAdjuntosLibroObras) {
                            int id = adjuntosLibroObras.getId();
                            String nombre = adjuntosLibroObras.getNombre();
                            String carpeta = adjuntosLibroObras.getCarpeta();
                            String descripcion = adjuntosLibroObras.getDescripcion();
                            if(("[").equalsIgnoreCase(jsonAdjuntos)){
                                jsonAdjuntos = jsonAdjuntos + "{\n" +
                                        "  \"id\": "  + "\"" + id + "\"" + ",\n" +
                                        "  \"nombre\": "  + "\"" + nombre + "\"" + ",\n" +
                                        "  \"carpeta\": "  + "\"" + carpeta + "\"" + ",\n" +
                                        "  \"descripcion\": "  + "\"" + descripcion + "\"" +
                                        "}";
                            }else{
                                jsonAdjuntos = jsonAdjuntos + ",{\n" +
                                        "  \"id\": "  + "\"" + id + "\"" + ",\n" +
                                        "  \"nombre\": "  + "\"" + nombre + "\"" + ",\n" +
                                        "  \"carpeta\": "  + "\"" + carpeta + "\"" + ",\n" +
                                        "  \"descripcion\": "  + "\"" + descripcion + "\"" +
                                        "}";
                            }

                        }
                    }

                    jsonAdjuntos = jsonAdjuntos + "]";

                    checkListOtDetalleAux.setAdjuntos(jsonAdjuntos);
                    checkListOtDetalleAux.setObservacion(checkListOtDetalle.getObservacion());
                    checkListOtDetalleAux.setOpcionId(checkListOtDetalle.getOpcionId());
                    checkListOtDetalleAux.setOpcionNombre(checkListOtDetalle.getOpcionNombre());
                    listadoCheckListOtDetalle.add(checkListOtDetalleAux);
                }

                checkListOtAux.setCheckListOtDetalle(listadoCheckListOtDetalle);
                listadoFinal.add(checkListOtAux);
            }

            return listadoFinal;
        } catch (HibernateException hibEx) {
            log.error("Error al obtener checkListConfiguracion. ", hibEx);
            throw hibEx;
        }
    }



    protected int registrarCheckListOt(CheckListOt checkListOt) {
        int registro = 0;
        int idCheckListOt = 0;
        try {
            registro = (Integer) sesion.save(checkListOt);
            sesion.flush();
            idCheckListOt = checkListOt.getId();
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + checkListOt.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return idCheckListOt;
    }

    protected void actualizarPuntuacionCheckListOt(int idCheckListOT, int puntuacion) {
        int registro = 0;
        CheckListOt checkListOt = new CheckListOt();
        try {
            checkListOt = (CheckListOt) sesion.createCriteria(CheckListOt.class)
                    .add(Restrictions.eq("id", idCheckListOT)).uniqueResult();

            checkListOt.setPuntuacionFinal(puntuacion);
            sesion.update(checkListOt);
            sesion.flush();
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + checkListOt.getClass().toString());
            System.out.println(e);
            throw e;
        }

    }


    protected boolean actualizarCheckListOtDetale(int idCheckListOt, int idCheckListOtDetalle, int idLibroObras) {
        boolean actualizo = false;
        List<CheckListOtDetalle> listado = new ArrayList<>();
        try {
            listado = (List<CheckListOtDetalle>) sesion.createCriteria(CheckListOtDetalle.class)
                    .add(Restrictions.eq("id", idCheckListOtDetalle))
                    .add(Restrictions.eq("checkListOt.id", idCheckListOt)).list();

            for (CheckListOtDetalle checkListOt: listado){
                LibroObras libroObras = new LibroObras();
                libroObras.setId(idLibroObras);
                checkListOt.setLibroObra(libroObras);

                sesion.update(checkListOt);
            }
            sesion.flush();
            actualizo = true;
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto CheckListOtDetalle");
            System.out.println(e);
            throw e;
        }
        return actualizo;
    }


    protected int registrarCheckListOtDetalle(CheckListOtDetalle checkListOtDetalle) {
        int registro = 0;
        int idCheckListOtDetalle = 0;
        try {
            registro = (Integer) sesion.save(checkListOtDetalle);
            sesion.flush();
            idCheckListOtDetalle = checkListOtDetalle.getId();
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + checkListOtDetalle.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return idCheckListOtDetalle;
    }
    protected boolean asignarUsuarioPagos(int usuarioId, int idUsuarioAsignado, List<DetalleBolsasValidacionesPag> listadoOt) throws Exception {
        boolean actualizar = false;
        int eventoPago = 7;

        try {

            for (DetalleBolsasValidacionesPag detalle: listadoOt){

                Criteria criteria = sesion.createCriteria(DetalleBolsasValidacionesPag.class);
                criteria.add(Restrictions.eq("otId", detalle.getOtId()));
                criteria.add(Restrictions.eq("actaId", detalle.getActaId()));
//                criteria.add(Restrictions.eq("validacionActa", false));
                DetalleBolsasValidacionesPag bolsa = (DetalleBolsasValidacionesPag) criteria.uniqueResult();

                if(bolsa != null){
                    bolsa.setUsuarioPagadorId(idUsuarioAsignado);
                    bolsa.setUsuarioValidadorId(usuarioId);
                    bolsa.setFechaValidacionActa(new Date());
                    sesion.update(bolsa);

                }else{
                    bolsa = new DetalleBolsasValidacionesPag();
                    bolsa.setActaId(detalle.getActaId());
                    bolsa.setOtId(detalle.getOtId());
                    bolsa.setUsuarioPagadorId(idUsuarioAsignado);
                    bolsa.setUsuarioValidadorId(usuarioId);
                    bolsa.setFechaValidacionActa(new Date());
                    bolsa.setValidacionParcialActa(true);
                    bolsa.setValidacionActa(null);
                    bolsa.setMotivoRechazoGestionEconomicaId(null);
                    sesion.save(bolsa);
                }

                WorkflowEventosEjecucion workf = obtenerWorkflowEventosEjecucionPorEventoYOt2(eventoPago,detalle.getOtId());
                workf.setUsuarioEjecutorId(idUsuarioAsignado);
                workf.setUsuarioId(idUsuarioAsignado);

            }

            actualizar = true;

        } catch (Exception e) {
            System.out.println("No se pudo actualizar asignar el usuario a la bolsa en pagos");
            throw e;
        }
        return actualizar;
    }

    public List<Especialidades> obtieneEspecialidades(int actividadId) {
        List<Especialidades> especialidadesList = new ArrayList<Especialidades>();
        Especialidades especialidades = new Especialidades();
        try {
            List serviciosServiciosUnidadList = new ArrayList<>();

            SQLQuery sqlQuery = sesion.createSQLQuery("select especialidad_id from servicios_servicios_unidad WHERE actividad_id = :actididad_id GROUP BY especialidad_id");
            sqlQuery.setParameter("actididad_id", actividadId);
            serviciosServiciosUnidadList = (List) sqlQuery.list();

            for (int i = 0; i < serviciosServiciosUnidadList.size(); i++) {
                especialidades = new Especialidades();
                int especialidadId = (Integer) serviciosServiciosUnidadList.get(i);
                especialidades = (Especialidades) sesion.createCriteria(Especialidades.class)
                        .add(Restrictions.eq("id", especialidadId)).uniqueResult();
                especialidadesList.add(especialidades);
            }


            return especialidadesList;
        } catch (Exception e) {
            log.error("Error al obtener especialidades, error : ", e);
            throw e;
        }
    }

    public List<HashMap> listarServiciosUnidadPorServicioYActividad(String codigoServicio, int actividadId) throws HibernateException {

        List<ServiciosServiciosUnidad> listadoServiciosServiciosUnidad = new ArrayList<>();
        List<ServiciosUnidad> listadoServiciosUnidad = new ArrayList<>();
        List<ServiciosUnidadMateriales> listadoServiciosUnidadMateriales = new ArrayList<>();
        List<Materiales> listadoMateriales = new ArrayList<>();

        List<HashMap> listadoServicioUnidadMapa = new ArrayList<>();

        try {

            // obtengo listadoServiciosServiciosUnidad SELECT codigo_unidad_obra from servicios_servicios_unidad WHERE codigo_mano_obra = 'R103';
            Criteria criteria = sesion.createCriteria(ServiciosServiciosUnidad.class);
            criteria.add(Restrictions.eq("codigoManoObra", codigoServicio));
            criteria.add(Restrictions.eq("actividades.id", actividadId));

            listadoServiciosServiciosUnidad = criteria.list();

            // obtengo los servicio_unidad SELECT codigo from servicios_unidad WHERE codigo in (SELECT codigo_unidad_obra from servicios_servicios_unidad WHERE codigo_mano_obra = 'R103');

            if (listadoServiciosServiciosUnidad.size() > 0) {

                for (ServiciosServiciosUnidad serviciosServiciosUnidad : listadoServiciosServiciosUnidad) {

                    String codigoUnidadDeObra = serviciosServiciosUnidad.getCodigoUnidadObra();
                    HashMap servicioUnidadMapa = new HashMap();

                    if (!"0".equalsIgnoreCase(codigoUnidadDeObra.trim())) {

                        // obtengo los datos del servicio unidad
                        ServiciosUnidad serviciosUnidad = (ServiciosUnidad) sesion.createCriteria(ServiciosUnidad.class)
                                .add(Restrictions.eq("codigo", codigoUnidadDeObra))
                                .uniqueResult();

                        // lleno el mapa de respuesta
                        servicioUnidadMapa.put("id", serviciosUnidad.getId());

                        // obtengo el tipo de medida
                        int tipoUnidadMedidaServicio = serviciosUnidad.getTipoUnidadMedidaId();

                        TipoUnidadMedida tipoUnidadMedida = (TipoUnidadMedida) sesion.createCriteria(TipoUnidadMedida.class)
                                .add(Restrictions.eq("id", tipoUnidadMedidaServicio))
                                .uniqueResult();

                        servicioUnidadMapa.put("tipoUnidadMedidaServicio", tipoUnidadMedida.getNombre());
                        servicioUnidadMapa.put("nombre", serviciosUnidad.getNombre());
                        servicioUnidadMapa.put("descripcion", serviciosUnidad.getDescripcion());
                        servicioUnidadMapa.put("codigo", serviciosUnidad.getCodigo());
                        servicioUnidadMapa.put("clave", serviciosUnidad.getClave());


                        //SELECT * from servicios_unidad_materiales WHERE codigo_unidad_obra in (SELECT codigo from servicios_unidad WHERE codigo = 'A002');
                        listadoServiciosUnidadMateriales = (List) sesion.createCriteria(ServiciosUnidadMateriales.class)
                                .add(Restrictions.eq("codigoUnidadObra", codigoUnidadDeObra))
                                .list();
                        double precio = 0;
                        if (listadoServiciosUnidadMateriales.size() > 0) {
                            // Obtengo los materiales SELECT * from materiales WHERE codigo in (SELECT codigo_material from servicios_unidad_materiales WHERE codigo_unidad_obra in (SELECT codigo from servicios_unidad WHERE codigo in (SELECT codigo_unidad_obra from servicios_servicios_unidad WHERE codigo_mano_obra = 'R103')));
                            List<HashMap> listadoMaterialesContratistaServiciosUnidad = new ArrayList<>();
                            List<HashMap> listadoMaterialesCTCServiciosUnidad = new ArrayList<>();
                            for (ServiciosUnidadMateriales serviciosUnidadMateriales : listadoServiciosUnidadMateriales) {
                                double cantidadMaterial = serviciosUnidadMateriales.getCantidadMaterial();
                                String codigoMaterial = serviciosUnidadMateriales.getCodigoMaterial();

                                // obtengo el detalle del material
                                listadoMateriales = (List) sesion.createCriteria(Materiales.class)
                                        .add(Restrictions.eq("codigo", codigoMaterial))
                                        .list();
                                HashMap materialMap = new HashMap();

                                if (listadoMateriales.size() > 0) {

                                    for (Materiales material : listadoMateriales) {

                                        materialMap.put("id", material.getId());
//                                        materialMap.put("familia", material.getFamilia().getId());
                                        // obtengo el tipo de medida

                                        // obtengo el tipo de medida
                                        int tipoUnidadMedidaMateriales = material.getTipoUnidadMedida().getId();

                                        TipoUnidadMedida tipoUnidadMedidaMaterial = (TipoUnidadMedida) sesion.createCriteria(TipoUnidadMedida.class)
                                                .add(Restrictions.eq("id", tipoUnidadMedidaMateriales))
                                                .uniqueResult();

                                        materialMap.put("unidadMedida", tipoUnidadMedidaMaterial.getNombre());

                                        int tipoMonedaMaterial = material.getTipoMoneda().getId();
                                        TipoMoneda tipoMoneda = (TipoMoneda) sesion.createCriteria(TipoMoneda.class)
                                                .add(Restrictions.eq("id", tipoMonedaMaterial))
                                                .uniqueResult();
                                        materialMap.put("tipoMoneda", tipoMoneda.getNombre());
                                        materialMap.put("tipoMonedaId", tipoMoneda.getId());
                                        materialMap.put("codigo", material.getCodigo());
                                        materialMap.put("codigoSap", material.getCodigoSap());
                                        materialMap.put("nombre", material.getNombre());
                                        materialMap.put("descripcion", material.getDescripcion());
                                        materialMap.put("tipoCambio", retornarValorMonedaEnPesos(tipoMoneda.getId()));
                                        materialMap.put("distribuidor", material.getDistribuidor());
                                        // para obtener el precio multiplico por la cantidad material de la tabla serviciosUnidadMateriales
                                        materialMap.put("precio", (double) Math.round(material.getPrecio() * cantidadMaterial * 100) / 100);
                                        materialMap.put("precioPesos", retornarPrecioEnPesosChilenos(material.getPrecio(), tipoMoneda.getId()) * cantidadMaterial);
                                        precio = precio + retornarPrecioEnPesosChilenos(material.getPrecio(), tipoMoneda.getId()) * cantidadMaterial;
                                    }
                                }
                                String ctc = (String) materialMap.get("distribuidor");
                                if ("CTC".equalsIgnoreCase(ctc.trim())) {

                                    listadoMaterialesCTCServiciosUnidad.add(materialMap);

                                } else {

                                    listadoMaterialesContratistaServiciosUnidad.add(materialMap);
                                }
                            }
                            servicioUnidadMapa.put("precio", precio);
                            servicioUnidadMapa.put("materialesCTC", listadoMaterialesCTCServiciosUnidad);
                            servicioUnidadMapa.put("materialesContratista", listadoMaterialesContratistaServiciosUnidad);
                        }

                        listadoServicioUnidadMapa.add(servicioUnidadMapa);
                    }


                }


            }

            return listadoServicioUnidadMapa;

        } catch (HibernateException e) {
            log.error("Error al obtener especialidades, error : ", e);
            System.out.println("Error al obtener servicios_unidad, error : " + e.getCause());
            throw e;
        }
    }

    public double obtenerPrecioServicioUnidadBucle(ServiciosUnidad serviciosUnidad) {

        double precio = 0d;
        String codigoUnidadDeObra = serviciosUnidad.getCodigo();
        if (!"0".equalsIgnoreCase(codigoUnidadDeObra.trim())) {

            // obtengo el tipo de medida
            int tipoUnidadMedidaServicio = serviciosUnidad.getTipoUnidadMedidaId();

            TipoUnidadMedida tipoUnidadMedida = (TipoUnidadMedida) sesion.createCriteria(TipoUnidadMedida.class)
                    .add(Restrictions.eq("id", tipoUnidadMedidaServicio))
                    .uniqueResult();

            List<ServiciosUnidadMateriales> listadoServiciosUnidadMateriales = (List) sesion.createCriteria(ServiciosUnidadMateriales.class)
                    .add(Restrictions.eq("codigoUnidadObra", codigoUnidadDeObra))
                    .list();

            if (listadoServiciosUnidadMateriales.size() > 0) {

                for (ServiciosUnidadMateriales serviciosUnidadMateriales : listadoServiciosUnidadMateriales) {
                    double cantidadMaterial = serviciosUnidadMateriales.getCantidadMaterial();
                    String codigoMaterial = serviciosUnidadMateriales.getCodigoMaterial();

                    // obtengo el detalle del material
                    List<Materiales> listadoMateriales = (List) sesion.createCriteria(Materiales.class)
                            .add(Restrictions.eq("codigo", codigoMaterial))
                            .list();

                    if (listadoMateriales.size() > 0) {

                        for (Materiales material : listadoMateriales) {

                            // obtengo el tipo de medida
                            int tipoUnidadMedidaMateriales = material.getTipoUnidadMedida().getId();

                            TipoUnidadMedida tipoUnidadMedidaMaterial = (TipoUnidadMedida) sesion.createCriteria(TipoUnidadMedida.class)
                                    .add(Restrictions.eq("id", tipoUnidadMedidaMateriales))
                                    .uniqueResult();

                            int tipoMonedaMaterial = material.getTipoMoneda().getId();
                            TipoMoneda tipoMoneda = (TipoMoneda) sesion.createCriteria(TipoMoneda.class)
                                    .add(Restrictions.eq("id", tipoMonedaMaterial))
                                    .uniqueResult();

                            precio = precio + retornarPrecioEnPesosChilenos(material.getPrecio(),material.getTipoMoneda().getId()) * cantidadMaterial;
                        }
                    }
                }
            }
        }

        return precio;

    }


    public List<HashMap> listarServiciosUnidadPorActividadGateways(int actividadId, int contratoId, List serviciosList) throws HibernateException {
//       Inicializan todos los Mapas a utilizar
        List<HashMap> servicioUnidadMapaCompleto = new ArrayList<>();
        HashMap servicioUnidadMapa = new HashMap();
        HashMap unidadMedidaServicio = new HashMap();
        HashMap unidadMedidamaterial = new HashMap();
        HashMap servicioUnidadMaterialesMapa = new HashMap();
        HashMap materialMap = new HashMap();
        HashMap tipoMonedaMap = new HashMap();

//        Inicializan los objetos
        List<ServiciosUnidad> serviciosUnidadList = new ArrayList<ServiciosUnidad>();
        List serviciosServiciosUnidadList = new ArrayList<>();
        Servicios servicios = new Servicios();
        TipoUnidadMedida tipoUnidadMedidaServicio = new TipoUnidadMedida();
        TipoUnidadMedida tipoUnidadMedidaMaterial = new TipoUnidadMedida();
        Materiales materiales = new Materiales();
        TipoMoneda tipoMoneda = new TipoMoneda();

        List nombreTipoUnidadMedidaMaterialList = new ArrayList<>();
        String nombreTipoUnidadMedidaMaterial = "";


        try {
            for (int i = 0; serviciosList.size() > i; i++) {
                servicios = new Servicios();
                int servicioId = Integer.parseInt(serviciosList.get(i).toString());
                Criteria criteria = sesion.createCriteria(Servicios.class);
                criteria.add(Restrictions.eq("id", servicioId));
                servicios = (Servicios) criteria.uniqueResult();

                serviciosServiciosUnidadList = new ArrayList<>();

                SQLQuery sqlQuery = sesion.createSQLQuery("SELECT codigo_unidad_obra from servicios_servicios_unidad where actividad_id = :actividad and codigo_mano_obra = :codigoManoObra");
                sqlQuery.setParameter("actividad", actividadId);
                sqlQuery.setParameter("codigoManoObra", servicios.getCodigo());
                serviciosServiciosUnidadList = (List) sqlQuery.list();
                String codigo = "";
                if (serviciosServiciosUnidadList.size() > 0) {
                    for (int u = 0; serviciosServiciosUnidadList.size() > u; u++) {
                        codigo = serviciosServiciosUnidadList.get(u).toString();
                        serviciosUnidadList = new ArrayList<>();
                        Criteria criteria3 = sesion.createCriteria(ServiciosUnidad.class);
//                                .setFetchMode("serviciosUnidadMaterialeses", FetchMode.JOIN);
                        criteria3.add(Restrictions.eq("codigo", codigo));
                        serviciosUnidadList = (List<ServiciosUnidad>) criteria3.list();
                        sesion.flush();

                        if (serviciosUnidadList.size() > 0 && !codigo.replace(" ", "").equals("0")) {
                            for (ServiciosUnidad serviciosUnidad : serviciosUnidadList) {
//                                Se reinician los mapas y Objetos
                                unidadMedidaServicio = new HashMap();
                                servicioUnidadMaterialesMapa = new HashMap();
                                materialMap = new HashMap();
                                servicioUnidadMapa = new HashMap();

//                                Se obtiene el tipo unidad de medida del servicio
                                tipoUnidadMedidaServicio = new TipoUnidadMedida();
                                Criteria criteriaTipoMedida = sesion.createCriteria(TipoUnidadMedida.class);
                                criteriaTipoMedida.add(Restrictions.eq("id", serviciosUnidad.getTipoUnidadMedidaId()));
                                tipoUnidadMedidaServicio = (TipoUnidadMedida) criteriaTipoMedida.uniqueResult();
                                sesion.flush();

//                                Se setea la unidad de medida del servicio en el mapa unidad de medida servicio
                                unidadMedidaServicio.put("id", tipoUnidadMedidaServicio.getId());
                                unidadMedidaServicio.put("codigo", tipoUnidadMedidaServicio.getCodigo());
                                unidadMedidaServicio.put("nombre", tipoUnidadMedidaServicio.getNombre());


//                                Se obtienen los servicios unidad materiales por codigo de mano de obra
                                ServiciosUnidadMateriales serviciosUnidadMateriales = new ServiciosUnidadMateriales();
                                Criteria prueba = sesion.createCriteria(ServiciosUnidadMateriales.class);
                                prueba.add(Restrictions.eq("codigoUnidadObra", codigo));
                                serviciosUnidadMateriales = (ServiciosUnidadMateriales) prueba.uniqueResult();
                                sesion.flush();

                                if (serviciosUnidadMateriales != null) {


                                    //                                Se prueba los datos obtenidos
                                    System.out.println("**************************************************************************************************");
                                    System.out.println("Este es el objeto:      " + serviciosUnidadMateriales.getId());
                                    //                                System.out.println(serviciosUnidadMateriales.get(1));
                                    //                                System.out.println(serviciosUnidadMateriales.get(1));

                                    //                                Se carga el material asociado a la unidad de servicio material
                                    materiales = new Materiales();
                                    Criteria criteriaMaterial = sesion.createCriteria(Materiales.class)
                                            .setFetchMode("tipoMoneda", FetchMode.JOIN)
                                            .setFetchMode("tipoUnidadMedida", FetchMode.JOIN);
                                    criteriaMaterial.setFetchMode("tipoUnidadMedida", FetchMode.JOIN);
                                    criteriaMaterial.add(Restrictions.eq("codigo", serviciosUnidadMateriales.getCodigoMaterial()));
                                    materiales = (Materiales) criteriaMaterial.uniqueResult();
                                    sesion.flush();

                                    //                                Se setea el nombre de unidad de medida del material asociado
                                    nombreTipoUnidadMedidaMaterialList = new ArrayList<>();
                                    int unidadMedidaId = materiales.getTipoUnidadMedida().getId();
                                    SQLQuery sqlQuery2 = sesion.createSQLQuery("SELECT nombre from tipo_unidad_medida where id = :tipoUniMedId and estado = 'A'");
                                    sqlQuery2.setParameter("tipoUniMedId", unidadMedidaId);
                                    nombreTipoUnidadMedidaMaterialList = (List) sqlQuery2.list();
                                    nombreTipoUnidadMedidaMaterial = nombreTipoUnidadMedidaMaterialList.get(0).toString();

                                    unidadMedidamaterial = new HashMap();
                                    unidadMedidamaterial.put("id", unidadMedidaId);
                                    unidadMedidamaterial.put("nombre", nombreTipoUnidadMedidaMaterial);


//                                    Se setea mapa de tipo moneda
                                    tipoMonedaMap = new HashMap();
                                    tipoMonedaMap.put("id", materiales.getTipoMoneda().getId());
                                    tipoMonedaMap.put("nombre", materiales.getTipoMoneda().getNombre());
                                    tipoMonedaMap.put("descripcion", materiales.getTipoMoneda().getDescripcion());


//                                Se prepara el mapa completo de material
                                    materialMap.put("id", materiales.getId());
                                    materialMap.put("familia", materiales.getFamilia().getId());
                                    materialMap.put("unidadMedida", unidadMedidamaterial);
                                    materialMap.put("tipoMoneda", tipoMonedaMap);
                                    materialMap.put("codigo", materiales.getCodigo());
                                    materialMap.put("codigoSap", materiales.getCodigoSap());
                                    materialMap.put("nombre", materiales.getNombre());
                                    materialMap.put("descripcion", materiales.getDescripcion());
                                    materialMap.put("precio", materiales.getPrecio());


                                    //                                Se setea el detalle completo en el mapa servicio unidad material
                                    servicioUnidadMaterialesMapa.put("id", serviciosUnidadMateriales.getId());
                                    servicioUnidadMaterialesMapa.put("codigoUnidadObra", serviciosUnidad.getCodigo());
                                    servicioUnidadMaterialesMapa.put("codigoMaterial", serviciosUnidadMateriales.getCodigoMaterial());
                                    servicioUnidadMaterialesMapa.put("cantidadMaterial", serviciosUnidadMateriales.getCantidadMaterial());
                                    servicioUnidadMaterialesMapa.put("materialDetalle", materialMap);

//                                    Trataremos de obtener el precio xd


                                }
//                                Se setea el detalle completo del servicio obtenido
                                servicioUnidadMapa.put("id", serviciosUnidad.getId());
                                servicioUnidadMapa.put("tipoUnidadMedidaServicio", unidadMedidaServicio);
                                servicioUnidadMapa.put("nombre", serviciosUnidad.getNombre());
                                servicioUnidadMapa.put("descripcion", serviciosUnidad.getDescripcion());
                                servicioUnidadMapa.put("codigo", serviciosUnidad.getCodigo());
                                servicioUnidadMapa.put("clave", serviciosUnidad.getClave());
                                servicioUnidadMapa.put("detalleUnidadMaterialesMapa", servicioUnidadMaterialesMapa);

                                servicioUnidadMapaCompleto.add(servicioUnidadMapa);
                            }

                        }
                    }
                }
            }

            return servicioUnidadMapaCompleto;
        } catch (HibernateException e) {
            log.error("Error al obtener especialidades, error : ", e);
            System.out.println("Error al obtener servicios_unidad, error : " + e.getCause());
            throw e;
        }
    }

    protected Integer registrarAdjuntosLibroObrasCalidad(AdjuntosLibroObras adjunto) throws Exception {
        int registro = 0;
        try {
            registro = (Integer) sesion.save(adjunto);
            sesion.flush();
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + adjunto.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return registro;
    }

    protected Integer registrarLibroObrasCalidad(LibroObras libro) throws Exception {
        int registro = 0;
        try {
            registro = (Integer) sesion.save(libro);
            sesion.flush();
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + libro.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return registro;
    }

    protected boolean existePdfActaPagada (int actaId) throws Exception{
        boolean estaPdf = false;
        try{
            Query query = sesion.createSQLQuery("select exists(select 1 from detalle_bolsas_validaciones_pag dbvp , pagos pag, ot o WHERE dbvp.pagos_id = pag.id AND dbvp.ot_id = o.id AND dbvp.pdf_generado = TRUE AND dbvp.acta_id = :actaid);");
            query.setParameter("actaid",actaId);
            estaPdf = (Boolean) query.uniqueResult();

        }catch (HibernateException e){
            throw e;
        }
        return  estaPdf;
    }




    protected boolean validaPagoProbado (int actaId) throws Exception{
        boolean pagoAprobado = false;
        try{
            Actas acta = (Actas)  sesion.createCriteria(Actas.class)
                    .add(Restrictions.eq("id", actaId)).uniqueResult();
            pagoAprobado = acta.getPagoAprobado();
        }catch (HibernateException e){
            pagoAprobado = false;
        }
        return  pagoAprobado;
    }


    protected boolean eliminarCosteo(Cubicador cubicador)throws Exception {
        boolean respuesta = false;
        try{
            sesion.delete(cubicador);
            sesion.flush();
            respuesta = true;
        }catch (Exception e){
            throw e;
        }
        return respuesta;
    }

    protected int obtenerIdPagoPorBolsa (int bolsaId) throws Exception{
        int pagoId = 0;
        try{
            Query query = sesion.createSQLQuery("select DISTINCT (dbvp.pagos_id) from detalle_bolsas_validaciones_pag dbvp WHERE dbvp.bolsa_pag_id = :bolsaid AND dbvp.pagos_id IS NOT NULL");
            query.setParameter("bolsaid",bolsaId);
            pagoId = (Integer) query.uniqueResult();

        }catch (HibernateException e){
            throw e;
        }
        return  pagoId;
    }

    protected int obtenerIdPagoPorActa (int actaId) throws Exception{
        int pagoId = 0;
        try{
            Query query = sesion.createSQLQuery("select DISTINCT (dbvp.pagos_id) from detalle_bolsas_validaciones_pag dbvp WHERE dbvp.acta_id = :actaid");
            query.setParameter("actaid",actaId);
            pagoId = (Integer) query.uniqueResult();

        }catch (HibernateException e){
            throw e;
        }
        return  pagoId;
    }

    public Map obtieneServiciosDisenoAp(int otId)throws Exception {
        Map mapaServiciosDiseno = new HashMap();
        Map serviciosCubiMap = new HashMap();

        Map alcancesMap = new HashMap();
        Map tipoServMap = new HashMap();

        List listServiciosDis = new ArrayList();
        List listServiciosCubi = new ArrayList();

        List listServiciosAdic = new ArrayList();
        Map servicioMap = new HashMap();
        ProveedoresServicios proveedoresServicio = new ProveedoresServicios();

        String activo = "A";
        try{

            Ot ot = (Ot) sesion.createCriteria(Ot.class)
                    .add(Restrictions.eq("id", otId))
                    .setFetchMode("contrato", FetchMode.JOIN)
                    .uniqueResult();
            Cubicador cubicador = (Cubicador) sesion.createCriteria(Cubicador.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .setFetchMode("proveedor", FetchMode.JOIN)
                    .uniqueResult();

            char estado = activo.charAt(0);


            List<CubicadorServicios> serviciosCubicados = new ArrayList<CubicadorServicios>();


            Servicios servicio = new Servicios();
            Alcances alcances = new Alcances();

            Criteria criteriaSerCubList = sesion.createCriteria(CubicadorServicios.class)
                    .add(Restrictions.eq("id.cubicadorId", cubicador.getId()));
            serviciosCubicados = (List<CubicadorServicios>) criteriaSerCubList.list();

            Actas actas = new Actas();
            actas = obtenerActaParaValidar(otId);

            List<DetalleServiciosActas> detalleServiciosActasList = new ArrayList<DetalleServiciosActas>();
            detalleServiciosActasList  = (List<DetalleServiciosActas>) sesion.createCriteria(DetalleServiciosActas.class)
                    .add(Restrictions.eq("id.actaId", actas.getId()))
                    .list();

            List<DetalleServiciosAdicionalesActas> detalleServiciosAdicionalesActasList = new ArrayList<DetalleServiciosAdicionalesActas>();
            detalleServiciosAdicionalesActasList  = (List<DetalleServiciosAdicionalesActas>) sesion.createCriteria(DetalleServiciosAdicionalesActas.class)
                    .add(Restrictions.eq("id.actaId", actas.getId()))
                    .list();


            for(DetalleServiciosActas cubicadorServicios : detalleServiciosActasList){
                servicio = new Servicios();
                alcances = new Alcances();
                Criteria criteriaServCub = sesion.createCriteria(Servicios.class)
                        .add(Restrictions.eq("id", cubicadorServicios.getServicio().getId()))
//                        se comenta estado por problemas de buevo catalogo
//                        .add(Restrictions.eq("estado", estado))
                        .setFetchMode("tipoServicio", FetchMode.JOIN);
                servicio = (Servicios) criteriaServCub.uniqueResult();

                Criteria criteriaAlcances = sesion.createCriteria(Alcances.class)
                        .add(Restrictions.eq("codigo", servicio.getCodAlcance()));
                alcances = (Alcances) criteriaAlcances.uniqueResult();

                serviciosCubiMap = new HashMap();

                alcancesMap.put("codigo", alcances.getCodigo());
                alcancesMap.put("nombre", formateaStringConCaracteresNoaceptadosEnJSon(alcances.getNombre(), ""));
                alcancesMap.put("descripcion", formateaStringConCaracteresNoaceptadosEnJSon(alcances.getDescripcion(), ""));
                alcancesMap.put("equiposEntregasTch", formateaStringConCaracteresNoaceptadosEnJSon(alcances.getEquiposEntregaTch(), ""));
                alcancesMap.put("materialesEc", formateaStringConCaracteresNoaceptadosEnJSon(alcances.getMaterialesEecc(), ""));
                alcancesMap.put("materialesTCh", formateaStringConCaracteresNoaceptadosEnJSon(alcances.getMaterialesTch(), ""));
                alcancesMap.put("alcanceGeneral", formateaStringConCaracteresNoaceptadosEnJSon(alcances.getAlcanceGeneral(), ""));

                tipoServMap.put("id", servicio.getTipoServicio().getId());
                tipoServMap.put("nombre",servicio.getTipoServicio().getNombre());
                tipoServMap.put("descripcion",servicio.getTipoServicio().getDescripcion());


                serviciosCubiMap.put("idServicio", servicio.getId());
                serviciosCubiMap.put("nombre", formateaStringConCaracteresNoaceptadosEnJSon(servicio.getNombre(), "pulg."));
                serviciosCubiMap.put("descripcion", formateaStringConCaracteresNoaceptadosEnJSon(servicio.getDescripcion(), "pulg."));
                serviciosCubiMap.put("alcances", alcancesMap);
                serviciosCubiMap.put("tipoServicio", tipoServMap);
                serviciosCubiMap.put("cantidad", cubicadorServicios.getTotalUnidadesEjecucionActa());


                proveedoresServicio = new ProveedoresServicios();
                Criteria criteriaProv = sesion.createCriteria(ProveedoresServicios.class)
                        .add(Restrictions.eq("contratos.id", ot.getContrato().getId()))
                        .add(Restrictions.eq("proveedores.id", cubicador.getProveedor().getId()))
                        .add(Restrictions.eq("regiones.id", cubicador.getRegion().getId()))
                        .add(Restrictions.eq("servicios.id", servicio.getId()))
                        .setFetchMode("tipoMoneda", FetchMode.JOIN);
                proveedoresServicio = (ProveedoresServicios) criteriaProv.uniqueResult();

                serviciosCubiMap.put("precio", proveedoresServicio.getPrecio());

                listServiciosCubi.add(serviciosCubiMap);
            }

            mapaServiciosDiseno.put("serviciosCubicados", listServiciosCubi);



            for(DetalleServiciosAdicionalesActas cubicadorAdicionales : detalleServiciosAdicionalesActasList){
                servicio = new Servicios();
                alcances = new Alcances();
                Criteria criteriaServCub = sesion.createCriteria(Servicios.class)
                        .add(Restrictions.eq("id", cubicadorAdicionales.getServicio().getId()))
//                        se comenta por cambio de cataogo
//                        .add(Restrictions.eq("estado", estado))
                        .setFetchMode("tipoServicio", FetchMode.JOIN);
                servicio = (Servicios) criteriaServCub.uniqueResult();

                Criteria criteriaAlcances = sesion.createCriteria(Alcances.class)
                        .add(Restrictions.eq("codigo", servicio.getCodAlcance()));
                alcances = (Alcances) criteriaAlcances.uniqueResult();

                serviciosCubiMap = new HashMap();

                alcancesMap.put("codigo", alcances.getCodigo());
                alcancesMap.put("nombre", formateaStringConCaracteresNoaceptadosEnJSon(alcances.getNombre(), ""));
                alcancesMap.put("descripcion", formateaStringConCaracteresNoaceptadosEnJSon(alcances.getDescripcion(), ""));
                alcancesMap.put("equiposEntregasTch", formateaStringConCaracteresNoaceptadosEnJSon(alcances.getEquiposEntregaTch(), ""));
                alcancesMap.put("materialesEc", formateaStringConCaracteresNoaceptadosEnJSon(alcances.getMaterialesEecc(), ""));
                alcancesMap.put("materialesTCh", formateaStringConCaracteresNoaceptadosEnJSon(alcances.getMaterialesTch(), ""));
                alcancesMap.put("alcanceGeneral", formateaStringConCaracteresNoaceptadosEnJSon(alcances.getAlcanceGeneral(), ""));

                tipoServMap.put("id", servicio.getTipoServicio().getId());
                tipoServMap.put("nombre",servicio.getTipoServicio().getNombre());
                tipoServMap.put("descripcion",servicio.getTipoServicio().getDescripcion());


                serviciosCubiMap.put("idServicio", servicio.getId());
                serviciosCubiMap.put("nombre", formateaStringConCaracteresNoaceptadosEnJSon(servicio.getNombre(), "pulg."));
                serviciosCubiMap.put("descripcion", formateaStringConCaracteresNoaceptadosEnJSon(servicio.getDescripcion(), "pulg."));
                serviciosCubiMap.put("alcances", alcancesMap);
                serviciosCubiMap.put("tipoServicio", tipoServMap);
                serviciosCubiMap.put("cantidad", cubicadorAdicionales.getTotalUnidadesEjecucionActa());


                proveedoresServicio = new ProveedoresServicios();
                Criteria criteriaProv = sesion.createCriteria(ProveedoresServicios.class)
                        .add(Restrictions.eq("contratos.id", ot.getContrato().getId()))
                        .add(Restrictions.eq("proveedores.id", cubicador.getProveedor().getId()))
                        .add(Restrictions.eq("regiones.id", cubicador.getRegion().getId()))
                        .add(Restrictions.eq("servicios.id", servicio.getId()))
                        .setFetchMode("tipoMoneda", FetchMode.JOIN);
                proveedoresServicio = (ProveedoresServicios) criteriaProv.uniqueResult();

                serviciosCubiMap.put("precio", proveedoresServicio.getPrecio());

                listServiciosAdic.add(serviciosCubiMap);
            }

            mapaServiciosDiseno.put("serviciosAdicionales", listServiciosAdic);
            mapaServiciosDiseno.put("cubicadorId", cubicador.getId());

            return mapaServiciosDiseno;
        }catch(Exception e){
            throw e;
        }
    }
    public String formateaStringConCaracteresNoaceptadosEnJSon(String entrada, String remplazo)throws Exception{
        String respuesta = "";
        try{
            if(entrada != null){
                if(remplazo.equals("")){
                    respuesta = entrada.replace("\n"," ");
                    respuesta = respuesta.replace("\t"," ");
                } else {
                    respuesta = entrada.replace("\"", " "+remplazo+" ");
                    respuesta = respuesta.replace("\n"," ");
                    respuesta = respuesta.replace("\t"," ");
                }
            }
            return respuesta;
        }catch(Exception e){
            throw e;
        }
    }

    public List obtieneDetalleCosteoPorContratoYtipoDetalle(int otId, int contratoId, String tipoDetalle)throws Exception {
        List respuestaList = new ArrayList();
        try{
            List<DetalleMaterialesActas> materialesList = new ArrayList<DetalleMaterialesActas>();
            List<DetalleMaterialesAdicionalesActas> materialesAdicionalesList = new ArrayList<DetalleMaterialesAdicionalesActas>();
            List<DetalleServiciosActas> serviciosList = new ArrayList<DetalleServiciosActas>();
            List<DetalleServiciosAdicionalesActas> serviciosAdicionalesList = new ArrayList<DetalleServiciosAdicionalesActas>();
            Map serviciosMaterialesMap = new HashMap();
            Map respuestaMap = new HashMap();
            if (contratoId == 1) {
                respuestaMap = obtieneServiciosDisenoAp(otId);
                List<LinkedHashMap> lista = new ArrayList<>();

                switch (tipoDetalle) {
                    case "servicios":
                        lista = new ArrayList<>();
                        lista = (List<LinkedHashMap>) respuestaMap.get("serviciosCubicados");
                        for(Map servicio : lista){
                            serviciosMaterialesMap = new HashMap();
                            serviciosMaterialesMap.put("id", servicio.get(""));
                            serviciosMaterialesMap.put("nombre", servicio.get(""));
                            serviciosMaterialesMap.put("descripcion", servicio.get(""));
                            serviciosMaterialesMap.put("cantidadCubicada", servicio.get(""));
                            serviciosMaterialesMap.put("precio", servicio.get(""));
                            serviciosMaterialesMap.put("tipoMonedaId", servicio.get(""));
                            serviciosMaterialesMap.put("montoTotalCubicado", servicio.get(""));
                            serviciosMaterialesMap.put("cantidadAcumuladaInformada", servicio.get(""));
                            serviciosMaterialesMap.put("validar", false);
                            serviciosMaterialesMap.put("cantidadActualInformada", servicio.get(""));
                            serviciosMaterialesMap.put("montoTotalServicio", servicio.get(""));
                        }
                        break;
                    case "serviciosAdicionales":
                        lista = new ArrayList<>();
                        lista = (List<LinkedHashMap>) respuestaMap.get("serviciosAdicionales");
                        for(Map servicioAdicional : lista){
                            serviciosMaterialesMap = new HashMap();
                            serviciosMaterialesMap.put("id", servicioAdicional.get(""));
                            serviciosMaterialesMap.put("nombre", servicioAdicional.get(""));
                            serviciosMaterialesMap.put("descripcion", servicioAdicional.get(""));
                            serviciosMaterialesMap.put("cantidadCubicada", servicioAdicional.get(""));
                            serviciosMaterialesMap.put("precio", servicioAdicional.get(""));
                            serviciosMaterialesMap.put("tipoMonedaId", servicioAdicional.get(""));
                            serviciosMaterialesMap.put("montoTotalCubicado", servicioAdicional.get(""));
                            serviciosMaterialesMap.put("cantidadAcumuladaInformada", servicioAdicional.get(""));
                            serviciosMaterialesMap.put("validar", false);
                            serviciosMaterialesMap.put("cantidadActualInformada", servicioAdicional.get(""));
                            serviciosMaterialesMap.put("montoTotalServicio", servicioAdicional.get(""));
                        }
                        break;
                }
            }
            return respuestaList;
        } catch (Exception e){
            throw e;
        }

    }

    public List<WorkflowEventosEjecucion> obtenerWeePorUsuario(int usuariosId) {
        List<WorkflowEventosEjecucion> listadoWEE = new ArrayList<>();

        Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class);
        criteria.add(Restrictions.eq("usuarioId", usuariosId));
        criteria.add(Restrictions.eq("ejecutado", 9999));

        listadoWEE = criteria.list();

        return listadoWEE;
    }

    public List<UsuariosOt> obtenerusuariosOTPorUsuario(int usuariosId) {
        List<UsuariosOt> listadousuariosOt = new ArrayList<>();

        Criteria criteria = sesion.createCriteria(UsuariosOt.class);
        criteria.add(Restrictions.eq("id.usuarioId", usuariosId));

        listadousuariosOt = criteria.list();


        return listadousuariosOt;
    }

    public int obtenerGestorPorOtId(int otId) {
        int gestorId = 0;

        Criteria criteria = sesion.createCriteria(Ot.class);
        criteria.add(Restrictions.eq("id", otId));

        Ot ot = (Ot) criteria.uniqueResult();

        gestorId = ot.getGestor().getId();

        return gestorId;
    }

    public List<Ot> obtenerOtDeUsuarios(List<Integer> idGestores) {
        List<Ot> listadoOt = new ArrayList<>();


        for(Integer idUsuario: idGestores){

            Criteria criteria = sesion.createCriteria(Ot.class);
            criteria.add(Restrictions.eq("gestor.id", idUsuario));
            criteria.add(Restrictions.eq("contrato.id", 1));

            List<Ot> listadoOtUsuario = (List<Ot>) criteria.list();

            for(Ot otUsuario: listadoOtUsuario){
                listadoOt.add(otUsuario);
            }

        }

        return listadoOt;
    }
    private String obtienefechaSegunFormato(Date fecha, String formatoSalida) {
        String fechaString = "";
        if (fecha == null) {
            return fechaString;
        } else {
            try {
                DateFormat formato = new SimpleDateFormat(formatoSalida);
                fechaString = formato.format(fecha);
                //System.out.println("====================>  Fecha de salida :  "+fechaString);
                return fechaString;
            } catch (Exception e) {
                fechaString = "error: "+e;
                return fechaString;
            }
        }
    }
    public boolean reemplazaTabsBdGateways(int contratoId)throws Exception {
        boolean respuesta = false;
        List<Ot> listadoOt = new ArrayList<>();
        Criteria criteria = sesion.createCriteria(Ot.class);
        criteria.add(Restrictions.eq("contrato.id", contratoId));
        listadoOt = (List<Ot>) criteria.list();
        String jsonNewDetalle = "";

        for(Ot ot : listadoOt){

            jsonNewDetalle = formateaStringConCaracteresNoaceptadosEnJSon(ot.getJsonDetalleOt(), "");
            ot.setJsonDetalleOt(jsonNewDetalle);
            sesion.update(ot);
        }
        return respuesta;
    }

    public List obtieneFormularioCalidadGateways(int otId, int usuarioId, String tipoFormulario) throws Exception {
        List formularioRespuesta = new ArrayList();
        List<FormulariosSeguimientosCalidad>formulariosSeguimiento = new ArrayList<>();
        Criteria criteriaSeguimiento = sesion.createCriteria(FormulariosSeguimientosCalidad.class);
        criteriaSeguimiento.add(Restrictions.eq("usuarioId", usuarioId));
        criteriaSeguimiento.add(Restrictions.eq("otId", otId));
//        criteriaSeguimiento.add(Restrictions.eq("formulariosCalidad.tipoFormulario", tipoFormulario));
        criteriaSeguimiento.addOrder(Order.desc("id"));
        criteriaSeguimiento.setMaxResults(1);
        formulariosSeguimiento = (List<FormulariosSeguimientosCalidad>) criteriaSeguimiento.list();

        Ot ot = new Ot();
        ot = obtenerDetalleBasicoOt(otId);
        String estadoForm = "A";
        List<FormulariosCalidad> listaFormulariosCalidad = new ArrayList<>();
        Criteria criteriaFormulario = sesion.createCriteria(FormulariosCalidad.class);
        criteriaFormulario.add(Restrictions.eq("contratoId", ot.getContrato().getId())).setFetchMode("contratoId", FetchMode.JOIN);
        criteriaFormulario.add(Restrictions.eq("tipoFormulario", tipoFormulario));
        criteriaFormulario.add(Restrictions.eq("estado", estadoForm.charAt(0)));
        criteriaFormulario.addOrder(Order.asc("id"));
        listaFormulariosCalidad = (List<FormulariosCalidad>) criteriaFormulario.list();

        List<FormulariosCalidad> listaFormulariosCalidadNew = new ArrayList<>();

        for(FormulariosCalidad formularios : listaFormulariosCalidad){
            FormulariosCalidad form = new FormulariosCalidad(formularios.getId(), formularios.getFormularioCalidad(), formularios.getContratoId(), formularios.getEstado() );
            listaFormulariosCalidadNew.add(form);
        }
        List<Map> listaItemsFinal = new ArrayList<>();
        Map itemsFinal = new HashMap();

        Map itemsSalida = new HashMap();

        if(formulariosSeguimiento.size() > 0){
            for (FormulariosCalidad formu :listaFormulariosCalidadNew) {

                HashMap<String, Object> formularioCalidad = new ObjectMapper().readValue(formu.getFormularioCalidad(), HashMap.class);
                List<Map> listaDeItems = (List<Map>) formularioCalidad.get("itemsSeguimiento");
                int idFormulario = Integer.parseInt(formularioCalidad.get("formularioId").toString());
                boolean aplica = false;

                listaItemsFinal = new ArrayList<>();
                for (Map items : listaDeItems) {
                    List historicoItems = new ArrayList();
                    double idItems = Double.parseDouble(items.get("id").toString());
                    itemsFinal = new HashMap();
                    List<Map> formulariosSeguimientosCalidadHistorico = new ArrayList<>();
                    formulariosSeguimientosCalidadHistorico = obtieneHistoricoCalidadGateways(otId, usuarioId, idItems);

                    items.put("historico", formulariosSeguimientosCalidadHistorico);
                    itemsSalida = items;
                    if (idFormulario != 6 && formulariosSeguimientosCalidadHistorico.size() > 0) {
                        aplica = true;
                        itemsFinal = new HashMap();
                        itemsFinal = obtenerUltimoSeguimientoCalidad(otId, usuarioId, idItems);
                        switch (itemsFinal.get("estado").toString()) {
                            case "ok":
                                itemsSalida.remove("ok");
                                itemsSalida.remove("observaciones");
                                itemsSalida.remove("porcentaje");

                                itemsSalida.put("observaciones", itemsFinal.get("observaciones").toString());
                                itemsSalida.put("porcentaje", itemsFinal.get("porcentaje"));
                                itemsSalida.put("ok", true);
                                break;
                            case "nok":
                                itemsSalida.remove("nok");
                                itemsSalida.remove("observaciones");
                                itemsSalida.remove("porcentaje");

                                itemsSalida.put("observaciones", itemsFinal.get("observaciones").toString());
                                itemsSalida.put("porcentaje", itemsFinal.get("porcentaje"));
                                itemsSalida.put("nok", true);
                                break;
                            case "na":
                                itemsSalida.remove("nok");
                                itemsSalida.remove("observaciones");
                                itemsSalida.remove("porcentaje");

                                itemsSalida.put("observaciones", itemsFinal.get("observaciones").toString());
                                itemsSalida.put("porcentaje", itemsFinal.get("porcentaje"));
                                itemsSalida.put("nok", true);
                                break;
                        }
                    } else if(formulariosSeguimientosCalidadHistorico.size() > 0 ){
                        itemsFinal = new HashMap();
                        itemsFinal = obtenerUltimoSeguimientoCalidad(otId, usuarioId, idItems);

                        itemsSalida.remove("porcentaje");
                        itemsSalida.remove("observaciones");

                        aplica = true;

                        itemsSalida.put("observaciones", itemsFinal.get("observaciones").toString());
                        itemsSalida.put("porcentaje", itemsFinal.get("porcentaje"));

                    }
                    listaItemsFinal.add(itemsSalida);


                }
                formularioCalidad.remove("aplica");
                formularioCalidad.put("aplica", aplica);
                formularioCalidad.remove("itemsSeguimiento");
                formularioCalidad.put("itemsSeguimiento", listaItemsFinal);
                formularioRespuesta.add(formularioCalidad);

            }

        } else {


            for(FormulariosCalidad formularios : listaFormulariosCalidad){
                HashMap<String,Object> formulario = new ObjectMapper().readValue(formularios.getFormularioCalidad(), HashMap.class);
                formulario.put("formularioId", formularios.getId());
                formularioRespuesta.add(formulario);
            }
        }
        return formularioRespuesta;
    }
    private Map obtenerUltimoSeguimientoCalidad(int otId, int usuarioId, double idItems) throws Exception {
        FormulariosSeguimientosCalidad ultimoFormulario = new FormulariosSeguimientosCalidad();
        Criteria criteriaSeguimiento = sesion.createCriteria(FormulariosSeguimientosCalidad.class);
        criteriaSeguimiento.add(Restrictions.eq("otId", otId));
        criteriaSeguimiento.add(Restrictions.eq("formularioItemId", idItems));
        criteriaSeguimiento.addOrder(Order.desc("id"));
        criteriaSeguimiento.setMaxResults(1);
        ultimoFormulario = (FormulariosSeguimientosCalidad) criteriaSeguimiento.uniqueResult();
        Map respuesta = new HashMap();

        if(ultimoFormulario != null){
            respuesta.put("estado", ultimoFormulario.getEstado());
            respuesta.put("porcentaje", ultimoFormulario.getPorcentaje());
            respuesta.put("observaciones", ultimoFormulario.getObservaciones());
        } else {
            respuesta.put("estado", "null");
            respuesta.put("porcentaje", 0);
        }
        return respuesta;
    }
    private List<Map> obtieneHistoricoCalidadGateways(int otId, int usuarioId, double idItems) throws Exception {
        List<Map> listaFormulariosFinal = new ArrayList<>();
        List<FormulariosSeguimientosCalidad> listaFormularios = new ArrayList<>();
        Criteria criteriaSeguimiento = sesion.createCriteria(FormulariosSeguimientosCalidad.class);
        criteriaSeguimiento.add(Restrictions.eq("otId", otId));
        criteriaSeguimiento.add(Restrictions.eq("formularioItemId", idItems));
        criteriaSeguimiento.addOrder(Order.desc("id"));
        listaFormularios = (List<FormulariosSeguimientosCalidad>) criteriaSeguimiento.list();
        Map itemHistorico = new HashMap();

        if(listaFormularios.size() > 0){
            for(FormulariosSeguimientosCalidad formulariosIngresados : listaFormularios){
                Usuarios usuario = new Usuarios();
                usuario = obtenerUsuarioPorId(formulariosIngresados.getUsuarioId());
                itemHistorico = new HashMap();
                itemHistorico.put("nombreUsuario", (usuario.getNombres()+ " "+ usuario.getApellidos()));
                itemHistorico.put("fechaIngreso", obtienefechaSegunFormato(formulariosIngresados.getFechaCreacion(), "dd-MM-yyyy hh:mm"));
                itemHistorico.put("estado", formulariosIngresados.getEstado());
                itemHistorico.put("porcentaje", formulariosIngresados.getPorcentaje());
                itemHistorico.put("observacion", formulariosIngresados.getObservaciones());
                listaFormulariosFinal.add(itemHistorico);
            }
        }
        return listaFormulariosFinal;
    }

    public boolean registraFormularioSeguimientoCalidadGateways(int otId, int usuarioId, HashMap<String, Object> mapEntrada, List<ArchivoAux> listadoArchivo, LibroObras libroObras)throws Exception {
        FormulariosSeguimientosCalidad formularioSeguimientoIngresado = new FormulariosSeguimientosCalidad();
        FormulariosSeguimientosCalidad nuevoFormularioSeguimiento = new FormulariosSeguimientosCalidad();
        Map preguntaMap = new HashMap();
        boolean respuesta = false;
        Ot ot = new Ot();
        ot = obtenerDetalleBasicoOt(otId);
        int idLibroObra = 0;

        String[] roles = new String[8];
        roles[0] = "2";
        roles[1] = "4";
        roles[2] = "5";
        roles[3] = "7";
        roles[4] = "10";
        roles[5] = "11";
        roles[6] = "3";
        roles[7] = "14";

        String estadoSeleccion = "No Aplica";
        Map mapaParametros = (HashMap) mapEntrada.get("parametros");
        int formularioId = Integer.parseInt(mapaParametros.get("formularioId").toString());
        double preguntaId = Double.parseDouble(mapaParametros.get("id").toString());
        String tipoFormulario = mapaParametros.get("tipoFormulario").toString();
        if(tipoFormulario.equals("OOEE")){
            estadoSeleccion = mapaParametros.get("estado").toString();
        }
        String observacion = "";
        if(mapaParametros.containsKey("observacion")){
            observacion = mapaParametros.get("observacion").toString();
        }

//        double porcentaje = Double.parseDouble(mapaParametros.get("porcentaje").toString());


        FormulariosCalidad formulariosCalidad = new FormulariosCalidad();
        String estado = "A";
        FormulariosCalidad formlarioCalidad = new FormulariosCalidad();
        Criteria criteriaFormulario = sesion.createCriteria(FormulariosCalidad.class);
        criteriaFormulario.add(Restrictions.eq("id", formularioId));
        formlarioCalidad = (FormulariosCalidad) criteriaFormulario.uniqueResult();
        HashMap<String,Object> formulario = new ObjectMapper().readValue(formlarioCalidad.getFormularioCalidad(), HashMap.class);
        List<Map> listaDeItems = (List<Map>)formulario.get("itemsSeguimiento");
        String nombreItem = "";
        for(Map items : listaDeItems){
            double idItem = Double.parseDouble(items.get("id").toString());
            if(idItem == preguntaId){
                nombreItem = items.get("item").toString();
                break;
            }
        }


        FormulariosSeguimientosCalidad formulariosSeguimientosCalidad = new FormulariosSeguimientosCalidad();
        formulariosSeguimientosCalidad.setDescripcionItem(nombreItem);
        formulariosSeguimientosCalidad.setEstado(estadoSeleccion);
        Date fechaIngreso = new Date();
        formulariosSeguimientosCalidad.setFechaCreacion(fechaIngreso);
        formulariosSeguimientosCalidad.setFormularioItemId(preguntaId);
        formulariosSeguimientosCalidad.setFormulariosCalidad(formulariosCalidad);
        libroObras.setObservaciones(observacion);
        if(libroObras.getObservaciones() != null && !libroObras.getObservaciones().equals("")){
            idLibroObra = LibroObraDAO.gestionarAdjuntoLibroObraOt3(listadoArchivo, libroObras,roles, usuarioId, otId, nombreItem);
//                nuevoFormularioSeguimiento.setLibroObraId(idLibroObra);
            formulariosSeguimientosCalidad.setLibroObraId(idLibroObra);
        }
        formulariosSeguimientosCalidad.setFormulariosCalidad(formlarioCalidad);
        formulariosSeguimientosCalidad.setObservaciones(observacion);
        if(mapaParametros.containsKey("porcentaje")){formulariosSeguimientosCalidad.setPorcentaje(Double.parseDouble(mapaParametros.get("porcentaje").toString()));}
        formulariosSeguimientosCalidad.setUsuarioId(usuarioId);
        formulariosSeguimientosCalidad.setOtId(otId);

        if(mapaParametros.containsKey("categoria") && mapaParametros.containsValue("categoria")){formulariosSeguimientosCalidad.setCategoria(Integer.parseInt(mapaParametros.get("categoria").toString()));}

        if(tipoFormulario.equals("OOCC")){

            // nuevos parametros a solicitud del pana F.P.
            int gantt = Integer.parseInt(mapaParametros.get("gantt").toString());
            int libroObra = Integer.parseInt(mapaParametros.get("libroObra").toString());
            int documentoLegal = Integer.parseInt(mapaParametros.get("documentoLegal").toString());
            int materialAcopio = Integer.parseInt(mapaParametros.get("materialAcopio").toString());
            int cantidadPersonal = Integer.parseInt(mapaParametros.get("cantidadPersonal").toString());
            int cantidadAcreditados = Integer.parseInt(mapaParametros.get("cantidadAcreditado").toString());
            String nombresAcreditados = mapaParametros.get("nombresAcreditados").toString();
            String region = mapaParametros.get("region").toString();

            formulariosSeguimientosCalidad.setGantt(gantt);
            formulariosSeguimientosCalidad.setLibroObra(libroObra);
            formulariosSeguimientosCalidad.setDocumentoLegal(documentoLegal);
            formulariosSeguimientosCalidad.setMaterialAcopio(materialAcopio);
            formulariosSeguimientosCalidad.setCantidadPersonal(cantidadPersonal);
            formulariosSeguimientosCalidad.setCantidadAcreditados(cantidadAcreditados);
            formulariosSeguimientosCalidad.setNombresAcreditados(nombresAcreditados);
            formulariosSeguimientosCalidad.setRegion(region);

            // nuevos parametros a solicitud del pana
        }

        sesion.save(formulariosSeguimientosCalidad);
        sesion.flush();
        respuesta = true;

        return respuesta;
    }
    private void informarAvanceCalidad(String observaciones, Ot ot, int usuarioId) throws Exception{
        try {
//                this.getGateway().continuarFlujoWorkfloEventoEjecucion(otId, eventoId);
                Map map = new HashMap();
                Map mapAux = new HashMap();
                mapAux.put("id",ot.getId());
                map.put("ot",mapAux);
                map.put("observaciones",observaciones);

                LibroObras libroObras = new LibroObras();
                libroObras.setObservaciones(observaciones);
                libroObras.setOt(ot);
                libroObras.setFechaCreacion(new Date());
                String[] roles = new String[8];
                roles[0] = "2";
                roles[1] = "4";
                roles[2] = "5";
                roles[3] = "7";
                roles[4] = "10";
                roles[5] = "11";
                roles[6] = "3";
                roles[7] = "14";
                List<ArchivoAux> listadoArchivo = new ArrayList<>();
                LibroObraDAO.gestionarAdjuntoLibroObraOt2(listadoArchivo, libroObras, roles, usuarioId, ot.getId());
    } catch (Exception e){
            throw e;
        }
    }
    private String toJson(String s) {
        s = s.substring(0, s.length()).replace("{", "{\"");
        s = s.substring(0, s.length()).replace("}", "\"}");
        s = s.substring(0, s.length()).replace(", ", "\", \"");
        s = s.substring(0, s.length()).replace("=", "\":\"");
        s = s.substring(0, s.length()).replace("\"[", "[");
        s = s.substring(0, s.length()).replace("]\"", "]");
        s = s.substring(0, s.length()).replace("}\", \"{", "}, {");
        return s;
    }

    public boolean validaRolCalidad(int usuarioEjecutorId) {
        boolean respuesta = false;
        Usuarios usuarios= new Usuarios();
        Criteria criteriaUser = sesion.createCriteria(Usuarios.class);
        criteriaUser.add(Restrictions.eq("id", usuarioEjecutorId));
        usuarios = (Usuarios) criteriaUser.uniqueResult();

        for(Roles rolesUsuario: usuarios.getUsuariosRoles()){
            if(rolesUsuario.getId() == 14){
                respuesta = true;
            }
        }
        return respuesta;
    }

    //bucle
    public Servicios validarServicioAgenciaEspecialidadBucle(int idServicio, int idAgencia, int idContrato) throws Exception {

        try {

            Servicios servicio = (Servicios) sesion.load(Servicios.class, idServicio);

            AgenciasContratosEspecialidades agenciasContratosEspecialidadesId = (AgenciasContratosEspecialidades) sesion.createCriteria(AgenciasContratosEspecialidades.class)
                    .add(Restrictions.and(
                            Restrictions.eq("id.especialidadId", servicio.getEspecialidadId()),
                            Restrictions.eq("id.contratoId", idContrato),
                            Restrictions.eq("id.agenciaId", idAgencia))).uniqueResult();
            if (agenciasContratosEspecialidadesId == null) {
                throw new Exception("La Especialidad del Servicio con Id:" + idServicio + " no corresponde con la Agencia con Id:" + idAgencia);
            }

            //Agregar Precio.
            servicio.setPrecio(agenciasContratosEspecialidadesId.getId().getPrecio() * servicio.getPuntosBaremos());

            return servicio;
        } catch (HibernateException e) {
            throw e;
        }
    }

    public ServiciosUnidad validadarServicioConServiciosUnidadBucle(Servicios servicio, int idServicioUnidad, int idActividad) throws Exception {

        try {
            ServiciosUnidad servicioUnidad = (ServiciosUnidad) sesion.load(ServiciosUnidad.class, idServicioUnidad);
            ServiciosServiciosUnidad serviciosServiciosUnidad = (ServiciosServiciosUnidad) sesion.createCriteria(ServiciosServiciosUnidad.class)
                    .add(Restrictions.and(
                            Restrictions.eq("actividades.id", idActividad),
                            Restrictions.eq("codigoManoObra", servicio.getCodigo()),
                            Restrictions.eq("codigoUnidadObra", servicioUnidad.getCodigo()))).uniqueResult();

            if (serviciosServiciosUnidad == null) {
                throw new Exception("No hay coincidencia entre servicio unidad y servicio.");
            }

            return servicioUnidad;

        } catch (HibernateException e) {
            throw e;
        }

    }

    public List<Proveedores> listarProveedoresPorAgencia(int agenciaId) throws Exception {


        List<Proveedores> listadoRetorno = new ArrayList<>();

        Criteria criteria = sesion.createCriteria(AgenciasProveedores.class);
        criteria.add(Restrictions.eq("id.agenciaId", agenciaId));

        List<AgenciasProveedores> agenciasProveedores = criteria.list();

        for (AgenciasProveedores agenciaProveedor : agenciasProveedores) {

            Criteria croteriaProveedor = sesion.createCriteria(Proveedores.class);
            croteriaProveedor.add(Restrictions.eq("id", agenciaProveedor.getId().getProveedorId()));

            Proveedores proveedor = (Proveedores) croteriaProveedor.uniqueResult();
            listadoRetorno.add(proveedor);
        }


        for (Proveedores proveedor : listadoRetorno) {

            validarYSetearObjetoARetornar(proveedor, null);
        }


        return listadoRetorno;

    }

    public double obtenerValorModificacionVial(int agenciaId, double puntoBaremos) {
        double valorTotal = 0;

        // obtengo el factor del tramo segun la cantidad de puntos baremos
        Criteria criteriaTramos = sesion.createCriteria(TramosBucle.class);

        List<TramosBucle> listadoTramos = criteriaTramos.list();

        double factor = 0;

        for (TramosBucle tramo : listadoTramos) {

            if (puntoBaremos > tramo.getRangoInicio() && puntoBaremos < tramo.getRangoFin()) {
                factor = tramo.getFactor();
            }

        }


        // obtengo el precio de la agencia segun especualidad
        Criteria criteria = sesion.createCriteria(AgenciasContratosEspecialidades.class);
        criteria.add(Restrictions.eq("id.agenciaId", agenciaId));
        criteria.add(Restrictions.eq("id.contratoId", 9));
        criteria.add(Restrictions.eq("id.especialidadId", 1));

        AgenciasContratosEspecialidades agenciaControEspecialidad = (AgenciasContratosEspecialidades) criteria.uniqueResult();


        valorTotal = factor * puntoBaremos * agenciaControEspecialidad.getId().getPrecio();


        return valorTotal;
    }

    public void registrarCubicadorExtra(CubicadorExtrasBucle cubicadorExtras) {
        try {
            sesion.save(cubicadorExtras);
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + cubicadorExtras.getClass().toString());
            System.out.println(e);
            throw e;
        }
    }

    public List<ServiciosUnidad> obtieneServiciosUnidadList(CubicadorServiciosServiciosUnidad cubicadorServiciosServiciosUnidad, String filtroCriteria) throws Exception {
        List<ServiciosUnidad> serviciosUnidadList = new ArrayList<>();
        try {
            Criteria criteria = sesion.createCriteria(ServiciosUnidad.class);
            criteria.add(Restrictions.eq("id", cubicadorServiciosServiciosUnidad.getServicioUnidadId()));
            serviciosUnidadList = (List<ServiciosUnidad>) criteria.list();

            return serviciosUnidadList;
        } catch (Exception e) {
            throw e;
        }

    }

    public List<ServiciosUnidadMateriales> obtieneMaterialesLis(String codigoUnidadObra) {
        List<ServiciosUnidadMateriales> serviciosUnidadMaterialesList = new ArrayList<>();
        try {
            Criteria criteria = sesion.createCriteria(ServiciosUnidadMateriales.class);
            criteria.add(Restrictions.eq("codigoUnidadObra", codigoUnidadObra));
            serviciosUnidadMaterialesList = (List<ServiciosUnidadMateriales>) criteria.list();

            return serviciosUnidadMaterialesList;
        } catch (Exception e) {
            throw e;
        }
    }

    public void guardaMaterialesBucleSegunServicioUnidad(List<ServiciosUnidadMateriales> serviciosUnidadMaterialesList, int cubicadorServicioServicioUnidadId) {
        try {
            Materiales materiales = new Materiales();
            CubicadorMaterialesBucle cubicadorMaterialesBucle = new CubicadorMaterialesBucle();
            for (ServiciosUnidadMateriales serviciosUnidadMateriales : serviciosUnidadMaterialesList) {
                materiales = new Materiales();
                Criteria criteria = sesion.createCriteria(Materiales.class);
                criteria.add(Restrictions.eq("codigo", serviciosUnidadMateriales.getCodigoMaterial()));
                materiales = (Materiales) criteria.uniqueResult();


                cubicadorMaterialesBucle = new CubicadorMaterialesBucle();
                cubicadorMaterialesBucle.setCantidadMaterial(serviciosUnidadMateriales.getCantidadMaterial());
                cubicadorMaterialesBucle.setCodigoMaterial(materiales.getCodigo());
                cubicadorMaterialesBucle.setCubicadorServiciosServiciosUnidadId(cubicadorServicioServicioUnidadId);
                cubicadorMaterialesBucle.setDistribuidor(materiales.getDistribuidor());
                cubicadorMaterialesBucle.setPrecio(materiales.getPrecio());
                cubicadorMaterialesBucle.setTipoMonedaId(materiales.getTipoMoneda().getId());
                cubicadorMaterialesBucle.setCodigoUnidadObra(serviciosUnidadMateriales.getCodigoUnidadObra());


                sesion.save(cubicadorMaterialesBucle);
                sesion.flush();


            }
        } catch (Exception e) {
            throw e;
        }
    }

//    private static RedisClient redisClient = new RedisClient(config.getHostRedis(),config.getPuertoRedis());

}


