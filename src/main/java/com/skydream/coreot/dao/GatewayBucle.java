/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.*;
import com.skydream.coreot.util.Config;
import com.skydream.coreot.util.NewHibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.hibernate.InstantiationException;
import org.hibernate.criterion.Restrictions;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;


/**
 * @author mcj
 */
public class GatewayBucle {

    protected void abrirSesion() throws Exception {
        try {
            SessionFactory sesionFactory = NewHibernateUtil.getSessionFactory();
            this.sesion = sesionFactory.openSession();
        } catch (Exception e) {
            System.out.println("No se pudo iniciar sesión. " + e.getStackTrace().toString());
            log.error("===========================================================");
            log.error("Error al abrir session Gateways, error: "+e.toString());
            throw e;
        }
    }

    protected void cerrarSesion() {
        this.sesion.close();
    }

    protected void rollback() {
        this.trx.rollback();
    }

    protected void commit() {
        this.trx.commit();
    }

    protected void iniciarTransaccion() {
        this.trx = sesion.beginTransaction();
    }

    private Session sesion;
    private Transaction trx;
    private static final Logger log = Logger.getLogger(GatewayBucle.class);
    private static Config config = Config.getINSTANCE();


    public List<Object> listarTipoOt() {

        Criteria criteria = sesion.createCriteria(TipoOt.class);
        List<TipoOt> listado = criteria.list();
        List<Object> listadoFinal = new ArrayList<>();

        for (TipoOt tipoOt: listado){
            if(tipoOt.getNombre().equalsIgnoreCase("OT Full")){
                listadoFinal.add(tipoOt);
            }
            if(tipoOt.getNombre().equalsIgnoreCase("OT Diseño")){
                listadoFinal.add(tipoOt);
            }
            if(tipoOt.getNombre().equalsIgnoreCase("OT Construcción")){
                listadoFinal.add(tipoOt);
            }
        }

        return listadoFinal;
    }

    public List<Object> listarComunasPorRegion(int regionId) throws java.lang.InstantiationException {

        Criteria criteria = sesion.createCriteria(Comunas.class);
        criteria.add(Restrictions.eq("region.id",regionId));
        List<Comunas> listadoComunas = criteria.list();
        List<Object> listadoComunasFinal = criteria.list();

        for (Comunas comuna: listadoComunas){

            comuna.setRegion(null);
            listadoComunasFinal.add(comuna);
        }

        return listadoComunasFinal;

    }

}


