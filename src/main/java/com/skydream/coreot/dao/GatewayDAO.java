/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.ObjetoCoreOT;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mcj
 */
public interface GatewayDAO {

    boolean crear(ObjetoCoreOT obj, Map params) throws Exception;

    int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception;

    boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception;

    List listar(ObjetoCoreOT obj, Map params) throws Exception;

    List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception;

    boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception;
    
    ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT)throws Exception;

}
