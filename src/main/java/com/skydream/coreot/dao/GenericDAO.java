/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.*;

import java.util.*;

import com.skydream.coreot.util.Config;
import com.skydream.coreot.util.RenderPDF;
import com.skydream.coreot.util.SecureTransmission;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

/**
 *
 * @author mcj
 */
public class GenericDAO implements GatewayDAO {

    protected List<ObjetoCoreOT> listarObjCoreOT(ObjetoCoreOT obj, Map params) throws Exception {
        Gateway gP = new Gateway();
        return gP.listarGenerico(obj.getClass(),null, null);
    }

    public List listarGenerico(Class claseObj, Map<String,Object> filtros, List<String> joinObjCoreOT)throws Exception{
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            return gate.listarGenerico(claseObj, filtros, joinObjCoreOT);
        } finally {
            gate.cerrarSesion();
        }
    }

    public ObjetoCoreOT retornarObjetoCoreOT(Class claseObjeto, int idObjetoCoreOT)throws Exception{
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            return gate.retornarObjetoCoreOT(claseObjeto, idObjetoCoreOT);
        } finally {
            gate.cerrarSesion();
        }
    }

    protected List<Usuarios> listarUsuarios(Map datosSesion) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            return gate.listarUsuarios();
        } finally {
            gate.cerrarSesion();
        }
    }

    protected List<ObjetoCoreOT> listarConFiltro(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        Gateway gate = new Gateway();
        List<ObjetoCoreOT> lista = gate.listarConFiltro(obj.getClass(), filtro, valorFiltro);
        for (ObjetoCoreOT objeto : lista) {
            gate.validarYSetearObjetoARetornar(objeto, null);
        }
        return lista;
    }

    protected boolean eliminarGenerico() {
        return false;
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                gateway.registrar(obj);
                gateway.commit();
            } catch (HibernateException e) {
                e.printStackTrace();
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        Gateway gP = new Gateway();
        try {
            gP.abrirSesion();
            gP.iniciarTransaccion();
            try {
                gP.actualizar2(obj);
                gP.commit();
            } catch (Exception e) {
                gP.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gP.cerrarSesion();
        }
    }

    @Override
    public List listar(ObjetoCoreOT obj, Map params) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            return gate.listarGenerico(obj.getClass(), null, null);
        } catch (Exception e) {
            throw e;
        } finally {
            gate.cerrarSesion();
        }
    }
    public List<UsuariosOt> obtenerUsuariosOt( int otId, int eventoId) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            return gate.obtieneUsuariosOtEventos(otId, eventoId);
        } catch (Exception e) {
            throw e;
        } finally {
            gate.cerrarSesion();
        }
    }
    public List<UsuariosOt> obtenerUsuariosOtId( int otId) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            return gate.obtieneUsuariosOt(otId);
        } catch (Exception e) {
            throw e;
        } finally {
            gate.cerrarSesion();
        }
    }
    public List<WorkflowEventosEjecucion> obtenerUsuariosOtWe( int otId, int eventoId) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            return gate.obtieneUsuariosOtEventosWE(otId, eventoId);
        } catch (Exception e) {
            throw e;
        } finally {
            gate.cerrarSesion();
        }
    }
    public List<WorkflowEventosEjecucion> obtenerUsuariosOtIdWe( int otId) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            return gate.obtieneUsuariosOtIdEventosWE(otId);
        } catch (Exception e) {
            throw e;
        } finally {
            gate.cerrarSesion();
        }
    }
    public List<Decisiones> obtenerDecisionesPorEventoId(int eventoId, int otId) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            return gate.obtenerDecisionesPorEventoId(eventoId, otId);
        } catch (Exception e) {
            throw e;
        } finally {
            gate.cerrarSesion();
        }
    }

    public List<Roles> listarRolesPorPerfil(int perfilId) throws Exception {
        List<Roles> listadoRoles = null;
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();
            listadoRoles = gate.listarRolesPorPerfil(perfilId);
            for (Roles rol : listadoRoles) {
                Perfiles prefil = new Perfiles();
                rol.setPerfil(prefil);
                join.add("rolesPrivilegios");
                join.add("rolesOpciones");
                join.add("usuariosRoles");
                gate.validarYSetearObjetoARetornar(rol, join);
            }
        } finally {
            gate.cerrarSesion();
        }
        return listadoRoles;
    }

    public List<Contratos> obtenerContratoPorId(int contratoId) throws Exception {
        List<Contratos> listadoContratos = null;
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();
            listadoContratos = gate.listarContratosPorId(contratoId);
            for (Contratos cont : listadoContratos) {
//                Workflow workflow = new Workflow();
//                cont.setWorkflow(workflow);
                join.add("contratosProveedoreses");
                join.add("contratosAgencias");
                gate.validarYSetearObjetoARetornar(cont, join);
            }
        } finally {
            gate.cerrarSesion();
        }
        return listadoContratos;
    }

    public List<Workflow> obtenerWorkflowPorId(int workfloId) throws Exception {
        List<Workflow> listadoWorkflows = null;
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();
            listadoWorkflows = gate.listarWorkflowPorId(workfloId);
            for (Workflow work : listadoWorkflows) {
                join.add("workflowEventos");
                gate.validarYSetearObjetoARetornar(work, join);
            }
        } finally {
            gate.cerrarSesion();
        }
        return listadoWorkflows;
    }

    public List<Roles> obtenerRolPorId(int idRol) throws Exception {
        List<Roles> listadoRoles = null;
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();
            listadoRoles = gate.listarRolPorId(idRol);
            for (Roles rol : listadoRoles) {
//                Perfiles prefil = new Perfiles();
//                rol.setPerfil(prefil);

                gate.validarYSetearObjetoARetornar(rol, join);
            }
        } finally {
            gate.cerrarSesion();
        }
        return listadoRoles;
    }

    public Roles obtenerRolPorUsuarioId(int usuarioId) throws Exception {
        Roles rol = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            rol = gate.obtenerRolPorUsuarioId(usuarioId);

        } finally {
            gate.cerrarSesion();
        }
        return rol;
    }

    public Boolean asignarResponsableWorkflowEventoEjecucionAction(int idOt, Eventos evento) throws Exception {
        Gateway gateway = new Gateway();
        Boolean respuesta = false;
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            WorkflowEventosEjecucion wee = (WorkflowEventosEjecucion) gateway.obtenerWorkflowEventosEjecucionPorEventoYOt2(evento.getId(), idOt);
            wee.setUsuarioId(evento.getDuracion());
            gateway.commit();

//            try {
//                gateway.commit();
//            } catch (HibernateException e) {
//                log.fatal("Error al asignar el usuario. ", e);
//                gateway.rollback();
//                throw e;
//            }
            respuesta = true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
        return respuesta;
    }

    protected TreeSet<EventosAcciones> setearEventosAccionesPorTipoAccion(Eventos evento) {
        TreeSet<EventosAcciones> listadoAcciones = new TreeSet<>();
        Set<EventosAcciones> acciones = evento.getEventosAcciones();
        for (EventosAcciones acc : acciones) {
            Acciones accionObjAux = acc.getAcciones();
            // obtener el tipo accion de Front-End
//            TipoAccion tipoAccion = new TipoAccion(1);
            if (accionObjAux.getTipoAccion().getNombre().equals("Front-End")) {
                Acciones newAccion = new Acciones(accionObjAux.getId(), accionObjAux.getTipoAccion(), accionObjAux.getNombre(),
                        accionObjAux.getDescripcion(), accionObjAux.getCodigo(), accionObjAux.getTipoEjecucionBackEnd(), null, null);
                Set<AccionesParametros> newListadoParametros = retornarAccionesParametros(accionObjAux);
                newAccion.setAccionesParametros(newListadoParametros);
                EventosAcciones eveAcciones = new EventosAcciones(newAccion, null, acc.getOrden());
                listadoAcciones.add(eveAcciones);
            }

        }
        return listadoAcciones;
    }

    private TreeSet<AccionesParametros> retornarAccionesParametros(Acciones accionObjAux) {
        TreeSet<AccionesParametros> newListadoParametros = new TreeSet<>();
        Set<AccionesParametros> parametros = accionObjAux.getAccionesParametros();
        for (AccionesParametros accionParam : parametros) {
            Parametros param = accionParam.getParametros();
            TipoParametro tipoParametro = new TipoParametro(param.getTipoParametro().getId());
            Parametros newParametro = new Parametros(param.getId(), tipoParametro, param.getNombre(), param.getDescripcion(), null, null);
            Set<DetalleParametros> newSetDetalleParametros = new HashSet<>();
            Set<DetalleParametros> detParams = param.getDetalleParametros();
            for (DetalleParametros detalle : detParams) {
                DetalleParametros newDetalleParametros = new DetalleParametros(detalle.getId(), newParametro, detalle.getLlave(), detalle.getValor());
                newSetDetalleParametros.add(newDetalleParametros);
            }
            newParametro.setDetalleParametros(newSetDetalleParametros);
            AccionesParametros newAccionParametro = new AccionesParametros(null, newParametro, accionParam.getOrden());
            newListadoParametros.add(newAccionParametro);
        }
        return newListadoParametros;
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Notificaciones> obtenerNotificacionesPorUsuario(int idUsuario) throws Exception {
        List<Notificaciones> listado = new ArrayList<>();
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();
            listado = gate.obtenerNotificacionesNOLeidasPorUsuario(idUsuario, 10);
            if(listado.size() < 10 && listado.size()> 0){
                List<Notificaciones> notificacionesLeidas = gate.obtenerNotificacionesLeidasPorUsuario(idUsuario,10);
                int faltantes = 10 - listado.size();
                for(int i = 1; i < faltantes; i++){
                    listado.add(notificacionesLeidas.get(i));
                }
            }
        } finally {
            gate.cerrarSesion();
        }
        return listado;
    }

    public List<OtAux> obtenerListadoOtSbe(int idContrato) throws Exception {
        List<Ot> listado = new ArrayList<>();
        List<OtAux> listadoFinal = new ArrayList<>();
        Gateway gate = new Gateway();
        try {
            List<String> join = new ArrayList<>();
            gate.abrirSesion();
            listado = gate.obtenerListadoOtSbe(idContrato);
            for (Ot otAux : listado) {
                System.out.println(otAux.getId());
                if(otAux.getId() == 3508){
                    System.out.println("entre evento: "+ otAux.getId());
                }
                OtAux otFinal = new OtAux();
                otFinal.setId(otAux.getId());
                otFinal.setNombre(otAux.getNombre());
                PlanDeProyecto pdp = new PlanDeProyecto();
                if(otAux.getSitio() != null){
                    pdp.setId(otAux.getSitio().getPlanDeProyecto().getId());
                    pdp.setNombre(otAux.getSitio().getPlanDeProyecto().getNombre());

                    Sitios sitio = new Sitios();
                    sitio.setId(otAux.getSitio().getId());
                    sitio.setNombre(otAux.getSitio().getNombre());
                    sitio.setCodigo(otAux.getSitio().getCodigo());
                    sitio.setDireccion(otAux.getSitio().getDireccion());
                    sitio.setLongitud(otAux.getSitio().getLongitud());
                    sitio.setLatitud(otAux.getSitio().getLatitud());

                    otFinal.setSitio(sitio);
                }
                otFinal.setPlanDeProyecto(pdp);
                listadoFinal.add(otFinal);
            }

        } finally {
            gate.cerrarSesion();
        }
        return listadoFinal;
    }

    public List<Regiones> obtenerListadoRegionesPorContrato(int idContrato) throws Exception {
        List<Integer> listado = new ArrayList<>();
        List<Regiones> listadoFinal = new ArrayList<>();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            listado = gate.obtenerListadoRegionesPorContrato(idContrato);
            for (Integer id : listado) {
                Regiones re = (Regiones) gate.retornarObjetoCoreOT(Regiones.class, id);
                Regiones region = new Regiones();
                region.setId(re.getId());
                region.setNombre(re.getNombre());
                region.setCodigo(re.getCodigo());
                listadoFinal.add(region);
            }
        } finally {
            gate.cerrarSesion();
        }
        return listadoFinal;
    }

    public List<Proveedores> obtenerListadoProveedoresPorRegion(int idRegion, int idContrato) throws Exception {
        List<Integer> listado = null;
        List<Proveedores> listadoFinal = new ArrayList<>();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            listado = gate.obtenerListadoProveedoresPorRegion(idRegion, idContrato);
            for (Integer id : listado) {
                Proveedores pro = (Proveedores) gate.retornarObjetoCoreOT(Proveedores.class, id);
                Proveedores proveedores = new Proveedores();
                proveedores.setId(pro.getId());
                proveedores.setNombre(pro.getNombre());
                listadoFinal.add(proveedores);
            }
        } finally {
            gate.cerrarSesion();
        }
        return listadoFinal;
    }
    public List<Regiones> listarRegionesPorProveedorYContrato(int contratoId, int proveedorId) throws Exception {
        List<Regiones> listadoRegiones = new ArrayList<>();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            Map<String,Object> mapaFiltros = new HashMap<>();
            mapaFiltros.put("id.contratoId", contratoId);
            mapaFiltros.put("id.proveedorId", proveedorId);
            List<String> joins = new ArrayList<>();
            joins.add("regiones");
            List<ContratosProveedores> contratosProveedores = (List)gate.listarGenerico(ContratosProveedores.class, mapaFiltros, null);
            for(ContratosProveedores item : contratosProveedores){
                Regiones region = item.getRegiones();
                gate.validarYSetearObjetoARetornar(region, null);
                listadoRegiones.add(region);
            }
            
        } finally {
            gate.cerrarSesion();
        }
        return listadoRegiones;
    }

    public List<Proveedores> listarProveedoresPorContrato(int idContrato){
        List<Proveedores> listadoRetorno = new ArrayList<>();
        Gateway gate = new Gateway();
        try{
            List<ContratosProveedores> contratosProveedores = new ArrayList<ContratosProveedores>();
            gate.abrirSesion();
            if(idContrato == 4 ){
                List<Proveedores> proveedores = new ArrayList<Proveedores>();
                proveedores = gate.obtenerListadoProveedoresPorContratoUnificado(idContrato);
                for(Proveedores item : proveedores){
                    gate.validarYSetearObjetoARetornar(item, null);
                    listadoRetorno.add(item);
                }
            } else {
                contratosProveedores = gate.obtenerListadoProveedoresPorContrato(idContrato);
                for(ContratosProveedores item : contratosProveedores){
                    Proveedores proveedor = item.getProveedores();
                    gate.validarYSetearObjetoARetornar(proveedor, null);
                    listadoRetorno.add(item.getProveedores());
                }
            }

        }catch(Exception ex){
            log.error("Error al obtener Proveedores por contrato.",ex);
        }finally{
            gate.cerrarSesion();
        }
        return listadoRetorno;
    }

    public List<Sitios> obtenerListadoDeSitiosPorPlanDeProyecto(int planDeProyectoId) throws Exception {
        List<Sitios> listado = new ArrayList<>();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            listado = gate.obtenerListadoDeSitiosPorPlanDeProyecto(planDeProyectoId);

        } finally {
            gate.cerrarSesion();
        }
        return listado;
    }

    public void leerNotificacion(int idNotificacion) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            gate.leerNotificacion(idNotificacion);
            gate.commit();
        } finally {
            gate.cerrarSesion();
        }

    }

    public List<Integer> listarIdPmo(int contratoId, int usuarioId) throws Exception {
        Gateway gate = new Gateway();
        List<Integer> listado = new ArrayList<>();
        Map gerencia = new HashMap();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            listado = gate.listarIdPmo(contratoId, usuarioId);
        } finally {
            gate.cerrarSesion();
        }

        return listado;

    }

    public List<GestionEconomica> listarLpIdPmo(int pmoId, int contratoId) throws Exception {
        Gateway gate = new Gateway();
        List<GestionEconomica> listado = new ArrayList<>();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            listado = gate.listarLpIdPmo(pmoId, contratoId);
        } finally {
            gate.cerrarSesion();
        }

        return listado;

    }

    public List<GestionEconomica> listarPep2PorLp(int idPmo, int lp, int contratoId) throws Exception {
        Gateway gate = new Gateway();
        List<GestionEconomica> listado = new ArrayList<>();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            listado = gate.listarPep2PorLp(idPmo, lp, contratoId);
        } finally {
            gate.cerrarSesion();
        }

        return listado;

    }


    public List<Usuarios> obtenerGestoresConstruccion(int proveedorId) throws Exception {
        Gateway gate = new Gateway();
        List<Usuarios> listado = new ArrayList<>();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            listado = gate.obtenerGestoresConstruccion(proveedorId);
        } finally {
            gate.cerrarSesion();
        }

        return listado;

    }


    public List<Contratos> listarContratosPorUsuario(int usuarioId) throws Exception {
        Gateway gate = new Gateway();
        List<Contratos> listado = new ArrayList<>();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            listado = gate.listarContratosPorUsuario(usuarioId);
        } finally {
            gate.cerrarSesion();
        }

        return listado;
    }

    public List<TipoServicio> listarTipoServiciosPorContrato(int contratoId) throws Exception {
        Gateway gate = new Gateway();
        List<TipoServicio> listado = new ArrayList<>();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            listado = gate.listarTipoServiciosPorContrato(contratoId);
        } finally {
            gate.cerrarSesion();
        }

        return listado;

    }

    public List<Agencias> listarAgenciasPorRegion(int regionId) throws Exception {
        Gateway gate = new Gateway();
        List<Agencias> listado = new ArrayList<>();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            listado = gate.listarAgenciasPorRegion(regionId);
            for (Agencias agencia : listado) {
                gate.validarYSetearObjetoARetornar(agencia, null);
            }
        } finally {
            gate.cerrarSesion();
        }

        return listado;

    }

    public List<Centrales> listarCentralesPorAgencia(int agenciaId) throws Exception {
        Gateway gate = new Gateway();
        List<Centrales> listado = new ArrayList<>();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            listado = gate.listarCentralesPorAgencia(agenciaId);
            for (Centrales central : listado) {
                gate.validarYSetearObjetoARetornar(central, null);
            }
        } finally {
            gate.cerrarSesion();
        }

        return listado;

    }

 public List<Familias> listarFamiliasMateriales() throws Exception {
        Gateway gate = new Gateway();
        List<Familias> listado = new ArrayList<>();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            listado = gate.listarFamiliasMateriales();
//            for (Centrales central : listado) {
//                gate.validarYSetearObjetoARetornar(central, null);
//            }
        } finally {
            gate.cerrarSesion();
        }

        return listado;

    }

    public List<Materiales> listarMaterialesPorFamilia(int familiaId) throws Exception {
        Gateway gate = new Gateway();
        List<Materiales> listado = new ArrayList<>();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            listado = gate.listarMaterialesPorFamilia(familiaId);
            for (Materiales material : listado) {
                gate.validarYSetearObjetoARetornar(material, null);
            }
        } finally {
            gate.cerrarSesion();
        }

        return listado;

    }


    public Eventos obtenerEventoPorId(int eventoId) throws Exception {
        Eventos evento = new Eventos();
        Gateway gate = new Gateway();
        try {
            List<Eventos> listado = new ArrayList<>();
            gate.abrirSesion();
            evento = gate.listarEventosPorId(eventoId);
        } finally {
            gate.cerrarSesion();
        }
        return evento;
    }

    public List<UsuariosEventosContratos> obtenerUsuariosEventosContratos(int eventoId, int contratoId) throws Exception {
        List<UsuariosEventosContratos> usuariosEventosContratos = new ArrayList<>();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            usuariosEventosContratos = gate.obtenerUsuariosEventosContratos(eventoId, contratoId);
        } finally {
            gate.cerrarSesion();
        }
        return usuariosEventosContratos;
    }

    public Usuarios obtenerUsuario(int usuarioId) throws Exception {
        Usuarios usuario = new Usuarios();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            usuario = gate.obtenerUsuarioPorId(usuarioId);
        } finally {
            gate.cerrarSesion();
        }
        return usuario;
    }

    public List<Usuarios> obtenerUsuariosPorRolContrato(int rolId, int contratoId) throws Exception {
        List<Usuarios> usuarios = new ArrayList<>();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            usuarios = gate.obtenerUsuariosPorRolContrato(rolId, contratoId);
        } finally {
            gate.cerrarSesion();
        }
        return usuarios;
    }

    public List<Usuarios> obtenerUsuariosOrganigrama(int rolId, int usuarioId, int contratoId) throws Exception {
        List<Usuarios> usuarios = new ArrayList<>();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            usuarios = gate.obtenerOrganigrama(rolId, usuarioId, contratoId);
        } finally {
            gate.cerrarSesion();
        }
        return usuarios;
    }

    public List<Usuarios> obtenerUsuariosOrganigramaSinRol(int usuarioId, int contratoId) throws Exception {
        List<Usuarios> usuarios = new ArrayList<>();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            usuarios = gate.obtenerOrganigramaSinRol(usuarioId, contratoId);
        } finally {
            gate.cerrarSesion();
        }
        return usuarios;
    }

    public List<Usuarios> obtenerUsuariosPorRolProovedor(int rolId, int proveedorId, int contratoId) throws Exception {
        List<Usuarios> usuarios = new ArrayList<>();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            usuarios = gate.obtenerUsuariosPorRolProovedor(rolId, proveedorId, contratoId);
        } finally {
            gate.cerrarSesion();
        }
        return usuarios;
    }


    public List<Usuarios> obtenerUsuariosOrganigramaEECC(int usuarioId, int proveedorId, int contratoId) throws Exception {
        List<Usuarios> usuarios = new ArrayList<>();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            usuarios = gate.obtenerUsuariosOrganigramaEECC(usuarioId, proveedorId, contratoId);
        } finally {
            gate.cerrarSesion();
        }
        return usuarios;
    }



    public List<UsuariosValidadores> obtenerUsuariosValidadores(int otId) throws Exception {
        List<UsuariosValidadores> listadoUsuariosValidadores = null;
        List<UsuariosValidadores> listaRetorno = new ArrayList();
        Gateway gate = new Gateway();
        
        try {
            gate.abrirSesion();
            listadoUsuariosValidadores = gate.listarUsuariosValidadoresPorOt(otId);
            for (UsuariosValidadores uv : listadoUsuariosValidadores) {
                UsuariosValidadores usuariosValidadores = new UsuariosValidadores();
                
                int eventoId = uv.getEventos().getId();
                String nombreEvento = uv.getEventos().getNombre();
                Eventos evento = new Eventos();
                evento.setId(eventoId);
                evento.setNombre(nombreEvento);
                usuariosValidadores.setEventos(evento);
                
                int usuarioId = uv.getUsuarios().getId();
                String nombresUsuario = uv.getUsuarios().getNombres();
                String apellidosUsuario = uv.getUsuarios().getApellidos();
                Usuarios user = new Usuarios();
                user.setId(usuarioId);
                user.setNombres(nombresUsuario);
                user.setApellidos(apellidosUsuario);
                usuariosValidadores.setUsuarios(user);
                
                int conceptoId = uv.getConceptos().getId();
                String nombreConcepto = "Validación de " + uv.getConceptos().getTabla();
                Conceptos concepto = new Conceptos();
                concepto.setId(conceptoId);
                concepto.setTabla(nombreConcepto);
                concepto.setDescripcion(nombreConcepto);
                usuariosValidadores.setConceptos(concepto);
                            
                listaRetorno.add(usuariosValidadores);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            gate.cerrarSesion();
        }
        return listaRetorno;
    }

    public void registrarJournal(String token, int usuarioId, String path, String queryString, Date fecha)throws Exception{
        Journal journal = new Journal(token,usuarioId, path,queryString,fecha);
        Gateway gate = new Gateway();
        gate.registrarJournal(journal);
    }

    public void registrarValoresTipoCambio(List<TipoMonedaValores> listadoValores)throws Exception{
        Gateway gate = new Gateway();
            for(TipoMonedaValores item : listadoValores){
                try {
                    gate.abrirSesion();
                    gate.iniciarTransaccion();
                    gate.registrarObjCoreOT(item);
                    try{
                        gate.commit();
                    }catch(HibernateException hibernateException){
                        gate.rollback();
                        System.out.println(hibernateException);
                    }
                } finally {
                    gate.cerrarSesion();
                }
            }
    }

    public String obtenerGraficoGlobal(int graficoId, int usuarioId, int contratoId, int proveedorId, int ano, int mes) throws Exception {
        String respJSON = "";
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            respJSON = gate.obtenerGraficoGlobal(graficoId, usuarioId, contratoId, proveedorId, ano, mes);
        } finally {
            gate.cerrarSesion();
        }
        return respJSON;
    }

    public String obtenerGraficoDetalle(int graficoId, int usuarioId, String serie, String categoria1, String categoria2, String categoria3, int mes) throws Exception {
        String respJSON = "";
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            respJSON = gate.obtenerGraficoDetalle(graficoId, usuarioId, serie, categoria1, categoria2, categoria3, mes);
        } finally {
            gate.cerrarSesion();
        }
        return respJSON;
    }

    public double retornarPrecioEnPesosChilenos(double valor, int codigoTipoMoneda)throws Exception{
        double retorno = 0;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.retornarPrecioEnPesosChilenos(valor, codigoTipoMoneda);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public Repositorios obtenerRepositorio(int idRepositorio)throws Exception{
        Repositorios retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerRepositorio(idRepositorio);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<DetalleBolsaGestionEconomica> obtenerListaBolsasOt(int usuarioId, int rolId, int cicloFacturacionId, String estado)throws Exception{
        List<DetalleBolsaGestionEconomica> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaBolsasOt(usuarioId, rolId, cicloFacturacionId, estado);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<DetalleBolsaGestionEconomica> obtenerListaBolsasOtPagadas(int usuarioId, int rolId)throws Exception{
        List<DetalleBolsaGestionEconomica> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaBolsasOtPagadas(usuarioId, rolId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public String obtenerListaBolsasOtPagos(int usuarioId, String estado)throws Exception{
        String retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaBolsasOtPagos(usuarioId, estado);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<DetalleOtPorBolsa> obtenerListaOtsPorBolsa(int usuarioId, DetalleBolsaGestionEconomica bolsa, int rolId)throws Exception{
        List<DetalleOtPorBolsa> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaOtsPorBolsa(usuarioId, bolsa, rolId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<DetalleOtPorBolsa> obtenerListaOtsPorBolsaPagos(int usuarioId, String jsonActas)throws Exception{
        List<DetalleOtPorBolsa> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaOtsPorBolsaPagos(usuarioId, jsonActas);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<OtDetalleBolsasGE> obtenerListaDetalleOtsPorBolsa(int usuarioId, DetalleBolsaGestionEconomica bolsa, String estado, int rolId)throws Exception{
        List<OtDetalleBolsasGE> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaDetalleOtsPorBolsa(usuarioId, bolsa, estado, rolId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<OtDetalleBolsasGE> obtenerListaDetalleOtsPorBolsaPagadas(int usuarioId, DetalleBolsaGestionEconomica bolsa, int rolId)throws Exception{
        List<OtDetalleBolsasGE> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaDetalleOtsPorBolsaPagadas(usuarioId, bolsa, rolId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<OtDetalleBolsasGE> obtenerListaDetalleOtsPorBolsaPagos(int usuarioId, String jsonActas)throws Exception{
        List<OtDetalleBolsasGE> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaDetalleOtsPorBolsaPagos(usuarioId, jsonActas);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }


    /*public DetalleBolsaGestionEconomica obtenerBolsaPorId(int bolsaId, int usuarioId, int rolId)throws Exception{
        DetalleBolsaGestionEconomica retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerBolsaPorId(bolsaId, usuarioId, rolId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }*/

    public List<ServiciosOt> obtenerListaDetalleServiciosPorActa(int actaId)throws Exception{
        List<ServiciosOt> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaDetalleServiciosPorActa(actaId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<Object[]> obtenerListaReporteOtExcel(int usuarioId)throws Exception{
        List<Object[]> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaReporteOtExcel(usuarioId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<Object[]> obtenerHojaReporte(int hojaId, int usuarioId)throws Exception{
        List<Object[]> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerHojaReporte(hojaId, usuarioId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<Object[]> obtenerHojasPorReporte(int reporteId)throws Exception{
        List<Object[]> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerHojasPorReporte(reporteId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public Object[] obtenerReportePorRol(int rolId)throws Exception{
        Object[] retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerReportePorRol(rolId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<Object[]> obtenerListaReporteOtTipoServicioExcel(int usuarioId)throws Exception{
        List<Object[]> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaReporteOtTipoServicioExcel(usuarioId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<Object[]> obtenerListaReporteOtServicioExcel(int usuarioId)throws Exception{
        List<Object[]> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaReporteOtServicioExcel(usuarioId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<Object[]> obtenerListaReporteOtActividadesPlazosExcel(int usuarioId)throws Exception{
        List<Object[]> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaReporteOtActividadesPlazosExcel(usuarioId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<Object[]> obtenerListaReporteOtContratoActividadExcel(int usuarioId)throws Exception{
        List<Object[]> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaReporteOtContratoActividadExcel(usuarioId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<Object[]> obtenerListaReporteGestionEconomicaExcel(int usuarioId, String estado, int rolId)throws Exception{
        List<Object[]> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaReporteGestionEconomicaExcel(usuarioId, estado, rolId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<Object[]> obtenerListaReporteGestionEconomicaExcelPagadas(int usuarioId, int rolId)throws Exception{
        List<Object[]> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaReporteGestionEconomicaExcelPagadas(usuarioId, rolId);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public List<Object[]> obtenerListaReporteGestionEconomicaExcelDash(int usuarioId, int rolId, boolean flagAprobadas, boolean flagGlobal)throws Exception{
        List<Object[]> retorno = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            retorno = gate.obtenerListaReporteGestionEconomicaExcelDash(usuarioId, rolId, flagAprobadas, flagGlobal);
        } finally {
            gate.cerrarSesion();
        }
        return retorno;
    }

    public void validarEjecucionDeEvento(int usuarioId, int eventoId, int otId)throws Exception{
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            boolean ejecucionPermitida;
            ejecucionPermitida = gate.validarEjecucionDeEvento(usuarioId, eventoId, otId);
            if(ejecucionPermitida==false){
                throw new Exception("Usuario NO posee permiso para ejecutar este evento.");
            }
        } finally {
            gate.cerrarSesion();
        }
    }

    public void crearMotivoRechazoGestionEconomica(MotivosRechazosGestionEconomica motivoRechazoGestionEconomica)throws Exception{
        Gateway gate = new Gateway();
            try {
                gate.abrirSesion();
                gate.iniciarTransaccion();
                gate.registrarObjCoreOT(motivoRechazoGestionEconomica);
                try{
                    gate.commit();
                }catch(HibernateException hibernateException){
                    gate.rollback();
                    log.error(hibernateException);
                }
            } finally {
                gate.cerrarSesion();
            }
    }

    public void actualizarBolsas(String jsonBolsa, int usuarioId, int rolId)throws Exception{
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            gate.actualizarBolsas(jsonBolsa, usuarioId, rolId);
            try{
                gate.commit();
            }catch(HibernateException hibernateException){
                gate.rollback();
                log.error(hibernateException);
            }
        } finally {
            gate.cerrarSesion();
        }
    }

    public void validarBolsas(String jsonBolsas, int usuarioId, int rolId)throws Exception{
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            gate.validarBolsas(jsonBolsas, usuarioId, rolId);
            try{
                gate.commit();
            }catch(HibernateException hibernateException){
                gate.rollback();
                log.error(hibernateException);
            }
        } finally {
            gate.cerrarSesion();
        }
    }

    public byte[]  obtenerPdfActa(int actaId, int otId, int usuarioId)throws Exception{
        Gateway gate = new Gateway();
        String xmlString;
        String nombrePdf;
        String nombreXsl;
        int contrato;
        byte salida [] = null;
        try {
            contrato = ((Ot) OtDAO.getINSTANCE().obtenerDetalleOt(otId)).getContrato().getId();
            nombrePdf = "pdf_acta_"+otId+"_"+actaId;
            nombreXsl = "plantilla_acta_contrato_"+contrato;

            gate.abrirSesion();
            gate.iniciarTransaccion();

            Map<String,Object> filtros =  new HashMap<>();
            Config config = Config.getINSTANCE();
            filtros.put("nombre",config.getNombreRepositorioBase());
            filtros.put("estadoActivo",Boolean.TRUE);

            Repositorios repo = (Repositorios)gate.obtenerRegistroConFiltro(Repositorios.class, null, filtros);

            xmlString = gate.obtenerXmlActa(actaId);

            salida = RenderPDF.getINSTANCE().generarPDF_From_XmlString_XslFTP_toByte(xmlString, repo, nombrePdf, nombreXsl);

            try{
                gate.commit();
            }catch(HibernateException hibernateException){
                gate.rollback();
                log.error(hibernateException);
            }
        } finally {
            gate.cerrarSesion();
        }
        return salida;
    }

    public CheckList obtenerCheckList(int contratoId)throws Exception{
        Gateway gate = new Gateway();
        CheckList retorno = new CheckList();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                retorno = gate.obtenerCheckList(contratoId);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }
            return retorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    public CheckListOt obtenerCheckListOtPorId(int checkListOtId)throws Exception{
        Gateway gate = new Gateway();
        CheckListOt retorno = new CheckListOt();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                retorno = gate.obtenerCheckListOtPorId(checkListOtId);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }
            return retorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    public int registrarCheckListOt(CheckListOt checkListOt)throws Exception{
        Gateway gate = new Gateway();
        int retorno = 0;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                retorno = gate.registrarCheckListOt(checkListOt);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }
            gate.commit();
            return retorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    public void actualizarPuntuacionCheckListOt(int idCheckListOT, int puntuacion)throws Exception{
        Gateway gate = new Gateway();

        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                gate.actualizarPuntuacionCheckListOt(idCheckListOT, puntuacion);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }
            gate.commit();

        } finally {
            gate.cerrarSesion();
        }
    }

    public boolean actualizarCheckListOtDetalle(int idCheckListOt, int idCheckListOtDetalle, int idLibroObras)throws Exception{
        Gateway gate = new Gateway();
        boolean actualizo = false;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                actualizo = gate.actualizarCheckListOtDetale(idCheckListOt, idCheckListOtDetalle, idLibroObras);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }
            gate.commit();
            return actualizo;
        } finally {
            gate.cerrarSesion();
        }
    }

    public List<CheckListOtDetalle> obtenerCheckListOtDetalle(int idCheckListOt)throws Exception{
        Gateway gate = new Gateway();
        List<CheckListOtDetalle> listado = new ArrayList<>();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                listado = gate.obtenerCheckListOtDetalle(idCheckListOt);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }
            return listado;
        } finally {
            gate.cerrarSesion();
        }
    }

    public List<CheckListOtAux> obtenerCheckListOtPorOtID(int otId)throws Exception{
        Gateway gate = new Gateway();
        List<CheckListOtAux> listado = new ArrayList<>();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                listado = gate.obtenerCheckListOtPorOtID(otId);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }
            return listado;
        } finally {
            gate.cerrarSesion();
        }
    }


    public int registrarCheckListOtDetalle(CheckListOtDetalle checkListOtDetalle)throws Exception{
        Gateway gate = new Gateway();
        int id = 0;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                id = gate.registrarCheckListOtDetalle(checkListOtDetalle);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }
            gate.commit();
            return id;
        } finally {
            gate.cerrarSesion();
        }
    }

    public void adjuntarHem(ArchivoAux archivo, String numeroHem, int usuarioId) throws Exception {
        Gateway gate = new Gateway();
        Map<String,Object> filtros =  new HashMap<>();
        Config config = Config.getINSTANCE();
        filtros.put("nombre",config.getNombreRepositorioBase());
        filtros.put("estadoActivo",Boolean.TRUE);
        Repositorios repo = null;
        String ruta = "/Gestion_Economica/HEM/";
        try {
            gate.abrirSesion();
            repo = (Repositorios) gate.obtenerRegistroConFiltro(Repositorios.class, null, filtros);
            AdjuntosPagos adjuntoPago = null;
            boolean existeAdjunto = false;

            Map<String,Object> filtrosAdjuntosPagos =  new HashMap<>();
            filtrosAdjuntosPagos.put("hemId", Long.parseLong(numeroHem));
            adjuntoPago = (AdjuntosPagos)gate.obtenerRegistroConFiltro(AdjuntosPagos.class, null, filtrosAdjuntosPagos);

            if(adjuntoPago == null){
                adjuntoPago = new AdjuntosPagos();
                adjuntoPago.setCarpeta(repo.getUrl() + ruta);
                adjuntoPago.setExtension(archivo.getExtension());
                adjuntoPago.setNombre(archivo.getNombre());
                adjuntoPago.setHemId(Long.parseLong(numeroHem));
                String SFTPHost = repo.getServidor();//"172.25.186.136";
                String SFTPUser = repo.getUsuario();//"otec-sftp";
                String SFTPPassword = repo.getPassword();//"otec12345";
                String SFTPDir = repo.getUrl()+ ruta;//"OTEC/Gestion_Economica/" + numeroHem;
                SecureTransmission.makeDirectory(SFTPHost, SFTPUser, SFTPPassword, SFTPDir);

                String nombreFile = archivo.getNombre();
                SecureTransmission.sendFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir, nombreFile, archivo.getBaos().toByteArray());
            }else{
                existeAdjunto = true;
                adjuntoPago.setCarpeta(repo.getUrl() + ruta);
                adjuntoPago.setExtension(archivo.getExtension());
                adjuntoPago.setNombre(archivo.getNombre());

                if(adjuntoPago.getPagos() == null){
                    String SFTPHost = repo.getServidor();//"172.25.186.136";
                    String SFTPUser = repo.getUsuario();//"otec-sftp";
                    String SFTPPassword = repo.getPassword();//"otec12345";
                    String SFTPDir = repo.getUrl()+ ruta;//"OTEC/Gestion_Economica/" + numeroHem;
                    SecureTransmission.makeDirectory(SFTPHost, SFTPUser, SFTPPassword, SFTPDir);

                    String nombreFile = archivo.getNombre();
                    SecureTransmission.sendFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir, nombreFile, archivo.getBaos().toByteArray());
                }else throw new Exception("Ya existe pago asociado a esta Hem.");
            }

            gate.iniciarTransaccion();
            try {
                if(existeAdjunto){
                    gate.actualizar2(adjuntoPago);
                }else{
                    gate.registrar(adjuntoPago);
                }
                gate.commit();
            } catch (HibernateException he) {
                log.error("Error al adjuntar HEM.", he);
                gate.rollback();
                throw he;
            }
        } finally {
            gate.cerrarSesion();
        }


    }

    public AdjuntosPagos obtenerArchivoHem(Integer idPago)throws Exception{
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                Map<String,Object> filtros =  new HashMap<>();
                Config config = Config.getINSTANCE();
                filtros.put("nombre",config.getNombreRepositorioBase());
                filtros.put("estadoActivo",Boolean.TRUE);
                gate.obtenerRegistroConFiltro(Repositorios.class,null,filtros);
                Repositorios repo = (Repositorios)gate.obtenerRegistroConFiltro(Repositorios.class, null, filtros);
                Pagos registro = (Pagos)gate.retornarObjetoCoreOT(Pagos.class,idPago);
                Map<String,Object> filtrosAdjuntosPagos =  new HashMap<>();
                filtrosAdjuntosPagos.put("hemId", registro.getHemId());
                AdjuntosPagos adjuntoPago = (AdjuntosPagos)gate.obtenerRegistroConFiltro(AdjuntosPagos.class, null, filtrosAdjuntosPagos);
                gate.validarYSetearObjetoARetornar(adjuntoPago,null);
                String ruta = adjuntoPago.getCarpeta();
                String nombreArchivo = adjuntoPago.getNombre();
                adjuntoPago.setArchivoHem(SecureTransmission.getFile(repo.getServidor(), repo.getUsuario(),
                                                                        repo.getPassword(), ruta, nombreArchivo));
                return adjuntoPago;
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
                throw new Exception("Error al obtener archivo HEM.");
            }
        } finally {
            gate.cerrarSesion();
        }
    }

    public int obtenerIdPagoPorBolsa(int bolsaId)throws Exception{
        Gateway gate = new Gateway();
        int pagoId = 0;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                pagoId = gate.obtenerIdPagoPorBolsa(bolsaId);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }
            return pagoId;
        } finally {
            gate.cerrarSesion();
        }
    }

    public int obtenerIdPagoPorActa(int actaId)throws Exception{
        Gateway gate = new Gateway();
        int pagoId = 0;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                pagoId = gate.obtenerIdPagoPorActa(actaId);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }
            return pagoId;
        } finally {
            gate.cerrarSesion();
        }
    }

    public boolean existePdfActaPagada(int actaId)throws Exception{
        Gateway gate = new Gateway();
        boolean estaPdf = false;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                estaPdf = gate.existePdfActaPagada(actaId);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }
            return estaPdf;
        } finally {
            gate.cerrarSesion();
        }
    }


    public boolean validaPagoProbado(int actaId)throws Exception{
        Gateway gate = new Gateway();
        boolean pagoAprobado = false;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                pagoAprobado = gate.validaPagoProbado(actaId);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }
            return pagoAprobado;
        } finally {
            gate.cerrarSesion();
        }
    }


    public byte[] obtenerPdfActaPagada(int actaId, int otId)throws Exception{
        Gateway gate = new Gateway();
        byte salida [] = null;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{

                Map<String,Object> filtros =  new HashMap<>();
                Config config = Config.getINSTANCE();
                filtros.put("nombre",config.getNombreRepositorioBase());
                filtros.put("estadoActivo",Boolean.TRUE);
                String ruta = "/Gestion_Economica/Actas_Pagadas/";

                Repositorios repo = (Repositorios)gate.obtenerRegistroConFiltro(Repositorios.class, null, filtros);
                //String pdfActaPagada = ruta + "pdf_acta_"+otId+"_"+actaId;
                String rutaArchivo = repo.getUrl()+ruta;
                String nombreArchivo = "pdf_acta_"+otId+"_"+actaId+".pdf";
                salida = SecureTransmission.getFile(repo.getServidor(), repo.getUsuario(), repo.getPassword(), rutaArchivo, nombreArchivo);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
                throw new Exception("Error al obtener pdf Acta Pagada.");
            }
        } finally {
            gate.cerrarSesion();
        }
        return salida;
    }

    public static GenericDAO getINSTANCE() {
        return INSTANCE;
    }

    private static final GenericDAO INSTANCE = new GenericDAO();
    private static final Logger log = Logger.getLogger(GenericDAO.class);

    public Map obtieneServiciosDiseno(int otId)throws Exception {
        Map mapaServiciosDiseno = new HashMap();
        Gateway gate = new Gateway();
        try{
            gate.abrirSesion();
            gate.iniciarTransaccion();
            mapaServiciosDiseno = gate.obtieneServiciosDisenoAp(otId);

            return mapaServiciosDiseno;
        }catch (Exception e){
            throw e;
        }
    }

    public List<WorkflowEventosEjecucion> obtenerWeePorUsuario(int usuariosId) throws Exception{
        List<WorkflowEventosEjecucion> listadoWEE = new ArrayList<>();
        Gateway gate = new Gateway();
        int id = 0;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                listadoWEE = gate.obtenerWeePorUsuario(usuariosId);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }

            return listadoWEE;
        } finally {
            gate.cerrarSesion();
        }
    }

    public List<UsuariosOt> obtenerusuariosOTPorUsuario(int usuariosId) throws Exception{
        List<UsuariosOt> listadousuariosOt = new ArrayList<>();
        Gateway gate = new Gateway();
        int id = 0;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                listadousuariosOt = gate.obtenerusuariosOTPorUsuario(usuariosId);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }

            return listadousuariosOt;
        } finally {
            gate.cerrarSesion();
        }
    }

    public int obtenerGestorPorOtId(int otId) throws Exception{
        int gestorId = 0;
        Gateway gate = new Gateway();
        int id = 0;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                gestorId = gate.obtenerGestorPorOtId(otId);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }

            return gestorId;
        } finally {
            gate.cerrarSesion();
        }
    }

    public List<Ot> obtenerOtDeUsuarios(List<Integer> idGestores) throws Exception{
        List<Ot> listadoOt = new ArrayList<>();
        Gateway gate = new Gateway();
        int id = 0;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try{
                listadoOt = gate.obtenerOtDeUsuarios(idGestores);
            }catch(HibernateException hibernateException){
                log.error(hibernateException);
            }

            return listadoOt;
        } finally {
            gate.cerrarSesion();
        }
    }

    public boolean reemplazaTabsBd(int contratoId)throws Exception {
        boolean respuesta = false;
        Gateway gate = new Gateway();
        try{
            gate.abrirSesion();
            gate.iniciarTransaccion();
            respuesta = gate.reemplazaTabsBdGateways(contratoId);
            gate.commit();
            respuesta = true;
            return respuesta;
        }catch (Exception e){
            throw e;
        }finally {
            gate.cerrarSesion();
        }

    }

    public Map obtieneDetalleActaBucle() {
        return null;
    }
}
