/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.*;
import com.skydream.coreot.util.Config;
import com.skydream.coreot.util.SecureTransmission;
import com.skydream.coreot.util.SendMail;

import java.io.File;
import java.util.*;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;

/**
 *
 * @author rrr
 */
public class LibroObraDAO implements GatewayDAO {

    public static LibroObraDAO getINSTANCE() {
        return INSTANCE;
    }

    private LibroObraDAO() {
    }

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        Gateway gateway = new Gateway();
        LibroObras libroObra = (LibroObras) obj;

        int idLibroObra = 0;
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                Usuarios usuarioEjecutor = new Usuarios();
                usuarioEjecutor.setId(usuarioId);
                libroObra.setUsuarioEjecutor(usuarioEjecutor);

                Date fechaCreacion = new Date();
                libroObra.setFechaCreacion(fechaCreacion);

                idLibroObra = gateway.registrarConId(obj, "LibroObra");

                gateway.commit();

                int otId = libroObra.getOt().getId();
                Ot ot = (Ot) gateway.retornarObjetoCoreOT(Ot.class, otId);
                Hibernate.initialize(ot);

                int usuarioGestorId = ot.getGestor().getId();

                Usuarios usuarioGestor = (Usuarios) gateway.retornarObjetoCoreOT(Usuarios.class, usuarioGestorId);
                Hibernate.initialize(usuarioGestor);

                Config config = Config.getINSTANCE();

                String asunto = config.getAsuntoMailGestorInformeLibroObra();
                String cuerpo = config.getCuerpoMailGestorInformeLibroObra() + "\n";
                enviarMail(usuarioGestor, asunto, cuerpo);

            } catch (HibernateException e) {
                log.fatal("Error al crear LibroObra. ", e);
                gateway.rollback();
                throw e;
            }
            return idLibroObra;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    @Override
    public List listar(ObjetoCoreOT obj, Map params) throws Exception {
        Ot ot = (Ot) obj;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = gate.listar(ot, join);
            for (ObjetoCoreOT objeto : listaRetorno) {
                Ot ser = (Ot) objeto;

                gate.validarYSetearObjetoARetornar(ser, join);
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    private void enviarMail(Usuarios usuario, String asunto, String cuerpo) throws Exception {
        try {
            String mailDestino = usuario.getEmail();
            SendMail sendMail = SendMail.getINSTANCE();
            boolean envioOK = sendMail.enviarMail(mailDestino, asunto, cuerpo, null);
            if (!envioOK) {
                throw new Exception("Se produjo un error al enviar email.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public static void gestionarAdjuntoLibroObraOt2(List<ArchivoAux> listArchivos, LibroObras libroObras, String[] roles, int usuarioId, int otId) throws Exception {
        Gateway gateway = new Gateway();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                Set<Roles> listadoRoles = new HashSet<>();
                for(String rol: roles){
                    listadoRoles.add((Roles)gateway.retornarObjetoCoreOT(Roles.class,Integer.parseInt(rol)));
                }
                libroObras.setLibroObrasRoles(listadoRoles);
                Usuarios usrEjecutor = (Usuarios)gateway.retornarObjetoCoreOT(Usuarios.class,usuarioId);
                libroObras.setUsuarioEjecutor(usrEjecutor);
                libroObras.setFechaCreacion(new Date());
                gateway.persistirObjeto(libroObras);

                if(!listArchivos.isEmpty()){
                    Map<String,Object> filtros =  new HashMap<>();
                    Config config = Config.getINSTANCE();
                    filtros.put("nombre",config.getNombreRepositorioBase());
                    filtros.put("estadoActivo",Boolean.TRUE);
                    gateway.obtenerRegistroConFiltro(Repositorios.class,null,filtros);
                    Repositorios repositorio = (Repositorios)gateway.obtenerRegistroConFiltro(Repositorios.class,null,filtros);
                    String nombreCarpeta = "Ordenes_de_Trabajo";
                    String rutaAlmacenar = "Ot-" + otId + "/" +"Libro-" +libroObras.getId();

                    for (ArchivoAux archivo: listArchivos){
                        AdjuntosLibroObras adjLibroObra = new AdjuntosLibroObras();
                        adjLibroObra.setRepositorio(repositorio);
                        adjLibroObra.setLibroObra(libroObras);
                        adjLibroObra.setNombre(archivo.getNombre());
                        adjLibroObra.setCarpeta(repositorio.getUrl() + "/" + nombreCarpeta + "/" + rutaAlmacenar);
                        adjLibroObra.setExtension(archivo.getExtension());
                        gateway.registrar((ObjetoCoreOT) adjLibroObra);
                    }
                    guardarAdjuntoLibroObraOt2(repositorio, listArchivos, nombreCarpeta, rutaAlmacenar);
                }

                gateway.commit();

            } catch (Exception e) {
                log.fatal("Error al crear LibroObra. ", e);
                gateway.rollback();
                throw e;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    public static int gestionarAdjuntoLibroObraOt3(List<ArchivoAux> listArchivos, LibroObras libroObras, String[] roles, int usuarioId, int otId, String nombreItem) throws Exception {
        Gateway gateway = new Gateway();
        int idLibroObra = 0;
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                Set<Roles> listadoRoles = new HashSet<>();
                for(String rol: roles){
                    listadoRoles.add((Roles)gateway.retornarObjetoCoreOT(Roles.class,Integer.parseInt(rol)));
                }
                String observacion = "Observación Calidad para el ITEM " + nombreItem + " : "+ libroObras.getObservaciones();
                libroObras.setObservaciones(observacion);
                libroObras.setLibroObrasRoles(listadoRoles);
                Usuarios usrEjecutor = (Usuarios)gateway.retornarObjetoCoreOT(Usuarios.class,usuarioId);
                libroObras.setUsuarioEjecutor(usrEjecutor);
                libroObras.setFechaCreacion(new Date());
                gateway.persistirObjeto(libroObras);
                idLibroObra = libroObras.getId();

                if(!listArchivos.isEmpty()){
                    Map<String,Object> filtros =  new HashMap<>();
                    Config config = Config.getINSTANCE();
                    filtros.put("nombre",config.getNombreRepositorioBase());
                    filtros.put("estadoActivo",Boolean.TRUE);
                    gateway.obtenerRegistroConFiltro(Repositorios.class,null,filtros);
                    Repositorios repositorio = (Repositorios)gateway.obtenerRegistroConFiltro(Repositorios.class,null,filtros);
                    String nombreCarpeta = "Ordenes_de_Trabajo";
                    String rutaAlmacenar = "Ot-" + otId + "/" +"Libro-" +libroObras.getId();

                    for (ArchivoAux archivo: listArchivos){
                        AdjuntosLibroObras adjLibroObra = new AdjuntosLibroObras();
                        adjLibroObra.setRepositorio(repositorio);
                        adjLibroObra.setLibroObra(libroObras);
                        adjLibroObra.setNombre(archivo.getNombre());
                        adjLibroObra.setCarpeta(repositorio.getUrl() + "/" + nombreCarpeta + "/" + rutaAlmacenar);
                        adjLibroObra.setExtension(archivo.getExtension());
                        gateway.registrar((ObjetoCoreOT) adjLibroObra);
                    }
                    guardarAdjuntoLibroObraOt2(repositorio, listArchivos, nombreCarpeta, rutaAlmacenar);
                }

                gateway.commit();

            } catch (Exception e) {
                log.fatal("Error al crear LibroObra. ", e);
                gateway.rollback();
                throw e;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
        return idLibroObra;
    }




    public static int gestionarAdjuntoLibroObraCalidadOt(List<ArchivoAux> listArchivos, LibroObras libroObras, String[] roles, int usuarioId, int otId) throws Exception {
        int idLibroObra = 0;
        Gateway gateway = new Gateway();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                Set<Roles> listadoRoles = new HashSet<>();
                for(String rol: roles){
                    listadoRoles.add((Roles)gateway.retornarObjetoCoreOT(Roles.class,Integer.parseInt(rol)));
                }
                libroObras.setLibroObrasRoles(listadoRoles);
                Usuarios usrEjecutor = (Usuarios)gateway.retornarObjetoCoreOT(Usuarios.class,usuarioId);
                libroObras.setUsuarioEjecutor(usrEjecutor);
                libroObras.setFechaCreacion(new Date());
                idLibroObra = gateway.registrarLibroObrasCalidad(libroObras);

                if(!listArchivos.isEmpty()){
                    Map<String,Object> filtros =  new HashMap<>();
                    Config config = Config.getINSTANCE();
                    filtros.put("nombre",config.getNombreRepositorioBase());
                    filtros.put("estadoActivo",Boolean.TRUE);
                    gateway.obtenerRegistroConFiltro(Repositorios.class,null,filtros);
                    Repositorios repositorio = (Repositorios)gateway.obtenerRegistroConFiltro(Repositorios.class,null,filtros);
                    String nombreCarpeta = "Ordenes_de_Trabajo";
                    String rutaAlmacenar = "Ot-" + otId + "/" +"Libro-Calidad-" +libroObras.getId();

                    for (ArchivoAux archivo: listArchivos){
                        AdjuntosLibroObras adjLibroObra = new AdjuntosLibroObras();
                        adjLibroObra.setRepositorio(repositorio);
                        adjLibroObra.setLibroObra(libroObras);
                        adjLibroObra.setNombre(archivo.getNombre());
                        adjLibroObra.setCarpeta(repositorio.getUrl() + "/" + nombreCarpeta + "/" + rutaAlmacenar);
                        adjLibroObra.setExtension(archivo.getExtension());
                        gateway.registrarAdjuntosLibroObrasCalidad(adjLibroObra);
                    }
                    guardarAdjuntoLibroObraOt2(repositorio, listArchivos, nombreCarpeta, rutaAlmacenar);
                }

                gateway.commit();

            } catch (Exception e) {
                log.fatal("Error al crear LibroObra. ", e);
                gateway.rollback();
                throw e;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }

        return idLibroObra;
    }


    public static void adjuntarCartaAdjudicacionOt(ArchivoAux archivo, int usuarioId, Long numeroCarta, int otId, CartaAdjudicacion cartaAdjudicacion, String[] roles) throws Exception {
        Gateway gateway = new Gateway();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                //Aqui se asocian los roles para cada archivo subido, esto se alamacena en la tabla adjuntos_roles.
                Set<Roles> listadoRoles = new HashSet<>();
                for(String rol: roles){
                    listadoRoles.add((Roles)gateway.retornarObjetoCoreOT(Roles.class,Integer.parseInt(rol)));
                }

                List<Usuarios> usuarios = gateway.listarUsuariosPorId(usuarioId);

                Repositorios repositorio = (Repositorios) gateway.retornarObjetoCoreOT(Repositorios.class, usuarios.get(0).getRepositorio().getId());
                Hibernate.initialize(repositorio);

                String nombreCarpeta = "Ordenes_de_Trabajo";
                String rutaAlmacenar = "Ot-"+otId + "/" + "CartaAdjudicacion-" + numeroCarta ;


                Adjuntos adjunto = new Adjuntos();
                adjunto.setCarpeta(repositorio.getUrl() + "/" + nombreCarpeta + "/" + rutaAlmacenar);
//                adjunto.setCarpeta(repositorio.getUrl() + rutaAlmacenar);
                adjunto.setAdjuntosRoles(listadoRoles);
                adjunto.setDescripcion("Carta de adjudicacion numero: " + numeroCarta);
                adjunto.setExtension(archivo.getExtension());
                adjunto.setNombre(archivo.getNombre());
                adjunto.setRepositorio(repositorio);
                Ot ot = new Ot();
                ot.setId(otId);
                adjunto.setOt(ot);

                int idAdjuducacion = gateway.registrarAdjunto(adjunto);
                adjunto.setId(idAdjuducacion);

                cartaAdjudicacion.setAdjuntoId(adjunto.getId());
                gateway.registrarCartaAdjudicacion(cartaAdjudicacion);

                guardarAdjuntoCartaAdjudicacionOt(repositorio, archivo, nombreCarpeta, rutaAlmacenar);

                gateway.commit();

            } catch (HibernateException e) {
                log.fatal("Error al crear LibroObra. ", e);
                gateway.rollback();
                throw e;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }


    private static void guardarAdjuntoLibroObraOt2(Repositorios repo, List<ArchivoAux> listArchivos, String nombreCarpeta, String rutaAlmacenar) throws Exception {

        String SFTPHost = repo.getServidor();
        String SFTPUser = repo.getUsuario();
        String SFTPPassword = repo.getPassword();
        String SFTPDir = repo.getUrl() + "/" + nombreCarpeta + "/" + rutaAlmacenar;
        SecureTransmission.makeDirectory(SFTPHost, SFTPUser, SFTPPassword, SFTPDir);

        for (ArchivoAux archivo: listArchivos){
            String nombreFile = archivo.getNombre();
            SecureTransmission.sendFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir, nombreFile, archivo.getBaos().toByteArray());
        }

    }

    private static void guardarAdjuntoCartaAdjudicacionOt(Repositorios repo, ArchivoAux archivo, String nombreCarpeta, String rutaAlmacenar) throws Exception {

        String SFTPHost = repo.getServidor();
        String SFTPUser = repo.getUsuario();
        String SFTPPassword = repo.getPassword();
        String SFTPDir = repo.getUrl() + "/" + nombreCarpeta + "/" + rutaAlmacenar;
        SecureTransmission.makeDirectory(SFTPHost, SFTPUser, SFTPPassword, SFTPDir);


        String nombreFile = archivo.getNombre();
        SecureTransmission.sendFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir, nombreFile, archivo.getBaos().toByteArray());



    }

    public static void gestionarAdjuntoLibroObraOt(int idLibroObra, String nombreArchivo, String extensionArchivo, byte[] archivo, int idLibroDeObras, int usuarioId, int otId, int contador) throws Exception {
        Gateway gateway = new Gateway();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                LibroObras libroObra = (LibroObras) gateway.retornarObjetoCoreOT(LibroObras.class, idLibroObra);
                Hibernate.initialize(libroObra);
                
                int idOt = libroObra.getOt().getId();
                Ot ot = (Ot) gateway.retornarObjetoCoreOT(Ot.class, idOt);
                Hibernate.initialize(ot);

                List<Usuarios> usuarios = gateway.listarUsuariosPorId(usuarioId);

                Repositorios repositorio = (Repositorios) gateway.retornarObjetoCoreOT(Repositorios.class, usuarios.get(0).getRepositorio().getId());
                Hibernate.initialize(repositorio);

                AdjuntosLibroObras adjLibroObra = new AdjuntosLibroObras();

                adjLibroObra.setRepositorio(repositorio);
                adjLibroObra.setLibroObra(libroObra);
                adjLibroObra.setNombre(nombreArchivo);
                //Es para cuando se cree un directorio para cada Ot
                //String carpetaOt = "ot-" + ot.getId() + "/";
                //adjLibroObra.setCarpeta(carpetaOt);

                String nombreCarpeta = "Ordenes de Trabajo";
                String rutaAlmacenar = "Ot-" + otId +"-Libro-" +idLibroDeObras + "_"+contador;
//                adjLibroObra.setCarpeta(repositorio.getUrl() + rutaAlmacenar);
                adjLibroObra.setCarpeta(repositorio.getUrl() + "/" + nombreCarpeta + "/" + rutaAlmacenar);
                adjLibroObra.setExtension(extensionArchivo);
                gateway.registrar((ObjetoCoreOT)adjLibroObra);
                gateway.commit();
                
                guardarAdjuntoLibroObraOt(repositorio, ot, nombreArchivo, archivo, nombreCarpeta, rutaAlmacenar);

            } catch (HibernateException e) {
                log.fatal("Error al crear LibroObra. ", e);
                gateway.rollback();
                throw e;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    private static void guardarAdjuntoLibroObraOt(Repositorios repo, Ot otLibroObra, String nombreArchivo, byte[] arrImagen, String nombreCarpeta, String rutaAlmacenar) throws Exception {
        String nombreFile = nombreArchivo;
        String SFTPHost = repo.getServidor();
        String SFTPUser = repo.getUsuario();
        String SFTPPassword = repo.getPassword();
        String SFTPDir = repo.getUrl() + "/" + nombreCarpeta + "/" + rutaAlmacenar;
        SecureTransmission.makeDirectory(SFTPHost, SFTPUser, SFTPPassword, SFTPDir);
        SecureTransmission.sendFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir, nombreFile, arrImagen);
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            Map<String,Object> filtro = new HashMap<>();
            filtro.put("id",idObjetoCoreOT);
            LibroObras libroObra = (LibroObras)gate.obtenerRegistroConFiltro(LibroObras.class,null,filtro);
            List<String> join = new ArrayList<>();
            join.add("adjuntos");
            gate.validarYSetearObjetoARetornar(libroObra, join);
            return libroObra;
        } finally {
            gate.cerrarSesion();
        }
    }

    public int registrarLibroObras(LibroObras libroObras, Set<Roles> setRoles, int usuarioId) throws Exception {
        Gateway gateway = new Gateway();

        int idLibroObra = 0;
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                Usuarios usuarioEjecutor = new Usuarios();
                usuarioEjecutor.setId(usuarioId);
                libroObras.setUsuarioEjecutor(usuarioEjecutor);


                libroObras.setLibroObrasRoles(setRoles);

                Date fechaCreacion = new Date();
                libroObras.setFechaCreacion(fechaCreacion);

                idLibroObra = gateway.registrarConId(libroObras, "LibroObra");

                int otId = libroObras.getOt().getId();
                Ot ot = (Ot) gateway.retornarObjetoCoreOT(Ot.class, otId);
                Hibernate.initialize(ot);

                int usuarioGestorId = ot.getGestor().getId();

                Usuarios usuarioGestor = (Usuarios) gateway.retornarObjetoCoreOT(Usuarios.class, usuarioGestorId);
                Hibernate.initialize(usuarioGestor);

                Config config = Config.getINSTANCE();

                String asunto = config.getAsuntoMailGestorInformeLibroObra();
                String cuerpo = config.getCuerpoMailGestorInformeLibroObra() + "\n";
                enviarMail(usuarioGestor, asunto, cuerpo);

                gateway.commit();

            } catch (HibernateException e) {
                log.fatal("Error al crear LibroObra. ", e);
                gateway.rollback();
                throw e;
            }
            return idLibroObra;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    private static final Logger log = Logger.getLogger(LibroObraDAO.class);
    private static final LibroObraDAO INSTANCE = new LibroObraDAO();

}
