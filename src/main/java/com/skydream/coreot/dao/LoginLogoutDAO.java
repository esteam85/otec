/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.*;
import com.skydream.coreot.util.SecureAccess;
import java.security.SecureRandom;
import java.util.*;

import org.apache.commons.codec.binary.Base64;
import org.apache.struts2.ServletActionContext;
import org.hibernate.HibernateException;
import org.jboss.logging.Logger;

/**
 *
 * @author mcj
 */
public class LoginLogoutDAO implements GatewayDAO{

    private LoginLogoutDAO() {
    }

    public static Usuarios validarLoginYRetornarUsuario(String nombreUsuario, String clave) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            int idUsuario = gate.validarUsuarioYRetornarId(nombreUsuario, clave);
            Usuarios usr = null;
            if (idUsuario > 0) {
                List lista = gate.listarConFiltro(Usuarios.class, "id", idUsuario);
                if (lista != null) {
                    usr = (Usuarios) lista.get(0);
                    boolean claveValida = SecureAccess.validatePassword(clave, usr.getClave());

                    if (claveValida) {
                        setearRoles(usr);
                        setearContratos(usr);
                        usr.setArea(setearArea(usr));
                        Proveedores provAux = usr.getProveedor();
                        Proveedores prov = new Proveedores(provAux.getId(), null, provAux.getRut(), provAux.getDv(), provAux.getNombre(), provAux.getEmail(), provAux.getTelefono(), provAux.getDireccion(), provAux.getEstado());
                        usr.setProveedor(prov);
                        setearRepositorio(usr);
                        setearNulos(usr);
                    } else {
                        usr = null;
                    }
                }
            }
            return usr;
        } catch (Exception e) {
            log.error(e);
            throw new Exception("Error al validar Credenciales.");
        } finally {
            gate.cerrarSesion();
        }
    }

    public static String retornarTokenSesionUsuario(Usuarios usuario, int rolId, boolean tokenApp) throws Exception {
        boolean existeToken = true;
        Gateway gate = new Gateway();
        String token = null;
        int tipoToken = 0;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            if (tokenApp){
                tipoToken = 2;
                validaSesionUnicaMovil(usuario, rolId, tipoToken, gate);
            } else {
                tipoToken = 1;
            }
            while (existeToken) {
                token = retornarTokenAleatorio();
                existeToken = gate.validarToken(token, 'A');
                if (!existeToken) {
                    Roles rolUsr = (Roles) gate.retornarObjetoCoreOT(Roles.class, rolId);
                    Login login = new Login();
                    login.setUsuarios(usuario);
                    login.setRoles(rolUsr);
                    login.setToken(token);
                    Date fecha = new Date();
                    login.setFecha(fecha);
                    login.setFechaUltimaAccion(fecha);
                    login.setEstado('A');
                    login.setTipoToken(tipoToken);
                    try{
                        gate.registrar(login);
                        gate.commit();
                    }catch(HibernateException he){
                        log.error("Error al registrar login.",he);
                        throw he;
                    }
                    break;
                }
            }
        }catch(Exception ex){
            log.error("Error al registrar login.",ex);
            throw ex;
        }finally {
            gate.cerrarSesion();
        }
        return token;
    }
    private static boolean validaSesionUnicaMovil(Usuarios usuario, int rolId, int tipoToken, Gateway gate) {
        boolean validacion = false;
        List<Login> listaLogin = new ArrayList<Login>();
        try {
            listaLogin = gate.obtieneLogueosAbiertosMovil(usuario, rolId, tipoToken);
            if(listaLogin.size() > 0){
                for(Login login : listaLogin){
                    gate.eliminaSesionMovilAbierta(login);
                }
            }

        }catch (Exception e){
            log.error("Error al modificar login sesion movil, error: "+ e.toString());
            throw e;
        }
        return validacion;
    }

    private static String retornarTokenAleatorio() {
        SecureRandom random = new SecureRandom();
        byte[] arrBytes = new byte[16];
        random.nextBytes(arrBytes);
        return new String(Base64.encodeBase64URLSafe(arrBytes));
    }

    private static void setearNulos(Usuarios usr) {
        usr.setClave(null);
        usr.setOts(null);
        usr.setFolioses(null);
        usr.setHistoricoWorkflows(null);
        usr.setLibroObrases(null);
    }

    private static void setearRepositorio(Usuarios usr) {
        Repositorios rep = usr.getRepositorio();
        Repositorios repositorio = new Repositorios(rep.getId(), rep.getNombre(), null,
                null, null, 0, null, rep.getDescripcion(), null, null);
        usr.setRepositorio(repositorio);
    }

    private static Areas setearArea(Usuarios usr) {
        Areas areaUser = usr.getArea();
        Areas area = new Areas();
        area.setDescripcion(areaUser.getDescripcion());
        area.setId(areaUser.getId());
        area.setNombre(areaUser.getNombre());
        return area;

    }

    private static void setearRoles(Usuarios usr) {
        Set<Roles> rolesUsr = new HashSet<>();
        for (Roles rol : usr.getUsuariosRoles()) {

            Perfiles perf = rol.getPerfil();
            Perfiles perfil = new Perfiles(perf.getId(), perf.getNombre(), perf.getDescripcion(),
                    perf.getEstado(), null);
            Set<Opciones> opciones = setearRolesOpciones(rol);
            Set<Privilegios> privilegios = setearRolesPrivilegios(rol);
            Roles rolAux = new Roles(rol.getId(), perfil, rol.getNombre(), rol.getEstado(), null, privilegios, null, opciones);
            rolesUsr.add(rolAux);
        }
        usr.setUsuariosRoles(rolesUsr);
    }

    private static Set<Privilegios> setearRolesPrivilegios(Roles rol) {
        Set<Privilegios> privilegios = new HashSet<>();
        for (Privilegios priv : rol.getRolesPrivilegios()) {
            Privilegios privilegio = new Privilegios(priv.getId(), priv.getNombre(), priv.getDescripcion(), null);
            privilegios.add(privilegio);
        }
        return privilegios;
    }

    private static Set<Opciones> setearRolesOpciones(Roles rol) {
        Set<Opciones> opciones = new HashSet<>();
        for (Opciones opc : rol.getRolesOpciones()) {
            Opciones opcionRol = new Opciones(opc.getId(), opc.getCodigoMenu(), opc.getNombre(),
                    opc.getDescripcion(), opc.getEstado(), null);
            opciones.add(opcionRol);
        }
        return opciones;
    }

    private static void setearContratos(Usuarios usr) {
        Set<Contratos> contratos = new HashSet<>();
        for (Contratos con : usr.getUsuariosContratos()) {
            Workflow wf = con.getWorkflow();
            Workflow workf = new Workflow(wf.getId(), wf.getNombre(), wf.getDescripcion(), wf.getFechaCreacion(),
                    wf.getEstado(), null, null);
            TipoPlanta tp = con.getTipoPlanta();
            TipoPlanta tipoPlanta = new TipoPlanta(tp.getId(), tp.getNombre(), tp.getDescripcion(), null);
            Contratos contrato = new Contratos(con.getId(), workf, tipoPlanta, con.getNombre(),
                    con.getFechaInicio(), con.getFechaTermino(), con.getEstado(), null,
                    null, null);
            contratos.add(contrato);
        }
        usr.setUsuariosContratos(contratos);
    }

    public boolean validarToken(String passwordEnviada, byte[] salt, String passwordUsuario) throws Exception {

        return false;
    }

    public boolean validarPrivilegioUsuario(String token) throws Exception {
        Gateway gate = new Gateway();
        return false;
    }
    
    public Login retornarLoginSesionActual(String token) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            Login login = gate.retornarLoginSesionActual(token);
            List<String> joins = new ArrayList<>();
            joins.add("usuarios");
            joins.add("roles");
            gate.validarYSetearObjetoARetornar(login,joins);

            return login;
        } catch (Exception e) {
            log.info(e);
            throw new Exception("Error al retornar Login.");
        } finally {
            gate.cerrarSesion();
        }
    }
    
    public boolean validarTokenSesion(String token, int usuarioId) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            boolean existeTokenValido = gate.validarTokenSesion(token, usuarioId, 'A');
            return existeTokenValido;
        } catch (Exception e) {
            log.info(e);
            throw new Exception("Error al validar sesion.");
        } finally {
            gate.cerrarSesion();
        }
    }

    public void cerrarSesion(String token)throws Exception{
        Gateway gate = new Gateway();

        try {
            gate.abrirSesion();
            Login login = gate.retornarLoginSesionActual(token);
            login.setEstado('C');
            gate.iniciarTransaccion();
            try{
                gate.actualizar2(login);
                gate.commit();
            }catch(HibernateException he){
                gate.rollback();
            }

        } catch (Exception e) {
            log.info(e);
            throw new Exception("Error al validar token.");
        } finally {
            gate.cerrarSesion();
        }
    }

    public void setearFechaUltimaAccion(String token)throws Exception{
        Gateway gate = new Gateway();

        try {
            gate.abrirSesion();
            Login login = gate.retornarLoginSesionActual(token);
            login.setFechaUltimaAccion(new Date());
            gate.iniciarTransaccion();
            try{
                gate.actualizar2(login);
                gate.commit();
            }catch(HibernateException he){
                gate.rollback();
            }
        } catch (Exception e) {
            log.info(e);
            throw new Exception("Error al validar token.");
        } finally {
            gate.cerrarSesion();
        }
    }

    public void validarSesionUsuario(String token, int usuarioId, boolean registrarUltimaModificacion)throws Exception{
        if(usuarioId ==0 || !validarTokenSesion(token, usuarioId)){
            throw new Exception("Error al validar sesion de usuario.");
        }else if(registrarUltimaModificacion){
            setearFechaUltimaAccion(token);
        }
    }

    public static LoginLogoutDAO getINSTANCE() {
        return INSTANCE;
    }

    private static final LoginLogoutDAO INSTANCE = new LoginLogoutDAO();
    private static final Logger log = Logger.getLogger(LoginLogoutDAO.class);

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        return false;
    }

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        return 0;
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        return false;
    }

    @Override
    public List listar(ObjetoCoreOT obj, Map params) throws Exception {
        return null;
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        return null;
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        return false;
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        return null;
    }
}
