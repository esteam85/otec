package com.skydream.coreot.dao;

/**
 * Created by Esteam on 04-11-16.
 */
public class MaterialesBucleResponse {

    private int id;
    private String unidadMedida;
    private String tipoMoneda;
    private int tipoMonedaId;
    private String codigo;
    private String codigoUnidadDeObra;
    private String codigoSap;
    private String nombre;
    private String descripcion;
    private double tipoCambio;
    private String distribuidor;
    private double precio;
    private double precioPesos;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public String getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(String tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public int getTipoMonedaId() {
        return tipoMonedaId;
    }

    public void setTipoMonedaId(int tipoMonedaId) {
        this.tipoMonedaId = tipoMonedaId;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigoSap() {
        return codigoSap;
    }

    public void setCodigoSap(String codigoSap) {
        this.codigoSap = codigoSap;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(double tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public double getPrecioPesos() {
        return precioPesos;
    }

    public void setPrecioPesos(double precioPesos) {
        this.precioPesos = precioPesos;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getDistribuidor() {
        return distribuidor;
    }

    public void setDistribuidor(String distribuidor) {
        this.distribuidor = distribuidor;
    }

    public String getCodigoUnidadDeObra() {
        return codigoUnidadDeObra;
    }

    public void setCodigoUnidadDeObra(String codigoUnidadDeObra) {
        this.codigoUnidadDeObra = codigoUnidadDeObra;
    }
}
