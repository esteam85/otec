/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.*;
import com.skydream.coreot.util.Config;
import com.skydream.coreot.util.SendMail;

import java.util.*;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;

/**
 *
 * @author rrr
 */
public class OtDAO implements GatewayDAO {

    private OtDAO() {
    }

    public static OtDAO getINSTANCE() {
        return INSTANCE;
    }

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        Ot ot = (Ot) obj;
        Gateway gateway = new Gateway();
        int idOt = 0;
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                Date fechaCreacion = new Date();
                ot.setFechaCreacion(fechaCreacion);
                idOt = gateway.registrarConId(obj,"Ot");
                //Obtener el cubicador segun id enviado
                //Cubicador cubicador = (Cubicador) gateway.retornarObjetoCoreOT(Cubicador.class, ot.getCubicador().getId());
                //Hibernate.initialize(cubicador);
                // Seteo Id de ot en cubicador
                //Ot otAux = new Ot();
                //otAux.setId(idOt);
                //cubicador.setOt(otAux);
                // Update en base dato
                //gateway.actualizarCubicador(cubicador);
                //guardarWorkflowEventosEjecucion(ot, gateway, usuarioId);
                gateway.commit();

            } catch (HibernateException e) {
                log.fatal("Error al crear OT. ", e);
                gateway.rollback();
                throw e;
            }
            return idOt;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    private void guardarWorkflowEventosEjecucion(Ot ot, Gateway gateway, int usuarioId) throws HibernateException, Exception {
        Contratos contratoOt = ot.getContrato();
        int idContrato = (Integer) contratoOt.getId();
        Contratos contrato = (Contratos) gateway.retornarObjetoCoreOT(Contratos.class, idContrato);
        Hibernate.initialize(contrato);
        
        Workflow workflowOt = contrato.getWorkflow();
        int idWorkflow = (Integer) workflowOt.getId();
        Workflow workflow = (Workflow) gateway.retornarObjetoCoreOT(Workflow.class, idWorkflow);
        Hibernate.initialize(workflow);
        
        Set<WorkflowEventos> listadoWorkflowEventos = workflow.getWorkflowEventos();
        
        if (listadoWorkflowEventos.size() > 0) {
            
            int ctd = 0;
            
            for (ObjetoCoreOT workflowkEvento : listadoWorkflowEventos) {
                WorkflowEventos we = (WorkflowEventos) workflowkEvento;
                WorkflowEventosEjecucion wee = new WorkflowEventosEjecucion();
                
                WorkflowEventosEjecucionId weeId = new WorkflowEventosEjecucionId();
                //weeId.setId(cubicador.getId());
//                weeId.setEventoId(we.getEvento().getId());
//                weeId.setRolId(we.getRol().getId());
//                weeId.setWorkflowId(we.getWorkflow().getId());
//                weeId.setDecisionId(we.getDecision().getId());
                
                wee.setId(weeId);
                
                wee.setOt(ot);
                
                wee.setProximo(we.getProximo());
                wee.setEventoPadre(we.getEventoPadre());
                Date fecha = new Date();
                if (ctd == 0) {
                    wee.setFechaInicioReal(fecha);
                    wee.setFechaTerminoReal(fecha);
                    wee.setEjecutado(1);
                    wee.setUsuarioEjecutorId(usuarioId);
                    
                }else{
                    wee.setEjecutado(1);
                }
                
                wee.setUsuarioId(usuarioId);
                
                wee.setFechaCreacion(fecha);
                
//                        wee.setUsuarioEjecutorId(usuarioId);
//                        wee.setUsuarioId(usuarioId);
                
                boolean registro = gateway.registrarWee(wee);
                
                if (registro && ctd == 1) {
                    //Enviar email a contratista
                    Usuarios usuarioEjecutor = (Usuarios) gateway.retornarObjetoCoreOT(Usuarios.class, usuarioId);
                    Hibernate.initialize(usuarioEjecutor);
                    Config config = Config.getINSTANCE();
                    String asunto = config.getAsuntoMailAsignacionOk();
                    String cuerpo = config.getCuerpoMailAsignacionOk() + "\n";
                    enviarMail(usuarioEjecutor, asunto, cuerpo);
                }
                ctd++;
            }
            
        }
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        Ot ot = (Ot) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            //setearMateriales(params, gateway, ot);
            gateway.iniciarTransaccion();
            try {
                gateway.actualizar2(obj);
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al actualizar Servicio. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    public boolean actualizarCubicador(Cubicador cub) throws Exception {

        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                gateway.actualizarCubicador(cub);
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al actualizar Servicio. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    @Override
    public List listar(ObjetoCoreOT obj, Map params) throws Exception {
        Ot ot = (Ot) obj;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = gate.listar(ot, join);
            for (ObjetoCoreOT objeto : listaRetorno) {
                Ot ser = (Ot) objeto;

                gate.validarYSetearObjetoARetornar(ser, join);
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    public List<Ot> retornarOtPendientes(int idUsuario) throws Exception {
        List<Ot> listadoOts = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
//            Usuarios usr = (Usuarios) gate.retornarObjetoCoreOT(Usuarios.class, idUsuario);
            List<WorkflowEventosEjecucion> listadoWkFlowEE = gate.retornarWrkFlwEvEjecPorUsuario(idUsuario);
            listadoOts = new ArrayList<>();
            for (WorkflowEventosEjecucion wee : listadoWkFlowEE) {
                gate.validarYSetearObjetoARetornar(wee.getOt(), null);
                listadoOts.add(wee.getOt());
            }
        } finally {
            gate.cerrarSesion();
        }
        return listadoOts;
    }

    public Boolean asignarResponsable(int eventoId, int otId, int usuarioEjecutorId) throws Exception {
        Gateway gateway = new Gateway();
        Boolean respuesta = false;
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            WorkflowEventosEjecucion wee = (WorkflowEventosEjecucion) gateway.obtenerWorkflowEventosEjecucionPorEventoYOt(eventoId, otId);

            wee.setUsuarioEjecutorId(usuarioEjecutorId);
            wee.setEjecutado(1);
            gateway.commit();

            List<ObjetoCoreOT> usuarios = gateway.listarConFiltro(Usuarios.class, "id", wee.getUsuarioId());

            List<ObjetoCoreOT> usuariosEjecutor = gateway.listarConFiltro(Usuarios.class, "id", wee.getUsuarioEjecutorId());

            Config config = Config.getINSTANCE();

            String asunto = config.getAsuntoMailAsignacionOk();
            String cuerpo = config.getCuerpoMailAsignacionOk() + "\n";
            enviarMail((Usuarios) usuarios.get(0), asunto, cuerpo);

            asunto = config.getAsuntoMailAsignacionEjecutor();
            cuerpo = config.getCuerpoMailAsignacionEjecutor() + "\n";
            enviarMail((Usuarios) usuariosEjecutor.get(0), asunto, cuerpo);

            try {
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al asignar el responsable. ", e);
                gateway.rollback();
                throw e;
            }
            respuesta = true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
        return respuesta;
    }
    
    public boolean finalizaTrabajo(int eventoId, int otId, int usuarioEjecutorId) throws Exception {
        Gateway gateway = new Gateway();
        Boolean respuesta = false;
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            WorkflowEventosEjecucion wee = (WorkflowEventosEjecucion) gateway.obtenerWorkflowEventosEjecucionNNN(eventoId, otId, usuarioEjecutorId);
            int idUsuarioGestor = wee.getOt().getGestor().getId();
            wee.setUsuarioEjecutorId(usuarioEjecutorId);
            wee.setEjecutado(1);
            gateway.commit();

            List<ObjetoCoreOT> usuarios = gateway.listarConFiltro(Usuarios.class, "id", idUsuarioGestor);
            Usuarios usr = (Usuarios)usuarios.get(0);
            gateway.validarYSetearObjetoARetornar(usr, null);

            String asunto = "*** Finalizacion de Trabajos ***";
            String cuerpo = "Te comunicamos que han finalizado las actividades correspondientes a la OT " + otId;
            enviarMail(usr, asunto, cuerpo);

            try {
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al asignar el responsable. ", e);
                gateway.rollback();
                throw e;
            }
            respuesta = true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
        return respuesta;
    }

    public Ot obtenerDetalleOt(Integer otId) throws Exception {
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            Ot ot = (Ot) gateway.obtenerDetalleOt2(otId);
            gateway.validarYSetearObjetoARetornar(ot, null);
            return ot;
        } catch (Exception e) {
            log.fatal(e);
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    public OtAux obtenerDetalleOtAux(Integer otId, Login login) throws Exception {
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            Usuarios usr = (Usuarios)gateway.retornarObjetoCoreOT(Usuarios.class, login.getUsuarios().getId());
            Set<Roles> roles = usr.getUsuariosRoles();
            verificaUsuarioEvento(otId, login.getUsuarios().getId());
            OtAux ot = (OtAux) gateway.obtenerDetalleOtAux(otId, login);
//            Cubicador cubi = ()ot.getDetalleOt().get("cubicacion");
//            gateway.validarYSetearObjetoARetornar(ot,null);
            return ot;
        } catch (Exception e) {
            log.fatal(e);
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    public Ot obtenerDetalleOtLLave(Integer otId) throws Exception {
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            Ot ot = gateway.obtenerDetalleOtLLave(otId);
            return ot;
        } catch (Exception e) {
            log.fatal(e);
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }


    public OtAux obtenerDetalleOtCompleto(Integer otId) throws Exception {
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            OtAux ot = (OtAux) gateway.obtenerDetalleOtCompleto(otId);
            return ot;
        } catch (Exception e) {
            log.fatal(e);
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    private boolean verificaUsuarioEvento (int otId, int usuarioId) throws Exception{
        boolean respuesta = false;
        List<UsuariosOt> lista = GenericDAO.getINSTANCE().obtenerUsuariosOtId(otId);

        for(UsuariosOt usuariosOt : lista){
            if(usuariosOt.getUsuarios().getId() == usuarioId){
                respuesta = true;
            }
        }
        if(respuesta){
            return  respuesta;
        } else {
            List<WorkflowEventosEjecucion> listaWe = GenericDAO.getINSTANCE().obtenerUsuariosOtIdWe(otId);
            for(WorkflowEventosEjecucion usuariosOtWe : listaWe){
                if(usuariosOtWe.getUsuarioId() == usuarioId){
                    respuesta = true;
                }
            }
            if(respuesta){
                return respuesta;
            } else {
                throw new Exception("Error, Usuario ejecutor no esta asociado con la OT seleccionada");
            }
        }
    }
    public List<OtAux> listarOtEjecucionPorUsuarioId(int usuarioEjecutorId) throws Exception {
        List<OtAux> listadoOtsEjecucion = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();

            List<OtAux> listadoWEE = gate.listarWorkflowEventosEjecucionPorUsuarioId(usuarioEjecutorId);
            listadoOtsEjecucion = new ArrayList<>();
            for (OtAux otAux : listadoWEE) {
                OtAux ot = new OtAux();
                ot = gate.obtenerDetalleOt(otAux);

                listadoOtsEjecucion.add(ot);
            }

        } finally {
            gate.cerrarSesion();
        }
        return listadoOtsEjecucion;
    }

    public List<String> obtenerAccionesOt(int otId, int usuarioId) throws Exception {
        List<String> listadoAccionesOt = null;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            verificaUsuarioEvento(otId, usuarioId);
            listadoAccionesOt = gate.obtenerAccionesOt(otId);
        } finally {
            gate.cerrarSesion();
        }
        return listadoAccionesOt;
    }

    public int obtenerTotalOtPorEstado(int usuarioEjecutorId, String estado, Map<String,String> listadoFiltros) throws Exception {
        int total = 0;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            total = gate.obtenerTotalOtPorEstado(usuarioEjecutorId, estado, listadoFiltros);
        } finally {
            gate.cerrarSesion();
        }
        return total;
    }

    public List<OtAux> listarOtPorEstado(int usuarioEjecutorId, int pos, int cantidad, String estado, String filtro, String orden, Map<String,String> listadoFiltros) throws Exception {
        List<Ot> listadoOtAux = new ArrayList<>();
        List<OtAux> listadoOtAbiertas = new ArrayList<>();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            listadoOtAux = gate.listarOtPorEstado(usuarioEjecutorId, pos, cantidad, estado, filtro, orden, listadoFiltros);
            for (Ot otAux : listadoOtAux) {
                OtAux ot = new OtAux();
                ot.setId(otAux.getId());
                ot = gate.obtenerDetalleOt(ot);
                if(estado.equalsIgnoreCase("play")){
                    ot.setEjecutar(true);
                }
                // el editar va en true cuando esta en informar avance, de esta manera se puede cambiar al jefe de obras
                ot.setEditar(gate.validaEditar(ot.getId(), usuarioEjecutorId));
                if(gate.validaRolCalidad(usuarioEjecutorId)){
                    Map calidadMap = new HashMap();

                    Map formularios = new HashMap();
                    List listadoCalidad = new ArrayList();
                    listadoCalidad = obtieneFormularioCalidad(otAux.getId(), usuarioEjecutorId, "OOEE");
                    formularios.put("tipoFormulario", "OOEE");
                    formularios.put("otId", otAux.getId());
                    formularios.put("formularios", listadoCalidad);
                    calidadMap.put("OOEE", formularios);

                    formularios = new HashMap();
                    listadoCalidad = new ArrayList();
                    listadoCalidad = obtieneFormularioCalidad(otAux.getId(), usuarioEjecutorId, "OOCC");
                    formularios.put("tipoFormulario", "OOCC");
                    formularios.put("otId", otAux.getId());
                    formularios.put("formularios", listadoCalidad);
                    calidadMap.put("OOCC", formularios);

                    ot.setCalidad(calidadMap);

                }
                listadoOtAbiertas.add(ot);
            }
        } finally {
            gate.cerrarSesion();
        }
        return listadoOtAbiertas;
    }

    public List<OtAux> listarOtPorId(int usuarioEjecutorId, int otId, String estado) throws Exception {
        List<Ot> listadoOtAux = new ArrayList<>();
        List<OtAux> listadoOtPorId = new ArrayList<>();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            listadoOtAux = gate.listarOtPorId(usuarioEjecutorId, otId, estado);
            for (Ot otAux : listadoOtAux) {
                OtAux ot = new OtAux();
                ot.setId(otAux.getId());
                if(estado.equalsIgnoreCase("play")){
                    ot.setEjecutar(true);
                }
                ot = gate.obtenerDetalleOt(ot);
                //ot.setEditar(gate.validaEditar(ot.getId(), usuarioEjecutorId));

                listadoOtPorId.add(ot);
            }
        } finally {
            gate.cerrarSesion();
        }
        return listadoOtPorId;
    }

    public List<OtAux> listarOtPorNombre(int usuarioEjecutorId, String otNombre, String estado) throws Exception {
        List<Ot> listadoOtAux = new ArrayList<>();
        List<OtAux> listadoOtPorId = new ArrayList<>();
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            listadoOtAux = gate.listarOtPorNombre(usuarioEjecutorId, otNombre, estado);
            for (Ot otAux : listadoOtAux) {
                OtAux ot = new OtAux();
                ot.setId(otAux.getId());
                ot = gate.obtenerDetalleOt(ot);
                if(estado.equalsIgnoreCase("play")){
                    ot.setEjecutar(true);
                }
                //ot.setEditar(gate.validaEditar(ot.getId(), usuarioEjecutorId));

                listadoOtPorId.add(ot);
            }
        } finally {
            gate.cerrarSesion();
        }
        return listadoOtPorId;
    }



    private void enviarMail(Usuarios usuario, String asunto, String cuerpo) throws Exception {
        try {
            String mailDestino = usuario.getEmail();
            SendMail sendMail = SendMail.getINSTANCE();
            boolean envioOK = sendMail.enviarMail(mailDestino, asunto, cuerpo, null);
            if (!envioOK) {
                throw new Exception("Se produjo un error al enviar email.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean isTramiteEnProcesoOt(int otId) throws Exception {
        boolean valida = true;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            if(gate.obtenerTotalTramitesEnProcesoOt(otId) == 0){
                valida = false;
            }
        } finally {
            gate.cerrarSesion();
        }
        return valida;
    }
    
    public boolean agregarTramite(int otId, int tipoTramiteId, String tramite, int usuarioId) throws Exception {

        boolean respuesta = false;
        int estadoTramiteIngresado = 1;

        this.verificaUsuarioEvento(otId, usuarioId);

        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            
            Ot ot = (Ot) gateway.retornarObjetoCoreOT(Ot.class, otId);
            Hibernate.initialize(ot);
            
            TipoTramite tipoTramite = (TipoTramite) gateway.retornarObjetoCoreOT(TipoTramite.class, tipoTramiteId);
            Hibernate.initialize(tipoTramite);

            EstadoTramite estadoTramite = (EstadoTramite) gateway.retornarObjetoCoreOT(EstadoTramite.class, estadoTramiteIngresado);
            Hibernate.initialize(estadoTramite);
                
            gateway.iniciarTransaccion();
            
            try {
                
                OtTramites otTramite = new OtTramites();
                otTramite.setOt(ot);
                otTramite.setTipoTramite(tipoTramite);
                otTramite.setTramite(tramite);
                
                Date fechaInicioTramite = new Date();
                otTramite.setFechaInicioTramite(fechaInicioTramite);
                otTramite.setFechaTerminoTramite(null);
                otTramite.setTerminado(false);
                otTramite.setEstadoTramite(estadoTramite);
                
                //Este método registar en la tabla ot_tramites.
                respuesta = gateway.registrarOtTramite(otTramite);
                
                if (respuesta = true)
                {
                    int solicitudmensaje = 19;
                    ot.setTieneTramites(true);

                    MensajeEvento mensajeEvento = (MensajeEvento) gateway.retornarObjetoCoreOT(MensajeEvento.class, solicitudmensaje);
                    Hibernate.initialize(mensajeEvento);

                    String mensajeNotificacion = mensajeEvento.getMensajeNotificacion() + "'"+tipoTramite.getDescripcion()+"' para la orden de trabajo "+ otId;

                    enviarNotificacionTramite(mensajeNotificacion, ot, usuarioId);
                }
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al asociar el tramite. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    public boolean apruebaTramite(int tramiteId, int usuarioId) throws Exception {

        boolean respuesta = false;
        int estadoOtDetenido = 5;
        int estadoTramiteAprobado = 2;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();

            OtTramites otTramites = (OtTramites) gateway.retornarObjetoCoreOT(OtTramites.class, tramiteId);
            Hibernate.initialize(otTramites);

            this.verificaUsuarioEvento(otTramites.getOt().getId(), usuarioId);

            EstadoTramite estadoTramite = (EstadoTramite) gateway.retornarObjetoCoreOT(EstadoTramite.class, estadoTramiteAprobado);
            Hibernate.initialize(estadoTramite);

            Ot ot = (Ot) gateway.retornarObjetoCoreOT(Ot.class, otTramites.getOt().getId());
            Hibernate.initialize(ot);

            if(ot.getGestor().getId() != usuarioId )
                throw new Exception("Error, Usuario no autorizado para aprobar tramite");

            TipoEstadoOt tipoEstadoOt = (TipoEstadoOt) gateway.retornarObjetoCoreOT(TipoEstadoOt.class, estadoOtDetenido);
            Hibernate.initialize(tipoEstadoOt);

            gateway.iniciarTransaccion();

            try {
                respuesta = true;
                int apruebamensaje = 20;

                otTramites.setEstadoTramite(estadoTramite);
                ot.setTipoEstadoOt(tipoEstadoOt);
                ot.setTieneTramites(false);

                MensajeEvento mensajeEvento = (MensajeEvento) gateway.retornarObjetoCoreOT(MensajeEvento.class, apruebamensaje);
                Hibernate.initialize(mensajeEvento);

                String mensajeNotificacion = mensajeEvento.getMensajeNotificacion() + "'"+otTramites.getTipoTramite().getDescripcion()+"' para la orden de trabajo "+ otTramites.getOt().getId();

                enviarNotificacionTramite(mensajeNotificacion, ot, usuarioId);

                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al aprobar el tramite. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    public boolean rechazaTramite(int tramiteId, int usuarioId) throws Exception {

        boolean respuesta = false;
        int estadoTramiteRechazado = 3;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();

            OtTramites otTramites = (OtTramites) gateway.retornarObjetoCoreOT(OtTramites.class, tramiteId);
            Hibernate.initialize(otTramites);

            this.verificaUsuarioEvento(otTramites.getOt().getId(), usuarioId);

            Ot ot = (Ot) gateway.retornarObjetoCoreOT(Ot.class, otTramites.getOt().getId());
            Hibernate.initialize(ot);

            if(ot.getGestor().getId() != usuarioId )
                throw new Exception("Error, Usuario no autorizado para rechazar tramite");

            EstadoTramite estadoTramite = (EstadoTramite) gateway.retornarObjetoCoreOT(EstadoTramite.class, estadoTramiteRechazado);
            Hibernate.initialize(estadoTramite);

            gateway.iniciarTransaccion();

            try {

                respuesta = true;
                Date fechaTerminoTramite = new Date();
                int rechazomensaje = 24;

                otTramites.setFechaTerminoTramite(fechaTerminoTramite);
                otTramites.setTerminado(true);
                otTramites.setEstadoTramite(estadoTramite);
                ot.setTieneTramites(false);

                MensajeEvento mensajeEvento = (MensajeEvento) gateway.retornarObjetoCoreOT(MensajeEvento.class, rechazomensaje);
                Hibernate.initialize(mensajeEvento);

                String mensajeNotificacion = mensajeEvento.getMensajeNotificacion() + "'"+otTramites.getTipoTramite().getDescripcion()+"' para la orden de trabajo "+ otTramites.getOt().getId();

                enviarNotificacionTramite(mensajeNotificacion, ot, usuarioId);

                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al rechazar el tramite. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    public boolean finalizarTramite(int tramiteId, int usuarioId) throws Exception {

        boolean respuesta = false;
        int estadoTramiteConcluido = 4;
        int estadoTramiteAprobado = 2;
        int estadoOtAbierta = 1;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();

            EstadoTramite estadoTramite = (EstadoTramite) gateway.retornarObjetoCoreOT(EstadoTramite.class, estadoTramiteConcluido);
            Hibernate.initialize(estadoTramite);

            OtTramites otTramite = (OtTramites) gateway.retornarObjetoCoreOT(OtTramites.class, tramiteId);
            Hibernate.initialize(tramiteId);

            this.verificaUsuarioEvento(otTramite.getOt().getId(), usuarioId);

            Ot ot = (Ot) gateway.retornarObjetoCoreOT(Ot.class, otTramite.getOt().getId());

            TipoEstadoOt tipoEstadoOt = (TipoEstadoOt) gateway.retornarObjetoCoreOT(TipoEstadoOt.class, estadoOtAbierta);
            Hibernate.initialize(tipoEstadoOt);
            
            gateway.iniciarTransaccion();
            
            try {

                if(otTramite.getEstadoTramite().getId() == estadoTramiteAprobado){
                    Date fechaTerminoTramite = new Date();
                    otTramite.setFechaTerminoTramite(fechaTerminoTramite);
                    otTramite.setTerminado(true);
                    otTramite.setEstadoTramite(estadoTramite);

                    respuesta = gateway.actualizarOtTramite(otTramite);

                    if (respuesta = true)
                    {
                        ot.setTieneTramites(false);
                        ot.setTipoEstadoOt(tipoEstadoOt);

                        int finalizadomensaje = 25;

                        MensajeEvento mensajeEvento = (MensajeEvento) gateway.retornarObjetoCoreOT(MensajeEvento.class, finalizadomensaje);
                        Hibernate.initialize(mensajeEvento);

                        String mensajeNotificacion = mensajeEvento.getMensajeNotificacion() + "'"+otTramite.getTipoTramite().getDescripcion()+"' para la orden de trabajo "+ otTramite.getOt().getId();

                        enviarNotificacionTramite(mensajeNotificacion, ot, usuarioId);
                    }

                }else throw new Exception("Solo se puede concluir un tramite que este aprobado.");

                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al finalizar el tramite. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }
    
        public List<OtTramites> listarTramitesPorOt(int otId, int usuarioId) throws Exception {
        Gateway gate = new Gateway();
        List<OtTramites> listado = new ArrayList<>();
        this.verificaUsuarioEvento(otId, usuarioId);
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            listado = gate.listarTramitesPorOt2(otId);
        } finally {
            gate.cerrarSesion();
        }

        return listado;
    }

    public List<OtTramites> listarTramitesIngresadosYAprobadosPorOt(int otId, int usuarioId) throws Exception {
        Gateway gate = new Gateway();
        List<OtTramites> listadoTramitesIngresados = new ArrayList<>();
        List<OtTramites> listadoTramitesAprobados = new ArrayList<>();
        int estadoTramiteIngresado = 1;
        //int estadoTramiteAprobado = 2;
        this.verificaUsuarioEvento(otId, usuarioId);

        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            listadoTramitesIngresados = gate.listarTramitesPorOtyEstado(otId, estadoTramiteIngresado);
            //listadoTramitesAprobados = gate.listarTramitesPorOtyEstado(otId, estadoTramiteAprobado);
            //listadoTramitesIngresados.addAll(listadoTramitesAprobados);
        } finally {
            gate.cerrarSesion();
        }

        return listadoTramitesIngresados;
    }

    public List<TipoTramite> listarTipoTramite() throws Exception {
        Gateway gate = new Gateway();
        List<TipoTramite> listado = new ArrayList<>();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            listado = gate.listarTipoTramite();
        } finally {
            gate.cerrarSesion();
        }

        return listado;
    }

    public boolean enviarNotificacionTramite(String mensajeNotificacion, Ot ot, int idEmisor) throws Exception {

        boolean respuesta = false;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();

            gateway.iniciarTransaccion();

            try {

                Date fechaTerminoTramite = new Date();
                int eventoInformarAvance = 3;
                List<Integer> usuarioRolIdNotificacion = new ArrayList();
                List <Usuarios> usuariosMail = new ArrayList<Usuarios>();
                String asunto = "Detencion Ot "+ot.getId();

                usuarioRolIdNotificacion.add(2); //Gestor
                usuarioRolIdNotificacion.add(4); //Trabajador EC
                usuarioRolIdNotificacion.add(7); //Coordinador EC
                usuarioRolIdNotificacion.add(3);  //Administrador de Contrato EC

                for(int usuarioRolId: usuarioRolIdNotificacion){
                    List<Usuarios> usuariosNotificacion = new ArrayList<Usuarios>();
                    usuariosNotificacion = gateway.obtieneUsuariosOtRolSinEmisor(ot.getId(), usuarioRolId, idEmisor);
                    for(Usuarios usuario: usuariosNotificacion){
                        usuariosMail.add(usuario);
                        gateway.registrarConId(new Notificaciones(0,ot,eventoInformarAvance,usuario.getId(),mensajeNotificacion,new Date(),usuario.getEmail(),false),"Notificacion");
                    }
                }
                gateway.commit();

                for(Usuarios usuarioMail: usuariosMail){
                    //enviarMail(usuarioMail, asunto, mensajeNotificacion);
                }
                respuesta = true;
            } catch (HibernateException e) {
                log.fatal("Error al enviar notificacion del tramite. ", e);
                gateway.rollback();
                throw e;
            }
            return respuesta;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }
    public List obtieneObservacionesGerencia(Login login, int otId) throws Exception {
        Gateway gate = new Gateway();
        List<TipoTramite> listado = new ArrayList<>();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            listado = gate.obtieneObservacionesGerencia(login, otId);
        } finally {
            gate.cerrarSesion();
        }

        return listado;
    }

    public String obtenerXmlOt(int otId) throws Exception {
        Gateway gate = new Gateway();
        String xmlSalida = "";
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            xmlSalida = gate.obtenerXmlOt(otId);
        } finally {
            gate.cerrarSesion();
        }

        return xmlSalida;
    }

    public String obtenerXmlActa(int actaId) throws Exception {
        Gateway gate = new Gateway();
        String xmlSalida = "";
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            xmlSalida = gate.obtenerXmlActa(actaId);
        } finally {
            gate.cerrarSesion();
        }

        return xmlSalida;
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static final Logger log = Logger.getLogger(OtDAO.class);
    private static final OtDAO INSTANCE = new OtDAO();

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            Map<String,Object> filtro = new HashMap<>();
            filtro.put("id",idObjetoCoreOT);
            Ot ot = (Ot)gate.obtenerRegistroConFiltro(Ot.class,null,filtro);
            List<String> join = new ArrayList<>();
            gate.validarYSetearObjetoARetornar(ot, join);
            return ot;
        } finally {
            gate.cerrarSesion();
        }
    }


    public List obtieneFormularioCalidad(int otId, int usuarioId, String tipoFormulario) throws Exception{
        Gateway gate = new Gateway();
        List formularioCalidad = new ArrayList();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            formularioCalidad = gate.obtieneFormularioCalidadGateways(otId, usuarioId, tipoFormulario);
            return formularioCalidad;
        } finally {
            gate.cerrarSesion();
        }
    }

    public boolean registraFormularioSeguimientoCalidad(int otId, int usuarioId, HashMap<String, Object> mapaParametros, List<ArchivoAux> listadoArchivo, LibroObras libroObras)throws Exception {
        Gateway gate = new Gateway();
        boolean respuesta = false;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            respuesta = gate.registraFormularioSeguimientoCalidadGateways(otId, usuarioId, mapaParametros, listadoArchivo, libroObras);
            if(respuesta == true){
                gate.commit();
            }
            return respuesta;
        } catch (Exception e){
            throw e;
        }
        finally {
            gate.cerrarSesion();
        }
    }
}
