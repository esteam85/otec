package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.DetalleParametros;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import com.skydream.coreot.pojos.Parametros;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

/**
 *
 * @author mcj
 */
public class ParametroDAO implements GatewayDAO {

    private ParametroDAO() {
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        boolean retornoOK = false;
        Parametros parametro = (Parametros) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearDetallesParametro(params, parametro);
            if (parametro.getDetalleParametros().isEmpty()) {
                throw new Exception("Lista de Detalle Parametros no enviada.");
            }
            gateway.iniciarTransaccion();
            try {
                gateway.registrar(parametro);
                for (DetalleParametros detalle : parametro.getDetalleParametros()) {
                    gateway.registrar(detalle);
                }

                gateway.commit();
            } catch (HibernateException ex) {
                log.fatal("Error al crear Parametro. ", ex);
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return retornoOK;
    }

    private void setearDetallesParametro(Map params, Parametros parametro) throws Exception {
        if (params.containsKey("detalleParametros")) {
            List<Map> listadoDetallesParametro = (ArrayList<Map>) params.get("detalleParametros");
            for (Map detalleParam : listadoDetallesParametro) {
                String llave = (String) detalleParam.get("llave");
                String valor = (String) detalleParam.get("valor");
                boolean isTemplateOption = (boolean) detalleParam.get("isTemplateOption");
                DetalleParametros detalleParametro = new DetalleParametros(0, parametro, llave, valor, isTemplateOption);
                parametro.getDetalleParametros().add(detalleParametro);
            }
        }
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        boolean retornoOK = false;
        Parametros parametro = (Parametros) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearDetallesParametro(params, parametro);
            gateway.iniciarTransaccion();
            try {
                gateway.actualizar2(parametro);
                for (DetalleParametros detalle : parametro.getDetalleParametros()) {
                    gateway.actualizar2(detalle);
                }
                gateway.commit();
            } catch (HibernateException ex) {
                log.fatal("Error al actualizar Parametro. ", ex);
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return retornoOK;
    }

    @Override
    public List listar(ObjetoCoreOT obj, Map params) throws Exception {
        Parametros parametro = (Parametros) obj;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = (List<ObjetoCoreOT>) gate.listar(parametro, join);
            join.add("detalleParametros");
            for (ObjetoCoreOT objeto : listaRetorno) {
                Parametros parametroAux = (Parametros) objeto;
                gate.validarYSetearObjetoARetornar(parametroAux, join);
                Set<DetalleParametros> listadoDetallesParametro = parametroAux.getDetalleParametros();
                for (DetalleParametros detalleParametro : listadoDetallesParametro) {
                    detalleParametro.setParametro(null);
                }
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static ParametroDAO getINSTANCE() {
        return INSTANCE;
    }

    private static final ParametroDAO INSTANCE = new ParametroDAO();
    private static final Logger log = Logger.getLogger(ProveedorDAO.class);

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
