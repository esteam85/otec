/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.ObjetoCoreOT;
import com.skydream.coreot.pojos.Perfiles;
import com.skydream.coreot.pojos.Proveedores;
import com.skydream.coreot.pojos.TipoProveedor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

/**
 *
 * @author mcj
 */
public class PerfilDAO implements GatewayDAO {

    private PerfilDAO() {
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        boolean retornoOK = false;
        Perfiles perfil = (Perfiles) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            try {
                gateway.iniciarTransaccion();
                gateway.registrar(perfil);
                gateway.commit();
            } catch (HibernateException ex) {
                log.error("Error al crear Perfil. ", ex);
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return retornoOK;
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        boolean retornoOK = false;
        Perfiles perfil = (Perfiles) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            try {
                gateway.iniciarTransaccion();
                gateway.actualizar2(perfil);
                gateway.commit();
            } catch (HibernateException ex) {
                log.error("Error al actualizar Perfil. ", ex);
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return retornoOK;
    }

    @Override
    public List listar(ObjetoCoreOT obj, Map params) throws Exception {
        Perfiles perfil = (Perfiles) obj;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = gate.listar(perfil, join);
            for (ObjetoCoreOT objeto : listaRetorno) {
                Perfiles perf = (Perfiles) objeto;
                if (!perf.getProveedores().isEmpty()) {
                    join.add("proveedores");
                }
                gate.validarYSetearObjetoARetornar(perf, join);
                if (perf.getProveedores() != null) {
                    Set<Proveedores> listadoProveedores = perf.getProveedores();
                    for (Proveedores prv : listadoProveedores) {
                        TipoProveedor tp = prv.getTipoProveedor();
                        TipoProveedor tipoProv = new TipoProveedor(tp.getId(), tp.getNombre(), tp.getDescripcion(),
                                tp.getEstado(), null);
                        prv.setTipoProveedor(tipoProv);
                    }
                }
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static PerfilDAO getINSTANCE() {
        return INSTANCE;
    }

    private static final PerfilDAO INSTANCE = new PerfilDAO();

    private static final Logger log = Logger.getLogger(PerfilDAO.class);

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
