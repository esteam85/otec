/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.*;

import java.util.*;
import java.util.regex.Pattern;

import com.skydream.coreot.util.SendMail;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

/**
 *
 * @author mcj
 */
public class ProveedorDAO implements GatewayDAO {

    private ProveedorDAO() {
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        boolean retornoOK = false;
        Proveedores proveedor = (Proveedores) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearServicios(params, gateway, proveedor);
            setearPerfiles(params, gateway, proveedor);
            gateway.iniciarTransaccion();
            try {
                gateway.registrar(proveedor);
                gateway.commit();
            } catch (HibernateException ex) {
                log.fatal("Error al crear Proveedor. ", ex);
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return retornoOK;
    }

    private void setearServicios(Map params, Gateway gateway, Proveedores proveedor) throws Exception {
        if (params.containsKey("proveedoresServicios")) {
            List<Map> listadoServicios = (ArrayList<Map>) params.get("proveedoresServicios");
            for (Map serv : listadoServicios) {
                Integer idServ = (Integer) serv.get("id");
                Servicios srv = (Servicios) gateway.obtenerDatoPorId(new Servicios(idServ), null);
                proveedor.getProveedoresServicios().add(srv);
            }
        }
    }

    private void setearPerfiles(Map params, Gateway gateway, Proveedores proveedor) throws Exception {
        if (params.containsKey("proveedorPerfiles")) {
            List<Map> listadoPerfiles = (ArrayList<Map>) params.get("proveedorPerfiles");
            for (Map prov : listadoPerfiles) {
                Integer idProv = (Integer) prov.get("id");
                Perfiles perf = (Perfiles) gateway.retornarObjetoCoreOT(Perfiles.class, idProv);
                proveedor.getProveedorPerfiles().add(perf);
            }
        }
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        boolean retornoOK = false;
        Proveedores proveedor = (Proveedores) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearServicios(params, gateway, proveedor);
            setearPerfiles(params, gateway, proveedor);
            gateway.iniciarTransaccion();
            try {
                gateway.actualizar2(obj);
                gateway.commit();
            } catch (HibernateException ex) {
                log.fatal("Error al actualizar Proveedor. ", ex);
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return retornoOK;
    }

    @Override
    public List listar(ObjetoCoreOT obj, Map params) throws Exception {
        Proveedores proveedor = (Proveedores) obj;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = gate.listar(proveedor, join);
//            join.add("proveedoresServicios");
            join.add("proveedorPerfiles");
            for (ObjetoCoreOT objeto : listaRetorno) {
                Proveedores prov = (Proveedores) objeto;
                gate.validarYSetearObjetoARetornar(prov, join);
//                Set<ProveedoresServicios> listadoProveedoresServicios = prov.getProveedoresServicios();
//                for (ProveedoresServicios item : listadoProveedoresServicios) {
//                    Servicios servicio = item.getServicios();
////                    TipoServicio tipoServ = new TipoServicio(servicio.getTipoServicio().getId(),
////                            servicio.getNombre(), servicio.getDescripcion(), servicio.getEstado(), null);
////                    servicio.setTipoServicio(tipoServ);
////                    TipoUnidadMedida tipoUnidadMed = new TipoUnidadMedida(servicio.getTipoUnidadMedida().getId(),
////                            servicio.getCodigo(), servicio.getNombre(), servicio.getEstado(), null, null, null);
////                    servicio.setTipoUnidadMedida(tipoUnidadMed);
//                }
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static ProveedorDAO getInstance() {
        return INSTANCE;
    }

    private static final ProveedorDAO INSTANCE = new ProveedorDAO();
    private static final Logger log = Logger.getLogger(ProveedorDAO.class);

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int crearProveedorDesdeCubicacion(Proveedores proveedor, ProveedoresCubicacion proveedoresCubicacion) {
        Gateway gateway = new Gateway();
        int idProveedor = 0;
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                idProveedor = gateway.registrar(proveedor);
                proveedor.setId(idProveedor);

                // luego creo los usuarios
                Usuarios usuario = new Usuarios();
                Areas areaAux = new Areas();
                areaAux.setId(4);
                usuario.setArea(areaAux);
                usuario.setProveedor(proveedor);
                Repositorios repositorios = new Repositorios();
                repositorios.setId(1);
                usuario.setRepositorio(repositorios);
                String[] nombreUsuario =  proveedoresCubicacion.getEmail().split("@");
                usuario.setNombreUsuario(nombreUsuario[0] + "-ec");
                usuario.setClave("10000:8c391fceffd9275b07cd511f5481f2e03991a38c1b6a9ab1:15710daca6ebd65a45412451431770706612dfd70f5cac71");
                usuario.setRut(proveedoresCubicacion.getRut());
                usuario.setDv(proveedoresCubicacion.getDv());
                usuario.setNombres(nombreUsuario[0]);
                String[] apellido = nombreUsuario[1].split(Pattern.quote("."));
                usuario.setApellidos(apellido[0]);
                usuario.setEmail(proveedoresCubicacion.getEmail());
                usuario.setCelular(proveedoresCubicacion.getTelefono());
                char estado = 'A';
                usuario.setEstado(estado);

                Roles rol = (Roles) gateway.retornarObjetoCoreOT(Roles.class, 3);

                Set<Roles> roles = new HashSet<>();
                roles.add(rol);

                usuario.setUsuariosRoles(roles);

                gateway.registrar(usuario);

                String asunto = "Bienvenido al Portal Ot de Telefonica";
                String cuerpo = "Su cuenta para ingresar la portal es:" + "\n" +
                        "usuario: " + usuario.getNombreUsuario() + "\n" +
                        "clave: " + "admin";
                SendMail.getINSTANCE().enviarMail(usuario.getEmail(),asunto,cuerpo,null);

                gateway.commit();
            } catch (HibernateException ex) {
                log.fatal("Error al crear Proveedor. ", ex);
                gateway.rollback();
                throw ex;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            gateway.cerrarSesion();
        }
        return idProveedor;
    }

    public Long obtenerCodigoAcuerdoPorProveedor(int contratoID, int proveedorID)throws Exception{
        Gateway gate = new Gateway();
        try{
            gate.abrirSesion();
            return gate.obtenerCodigoAcuerdoPorProveedor(contratoID,proveedorID);
        }catch(Exception ex){
            gate.cerrarSesion();
            throw ex;
        }

    }


}
