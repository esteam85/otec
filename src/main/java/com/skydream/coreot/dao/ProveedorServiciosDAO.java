package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Created by mcj on 16-11-15.
 */
public class ProveedorServiciosDAO implements GatewayDAO{

    private ProveedorServiciosDAO() {
    }

    public static ProveedorServiciosDAO getInstance() {
        return INSTANCE;
    }

    private static final ProveedorServiciosDAO INSTANCE = new ProveedorServiciosDAO();


    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        return false;
    }

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        return 0;
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        return false;
    }

    @Override
    public List listar(ObjetoCoreOT obj, Map params) throws Exception {
        return null;
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        return null;
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        return false;
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        return null;
    }

    public List listarServiciosPorProveedorRegionContrato(int contratoId, int regionId, int proveedorId, int tipoServicioId)throws Exception{
        Gateway gateway = new Gateway();
        List<ProveedoresServicios> listadoRetorno = null;
        try {
            gateway.abrirSesion();
            listadoRetorno = gateway.listarServiciosPorProveedorRegionContrato(contratoId,regionId,proveedorId, tipoServicioId);
            List<String> join = new ArrayList<>();
            join.add("servicios");
            for(ProveedoresServicios item : listadoRetorno){
                gateway.validarYSetearObjetoARetornar(item,join);
                item.setContratos(null);
                item.setProveedores(null);
                item.setRegiones(null);
            }

            return listadoRetorno;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    public List listarServiciosExtrasPorOt(int otId, int tipoServicioId, Login loginUsuario)throws Exception{
        Gateway gateway = new Gateway();
        List<ServiciosAux> listadoRetorno = new ArrayList<>();
        try {
            gateway.abrirSesion();

            OtAux ot = gateway.obtenerDetalleOtAux(otId,loginUsuario);
            int contratoId = ot.getContrato().getId();
            int proveedorId = ot.getProveedor().getId();

            List<Cubicador> cubicador = gateway.obtenerCubicacionPorIdOt(otId);
            int regionId = cubicador.get(0).getRegion().getId();

            listadoRetorno = gateway.listarServiciosPorProveedorRegionContrato2(contratoId,regionId,proveedorId, tipoServicioId, cubicador.get(0));

            return listadoRetorno;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    public List listarServiciosPorProveedorRegionContrato2(int contratoId, int regionId, int proveedorId, int tipoServicioId)throws Exception{
        Gateway gateway = new Gateway();
        List<ServiciosAux> listadoRetorno = new ArrayList<>();
        try {
            gateway.abrirSesion();
            listadoRetorno = gateway.listarServiciosPorProveedorRegionContrato2(contratoId,regionId,proveedorId, tipoServicioId, null);

            return listadoRetorno;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }
    public LinkedHashMap listarServicioPorProveedorRegionContratoServicioId(int contratoId, int regionId, int agenciaId, int centralId, int proveedorId, int tipoServicioId, int servicioId, double cantidad)throws Exception{
        Gateway gateway = new Gateway();
        LinkedHashMap serviciosAux = new LinkedHashMap();
        try {
            gateway.abrirSesion();
            serviciosAux = gateway.listarServicioPorProveedorRegionContratoServicioId(contratoId,regionId, agenciaId, centralId, proveedorId, tipoServicioId, servicioId, cantidad);
            if(serviciosAux.size() > 0){
                return serviciosAux;
            } else {
                return null;
            }

        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }
    public List<Servicios> listarServiciosPorEspecialidad(int especialidadId, int contratoId, int agenciaId)throws Exception{
        Gateway gateway = new Gateway();
        List<Servicios> listadoRetorno = new ArrayList<>();
        try {
            gateway.abrirSesion();
            listadoRetorno = gateway.listarServiciosPorEspecialidadGateways(especialidadId, contratoId, agenciaId);
            for (Servicios servicios : listadoRetorno){
                gateway.validarYSetearObjetoARetornar(servicios, null);
            }
            return listadoRetorno;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }
    public List<HashMap> listarServiciosUnidadPorActividad(int actividadId, int contratoId, List serviciosList) throws Exception {
        Gateway gateway = new Gateway();
        List<HashMap> listadoRetorno = new ArrayList<HashMap>();
        try {
            gateway.abrirSesion();
            listadoRetorno = gateway.listarServiciosUnidadPorActividadGateways(actividadId, contratoId, serviciosList);

            return listadoRetorno;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    public List<HashMap> listarServiciosUnidadPorServicioYActividad(String codigoServicio, int actividadId) throws Exception {
        Gateway gateway = new Gateway();
        List<HashMap> listadoRetorno = new ArrayList<HashMap>();
        try {
            gateway.abrirSesion();
            listadoRetorno = gateway.listarServiciosUnidadPorServicioYActividad(codigoServicio,actividadId);

            return listadoRetorno;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }


    public List<Proveedores> listarProveedoresPorAgencia(int agenciaId) throws Exception {
        Gateway gateway = new Gateway();
        List<Proveedores> listadoRetorno = new ArrayList<>();
        try {
            gateway.abrirSesion();
            listadoRetorno = gateway.listarProveedoresPorAgencia(agenciaId);

            return listadoRetorno;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }


    private static final Logger log = Logger.getLogger(ContratoDAO.class);

}
