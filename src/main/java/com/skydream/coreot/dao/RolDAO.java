
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.ObjetoCoreOT;
import com.skydream.coreot.pojos.Opciones;
import com.skydream.coreot.pojos.Privilegios;
import com.skydream.coreot.pojos.Roles;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

/**
 *
 * @author mcj
 */
public class RolDAO implements GatewayDAO {

    private RolDAO() {
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        boolean retornoOK = false;
        Roles rol = (Roles) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearPrivilegios(params, gateway, rol);
            setearOpciones(params, gateway, rol);
            gateway.iniciarTransaccion();
            try {
                gateway.registrar(rol);
                gateway.commit();
            } catch (HibernateException ex) {
                log.fatal("Error al crear ROL. ", ex);
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return retornoOK;
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        boolean retornoOK = false;
        Roles rol = (Roles) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearPrivilegios(params, gateway, rol);
            setearOpciones(params, gateway, rol);
            gateway.iniciarTransaccion();
            try {
                gateway.actualizar2(rol);
                gateway.commit();
            } catch (HibernateException ex) {
                log.fatal("Error al actualizar ROL. ", ex);
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return retornoOK;
    }

    @Override
    public List listar(ObjetoCoreOT obj, Map params) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = gate.listar((Roles) obj, join);
            for (ObjetoCoreOT objeto : listaRetorno) {
                Roles rol = (Roles) objeto;
                join.add("rolesPrivilegios");
                join.add("rolesOpciones");
                gate.validarYSetearObjetoARetornar(rol, join);
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    private void setearPrivilegios(Map params, Gateway gateway, Roles rol) throws Exception {
        if (params.containsKey("rolesPrivilegios")) {
            List<Map> listadoOpciones = (ArrayList<Map>) params.get("rolesPrivilegios");
            String[] join = {};
            for (Map serv : listadoOpciones) {
                Integer idOpcion = (Integer) serv.get("id");
                Privilegios opc = (Privilegios) gateway.obtenerDatoPorId(new Privilegios(idOpcion), join);
                rol.getRolesPrivilegios().add(opc);
            }
        }
    }

    private void setearOpciones(Map params, Gateway gateway, Roles rol) throws Exception {
        if (params.containsKey("rolesOpciones")) {
            List<Map> listadoOpciones = (ArrayList<Map>) params.get("rolesOpciones");
            String[] join = {};
            for (Map serv : listadoOpciones) {
                Integer idOpcion = (Integer) serv.get("id");
                Opciones opc = (Opciones) gateway.obtenerDatoPorId(new Opciones(idOpcion), join);
                rol.getRolesOpciones().add(opc);
            }
        }
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = gate.listarConFiltro(Roles.class, filtro, valorFiltro);
            join.add("rolesPrivilegios");
            for (ObjetoCoreOT objeto : listaRetorno) {
                Roles rol = (Roles) objeto;
                if (!rol.getRolesPrivilegios().isEmpty()) {
                    gate.validarYSetearObjetoARetornar(rol, join);
                } else {
                    gate.validarYSetearObjetoARetornar(rol, null);
                }
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    public static RolDAO getINSTANCE() {
        return INSTANCE;
    }

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private static final RolDAO INSTANCE = new RolDAO();
    private static final Logger log = Logger.getLogger(RolDAO.class);

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
