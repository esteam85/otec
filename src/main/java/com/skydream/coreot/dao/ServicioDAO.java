/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.*;

import java.util.*;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;

/**
 *
 * @author rrr
 */
public class ServicioDAO implements GatewayDAO {

    private ServicioDAO() {
    }

    public static ServicioDAO getInstance() {
        return INSTANCE;
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        Servicios servicio = (Servicios) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearMateriales(params, gateway, servicio);
            gateway.iniciarTransaccion();
            try {
                gateway.registrar(obj);
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al crear Servicio. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        Servicios servicio = (Servicios) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearMateriales(params, gateway, servicio);
            gateway.iniciarTransaccion();
            try {
                gateway.actualizar2(obj);
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al actualizar Servicio. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    @Override
    public List listar(ObjetoCoreOT obj, Map params) throws Exception {
        Servicios servicio = (Servicios) obj;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = gate.listar(servicio, join);
            for (ObjetoCoreOT objeto : listaRetorno) {
                Servicios ser = (Servicios) objeto;
                if(!ser.getProveedoresServicioses().isEmpty()){
                    List<ProveedoresServicios> setProveedoresServicios = new ArrayList<>(ser.getProveedoresServicioses());
                    ProveedoresServicios proveedoresServicios = setProveedoresServicios.get(0);
                    ser.setPrecio(proveedoresServicios.getPrecio());
                }else{
                    ser.setPrecio(1D);
                }
                join.add("serviciosMaterialeses");
                join.add("proveedoresServicioses");
                gate.validarYSetearObjetoARetornar(ser, join);
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    private void setearMateriales(Map params, Gateway gateway, Servicios servicio) throws Exception {
        if (params.containsKey("serviciosMaterialeses")) {
            List<Map> listadoMateriales = (ArrayList<Map>) params.get("serviciosMaterialeses");
            for (Map mapMate : listadoMateriales) {

                Map material = (HashMap) mapMate.get("materiales");
                int idMaterial = (Integer) material.get("id");

                Materiales mate = (Materiales) gateway.retornarObjetoCoreOT(Materiales.class, idMaterial);
                Hibernate.initialize(material);

                Integer cantidad = (Integer) mapMate.get("cantidad");

                ServiciosMateriales servMate = new ServiciosMateriales(servicio, mate, cantidad);
                servicio.getServiciosMaterialeses().add(servMate);
            }
        }
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static final Logger log = Logger.getLogger(ServicioDAO.class);
    private static final ServicioDAO INSTANCE = new ServicioDAO();

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
