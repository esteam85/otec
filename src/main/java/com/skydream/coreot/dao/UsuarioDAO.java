/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.*;
import com.skydream.coreot.util.Config;
import com.skydream.coreot.util.SecureAccess;
import com.skydream.coreot.util.SecureTransmission;
import com.skydream.coreot.util.SendMail;
import java.security.SecureRandom;
import java.util.*;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

/**
 *
 * @author mcj
 */
public class UsuarioDAO implements GatewayDAO {

    private UsuarioDAO() {}
    
    public static UsuarioDAO getINSTANCE() {
        return INSTANCE;
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        Usuarios usr = (Usuarios) obj;

        byte[] salto = SecureAccess.createSalt();
        String passUsr = SecureAccess.createHash(usr.getEmail().toCharArray(), salto);
        usr.setClave(passUsr);

        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearUsuariosContratos(params, gateway, usr);
            setearUsuariosRoles(params, gateway, usr);

            gateway.iniciarTransaccion();
            try {
                gateway.registrar(usr);
                //Repositorios repositorio = (Repositorios) gateway.obtenerDatoPorId(new Repositorios(2), arrVacio);
                boolean existeToken = true;
                String token_ = "";

                while (existeToken) {
                    token_ = retornarTokenSession();
                    existeToken = gateway.validarToken(token_, 'T');

                    if (!existeToken) {
                        Login login = new Login();
                        login.setUsuarios(usr);
                        login.setToken(token_);
                        Date fecha = new Date();
                        login.setFecha(fecha);
                        login.setEstado('T');
                        gateway.registrar(login);

                        break;
                    }
                }

                gateway.commit();

                enviarMailNuevoUsuario(usr, token_);
            } catch (Exception e) {
                log.fatal(e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }

    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        Usuarios usr = (Usuarios) obj;

        byte[] salto = SecureAccess.createSalt();
        String passUsr = SecureAccess.createHash(usr.getEmail().toCharArray(), salto);
        usr.setClave(passUsr);

        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                setearUsuariosContratos(params, gateway, usr);
                setearUsuariosRoles(params, gateway, usr);
                gateway.actualizar2(obj);
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal(e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    public static void guardarAvatarUsuario(String rutConDv, byte[] imagen) throws Exception {
        Gateway gateway = new Gateway();
        try {
            Usuarios usr = obtenerUsuarioPorRut(rutConDv, gateway);
            gestionarCreacionRepositorioUsuario(usr, imagen);
        } catch (Exception e) {
            log.fatal(e);
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    public Repositorios obtenerRepositorioPorUsuarioId(int usuarioId) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            Usuarios usuario = (Usuarios) gate.obtenerUsuarioPorId(usuarioId);
            int repositorioId = usuario.getRepositorio().getId();

            Repositorios repositorioAux = (Repositorios) gate.retornarObjetoCoreOT(Repositorios.class, repositorioId);
            Repositorios repositorio = new Repositorios();
            repositorio.setId(repositorioId);
            repositorio.setServidor(repositorioAux.getServidor());
            repositorio.setUsuario(repositorioAux.getUsuario());
            repositorio.setPassword(repositorioAux.getPassword());
            repositorio.setUrl(repositorioAux.getUrl());


            return repositorio;
        } catch (Exception e) {
            log.error("Error al obtener usuario.", e);
            throw e;
        } finally {
            gate.cerrarSesion();
        }
    }

    private static Usuarios obtenerUsuarioPorRut(String rutConDv, Gateway gateway) throws Exception, NumberFormatException {
        int largoCadena = rutConDv.length();
        int ultimaPosicion = largoCadena - 1;
        char dv = rutConDv.charAt(ultimaPosicion);
        Integer rut = Integer.parseInt(rutConDv.substring(0, ultimaPosicion));
        gateway.abrirSesion();
        Usuarios usr = (Usuarios) gateway.obtenerUsuarioPorRut(rut, dv);
        return usr;
    }

    public static void guardarDocumentoUsuario(String rutConDv, String nombreArchivo, byte[] imagen) throws Exception {
        Gateway gateway = new Gateway();
        try {
            Usuarios usr = obtenerUsuarioPorRut(rutConDv, gateway);
            guardarPdfEnRepositorio(usr.getRepositorio(), rutConDv, nombreArchivo, imagen);
        } catch (Exception e) {
            log.fatal(e);
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    private static void guardarPdfEnRepositorio(Repositorios repo, String rutUsuario, String nombreArchivo, byte[] arrImagen) {
        String nombreDocumento = nombreArchivo + ".pdf";
        String SFTPHost = repo.getServidor();
        String SFTPUser = repo.getUsuario();
        String SFTPPassword = repo.getPassword();
        String SFTPDir = repo.getUrl() + "/" + rutUsuario + "/Documentos/";
        SecureTransmission.sendFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir, nombreDocumento, arrImagen);
    }

    private static void gestionarCreacionRepositorioUsuario(Usuarios usr, byte[] imagen) throws Exception {
        Repositorios repo = usr.getRepositorio();
        if (repo != null) {
            String rutCompleto = String.valueOf(usr.getRut()) + String.valueOf(usr.getDv());
            crearDirectoriosUsuarioEnRepositorio(rutCompleto, repo);
            guardarAvatarEnRepositorio(repo, rutCompleto, imagen);
        } else {
            throw new Exception("Usuario NO posee repositorio asignado. ");
        }
    }

    private static void guardarAvatarEnRepositorio(Repositorios repo, String rutUsuario, byte[] arrImagen) {
        String nombreImagenAvatar = rutUsuario + ".jpg";
        String SFTPHost = repo.getServidor();
        String SFTPUser = repo.getUsuario();
        String SFTPPassword = repo.getPassword();
        String SFTPDir = repo.getUrl() + "/" + rutUsuario + "/Avatar/";
        SecureTransmission.sendFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir, nombreImagenAvatar, arrImagen);
    }

    private static String retornarNombreArchivoAvatar(Usuarios usr) {
        return String.valueOf(usr.getRut()) + usr.getDv();
    }

    private static String retornarURLAvatarUsuario(Usuarios usuario) {
        String rutCompleto = String.valueOf(usuario.getRut()) + String.valueOf(usuario.getDv());
        return usuario.getRepositorio().getUrl() + "/" + rutCompleto + "/Avatar/";
    }

    private void borrarAvatarEnRepositorio(Repositorios repo, String rutUsuario) {
        String nombreImagenAvatar = rutUsuario + ".jpg";
        String SFTPHost = repo.getServidor();
        String SFTPUser = repo.getUsuario();
        String SFTPPassword = repo.getPassword();
        String SFTPDir = repo.getUrl() + "/" + rutUsuario + "/Avatar/";
        SecureTransmission.deleteFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir, nombreImagenAvatar);
    }

    private void enviarMailNuevoUsuario(Usuarios usuario, String token) throws Exception {
        try {
            Config config = Config.getINSTANCE();
            String asunto = config.getAsuntoMailNuevoUsuario();
            String linkSetearClave = config.getUrlSetearClaveUsuario() + token;
            String msjeBienvenida = config.getCuerpoMailNuevoUsuario() + usuario.getNombreUsuario();
            String msjeURLCambioClave = "Para efectos de seguridad te solicitamos modificar tu password accediendo al siguiente link : " + linkSetearClave;
            String cuerpo = msjeBienvenida + "\n" + msjeURLCambioClave;
            String mailDestino = usuario.getEmail();
            SendMail sendMail = SendMail.getINSTANCE();
            boolean envioOK = sendMail.enviarMail(mailDestino, asunto, cuerpo, null);
            if (!envioOK) {
                throw new Exception("Se produjo un error al emitir mail de activacion.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void setearUsuariosContratos(Map params, Gateway gateway, Usuarios usuario) throws Exception {
        if (params.containsKey("usuariosContratos")) {
            List<Map> listadoContratos = (ArrayList<Map>) params.get("usuariosContratos");
            for (Map mapUsrPerf : listadoContratos) {
                Contratos con = (Contratos) gateway.retornarObjetoCoreOT(Contratos.class, (Integer) mapUsrPerf.get("id"));
                usuario.getUsuariosContratos().add(con);
            }
        } else {
            throw new Exception("Para crear usuario se debe enviar listado de 'usuariosContratos'");
        }
    }

    private void setearUsuariosRoles(Map params, Gateway gateway, Usuarios usuario) throws Exception {
        if (params.containsKey("usuariosRoles")) {
            List<Map> listadoPerfiles = (ArrayList<Map>) params.get("usuariosRoles");
            for (Map mapUsrPerf : listadoPerfiles) {
                Roles rol = (Roles) gateway.retornarObjetoCoreOT(Roles.class, (Integer) mapUsrPerf.get("id"));
                usuario.getUsuariosRoles().add(rol);
            }
        } else {
            throw new Exception("Para crear usuario se debe enviar listado de 'usuariosRoles'");
        }
    }

    @Override
    public List listar(ObjetoCoreOT objetoCoreOT, Map params) throws Exception {
        Usuarios usuario = (Usuarios) objetoCoreOT;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = gate.listar(usuario, join);
            for (ObjetoCoreOT objeto : listaRetorno) {
                Usuarios usr = (Usuarios) objeto;
                usr.setClave(null);
                join.add("usuariosContratos");
                join.add("usuariosRoles");
                gate.validarYSetearObjetoARetornar(usr, join);
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    public List obtenerUsuariosSegunProveedorId(int idProveedor, int idRol) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<Usuarios> listaRetorno = gate.listarUsuarios(idProveedor);
            List<Usuarios> listaFinal = new ArrayList<>();
            for (ObjetoCoreOT objeto : listaRetorno) {
                Usuarios usr = (Usuarios) objeto;
                Set<Roles> rolesUsuarios = usr.getUsuariosRoles();
                for (Roles rol: rolesUsuarios){
                    if(idRol == rol.getId()){
                        usr.setClave(null);
                        join.add("usuariosContratos");
                        join.add("usuariosRoles");
                        gate.validarYSetearObjetoARetornar(usr, join);
                        listaFinal.add(usr);
                    }
                }
            }
            return listaFinal;
        } finally {
            gate.cerrarSesion();
        }
    }

    public boolean actualizarTrabajadorWorkfloEventoEjecucion(int otId, int eventoId, int trabajadorId) throws Exception {
        Gateway gate = new Gateway();
        boolean actualizo = false;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            gate.actualizarTrabajadorWorkfloEventoEjecucion(otId, eventoId, trabajadorId );
            actualizo = true;
            gate.commit();
            return actualizo;
        } finally {
            gate.cerrarSesion();
        }
    }

    public boolean setearPassword(int usuarioId, String claveNueva) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            Usuarios usuario = (Usuarios)gate.retornarObjetoCoreOT(Usuarios.class,usuarioId);
            if (usuario != null) {
                try {
                    gate.iniciarTransaccion();
                    byte[] salto = SecureAccess.createSalt();
                    String passUsr = SecureAccess.createHash(claveNueva.toCharArray(), salto);
                    usuario.setClave(passUsr);
                    gate.actualizar2(usuario);
                    gate.commit();
                } catch (Exception ex) {
                    throw ex;
                }
            } else {
                throw new Exception("No se pudo actualizar la clave del usuario.");
            }
        } catch (Exception e) {
            throw e;
        } finally {
            gate.cerrarSesion();
        }
        return true;
    }


    public boolean validarPasswordUsuario(int usuarioId, String clave) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            Usuarios usuario = (Usuarios)gate.retornarObjetoCoreOT(Usuarios.class,usuarioId);
            if (usuario != null) {
                return SecureAccess.validatePassword(clave, usuario.getClave());
            } else {
                throw new Exception("Usuario NO encontrado.");
            }
        } catch (Exception e) {
            log.error("Error al validar password de usuario. ID : " + usuarioId);
            throw e;
        } finally {
            gate.cerrarSesion();
        }
    }

    public byte[] obtenerAvatar(String rutConDv) throws Exception {
        Gateway gate = new Gateway();
        int largoCadena = rutConDv.length();
        int ultimaPosicion = largoCadena - 1;
        char dv = rutConDv.charAt(ultimaPosicion);
        Integer rut = Integer.parseInt(rutConDv.substring(0, ultimaPosicion));
        try {
            gate.abrirSesion();
            Usuarios usuario = (Usuarios) gate.obtenerUsuarioPorRut(rut, dv);
            Repositorios rep = usuario.getRepositorio();
            String wkDir = retornarURLAvatarUsuario(usuario);
            String nombreArchivo = retornarNombreArchivoAvatar(usuario) + ".jpg";
            return SecureTransmission.getFile(rep.getServidor(), rep.getUsuario(), rep.getPassword(), wkDir, nombreArchivo);
        } catch (Exception e) {
            log.error("Error al obtener archivo avatar de usuario.", e);
            throw e;
        } finally {
            gate.cerrarSesion();
        }
    }

    private String retornarTokenSession() {
        SecureRandom random = new SecureRandom();
        byte[] arrBytes = new byte[16];
        random.nextBytes(arrBytes);
        Base64 base64 = new Base64(true);
        String varAux = new String(base64.encode(arrBytes));
        String cad = varAux.replaceAll("\r\n", "");
        return cad;
    }

    private static void crearDirectoriosUsuarioEnRepositorio(String rutUsuario, Repositorios rep) throws Exception {
        try {
            String dirPadre = rep.getUrl();

            String dirUsuario = dirPadre + "/" + rutUsuario;
            SecureTransmission.makeDirectory(rep.getServidor(), rep.getUsuario(), rep.getPassword(), dirUsuario);

            String dirAvatar = dirUsuario + "/Avatar";
            SecureTransmission.makeDirectory(rep.getServidor(), rep.getUsuario(), rep.getPassword(), dirAvatar);

            String dirDoc = dirUsuario + "/Documentos";
            SecureTransmission.makeDirectory(rep.getServidor(), rep.getUsuario(), rep.getPassword(), dirDoc);

        } catch (Exception ex) {
            log.fatal("Error al crear directorios en repositorio. ", ex);
            throw ex;
        }
    }

    public void crearUsuarioDesdePerfilProveedor(int idUsuarioProveedor, int idRolNuevoUsuario, String nombreUsuarioNuevoUsr, String rutNuevoUsuario, String nombresNuevoUsuario,
            String apellidosNuevoUsuario, String emailNuevoUsuario, String celularNuevoUsuario, byte[] avatar) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            Usuarios usuarioAutenticado = (Usuarios) gate.retornarObjetoCoreOT(Usuarios.class, idUsuarioProveedor);
            Proveedores proveedor = usuarioAutenticado.getProveedor();

            Roles rol = (Roles) gate.retornarObjetoCoreOT(Roles.class, idRolNuevoUsuario);

            //Validar que el Rol que se desee crear corresponda a un Perfil Proveedor
            if (rol == null || !rol.getPerfil().getNombre().contains("Proveedor")) {
                throw new Exception("Rol inv\u00e1lido");
            }

            Usuarios nvoUsuario = new Usuarios();
            Set<Roles> roles = new HashSet<>();
            roles.add(rol);
            nvoUsuario.setUsuariosRoles(roles);
            nvoUsuario.setNombreUsuario(nombreUsuarioNuevoUsr);
            String[] rutCompletoNvoUsuario = rutNuevoUsuario.split("-");
            nvoUsuario.setRut(Integer.parseInt(rutCompletoNvoUsuario[0]));
            nvoUsuario.setDv(rutCompletoNvoUsuario[1].charAt(0));
            nvoUsuario.setNombres(nombresNuevoUsuario);
            nvoUsuario.setApellidos(apellidosNuevoUsuario);
            nvoUsuario.setEmail(emailNuevoUsuario);
            nvoUsuario.setCelular(celularNuevoUsuario);
            nvoUsuario.setEstado('A');
            nvoUsuario.setProveedor(proveedor);
            byte[] salto = SecureAccess.createSalt();
            String passUsr = SecureAccess.createHash(nombreUsuarioNuevoUsr.toCharArray(), salto);
            nvoUsuario.setClave(passUsr);
            nvoUsuario.setArea(usuarioAutenticado.getArea());

            Map<String,Object> filtros = new HashMap<>();
            //Obtener repositorio desde Property nombreRepositorioUsuarios
            filtros.put("nombre", Config.getINSTANCE().getNombreRepositorioUsuarios());
            Repositorios repoNvoUsuario = (Repositorios)gate.obtenerRegistroConFiltro(Repositorios.class,null,filtros);
            nvoUsuario.setRepositorio(repoNvoUsuario);

            gate.iniciarTransaccion();
            try {
                gate.persistirObjeto(nvoUsuario);
                gate.commit();
                guardarAvatarUsuario(rutCompletoNvoUsuario[0] + rutCompletoNvoUsuario[1],avatar);
            } catch (HibernateException e) {
                gate.rollback();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            gate.cerrarSesion();
        }

    }


    public boolean asignarUsuarioPagos(int usuarioId, int idUsuarioAsignado, List<DetalleBolsasValidacionesPag> listadoOt) throws Exception {
        Gateway gate = new Gateway();
        boolean asigno = false;
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            asigno = gate.asignarUsuarioPagos(usuarioId, idUsuarioAsignado, listadoOt);
            gate.commit();
            return asigno;
        } finally {
            gate.cerrarSesion();
        }
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static final Logger log = Logger.getLogger(UsuarioDAO.class);
    private static final UsuarioDAO INSTANCE = new UsuarioDAO();

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
