/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.Decisiones;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import com.skydream.coreot.pojos.Eventos;
import com.skydream.coreot.pojos.Privilegios;
import com.skydream.coreot.pojos.Roles;
import com.skydream.coreot.pojos.Workflow;
import com.skydream.coreot.pojos.WorkflowEventos;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;

/**
 *
 * @author rrr
 */
public class WorkflowDAO implements GatewayDAO {

    private WorkflowDAO() {
    }

    public static WorkflowDAO getINSTANCE() {
        return INSTANCE;
    }

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        Workflow workflow = (Workflow) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearEventos(params, gateway, workflow);
            gateway.iniciarTransaccion();
            try {
                gateway.registrar(obj);
                gateway.commit();
            } catch (Exception e) {
                log.fatal("Error al crear Workflow. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        Workflow workflow = (Workflow) obj;
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            setearEventos(params, gateway, workflow);
            gateway.iniciarTransaccion();
            try {
                gateway.actualizar2(obj);
                gateway.commit();
            } catch (HibernateException e) {
                log.fatal("Error al actualizar Workflow. ", e);
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            gateway.cerrarSesion();
        }
    }

    @Override
    public List listar(ObjetoCoreOT objetoCoreOT, Map params) throws Exception {
        Workflow workflow = (Workflow) objetoCoreOT;
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            List<String> join = new ArrayList<>();
            List<ObjetoCoreOT> listaRetorno = gate.listar(workflow, join);
            for (ObjetoCoreOT objeto : listaRetorno) {
                Workflow wor = (Workflow) objeto;
//                setearWorkflowEventos(wor);
                join.add("workflowEventos");
                gate.validarYSetearObjetoARetornar(wor, join);
                Set<WorkflowEventos> setWe = wor.getWorkflowEventos();
                for (WorkflowEventos workfEventos : setWe) {
                    workfEventos.setWorkflow(null);
                    workfEventos.setId(null);
                }
            }
            return listaRetorno;
        } finally {
            gate.cerrarSesion();
        }
    }

    private void setearEventos(Map params, Gateway gateway, Workflow workflow) throws Exception {
        if (params.containsKey("workflowEventos")) {
            List<Map> listadoEventos = (ArrayList<Map>) params.get("workflowEventos");
            for (Map mapWorEve : listadoEventos) {

                Map evento = (HashMap) mapWorEve.get("evento");
                int idEvento = (Integer) evento.get("id");
                Eventos eve = (Eventos) gateway.retornarObjetoCoreOT(Eventos.class, idEvento);
                Hibernate.initialize(eve);

                Map rol = (HashMap) mapWorEve.get("rol");
                int idRol = (Integer) rol.get("id");
                Roles rol_ = (Roles) gateway.retornarObjetoCoreOT(Roles.class, idRol);
                Hibernate.initialize(rol_);

                Map decision = (HashMap) mapWorEve.get("decision");
                Decisiones decision_ = new Decisiones();
                if (decision.size() > 0) {
                    int idDecision = (Integer) decision.get("id");

                    decision_ = (Decisiones) gateway.retornarObjetoCoreOT(Decisiones.class, idDecision);
                    Hibernate.initialize(decision_);
                } else {
                    int idDecision = 0;

                    decision_ = (Decisiones) gateway.retornarObjetoCoreOT(Decisiones.class, idDecision);
                    Hibernate.initialize(decision_);
                }

                Integer proximo = (Integer) mapWorEve.get("proximo");
                Integer prioridad = (Integer) mapWorEve.get("prioridad");

                WorkflowEventos workEven = new WorkflowEventos(workflow, eve, rol_, decision_, proximo, prioridad);
                workflow.getWorkflowEventos().add(workEven);

                Privilegios privilegio = new Privilegios();
                privilegio.setEvento(eve);
                privilegio.setNombre(workflow.getNombre() + "- Privilegio - " + eve.getNombre());
                privilegio.setDescripcion("Privilegio - " + eve.getNombre());

                Set<Privilegios> privilegios = new HashSet<>();

                privilegios.add(privilegio);
                rol_.setRolesPrivilegios(privilegios);
                gateway.actualizar2(rol_);

            }
        } else {
            throw new Exception("Para crear un worflow se debe enviar listado de 'workflowEventos'");
        }
    }

    private void setearWorkflowEventos(Workflow workflow) {
        Set<WorkflowEventos> workflowEve = workflow.getWorkflowEventos();
        Set<WorkflowEventos> workEventos = new HashSet<>();
        for (WorkflowEventos we : workflowEve) {
            Eventos ev = we.getEvento();
            Eventos evento = new Eventos(ev.getId(), null, null, ev.getNombre(), ev.getDuracion(), ev.getEsTelefonica());

            Roles ro = we.getRol();
            Roles rol = new Roles(ro.getId(), null, ro.getNombre(), ro.getEstado());

            Decisiones decision = null;
            if (we.getDecision() != null) {
                Decisiones varAux = we.getDecision();
                decision = new Decisiones(varAux.getId(), varAux.getRespuesta());
            }
            WorkflowEventos workflowEvento = new WorkflowEventos(null, evento, rol, decision, we.getProximo(), we.getProximo());
            workEventos.add(workflowEvento);
        }
        workflow.setWorkflowEventos(workEventos);
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listarPorClaveForanea(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static final Logger log = Logger.getLogger(WorkflowDAO.class);
    private static final WorkflowDAO INSTANCE = new WorkflowDAO();

    @Override
    public int crearRetornarId(ObjetoCoreOT obj, Map params, int usuarioId) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjetoCoreOT retornarObjCoreOtPorId(int idObjetoCoreOT) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
