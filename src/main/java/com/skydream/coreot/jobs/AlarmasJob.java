package com.skydream.coreot.jobs;

import com.skydream.coreot.pojos.Alarmas;
import com.skydream.coreot.pojos.EventosAlarmas;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import com.skydream.coreot.pojos.WorkflowEventosEjecucion;
import com.skydream.coreot.pojos.WorkflowEventosAlarmasEjecucion;
import com.skydream.coreot.pojos.Notificaciones;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import com.skydream.coreot.pojos.Usuarios;
import com.skydream.coreot.pojos.Ot;
import com.skydream.coreot.pojos.TipoAlarma;
import com.skydream.coreot.util.SendMail;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by christian on 28-09-15.
 */
public class AlarmasJob implements Job {

    public void execute(JobExecutionContext ctx) throws JobExecutionException {

        try {
            // obtenerRegistros
            List<WorkflowEventosEjecucion> listadoEventos = obtenerRegistrosWorkflowEventos();
            generarAlarmas(listadoEventos);
        } catch (Exception ex) {
            Logger.getLogger(AlarmasJob.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void generarAlarmas(List<WorkflowEventosEjecucion> listadoWorkflowEventosEjecucion) throws Exception {

//        SendMail sendMail = SendMail.getINSTANCE();
//        boolean envioOK = sendMail.enviarMail("christian.espinoza@skydream.cl", "*** Notificacion ***", "Correeeeee");
//        if(envioOK){
//            System.out.printf(new Locale("es", "MX"), "%tc Se envio correo...%n", new java.util.Date());
//        }else{
//            System.out.printf(new Locale("es", "MX"), "%tc Noooo Se envio correo...%n", new java.util.Date());
//        }
//        System.out.printf(new Locale("es", "MX"), "%tc NOTIFICACION %n", new java.util.Date());
        GatewayJob gateway = new GatewayJob();
        List<WorkflowEventosAlarmasEjecucion> listadoWEAE = new ArrayList<>();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                for (WorkflowEventosEjecucion wee : listadoWorkflowEventosEjecucion) {

                    listadoWEAE = gateway.listarWorkflowEventosAlarmasEjecucion(wee.getId().getWorkflowId(), wee.getId().getEventoId());

                    Ot ot = (Ot) gateway.retornarObjetoCoreOT(Ot.class, wee.getOt().getId());
                    Hibernate.initialize(ot);

                    for (WorkflowEventosAlarmasEjecucion weae : listadoWEAE) {

                        //Date fechaTerminoEstimada = sdf.parse(wee.getFechaTerminoEstimada().toString());
                        EventosAlarmas eventoAlarma = gateway.obtenerEventoAlarma(weae.getId().getEventoId(), weae.getId().getAlarmaId());

                        Alarmas alarma = (Alarmas) gateway.retornarObjetoCoreOT(Alarmas.class, eventoAlarma.getAlarmas().getId());
                        Hibernate.initialize(alarma);
                        
                        TipoAlarma tipoAlarma = (TipoAlarma) gateway.retornarObjetoCoreOT(TipoAlarma.class, alarma.getTipoAlarma().getId());
                        Hibernate.initialize(tipoAlarma);

                        int tiempo = alarma.getTiempo();
                        int dias = diferenciaEnDias(new Date(), wee.getFechaTerminoEstimada());
                        String asuntoNotificacion = "Notificacion de Alarma";

                        String mensaje = "Alarma para OT con Número : " + ot.getId() + ", "  + eventoAlarma.getMensaje();
                        
                        switch (tipoAlarma.getIdentificador()) {

                            case -1:
                                if (Math.abs(dias) <= tiempo) {
                                    
                                    enviarNotificacion(wee.getUsuarioId(), wee.getId().getEventoId(), ot, asuntoNotificacion, mensaje);
                                    enviarNotificacion(wee.getUsuarioEjecutorId(), wee.getId().getEventoId(), ot, asuntoNotificacion, mensaje);
                                }
                                break;
                            case 0:
                                if (Math.abs(dias) <= tiempo) {
                                    enviarNotificacion(ot.getGestor().getId(), wee.getId().getEventoId(), ot, asuntoNotificacion, mensaje);
                                    enviarNotificacion(wee.getUsuarioId(), wee.getId().getEventoId(), ot, asuntoNotificacion, mensaje);
                                    enviarNotificacion(wee.getUsuarioEjecutorId(), wee.getId().getEventoId(), ot, asuntoNotificacion, mensaje);
                                }
                                break;
                            case 1:
                                if (Math.abs(dias) <= tiempo) {
                                    enviarNotificacion(ot.getGestor().getId(), wee.getId().getEventoId(), ot, asuntoNotificacion, mensaje);
                                    enviarNotificacion(wee.getUsuarioId(), wee.getId().getEventoId(), ot, asuntoNotificacion, mensaje);
                                    enviarNotificacion(wee.getUsuarioEjecutorId(), wee.getId().getEventoId(), ot, asuntoNotificacion, mensaje);
                                }
                                break;

                            default: ;
                                break;
                        }

                        listadoWEAE.clear(); //listado de la tabla WorkflowEventosAlarmasEjecucion.
                        System.out.printf(new Locale("es", "MX"), "%tc ENVIO DE NOTIFICACION %n", new java.util.Date());
                    }
                }
                gateway.commit();
            } catch (HibernateException ex) {
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }

    }

    private List<WorkflowEventosEjecucion> obtenerRegistrosWorkflowEventos() throws Exception {

        GatewayJob gateway = new GatewayJob();
        List<WorkflowEventosEjecucion> listadoWorkflowEventosEjecucion = new ArrayList<>();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                listadoWorkflowEventosEjecucion = gateway.listarWorkflowEventosEjecucion();

            } catch (HibernateException ex) {
                //log.fatal("Error al crear Parametro. ", ex);
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return listadoWorkflowEventosEjecucion;
    }

    public static int diferenciaEnDias(Date fechaMayor, Date fechaMenor) {
        Long fecMayor = fechaMayor.getTime();
        Long fecMenor = fechaMenor.getTime();
        Long diferenciaEnMs = fecMayor - fecMenor;

        long dias = diferenciaEnMs / (1000 * 60 * 60 * 24);
        return (int) dias;
    }

    private boolean enviarNotificacion(int usuarioId, int eventoId, Ot ot, String asunto, String cuerpoMensaje) throws Exception {

        boolean envioOk = false;
        GatewayJob gateway = new GatewayJob();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                Usuarios usuario = (Usuarios) gateway.retornarObjetoCoreOT(Usuarios.class, usuarioId);
                Hibernate.initialize(usuario);

                Notificaciones notificacion = new Notificaciones();
                notificacion.setOt(ot);
                notificacion.setEventoId(eventoId);
                notificacion.setUsuarioId(usuario.getId());
                notificacion.setMensaje(cuerpoMensaje);
                notificacion.setEmail(usuario.getEmail());

                Date fecha = new Date();
                Date fechaCreacion = sdf.parse(fecha.toString());
                notificacion.setFechaCreacion(fechaCreacion);

                gateway.registrar((ObjetoCoreOT) notificacion);
                
                List<String> listaUsuariosCc = new ArrayList<>();
                listaUsuariosCc.add("contacto@skydream.cl");

                SendMail sendMail = SendMail.getINSTANCE();
                envioOk = sendMail.enviarMail(usuario.getEmail(), asunto, cuerpoMensaje, listaUsuariosCc);

                gateway.commit();
            } catch (HibernateException ex) {
                
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return envioOk;
    }

}
