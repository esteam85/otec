package com.skydream.coreot.jobs;

import com.jcraft.jsch.*;
import com.skydream.coreot.pojos.Repositorios;
import com.skydream.coreot.util.Config;
import com.skydream.coreot.util.SendMail;
import org.apache.fop.apps.*;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * Genera pdf cuando acta este pagada dejando en repositorio
 */
public class CorreoGestionEconomicaJob implements Job {

    public void execute(JobExecutionContext ctx) throws JobExecutionException {

        try {
            //Logger.getLogger(PdfActaPagadaJob.class.getName()).log(Level.INFO, "Inciando Job generador de PDFs Actas.");
            log.info("Inciando Job generador de Correos Gestion Economica.");

            int usuarioId;
            String usuarioMail;

            boolean salida;
            Date fecha_ejecucion = new Date();

            List<Object[]> listaUsuariosCorreo = listarUsuariosCorreoGe(fecha_ejecucion);

            if(listaUsuariosCorreo.size() > 0){

                for(Object[] obj : listaUsuariosCorreo){

                    usuarioId = (Integer) obj[0];
                    usuarioMail = (String) obj[1];

                    salida = enviarCorreoGe(usuarioId, usuarioMail, fecha_ejecucion);
                    if (salida) {
                        setCorreoEnviado(usuarioId, fecha_ejecucion);
                    }
                }

            }
            //Logger.getLogger(PdfActaPagadaJob.class.getName()).log(Level.INFO, "Finalizando Job generador de PDFs Actas.");
            log.info("Finalizando Job generador de Correos Gestion Economica.");

        } catch (Exception ex) {
            log.error(ex);
        }
    }

    private boolean enviarCorreoGe(int usuarioId, String mail, Date fecha)  throws Exception {

        boolean envioOk = false;
        String asunto = "Listado de Ots Validadas y/o Rechazadas";
        String htmlCuerpo ="";
        String cuerpo = "Las Siguientes Ots fueron rechazadas y/o aprobadas.";

        try {

            htmlCuerpo = obtenerHtmlCorreoGe(usuarioId, fecha);

            cuerpo = cuerpo + htmlCuerpo;

            List<String> listaUsuariosCc = new ArrayList<>();
            //listaUsuariosCc.add("contacto@skydream.cl");

            SendMail sendMail = SendMail.getINSTANCE();

            if(sendMail.enviarMail(mail, asunto, cuerpo, listaUsuariosCc)){
                envioOk = true;
                log.info("Correo OK enviado a "+mail);
                Thread.sleep(5000);
            }else {
                log.info("Correo NOK enviado a "+mail);
                envioOk = false;
            }
        } catch (Exception ex) {
            log.error("Error al enviar Correo ("+mail+") :", ex);
            envioOk = false;
        }
        return envioOk;
    }

    private String obtenerHtmlCorreoGe (int usuarioId, Date fecha) throws Exception {

        GatewayJob gateway = new GatewayJob();
        String respHTML;

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                respHTML = gateway.obtenerHtmlCorreoGe(usuarioId, fecha);

            } catch (HibernateException ex) {
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return respHTML;
    }

    private List<Object[]> listarUsuariosCorreoGe (Date fecha) throws Exception {

        GatewayJob gateway = new GatewayJob();
        List<Object[]> listaUsuariosCorreo = new ArrayList<>();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                listaUsuariosCorreo = gateway.listarUsuariosCorreoGe(fecha);

            } catch (HibernateException ex) {
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return listaUsuariosCorreo;
    }

    private void setCorreoEnviado (int usuarioId, Date fecha) throws Exception {

        GatewayJob gateway = new GatewayJob();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                gateway.setCorreoEnviadoGe(usuarioId, fecha);
                gateway.commit();

            } catch (HibernateException ex) {
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
    }

    private static final Logger log = Logger.getLogger(GatewayJob.class);
}
