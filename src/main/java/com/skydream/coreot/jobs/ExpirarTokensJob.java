/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.jobs;

import com.skydream.coreot.pojos.Login;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.skydream.coreot.util.Config;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author rrr
 */
public class ExpirarTokensJob implements Job {

    public void execute(JobExecutionContext ctx) throws JobExecutionException {
        try {
            List<Login> listadoTokens = obtenerTokens();
            expirarTokens(listadoTokens);
        } catch (Exception ex) {
            Logger.getLogger(ExpirarTokensJob.class.getName());
        }
    }

    private void expirarTokens(List<Login> listadoTokens) throws Exception {
        GatewayJob gateway = new GatewayJob();

        try {
            gateway.abrirSesion();
            try {
                gateway.iniciarTransaccion();
                for (Login login : listadoTokens) {
                    int minutos = diferenciaEnMinutos(new Date(), login.getFechaUltimaAccion());
                    Integer tiempoExpiracionTokens = Integer.parseInt(Config.getINSTANCE().getTiempoExpiracionTokens());
                    boolean estadoActivo = login.getEstado()!='C' && login.getEstado()!='E';
                    if (login.getTipoToken()!=null)
                        estadoActivo = estadoActivo && login.getTipoToken() != 2;
                    if ((minutos > tiempoExpiracionTokens) && (estadoActivo)) {
                        Login loginAux = (Login) gateway.retornarObjetoCoreOT(Login.class, login.getId());
                        loginAux.setEstado('E');
                        gateway.actualizar2(loginAux);
                    }
                }
                gateway.commit();
            } catch (HibernateException ex) {
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
    }

    private List<Login> obtenerTokens() throws Exception {

        GatewayJob gateway = new GatewayJob();
        List<Login> listadoLogin = new ArrayList<>();

        try {
            gateway.abrirSesion();
            try {
                listadoLogin = gateway.listarTokens();
            } catch (HibernateException ex) {
                //log.fatal("Error", ex);
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return listadoLogin;
    }

    public static int diferenciaEnMinutos(Date fechaMayor, Date fechaMenor) {
        Long fecMayor = fechaMayor.getTime();
        Long fecMenor = fechaMenor.getTime();
        Long diferenciaEnMs = fecMayor - fecMenor;

        long minutos = diferenciaEnMs / (1000 * 60);
        return (int) minutos;
    }

}
