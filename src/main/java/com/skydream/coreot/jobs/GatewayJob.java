/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.jobs;

import com.skydream.coreot.pojos.*;
import com.skydream.coreot.util.NewHibernateUtil;
import org.apache.log4j.Logger;
import org.apache.log4j.pattern.IntegerPatternConverter;
import org.hibernate.*;
import org.hibernate.criterion.*;

import java.io.Serializable;
import java.lang.InstantiationException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 *
 * @author mcj
 */
public class GatewayJob {

    protected void abrirSesion() throws Exception {
        try {
            SessionFactory sesionFactory = NewHibernateUtil.getSessionFactory();
            this.sesion = sesionFactory.openSession();
        } catch (Exception e) {
            System.out.println("No se pudo iniciar sesión. " + e.getStackTrace().toString());
            throw e;
        }
    }

    protected void cerrarSesion() {
        this.sesion.close();
    }

    protected void rollback() {
        this.trx.rollback();
    }

    protected void commit() {
        this.trx.commit();
    }

    protected void iniciarTransaccion() {
        this.trx = sesion.beginTransaction();
    }

    protected Serializable registrar(ObjetoCoreOT obj) throws Exception {
        Serializable registro = 0;
        try {
            registro = sesion.save(obj);
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + obj.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return registro;
    }

    protected List<ObjetoCoreOT> listarGenerico(Class claseObj, Map<String,Object> filtros, List<String> joinObjCoreOT) throws Exception {
        List<ObjetoCoreOT> lista = new ArrayList();
        try {
            Criteria criteria = sesion.createCriteria(claseObj);
            if (joinObjCoreOT != null) {
                for (String join : joinObjCoreOT) {
                    criteria.setFetchMode(join, FetchMode.JOIN);
                }
            }
            if(filtros!=null){
                for(Map.Entry filtro : filtros.entrySet()){
                    criteria.add(Restrictions.eq((String) filtro.getKey(), filtro.getValue()));
                }
            }
            lista = criteria.list();
            if (lista != null) {
                for (ObjetoCoreOT objeto : lista) {
                    validarYSetearObjetoARetornar(objeto, joinObjCoreOT);
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return lista;
    }

    protected List<ObjetoCoreOT> listar(ObjetoCoreOT obj, List<String> joinObjCoreOT) throws Exception {
        try {
            Criteria criteria = sesion.createCriteria(obj.getClass());
            for (String join : joinObjCoreOT) {
                criteria.setFetchMode(join, FetchMode.JOIN);
            }
            List<ObjetoCoreOT> lista = criteria.list();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    protected List<WorkflowEventosEjecucion> listarWorkflowEventosEjecucion() throws Exception {
        List<WorkflowEventosEjecucion> lista = new ArrayList();
        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class);

            criteria.add(Restrictions.eq("ejecutado", 0));
            //criteria.add(Restrictions.isNotNull("fecha_inicio_estimada"));
            //criteria.add(Restrictions.isNotNull("fecha_termino_estimada"));
            lista = criteria.list();
        } catch (HibernateException e) {
            throw e;
        }
        return lista;
    }

    protected List<WorkflowEventosAlarmasEjecucion> listarWorkflowEventosAlarmasEjecucion(int workflowId, int eventoId) throws Exception {
        List<WorkflowEventosAlarmasEjecucion> lista = new ArrayList();
        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventosAlarmasEjecucion.class);

            criteria.setFetchMode("workflow_id", FetchMode.JOIN);
            criteria.add(Restrictions.eq("workflow_id", workflowId));
            criteria.setFetchMode("evento_id", FetchMode.JOIN);
            criteria.add(Restrictions.eq("evento_id", eventoId));
            lista = criteria.list();
        } catch (HibernateException e) {
            throw e;
        }
        return lista;
    }

    protected EventosAlarmas obtenerEventoAlarma(int eventoId, int alarmaId) throws Exception {
        EventosAlarmas eventoAlarma = new EventosAlarmas();
        try {
            Criteria criteria = sesion.createCriteria(EventosAlarmas.class);

            criteria.setFetchMode("evento_id", FetchMode.JOIN);
            criteria.add(Restrictions.eq("evento_id", eventoId));
            criteria.setFetchMode("alarma_id", FetchMode.JOIN);
            criteria.add(Restrictions.eq("alarma_id", alarmaId));
            eventoAlarma = (EventosAlarmas) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw e;
        }
        return eventoAlarma;
    }

    protected ObjetoCoreOT obtenerDatoPorId(ObjetoCoreOT obj, String[] joinObjCoreOT) throws Exception {
        ObjetoCoreOT objetoRetorno = null;
        Class claseObj = obj.getClass();
        Criteria criteria = sesion.createCriteria(claseObj);
        if (joinObjCoreOT != null) {
            for (String join : joinObjCoreOT) {
                criteria.setFetchMode(join, FetchMode.JOIN);
            }
        }
        Method method = claseObj.getMethod("getId");
        Integer id = (Integer) method.invoke(obj);
        criteria.add(Restrictions.eq("id", id));
        objetoRetorno = (ObjetoCoreOT) criteria.uniqueResult();
        if (objetoRetorno != null) {
            validarYSetearObjetoARetornar(objetoRetorno, null);
        }
        return objetoRetorno;
    }

    protected boolean actualizar(ObjetoCoreOT objetoCoreOT) throws Exception {
        boolean actualizar = false;
        try {
            Class claseObj = objetoCoreOT.getClass();
            Method method = claseObj.getMethod("getId");
            Integer id = (Integer) method.invoke(objetoCoreOT);
            ObjetoCoreOT registroActual = (ObjetoCoreOT) sesion.get(claseObj, id);
            if (registroActual != null) {
                validarYSetearObjetoAPersistir(objetoCoreOT, registroActual);
                sesion.update(registroActual);
                actualizar = true;
            } else {
                throw new Exception("No se encontró el registro a actualizar");
            }
        } catch (Exception e) {
            System.out.println("No se pudo actualizar el objeto " + objetoCoreOT.getClass().toString());
            throw e;
        }
        return actualizar;
    }

    protected ObjetoCoreOT retornarObjetoCoreOT(Class claseObjetoCoreOT, int id) throws Exception {
        try {
            ObjetoCoreOT objetoRetorno = (ObjetoCoreOT) sesion.load(claseObjetoCoreOT, id);
            return objetoRetorno;
        } catch (Exception e) {
            System.out.println("No se pudo cargar el objeto " + claseObjetoCoreOT.toString());
            throw e;
        }
    }

    protected boolean actualizar2(ObjetoCoreOT objetoCoreOT) throws Exception {
        try {
            sesion.update(objetoCoreOT);
        } catch (HibernateException e) {
            System.out.println("No se pudo actualizar el objeto " + objetoCoreOT.getClass().toString());
            throw e;
        }
        return true;
    }

    private void validarYSetearObjetoAPersistir(ObjetoCoreOT objetoEnviado, ObjetoCoreOT objetoBBDD) throws IllegalAccessException {
        String nombreClaseObjeto = objetoBBDD.getClass().getName();
        System.out.println("Clase : " + nombreClaseObjeto);
        Field[] atributos = objetoEnviado.getClass().getDeclaredFields();

        for (Field campo : atributos) {
            String nombreCampo = campo.getName();
            System.out.println("Nombre atributo : " + nombreCampo);
            Class claseCampo = campo.getType();

            if (!Set.class.isAssignableFrom(claseCampo)) {
                campo.setAccessible(true);
                Object valor = campo.get(objetoEnviado);
                campo.set(objetoBBDD, valor);
            }
        }
    }

    protected List<ObjetoCoreOT> listarConFiltro(Class claseObjeto, String filtro, int valorFiltro) throws Exception {
        List<ObjetoCoreOT> lista = null;
        try {
            Criteria criteria = sesion.createCriteria(claseObjeto);
            criteria.setFetchMode(filtro, FetchMode.JOIN);
            criteria.add(Restrictions.eq(filtro, valorFiltro));
            lista = criteria.list();
        } catch (HibernateException e) {
            throw e;
        }
        return lista;
    }

    protected List<Usuarios> obtenerCorreoUsuario(Usuarios usr, int valueId) throws Exception {
        List<Usuarios> lista = new ArrayList();
        usr = new Usuarios();
        try {
            Criteria criteria = sesion.createCriteria(usr.getClass());
            criteria.setFetchMode("id", FetchMode.JOIN);
            criteria.add(Restrictions.eq("id", valueId));
            lista = criteria.list();
        } catch (HibernateException e) {
            throw e;
        }
        return lista;
    }

    protected List<Perfiles> listarRolesPorPerfil() throws Exception {
        List<Perfiles> listaRetorno = new ArrayList<>();
        try {
            Criteria criteria = sesion.createCriteria(Roles.class);
            criteria.setProjection(Projections.projectionList()
                    .add(Projections.groupProperty("perfiles"))
            );
            List<Perfiles> lista = criteria.list();
            for (Perfiles p : lista) {
                Perfiles perfil = new Perfiles(p.getId(), p.getNombre(), p.getDescripcion(), p.getEstado(), null);
                perfil.setId(p.getId());
                perfil.setNombre(p.getNombre());
                perfil.setEstado(p.getEstado());
                perfil.setDescripcion(p.getDescripcion());
                Set<Roles> roles = new HashSet<>(p.getRoleses());
                for (Roles r : roles) {
                    r.setPerfil(null);
                    r.setWorkflowEventosRoleses(null);
                }
                perfil.setRoleses(roles);
                listaRetorno.add(perfil);
            }
        } catch (Exception e) {
            throw e;
        }
        return listaRetorno;
    }

    protected void validarYSetearObjetoARetornar(ObjetoCoreOT objCoreOT, List<String> camposJoin) {
        String nombreClaseObjeto = objCoreOT.getClass().getName();
        System.out.println("Clase : " + nombreClaseObjeto);
        Field[] atributos = objCoreOT.getClass().getDeclaredFields();

        try {
            for (Field campo : atributos) {
                System.out.println("Nombre atributo : " + campo.getName());
                Class claseCampo = campo.getType();
                campo.setAccessible(true);
                boolean isSet = Set.class.isAssignableFrom(claseCampo);
                boolean isJoinField = camposJoin != null ? camposJoin.contains(campo.getName()) : false;
                if (isSet && !isJoinField) {
                    campo.set(objCoreOT, null);
                } else if (ObjetoCoreOT.class.isAssignableFrom(claseCampo)) {
                    ObjetoCoreOT attrObjCoreOT = (ObjetoCoreOT) campo.get(objCoreOT);
                    ObjetoCoreOT nuevoAttrObjCoreOT = setearYRetornarAtributoObjCoreOT(attrObjCoreOT, claseCampo);
                    campo.set(objCoreOT, nuevoAttrObjCoreOT);
                } else if (isSet && isJoinField) {
                    Set setObjetos = (Set) campo.get(objCoreOT);
                    for (Object objSet : setObjetos) {
                        ObjetoCoreOT attrObjCoreOT = (ObjetoCoreOT) objSet;
                        validarYSetearObjetoARetornar(attrObjCoreOT, null);
                    }
                }
            }
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | InstantiationException ex) {
            System.out.println(ex);
        }
    }

    private ObjetoCoreOT setearYRetornarAtributoObjCoreOT(ObjetoCoreOT objCoreOT, Class claseTipoObjeto) throws NoSuchMethodException, InvocationTargetException, SecurityException, IllegalArgumentException, IllegalAccessException, InstantiationException {
        String nombreClaseObjeto = claseTipoObjeto.toString();
        System.out.println("Clase : " + nombreClaseObjeto);
        Field[] atributos = claseTipoObjeto.getDeclaredFields();

        Map<String, Method> mapaGets = retornarMapaGet(claseTipoObjeto.getMethods(), atributos);
        try {
            Constructor constructor = claseTipoObjeto.getConstructor();
            ObjetoCoreOT objetoRetorno = (ObjetoCoreOT) constructor.newInstance();
            for (Field campo : atributos) {
                System.out.println("Nombre atributo : " + campo.getName());
                Class claseCampo = campo.getType();
                Method metodo = mapaGets.get(campo.getName());
                campo.setAccessible(true);
                if (Set.class.isAssignableFrom(claseCampo) || ObjetoCoreOT.class.isAssignableFrom(claseCampo)) {
                    campo.set(objetoRetorno, null);
                } else {
                    Object objetoAux = metodo.invoke(objCoreOT);
                    campo.set(objetoRetorno, objetoAux);
                }
            }
            return objetoRetorno;
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | InstantiationException ex) {
            System.out.println("Error al setear nuevo atributo de objeto" + ex);
            throw ex;
        }
    }

    private Map<String, Method> retornarMapaGet(Method[] metodos, Field[] campos) {
        Map<String, Method> mapaRetorno = new HashMap<>();
        for (Method metodo : metodos) {
            String nombreMetodo = metodo.getName();
            if (isGetter(metodo)) {
                for (Field obj : campos) {
                    int largoEsperadoCampoGet = "get".length() + obj.getName().length();
                    boolean contieneCampo = nombreMetodo.toLowerCase().contains(obj.getName().toLowerCase());
                    if (contieneCampo && (nombreMetodo.length() == largoEsperadoCampoGet)) {
                        mapaRetorno.put(obj.getName(), metodo);
                        break;
                    }
                }
            }
        }
        return mapaRetorno;
    }

    private static boolean isGetter(Method method) {
        if (!method.getName().startsWith("get")) {
            return false;
        }
        if (method.getParameterTypes().length != 0) {
            return false;
        }
        if (void.class.equals(method.getReturnType())) {
            return false;
        }
        return true;
    }

    protected List<Perfiles> listarPerfiles() throws Exception {
        try {
            Criteria criteria = sesion.createCriteria(Perfiles.class)
                    .setFetchMode("proveedores", FetchMode.JOIN);
            List<Perfiles> lista = criteria.list();
            if (lista != null) {
                for (Perfiles p : lista) {
                    validarYSetearObjetoARetornar(p, null);
                }
            } else {
                throw new Exception("Busqueda sin resultados");
            }

            return lista;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<Usuarios> listarUsuarios() throws Exception {
        try {
            String hql = "SELECT NEW Usuarios(id, nombreUsuario, rut, dv, nombres, apellidos, email, celular, estado) "
                    + "FROM Usuarios";
            Query query = sesion.createQuery(hql);
            List<Usuarios> listaRetorno = query.list();
            return listaRetorno;
        } catch (Exception e) {
            System.out.println("No se pudo obtener listado de usuarios." + e);
            throw e;
        }
    }

    protected List<Proveedores> listarProveedores() throws Exception {
        try {
            Criteria criteria = sesion.createCriteria(ProveedoresServicios.class)
                    .setFetchMode("tipoProveedor", FetchMode.JOIN);
            List<Proveedores> lista = criteria.list();
            return lista;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<TipoEvento> listarTiposEvento() {

        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        try {
            Criteria criteria = session.createCriteria(TipoEvento.class);
            List<TipoEvento> listaRetorno = criteria.list();
            return listaRetorno;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        } finally {
            session.close();
        }
    }

    protected List<Eventos> listarEventos() {
        try {
            Criteria criteria = sesion.createCriteria(Eventos.class);
            List<Eventos> lista = criteria.list();
            for (Eventos evento : lista) {
                MensajeEvento msjeEvento = new MensajeEvento();
                msjeEvento.setId(evento.getMensajeEvento().getId());
                evento.setMensajeEvento(msjeEvento);
                TipoEvento tipoEvento = new TipoEvento();
                tipoEvento.setId(evento.getTipoEvento().getId());
                evento.setTipoEvento(tipoEvento);
            }
            return lista;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<Privilegios> listarOpciones() throws Exception {
        List<Privilegios> listaRetorno = null;
        try {
            Criteria crit = sesion.createCriteria(Privilegios.class);
            listaRetorno = crit.list();
            for (Privilegios obj : listaRetorno) {
                validarYSetearObjetoARetornar(obj, null);
            }
        } catch (HibernateException e) {
            System.out.println(e);
            throw e;
        }
        return listaRetorno;
    }

    protected List<RolesPrivilegios> listarOpcionesRoles() throws Exception {
        List<RolesPrivilegios> listaRetorno = null;
        try {
            Criteria crit = sesion.createCriteria(RolesPrivilegios.class);
            listaRetorno = crit.list();
            for (RolesPrivilegios obj : listaRetorno) {
                validarYSetearObjetoARetornar(obj, null);
            }
        } catch (HibernateException e) {
            System.out.println(e);
            throw e;
        }
        return listaRetorno;
    }

    protected List<Agencias> listarAgencias() {

        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        try {
            Criteria criteria = session.createCriteria(Agencias.class);
            List<Agencias> listaRetorno = criteria.list();
            return listaRetorno;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        } finally {
            session.close();
        }
    }

    protected int validarLoginYRetornarId(String usuario, String clave) {
        try {
            Usuarios usr = (Usuarios) sesion.createCriteria(Usuarios.class)
                    .add(Restrictions.eq("nombreUsuario", usuario))
                    .add(Restrictions.eq("clave", clave))
                    .add(Restrictions.eq("estado", 'A'))
                    .uniqueResult();
            return usr.getId();
        } catch (HibernateException hibEx) {
            log.error("Error al validar credenciales de usuario. ", hibEx);
            throw hibEx;
        }
    }

    protected boolean validarToken(String token, char estado) {
        try {
            long rows = (Long) sesion.createCriteria(Login.class)
                    .add(Restrictions.eq("token", token))
                    .add(Restrictions.eq("estado", estado))
                    .setProjection(Projections.rowCount())
                    //Pendiente agregar filtro de tiempo
                    .uniqueResult();
            return rows == 1L;
        } catch (HibernateException hibEx) {
            log.error("Error al validar credenciales de usuario. ", hibEx);
            throw hibEx;
        }
    }

    protected Login retornarLogin(String token, char estado) {
        try {
            Login login = (Login) sesion.createCriteria(Login.class)
                    .add(Restrictions.eq("token", token))
                    .add(Restrictions.eq("estado", estado))
                    //Pendiente agregar filtro de tiempo
                    .uniqueResult();
            return login;
        } catch (HibernateException hibEx) {
            log.error("Error al validar credenciales de usuario. ", hibEx);
            throw hibEx;
        }
    }

    protected List<Login> listarTokens() throws Exception {
        List<Login> lista = null;
        try {
            Criteria criteria = sesion.createCriteria(Login.class);
            //criteria.add(Restrictions.eq("estado", "T"));
            criteria.add(Restrictions.ne("estado","C".charAt(0)));
            criteria.add(Restrictions.ne("estado", "E".charAt(0)));
            lista = criteria.list();
        } catch (HibernateException e) {
            throw e;
        }
        return lista;
    }

    protected List<Object[]> listarActasPagadas () throws Exception{
        List<Object[]> listaActasPagadas = new ArrayList();
        try{
            Query query = sesion.createSQLQuery("select dbvp.acta_id, dbvp.ot_id, o.contrato_id from detalle_bolsas_validaciones_pag dbvp , pagos pag, ot o WHERE dbvp.pagos_id = pag.id AND dbvp.ot_id = o.id AND dbvp.pdf_generado IS NOT TRUE");
            listaActasPagadas = query.list();

        }catch (HibernateException e){
            throw e;
        }
        return  listaActasPagadas;
    }

    protected String obtenerXmlActa(int actaId)throws Exception{

        String respXML ="";
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from retornar_xml_acta(:id_acta)");
            sqlQuery.setParameter("id_acta",actaId);

            respXML = (String)sqlQuery.uniqueResult();

        }catch(Exception e){
            log.error("Error al obtener XML acta",e);
        }
        return respXML;
    }

    protected void setPdfGeneradoActa(int actaId) throws Exception {

        try {
            Criteria criteria = sesion.createCriteria(DetalleBolsasValidacionesPag.class);
            criteria.add(Restrictions.eq("actaId", actaId));
            DetalleBolsasValidacionesPag bolsa = (DetalleBolsasValidacionesPag) criteria.uniqueResult();
            bolsa.setPdfGenerado(true);
            sesion.update(bolsa);
        } catch (HibernateException e) {
            throw e;
        }
    }

    protected Repositorios obtenerRepositorio(String nombre) throws Exception {

        Repositorios repo = null;

        try {
            Criteria criteria = sesion.createCriteria(Repositorios.class);
            criteria.add(Restrictions.eq("nombre", nombre));
            criteria.add(Restrictions.eq("estadoActivo", Boolean.TRUE));
            repo = (Repositorios) criteria.uniqueResult();

        } catch (HibernateException e) {
            throw e;
        }
        return repo;
    }

    protected List<Object[]> listarUsuariosCorreoGe (Date fecha) throws Exception{
        List<Object[]> listaUsuariosCorreo = new ArrayList();
        try{
            Query query = sesion.createSQLQuery("SELECT distinct(cge.usuario_id), u.email FROM correos_gestion_economica cge, usuarios u " +
                    "      WHERE cge.usuario_id = u.id " +
                    "            AND cge.fecha_creacion < :fecha " +
                    "            AND cge.correo_enviado IS NOT TRUE");
            query.setParameter("fecha", fecha);
            listaUsuariosCorreo = query.list();

        }catch (HibernateException e){
            throw e;
        }
        return  listaUsuariosCorreo;
    }

    protected String obtenerHtmlCorreoGe(int usuarioId, Date fecha)throws Exception{

        String respHTML ="";
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from retornar_html_correo_ge(:usuario_id, :fecha)");
            sqlQuery.setParameter("usuario_id",usuarioId);
            sqlQuery.setParameter("fecha",fecha);

            respHTML = (String)sqlQuery.uniqueResult();

        }catch(Exception e){
            log.error("Error al obtener HTML Correo GE",e);
            throw e;
        }
        return respHTML;
    }

    protected String setCorreoEnviadoGe(int usuarioId, Date fecha)throws Exception{

        String respHTML ="";
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from validar_correo_ge(:usuario_id, :fecha)");
            sqlQuery.setParameter("usuario_id",usuarioId);
            sqlQuery.setParameter("fecha",fecha);

            respHTML = (String)sqlQuery.uniqueResult();

        }catch(Exception e){
            log.error("Error al validar Correo GE",e);
            throw e;
        }
        return respHTML;
    }

    protected List<Object[]> listarReportesConfigurados () throws Exception{
        List<Object[]> listaReportesConfigurados = new ArrayList();
        try{
            Query query = sesion.createSQLQuery("SELECT * FROM listar_reportes_configurados()");
            listaReportesConfigurados = query.list();

        }catch (HibernateException e){
            throw e;
        }
        return  listaReportesConfigurados;
    }

    protected List<Object[]> obtenerUsuariosReportes (int reporteId) throws Exception{
        List<Object[]> listaUsuariosReportes = new ArrayList();
        try{
            Query query = sesion.createSQLQuery("SELECT * FROM listar_usuarios_reportes(:reporte_id)");
            query.setParameter("reporte_id", reporteId);
            listaUsuariosReportes = query.list();

        }catch (HibernateException e){
            throw e;
        }
        return  listaUsuariosReportes;
    }

    protected List<Object[]> obtenerHojasPorReporte(int reporteId)throws Exception{
        List<Object[]> listaHojas = new ArrayList();
        try{

            Query query = sesion.createSQLQuery("SELECT rh.rep_hoja_id, h.nombre, h.columnas, h.query FROM rep_reporte_hoja rh, rep_hoja h WHERE rh.rep_reporte_id = :reporteid AND rh.rep_hoja_id = h.id");
            query.setParameter("reporteid",reporteId);

            listaHojas = query.list();

        }catch(Exception e){
            log.error("Error al obtener hojas por reporte ot",e);
            e.printStackTrace();
        }
        return listaHojas;
    }

    protected List<Object[]> obtenerHojaReporte(int hojaId)throws Exception{
        List<Object[]> resultado = null;
        try{
            String queryHoja = obtenerQueryHojaReporte(hojaId);

            SQLQuery sqlQuery = sesion.createSQLQuery(queryHoja);

            resultado = (List<Object[]>) sqlQuery.list();

        }catch(Exception e){
            log.error("Error al obtener hoja reporte ot",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected String obtenerQueryHojaReporte(int hojaId)throws Exception{
        String resultado = null;
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT query FROM rep_hoja WHERE id = :hojaid");
            sqlQuery.setParameter("hojaid",hojaId);

            resultado = sqlQuery.uniqueResult().toString();

        }catch(Exception e){
            log.error("Error al obtener Query hoja reporte ot",e);
            e.printStackTrace();
        }
        return resultado;
    }

    protected int actualizarUsuarioReporteEjecucion(int usuarioId, int reporteId)throws Exception{
        int salida = 0;
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("UPDATE rep_reporte_usuario SET ultimo_envio = current_timestamp WHERE usuario_id = :usuarioid AND rep_reporte_id = :reporteid");
            sqlQuery.setParameter("usuarioid",usuarioId);
            sqlQuery.setParameter("reporteid",reporteId);

            salida = sqlQuery.executeUpdate();

        }catch(Exception e){
            log.error("Error al atualizar reporte ejecucion",e);
            e.printStackTrace();
        }
        return salida;
    }

    protected String actualizarVistasMaterializadas() throws Exception{
        String resultado = null;
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * FROM actualizar_vistas_materializadas()");

            resultado = sqlQuery.uniqueResult().toString();

        }catch(Exception e){
            log.error("Error al actulizar vistas materializadas",e);
            e.printStackTrace();
        }
        return resultado;
    }

    private Session sesion;
    private Transaction trx;

    private static final Logger log = Logger.getLogger(GatewayJob.class);
}
