package com.skydream.coreot.jobs;

import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.pojos.TipoMoneda;
import com.skydream.coreot.pojos.TipoMonedaValores;
import com.skydream.coreot.pojos.TipoMonedaValoresId;
import com.skydream.coreot.util.Config;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.hibernate.HibernateException;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by mcj on 30-03-16.
 */
public class IndicadoresEconomicosJob implements Job {


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try{
            String[] indicadores = {"dolar","euro","uf"};

            for(String tipoIndicador : indicadores){
//                int tipoMonedaId = retornarIDTipoMoneda(tipoIndicador);
//                if(!existeRegistroTipoCambio(tipoMonedaId, new Date())){
//                }
                String respuestaServicio = invocarServicio(tipoIndicador);

                if(respuestaServicio.isEmpty()){
                    respuestaServicio = invocarServicioSBIF(tipoIndicador);
                    ObjectMapper mapper = new ObjectMapper();
                    Map mapRetorno = mapper.readValue(respuestaServicio, new TypeReference<HashMap<String, Object>>() {});
                    int idTipoMoneda = retornarIDTipoMoneda(tipoIndicador);
                    String tag = "";
                    if(tipoIndicador.equals("dolar")) tag = "Dolares";
                    if(tipoIndicador.equals("euro")) tag = "Euros";
                    if(tipoIndicador.equals("uf")) tag = "UFs";

                    List<LinkedHashMap> registros = (List) mapRetorno.get(tag);
                    List<TipoMonedaValores> listadoTipoMonedaValores = retornarListadoTipoMonedaValoresSBIF(registros, idTipoMoneda);
                    registrarTipoMonedaValores(listadoTipoMonedaValores);

                }else{
                    ObjectMapper mapper = new ObjectMapper();
                    Map mapRetorno = mapper.readValue(respuestaServicio, new TypeReference<HashMap<String, Object>>() {
                    });
                    int idTipoMoneda = retornarIDTipoMoneda((String) mapRetorno.get("codigo"));
                    List<LinkedHashMap> registros = (List) mapRetorno.get("serie");
                    List<TipoMonedaValores> listadoTipoMonedaValores = retornarListadoTipoMonedaValores(registros, idTipoMoneda);
                    registrarTipoMonedaValores(listadoTipoMonedaValores);
                }
            }
        }catch(Exception ex){
            log.error(ex);
        }
    }

    private String invocarServicio(String nombreIndicador){
        try{
            SimpleDateFormat formatoFecha = new SimpleDateFormat("dd-MM-yyyy");
            String fecha = formatoFecha.format(new Date());
            String uri = Config.getINSTANCE().getUrlServicioIndicadoresEconomicos() + nombreIndicador + "/" + fecha;
            URL url = new URL(uri);
            log.info("Invocacion servicio indicadores <" + url.toString() + ">");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(conn.getInputStream())
            );
            String output;
            System.out.println("Output from Server .... \n");
            String jsonRetorno = "";
            while ((output = br.readLine()) != null) {
                jsonRetorno = output;
                System.out.println(output);
            }
            conn.disconnect();
            return jsonRetorno;
        }catch(Exception ex){
            log.error("Error al invocar servicio Indicador Economico");
            return "";
        }
    }

    private String invocarServicioSBIF(String nombreIndicador)throws Exception{
        try{
            String apiKey = Config.getINSTANCE().getApiKeySBIF();
            String uri = "http://api.sbif.cl/api-sbifv3/recursos_api/"+nombreIndicador+"?apikey="+apiKey+"&formato=json";
            URL url = new URL(uri);
            log.info("Invocacion servicio indicadores SBIF <" + url.toString() + ">");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(conn.getInputStream())
            );
            String output;
            System.out.println("Output from Server .... \n");
            String jsonRetorno = "";
            while ((output = br.readLine()) != null) {
                jsonRetorno = jsonRetorno + output;
                System.out.println(output);
            }
            conn.disconnect();
            return jsonRetorno;
        }catch(Exception ex){
            log.error("Error al invocar servicio Indicador Economico SBIF");
            throw ex;
        }
    }

    private int retornarIDTipoMoneda(String codigoTipoMoneda) throws Exception {
        try {
            String nombreTipoMoneda = null;
            switch(codigoTipoMoneda){
                case "dolar":
                    nombreTipoMoneda = "USD";
                    break;
                case "uf":
                    nombreTipoMoneda = "UF";
                    break;
                case "euro":
                    nombreTipoMoneda = "EUR";
                    break;
            }
            Map filtros = new HashMap();
            filtros.put("nombre", nombreTipoMoneda);
            List listTipoMoneda = GenericDAO.getINSTANCE().listarGenerico(TipoMoneda.class, filtros, null);
            int tipoMonedaId = ((TipoMoneda)listTipoMoneda.get(0)).getId();
            return tipoMonedaId;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al obtener ID TipoMoneda.");
        }
    }

    private List<TipoMonedaValores> retornarListadoTipoMonedaValores(List<LinkedHashMap> registros, int idTipoMoneda)throws Exception{
        List<TipoMonedaValores> listadoTipoMonedaValores = new ArrayList<>();
        for(LinkedHashMap registro: registros){
            SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String valorFecha =  (String) registro.get("fecha");
            valorFecha = valorFecha.replace("Z","").replace("T"," ");
            Date fecha = formatoDelTexto.parse(valorFecha);
            TipoMonedaValoresId tipoMonedaValoresId = new TipoMonedaValoresId(idTipoMoneda, fecha);
            Object objValor = (Number) registro.get("valor");
            Double valor = null;
            if (objValor instanceof Integer) {
                valor = ((Integer) objValor).doubleValue();
            } else valor = (Double) objValor;
            TipoMonedaValores tipoMonedaValores = new TipoMonedaValores(tipoMonedaValoresId, valor);
            listadoTipoMonedaValores.add(tipoMonedaValores);
        }
        return listadoTipoMonedaValores;
    }

    private List<TipoMonedaValores> retornarListadoTipoMonedaValoresSBIF(List<LinkedHashMap> registros, int idTipoMoneda)throws Exception{
        List<TipoMonedaValores> listadoTipoMonedaValores = new ArrayList<>();
        for(LinkedHashMap registro: registros){
            SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
            String valorFecha =  (String) registro.get("Fecha");
            Date fecha = formatoDelTexto.parse(valorFecha);
            TipoMonedaValoresId tipoMonedaValoresId = new TipoMonedaValoresId(idTipoMoneda, fecha);
            double valor = Double.parseDouble(registro.get("Valor").toString().replace(",","."));

            TipoMonedaValores tipoMonedaValores = new TipoMonedaValores(tipoMonedaValoresId, valor);
            listadoTipoMonedaValores.add(tipoMonedaValores);
        }
        return listadoTipoMonedaValores;
    }

    private void registrarTipoMonedaValores(List<TipoMonedaValores> listadoTipoMonedaValores){
        GatewayJob gate = new GatewayJob();
        try {
            gate.abrirSesion();
            gate.iniciarTransaccion();
            try {
                for(TipoMonedaValores tipoMonedaValores : listadoTipoMonedaValores){
                    gate.registrar(tipoMonedaValores);
                }
                gate.commit();
            } catch (HibernateException ex) {
                gate.rollback();
                throw ex;
            }
        }catch(Exception ex){
            log.error("Error al registrarTipoMonedaValores");
        } finally {
            gate.cerrarSesion();
        }
    }

    private boolean existeRegistroTipoCambio(int tipoMonedaId, Date fecha)throws Exception{
        GatewayJob gate = new GatewayJob();
        try {
            gate.abrirSesion();
            try {
                Map<String, Object> filtros = new HashMap<>();
                filtros.put("id.tipoMonedaId",tipoMonedaId);
                filtros.put("id.fecha",fecha);
                List retornoConsulta = gate.listarGenerico(TipoMonedaValores.class,filtros,null);
                if(retornoConsulta==null || retornoConsulta.isEmpty()){
                    return false;
                }else{
                    return true;
                }
            } catch (HibernateException ex) {
                log.error("Error al obtener datos de tipoMonedaValores",ex);
                throw ex;
            }
        }catch(Exception ex){
            log.error("Error en existeRegistroTipoCambio.",ex);
            throw ex;
        } finally {
            gate.cerrarSesion();
        }
    }

    private static final Logger log = Logger.getLogger(GatewayJob.class);

}
