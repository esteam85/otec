package com.skydream.coreot.jobs;

import com.jcraft.jsch.*;
import com.skydream.coreot.dao.OtDAO;
import com.skydream.coreot.pojos.*;
import com.skydream.coreot.util.Config;
import org.apache.log4j.Logger;
import com.skydream.coreot.util.RenderPDF;
import com.skydream.coreot.util.SecureTransmission;
import com.skydream.coreot.util.SendMail;
import org.apache.fop.apps.*;
import org.apache.log4j.*;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Genera pdf cuando acta este pagada dejando en repositorio
 */
public class PdfActaPagadaJob implements Job {

    private ChannelSftp channelSftp;
    private Session session;
    private Repositorios repo;

    public void execute(JobExecutionContext ctx) throws JobExecutionException {

        try {
            //Logger.getLogger(PdfActaPagadaJob.class.getName()).log(Level.INFO, "Inciando Job generador de PDFs Actas.");
            log.info("Inciando Job generador de PDFs Actas.");
            // obtenerRegistros
            String nombrePdf;
            String nombreXsl;
            String xmlString;
            String SFTPDir_pdf;
            String ruta = "/Gestion_Economica/Actas_Pagadas/";
            //int repoId = 1;
            int actaId;
            int otId;
            int contradoId;
            boolean salida;

            repo = obtenerRepositorioBase();
            SFTPDir_pdf = repo.getUrl()+ ruta;

            List<Object[]> listadoActasPagadas = listarActasPagadas();

            if(listadoActasPagadas.size() > 0){
                openSessionSFTP(repo);

                makeDirectory(SFTPDir_pdf);

                for(Object[] obj : listadoActasPagadas){

                    actaId = (Integer) obj[0];
                    otId = (Integer) obj[1];
                    contradoId = (Integer) obj[2];

                    nombrePdf = "pdf_acta_"+otId+"_"+actaId;
                    nombreXsl = "plantilla_acta_contrato_"+contradoId;

                    if(!existsFile(SFTPDir_pdf, nombrePdf+".pdf")) {

                        xmlString = obtenerXmlActa(actaId);

                        if (xmlString != null) {
                            salida = generarPDFActaPagada(xmlString, repo, nombrePdf, nombreXsl);
                            if (salida) {
                                setPdfGeneradoActa(actaId);
                            }
                        }else{
                            log.info("PDF XML "+nombrePdf+" NULO ");
                        }
                    }else {
                        //log.info("PDF "+nombrePdf+" ya existe en repo "+SFTPDir_pdf);
                        setPdfGeneradoActa(actaId);
                    }
                }

                closeSessionSFTP();
            }
            //Logger.getLogger(PdfActaPagadaJob.class.getName()).log(Level.INFO, "Finalizando Job generador de PDFs Actas.");
            log.info("Finalizando Job generador de PDFs Actas.");

        } catch (Exception ex) {
            log.error(ex);
        }
    }

    private Repositorios obtenerRepositorioBase () throws Exception {

        GatewayJob gateway = new GatewayJob();
        Repositorios repo = null;
        Config config = Config.getINSTANCE();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                repo = gateway.obtenerRepositorio(config.getNombreRepositorioBase());

            } catch (HibernateException ex) {
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return repo;
    }

    private String obtenerXmlActa (int actaId) throws Exception {

        GatewayJob gateway = new GatewayJob();
        String xmlActa;

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                xmlActa = gateway.obtenerXmlActa(actaId);

            } catch (HibernateException ex) {
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return xmlActa;
    }

    private List<Object[]> listarActasPagadas () throws Exception {

        GatewayJob gateway = new GatewayJob();
        List<Object[]> listaActasPagadas = new ArrayList<>();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                listaActasPagadas = gateway.listarActasPagadas();

            } catch (HibernateException ex) {
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return listaActasPagadas;
    }

    private void setPdfGeneradoActa (Integer actaId) throws Exception {

        GatewayJob gateway = new GatewayJob();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                gateway.setPdfGeneradoActa(actaId);
                gateway.commit();

            } catch (HibernateException ex) {
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
    }

    private boolean generarPDFActaPagada(String xmlString, Repositorios repo, String _nombrePdf, String _nombreXsl)  throws IOException, FOPException, TransformerException, Exception {

        boolean salida = false;
        String nombreXsl = _nombreXsl+".xsl";
        String ruta = "/Gestion_Economica/Actas_Pagadas/";
        String nombrePdf = _nombrePdf + ".pdf";

        String SFTPDir_xsl = repo.getUrl()  + "/PDFs/Plantillas/";
        String SFTPDir_pdf = repo.getUrl()+ ruta;

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {

            System.setProperty("java.awt.headless", "true");

            byte[] bytePlantillaXSL = getFile(SFTPDir_xsl, nombreXsl);
            InputStream inputStreamXSL = new ByteArrayInputStream(bytePlantillaXSL);

            // the XSL FO file
            // create an instance of fop factory
            FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
            // a user agent is needed for transformation
            FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
            // Setup output
            // Construct fop with desired output format
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

            // Setup XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(inputStreamXSL));

            // Resulting SAX events (the generated FO) must be piped through to FOP
            Result res = new SAXResult(fop.getDefaultHandler());

            // Start XSLT transformation and FOP processing
            // That's where the XML is first transformed to XSL-FO and then
            // PDF is created

            StringReader reader = new StringReader(xmlString);
            //Si se requiere de fuente string usando StringReader
            transformer.transform(new javax.xml.transform.stream.StreamSource(reader), res);

            if(!sendFile(SFTPDir_pdf, nombrePdf, out.toByteArray())){
                salida = false;
                log.info("PDF "+nombrePdf+" OK enviado a "+SFTPDir_pdf);
            }else {
                log.info("PDF "+nombrePdf+" NOK enviado a "+SFTPDir_pdf);
                salida = true;
            }
        } catch (Exception ex) {
            log.error("Error al generar PDF ("+nombrePdf+") en repositorio "+ SFTPDir_pdf +" y xsl en "+SFTPDir_xsl+" :", ex);
            log.info("PDF FOP "+nombrePdf+" NOK enviado a "+SFTPDir_pdf);
            salida = false;
        }finally {
            out.close();
        }
        return salida;
    }

    private void openSessionSFTP(Repositorios repo) throws Exception{
        int SFTPPORT = 22;
        session = null;
        try {
            JSch jsch = new JSch();
            String SFTPHOST = repo.getServidor();
            String SFTPUSER = repo.getUsuario();
            String SFTPPASS = repo.getPassword();

            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
            session.setPassword(SFTPPASS);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            setChannelSftp((ChannelSftp) channel);

            //log.debug("Ruta completa a revisar --> " + makeDir);

        } catch (JSchException ex) {
            log.error(ex);
            throw ex;
        }
    }

    private void closeSessionSFTP(){
        getSession().disconnect();
        getChannelSftp().exit();
    }

    private void makeDirectory(String makeDir) throws Exception{

        try {
            //log.debug("Ruta completa a revisar --> " + makeDir);

            String[] subCarpetas = makeDir.split("/");

            String varAux = "";
            for(String elemento : subCarpetas){
                //log.debug("Directorio en revision --> " + varAux);
                if(!elemento.isEmpty()){
                    varAux += "/".concat(elemento);
                    //log.debug("Directorio en revision (2) --> " + varAux);
                    SftpATTRS attrs=null;
                    try {
                        attrs = getChannelSftp().stat(varAux);
                        //log.debug("Valor de attrs --> " + attrs);
                    } catch (Exception e) {
                        //log.error("directorio" + varAux + " no encontrado.");
                    }
                    if (attrs == null) {
                        //log.debug("Creando directorio " + varAux);
                        getChannelSftp().mkdir(varAux);
                    }
                }
            }

        } catch (Exception ex) {
            log.error(ex);
            throw ex;
        }
    }

    private byte[] getFile(String SFTPWORKINGDIR, String fileName) throws Exception{

        try {
            try {
                openSessionSFTP(repo);
            }catch(Exception e){
                log.error(e);
            }
            getChannelSftp().cd(SFTPWORKINGDIR);
            byte[] buffer = new byte[1024];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try (BufferedInputStream bis = new BufferedInputStream(getChannelSftp().get(fileName))) {
                int readCount;
                while ((readCount = bis.read(buffer)) > 0) {
                    baos.write(buffer, 0, readCount);
                }
            }

            return baos.toByteArray();
        } catch (SftpException | IOException ex) {
            log.error(ex);
            throw new Exception("Error al intentar obtener el archivo.");
        }

    }

    private boolean existsFile(String SFTPWORKINGDIR, String fileName){
        boolean salida = false;
        try {
            try {
                openSessionSFTP(repo);
            }catch(Exception e){
                log.error(e);
            }
            getChannelSftp().cd(SFTPWORKINGDIR);
            if(getChannelSftp().get(fileName)!=null){
                salida = true;
            }else{
                salida = false;
            }
        } catch (SftpException e) {
            salida = false;
        }
        return salida;
    }

    private boolean sendFile(String SFTPWORKINGDIR, String nameFile, byte[] byteArrInputStream) {
        boolean archivoEscrito = false;
        try {
            try {
                openSessionSFTP(repo);
            }catch(Exception e){
                log.error(e);
            }
            //log.debug("Ruta en que se grabará nuevo archivo --> " + SFTPWORKINGDIR);
            getChannelSftp().cd(SFTPWORKINGDIR);
            InputStream is = new ByteArrayInputStream(byteArrInputStream);
            //log.debug("Archivo que se grabará --> " + nameFile);
            getChannelSftp().put(is, nameFile);
            archivoEscrito = true;
        } catch (SftpException ex) {
            log.error(ex);
            archivoEscrito = false;
        }
        return archivoEscrito;
    }

    private ChannelSftp getChannelSftp() {
        return channelSftp;
    }

    private void setChannelSftp(ChannelSftp channelSftp) {
        this.channelSftp = channelSftp;
    }

    private Session getSession() {
        return session;
    }

    private void setSession(Session session) {
        this.session = session;
    }

    private Repositorios getRepo() {
        return repo;
    }

    private void setRepo(Repositorios repo) {
        this.repo = repo;
    }

    private static final Logger log = Logger.getLogger(GatewayJob.class);
}
