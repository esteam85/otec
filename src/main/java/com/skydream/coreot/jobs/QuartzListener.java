package com.skydream.coreot.jobs;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;

import com.skydream.coreot.util.Config;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.ee.servlet.QuartzInitializerListener;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@WebListener
public class QuartzListener extends QuartzInitializerListener {

    private static final Logger LOG = LoggerFactory.getLogger(QuartzListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        super.contextInitialized(sce);
        ServletContext ctx = sce.getServletContext();
        StdSchedulerFactory factory = (StdSchedulerFactory) ctx.getAttribute(QUARTZ_FACTORY_KEY);
        try {
            Scheduler scheduler = factory.getScheduler();
            JobDetail job = JobBuilder.newJob(SimpleJob.class).build();
            
            JobDetail jobExpirarTokens = JobBuilder.newJob(ExpirarTokensJob.class)
                    .withIdentity("job1", "group1").build();

            JobDetail jobCargarIndicadores = JobBuilder.newJob(IndicadoresEconomicosJob.class)
                    .withIdentity("job2", "group2").build();
            Trigger trigger1 = TriggerBuilder.newTrigger().withIdentity("cronTrigger1", "group1").withSchedule(
                    CronScheduleBuilder.cronSchedule("0 0/3 * 1/1 * ? *")
            ).build();

            Trigger trigger2 = TriggerBuilder.newTrigger().withIdentity("cronTrigger2","group2").withSchedule(
                    CronScheduleBuilder.cronSchedule("0 0/30 * 1/1 * ? *"))
                    .build();

            JobDetail jobGeneraPdfActasPagadas = JobBuilder.newJob(PdfActaPagadaJob.class)
                    .withIdentity("job3", "group3").build();
            Trigger trigger3 = TriggerBuilder.newTrigger().withIdentity("cronTrigger3","group3").withSchedule(
                    CronScheduleBuilder.cronSchedule(Config.getINSTANCE().getSchedulerjobActas()))
                    .build();

            JobDetail jobEnvioCorreosGe = JobBuilder.newJob(CorreoGestionEconomicaJob.class)
                    .withIdentity("job4", "group4").build();
            Trigger trigger4 = TriggerBuilder.newTrigger().withIdentity("cronTrigger4","group4").withSchedule(
                    CronScheduleBuilder.cronSchedule("0 0/60 * * * ?"))
                    .build();

            JobDetail jobReportes = JobBuilder.newJob(ReportesJob.class)
                    .withIdentity("job5", "group5").build();
            Trigger trigger5 = TriggerBuilder.newTrigger().withIdentity("cronTrigger5","group5").withSchedule(
                    CronScheduleBuilder.cronSchedule(Config.getINSTANCE().getSchedulerjobReportes()))
                    .build();

            JobDetail jobVistas = JobBuilder.newJob(VistasMaterializadasJob.class)
                    .withIdentity("job6", "group6").build();
            Trigger trigger6 = TriggerBuilder.newTrigger().withIdentity("cronTrigger6","group6").withSchedule(
                    CronScheduleBuilder.cronSchedule(Config.getINSTANCE().getSchedulerjobVistas()))
                    .build();

            scheduler.scheduleJob(jobExpirarTokens, trigger1);
            scheduler.scheduleJob(jobCargarIndicadores, trigger2);
            scheduler.scheduleJob(jobGeneraPdfActasPagadas, trigger3);
//            scheduler.scheduleJob(jobEnvioCorreosGe, trigger4);
            scheduler.scheduleJob(jobReportes, trigger5);
            scheduler.scheduleJob(jobVistas, trigger6);
            scheduler.start();


        } catch (Exception e) {
            LOG.error("Ocurri\u00f3 un error al calendarizar el trabajo", e);
        }
    }
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        super.contextDestroyed(sce);
    }
}