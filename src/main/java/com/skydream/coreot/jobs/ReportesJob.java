package com.skydream.coreot.jobs;

import com.jcraft.jsch.*;
import com.jcraft.jsch.Session;
import com.skydream.coreot.pojos.Repositorios;
import com.skydream.coreot.util.Config;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;
import org.hibernate.HibernateException;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.*;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Genera reportes automatizados envio por correo y guardados en el repositorio FTP
 */
public class ReportesJob implements Job {

    private ChannelSftp channelSftp;
    private Session session;
    private Repositorios repo;
    private String rutaReporte;
    private String mail;
    private String passMail;

    public void execute(JobExecutionContext ctx) throws JobExecutionException {

        try {

            log.info("Inciando Job generador de Reportes.");

            int reporteId;
            String reporteNombre;

            int hojaId;
            String hojaNombre;
            String hojaColumnas;
            String hojaQuery;

            int usuarioId;
            String usuarioMail;

            XSSFWorkbook workbook;

            setRutaReporte("/Reportes/");
            setMail(Config.getINSTANCE().getMailReporte());
            setPassMail(Config.getINSTANCE().getPassReporte());

            repo = obtenerRepositorioBase();

            String SFTPDir_repo = repo.getUrl()+ getRutaReporte();

            List<Object[]> listaReportes = listarReportesConfigurados();

            if(listaReportes.size() > 0){

                openSessionSFTP(repo);

                makeDirectory(SFTPDir_repo);

                for(Object[] obj : listaReportes){

                    reporteId = (Integer) obj[0];
                    reporteNombre = (String) obj[1];
                    workbook = new XSSFWorkbook();

                    List<Object[]> listaHojasReporte = obtenerHojasPorReporte(reporteId);

                    if(listaHojasReporte.size() > 0){

                        for(Object[] obj2 : listaHojasReporte){ //rh.rep_hoja_id, h.nombre, h.columnas, h.query

                            hojaId = (Integer) obj2[0];
                            hojaNombre = (String) obj2[1];
                            hojaColumnas = (String) obj2[2];
                            hojaQuery = (String) obj2[3];

                            List<Object[]> hojaReporte = obtenerHojaReporte(hojaId);

                            XSSFSheet sheet1 = workbook.createSheet(hojaNombre);

                            writeSheetOptimized(workbook, sheet1, hojaColumnas, hojaReporte);

                        }

                    }

                    reporteNombre = reporteNombre + '_' + getTiempoActual() + ".xlsx";

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    workbook.write(baos);

                    byte[] bytes  = baos.toByteArray();

                    guardarReporteEnFTP(repo,reporteNombre,bytes);

                    List<Object[]> listaUsuariosReporte = obtenerUsuariosReportes(reporteId);

                    if(listaUsuariosReporte.size() > 0){

                        for(Object[] obj3 : listaUsuariosReporte){

                            usuarioId = (Integer) obj3[0];
                            usuarioMail = (String) obj3[1];

                            actualizarUsuarioReporteEjecucion(usuarioId, reporteId);

                            enviarReportePorMail(usuarioMail,reporteNombre,reporteNombre,null,reporteNombre,bytes);
                        }

                    }

                }

                closeSessionSFTP();

            }

            log.info("Finalizando Job generador de Reportes.");

        } catch (Exception ex) {
            log.error(ex);
        }
    }

    private List<Object[]> listarReportesConfigurados () throws Exception {

        GatewayJob gateway = new GatewayJob();
        List<Object[]> listaReportesConfigurados = new ArrayList<>();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                listaReportesConfigurados = gateway.listarReportesConfigurados();

            } catch (HibernateException ex) {
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return listaReportesConfigurados;
    }

    private List<Object[]> obtenerUsuariosReportes (int reporteId) throws Exception {

        GatewayJob gateway = new GatewayJob();
        List<Object[]> listaUsuariosReportes = new ArrayList<>();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                listaUsuariosReportes = gateway.obtenerUsuariosReportes(reporteId);

            } catch (HibernateException ex) {
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return listaUsuariosReportes;
    }

    private List<Object[]> obtenerHojasPorReporte (int reporteId) throws Exception {

        GatewayJob gateway = new GatewayJob();
        List<Object[]> listaHojas = new ArrayList<>();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                listaHojas = gateway.obtenerHojasPorReporte(reporteId);

            } catch (HibernateException ex) {
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return listaHojas;
    }

    private List<Object[]> obtenerHojaReporte (int hojaId) throws Exception {

        GatewayJob gateway = new GatewayJob();
        List<Object[]> resultado = new ArrayList<>();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                resultado = gateway.obtenerHojaReporte(hojaId);

            } catch (HibernateException ex) {
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return resultado;
    }

    private int actualizarUsuarioReporteEjecucion (int usuarioId, int reporteId) throws Exception {

        GatewayJob gateway = new GatewayJob();

        int salida = 0;

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                salida = gateway.actualizarUsuarioReporteEjecucion(usuarioId, reporteId);
                gateway.commit();

            } catch (HibernateException ex) {
                gateway.rollback();
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return salida;
    }

    public void writeSheetOptimized(XSSFWorkbook workbook, XSSFSheet sheet, String titulos, List<Object[]> data){

        Row rowTitle = sheet.createRow(0);
        int cellnumTitle = 0;

        String[] listaTitulos = titulos.split(";");

        for(String obj : listaTitulos){
            Cell cell = rowTitle.createCell(cellnumTitle++);
            cell.setCellValue(obj);
        }

        int rownum = 1;

        XSSFCreationHelper createHelper = workbook.getCreationHelper();
        XSSFCellStyle cellStyle         = workbook.createCellStyle();
        XSSFDataFormat xssfDataFormat = createHelper.createDataFormat();

        for (Object[] objList : data) {
            Row row = sheet.createRow(rownum++);
            int cellnum = 0;
            for (Object obj : objList) {
                Cell cell = row.createCell(cellnum++);
                String formato = "";

                if(obj instanceof String){
                    cell.setCellValue((String)obj);
                }else if(obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }else if(obj instanceof Date){
                    cell.setCellValue(""+obj);
                    //formato = "yyyy-MM-dd";
                }else if(obj instanceof  Double){
                    cell.setCellValue((Double) obj);
                    formato = "#,##0.000";
                }else if(obj instanceof BigInteger){
                    cell.setCellValue(((BigInteger) obj).longValue());
                }else {
                    if(!(""+obj).trim().equalsIgnoreCase("null")) cell.setCellValue(""+obj);
                }
                cellStyle.setDataFormat(xssfDataFormat.getFormat(formato));
                cell.setCellStyle(cellStyle);

            }
        }
    }

    private Repositorios obtenerRepositorioBase () throws Exception {

        GatewayJob gateway = new GatewayJob();
        Repositorios repo = null;
        Config config = Config.getINSTANCE();

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                repo = gateway.obtenerRepositorio(config.getNombreRepositorioBase());

            } catch (HibernateException ex) {
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return repo;
    }

    private boolean guardarReporteEnFTP(Repositorios repo, String nombreReporte, byte[] bytes)  throws IOException, Exception {

        boolean salida = false;

        String SFTPDir_repo = repo.getUrl()+ getRutaReporte();

        try {

            if(!sendFile(SFTPDir_repo, nombreReporte, bytes)){
                salida = false;
                log.info("Reporte "+nombreReporte+" OK enviado a "+SFTPDir_repo);
            }else {
                log.info("Reporte "+nombreReporte+" NOK enviado a "+SFTPDir_repo);
                salida = true;
            }
        } catch (Exception ex) {
            log.error("Error al guardar reporte ("+nombreReporte+") en repositorio "+ SFTPDir_repo +" :", ex);
            log.info("Reporte "+nombreReporte+" NOK enviado a "+SFTPDir_repo);
            salida = false;
        }
        return salida;
    }

    private void openSessionSFTP(Repositorios repo) throws Exception{
        int SFTPPORT = 22;
        session = null;
        try {
            JSch jsch = new JSch();
            String SFTPHOST = repo.getServidor();
            String SFTPUSER = repo.getUsuario();
            String SFTPPASS = repo.getPassword();

            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
            session.setPassword(SFTPPASS);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            setChannelSftp((ChannelSftp) channel);

            //log.debug("Ruta completa a revisar --> " + makeDir);

        } catch (JSchException ex) {
            log.error(ex);
            throw ex;
        }
    }

    private void closeSessionSFTP(){
        getSession().disconnect();
        getChannelSftp().exit();
    }

    private void makeDirectory(String makeDir) throws Exception{

        try {
            //log.debug("Ruta completa a revisar --> " + makeDir);

            String[] subCarpetas = makeDir.split("/");

            String varAux = "";
            for(String elemento : subCarpetas){
                //log.debug("Directorio en revision --> " + varAux);
                if(!elemento.isEmpty()){
                    varAux += "/".concat(elemento);
                    //log.debug("Directorio en revision (2) --> " + varAux);
                    SftpATTRS attrs=null;
                    try {
                        attrs = getChannelSftp().stat(varAux);
                        //log.debug("Valor de attrs --> " + attrs);
                    } catch (Exception e) {
                        //log.error("directorio" + varAux + " no encontrado.");
                    }
                    if (attrs == null) {
                        //log.debug("Creando directorio " + varAux);
                        getChannelSftp().mkdir(varAux);
                    }
                }
            }

        } catch (Exception ex) {
            log.error(ex);
            throw ex;
        }
    }

    private boolean sendFile(String SFTPWORKINGDIR, String nameFile, byte[] byteArrInputStream) {
        boolean archivoEscrito = false;
        try {
            try {
                openSessionSFTP(repo);
            }catch(Exception e){
                log.error(e);
            }
            //log.debug("Ruta en que se grabará nuevo archivo --> " + SFTPWORKINGDIR);
            getChannelSftp().cd(SFTPWORKINGDIR);
            InputStream is = new ByteArrayInputStream(byteArrInputStream);
            //log.debug("Archivo que se grabará --> " + nameFile);
            getChannelSftp().put(is, nameFile);
            archivoEscrito = true;
        } catch (SftpException ex) {
            log.error(ex);
            archivoEscrito = false;
        }
        return archivoEscrito;
    }

    public boolean enviarReportePorMail(String emailDestino, String asunto, String cuerpoMensaje, List<String> destinatariosACopiar, String nombreReporte, byte[] bytes) {

        Properties props = new Properties();
        props.put("mail.smtp.host", "mail.its.cl");
        props.put("mail.smtp.socketFactory.port", "587");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "587");

        javax.mail.Session session = null;
        try {
            session = javax.mail.Session.getDefaultInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(getMail(), getPassMail());
                        }
                    });
        } catch (Throwable e) {
            e.printStackTrace();
        }

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(getMail()));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(emailDestino));
            if(destinatariosACopiar!=null){
                for(String destinatario : destinatariosACopiar){
                    message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(destinatario));
                }
            }
            message.setSubject(asunto);

            Multipart multipart = new MimeMultipart();

            DataSource ds = null;

            MimeBodyPart attachPart = new MimeBodyPart();

            ds = new ByteArrayDataSource(bytes, "application/excel");
            DataHandler dh = new DataHandler(ds);
            attachPart.setDataHandler(dh);
            attachPart.setFileName(nombreReporte);
            //attachPart.setText(cuerpoMensaje);
            message.setText(cuerpoMensaje);
            //String attachFile = "/Users/FParedes/Desktop/OTEC/Reportes/reporte.xlsx";
            //attachPart.attachFile(attachFile);
            multipart.addBodyPart(attachPart);
            message.setContent(multipart);

            Transport.send(message);
        } catch (MessagingException e) {
            log.error(e);
        } catch (Exception e) {
            log.error(e);
        }

        return true;
    }

    public String getTiempoActual(){
        DateFormat df = new SimpleDateFormat("MMddyyyy_HHmmss");
        Date today = Calendar.getInstance().getTime();
        return df.format(today);
    }

    private ChannelSftp getChannelSftp() {
        return channelSftp;
    }

    private void setChannelSftp(ChannelSftp channelSftp) {
        this.channelSftp = channelSftp;
    }

    private Session getSession() {
        return session;
    }

    private void setSession(Session session) {
        this.session = session;
    }

    private Repositorios getRepo() {
        return repo;
    }

    private void setRepo(Repositorios repo) {
        this.repo = repo;
    }

    private String getRutaReporte () {return rutaReporte;}

    private void setRutaReporte (String rutaReporte) { this.rutaReporte = rutaReporte; }

    public String getPassMail() {
        return passMail;
    }

    public void setPassMail(String passMail) {
        this.passMail = passMail;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    private static final Logger log = Logger.getLogger(GatewayJob.class);
}
