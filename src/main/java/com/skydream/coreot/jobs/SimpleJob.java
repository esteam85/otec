package com.skydream.coreot.jobs;

import com.skydream.coreot.dao.LoginLogoutDAO;
import java.util.ArrayList;
import java.util.List;
import com.skydream.coreot.pojos.Usuarios;
import com.skydream.coreot.pojos.WorkflowEventosEjecucion;
import com.skydream.coreot.util.SendMail;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by christian on 28-09-15.
 */

public class SimpleJob implements Job {

    public void execute(JobExecutionContext ctx) throws JobExecutionException {

        try {
            //List<WorkflowEventosEjecucion> listadoEventos = obtenerRegistrosPrueba();
            //enviarYRegistrarNotificaciones(listadoEventos);
            boolean aux = validarInicioSesion("admin", "admin");
            if(!aux){
                enviarCorreo();
            }            
        } catch (Exception ex) {
            Logger.getLogger(SimpleJob.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void enviarYRegistrarNotificaciones(List<WorkflowEventosEjecucion> listadoEventos) {

//        SendMail sendMail = SendMail.getINSTANCE();
//        boolean envioOK = sendMail.enviarMail("christian.espinoza@skydream.cl", "*** Notificacion ***", "Correeeeee");
//        if(envioOK){
//            System.out.printf(new Locale("es", "MX"), "%tc Se envio correo...%n", new java.util.Date());
//        }else{
//            System.out.printf(new Locale("es", "MX"), "%tc Noooo Se envio correo...%n", new java.util.Date());
//        }
//        System.out.printf(new Locale("es", "MX"), "%tc NOTIFICACION %n", new java.util.Date());
    }

    private List<WorkflowEventosEjecucion> obtenerRegistrosPrueba() {
        List<WorkflowEventosEjecucion> listadoEventos = new ArrayList<>();

        // En este gateway crea los metodos con las querys
        //GatewayJob gatewayJob = new GatewayJob();
        //listadoEventos = gatewayJob.xxxxxxxx();
        //Dummy
        WorkflowEventosEjecucion workflowEvent1 = new WorkflowEventosEjecucion();
        workflowEvent1.setUsuarioId(1);
        listadoEventos.add(workflowEvent1);
        WorkflowEventosEjecucion workflowEvent2 = new WorkflowEventosEjecucion();
        workflowEvent2.setUsuarioId(2);
        listadoEventos.add(workflowEvent2);

        return listadoEventos;
    }

    private Boolean validarInicioSesion(String usuario, String clave) throws Exception {
        Boolean retorno = false;
        try {
            Usuarios usr = LoginLogoutDAO.validarLoginYRetornarUsuario(usuario, clave);
            if (usr != null) {
                retorno = true;
            }
        } catch (Exception ex) {
            throw new Exception("Error al validar credenciales de usuario.");
        }
        return retorno;

    }
    
    private void enviarCorreo() {

        SendMail sendMail = SendMail.getINSTANCE();
        boolean envioOK = sendMail.enviarMail("roberto.ramirez@its.cl", "Caida Sistema OTEC", "Caida Sistema OTEC", null);
        
        
        //boolean envioOK = sendMail.enviarMail("christian.espinoza@skydream.cl", "*** Notificacion ***", "Correeeeee");
        if(envioOK){
            System.out.printf(new Locale("es", "MX"), "%tc Se envio correo para reiniciar servicios OTEC%n", new java.util.Date());
        }
        else{
            System.out.printf(new Locale("es", "MX"), "%tc Noooo Se envio correo...%n", new java.util.Date());
        }
        //System.out.printf(new Locale("es", "MX"), "%tc NOTIFICACION %n", new java.util.Date());
    }    
    
    

}
