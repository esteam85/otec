package com.skydream.coreot.jobs;

import com.jcraft.jsch.*;
import com.jcraft.jsch.Session;
import com.skydream.coreot.pojos.Repositorios;
import com.skydream.coreot.util.Config;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;
import org.hibernate.HibernateException;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Actualiza vistas materializadas
 */
public class VistasMaterializadasJob implements Job {

    public void execute(JobExecutionContext ctx) throws JobExecutionException {

        try {

            log.info("Inciando Job generador de Vistas Materializadas.");

            String salida = actualizarVistasMaterializadas();


            log.info("Finalizando " + salida + "Job Vistas Materializadas.");

        } catch (Exception ex) {
            log.error(ex);
        }
    }

    private String actualizarVistasMaterializadas () throws Exception {

        GatewayJob gateway = new GatewayJob();
        String resultado ;

        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {

                resultado = gateway.actualizarVistasMaterializadas();

            } catch (HibernateException ex) {
                throw ex;
            }
        } finally {
            gateway.cerrarSesion();
        }
        return resultado;
    }

    private static final Logger log = Logger.getLogger(GatewayJob.class);
}
