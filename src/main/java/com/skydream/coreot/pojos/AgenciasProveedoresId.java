package com.skydream.coreot.pojos;
// Generated 22-sep-2016 17:25:15 by Hibernate Tools 4.3.1



/**
 * AgenciasProveedoresId generated by hbm2java
 */
public class AgenciasProveedoresId  implements java.io.Serializable {


    private int agenciaId;
    private int proveedorId;
    private int nivel;

    public AgenciasProveedoresId() {
    }

    public AgenciasProveedoresId(int agenciaId, int proveedorId, int nivel) {
        this.agenciaId = agenciaId;
        this.proveedorId = proveedorId;
        this.nivel = nivel;
    }

    public int getAgenciaId() {
        return this.agenciaId;
    }

    public void setAgenciaId(int agenciaId) {
        this.agenciaId = agenciaId;
    }
    public int getProveedorId() {
        return this.proveedorId;
    }

    public void setProveedorId(int proveedorId) {
        this.proveedorId = proveedorId;
    }
    public int getNivel() {
        return this.nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }


    public boolean equals(Object other) {
        if ( (this == other ) ) return true;
        if ( (other == null ) ) return false;
        if ( !(other instanceof AgenciasProveedoresId) ) return false;
        AgenciasProveedoresId castOther = ( AgenciasProveedoresId ) other;

        return (this.getAgenciaId()==castOther.getAgenciaId())
                && (this.getProveedorId()==castOther.getProveedorId())
                && (this.getNivel()==castOther.getNivel());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + this.getAgenciaId();
        result = 37 * result + this.getProveedorId();
        result = 37 * result + this.getNivel();
        return result;
    }


}


