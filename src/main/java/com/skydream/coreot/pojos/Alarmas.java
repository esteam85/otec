package com.skydream.coreot.pojos;
// Generated 03-ago-2015 20:24:21 by Hibernate Tools 4.3.1

import com.skydream.coreot.dao.GatewayDAO;
import com.skydream.coreot.dao.GenericDAO;
import java.util.Map;

/**
 * Alarmas generated by hbm2java
 */
public class Alarmas implements java.io.Serializable, ObjetoCoreOT {

    private int id;
    private TipoAlarma tipoAlarma;
    private String nombre;
    private Integer tiempo;

    public Alarmas() {
    }

    public Alarmas(int id) {
        this.id = id;
    }

    public Alarmas(TipoAlarma tipoAlarma) {
        this.tipoAlarma = tipoAlarma;
    }

    public Alarmas(int id, TipoAlarma tipoAlarma, String nombre, Integer tiempo) {
        this.id = id;
        this.tipoAlarma = tipoAlarma;
        this.nombre = nombre;
        this.tiempo = tiempo;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TipoAlarma getTipoAlarma() {
        return this.tipoAlarma;
    }

    public void setTipoAlarma(TipoAlarma tipoAlarma) {
        this.tipoAlarma = tipoAlarma;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getTiempo() {
        return this.tiempo;
    }

    public void setTiempo(Integer tiempo) {
        this.tiempo = tiempo;
    }

    @Override
    public void setearObjetoDesdeMap(Map parametros, int contador) throws Exception {
        Map params = parametros;
        contador++;
        if (params.containsKey("id")) {
            this.setId(Integer.parseInt(params.get("id").toString()));
        }
        if (params.containsKey("tipoAlarma") && contador <= 1) {
            Map tipo = (Map) parametros.get("tipoAlarma");
            this.getTipoAlarma().setearObjetoDesdeMap(tipo, contador);
        }
        if (params.containsKey("nombre")) {
            this.setNombre(params.get("nombre").toString());
        }
        if (params.containsKey("tiempo")) {
            this.setTiempo((Integer) params.get("tiempo"));
        }
    }

    @Override
    public GatewayDAO obtenerGatewayDAO() {
        return new GenericDAO();
    }

}
