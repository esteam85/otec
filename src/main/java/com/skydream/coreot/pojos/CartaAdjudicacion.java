package com.skydream.coreot.pojos;

import java.math.BigInteger;

/**
 * Created by christian on 17-12-15.
 */
public class CartaAdjudicacion implements java.io.Serializable {


    private int id;
    private int adjuntoId;
    private long numero;
    private long numeroPedido;
    private String materia;
    private String numeroCarta;

    public CartaAdjudicacion() {
    }


    public CartaAdjudicacion(int id, int adjuntoId, long numero, long numeroPedido, String numeroCarta) {
        this.id = id;
        this.adjuntoId = adjuntoId;
        this.numero = numero;
        this.numeroPedido = numeroPedido;
        this.numeroCarta = numeroCarta;
    }
    public CartaAdjudicacion(int id, int adjuntoId, long numero, long numeroPedido, String materia, String numeroCarta) {
        this.id = id;
        this.adjuntoId = adjuntoId;
        this.numero = numero;
        this.numeroPedido = numeroPedido;
        this.materia = materia;
        this.numeroCarta = numeroCarta;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getAdjuntoId() {
        return this.adjuntoId;
    }

    public void setAdjuntoId(int adjuntoId) {
        this.adjuntoId = adjuntoId;
    }
    public long getNumero() {
        return this.numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }
    public long getNumeroPedido() {
        return this.numeroPedido;
    }

    public void setNumeroPedido(long numeroPedido) {
        this.numeroPedido = numeroPedido;
    }
    public String getMateria() {
        return this.materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }
    public String getNumeroCarta() {
        return this.numeroCarta;
    }

    public void setNumeroCarta(String numeroCarta) {
        this.numeroCarta = numeroCarta;
    }




}

