package com.skydream.coreot.pojos;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.math.BigInteger;

/**
 * Created by christian on 17-12-15.
 */
@JsonIgnoreProperties({"idPmo", "lp", "pep2"})

public class CartaAdjudicacionAux {

    private String fechaAdjudicacion;
    private long numero;
    private String numeroCarta;
    private long numeroPedido;
    private String materia;
    private String asunto;
    private String idPmo;
    private String lp;
    private String pep2;


    public String getFechaAdjudicacion() {
        return fechaAdjudicacion;
    }

    public void setFechaAdjudicacion(String fechaAdjudicacion) {
        this.fechaAdjudicacion = fechaAdjudicacion;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public String getNumeroCarta() {
        return numeroCarta;
    }

    public void setNumeroCarta(String numeroCarta) {
        this.numeroCarta = numeroCarta;
    }

    public long getNumeroPedido() {
        return numeroPedido;
    }

    public void setNumeroPedido(long numeroPedido) {
        this.numeroPedido = numeroPedido;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getIdPmo() {
        return idPmo;
    }

    public void setIdPmo(String idPmo) {
        this.idPmo = idPmo;
    }

    public String getLp() {
        return lp;
    }

    public void setLp(String lp) {
        this.lp = lp;
    }

    public String getPep2() {
        return pep2;
    }

    public void setPep2(String pep2) {
        this.pep2 = pep2;
    }
}
