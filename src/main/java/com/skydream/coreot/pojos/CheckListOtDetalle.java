package com.skydream.coreot.pojos;
// Generated 23-jun-2016 9:11:09 by Hibernate Tools 4.3.1



/**
 * CheckListOtDetalle generated by hbm2java
 */
public class CheckListOtDetalle  implements java.io.Serializable {

    private  int id;
    private CheckListOt checkListOt;
    private LibroObras libroObra;
    private String item;
    private String observacion;
    private int opcionId;
    private String opcionNombre;
    private int puntuacion;

    public CheckListOtDetalle() {
    }

    public CheckListOtDetalle(int id, CheckListOt checkListOt, LibroObras libroObra, String item, String observacion, int opcionId, String opcionNombre, int puntuacion) {
        this.id = id;
        this.checkListOt = checkListOt;
        this.libroObra = libroObra;
        this.item = item;
        this.observacion = observacion;
        this.opcionId = opcionId;
        this.opcionNombre = opcionNombre;
        this.puntuacion = puntuacion;
    }


    public String getItem() {
        return this.item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getObservacion() {
        return this.observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    public int getOpcionId() {
        return this.opcionId;
    }

    public void setOpcionId(int opcionId) {
        this.opcionId = opcionId;
    }
    public String getOpcionNombre() {
        return this.opcionNombre;
    }

    public void setOpcionNombre(String opcionNombre) {
        this.opcionNombre = opcionNombre;
    }


    public CheckListOt getCheckListOt() {
        return checkListOt;
    }

    public void setCheckListOt(CheckListOt checkListOt) {
        this.checkListOt = checkListOt;
    }

    public LibroObras getLibroObra() {
        return libroObra;
    }

    public void setLibroObra(LibroObras libroObra) {
        this.libroObra = libroObra;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }
}


