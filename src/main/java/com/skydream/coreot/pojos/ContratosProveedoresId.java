package com.skydream.coreot.pojos;
// Generated 29-ene-2016 16:40:54 by Hibernate Tools 4.3.1



/**
 * ContratosProveedoresId generated by hbm2java
 */
public class ContratosProveedoresId  implements java.io.Serializable {


    private int contratoId;
    private int proveedorId;
    private int regionId;

    public ContratosProveedoresId() {
    }

    public ContratosProveedoresId(int contratoId, int proveedorId, int regionId) {
        this.contratoId = contratoId;
        this.proveedorId = proveedorId;
        this.regionId = regionId;
    }

    public int getContratoId() {
        return this.contratoId;
    }

    public void setContratoId(int contratoId) {
        this.contratoId = contratoId;
    }

    public int getProveedorId() {
        return this.proveedorId;
    }

    public void setProveedorId(int proveedorId) {
        this.proveedorId = proveedorId;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }


    public boolean equals(Object other) {
        if ((this == other)) return true;
        if ((other == null)) return false;
        if (!(other instanceof ContratosProveedoresId)) return false;
        ContratosProveedoresId castOther = (ContratosProveedoresId) other;

        return (this.getContratoId() == castOther.getContratoId())
                && (this.getProveedorId() == castOther.getProveedorId())
                && (this.getRegionId()== castOther.getRegionId());
    }

    public int hashCode() {
        int result = 17;
        result = 37 * result + this.getContratoId();
        result = 37 * result + this.getProveedorId();
        result = 37 * result + this.getRegionId();
        return result;
    }

}


