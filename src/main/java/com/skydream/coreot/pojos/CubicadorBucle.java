package com.skydream.coreot.pojos;

import java.util.List;

/**
 * Created by Esteam on 19-08-16.
 */
public class CubicadorBucle {


    private Cubicador cubicador;
    private List<ServicioBucle> serviviosBucle;

    public CubicadorBucle(Cubicador cubicador, List<ServicioBucle> serviviosBucle) {
        this.cubicador = cubicador;
        this.serviviosBucle = serviviosBucle;
    }

    public Cubicador getCubicador() {
        return cubicador;
    }

    public void setCubicador(Cubicador cubicador) {
        this.cubicador = cubicador;
    }

    public List<ServicioBucle> getServiviosBucle() {
        return serviviosBucle;
    }

    public void setServiviosBucle(List<ServicioBucle> serviviosBucle) {
        this.serviviosBucle = serviviosBucle;
    }
}
