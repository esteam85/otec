package com.skydream.coreot.pojos;
// Generated 11-feb-2016 15:09:32 by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * CubicadorDetalleAdicionales generated by hbm2java
 */
public class CubicadorDetalleAdicionales  implements java.io.Serializable {


     private int id;
     private int cubicadorId;
     private int regionId;
     private Integer agenciaId;
     private Integer centralId;
     private Integer sitioId;
     private int servicioId;
     private Integer materialId;
     private int tipoMonedaId;
     private double cantidad;
     private Boolean validacionSistema;
     private double precio;
     private double total;
     private Date fechaCreacion;

    private Servicios servicios;
    private Alcances alcances;
    private Regiones regiones;
    private Agencias agencia;
    private TipoMoneda tipoMoneda;
    private Sitios sitios;
    private Materiales materiales;
    private Centrales centrales;

    public CubicadorDetalleAdicionales() {
    }

	
    public CubicadorDetalleAdicionales(int id, int cubicadorId, int regionId, int servicioId, int tipoMonedaId, double cantidad, double precio, double total) {
        this.id = id;
        this.cubicadorId = cubicadorId;
        this.regionId = regionId;
        this.servicioId = servicioId;
        this.tipoMonedaId = tipoMonedaId;
        this.cantidad = cantidad;
        this.precio = precio;
        this.total = total;
    }
    public CubicadorDetalleAdicionales(int id, int cubicadorId, int regionId, Integer agenciaId, Integer centralId, Integer sitioId, int servicioId, Integer materialId, int tipoMonedaId, double cantidad, Boolean validacionSistema, double precio, double total, Date fechaCreacion) {
       this.id = id;
       this.cubicadorId = cubicadorId;
       this.regionId = regionId;
       this.agenciaId = agenciaId;
       this.centralId = centralId;
       this.sitioId = sitioId;
       this.servicioId = servicioId;
       this.materialId = materialId;
       this.tipoMonedaId = tipoMonedaId;
       this.cantidad = cantidad;
       this.validacionSistema = validacionSistema;
       this.precio = precio;
       this.total = total;
       this.fechaCreacion = fechaCreacion;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public int getCubicadorId() {
        return this.cubicadorId;
    }
    
    public void setCubicadorId(int cubicadorId) {
        this.cubicadorId = cubicadorId;
    }
    public int getRegionId() {
        return this.regionId;
    }
    
    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }
    public Integer getAgenciaId() {
        return this.agenciaId;
    }
    
    public void setAgenciaId(Integer agenciaId) {
        this.agenciaId = agenciaId;
    }
    public Integer getCentralId() {
        return this.centralId;
    }
    
    public void setCentralId(Integer centralId) {
        this.centralId = centralId;
    }
    public Integer getSitioId() {
        return this.sitioId;
    }
    
    public void setSitioId(Integer sitioId) {
        this.sitioId = sitioId;
    }
    public int getServicioId() {
        return this.servicioId;
    }
    
    public void setServicioId(int servicioId) {
        this.servicioId = servicioId;
    }
    public Integer getMaterialId() {
        return this.materialId;
    }
    
    public void setMaterialId(Integer materialId) {
        this.materialId = materialId;
    }
    public int getTipoMonedaId() {
        return this.tipoMonedaId;
    }
    
    public void setTipoMonedaId(int tipoMonedaId) {
        this.tipoMonedaId = tipoMonedaId;
    }
    public double getCantidad() {
        return this.cantidad;
    }
    
    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }
    public Boolean getValidacionSistema() {
        return this.validacionSistema;
    }
    
    public void setValidacionSistema(Boolean validacionSistema) {
        this.validacionSistema = validacionSistema;
    }
    public double getPrecio() {
        return this.precio;
    }
    
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    public double getTotal() {
        return this.total;
    }
    
    public void setTotal(double total) {
        this.total = total;
    }
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }


    public Regiones getRegiones() {
        return regiones;
    }

    public void setRegiones(Regiones regiones) {
        this.regiones = regiones;
    }

    public Agencias getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencias agencia) {
        this.agencia = agencia;
    }

    public TipoMoneda getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(TipoMoneda tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public Sitios getSitios() {
        return sitios;
    }

    public void setSitios(Sitios sitios) {
        this.sitios = sitios;
    }

    public Materiales getMateriales() {
        return materiales;
    }

    public void setMateriales(Materiales materiales) {
        this.materiales = materiales;
    }

    public Centrales getCentrales() {
        return centrales;
    }

    public void setCentrales(Centrales centrales) {
        this.centrales = centrales;
    }

    public Servicios getServicios() {
        return servicios;
    }

    public void setServicios(Servicios servicios) {
        this.servicios = servicios;
    }

    public Alcances getAlcances() {
        return alcances;
    }

    public void setAlcances(Alcances alcances) {
        this.alcances = alcances;
    }
}


