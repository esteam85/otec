package com.skydream.coreot.pojos;
// Generated 10-feb-2016 18:25:01 by Hibernate Tools 4.3.1



/**
 * CubicadorDetalleUnificadoActasId generated by hbm2java
 */
public class CubicadorDetalleUnificadoActasId  implements java.io.Serializable {


     private int actaId;
     private int cubicadorDetalleId;

    public CubicadorDetalleUnificadoActasId() {
    }

    public CubicadorDetalleUnificadoActasId(int actaId, int cubicadorDetalleId) {
       this.actaId = actaId;
       this.cubicadorDetalleId = cubicadorDetalleId;
    }
   
    public int getActaId() {
        return this.actaId;
    }
    
    public void setActaId(int actaId) {
        this.actaId = actaId;
    }
    public int getCubicadorDetalleId() {
        return this.cubicadorDetalleId;
    }
    
    public void setCubicadorDetalleId(int cubicadorDetalleId) {
        this.cubicadorDetalleId = cubicadorDetalleId;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof CubicadorDetalleUnificadoActasId) ) return false;
		 CubicadorDetalleUnificadoActasId castOther = ( CubicadorDetalleUnificadoActasId ) other; 
         
		 return (this.getActaId()==castOther.getActaId())
 && (this.getCubicadorDetalleId()==castOther.getCubicadorDetalleId());
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + this.getActaId();
         result = 37 * result + this.getCubicadorDetalleId();
         return result;
   }   


}


