package com.skydream.coreot.pojos;

import com.skydream.coreot.dao.GatewayDAO;

import java.util.Map;

public class CubicadorServiciosServiciosUnidad  implements java.io.Serializable, ObjetoCoreOT  {


    private int id;
    private int cubicadorServicioBucleId;
    private int servicioUnidadId;
    private int cantidad;
    private double precio;
    private double total;

    public CubicadorServiciosServiciosUnidad() {
    }

    public CubicadorServiciosServiciosUnidad(int cubicadorServicioBucleId, int servicioUnidadId, int cantidad, double precio, double total, String direccion, String latitud, String longitud) {

        this.cubicadorServicioBucleId = cubicadorServicioBucleId;
        this.servicioUnidadId = servicioUnidadId;
        this.cantidad = cantidad;
        this.precio = precio;
        this.total = total;
    }

    public CubicadorServiciosServiciosUnidad(int cubicadorServicioBucleId, int servicioUnidadId, int cantidad, double precio, double total) {

        this.cubicadorServicioBucleId = cubicadorServicioBucleId;
        this.servicioUnidadId = servicioUnidadId;
        this.cantidad = cantidad;
        this.precio = precio;
        this.total = total;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getCubicadorServicioBucleId() {
        return this.cubicadorServicioBucleId;
    }

    public void setCubicadorServicioBucleId(int cubicadorServicioBucleId) {
        this.cubicadorServicioBucleId = cubicadorServicioBucleId;
    }
    public int getServicioUnidadId() {
        return this.servicioUnidadId;
    }

    public void setServicioUnidadId(int servicioUnidadId) {
        this.servicioUnidadId = servicioUnidadId;
    }
    public int getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    public double getPrecio() {
        return this.precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    public double getTotal() {
        return this.total;
    }

    public void setTotal(double total) {
        this.total = total;
    }


    @Override
    public GatewayDAO obtenerGatewayDAO() {
        return null;
    }

    @Override
    public void setearObjetoDesdeMap(Map parametros, int contador) throws Exception {

    }
}