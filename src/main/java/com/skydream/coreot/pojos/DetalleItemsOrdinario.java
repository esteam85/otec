package com.skydream.coreot.pojos;

//import com.sun.tools.javac.util.List;

/**
 * Created by jonathanRamirez on 11-01-16.
 */
public class DetalleItemsOrdinario {
    private CubicadorOrdinario cubicadorOrdinario;
    private DetalleCubicadorOrdinarioActas detalleCubicadorOrdinarioActas;

    public CubicadorOrdinario getCubicadorOrdinario() {
        return cubicadorOrdinario;
    }

    public void setCubicadorOrdinario(CubicadorOrdinario cubicadorOrdinario) {
        this.cubicadorOrdinario = cubicadorOrdinario;
    }

    public DetalleCubicadorOrdinarioActas getDetalleCubicadorOrdinarioActas() {
        return detalleCubicadorOrdinarioActas;
    }

    public void setDetalleCubicadorOrdinarioActas(DetalleCubicadorOrdinarioActas detalleCubicadorOrdinarioActas) {
        this.detalleCubicadorOrdinarioActas = detalleCubicadorOrdinarioActas;
    }
}
