package com.skydream.coreot.pojos;

//import com.sun.tools.javac.util.List;
/**
 * Created by jonathanRamirez on 11-01-16.
 */
public class DetalleServiciosUnificado {
    private CubicadorDetalle cubicadorDetalle;
    private CubicadorDetalleAdicionales cubicadorDetalleAdicionales;
    private CubicadorDetalleUnificadoActas cubicadorDetalleUnificadoActas;

    public CubicadorDetalle getCubicadorDetalle() {
        return cubicadorDetalle;
    }

    public void setCubicadorDetalle(CubicadorDetalle cubicadorDetalle) {
        this.cubicadorDetalle = cubicadorDetalle;
    }

    public CubicadorDetalleUnificadoActas getCubicadorDetalleUnificadoActas() {
        return cubicadorDetalleUnificadoActas;
    }

    public void setCubicadorDetalleUnificadoActas(CubicadorDetalleUnificadoActas cubicadorDetalleUnificadoActas) {
        this.cubicadorDetalleUnificadoActas = cubicadorDetalleUnificadoActas;
    }

    public CubicadorDetalleAdicionales getCubicadorDetalleAdicionales() {
        return cubicadorDetalleAdicionales;
    }

    public void setCubicadorDetalleAdicionales(CubicadorDetalleAdicionales cubicadorDetalleAdicionales) {
        this.cubicadorDetalleAdicionales = cubicadorDetalleAdicionales;
    }
}
