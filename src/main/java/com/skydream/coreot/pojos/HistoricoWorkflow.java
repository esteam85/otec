package com.skydream.coreot.pojos;
// Generated 24-jul-2015 14:51:22 by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * HistoricoWorkflow generated by hbm2java
 */
public class HistoricoWorkflow  implements java.io.Serializable {


     private int id;
     private EstadoWorkflow estadoWorkflow;
     private EventosAcciones eventosAcciones;
     private Ot ot;
     private Usuarios usuarios;
     private Date fechaInicio;
     private Date fechaTermino;
     private String descripcion;

    public HistoricoWorkflow() {
    }

	
    public HistoricoWorkflow(int id, EstadoWorkflow estadoWorkflow, EventosAcciones eventosAcciones, Ot ot, Usuarios usuarios) {
        this.id = id;
        this.estadoWorkflow = estadoWorkflow;
        this.eventosAcciones = eventosAcciones;
        this.ot = ot;
        this.usuarios = usuarios;
    }
    public HistoricoWorkflow(int id, EstadoWorkflow estadoWorkflow, EventosAcciones eventosAcciones, Ot ot, Usuarios usuarios, Date fechaInicio, Date fechaTermino, String descripcion) {
       this.id = id;
       this.estadoWorkflow = estadoWorkflow;
       this.eventosAcciones = eventosAcciones;
       this.ot = ot;
       this.usuarios = usuarios;
       this.fechaInicio = fechaInicio;
       this.fechaTermino = fechaTermino;
       this.descripcion = descripcion;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public EstadoWorkflow getEstadoWorkflow() {
        return this.estadoWorkflow;
    }
    
    public void setEstadoWorkflow(EstadoWorkflow estadoWorkflow) {
        this.estadoWorkflow = estadoWorkflow;
    }
    public EventosAcciones getEventosAcciones() {
        return this.eventosAcciones;
    }
    
    public void setEventosAcciones(EventosAcciones eventosAcciones) {
        this.eventosAcciones = eventosAcciones;
    }
    public Ot getOt() {
        return this.ot;
    }
    
    public void setOt(Ot ot) {
        this.ot = ot;
    }
    public Usuarios getUsuarios() {
        return this.usuarios;
    }
    
    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }
    public Date getFechaInicio() {
        return this.fechaInicio;
    }
    
    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }
    public Date getFechaTermino() {
        return this.fechaTermino;
    }
    
    public void setFechaTermino(Date fechaFin) {
        this.fechaTermino = fechaTermino;
    }
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }




}


