package com.skydream.coreot.pojos;
// Generated 10-feb-2016 18:09:34 by Hibernate Tools 4.3.1



/**
 * IdpmoContratos generated by hbm2java
 */
public class IdpmoContratos  implements java.io.Serializable {


     private IdpmoContratosId id;

    public IdpmoContratos() {
    }

    public IdpmoContratos(IdpmoContratosId id) {
       this.id = id;
    }
   
    public IdpmoContratosId getId() {
        return this.id;
    }
    
    public void setId(IdpmoContratosId id) {
        this.id = id;
    }




}


