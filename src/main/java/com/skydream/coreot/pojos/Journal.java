package com.skydream.coreot.pojos;

import java.util.Date;

/**
 * Created by mcj on 17-02-16.
 */
public class Journal {

//    private int id;
    private String token;
    private int usuarioId;
    private String path;
    private String queryString;
    private Date fecha;

    public Journal(){}

    public Journal(String token, int usuarioId, String path, String queryString, Date fecha){
        this.token = token;
        this.usuarioId = usuarioId;
        this.path = path;
        this.queryString = queryString;
        this.fecha = fecha;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}
