package com.skydream.coreot.pojos;
// Generated 15-sep-2015 14:39:59 by Hibernate Tools 4.3.1


import com.skydream.coreot.dao.GatewayDAO;
import com.skydream.coreot.dao.GenericDAO;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Login generated by hbm2java
 */
public class Login  implements java.io.Serializable , ObjetoCoreOT {


    private int id;
    private Usuarios usuarios;
    private Roles roles;
    private String token;
    private Date fecha;
    private Date fechaUltimaAccion;
    private char estado;
    private Integer tipoToken;

    public Login() {
    }
    
    public Login(int id) {
        this.id = id;
    }

    public Login(int id, Usuarios usuarios, String token, Date fecha, char estado) {
       this.id = id;
       this.usuarios = usuarios;
       this.token = token;
       this.fecha = fecha;
       this.estado = estado;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public Usuarios getUsuarios() {
        return this.usuarios;
    }
    
    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }
    public String getToken() {
        return this.token;
    }
    
    public void setToken(String token) {
        this.token = token;
    }
    public Date getFecha() {
        return this.fecha;
    }
    
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    public char getEstado() {
        return this.estado;
    }
    
    public void setEstado(char estado) {
        this.estado = estado;
    }
    
    public Roles getRoles() {
        return roles;
    }

    public void setRoles(Roles roles) {
        this.roles = roles;
    }

    public Date getFechaUltimaAccion() {
        return fechaUltimaAccion;
    }

    public void setFechaUltimaAccion(Date fechaUltimaAccion) {
        this.fechaUltimaAccion = fechaUltimaAccion;
    }
    
     @Override
    public void setearObjetoDesdeMap(Map parametros, int contador) throws Exception{
        Map params = parametros;
        contador++;
        if (params.containsKey("id")) {
            this.setId(Integer.parseInt(params.get("id").toString()));
        }
        if (params.containsKey("token")) {
            this.setToken(params.get("token").toString());
        }
        if (params.containsKey("fecha")) {
            String fechaLogin = params.get("fecha").toString();
            SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            this.setFecha(formatoDelTexto.parse(fechaLogin));
        }
        if (params.containsKey("estado")) {
            this.setEstado(params.get("estado").toString().charAt(0));
        }
         if (params.containsKey("usuarios")) {
             //pendiente
         }else this.usuarios = null;
    }    

    @Override
    public GatewayDAO obtenerGatewayDAO() {
        return new GenericDAO();
    }


    public Integer getTipoToken() {
        return tipoToken;
    }

    public void setTipoToken(Integer tipoToken) {
        this.tipoToken = tipoToken;
    }
}


