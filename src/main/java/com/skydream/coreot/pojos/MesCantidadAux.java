package com.skydream.coreot.pojos;

import java.math.BigInteger;

/**
 * Created by christian on 17-12-15.
 */
public class MesCantidadAux {

    private int cantidad;
    private int mes;

    public MesCantidadAux(int mes, int cantidad) {
        this.mes= mes;
        this.cantidad= cantidad;
    }


    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }
}
