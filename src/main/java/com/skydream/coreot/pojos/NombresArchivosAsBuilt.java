package com.skydream.coreot.pojos;
// Generated 29-sep-2015 22:49:31 by Hibernate Tools 4.3.1

import java.io.InputStream;
import java.io.Serializable;

/**
 * Ot generated by Christian
 */

public class NombresArchivosAsBuilt implements Serializable {

    private String nombre;
    private String tipoElementoConstruccion;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoElementoConstruccion() {
        return tipoElementoConstruccion;
    }

    public void setTipoElementoConstruccion(String tipoElementoConstruccion) {
        this.tipoElementoConstruccion = tipoElementoConstruccion;
    }
}
