package com.skydream.coreot.pojos;

import java.util.TreeSet;

/**
 * Created by christian on 22-10-15.
 */
public class ObtenerAccionesFrontEndAux {

    private TreeSet<EventosAcciones> eventosAcciones;
    private int idEvento;


    public TreeSet<EventosAcciones> getEventosAcciones() {
        return eventosAcciones;
    }

    public void setEventosAcciones(TreeSet<EventosAcciones> eventosAcciones) {
        this.eventosAcciones = eventosAcciones;
    }

    public int getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(int idEvento) {
        this.idEvento = idEvento;
    }
}
