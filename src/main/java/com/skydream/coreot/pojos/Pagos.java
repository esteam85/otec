package com.skydream.coreot.pojos;
// Generated 03-nov-2015 21:30:56 by Hibernate Tools 4.3.1


import com.skydream.coreot.dao.GatewayDAO;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Pagos generated by hbm2java
 */
public class Pagos  implements java.io.Serializable, ObjetoCoreOT {


     private int id;
     private Long hemId;
     private Long derivadaId;
     private Integer facturaId;
     private Date fechaContable;
     private Date fechaDerivada;
     private Date fechaFactura;
     private Date fechaHem;
     private Character estado;
     private Double monto;
     private Set actas = new HashSet(0);
     private Date fechaPago;

    public Pagos() {
    }

	
    public Pagos(int id) {
        this.id = id;
    }
    public Pagos(int id, Long hemId, Integer facturaId, Date fechaDerivada, Date fechaFactura, Date fechaHem, Character estado, Set actases) {
       this.id = id;
       this.hemId = hemId;
       this.facturaId = facturaId;
       this.fechaDerivada = fechaDerivada;
       this.fechaFactura = fechaFactura;
       this.fechaHem = fechaHem;
       this.estado = estado;
       this.actas = actases;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public Long getHemId() {
        return this.hemId;
    }
    
    public void setHemId(Long hemId) {
        this.hemId = hemId;
    }
    public Integer getFacturaId() {
        return this.facturaId;
    }
    
    public void setFacturaId(Integer facturaId) {
        this.facturaId = facturaId;
    }
    public Date getFechaDerivada() {
        return this.fechaDerivada;
    }
    
    public void setFechaDerivada(Date fechaDerivada) {
        this.fechaDerivada = fechaDerivada;
    }
    public Date getFechaFactura() {
        return this.fechaFactura;
    }
    
    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }
    public Date getFechaHem() {
        return this.fechaHem;
    }
    
    public void setFechaHem(Date fechaHem) {
        this.fechaHem = fechaHem;
    }
    public Character getEstado() {
        return this.estado;
    }
    
    public void setEstado(Character estado) {
        this.estado = estado;
    }
    public Set getActas() {
        return this.actas;
    }
    
    public void setActas(Set actas) {
        this.actas = actas;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }


    public Long getDerivadaId() {
        return derivadaId;
    }

    public void setDerivadaId(Long derivadaId) {
        this.derivadaId = derivadaId;
    }

    public Date getFechaContable() {
        return fechaContable;
    }

    public void setFechaContable(Date fechaContable) {
        this.fechaContable = fechaContable;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    @Override
    public GatewayDAO obtenerGatewayDAO() {
        return null;
    }

    @Override
    public void setearObjetoDesdeMap(Map parametros, int contador) throws Exception {

    }
}


