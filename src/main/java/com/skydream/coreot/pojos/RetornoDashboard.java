package com.skydream.coreot.pojos;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by christian on 17-12-15.
 */
public class RetornoDashboard {

    private List<Integer> y;
    private List<Integer> x;

//    public RetornoDashboard(List<Double> x, List<BigInteger> y) {
//        this.x= x;
//        this.y= y;
//    }


    public List<Integer> getY() {
        return y;
    }

    public void setY(List<Integer> y) {
        this.y = y;
    }

    public List<Integer> getX() {
        return x;
    }

    public void setX(List<Integer> x) {
        this.x = x;
    }
}
