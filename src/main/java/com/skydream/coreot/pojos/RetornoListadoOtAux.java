package com.skydream.coreot.pojos;
// Generated 29-sep-2015 22:49:31 by Hibernate Tools 4.3.1

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.List;

/**
 * Ot generated by christian
 */

public class RetornoListadoOtAux implements Serializable {

    private int total;
    private List<OtAux> listado;


    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<OtAux> getListado() {
        return listado;
    }

    public void setListado(List<OtAux> listado) {
        this.listado = listado;
    }
}
