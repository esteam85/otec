package com.skydream.coreot.pojos;

import java.util.List;

/**
 * Created by Esteam on 19-08-16.
 */
public class ServicioBucle {

    private int cantidad;
    private Servicios servicio;
    private int actividadId;
    private double total;
    private CubicadorDireccionesBucle direccion;
    private List<ServiciosUnidadBucle> serviciosUnidadBucle;

    public ServicioBucle(int cantidad,int actividadId, Servicios servicio, CubicadorDireccionesBucle direccion , List<ServiciosUnidadBucle> serviciosUnidadBucle) {

        this.servicio = servicio;
        this.cantidad = cantidad;
        this.direccion = direccion;

//        this.cantidad = 0d;
//        if(objCantidad instanceof  Double){
//            this.cantidad = (Double)objCantidad;
//        }else this.cantidad = ((Integer)objCantidad).doubleValue();

        this.total = this.cantidad * servicio.getPrecio();
        this.serviciosUnidadBucle = serviciosUnidadBucle;
        this.actividadId = actividadId;
    }

    public Servicios getServicio() {
        return servicio;
    }

    public void setServicio(Servicios servicio) {
        this.servicio = servicio;
    }

    public List<ServiciosUnidadBucle> getServiciosUnidadBucle() {
        return serviciosUnidadBucle;
    }

    public void setServiciosUnidadBucle(List<ServiciosUnidadBucle> serviciosUnidad) {
        this.serviciosUnidadBucle = serviciosUnidad;
    }


    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getActividadId() {
        return actividadId;
    }

    public void setActividadId(int actividadId) {
        this.actividadId = actividadId;
    }

    public CubicadorDireccionesBucle getDireccion() {
        return direccion;
    }

    public void setDireccion(CubicadorDireccionesBucle direccion) {
        this.direccion = direccion;
    }
}
