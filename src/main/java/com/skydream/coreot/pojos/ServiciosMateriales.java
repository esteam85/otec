package com.skydream.coreot.pojos;
// Generated 24-jul-2015 14:51:22 by Hibernate Tools 4.3.1

import com.skydream.coreot.dao.GatewayDAO;
import java.util.Map;

/**
 * ServiciosMateriales generated by hbm2java
 */
public class ServiciosMateriales implements java.io.Serializable, ObjetoCoreOT {

    private Materiales materiales;
    private Servicios servicios;
    private Integer cantidad;

    public ServiciosMateriales() {
    }

    public ServiciosMateriales(Materiales materiales, Servicios servicios) {
        this.materiales = materiales;
        this.servicios = servicios;
    }

    public ServiciosMateriales(Materiales materiales, Servicios servicios, Integer cantidad) {
        this.materiales = materiales;
        this.servicios = servicios;
        this.cantidad = cantidad;
    }

    public ServiciosMateriales(Servicios servicios, Materiales materiales, Integer cantidad) {
        this.materiales = materiales;
        this.servicios = servicios;
        this.cantidad = cantidad;
    }

    public Materiales getMateriales() {
        return this.materiales;
    }

    public void setMateriales(Materiales materiales) {
        this.materiales = materiales;
    }

    public Servicios getServicios() {
        return this.servicios;
    }

    public void setServicios(Servicios servicios) {
        this.servicios = servicios;
    }

    public Integer getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public GatewayDAO obtenerGatewayDAO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setearObjetoDesdeMap(Map parametros, int contador) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
