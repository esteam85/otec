package com.skydream.coreot.pojos;

import com.skydream.coreot.dao.CubicadorDAO;

import java.util.Set;

/**
 * Created by Esteam on 19-08-16.
 */
public class ServiciosUnidadBucle extends ServiciosUnidad {

    private int cantidad;
    private double precio;
    private double total;
    private String direccion;
    private String latitud;
    private String longitud;

    public ServiciosUnidadBucle(ServiciosUnidad serviciosUnidad, int cantidad, double precio) {
        super(serviciosUnidad.getId(), serviciosUnidad.getTipoUnidadMedidaId(), serviciosUnidad.getNombre(), serviciosUnidad.getDescripcion(), serviciosUnidad.getCodigo(), serviciosUnidad.getEstado(), serviciosUnidad.getClave(), serviciosUnidad.getServiciosUnidadMaterialeses(), serviciosUnidad.getServiciosServiciosUnidads());
        this.precio = precio;
        this.cantidad = cantidad;

//        this.cantidad = 0d;
//        if(objCantidad instanceof  Double){
//            this.cantidad = (Double)objCantidad;
//        }else this.cantidad = ((Integer)objCantidad).doubleValue();

        this.total = this.precio * this.cantidad;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }


}
