package com.skydream.coreot.pojos;
// Generated 16-nov-2015 14:53:53 by Hibernate Tools 4.3.1


import com.skydream.coreot.dao.GatewayDAO;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties({"ots"})
public class Sitios implements java.io.Serializable, ObjetoCoreOT {


     private int id;
     private Contratos contratos;
     private PlanDeProyecto planDeProyecto;
     private Regiones regiones;
     private String nombre;
     private String descripcion;
     private String codigo;
     private String codigoSitio;
     private Integer anio;
     private String requerimiento;
     private Integer numProyectoEmplazamiento;
     private String comuna;
     private String latitud;
     private String longitud;
     private String direccion;
     private String tipo;
     private String subTipo;
     private String operador;
     private String codigoOperador;
     private String zona;
     private String tecnologia;
     private String vendor;
     private String rf;
     private String urbanoRural;
     private String estructura;
     private String alturaEstructura;
     private String alturaSoporte;
     private String priorizacion;
     private String hwAsignado;
     private String cluster;
     private String optimizador;
     private String observaciones;
     private String pep1;
     private String pep2;
     private Integer idPmo;
     private Integer lineaPresupuestaria;
     private Date fechaCreacion;
     private char estado;
     private Set ots = new HashSet(0);

    public Sitios() {
    }
    
    public Sitios(Contratos contratos, Regiones regiones) {
        this.contratos = contratos;
        this.regiones = regiones;
    }

	
    public Sitios(int id, Contratos contratos, Regiones regiones) {
        this.id = id;
        this.contratos = contratos;
        this.regiones = regiones;
    }
    public Sitios(int id, Contratos contratos, Regiones regiones, String nombre, String descripcion, String codigo, String codigoSitio, Integer anio, String requerimiento, Integer numProyectoEmplazamiento, String comuna, String latitud, String longitud, String direccion, String tipo, String subTipo, String operador, String codigoOperador, String zona, String tecnologia, String vendor, String rf, String urbanoRural, String estructura, String alturaEstructura, String alturaSoporte, String priorizacion, String hwAsignado, String cluster, String optimizador, String observaciones, String pep1, String pep2, Integer idPmo, Integer lineaPresupuestaria, Date fechaCreacion, char estado, PlanDeProyecto planDeProyecto) {
       this.id = id;
       this.contratos = contratos;
       this.regiones = regiones;
       this.nombre = nombre;
       this.descripcion = descripcion;
       this.codigo = codigo;
       this.codigoSitio = codigoSitio;
       this.anio = anio;
       this.requerimiento = requerimiento;
       this.numProyectoEmplazamiento = numProyectoEmplazamiento;
       this.comuna = comuna;
       this.latitud = latitud;
       this.longitud = longitud;
       this.direccion = direccion;
       this.tipo = tipo;
       this.subTipo = subTipo;
       this.operador = operador;
       this.codigoOperador = codigoOperador;
       this.zona = zona;
       this.tecnologia = tecnologia;
       this.vendor = vendor;
       this.rf = rf;
       this.urbanoRural = urbanoRural;
       this.estructura = estructura;
       this.alturaEstructura = alturaEstructura;
       this.alturaSoporte = alturaSoporte;
       this.priorizacion = priorizacion;
       this.hwAsignado = hwAsignado;
       this.cluster = cluster;
       this.optimizador = optimizador;
       this.observaciones = observaciones;
       this.pep1 = pep1;
       this.pep2 = pep2;
       this.idPmo = idPmo;
       this.lineaPresupuestaria = lineaPresupuestaria;
       this.fechaCreacion = fechaCreacion;
       this.estado = estado;
       this.setPlanDeProyecto(planDeProyecto);

    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public Contratos getContratos() {
        return this.contratos;
    }
    
    public void setContratos(Contratos contratos) {
        this.contratos = contratos;
    }
    public Regiones getRegiones() {
        return this.regiones;
    }
    
    public void setRegiones(Regiones regiones) {
        this.regiones = regiones;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getCodigo() {
        return this.codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    public String getCodigoSitio() {
        return this.codigoSitio;
    }
    
    public void setCodigoSitio(String codigoSitio) {
        this.codigoSitio = codigoSitio;
    }

    public Integer getAnio() {
        return this.anio;
    }
    
    public void setAnio(Integer anio) {
        this.anio = anio;
    }
    public String getRequerimiento() {
        return this.requerimiento;
    }
    
    public void setRequerimiento(String requerimiento) {
        this.requerimiento = requerimiento;
    }
    public Integer getNumProyectoEmplazamiento() {
        return this.numProyectoEmplazamiento;
    }
    
    public void setNumProyectoEmplazamiento(Integer numProyectoEmplazamiento) {
        this.numProyectoEmplazamiento = numProyectoEmplazamiento;
    }
    public String getComuna() {
        return this.comuna;
    }
    
    public void setComuna(String comuna) {
        this.comuna = comuna;
    }
    public String getLatitud() {
        return this.latitud;
    }
    
    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }
    public String getLongitud() {
        return this.longitud;
    }
    
    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }
    public String getDireccion() {
        return this.direccion;
    }
    
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public String getTipo() {
        return this.tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public String getSubTipo() {
        return this.subTipo;
    }
    
    public void setSubTipo(String subTipo) {
        this.subTipo = subTipo;
    }
    public String getOperador() {
        return this.operador;
    }
    
    public void setOperador(String operador) {
        this.operador = operador;
    }
    public String getCodigoOperador() {
        return this.codigoOperador;
    }
    
    public void setCodigoOperador(String codigoOperador) {
        this.codigoOperador = codigoOperador;
    }
    public String getZona() {
        return this.zona;
    }
    
    public void setZona(String zona) {
        this.zona = zona;
    }
    public String getTecnologia() {
        return this.tecnologia;
    }
    
    public void setTecnologia(String tecnologia) {
        this.tecnologia = tecnologia;
    }
    public String getVendor() {
        return this.vendor;
    }
    
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }
    public String getRf() {
        return this.rf;
    }
    
    public void setRf(String rf) {
        this.rf = rf;
    }
    public String getUrbanoRural() {
        return this.urbanoRural;
    }
    
    public void setUrbanoRural(String urbanoRural) {
        this.urbanoRural = urbanoRural;
    }
    public String getEstructura() {
        return this.estructura;
    }
    
    public void setEstructura(String estructura) {
        this.estructura = estructura;
    }
    public String getAlturaEstructura() {
        return this.alturaEstructura;
    }
    
    public void setAlturaEstructura(String alturaEstructura) {
        this.alturaEstructura = alturaEstructura;
    }
    public String getAlturaSoporte() {
        return this.alturaSoporte;
    }
    
    public void setAlturaSoporte(String alturaSoporte) {
        this.alturaSoporte = alturaSoporte;
    }
    public String getPriorizacion() {
        return this.priorizacion;
    }
    
    public void setPriorizacion(String priorizacion) {
        this.priorizacion = priorizacion;
    }
    public String getHwAsignado() {
        return this.hwAsignado;
    }
    
    public void setHwAsignado(String hwAsignado) {
        this.hwAsignado = hwAsignado;
    }
    public String getCluster() {
        return this.cluster;
    }
    
    public void setCluster(String cluster) {
        this.cluster = cluster;
    }
    public String getOptimizador() {
        return this.optimizador;
    }
    
    public void setOptimizador(String optimizador) {
        this.optimizador = optimizador;
    }
    public String getObservaciones() {
        return this.observaciones;
    }
    
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    public String getPep1() {
        return this.pep1;
    }
    
    public void setPep1(String pep1) {
        this.pep1 = pep1;
    }
    public String getPep2() {
        return this.pep2;
    }
    
    public void setPep2(String pep2) {
        this.pep2 = pep2;
    }
    public Integer getIdPmo() {
        return this.idPmo;
    }
    
    public void setIdPmo(Integer idPmo) {
        this.idPmo = idPmo;
    }
    public Integer getLineaPresupuestaria() {
        return this.lineaPresupuestaria;
    }
    
    public void setLineaPresupuestaria(Integer lineaPresupuestaria) {
        this.lineaPresupuestaria = lineaPresupuestaria;
    }
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    public char getEstado() {
        return this.estado;
    }
    
    public void setEstado(char estado) {
        this.estado = estado;
    }
    
    public Set getOts() {
        return ots;
    }

    public void setOts(Set ots) {
        this.ots = ots;
    }

    @Override
    public GatewayDAO obtenerGatewayDAO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setearObjetoDesdeMap(Map parametros, int contador) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    public PlanDeProyecto getPlanDeProyecto() {
        return planDeProyecto;
    }

    public void setPlanDeProyecto(PlanDeProyecto planDeProyecto) {
        this.planDeProyecto = planDeProyecto;
    }
}


