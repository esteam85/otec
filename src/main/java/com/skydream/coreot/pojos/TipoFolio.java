package com.skydream.coreot.pojos;
// Generated 24-jul-2015 14:51:22 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * TipoFolio generated by hbm2java
 */
@JsonIgnoreProperties({"folioses"})
public class TipoFolio  implements java.io.Serializable {


     private int id;
     private String nombre;
     private String descripcion;
     private char estado;
     private Set folioses = new HashSet(0);

    public TipoFolio() {
    }

	
    public TipoFolio(int id) {
        this.id = id;
    }
    public TipoFolio(int id, String nombre, String descripcion, char estado, Set folioses) {
       this.id = id;
       this.nombre = nombre;
       this.descripcion = descripcion;
       this.estado = estado;
       this.folioses = folioses;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public char getEstado() {
        return this.estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }
    
    public Set getFolioses() {
        return this.folioses;
    }
    
    public void setFolioses(Set folioses) {
        this.folioses = folioses;
    }




}


