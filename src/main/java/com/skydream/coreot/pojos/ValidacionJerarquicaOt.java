package com.skydream.coreot.pojos;
// Generated 27-oct-2015 23:07:44 by Hibernate Tools 4.3.1


import com.skydream.coreot.dao.GatewayDAO;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Christian
 */
public class ValidacionJerarquicaOt implements java.io.Serializable {


     private int ot_id;
     private List<ValidadoresOt> validadoresOt;

     public int getOt_id() {
          return ot_id;
     }

     public void setOt_id(int ot_id) {
          this.ot_id = ot_id;
     }

     public List<ValidadoresOt> getValidadoresOt() {
          return validadoresOt;
     }

     public void setValidadoresOt(List<ValidadoresOt> validadoresOt) {
          this.validadoresOt = validadoresOt;
     }
}


