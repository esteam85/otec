package com.skydream.coreot.pojos;

import java.util.Date;

/**
 * Created by christian on 29-08-16.
 */
public class ValidacionesOt {

    private Date fecha;
    private boolean validacion;
    private String comentario;

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public boolean isValidacion() {
        return validacion;
    }

    public void setValidacion(boolean validacion) {
        this.validacion = validacion;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
}
