package com.skydream.coreot.pojos;

import java.util.List;

/**
 * Created by christian on 29-08-16.
 */
public class ValidadoresOt {

    private int id;
    private String nombreUsuario;
    private String concepto;
    private List<ValidacionesOt> validaciones;

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public List<ValidacionesOt> getValidaciones() {
        return validaciones;
    }

    public void setValidaciones(List<ValidacionesOt> validaciones) {
        this.validaciones = validaciones;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }
}
