package com.skydream.coreot.pojos;
// Generated 24-jul-2015 14:51:22 by Hibernate Tools 4.3.1

import com.skydream.coreot.dao.GatewayDAO;
import java.util.Map;

/**
 * WorkflowEventos generated by hbm2java
 */
public class WorkflowEventos implements java.io.Serializable, ObjetoCoreOT, Comparable<WorkflowEventos>{

    private WorkflowEventosId id;
    private Workflow workflow;
    private Eventos evento;
    private Roles rol;
    private Decisiones decision;
    private Integer proximo;
    private Integer eventoPadre;
    private Integer orden;

    public WorkflowEventos() {
    }
    
    public WorkflowEventos(WorkflowEventosId id){
        this.id = id;
    }

    public WorkflowEventos(Workflow workflow, Eventos eventos, Roles rol, Decisiones decision) {
        this.workflow = workflow;
        this.evento = eventos;
        this.decision = decision;
        this.rol = rol;
        this.id.setWorkflowId(workflow.getId());
        this.id.setEventoId(eventos.getId());
        this.id.setRolId(rol.getId());
        this.id.setDecisionId(decision.getId());
    }

    public WorkflowEventos(Workflow workflow, Eventos eventos, Roles rol, Decisiones decision, Integer proximo, Integer eventoPadre) {
        this.workflow = workflow;
        this.evento = eventos;
        this.decision = decision;
        this.rol = rol;
        this.proximo = proximo;
        this.eventoPadre = eventoPadre;
    }

    public Workflow getWorkflow() {
        return this.workflow;
    }

    public void setWorkflow(Workflow workflow) {
        this.workflow = workflow;
    }

    public Eventos getEvento() {
        return this.evento;
    }

    public void setEvento(Eventos evento) {
        this.evento = evento;
    }

    public Decisiones getDecision() {
        return this.decision;
    }

    public void setDecision(Decisiones decision) {
        this.decision = decision;
    }
    
    public Roles getRol() {
        return rol;
    }

    public void setRol(Roles rol) {
        this.rol = rol;
    }

    public Integer getProximo() {
        return this.proximo;
    }

    public void setProximo(Integer proximo) {
        this.proximo = proximo;
    }

    public Integer getEventoPadre() {
        return eventoPadre;
    }

    public void setEventoPadre(Integer eventoPadre) {
        this.eventoPadre = eventoPadre;
    }
    
    public WorkflowEventosId getId() {
        return id;
    }

    public void setId(WorkflowEventosId id) {
        this.id = id;
    }

    @Override
    public GatewayDAO obtenerGatewayDAO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setearObjetoDesdeMap(Map parametros, int contador) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int compareTo(WorkflowEventos we){
        if(this.getEvento()==null || we.getEvento()==null){
            throw new RuntimeException("Error al ordenar objeto WorkflowEventos. No se encontró el objeto Evento.");
        }
        return Integer.compare(this.getEvento().getId(), we.getEvento().getId());
    }


    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }
}
