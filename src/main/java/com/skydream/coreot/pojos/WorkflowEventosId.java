/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.pojos;

/**
 *
 * @author mcj
 */
public class WorkflowEventosId implements java.io.Serializable{
    
    private int workflowId;
    private int eventoId;
    private int rolId;
    private int decisionId;
    
    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof WorkflowEventosId)) {
            return false;
        }
        WorkflowEventosId castOther = (WorkflowEventosId) other;

        return (this.getWorkflowId() == castOther.getWorkflowId())
                && this.getEventoId() == castOther.getEventoId()
                && this.getRolId() == castOther.getRolId()
                && this .getDecisionId() == castOther.getDecisionId();
    }
   
    @Override
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + this.getWorkflowId();
         result = 37 * result + this.getEventoId();
         result = 37 * result + this.getRolId();
         result = 37 * result + this.getDecisionId();
         return result;
   }

    public int getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(int workflowId) {
        this.workflowId = workflowId;
    }

    public int getEventoId() {
        return eventoId;
    }

    public void setEventoId(int eventoId) {
        this.eventoId = eventoId;
    }

    public int getRolId() {
        return rolId;
    }

    public void setRolId(int rolId) {
        this.rolId = rolId;
    }

    public int getDecisionId() {
        return decisionId;
    }

    public void setDecisionId(int decisionId) {
        this.decisionId = decisionId;
    }
   
   
    
}
