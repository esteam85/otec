package com.skydream.coreot.util;

import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author mcj
 */
public final class Config {

    public static Config getINSTANCE() {
        return INSTANCE;
    }

    private Config() {
//        String resourceFile = "com/skydream/coreot/util/config.properties";
        String resourceFile = "config.properties";
        InputStream isLog = this.getClass().getClassLoader().getResourceAsStream(resourceFile);
        Properties props = new Properties();

        try {
            props.load(isLog);
            configurar(props);
            isLog.close();
        } catch (Exception ex) {
            log.error("No se pudo cargar la configuración.", ex);
        }
    }

    private void configurar(Properties props) {
        this.setAsuntoMailNuevoUsuario(props.getProperty("asuntoMailNuevoUsuario"));
        this.setCuerpoMailNuevoUsuario(props.getProperty("cuerpoMailNuevoUsuario"));
        this.setUrlSetearClaveUsuario(props.getProperty("urlSetearClaveUsuario"));
        this.setAsuntoMailAsignacionEjecutor(props.getProperty("asuntoMailAsignacionEjecutor"));
        this.setAsuntoMailAsignacionOk(props.getProperty("asuntoMailAsignacionOk"));
        this.setCuerpoMailAsignacionEjecutor(props.getProperty("cuerpoMailAsignacionEjecutor"));
        this.setCuerpoMailAsignacionOk(props.getProperty("cuerpoMailAsignacionOk"));
        this.setAsuntoMailGestorInformeLibroObra("asuntoMailGestorInformeLibroObra");
        this.setCuerpoMailGestorInformeLibroObra("cuerpoMailGestorInformeLibroObra");
        this.setNombreRepositorioBase(props.getProperty("nombreRepositorioBase"));
        this.setNombreRepositorioUsuarios(props.getProperty("nombreRepositorioUsuarios"));
        this.setTiempoExpiracionTokens(props.getProperty("tiempoExpiracionTokens"));
        this.setHostRedis(props.getProperty("hostRedis"));
        this.setPuertoRedis(Integer.parseInt(props.getProperty("puertoRedis")));
        this.setUrlServicioIndicadoresEconomicos(props.getProperty("urlServicioIndicadoresEconomicos"));
        this.setApiKeySBIF(props.getProperty("apiKeySBIF"));
        this.setMailSoporte(props.getProperty("mailSoporte"));
        this.setPassSoporte(props.getProperty("passSoporte"));
        this.setMailReporte(props.getProperty("mailReporte"));
        this.setPassReporte(props.getProperty("passReporte"));
        this.setSchedulerjobReportes(props.getProperty("schedulerjobReportes"));
        this.setSchedulerjobVistas(props.getProperty("schedulerjobVistas"));
        this.setSchedulerjobActas(props.getProperty("schedulerjobActas"));
    }

    public String getAsuntoMailNuevoUsuario() {
        return asuntoMailNuevoUsuario;
    }

    public void setAsuntoMailNuevoUsuario(String asuntoMailNuevoUsuario) {
        this.asuntoMailNuevoUsuario = asuntoMailNuevoUsuario;
    }

    public String getCuerpoMailNuevoUsuario() {
        return cuerpoMailNuevoUsuario;
    }

    public void setCuerpoMailNuevoUsuario(String cuerpoMailNuevoUsuario) {
        this.cuerpoMailNuevoUsuario = cuerpoMailNuevoUsuario;
    }

    public String getUrlSetearClaveUsuario() {
        return urlSetearClaveUsuario;
    }

    public void setUrlSetearClaveUsuario(String urlSetearClaveUsuario) {
        this.urlSetearClaveUsuario = urlSetearClaveUsuario;
    }

    public String getAsuntoMailAsignacionEjecutor() {
        return asuntoMailAsignacionEjecutor;
    }

    public void setAsuntoMailAsignacionEjecutor(String asuntoMailAsignacionEjecutor) {
        this.asuntoMailAsignacionEjecutor = asuntoMailAsignacionEjecutor;
    }

    public String getAsuntoMailAsignacionOk() {
        return asuntoMailAsignacionOk;
    }

    public void setAsuntoMailAsignacionOk(String asuntoMailAsignacionOk) {
        this.asuntoMailAsignacionOk = asuntoMailAsignacionOk;
    }

    public String getCuerpoMailAsignacionEjecutor() {
        return cuerpoMailAsignacionEjecutor;
    }

    public void setCuerpoMailAsignacionEjecutor(String cuerpoMailAsignacionEjecutor) {
        this.cuerpoMailAsignacionEjecutor = cuerpoMailAsignacionEjecutor;
    }

    public String getCuerpoMailAsignacionOk() {
        return cuerpoMailAsignacionOk;
    }

    public void setCuerpoMailAsignacionOk(String cuerpoMailAsignacionOk) {
        this.cuerpoMailAsignacionOk = cuerpoMailAsignacionOk;
    }

    public String getAsuntoMailGestorInformeLibroObra() {
        return asuntoMailGestorInformeLibroObra;
    }

    public void setAsuntoMailGestorInformeLibroObra(String asuntoMailGestorInformeLibroObra) {
        this.asuntoMailGestorInformeLibroObra = asuntoMailGestorInformeLibroObra;
    }

    public String getCuerpoMailGestorInformeLibroObra() {
        return cuerpoMailGestorInformeLibroObra;
    }

    public void setCuerpoMailGestorInformeLibroObra(String cuerpoMailGestorInformeLibroObra) {
        this.cuerpoMailGestorInformeLibroObra = cuerpoMailGestorInformeLibroObra;
    }

    public String getNombreRepositorioUsuarios() {
        return nombreRepositorioUsuarios;
    }

    public void setNombreRepositorioUsuarios(String nombreRepositorioUsuarios) {
        this.nombreRepositorioUsuarios = nombreRepositorioUsuarios;
    }
    
    public String getNombreRepositorioBase() {
        return nombreRepositorioBase;
    }

    public void setNombreRepositorioBase(String nombreRepositorioBase) {
        this.nombreRepositorioBase = nombreRepositorioBase;
    }

    public String getTiempoExpiracionTokens() {
        return tiempoExpiracionTokens;
    }

    public void setTiempoExpiracionTokens(String tiempoExpiracionTokens) {
        this.tiempoExpiracionTokens = tiempoExpiracionTokens;
    }

    public String getHostRedis() {
        return hostRedis;
    }

    public void setHostRedis(String hostRedis) {
        this.hostRedis = hostRedis;
    }

    public Integer getPuertoRedis() {
        return puertoRedis;
    }

    public void setPuertoRedis(Integer puertoRedis) {
        this.puertoRedis = puertoRedis;
    }

    public String getUrlServicioIndicadoresEconomicos() {
        return urlServicioIndicadoresEconomicos;
    }

    public void setUrlServicioIndicadoresEconomicos(String urlServicioIndicadoresEconomicos) {
        this.urlServicioIndicadoresEconomicos = urlServicioIndicadoresEconomicos;
    }

    public String getApiKeySBIF() {
        return apiKeySBIF;
    }

    public void setApiKeySBIF(String apiKeySBIF) {
        this.apiKeySBIF = apiKeySBIF;
    }

    public String getMailSoporte() {return  mailSoporte;}

    public void setMailSoporte(String mailSoporte) {this.mailSoporte = mailSoporte;}

    public String getPassSoporte() {return passSoporte;}

    public void setPassSoporte(String passSoporte) {this.passSoporte = passSoporte;}

    public String getMailReporte() {
        return mailReporte;
    }

    public void setMailReporte(String mailReporte) {
        this.mailReporte = mailReporte;
    }

    public String getPassReporte() {
        return passReporte;
    }

    public void setPassReporte(String passReporte) {
        this.passReporte = passReporte;
    }


    public String getSchedulerjobVistas() {
        return schedulerjobVistas;
    }

    public void setSchedulerjobVistas(String schedulerjobVistas) {
        this.schedulerjobVistas = schedulerjobVistas;
    }

    public String getSchedulerjobReportes() {
        return schedulerjobReportes;
    }

    public void setSchedulerjobReportes(String schedulerjobReportes) {
        this.schedulerjobReportes = schedulerjobReportes;
    }

    public String getSchedulerjobActas() {
        return schedulerjobActas;
    }

    public void setSchedulerjobActas(String schedulerjobActas) {
        this.schedulerjobActas = schedulerjobActas;
    }

    private String asuntoMailNuevoUsuario;
    private String cuerpoMailNuevoUsuario;
    private String urlSetearClaveUsuario;
    private String asuntoMailAsignacionEjecutor;
    private String asuntoMailAsignacionOk;
    private String cuerpoMailAsignacionEjecutor;
    private String cuerpoMailAsignacionOk;
    private String asuntoMailGestorInformeLibroObra;
    private String cuerpoMailGestorInformeLibroObra;
    private String nombreRepositorioBase;
    private String nombreRepositorioUsuarios;
    private String tiempoExpiracionTokens;
    private String hostRedis;
    private Integer puertoRedis;
    private String urlServicioIndicadoresEconomicos;
    private String apiKeySBIF;
    private String mailSoporte;
    private String passSoporte;
    private String mailReporte;
    private String passReporte;
    private String schedulerjobReportes;
    private String schedulerjobVistas;
    private String schedulerjobActas;

    private static final Config INSTANCE = new Config();
    private static final Logger log = Logger.getLogger(Config.class);

}
