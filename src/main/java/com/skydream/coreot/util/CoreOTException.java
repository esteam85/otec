/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.util;

/**
 *
 * @author Administrador
 */
public class CoreOTException extends Exception{
    
    public CoreOTException(){}

    public CoreOTException(String mensajeError){
        this.msjeErrorControlado = mensajeError;
    }
    
    public CoreOTException(String mensajeError, int codigoError){
        this.msjeErrorControlado = mensajeError;
        this.codigoErrorControlado = codigoError;
    }

    /**
     * @return the msjeErrorControlado
     */
    public String getMsjeErrorControlado() {
        return msjeErrorControlado;
    }

    /**
     * @param msjeErrorControlado the msjeErrorControlado to set
     */
    public void setMsjeErrorControlado(String msjeErrorControlado) {
        this.msjeErrorControlado = msjeErrorControlado;
    }

    /**
     * @return the codigoErrorControlado
     */
    public int getCodigoErrorControlado() {
        return codigoErrorControlado;
    }

    /**
     * @param codigoErrorControlado the codigoErrorControlado to set
     */
    public void setCodigoErrorControlado(int codigoErrorControlado) {
        this.codigoErrorControlado = codigoErrorControlado;
    }

    private String msjeErrorControlado;
    private int codigoErrorControlado;
    
}