package com.skydream.coreot.util;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;
import org.apache.log4j.Logger;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 * Created by mcj on 16-02-16.
 */
public class NewRedisUtil {

    private static final Logger log = Logger.getLogger(NewRedisUtil.class);
    private static RedisClient redisClient;

    static {
        try {
            // config file.
            Config config = Config.getINSTANCE();
            redisClient = new RedisClient(config.getHostRedis(),config.getPuertoRedis());
        } catch (Throwable ex) {
            // Log the exception.
            log.fatal("Redis Connection failed.",ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

  public static RedisClient getRedisClient(){
      return redisClient;
  }

}
