/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.util;

import com.skydream.coreot.pojos.Repositorios;
import org.apache.fop.apps.*;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;

import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author mcj
 */
public class RenderPDF {

    Repositorios repoBase;

    private RenderPDF(){}

    public RenderPDF(Repositorios repo){
        this.repoBase = repo;
    }

    private static final Logger log = Logger.getLogger(RenderPDF.class);

    private String parserFechaDateNombreDocumentos(Date fecha) {
        if (fecha == null) {
            return "Sin informaciÃ³n";
        } else {
            try {
                DateFormat formato = new SimpleDateFormat("yyyyMMdd");
                String fechaString = formato.format(fecha);
                return fechaString;
            } catch (Exception e) {
                return "error fecha";
            }

        }
    }

    private String obtienefechaSegunFormato(Date fecha, String formatoSalida) {
        String fechaString = "";
        if (fecha.equals("null")) {
            return fecha.toString();
        } else {
            try {
                DateFormat formato = new SimpleDateFormat(formatoSalida);
                fechaString = formato.format(fecha);
                //System.out.println("====================>  Fecha de salida :  "+fechaString);
                return fechaString;
            } catch (Exception e) {
                fechaString = "error: "+e;
                return fechaString;
            }
        }
    }

    public void convertToPDF_FromXmlPath(String pathPDF, String pathXml, String pathXsl)  throws IOException, FOPException, TransformerException, Exception {
        // the XSL FO file
        File xsltFile = new File(pathXsl);
        // the XML file which provides the input
        StreamSource xmlSource = new StreamSource(new File(pathXml));
        // create an instance of fop factory
        FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
        // a user agent is needed for transformation
        FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
        // Setup output
        OutputStream out;
        out = new java.io.FileOutputStream(pathPDF);

        try {
            // Construct fop with desired output format
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

            // Setup XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(xsltFile));

            // Resulting SAX events (the generated FO) must be piped through to FOP
            Result res = new SAXResult(fop.getDefaultHandler());

            // Start XSLT transformation and FOP processing
            // That's where the XML is first transformed to XSL-FO and then
            // PDF is created
            //Si se requiere de fuente archivo
            transformer.transform(xmlSource, res);
            //Si se requiere de fuente string usando StringReader
            //transformer.transform(new javax.xml.transform.stream.StreamSource(reader), res);
        } catch (Exception ex) {
            log.error("Error al generar PDF. ", ex);
        }finally {
            out.close();
        }
    }

    public void convertToPDF_FromXmlJson(String pathPDF, String jsonString, String pathXsl)  throws IOException, FOPException, TransformerException, Exception {
        // the XSL FO file
        File xsltFile = new File(pathXsl);
        // create an instance of fop factory
        FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
        // a user agent is needed for transformation
        FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
        // Setup output
        OutputStream out;
        out = new java.io.FileOutputStream(pathPDF);

        try {
            // Construct fop with desired output format
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

            // Setup XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(xsltFile));

            // Resulting SAX events (the generated FO) must be piped through to FOP
            Result res = new SAXResult(fop.getDefaultHandler());

            // Start XSLT transformation and FOP processing
            // That's where the XML is first transformed to XSL-FO and then
            // PDF is created
            //System.out.println("Pasando JSON "+jsonString);

            JSONObject json = new JSONObject(jsonString);
            String xmlString = XML.toString(json);

            //System.out.println("Pasando XML "+xmlString);
            StringReader reader = new StringReader(xmlString);
            //Si se requiere de fuente string usando StringReader
            transformer.transform(new javax.xml.transform.stream.StreamSource(reader), res);
        } catch (Exception ex) {
            log.error("Error al generar PDF. ", ex);
            throw ex;
        }finally {
            out.close();
        }
    }

    public boolean generarPDF_From_XmlString_XslFTP(String xmlString, Repositorios repo, String _nombrePdf, String _nombreXsl)  throws IOException, FOPException, TransformerException, Exception {
        boolean salida = false;
        String formatoFecha = "ddMMyyyyHHmmss";
        String nombreXsl = _nombreXsl+".xsl";

        /*if(_nombrePdf.equals("pdf_ot")){
            nombreXsl = "plantilla_ot.xsl";
        }else if (_nombrePdf.equals("pdf_acta")){
            nombreXsl = "plantilla_acta.xsl";
        }else throw new Exception("Nombre PDF no valido.");*/

        String nombrePdf = _nombrePdf + "_"+ obtienefechaSegunFormato(new Date(),formatoFecha) + ".pdf";

        String SFTPHost = repo.getServidor();
        String SFTPUser = repo.getUsuario();
        String SFTPPassword = repo.getPassword();

        String SFTPHostBase = getRepoBase().getServidor();
        String SFTPUserBase = getRepoBase().getUsuario();
        String SFTPPasswordBase = getRepoBase().getPassword();

        String SFTPDir_xsl = getRepoBase().getUrl() + "/" + "/PDFs" + "/Plantillas";
        String SFTPDir_pdf = repo.getUrl() + "/" + "/PDFs" + "/Pdfs";

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {

            System.setProperty("java.awt.headless", "true");

            byte[] bytePlantillaXSL = SecureTransmission.getFile(SFTPHostBase, SFTPUserBase, SFTPPasswordBase, SFTPDir_xsl, nombreXsl);
            InputStream inputStreamXSL = new ByteArrayInputStream(bytePlantillaXSL);

            // the XSL FO file
            // create an instance of fop factory
            FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
            // a user agent is needed for transformation
            FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
            // Setup output
            // Construct fop with desired output format
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

            // Setup XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(inputStreamXSL));

            // Resulting SAX events (the generated FO) must be piped through to FOP
            Result res = new SAXResult(fop.getDefaultHandler());

            // Start XSLT transformation and FOP processing
            // That's where the XML is first transformed to XSL-FO and then
            // PDF is created

            //System.out.println("Pasando XML "+xmlString);

            StringReader reader = new StringReader(xmlString);
            //Si se requiere de fuente string usando StringReader
            transformer.transform(new javax.xml.transform.stream.StreamSource(reader), res);

            SecureTransmission.sendFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir_pdf, nombrePdf, out.toByteArray());
            salida = true;
        } catch (Exception ex) {
            log.error("Error al generar PDF ("+nombrePdf+") en repositorio "+ SFTPDir_pdf +" y xsl en "+SFTPDir_xsl+" :", ex);
            salida = false;
        }finally {
            out.close();
        }
        return salida;
    }

    public boolean generarPDFActaPagada(String xmlString, Repositorios repo, String _nombrePdf, String _nombreXsl)  throws IOException, FOPException, TransformerException, Exception {

        boolean salida = false;
        String nombreXsl = _nombreXsl+".xsl";
        String ruta = "/Gestion_Economica/Actas_Pagadas/";
        String nombrePdf = _nombrePdf + ".pdf";

        String SFTPHost = repo.getServidor();
        String SFTPUser = repo.getUsuario();
        String SFTPPassword = repo.getPassword();

        String SFTPDir_xsl = repo.getUrl() + "/" + "/PDFs" + "/Plantillas";
        String SFTPDir_pdf = repo.getUrl()+ ruta;
        SecureTransmission.makeDirectory(SFTPHost, SFTPUser, SFTPPassword, SFTPDir_pdf);

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {

            System.setProperty("java.awt.headless", "true");

            byte[] bytePlantillaXSL = SecureTransmission.getFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir_xsl, nombreXsl);
            InputStream inputStreamXSL = new ByteArrayInputStream(bytePlantillaXSL);

            // the XSL FO file
            // create an instance of fop factory
            FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
            // a user agent is needed for transformation
            FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
            // Setup output
            // Construct fop with desired output format
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

            // Setup XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(inputStreamXSL));

            // Resulting SAX events (the generated FO) must be piped through to FOP
            Result res = new SAXResult(fop.getDefaultHandler());

            // Start XSLT transformation and FOP processing
            // That's where the XML is first transformed to XSL-FO and then
            // PDF is created

            StringReader reader = new StringReader(xmlString);
            //Si se requiere de fuente string usando StringReader
            transformer.transform(new javax.xml.transform.stream.StreamSource(reader), res);

            SecureTransmission.sendFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir_pdf, nombrePdf, out.toByteArray());
            salida = true;
        } catch (Exception ex) {
            log.error("Error al generar PDF ("+nombrePdf+") en repositorio "+ SFTPDir_pdf +" y xsl en "+SFTPDir_xsl+" :", ex);
            salida = false;
        }finally {
            out.close();
        }
        return salida;
    }

    public void convertToPDF_FromXmlString(String pathPDF, String xmlString, String pathXsl)  throws IOException, FOPException, TransformerException, Exception {
        // the XSL FO file
        File xsltFile = new File(pathXsl);
        // create an instance of fop factory
        FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
        // a user agent is needed for transformation
        FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
        // Setup output
        OutputStream out;
        out = new java.io.FileOutputStream(pathPDF);

        try {
            // Construct fop with desired output format
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

            // Setup XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(xsltFile));

            // Resulting SAX events (the generated FO) must be piped through to FOP
            Result res = new SAXResult(fop.getDefaultHandler());

            // Start XSLT transformation and FOP processing
            // That's where the XML is first transformed to XSL-FO and then
            // PDF is created

            //System.out.println("Pasando XML "+xmlString);

            StringReader reader = new StringReader(xmlString);
            //Si se requiere de fuente string usando StringReader
            transformer.transform(new javax.xml.transform.stream.StreamSource(reader), res);
        } catch (Exception ex) {
            log.error("Error al generar PDF. ", ex);
            throw ex;
        }finally {
            out.close();
        }
    }

    public byte[] generarPDF_From_XmlString_XslFTP_toByte(String xmlString, Repositorios repo, String _nombrePdf, String _nombreXsl)  throws IOException, FOPException, TransformerException, Exception {

        String nombreXsl = _nombreXsl+".xsl";
        String ruta = "/PDFs/Plantillas/";
        String SFTPHost = repo.getServidor();
        String SFTPUser = repo.getUsuario();
        String SFTPPassword = repo.getPassword();
        String SFTPDir_xsl = repo.getUrl() + ruta;

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {

            System.setProperty("java.awt.headless", "true");

            byte[] bytePlantillaXSL = SecureTransmission.getFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir_xsl, nombreXsl);
            InputStream inputStreamXSL = new ByteArrayInputStream(bytePlantillaXSL);

            // the XSL FO file
            // create an instance of fop factory
            FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
            // a user agent is needed for transformation
            FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
            // Setup output
            // Construct fop with desired output format
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

            // Setup XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(inputStreamXSL));

            // Resulting SAX events (the generated FO) must be piped through to FOP
            Result res = new SAXResult(fop.getDefaultHandler());

            // Start XSLT transformation and FOP processing
            // That's where the XML is first transformed to XSL-FO and then
            // PDF is created

            //System.out.println("Pasando XML "+xmlString);

            StringReader reader = new StringReader(xmlString);
            //Si se requiere de fuente string usando StringReader
            transformer.transform(new javax.xml.transform.stream.StreamSource(reader), res);

            //SecureTransmission.sendFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir_pdf, nombrePdf, out.toByteArray());
        } catch (Exception ex) {
            log.error("Error al generar PDF ("+_nombrePdf+") en repositorio xsl "+SFTPDir_xsl+" :", ex);
        }finally {
            out.close();
        }
        return out.toByteArray();
    }

    /**
     * This method will convert the given XML to XSL-FO
     * @throws IOException
     * @throws FOPException
     * @throws TransformerException
     */
    public void convertToFO(String pathXml, String pathXsl)  throws IOException, FOPException, TransformerException {
        // the XSL FO file
        File xsltFile = new File(pathXsl);


        /*TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer(new StreamSource("F:\\Temp\\template.xsl"));*/

        // the XML file which provides the input
        StreamSource xmlSource = new StreamSource(new File(pathXml));

        // a user agent is needed for transformation
        /*FOUserAgent foUserAgent = fopFactory.newFOUserAgent();*/
        // Setup output
        OutputStream out;

        out = new java.io.FileOutputStream("F:\\Temp\\temp.fo");

        try {
            // Setup XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(xsltFile));

            // Resulting SAX events (the generated FO) must be piped through to FOP
            //Result res = new SAXResult(fop.getDefaultHandler());

            Result res = new StreamResult(out);

            //Start XSLT transformation and FOP processing
            transformer.transform(xmlSource, res);


            // Start XSLT transformation and FOP processing
            // That's where the XML is first transformed to XSL-FO and then
            // PDF is created
            transformer.transform(xmlSource, res);
        } finally {
            out.close();
        }
    }
    
    public static RenderPDF getINSTANCE(){
        return INSTANCE;
    }
    
    private static final RenderPDF INSTANCE = new RenderPDF();

    private Repositorios getRepoBase() {
        return repoBase;
    }

    private void setRepoBase(Repositorios repoBase) {
        this.repoBase = repoBase;
    }
    
}
