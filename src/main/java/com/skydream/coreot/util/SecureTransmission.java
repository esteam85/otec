/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.util;

import com.jcraft.jsch.*;

import java.io.*;

import org.jboss.logging.Logger;

/**
 *
 * @author mcj
 */
public class SecureTransmission {
    
    
    public static void sendFile(String SFTPHOST,String SFTPUSER, String SFTPPASS, String SFTPWORKINGDIR, String nameFile, byte[] byteArrInputStream) {
        int SFTPPORT = 22;
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
            session.setPassword(SFTPPASS);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp channelSftp = (ChannelSftp) channel;
            log.debug("Ruta en que se grabará nuevo archivo --> " + SFTPWORKINGDIR);
            channelSftp.cd(SFTPWORKINGDIR);
            InputStream is = new ByteArrayInputStream(byteArrInputStream);
            log.debug("Archivo que se grabará --> " + nameFile);
            channelSftp.put(is, nameFile);
        } catch (JSchException | SftpException ex) {
            log.error(ex);
        }
    }
    
    public static void deleteFile(String SFTPHOST,String SFTPUSER, String SFTPPASS, String SFTPWORKINGDIR, String nameFile) {
        int SFTPPORT = 22;
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
            session.setPassword(SFTPPASS);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp channelSftp = (ChannelSftp) channel;
            channelSftp.cd(SFTPWORKINGDIR);
            channelSftp.rm(nameFile);
        } catch (JSchException | SftpException ex) {
            log.error(ex);
        }
    }    
    
    
    public static void makeDirectory(String SFTPHOST,String SFTPUSER, String SFTPPASS, String makeDir) throws Exception{
        int SFTPPORT = 22;
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
            session.setPassword(SFTPPASS);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp channelSftp = (ChannelSftp) channel;

            log.debug("Ruta completa a revisar --> " + makeDir);

            String[] subCarpetas = makeDir.split("/");

            String varAux = "";
            for(String elemento : subCarpetas){
                log.debug("Directorio en revision --> " + varAux);
                if(!elemento.isEmpty()){
                    varAux += "/".concat(elemento);
                    log.debug("Directorio en revision (2) --> " + varAux);
                    SftpATTRS attrs=null;
                    try {
                        attrs = channelSftp.stat(varAux);
                        log.debug("Valor de attrs --> " + attrs);
                    } catch (Exception e) {
                        log.error("directorio" + varAux + " no encontrado.");
                    }
                    if (attrs == null) {
                        log.debug("Creando directorio " + varAux);
                        channelSftp.mkdir(varAux);
                    }
                }
            }

        } catch (JSchException ex) {
            log.error(ex);
            throw ex;
        }
    }
    
    
    public static byte[] getFile(String SFTPHOST, String SFTPUSER, String SFTPPASS, String SFTPWORKINGDIR, String fileName) throws Exception{
        int SFTPPORT = 22;
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
            session.setPassword(SFTPPASS);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp channelSftp = (ChannelSftp) channel;
            log.debug("Ruta del archivo en getFile --> " + SFTPWORKINGDIR);
//            SFTPWORKINGDIR = "OTEC/Ordenes_de_Trabajo/Ot-148/Libro-71";
            channelSftp.cd(SFTPWORKINGDIR);
            byte[] buffer = new byte[1024];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try (BufferedInputStream bis = new BufferedInputStream(channelSftp.get(fileName))) {
                int readCount;
                while ((readCount = bis.read(buffer)) > 0) {
                    baos.write(buffer, 0, readCount);
                }
            }
            
            return baos.toByteArray();
        } catch (JSchException | SftpException | IOException ex) {
            log.error(ex);
            throw new Exception("Error al intentar obtener el archivo.");
        }

    }

    private static final Logger log = Logger.getLogger(SecureTransmission.class);

}
