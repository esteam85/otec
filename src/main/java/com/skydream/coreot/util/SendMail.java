/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.util;

import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author mcj
 */
public class SendMail {
    
    private SendMail(){}
    
    public boolean enviarMail(String emailDestino, String asunto, String cuerpoMensaje, List<String> destinatariosACopiar) {

        Properties props = setearProperties();
        
        javax.mail.Session session = null;
        try {
            session = javax.mail.Session.getDefaultInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("soporte.otec@its.cl", "Sotec.its");
                        }
                    });
        } catch (Throwable e) {
            e.printStackTrace();
        }
        
        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("soporte.otec@its.cl"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(emailDestino));
            if(destinatariosACopiar!=null){
                for(String destinatario : destinatariosACopiar){
                    message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(destinatario));
                }
            }
            message.setSubject(asunto);
//            message.setText(cuerpoMensaje);
            message.setContent(cuerpoMensaje, "text/html");
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }

        return true;
    }

    private Properties setearProperties() {
        Properties props = new Properties();
//        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.host", "mail.its.cl");
//        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.port", "587");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
//        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.port", "587");
        return props;
    }
    
    public static SendMail getINSTANCE(){
        return INSTANCE;
    }
    
    private static final SendMail INSTANCE = new SendMail();
    
}
