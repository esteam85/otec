package com.skydream.eventmanager;

import com.skydream.coreot.pojos.Acciones;
import com.skydream.coreot.pojos.GestionEconomica_old;
import com.skydream.coreot.pojos.UsuariosValidadores;
import com.skydream.coreot.pojos.ValidacionesOtec;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by mcj on 25-11-15.
 */
public class AccionAdjuntarCarta extends Command {



    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {

        try {
            Map mapaOT = (Map) params.get("datosOt");
            int otId = (int) mapaOT.get("id");

            int eventoId = (int) params.get("idEvento");
            int usuarioId = (int) params.get("usuarioId");

            try {
                getGateway().actualizarWEE(otId);
            } finally {
                contAccionesBBDD.getAndIncrement();
            }



        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }

    }


    private static final Logger log = Logger.getLogger(AccionIngresarPago.class);

}
