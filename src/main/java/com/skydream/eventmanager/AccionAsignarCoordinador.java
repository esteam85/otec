/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionAsignarCoordinador extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        int idOt = 0;
        try {
            DetalleParametros detalleParametro = retornarDetalleParametros(accion);
            CoreOtFactory coreOtFactory = new CoreOtFactory();
            Map mapaOT = (Map) params.get("datosOt");
            int otId = (int)mapaOT.get("id");
            Ot ot = (Ot)getGateway().retornarObjetoCoreOT(Ot.class,otId);
            ot.setFechaInicioReal(new Date());
            int eventoId = (int) params.get("idEvento");
            Map coordinadorMap = (Map) params.get("coordinador");
            String coordinadorId = coordinadorMap.get("id").toString();
            int usuarioEjecutor = Integer.parseInt(params.get("usuarioEjecutor").toString());

            try {
                this.getGateway().actualizarWorkfloEventoEjecucion(otId, eventoId, Integer.parseInt(coordinadorId));
                getGateway().actualizarWEE(otId);
                Contratos contrato = new Contratos();
                contrato = (Contratos)this.getGateway().obtenerRegistroConFiltro(Contratos.class, null, "id", ot.getContrato().getId());
                this.getGateway().eliminaUsuarioOtInicial(ot, 2);
                this.getGateway().almacenaUsuarioOt(ot, contrato, ot.getGestor().getId(), Integer.parseInt(coordinadorId), 2 );
//                this.getGateway().almacenaUsuarioOt(ot, contrato, ot.getGestor().getId(), usuarioEjecutor, eventoId );


            } finally {
                contAccionesBBDD.getAndIncrement();
            }

            
        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }

    private int buscarData(String campo1, String campo2, Map params) {
        Map padre = (Map) params.get(campo1);
        String valor = (String) padre.get(campo2);
        return Integer.parseInt(valor);
    }

    private DetalleParametros retornarDetalleParametros(Acciones accion) {
        TreeSet<AccionesParametros> accionesParametros = (TreeSet) accion.getAccionesParametros();
        AccionesParametros accionParam = (AccionesParametros) accionesParametros.first();
        Parametros parametro = accionParam.getParametros();
        List<DetalleParametros> listaDetalleParametros = new ArrayList<>(parametro.getDetalleParametros());
        return listaDetalleParametros.get(0);
    }
    
    private static final Logger log = Logger.getLogger(AccionAsignarCoordinador.class);
    
}
