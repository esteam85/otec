/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionAutorizarPago extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        int idOt = 0;
        try {
            DetalleParametros detalleParametro = retornarDetalleParametros(accion);
            CoreOtFactory coreOtFactory = new CoreOtFactory();

            Map mapaOT = (Map) params.get("datosOt");
            int otId = (int)mapaOT.get("id");
            int idEventoPadre = (int) params.get("idEvento");
            String idEventoDesicion = (String) params.get("eventoId");
            int usuarioId = (int) params.get("usuarioId");

            String opcionAutorizacion = (String) params.get("opcionAutorizacion");
            String comentario = (String) params.get("comentario");
            boolean registroAutorizacion = false;

            boolean validacionOt = this.getGateway().determinaAprobacionGerarquica(otId);

            try {
                // opcion cuando autoriza
                Actas actas = this.getGateway().obtenerActaValida(otId);
                if(opcionAutorizacion.equalsIgnoreCase("Aprobar")){
                    List<UsuariosValidadores> usuariosValidadores = this.getGateway().obtenerUsuariosValidadoresd(otId, idEventoPadre);
                    int proximoValidador = 0;

                    for (UsuariosValidadores usuarios : usuariosValidadores){
                        if(usuarios.getUsuarios().getId() == usuarioId){
                            // registrar Validacion
                            proximoValidador = usuarios.getOrden() + 1;
                            // registrar la autorizacion
                            registroAutorizacion = this.getGateway().registrarAutorizacion(otId, idEventoPadre, usuarioId, comentario, true);
                        }
                    }

                    boolean proximoUsuarioValidador = false;
                    int usuarioIdProximo = 0;
                    for (UsuariosValidadores usuarios : usuariosValidadores){
                        if(usuarios.getOrden() == proximoValidador){
                            usuarioIdProximo = usuarios.getUsuarios().getId();
                            proximoUsuarioValidador = true;
                        }
                    }

                    if(registroAutorizacion){
                        if(proximoUsuarioValidador  && validacionOt){
                            // actualizo el registro de wee para notificar al proximo usuario que debe validar
                            boolean actualizo = this.getGateway().actualizarUsuarioWorkflowEventoEjecucion(otId, idEventoPadre, usuarioId, usuarioIdProximo);
                        }else{
                            // si no hay mas usuario validadores avanzo el evento al proximo y actualizo al acta a true en el campo validacion
//                            this.getGateway().actualizarValidacionActa(idActa, validacionUsuario, false);

//                        getGateway().actualizarWEE(otId);
                            boolean apruebaActaPago = false;
                            apruebaActaPago = this.getGateway().apruebaActaParaPago(actas);
                            this.getGateway().continuarFlujoWorkfloEventoEjecucionDecision(otId, Integer.parseInt(idEventoDesicion), false);
                        }
                    }else{
                        throw new Exception();
                    }

                }else{
                    boolean actualizaActa = false;
                    registroAutorizacion =  this.getGateway().registrarAutorizacion(otId, idEventoPadre, usuarioId, comentario, false);
                    actualizaActa = this.getGateway().actualizaActarechazoJefatura(actas);
                    actas.setValidacionUsuarios(false);
                    this.getGateway().continuarFlujoWorkfloEventoEjecucionDecision(otId, Integer.parseInt(idEventoDesicion), true);

                }


            } finally {
                contAccionesBBDD.getAndIncrement();
            }


        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }

    private int buscarData(String campo1, String campo2, Map params) {
        Map padre = (Map) params.get(campo1);
        String valor = (String) padre.get(campo2);
        return Integer.parseInt(valor);
    }

    private DetalleParametros retornarDetalleParametros(Acciones accion) {
        TreeSet<AccionesParametros> accionesParametros = (TreeSet) accion.getAccionesParametros();
        AccionesParametros accionParam = (AccionesParametros) accionesParametros.first();
        Parametros parametro = accionParam.getParametros();
        List<DetalleParametros> listaDetalleParametros = new ArrayList<>(parametro.getDetalleParametros());
        return listaDetalleParametros.get(0);
    }
    
    private static final Logger log = Logger.getLogger(AccionAutorizarPago.class);
    
}
