/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.Acciones;
import com.skydream.coreot.pojos.Usuarios;
import com.skydream.coreot.pojos.UsuariosValidadores;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author JonathanRamirez
 */
public class AccionCambioProveedor extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        try {
            CoreOtFactory coreOtFactory = new CoreOtFactory();

            int otId = Integer.parseInt( params.get("otId").toString());
            int eventoId = Integer.parseInt(params.get("eventoId").toString());
            int usuarioId = Integer.parseInt(params.get("usuarioId").toString());
            String obs = (String) params.get("obs");
            boolean cambioProveedor = false;
            boolean registroValidacion = false;
            Map mapaProveedor = (HashMap) params.get("proveedor");
            int proveedorId = Integer.parseInt( mapaProveedor.get("id").toString());
            int cubicacionId = Integer.parseInt( params.get("cubicacionId").toString());

            Map mapaContrato = (HashMap) params.get("contrato");
            int contratoId = Integer.parseInt( mapaContrato.get("id").toString());

            List<Usuarios> usuarios = new ArrayList<Usuarios>();
            Map datosWeeActuales = new HashMap();

            try {
                cambioProveedor = this.getGateway().cambioProveedor(otId, cubicacionId, proveedorId);
                datosWeeActuales = this.getGateway().obtieneRolIdCambioProveedorWee(eventoId, otId);
                int rolId = Integer.parseInt(datosWeeActuales.get("rolUsuario").toString());
                int usuarioidActualEventos = Integer.parseInt(datosWeeActuales.get("usuarioIdActual").toString());
                usuarios = this.getGateway().obtenerUsuariosPorRolProovedorGatewayAcciones(rolId, proveedorId, contratoId);
                if(cambioProveedor){
                    this.getGateway().actualizarUsuarioCambioProveedorWorkflowEventoEjecucion(otId, rolId, usuarios);
                    this.getGateway().continuarFlujoWorkfloEventoEjecucionDecision(otId, eventoId, false);
                }else{
                    throw new Exception();
                }
            } finally {
                contAccionesBBDD.getAndIncrement();
            }


        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }


    private static final Logger log = Logger.getLogger(AccionValidarActaUnificado.class);

}