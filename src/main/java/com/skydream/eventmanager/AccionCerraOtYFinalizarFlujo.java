/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.pojos.Acciones;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionCerraOtYFinalizarFlujo extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        Map datosOt = new HashMap();
        datosOt = (HashMap) params.get("datosOt");
        int otId = Integer.parseInt(datosOt.get("id").toString());
        int eventoId = Integer.parseInt(params.get("idEvento").toString());
        int usuarioEjecutor = Integer.parseInt(params.get("usuarioEjecutor").toString());
        try {

            
            try {
                int eventoIdActual = 0;
                getGateway().actualizarWorkfloEventoEjecucionCierreOt(otId, eventoId, usuarioEjecutor);
                this.getGateway().CerrarOtyFinalizar(otId);

            } finally {
                contAccionesBBDD.incrementAndGet();
            }

            
        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }

    
    private static final Logger log = Logger.getLogger(AccionCerraOtYFinalizarFlujo.class);
    
}
