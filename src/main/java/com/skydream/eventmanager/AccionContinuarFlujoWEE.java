/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.dao.LibroObraDAO;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionContinuarFlujoWEE extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        int idOt = 0;
        try {
//            DetalleParametros detalleParametro = retornarDetalleParametros(accion);
//            CoreOtFactory coreOtFactory = new CoreOtFactory();

            Map mapaOT = (Map) params.get("datosOt");
            int otId = (int)mapaOT.get("id");


            int eventoId = (int) params.get("idEvento");
            int usuarioEjecutor = (int) params.get("usuarioId");

            try {
//                this.getGateway().continuarFlujoWorkfloEventoEjecucion(otId, eventoId);
                getGateway().actualizarWEE(otId);
                if(eventoId == 3){
                    Ot ot = (Ot) getGateway().obtenerRegistroConFiltro(Ot.class,null, "id", otId);
                    Map map = new HashMap();
                    Map mapAux = new HashMap();
                    mapAux.put("id",otId);
                    map.put("ot",mapAux);
                    String observaciones = "Se FINALIZA OT número "+otId+" con fecha " +obtienefechaSegunFormato(new Date(), "dd-MM-yyyy hh:mm:ss")+" Informe por Sistema OTEC";
                    map.put("observaciones",observaciones);

                    LibroObras libroObras = new LibroObras();
                    libroObras.setObservaciones(observaciones);
                    libroObras.setOt(ot);
                    libroObras.setFechaCreacion(new Date());
                    String[] roles = new String[8];
                    roles[0] = "2";
                    roles[1] = "4";
                    roles[2] = "5";
                    roles[3] = "7";
                    roles[4] = "10";
                    roles[5] = "11";
                    roles[6] = "3";
                    roles[7] = "14";
                    List<ArchivoAux> listadoArchivo = new ArrayList<>();
                    LibroObraDAO.gestionarAdjuntoLibroObraOt2(listadoArchivo, libroObras, roles, usuarioEjecutor, otId);
                }
            } finally {
                contAccionesBBDD.getAndIncrement();
            }

            
        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }

    private int buscarData(String campo1, String campo2, Map params) {
        Map padre = (Map) params.get(campo1);
        String valor = (String) padre.get(campo2);
        return Integer.parseInt(valor);
    }

    private DetalleParametros retornarDetalleParametros(Acciones accion) {
        TreeSet<AccionesParametros> accionesParametros = (TreeSet) accion.getAccionesParametros();
        AccionesParametros accionParam = (AccionesParametros) accionesParametros.first();
        Parametros parametro = accionParam.getParametros();
        List<DetalleParametros> listaDetalleParametros = new ArrayList<>(parametro.getDetalleParametros());
        return listaDetalleParametros.get(0);
    }
    private String obtienefechaSegunFormato(Date fecha, String formatoSalida) {
        String fechaString = "";
        if (fecha == null) {
            return fechaString;
        } else {
            try {
                DateFormat formato = new SimpleDateFormat(formatoSalida);
                fechaString = formato.format(fecha);
                //System.out.println("====================>  Fecha de salida :  "+fechaString);
                return fechaString;
            } catch (Exception e) {
                fechaString = fecha.toString();
                return fechaString;
            }
        }
    }
    
    private static final Logger log = Logger.getLogger(AccionContinuarFlujoWEE.class);
    
}
