/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.dao.CubicadorDAO;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.HibernateException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionCrearOTConstruccionLLave extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        int idOt = 0;
        try {
            CoreOtFactory coreOtFactory = new CoreOtFactory();

            Map cubicacion = (Map) params.get("cubicacion");
            Map servicio = (Map) params.get("servicio");
            Map mapaOT = (Map) params.get("datosOt");
            int otId = (int)mapaOT.get("id");
            int eventoId = (int) params.get("idEvento");
            int usuarioId = (int) params.get("usuarioId");
            int gestorConstruccionId = usuarioId;
            int gestorAntiguo = (int) params.get("gestorId");
            String obs = (String) params.get("obs");

            try {

                // crear la cubicacion
                Map parametrosCubicacion = new HashMap();
                int id = (int)cubicacion.get("id");
                Cubicador cubicacionAntigua = CubicadorDAO.getINSTANCE().obtenerDetalleCubicacionLLave(id,otId);

                parametrosCubicacion.put("nombre",cubicacionAntigua.getNombre()+"-construccion");
                parametrosCubicacion.put("descripcion",cubicacionAntigua.getDescripcion()+"-construccion");

                Map region = new HashMap();
                region.put("id", cubicacionAntigua.getRegion().getId());
                parametrosCubicacion.put("region",region);

                Map proveedor = new HashMap();
                proveedor.put("id", cubicacionAntigua.getProveedor().getId());
                parametrosCubicacion.put("proveedor",proveedor);

                Map contrato = new HashMap();
                contrato.put("id", cubicacionAntigua.getContrato().getId());
                parametrosCubicacion.put("contrato",contrato);

                Map usuario = new HashMap();
                usuario.put("id", usuarioId);
                parametrosCubicacion.put("usuario",usuario);


                Set<CubicadorServicios> listadoServicios =  cubicacionAntigua.getCubicadorServicios();
                List<Map> listadoServiciosFinal = new ArrayList<>();

                for (CubicadorServicios cubServicio : listadoServicios) {
                    cubServicio.setCantidad((int)servicio.get("cantidadFinal"));
                    Map mapServ = new HashMap();
                    Map servicioAux = new HashMap();
                    servicioAux.put("id",cubServicio.getServicio().getId());
                    servicioAux.put("precio",cubServicio.getPrecio());
                    mapServ.put("servicio",servicioAux);
                    mapServ.put("cantidad",cubServicio.getCantidad());
                    listadoServiciosFinal.add(mapServ);
                }

                Set<CubicadorMateriales> listadoMateriales =  cubicacionAntigua.getCubicadorMateriales();
                List<Map> listadoMaterialesFinal = new ArrayList<>();

                for (CubicadorMateriales cubMaterial : listadoMateriales) {
                    Map mapMat = new HashMap();
                    Map materiallAux = new HashMap();
                    materiallAux.put("id",cubMaterial.getMaterial().getId());
                    materiallAux.put("precio",cubMaterial.getMontoTotal());
                    mapMat.put("material",materiallAux);
                    mapMat.put("cantidad",cubMaterial.getCantidad());
                    listadoMaterialesFinal.add(mapMat);
                }

                parametrosCubicacion.put("cubicadorServicios",listadoServiciosFinal);
                parametrosCubicacion.put("cubicadorMateriales",listadoMaterialesFinal);

                int idCubicacion = CubicadorDAO.getINSTANCE().crearCubicacionLlaveEnMano(parametrosCubicacion);
                // crear la ot
                ObjetoCoreOT objetoCoreOT = coreOtFactory.getObjetoCoreOt("Ot");
                Ot ot = (Ot) objetoCoreOT;
                Map objGestor = (Map) params.get("gestor");
                objGestor.remove("rut");
                params.remove("gestor");
                params.put("gestor",objGestor);
                params.remove("fechaCreacion");
                Date fechaCreacion = new Date();

                params.put("fechaCreacion",obtienefechaSegunFormato(fechaCreacion,"dd-MM-yyyy"));


                Map numeroAp = new HashMap();
                numeroAp.put("id", otId);
                params.put("numeroAp",numeroAp);


                JSONObject jsonDetalleOtAux = new JSONObject((String)params.get("jsonDetalleOt"));
                Map detalleOt = new HashMap();
                JSONObject idPmoAux = (JSONObject)jsonDetalleOtAux.getJSONObject("idPmo");
                Map idPmo = new HashMap();
                idPmo.put("id", idPmoAux.get("id"));

                JSONObject lpAux = (JSONObject)jsonDetalleOtAux.get("lp");
                Map lp = new HashMap();
                lp.put("lp", lpAux.get("lp"));

                JSONObject pep2Aux = (JSONObject)jsonDetalleOtAux.get("pep2");
                Map pep2 = new HashMap();
                pep2.put("pep2", pep2Aux.get("id"));



                JSONObject sitio = (JSONObject)jsonDetalleOtAux.get("sitio");

                String avba = (String)jsonDetalleOtAux.get("avba");
                String derivada = (String)jsonDetalleOtAux.get("derivada");

                detalleOt.put("pep2", pep2);
                detalleOt.put("idPmo", idPmo);
                detalleOt.put("lp", lp);
                detalleOt.put("avba", avba);
                detalleOt.put("derivada", derivada);
                detalleOt.put("direccion", sitio.get("direccion"));
                detalleOt.put("latitud", sitio.get("latitud"));
                detalleOt.put("longitud", sitio.get("longitud"));

                params.remove("jsonDetalleOt");
                params.put("jsonDetalleOt", detalleOt);

                Map estadoWorkflow = new HashMap();
                estadoWorkflow.put("id", 1);
                params.put("estadoWorkflow", estadoWorkflow);

                Map central = (Map) params.get("central");
                params.put("central", central);

                Map tipoSolicitud = new HashMap();
                tipoSolicitud.put("id", 1);
                params.put("tipoSolicitud", tipoSolicitud);

                Map subGerente = new HashMap();
                // el 53 es el id de Rodrigo Cancino
                subGerente.put("id", 53);
                params.put("subGerente", subGerente);


                ot.setearObjetoDesdeMap(params, 0);

                ot.setId(0);
                ot.setIdAp(otId);

                ot.setNombre(ot.getNombre() + "- Construcción");
                Usuarios usuarioGestor = new Usuarios();
                usuarioGestor.setId(gestorConstruccionId);
                ot.setGestor(usuarioGestor);
                int otNuevaId = objetoCoreOT.obtenerGatewayDAO().crearRetornarId(ot, params, usuarioId);


                // actualizar cubicacion
                GatewayAcciones gatewayAcciones = new GatewayAcciones();
                gatewayAcciones.abrirSesion();
                gatewayAcciones.iniciarTransaccion();

                boolean actualizoCubicacion = gatewayAcciones.actualizarCubicacion(otNuevaId, idCubicacion);

                // dejar la cubicacion de la ot antigua en cero
                boolean actualizoCubicacionServicio = gatewayAcciones.actualizarCubicacionServicios(id);

                // cierro la ot original
                boolean cerroOt = gatewayAcciones.cerratOt(otId);

                // Avanzo la Ot antigua a cierre
                boolean actualizoWee = gatewayAcciones.actualizarWorkfloEventoEjecucionCierre(otId, 8);

                // Obtengo los usuario, usuarios validadores y workfloweventos de la ot
                boolean completoDataOt = gatewayAcciones.commpletarDataOt(otId, otNuevaId, idCubicacion, gestorAntiguo, gestorConstruccionId, cubicacionAntigua.getProveedor().getId());

                if(actualizoCubicacion && cerroOt && actualizoWee && completoDataOt){
                    gatewayAcciones.commit();
//                    throw new Exception();
                }else{
                    throw new Exception();
                }


            } finally {
                contAccionesBBDD.getAndIncrement();
            }


        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }

    
    private static final Logger log = Logger.getLogger(AccionCrearOTConstruccionLLave.class);

    private String obtienefechaSegunFormato(Date fecha, String formatoSalida) {
        String fechaString = "";
        if (fecha.equals("null")) {
            return fecha.toString();
        } else {
            try {
                DateFormat formato = new SimpleDateFormat(formatoSalida);
                fechaString = formato.format(fecha);
                //System.out.println("====================>  Fecha de salida :  "+fechaString);
                return fechaString;
            } catch (Exception e) {
                fechaString = "error: "+e;
                return fechaString;
            }
        }
    }
    
}
