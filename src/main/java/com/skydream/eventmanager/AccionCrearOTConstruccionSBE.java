/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.dao.CubicadorDAO;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.HibernateException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionCrearOTConstruccionSBE extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        int idOt = 0;
        try {
            CoreOtFactory coreOtFactory = new CoreOtFactory();
            Map detalleOtParams = (Map) params.get("detalleOt");
            Map cubicacion = (Map) detalleOtParams.get("cubicacion");
            Map contratoMap = (Map) params.get("contrato");
            int otId = (int)params.get("otId");
            int eventoId = (int) params.get("idEvento");
            int usuarioId = (int) params.get("usuarioId");
            int gestorConstruccionId = usuarioId;
            int gestorAntiguo = usuarioId;
            int contratoId = (int) contratoMap.get("id");
            int actaId = (int) detalleOtParams.get("actaId");
            String obs = (String) params.get("obs");

            try {

                // crear la cubicacion
                Map parametrosCubicacion = new HashMap();
                int cubicadorId = (int)cubicacion.get("id");
                Cubicador cubicacionAntigua = CubicadorDAO.getINSTANCE().obtenerDetalleCubicacionLLave(cubicadorId,otId);

                parametrosCubicacion.put("nombre",cubicacionAntigua.getNombre()+" - Ot");
                parametrosCubicacion.put("descripcion",cubicacionAntigua.getDescripcion()+" - Ot");

                Map region = new HashMap();
                region.put("id", cubicacionAntigua.getRegion().getId());
                parametrosCubicacion.put("region",region);

                Map proveedor = new HashMap();
                proveedor.put("id", cubicacionAntigua.getProveedor().getId());
                parametrosCubicacion.put("proveedor",proveedor);

                Map contrato = new HashMap();
                contrato.put("id", cubicacionAntigua.getContrato().getId());
                parametrosCubicacion.put("contrato",contrato);

                Map usuario = new HashMap();
                usuario.put("id", usuarioId);
                parametrosCubicacion.put("usuario",usuario);

                GatewayAcciones gatewayAcciones = new GatewayAcciones();
                gatewayAcciones.abrirSesion();
                gatewayAcciones.iniciarTransaccion();

                Set<CubicadorServicios> listadoServicios =  cubicacionAntigua.getCubicadorServicios();
                List<Map> listadoServiciosFinal = new ArrayList<>();

                List<DetalleServiciosActas> detalleServiciosActasList = new ArrayList<>();
                List<DetalleServiciosAdicionalesActas> detalleServiciosAdicionalesActas = new ArrayList<>();

                detalleServiciosActasList = gatewayAcciones.obtieneDetalleServiciosEjecutados(actaId);
                detalleServiciosAdicionalesActas = gatewayAcciones.obtieneDetalleServiciosAdicionalesEjecutados(actaId);

                for (CubicadorServicios cubServicio : listadoServicios) {
                    Map mapServ = new HashMap();
                    Map servicioAux = new HashMap();
                    servicioAux.put("id",cubServicio.getServicio().getId());
                    servicioAux.put("precio",cubServicio.getPrecio());
                    mapServ.put("servicio",servicioAux);
                    for(DetalleServiciosActas detSerAct : detalleServiciosActasList){
                        if(detSerAct.getServicio().getId() == cubServicio.getServicio().getId()){
                            mapServ.put("cantidad", detSerAct.getTotalUnidadesEjecucionActa());
                        }
                    }

                    listadoServiciosFinal.add(mapServ);
                }
                for (DetalleServiciosAdicionalesActas detalleAdicionalesActa : detalleServiciosAdicionalesActas) {
                    Map mapServ = new HashMap();
                    Map servicioAux = new HashMap();

                    servicioAux.put("id",detalleAdicionalesActa.getServicio().getId());
                    double precioAdicionalUni = (detalleAdicionalesActa.getTotalMontoEjecucionActa() / detalleAdicionalesActa.getTotalUnidadesEjecucionActa());
                    servicioAux.put("precio",precioAdicionalUni);
                    mapServ.put("cantidad", detalleAdicionalesActa.getTotalUnidadesEjecucionActa());
                    mapServ.put("servicio",servicioAux);

                    listadoServiciosFinal.add(mapServ);
                }

                Set<CubicadorMateriales> listadoMateriales =  cubicacionAntigua.getCubicadorMateriales();
                List<Map> listadoMaterialesFinal = new ArrayList<>();

                for (CubicadorMateriales cubMaterial : listadoMateriales) {
                    Map mapMat = new HashMap();
                    Map materiallAux = new HashMap();
                    materiallAux.put("id",cubMaterial.getMaterial().getId());
                    materiallAux.put("precio",cubMaterial.getMontoTotal());
                    mapMat.put("material",materiallAux);
                    mapMat.put("cantidad",cubMaterial.getCantidad());
                    listadoMaterialesFinal.add(mapMat);
                }
                List<Map> listadoServiciosFinalMapList = new ArrayList<>();
                for(int i = 0;  i<listadoServiciosFinal.size(); i++){
                    Map mapMat = new HashMap();
                    boolean repetido = false;
                    double cantidadAdicional = 0;
                    Map servicio = (HashMap) listadoServiciosFinal.get(i);
                    Map servicioIdMap = (HashMap) servicio.get("servicio");
                    for(int u = 0;  u<listadoServiciosFinal.size(); u++){
                        Map servicio2 = (HashMap) listadoServiciosFinal.get(u);
                        Map servicioIdMap2 = (HashMap) servicio2.get("servicio");
                        if(i != u && (servicioIdMap2.get("id").toString().equals(servicioIdMap.get("id").toString())) ){
//                            System.out.println(servicioIdMap2.get("id"));
//                            System.out.println(servicioIdMap.get("id"));
                            repetido = true;
                            cantidadAdicional = Double.parseDouble(listadoServiciosFinal.get(u).get("cantidad").toString()) + Double.parseDouble(listadoServiciosFinal.get(i).get("cantidad").toString());
                            servicio2.put("cantidad", cantidadAdicional);
                            listadoServiciosFinalMapList.add(servicio2);
                            listadoServiciosFinal.remove(u);
                        }
                    }
                    if(!repetido){
                        listadoServiciosFinalMapList.add(listadoServiciosFinal.get(i));
                    }

                }

                parametrosCubicacion.put("cubicadorServicios",listadoServiciosFinalMapList);
                parametrosCubicacion.put("cubicadorMateriales",listadoMaterialesFinal);

                int nuevaCubicacionId = CubicadorDAO.getINSTANCE().crearCubicacionLlaveEnMano(parametrosCubicacion);
                // crear la ot
                ObjetoCoreOT objetoCoreOT = coreOtFactory.getObjetoCoreOt("Ot");
                Ot ot = (Ot) objetoCoreOT;
                Map objGestor = (Map) params.get("gestor");
                objGestor.remove("rut");
                params.remove("gestor");
                params.put("gestor",objGestor);
                params.remove("fechaCreacion");
                Date fechaCreacion = new Date();

                params.put("fechaCreacion",obtienefechaSegunFormato(fechaCreacion,"dd-MM-yyyy"));


                Map numeroAp = new HashMap();
                numeroAp.put("id", otId);
                params.put("numeroAp",numeroAp);


                JSONObject jsonDetalleOtAux = new JSONObject((String)params.get("jsonDetalleOt"));
                Map detalleOt = new HashMap();

                int idPmo = (int) params.get("idPmo");
                String pep2 = (String) params.get("pep2");
                String lp = (String) params.get("lp");

                Map idPmoMap = new HashMap();
                idPmoMap.put("id", idPmo);

                Map lpMap = new HashMap();
                lpMap.put("lp", lp);

                Map pep2Map = new HashMap();
                pep2Map.put("pep2", pep2);

                String avba = "";
                String derivada = "";
                if(contratoId == 5) {
                    avba = (String) jsonDetalleOtAux.get("avba");
                    derivada = (String)jsonDetalleOtAux.get("derivada");
                }

                JSONObject planDeProyecto = (JSONObject)jsonDetalleOtAux.get("planDeProyecto");
                Map planDeproyectoMap = new HashMap();
                planDeproyectoMap.put("id", planDeProyecto.get("id"));
                planDeproyectoMap.put("nombre", planDeProyecto.get("nombre"));

                JSONObject sitioObjectMap = (JSONObject)jsonDetalleOtAux.get("sitio");
                Map sitioMap = new HashMap();
                sitioMap.put("id", sitioObjectMap.get("id"));
                sitioMap.put("codigo", sitioObjectMap.get("codigo"));
                sitioMap.put("nombre", sitioObjectMap.get("nombre"));
                sitioMap.put("direccion", sitioObjectMap.get("direccion"));
                sitioMap.put("latitud", sitioObjectMap.get("latitud"));
                sitioMap.put("longitud", sitioObjectMap.get("longitud"));

                detalleOt.put("pep2", pep2Map);
                detalleOt.put("idPmo", idPmoMap);
                detalleOt.put("lp", lpMap);
                detalleOt.put("avba", avba);
                detalleOt.put("derivada", derivada);
                detalleOt.put("direccion", sitioObjectMap.get("direccion"));
                detalleOt.put("latitud", sitioObjectMap.get("latitud"));
                detalleOt.put("longitud", sitioObjectMap.get("longitud"));
                detalleOt.put("planDeProyecto", planDeproyectoMap);
                detalleOt.put("sitio", sitioMap);

                params.remove("jsonDetalleOt");
                params.put("jsonDetalleOt", detalleOt);

                Map estadoWorkflow = new HashMap();
                estadoWorkflow.put("id", 1);
                params.put("estadoWorkflow", estadoWorkflow);

                if(contratoId != 1){
                    Map central = (Map) params.get("central");
                    params.put("central", central);
                } else {
                    Map centralMap = new HashMap();
                    centralMap.put("id", 0);
                }

                Map tipoSolicitud = new HashMap();
                tipoSolicitud.put("id", 1);
                params.put("tipoSolicitud", tipoSolicitud);

                Map subGerente = new HashMap();
                // el 53 es el id de Rodrigo Cancino
                subGerente.put("id", 53);
                params.put("subGerente", subGerente);


                ot.setearObjetoDesdeMap(params, 0);

                ot.setId(0);
                ot.setIdAp(otId);
                switch (contratoId){
                    case 1:
                        ot.setNombre(ot.getNombre() + "- Ot con Diseño");
                        break;
                    default:
                        ot.setNombre(ot.getNombre() + "- Construcción");
                        break;
                }

                Usuarios usuarioGestor = new Usuarios();
                usuarioGestor.setId(gestorConstruccionId);
                ot.setCentral(null);
//                Se setea un estado activo para la nueva ot
                TipoEstadoOt tipoEstadoOt = new TipoEstadoOt();
                tipoEstadoOt.setId(1);
//                Se setea un tipo de OT que indica que es OT y no AP
                TipoOt tipoOt = new TipoOt();
                tipoOt.setId(1);

                Contratos contratos = new Contratos();
                contratos.setId(contratoId);
                ot.setTipoEstadoOt(tipoEstadoOt);
                ot.setGestor(usuarioGestor);
                ot.setIdAp(otId);
                ot.setNumeroAp(otId);
                ot.setTipoOt(tipoOt);
                Ot otOld = new Ot();
                otOld.setId(otId);
                otOld.setContrato(contratos);

                int otNuevaId = objetoCoreOT.obtenerGatewayDAO().crearRetornarId(ot, params, usuarioId);


                // actualizar cubicacion

                boolean actualizoCubicacion = gatewayAcciones.actualizarCubicacion(otNuevaId, nuevaCubicacionId);

                // dejar la cubicacion de la ot antigua en cero
                boolean actualizoCubicacionServicio = gatewayAcciones.actualizarCubicacionyActaServiciosCrearOtAp(cubicadorId, actaId);

                // cierro la ot original
                boolean cerroOt = gatewayAcciones.cerratOt(otId);

                // Avanzo la Ot antigua a cierre
                boolean actualizoWee = gatewayAcciones.actualizarWorkfloEventoEjecucionCierre(otId, eventoId);

                // Obtengo los usuario, usuarios validadores y workfloweventos de la ot
                boolean completoDataOt = gatewayAcciones.crearNuevoFlujoOtConstruccion(otOld, otNuevaId, nuevaCubicacionId, gestorAntiguo, gestorConstruccionId, cubicacionAntigua.getProveedor().getId());

                if(actualizoCubicacion && cerroOt && actualizoWee && completoDataOt){
                    gatewayAcciones.commit();
//                    throw new Exception();
                    gatewayAcciones.cerrarSesion();
                }else{
                    gatewayAcciones.cerrarSesion();
                    throw new Exception();
                }


            } finally {

//                throw new Exception("prueba nuevo Flujo SBE por JonsTRap2016NonStop");
                contAccionesBBDD.getAndIncrement();
            }


        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }

    
    private static final Logger log = Logger.getLogger(AccionCrearOTConstruccionSBE.class);

    private String obtienefechaSegunFormato(Date fecha, String formatoSalida) {
        String fechaString = "";
        if (fecha.equals("null")) {
            return fecha.toString();
        } else {
            try {
                DateFormat formato = new SimpleDateFormat(formatoSalida);
                fechaString = formato.format(fecha);
                //System.out.println("====================>  Fecha de salida :  "+fechaString);
                return fechaString;
            } catch (Exception e) {
                fechaString = "error: "+e;
                return fechaString;
            }
        }
    }
    
}
