package com.skydream.eventmanager;

import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by mcj on 26-10-15.
 */
public class AccionGenerarActaBucle extends Command{

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {

        Map mapaOT = (Map) params.get("datosOt");
        int otId = (int) mapaOT.get("id");
        int usuarioId = (int) params.get("usuarioId");
        Ot ot = new Ot();
        ot.setId(otId);
        boolean existe = false;

        List<ObjetoCoreOT> listadoActasAnteriores = getGateway().listarConFiltro(Actas.class, null, "ot.id", ot.getId(), "id");
        List<Actas> listadoActasValidadas = getGateway().obtenerListadoActasValidadas(ot.getId());

        Actas acta = new Actas();
        acta.setOt(ot);
        acta.setFecha(new Date());

        Number idActa = 0;

        try {
            getGateway().persistirObjeto(acta);
            idActa = acta.getId();
        } finally {
            contAccionesBBDD.incrementAndGet();
        }
        acta.setId(idActa.intValue());
        Cubicador cubicador = (Cubicador)getGateway().obtenerRegistroConFiltro(Cubicador.class, null, "ot.id", otId);
        CubicadorDetalle cubiAux = new CubicadorDetalle();
        // obtengo listado de item de cubicacion orginal ordinaria
        List<CubicadorDetalle> mapaConsolidadoMaterialesOt = null;
        mapaConsolidadoMaterialesOt = getGateway().obtenerCubicadorDetallePorIdCubicadorAcciones(cubicador.getId());
        double montoTotalInformadoActa = 0.0;
        double montoTotalInformadoCubicadoActa = 0.0;
        double cantidadCubicadaTotal = 0.0;
        double montoTotalAdicionales = 0.0;
        //obtengo el listado de item con cantidad informada desde el front
        List<LinkedHashMap> arrayMaterialesUtilizadosActa = (List)params.get("servicios");
        // recorro el listado y creo el objeto
        List<ItemOrdinarioAux> listadoItemInformado = new ArrayList<>();

        int posUltiActaValidada = listadoActasValidadas.size();
        Actas ultimaActaValidada = new Actas();
        int montoHistoricoAprobado = 0;
        if(posUltiActaValidada > 0){
            ultimaActaValidada = (Actas) listadoActasValidadas.get((posUltiActaValidada - 1));
        }

        for (LinkedHashMap itemInformadoAux: arrayMaterialesUtilizadosActa){

            Set set = itemInformadoAux.entrySet();
            Iterator i = set.iterator();
            ItemOrdinarioAux itemOrdinarioAux = new ItemOrdinarioAux();
            while(i.hasNext()) {
                Map.Entry me = (Map.Entry)i.next();
                boolean inserto = false;
                switch ((String)me.getKey()) {
                    case "id":
                        itemOrdinarioAux.setId((Integer) me.getValue());
                        break;
                    case "cantidadReal":
                        try {
                            itemOrdinarioAux.setCantidadInformada((Integer) me.getValue());
                            inserto = true;
                        }catch (Exception e){
                            // no lanzo excepcion porq puede ser double el valor
                        }
                        if(!inserto){
                            itemOrdinarioAux.setCantidadInformada((Double) me.getValue());
                        }
                        break;
                }
            }
            cubiAux = getGateway().obtenerCubicadorDetallePorId(itemOrdinarioAux.getId());

            montoTotalInformadoCubicadoActa = montoTotalInformadoCubicadoActa + (cubiAux.getPrecio() * itemOrdinarioAux.getCantidadInformada());
            listadoItemInformado.add(itemOrdinarioAux);
        }

        boolean validacionSistemaTotal = true;
        List<LinkedHashMap> arrayMaterialesUtilizadosActaAdicionales = (List)params.get("serviciosAdicionales");


        for (CubicadorDetalle itemOriginal: mapaConsolidadoMaterialesOt){
            cantidadCubicadaTotal = cantidadCubicadaTotal + itemOriginal.getTotal();
            int idItemCubicado = itemOriginal.getId();
            for(ItemOrdinarioAux itemInformado: listadoItemInformado){
                // busco el item cubicado en informado
                if(itemInformado.getId() ==  idItemCubicado){
                    // Instancio el objeto con los datos correspondientes
                    CubicadorDetalleUnificadoActas cubicadorDetalleUnificadoActasUltimo = new CubicadorDetalleUnificadoActas();
                    CubicadorDetalleUnificadoActasId cubicadorDetalleUnificadoActasIdUltimo = new CubicadorDetalleUnificadoActasId();

                    CubicadorDetalleUnificadoActas cubicadorDetalleUnificadoActas = new CubicadorDetalleUnificadoActas();
                    CubicadorDetalleUnificadoActasId cubicadorDetalleUnificadoActasId = new CubicadorDetalleUnificadoActasId();

                    if(posUltiActaValidada > 0) {
                        cubicadorDetalleUnificadoActasIdUltimo.setActaId(ultimaActaValidada.getId());
                        cubicadorDetalleUnificadoActasIdUltimo.setCubicadorDetalleId(itemInformado.getId());
                        cubicadorDetalleUnificadoActasUltimo = getGateway().obtieneCubicadorDetalleUnificadoActas(cubicadorDetalleUnificadoActasIdUltimo);
                        cubicadorDetalleUnificadoActas.setTotalUnidadesEjecucionOt(cubicadorDetalleUnificadoActasUltimo.getTotalUnidadesEjecucionOt());
                    } else {
                        cubicadorDetalleUnificadoActas.setTotalUnidadesEjecucionOt(0.0);
                    }

                    cubicadorDetalleUnificadoActasId.setActaId(acta.getId());
                    cubicadorDetalleUnificadoActasId.setCubicadorDetalleId(idItemCubicado);
                    cubicadorDetalleUnificadoActas.setId(cubicadorDetalleUnificadoActasId);

                    cubicadorDetalleUnificadoActas.setTotalMontoCubicado(itemOriginal.getTotal());
                    double montoInformado = itemOriginal.getPrecio() * itemInformado.getCantidadInformada();
                    cubicadorDetalleUnificadoActas.setTotalMontoEjecucion(montoInformado);
                    cubicadorDetalleUnificadoActas.setTotalUnidadesCubicadas(itemOriginal.getCantidad());

                    double cantidadInformada = itemInformado.getCantidadInformada();
                    List<LinkedHashMap> cubicadorDetalleAdicionalCosteo = new ArrayList();
                    cubicadorDetalleAdicionalCosteo = obtieneAdicionalesCubicados(itemOriginal, itemInformado);
                    if(cubicadorDetalleAdicionalCosteo != null){
                        arrayMaterialesUtilizadosActaAdicionales.add(cubicadorDetalleAdicionalCosteo.get(0));
                        cantidadInformada = itemOriginal.getCantidad();
                    }
                    cubicadorDetalleUnificadoActas.setTotalUnidadesEjecucionActa(cantidadInformada);


                    boolean validacionSistema = false;
                    if(itemOriginal.getCantidad() == itemInformado.getCantidadInformada()){
                        validacionSistema = true;
                    }else{
                        validacionSistemaTotal = false;
                    }
                    cubicadorDetalleUnificadoActas.setValidacionSistema(validacionSistema);

                    // inserto el registro con los datos correspondientes
                    boolean inserto = getGateway().registrarDetalleCubicacionUnificado(cubicadorDetalleUnificadoActas);
                }
            }
        }
        montoTotalInformadoActa = montoTotalInformadoCubicadoActa;
        if(arrayMaterialesUtilizadosActaAdicionales.size() > 0){
            List<ItemOrdinarioAux> listadoItemInformadoAdicional = new ArrayList<>();
            montoTotalAdicionales = guardarMaterialesAdicionales(arrayMaterialesUtilizadosActaAdicionales, cubicador.getId(), cubicador.getProveedor().getId(), contAccionesBBDD);
            montoTotalInformadoActa = montoTotalInformadoActa + montoTotalAdicionales;
        } else {
            eliminaAdicionalesAnterioresParaActualizar(cubicador.getId(), contAccionesBBDD);
        }
        acta.setMontoTotalActaCubicado(montoTotalInformadoCubicadoActa);
        acta.setMontoCubicado(cantidadCubicadaTotal);
        acta.setValidacionSistema(validacionSistemaTotal);
        acta.setMontoTotalActa(montoTotalInformadoActa);
        acta.setMontoTotalAPagar(montoTotalInformadoActa);
        acta.setMontoTotalActaAdicional(montoTotalAdicionales);


        String obs = params.get("obs").toString();
        Date fecha = new Date();
        String fechaFormat = obtienefechaSegunFormato(fecha, "dd-MM-yyyy");
        Usuarios usuario = null;
        usuario = (Usuarios) getGateway().retornarObjetoCoreOT(Usuarios.class, usuarioId);

        String observaciones = "{\n" +
                "  \"usuario\": "  + "\"" + usuario.getNombres() + " " + usuario.getApellidos() + "\"" + ",\n" +
                "  \"fecha\": "  + "\"" + fechaFormat + "\"" + ",\n" +
                "  \"observaciones\": "  + "\"" + obs + "\""  +
                "  }\n";
        int posUltiActa = listadoActasAnteriores.size();
        Actas ultimaActa = new Actas();
        String obsOld = "";
        if(posUltiActa > 0){
            ultimaActa = (Actas) listadoActasAnteriores.get((posUltiActa - 1));
            obsOld = ultimaActa.getObservaciones();
        } else {
            obsOld = null;
        }

        String observacionNueva = "";
        if(obsOld == null){
             observacionNueva = "[" + observaciones + "]";
        }else{
            obsOld = obsOld.replace("]",",");
            observacionNueva = obsOld + observaciones + "]";
        }
        acta.setObservaciones(observacionNueva);
//        throw new Exception("Error de prueba Generar Acta Unificado");

        getGateway().actualizarWEE(otId);

    }
    private void eliminaAdicionalesAnterioresParaActualizar(int cubicadorId, AtomicInteger contadorBD){

        try{
            getGateway().eliminaAdicionalesAnterioresParaActualizar(cubicadorId, contadorBD);
        } catch (Exception e){
            throw e;
        }
    }
    private List<LinkedHashMap> obtieneAdicionalesCubicados(CubicadorDetalle cubicadorDetalleCosteado, ItemOrdinarioAux itemOrdinarioAux){
        double cantidadAdicional = 0;
        double total = 0;
        int cantidad = 0;
        CubicadorDetalle cubicadorDetalle = new CubicadorDetalle();
        cubicadorDetalle.setServicios(cubicadorDetalleCosteado.getServicios());
        cubicadorDetalle.setPrecio(cubicadorDetalleCosteado.getPrecio());
        cubicadorDetalle.setRegiones(cubicadorDetalleCosteado.getRegiones());
        cubicadorDetalle.setTipoMoneda(cubicadorDetalleCosteado.getTipoMoneda());
        cubicadorDetalle.setAgencia(cubicadorDetalleCosteado.getAgencia());
        cubicadorDetalle.setCentrales(cubicadorDetalleCosteado.getCentrales());
        cubicadorDetalle.setCantidad(cubicadorDetalleCosteado.getCantidad());
        cubicadorDetalle.setAgenciaId(cubicadorDetalleCosteado.getAgenciaId());
        cubicadorDetalle.setCentralId(cubicadorDetalleCosteado.getCentralId());


        List<LinkedHashMap> detalleAdicionalList = new ArrayList();
        if(cubicadorDetalle.getCantidad() < itemOrdinarioAux.getCantidadInformada()){
            cantidadAdicional = itemOrdinarioAux.getCantidadInformada() - cubicadorDetalle.getCantidad();
            total = cubicadorDetalle.getPrecio() * cantidadAdicional;

            cubicadorDetalle.setCantidad(cantidadAdicional);
            cubicadorDetalle.setTotal(total);

            LinkedHashMap servicioMap = new LinkedHashMap();
            LinkedHashMap servicioMapOInt = new LinkedHashMap();
            servicioMapOInt.put("id", cubicadorDetalle.getServicios().getId());
            servicioMapOInt.put("precio", cubicadorDetalle.getPrecio());

            LinkedHashMap regionLinkedMap = new LinkedHashMap();
            regionLinkedMap.put("id", cubicadorDetalle.getRegiones().getId());
            servicioMapOInt.put("region", regionLinkedMap);

            LinkedHashMap tipoMonedaLinkedMap = new LinkedHashMap();
            tipoMonedaLinkedMap.put("id", cubicadorDetalle.getTipoMoneda().getId());
            servicioMapOInt.put("tipoMoneda", tipoMonedaLinkedMap);

            LinkedHashMap tipoServicioLinkedMap = new LinkedHashMap();
            tipoServicioLinkedMap.put("id", cubicadorDetalle.getServicios().getTipoServicio().getId());
            servicioMapOInt.put("tipoServicio", tipoServicioLinkedMap);

            servicioMap.put("servicio", servicioMapOInt);


            LinkedHashMap agenciaLinkedMap = new LinkedHashMap();
            agenciaLinkedMap.put("id", cubicadorDetalle.getAgenciaId());
            servicioMap.put("agencia", agenciaLinkedMap);

            LinkedHashMap centralLinkedMap = new LinkedHashMap();
            centralLinkedMap.put("id", cubicadorDetalle.getCentralId());
            servicioMap.put("central", centralLinkedMap);


            cantidad = (int)cubicadorDetalle.getCantidad();
            servicioMap.put("cantidad", cantidad);


            detalleAdicionalList.add(servicioMap);

        } else {
            detalleAdicionalList = null;
        }
        return detalleAdicionalList;
    }
    private String obtienefechaSegunFormato(Date fecha, String formatoSalida) {
        String fechaString = "";
        if (fecha.equals("null")) {
            return fecha.toString();
        } else {
            try {
                DateFormat formato = new SimpleDateFormat(formatoSalida);
                fechaString = formato.format(fecha);
                //System.out.println("====================>  Fecha de salida :  "+fechaString);
                return fechaString;
            } catch (Exception e) {
                fechaString = "error: "+e;
                return fechaString;
            }
        }
    }
 private double guardarMaterialesAdicionales(List<LinkedHashMap> arrayMaterialesUtilizadosActa, int idCubicador, int proveedorId, AtomicInteger contAccionesBBDD )throws Exception{
     double montoTotalAdicionales = 0.0;

     try {
         getGateway().eliminaAdicionalesAnterioresParaActualizar(idCubicador, contAccionesBBDD);
        for(Map mapaAdicionales : arrayMaterialesUtilizadosActa){
            Map servicioMap = (HashMap) mapaAdicionales.get("servicio");
            Map region = (HashMap) servicioMap.get("region");
            Map agencia = (HashMap) mapaAdicionales.get("agencia");
            Map central = (HashMap) mapaAdicionales.get("central");
            Map tipoMoneda = (HashMap) servicioMap.get("tipoMoneda");
            Map tipoServicioMap = (HashMap) servicioMap.get("tipoServicio");
            int cantidad = Integer.parseInt(mapaAdicionales.get("cantidad").toString());
            int idServicio = (Integer) servicioMap.get("id");
            int idRegion = Integer.parseInt(region.get("id").toString());
            int idAgencia = (Integer) agencia.get("id");
            int idCentral = (Integer) central.get("id");
            int idTipoMoneda = (Integer) tipoMoneda.get("id");
            int idTipoServicio = (Integer) tipoServicioMap.get("id");
            ServiciosAux serviciosAux = new ServiciosAux();
            serviciosAux = getGateway().obtieneServicioPorProveedorContratoRegion(idServicio, 4, idRegion, proveedorId, idTipoServicio);
            double precio = serviciosAux.getPrecio();
            double total = precio * cantidad;
            Date date = new Date();

            CubicadorDetalleAdicionales cubicadorDetalleAdicionales = new CubicadorDetalleAdicionales();
            cubicadorDetalleAdicionales.setServicioId(idServicio);
            cubicadorDetalleAdicionales.setRegionId(idRegion);
            cubicadorDetalleAdicionales.setAgenciaId(idAgencia);
            cubicadorDetalleAdicionales.setCentralId(idCentral);
            cubicadorDetalleAdicionales.setTipoMonedaId(idTipoMoneda);
            cubicadorDetalleAdicionales.setCantidad(cantidad);
            cubicadorDetalleAdicionales.setPrecio(precio);
            cubicadorDetalleAdicionales.setTotal(total);
            cubicadorDetalleAdicionales.setFechaCreacion(date);
            cubicadorDetalleAdicionales.setServicioId(idServicio);
            cubicadorDetalleAdicionales.setCubicadorId(idCubicador);

            montoTotalAdicionales = montoTotalAdicionales + total;
            boolean inserto = getGateway().registrarServiciosAdicionalesUnificado(cubicadorDetalleAdicionales);

        }
         getGateway().flush();
     }catch (Exception e){
         log.error("Error al guardar materiales adicionales en cubicador detalle unificado actas, error: "+ e.toString());
         throw e;
     }
     boolean validacionSistemaTotal = true;
return montoTotalAdicionales;

 }
    private static final Logger log = Logger.getLogger(AccionGenerarActaBucle.class);

}
