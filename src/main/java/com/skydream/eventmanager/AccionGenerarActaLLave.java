package com.skydream.eventmanager;

import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by mcj on 26-10-15.
 */
public class AccionGenerarActaLLave extends Command{

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {

        Map mapaOT = (Map) params.get("datosOt");
        int otId = (int) mapaOT.get("id");
        int usuarioId = (int) params.get("usuarioId");
        Ot ot = new Ot();
        ot.setId(otId);

        List<ObjetoCoreOT> listadoActasAnteriores = getGateway().listarConFiltro(Actas.class, null, "ot.id", ot.getId(), "id");
        Actas acta = new Actas();
        acta.setOt(ot);
        acta.setFecha(new Date());

        Number idActa = 0;

        try {
            getGateway().persistirObjeto(acta);
            idActa = acta.getId();
//            idActa = (Number)getGateway().registrarConId(acta);
        } finally {
            contAccionesBBDD.incrementAndGet();
        }

        acta.setId(idActa.intValue());
        Cubicador cubicador = (Cubicador)getGateway().obtenerRegistroConFiltro(Cubicador.class, null, "ot.id", otId);

        Map<Integer, DetalleActa> mapaConsolidadoMaterialesOt = retornarMapaMaterialesConsolidado(otId, acta, cubicador.getId(), listadoActasAnteriores, params);
        if(mapaConsolidadoMaterialesOt.size() > 0){
            setearPreciosMateriales(mapaConsolidadoMaterialesOt);
            setearDetallesMaterialesActa(acta, mapaConsolidadoMaterialesOt);
        }

        Map<Integer, DetalleActa> mapaConsolidadoServiciosOt = retornarMapaServiciosConsolidado(otId, acta, cubicador.getId(), listadoActasAnteriores, params);
        if(mapaConsolidadoServiciosOt.size() > 0){
            setearPreciosServicios(mapaConsolidadoServiciosOt, cubicador.getContrato().getId(),cubicador.getProveedor().getId(),cubicador.getRegion().getId());
            setearDetallesServiciosActa(acta, mapaConsolidadoServiciosOt);
        }

        setearTotalesActa(acta, mapaConsolidadoMaterialesOt, mapaConsolidadoServiciosOt);

        boolean validacionActa = validacionServicios(acta.getDetalleServiciosAdicionalesActas()) && validacionMateriales(acta.getDetalleMaterialesAdicionalesActas());

        acta.setValidacionSistema(validacionActa);

        String obs = params.get("obs").toString();
        Date fecha = new Date();
        String fechaFormat = obtienefechaSegunFormato(fecha, "dd-MM-yyyy");

        Usuarios usuario = (Usuarios) getGateway().retornarObjetoCoreOT(Usuarios.class, usuarioId);
        String observaciones = "{\n" +
                "  \"usuario\": "  + "\"" + usuario.getNombres() + " " + usuario.getApellidos() + "\"" + ",\n" +
                "  \"fecha\": "  + "\"" + fechaFormat + "\"" + ",\n" +
                "  \"observaciones\": "  + "\"" + obs + "\""  +
                "  }\n";

        int posUltiActa = listadoActasAnteriores.size();
        Actas ultimaActa = new Actas();
        String obsOld = "";
        if(posUltiActa > 0){
            ultimaActa = (Actas) listadoActasAnteriores.get((posUltiActa - 1));
            obsOld = ultimaActa.getObservaciones();
        } else {
            obsOld = null;
        }
        String observacionNueva = "";
        if(obsOld == null){
            observacionNueva = "[" + observaciones + "]";
        }else{
            obsOld = obsOld.replace("]",",");
            observacionNueva = obsOld + observaciones + "]";
        }
        acta.setObservaciones(observacionNueva);

        getGateway().actualizarWEE(otId);
    }
    private String obtienefechaSegunFormato(Date fecha, String formatoSalida) {
        String fechaString = "";
        if (fecha.equals("null")) {
            return fecha.toString();
        } else {
            try {
                DateFormat formato = new SimpleDateFormat(formatoSalida);
                fechaString = formato.format(fecha);
                //System.out.println("====================>  Fecha de salida :  "+fechaString);
                return fechaString;
            } catch (Exception e) {
                fechaString = "error: "+e;
                return fechaString;
            }
        }
    }

    private void setearTotalesActa(Actas acta, Map<Integer, DetalleActa> mapaConsolidadoMaterialesOt, Map<Integer, DetalleActa> mapaConsolidadoServiciosOt) {
        double totalCubicadoActa = 0;
        double totalHistoricoActa = 0;
        double totalEjecucionCubicadoActa = 0;
        double totalEjecucionAdicionalActa = 0;


        Set<DetalleServiciosActas> setDetalleServiciosActas = acta.getDetalleServiciosActas();
        for(DetalleServiciosActas item : setDetalleServiciosActas){
            totalCubicadoActa += item.getTotalMontoCubicado();
            totalEjecucionCubicadoActa += item.getTotalMontoEjecucionActa();
            totalHistoricoActa += item.getTotalHistoricoActa();
        }

        Set<DetalleServiciosAdicionalesActas> detalleServiciosAdicionalesActas = acta.getDetalleServiciosAdicionalesActas();
        for(DetalleServiciosAdicionalesActas item : detalleServiciosAdicionalesActas){
            totalEjecucionAdicionalActa += item.getTotalMontoEjecucionActa();
            if(item.getTotalMontoCubicado()==null){
                totalHistoricoActa += item.getTotalHistoricoActa();
            }
        }

        acta.setMontoCubicado(totalCubicadoActa);
        acta.setMontoHistorico(totalHistoricoActa);
        acta.setMontoTotalActaCubicado(totalEjecucionCubicadoActa);
        acta.setMontoTotalActaAdicional(totalEjecucionAdicionalActa);

        double montoTotalAPagar = acta.getMontoTotalActaCubicado() + acta.getMontoTotalActaAdicional();
        acta.setMontoTotalActa(montoTotalAPagar);
        acta.setMontoTotalAPagar(montoTotalAPagar);
    }


    private void setearPreciosServicios(Map<Integer, DetalleActa> detalle, int contratoId, int proveedorId, int regionId)throws Exception{

        Set<Integer> keys = detalle.keySet();
        Integer[] arrayIDs = keys.toArray(new Integer[keys.size()]);

        try{
            List<ProveedoresServicios> listado = getGateway().obtenerListadoProveedoresServicios(arrayIDs, contratoId,regionId,proveedorId);
            for(Map.Entry<Integer,DetalleActa> entry : detalle.entrySet()){
                DetalleActa detalleActa = entry.getValue();
                detalleActa.precioPorUnidadItem = retornarPrecioServicio(entry.getKey(),contratoId,proveedorId,regionId,listado);
            }

        }catch (Exception e){
            log.error("Error al setear precios.",e);
            throw  e;
        }

    }


    private Double retornarPrecioServicio(int servicioId, int contratoId, int proveedorId, int regionId, List<ProveedoresServicios> listadoServicios)throws Exception{
        Double precio = null;

        for(ProveedoresServicios proveedorServicio : listadoServicios){
            int idServicio = proveedorServicio.getServicios().getId();
            int idContrato = proveedorServicio.getContratos().getId();
            int idRegion = proveedorServicio.getRegiones().getId();
            int idProveedor = proveedorServicio.getProveedores().getId();

            if(idServicio==servicioId && idContrato==contratoId && idRegion==regionId && idProveedor==proveedorId){
                precio = proveedorServicio.getPrecio();
                listadoServicios.remove(proveedorServicio);
                break;
            }
        }
        //Si NO se encontró precio para el item entonces se levanta una excepcion
        if(precio==null){
            log.error("******* Precio NO encontrado *******" + "\n" +
                    "servicioID : " + servicioId + "\n" +
                    "contratoID : " + contratoId + "\n" +
                    "regionID : " + regionId + "\n" +
                    "proveedorID : " + proveedorId);
            throw new Exception("Precio no encontrado.");
        }
        return precio;
    }


    private void setearPreciosMateriales(Map<Integer, DetalleActa> detalle)throws Exception{

        Set<Integer> keys = detalle.keySet();
        Integer[] arrayIDs = keys.toArray(new Integer[keys.size()]);

        try{
            List<Materiales> listado = getGateway().obtenerListadoMateriales(arrayIDs);

            for(Map.Entry<Integer,DetalleActa> entry : detalle.entrySet()){
                DetalleActa detalleActa = entry.getValue();
                detalleActa.precioPorUnidadItem = retornarPrecioMateriales(entry.getKey(),listado);
            }

        }catch (Exception e){
            log.error("Error al setear precios.",e);
            throw  e;
        }

    }


    private Double retornarPrecioMateriales(int materialId, List<Materiales> listadoMateriales)throws Exception{
        Double precio = null;

        for(Materiales material : listadoMateriales){
            int idMaterial = material.getId();

            if(idMaterial==materialId){
                precio = material.getPrecio();
                listadoMateriales.remove(material);
                break;
            }
        }
        //Si NO se encontró precio para el item entonces se levanta una excepcion
        if(precio==null){
            log.error("******* Precio NO encontrado *******" + "\n" +
                    "materialID : " + materialId);
            throw new Exception("Precio no encontrado.");
        }
        return precio;
    }


    private boolean validacionServicios(Set<DetalleServiciosAdicionalesActas> detServiciosAdicionalesActa){
        boolean validacionOK = true;
        for(DetalleServiciosAdicionalesActas itemDetalleServicioAdicionalActa : detServiciosAdicionalesActa){
            if(itemDetalleServicioAdicionalActa.getTotalUnidadesEjecucionActa()>0){
                validacionOK = false;
                break;
            }
        }
        return validacionOK;
    }


    private boolean validacionMateriales(Set<DetalleMaterialesAdicionalesActas> detMaterialesAdicionalesActa){
        boolean validacionOK = true;
        for(DetalleMaterialesAdicionalesActas itemDetalleMaterialAdicionalActa : detMaterialesAdicionalesActa){
            if(itemDetalleMaterialAdicionalActa.getTotalUnidadesEjecucionActa()>0){
                validacionOK = false;
                break;
            }
        }
        return validacionOK;
    }


    private Map<Integer, DetalleActa> retornarMapaMaterialesConsolidado(int otId, Actas acta, int cubicadorId, List<ObjetoCoreOT> listadoActasAnteriores, Map params) throws Exception {
        Map<Integer, DetalleActa> mapaTotalMaterialesOt;

        List<String> join = new ArrayList<>();
        join.add("material");
        List<ObjetoCoreOT> listadoCubicadorMateriales = getGateway().listarConFiltro(CubicadorMateriales.class,join,"id.cubicadorId", cubicadorId,"id.materialId");

        List arrayMaterialesUtilizadosActa = (List)params.get("materiales");

        //Se obtiene la informacion consolidada de los materiales informados en las actas anteriores
        Map<Integer,DetalleActa> mapaConsolidadoDeMaterialesTodasLasActasAnteriores = retornarMapaConTotalMaterialesTodasLasActas(listadoActasAnteriores);

        //Se llena un mapa auxiliar con lo informado para la presente acta
        Map<Integer, DetalleActa> mapaMaterialesUtilizadosNuevaActa = retornarMapaDetalleActa(arrayMaterialesUtilizadosActa);

        //Se consolida la informacion historica de materiales utilizados para todas las actas anteriores correspondiente a la misma OT
        // con la de los materiales informados para la nueva acta
        //Como resultado quedará el mapaTotalMaterialesOt
        mapaTotalMaterialesOt = mapaConsolidadoDeMaterialesTodasLasActasAnteriores;
        for(Map.Entry<Integer,DetalleActa> entry : mapaMaterialesUtilizadosNuevaActa.entrySet()){
            DetalleActa item = entry.getValue();
            if (mapaTotalMaterialesOt.containsKey(item.id)){
                mapaTotalMaterialesOt.get(item.id).totalEjecutadoActa = item.totalEjecutadoActa;
            }else{
                mapaTotalMaterialesOt.put(item.id, item);
            }
        }

        //Se agrega al mapaConsolidadoDeMaterialesTodasLasActasAnteriores los materiales que fueron cubicados pero no fueron informados en la presente acta
        //Esta condicion debe darse solo para la primera acta que se genere por cada ot
        for(ObjetoCoreOT itemCubicadorMaterial : listadoCubicadorMateriales){
            CubicadorMateriales cubicadorMaterial = (CubicadorMateriales)itemCubicadorMaterial;
            Materiales material = cubicadorMaterial.getMaterial();
            //Verificar si el material fue informado en actas anteriores o en la presente acta
            if(!mapaTotalMaterialesOt.containsKey(material.getId())){
                //Si el material NO ha sido informado entonces se agrega al mapa de mapaMaterialesCubicadosNoInformados
                double montoCubicado = cubicadorMaterial.getMontoTotal();
                DetalleActa newItem = new DetalleActa(material.getId(),cubicadorMaterial.getCantidad(),0,0,montoCubicado,0,0,false);
                mapaTotalMaterialesOt.put(newItem.id,newItem);
            }else {
                DetalleActa detalleActa = mapaTotalMaterialesOt.get(material.getId());
                detalleActa.totalCubicado = cubicadorMaterial.getCantidad();
                detalleActa.montoCubicado = cubicadorMaterial.getMontoTotal();
            }
        }
        return mapaTotalMaterialesOt;
    }


    private void setearDetallesMaterialesActa(Actas acta, Map<Integer, DetalleActa> mapaMateriales)throws Exception{
        Set<DetalleMaterialesActas> setDetalleMaterialesActa = new HashSet<>();
        Set<DetalleMaterialesAdicionalesActas> setDetalleMaterialesAdicionalesActa = new HashSet<>();

        for(Map.Entry<Integer, DetalleActa> entry : mapaMateriales.entrySet()){
            DetalleActa detalleActa = entry.getValue();

            double sumaTotalMaterialUtilizado = detalleActa.totalHistorico + detalleActa.totalEjecutadoActa;
            double diferenciaUnidadesCubicadoMenosTotalUtilizado = detalleActa.totalCubicado - sumaTotalMaterialUtilizado;
            double diferenciaUnidadesCubicadoMenosTotalHistorico = detalleActa.totalCubicado - detalleActa.totalHistorico;
            double diferenciaMontoCubicadoMenosMontoHistorico = detalleActa.montoCubicado - detalleActa.montoHistorico;

            Materiales material = new Materiales(detalleActa.id);

            //Solo lo que ha sido cubicado y esta dentro de rango (total o parcialmente)
            if(detalleActa.totalCubicado > 0 && diferenciaUnidadesCubicadoMenosTotalHistorico > 0 ){
                DetalleMaterialesActas detalleMaterialActa = retornarDetalleMaterialActa(acta, detalleActa, diferenciaUnidadesCubicadoMenosTotalUtilizado,
                        diferenciaUnidadesCubicadoMenosTotalHistorico, material);
                setDetalleMaterialesActa.add(detalleMaterialActa);
            }

            //Lo que fue cubicado pero excede y lo que NO fue cubicado
            if(diferenciaUnidadesCubicadoMenosTotalUtilizado < 0){
                DetalleMaterialesAdicionalesActas detalleMaterialesAdicionalesActa = retornarDetalleMaterialAdicionalActa(acta, detalleActa,
                        diferenciaUnidadesCubicadoMenosTotalHistorico, diferenciaMontoCubicadoMenosMontoHistorico, material);
                setDetalleMaterialesAdicionalesActa.add(detalleMaterialesAdicionalesActa);
            }

        }
        acta.setDetalleMaterialesActas(setDetalleMaterialesActa);
        acta.setDetalleMaterialesAdicionalesActas(setDetalleMaterialesAdicionalesActa);
    }


    private DetalleMaterialesActas retornarDetalleMaterialActa(Actas acta, DetalleActa detalleActa, double diferenciaUnidadesCubicadoMenosTotalUtilizado,
                                                               double diferenciaUnidadesCubicadoMenosTotalHistorico, Materiales material) {
        DetalleMaterialesActas detalleMaterialActa = new DetalleMaterialesActas();
        DetalleMaterialesActasId detalleId = new DetalleMaterialesActasId(acta.getId(), detalleActa.id);
        detalleMaterialActa.setId(detalleId);
        detalleMaterialActa.setActas(acta);
        detalleMaterialActa.setMateriales(material);
        detalleMaterialActa.setTotalUnidadesCubicadas(detalleActa.totalCubicado);
        detalleMaterialActa.setTotalUnidadesEjecucionOt(detalleActa.totalHistorico);
        detalleMaterialActa.setTotalMontoCubicado(detalleActa.montoCubicado);
        detalleMaterialActa.setTotalHistoricoActa(detalleActa.montoHistorico);

        if(diferenciaUnidadesCubicadoMenosTotalUtilizado >= 0){
            //Si diferencia es positiva entonces lo informado en el presente acta esta dentro de lo estimado
            detalleMaterialActa.setTotalUnidadesEjecucionActa(detalleActa.totalEjecutadoActa);
            detalleMaterialActa.setTotalMontoEjecucionActa(detalleActa.totalEjecutadoActa * detalleActa.precioPorUnidadItem);
        }else{
            //Si ingresa aqui es por que el material SI fue cubicado pero la cantidad utilizada excede a la estimada
            // no obstante se toma la diferencia del total cubicado menos el total historico(actas anteriores sumadas)
            detalleMaterialActa.setTotalUnidadesEjecucionActa(diferenciaUnidadesCubicadoMenosTotalHistorico);
            detalleMaterialActa.setTotalMontoEjecucionActa(diferenciaUnidadesCubicadoMenosTotalHistorico * detalleActa.precioPorUnidadItem);
            detalleMaterialActa.setValidacionSistema(Boolean.FALSE);
        }
        return detalleMaterialActa;
    }

    private DetalleMaterialesAdicionalesActas retornarDetalleMaterialAdicionalActa(Actas acta, DetalleActa detalleActa, double diferenciaCubicadoMenosTotalHistorico,
                                                                                   double diferenciaMontoCubicadoMenosMontoHistorico, Materiales material) {
        DetalleMaterialesAdicionalesActasId id = new DetalleMaterialesAdicionalesActasId(acta.getId(), detalleActa.id);
        DetalleMaterialesAdicionalesActas detalleMaterialesAdicionalesActa = new DetalleMaterialesAdicionalesActas();
        detalleMaterialesAdicionalesActa.setId(id);
        detalleMaterialesAdicionalesActa.setMateriales(material);
        detalleMaterialesAdicionalesActa.setTotalUnidadesCubicadas(detalleActa.totalCubicado);
        detalleMaterialesAdicionalesActa.setTotalUnidadesEjecucionOt(detalleActa.totalHistorico);

        double totalCorrespondienteAPresenteActa = 0;
        double montoTotalServicioPresenteActa = 0;
        if(detalleActa.totalCubicado == 0){
            //Ingresa solo si material NO fue cubicado
            totalCorrespondienteAPresenteActa = detalleActa.totalEjecutadoActa;
        }else{
            //Ingresa solo si material SI fue cubicado
            //Si fue cubicado se debe calcular cuanto es la cantidad que excede a lo estimado
            totalCorrespondienteAPresenteActa = detalleActa.totalEjecutadoActa - diferenciaCubicadoMenosTotalHistorico;
        }
        montoTotalServicioPresenteActa = totalCorrespondienteAPresenteActa * detalleActa.precioPorUnidadItem;
        detalleMaterialesAdicionalesActa.setTotalUnidadesCubicadas(detalleActa.totalCubicado);
        detalleMaterialesAdicionalesActa.setTotalUnidadesEjecucionActa(totalCorrespondienteAPresenteActa);
        detalleMaterialesAdicionalesActa.setTotalMontoCubicado(detalleActa.montoCubicado);
        detalleMaterialesAdicionalesActa.setTotalHistoricoActa(detalleActa.montoHistorico);
        detalleMaterialesAdicionalesActa.setTotalMontoEjecucionActa(montoTotalServicioPresenteActa);
        return detalleMaterialesAdicionalesActa;
    }

    private Map<Integer,DetalleActa> retornarMapaConTotalMaterialesTodasLasActas(List<ObjetoCoreOT> listadoActasAnteriores) throws Exception {
        Map<Integer,DetalleActa> detalleActa = new HashMap<>();
        //Se obtienen todas las actas anteriores

        for(ObjetoCoreOT item : listadoActasAnteriores){
            Actas actaAntigua = (Actas)item;
            //Se consideran solo las actas que han sido aprobadas por los usuarios
            if (actaAntigua.getValidacionUsuarios()) {
                for(Object itemMaterial : actaAntigua.getDetalleMaterialesActas()){
                    DetalleMaterialesActas detalleMaterialesActas = (DetalleMaterialesActas)itemMaterial;
                    int idMaterial = detalleMaterialesActas.getMateriales().getId();
                    //Se verifica si el item ya se encuentra en el mapa y si existe se reemplaza la entrada con un nuevo valor
                    double nuevoTotal = 0;
                    double nuevoMontoTotal = 0;
                    double totalItemActual = detalleMaterialesActas.getTotalUnidadesEjecucionActa() == null?0:detalleMaterialesActas.getTotalUnidadesEjecucionActa();
                    double montoItemActual = detalleMaterialesActas.getTotalMontoEjecucionActa() == null?0:detalleMaterialesActas.getTotalMontoEjecucionActa();
                    if(detalleActa.containsKey(idMaterial)){
                        DetalleActa varAux = detalleActa.get(idMaterial);
                        double totalAnterior = varAux.totalHistorico;
                        nuevoTotal = totalAnterior + totalItemActual;
                        nuevoMontoTotal = varAux.montoHistorico + montoItemActual;
                        detalleActa.remove(idMaterial);
                    }else {
                        nuevoTotal = totalItemActual;
                        nuevoMontoTotal = montoItemActual;
                    }
                    double montoCubicado = detalleMaterialesActas.getTotalMontoCubicado();
                    DetalleActa newDetalleActa = new DetalleActa(idMaterial,detalleMaterialesActas.getTotalUnidadesCubicadas(),nuevoTotal,0,montoCubicado,nuevoMontoTotal,0,false);
                    detalleActa.put(idMaterial, newDetalleActa);
                }
            }
        }
        return detalleActa;
    }

    private Map<Integer, DetalleActa> retornarMapaDetalleActa(List<Map> listaDeParametros) {
        Map<Integer, DetalleActa> mapaRetorno = new HashMap();
        for (Map mapa : listaDeParametros) {
            DetalleActa detalleActa = new DetalleActa();
            detalleActa.setearObjetosDeInstancia(mapa);
            mapaRetorno.put(detalleActa.id, detalleActa);
        }
        return mapaRetorno;
    }

    private class DetalleActa implements Comparable<DetalleActa>{
        int id;
        double totalCubicado;
        double totalHistorico;
        double totalEjecutadoActa;
        double montoCubicado;
        double montoHistorico;
        double montoEjecutadoActa;
        double precioPorUnidadItem;
        boolean revisado = false;

        public DetalleActa(){};

        public DetalleActa(int id, double totalCubicado, double totalHistorico, double totalEjecutadoActa, double montoCubicado, double montoHistorico, double montoEjecutadoActa, boolean revisado){
            this.id = id;
            this.totalCubicado = totalCubicado;
            this.totalHistorico = totalHistorico;
            this.totalEjecutadoActa = totalEjecutadoActa;
            this.montoCubicado = montoCubicado;
            this.montoHistorico = montoHistorico;
            this.montoEjecutadoActa = montoEjecutadoActa;
            this.revisado = revisado;
        }

        public void setearObjetosDeInstancia(Map params){
            if(params.containsKey("id")){
                String intAux = (String)params.get("id");
                id = Integer.parseInt(intAux);
            }
            if(params.containsKey("cantidadReal")){
                Object cantidadAux = params.get("cantidadReal");
                if(cantidadAux instanceof Integer){
                    totalEjecutadoActa = ((Integer)params.get("cantidadReal")).doubleValue();
                }else totalEjecutadoActa = (Double)params.get("cantidadReal");
            }
            if(params.containsKey("montoTotal")){
                Object montoAux = params.get("montoTotal");
                if(montoAux instanceof Integer){
                    montoEjecutadoActa = ((Integer)params.get("montoTotal")).doubleValue();
                }else {
                    montoEjecutadoActa = (Double)params.get("montoTotal");
                };
            }
        }

        @Override
        public int compareTo(DetalleActa detalleActa) {
            return Integer.compare(id, detalleActa.id);
        }

        public boolean equals(Object other) {
            if ( (this == other ) ) return true;
            if ( (other == null ) ) return false;
            if ( !(other instanceof DetalleActa) ) return false;
            DetalleActa castOther = (DetalleActa) other;
            return (this.id==castOther.id);
        }

        public int hashCode() {
            int result = 17;
            result = 37 * result + this.id;
            return result;
        }
    }


    // ************************* SERVICIOS *************************


    private Map<Integer, DetalleActa> retornarMapaServiciosConsolidado(int otId, Actas acta, int cubicadorId, List<ObjetoCoreOT> listadoActasAnteriores, Map params) throws Exception {
        Map<Integer, DetalleActa> mapaTotalServiciosOt;

        List<String> join = new ArrayList<>();
        join.add("material");
        List<ObjetoCoreOT> listadoCubicadorServicios = getGateway().listarConFiltro(CubicadorServicios.class,join,"id.cubicadorId", cubicadorId,"id.servicioId");

        List arrayServiciosUtilizadosActa = (List)params.get("servicios");

        //Se obtiene la informacion consolidada de los servicios informados en las actas anteriores
        Map<Integer,DetalleActa> mapaConsolidadoDeServiciosTodasLasActasAnteriores = retornarMapaConTotalServiciosTodasLasActas(listadoActasAnteriores);

        //Se llena un mapa auxiliar con lo informado para la presente acta
        Map<Integer, DetalleActa> mapaServiciosUtilizadosNuevaActa = retornarMapaDetalleActa(arrayServiciosUtilizadosActa);

        //Se consolida la informacion historica de servicios utilizados para todas las actas anteriores correspondiente a la misma OT
        // con la de los de servicios informados para la nueva acta
        //Como resultado quedará el mapaTotalServiciosOt
        mapaTotalServiciosOt = mapaConsolidadoDeServiciosTodasLasActasAnteriores;
        for(Map.Entry<Integer,DetalleActa> entry : mapaServiciosUtilizadosNuevaActa.entrySet()){
            DetalleActa item = entry.getValue();
            if (mapaTotalServiciosOt.containsKey(item.id)){
                mapaTotalServiciosOt.get(item.id).totalEjecutadoActa = item.totalEjecutadoActa;
            }else{
                mapaTotalServiciosOt.put(item.id, item);
            }
        }

        //Se agrega al mapaConsolidadoDeServiciosTodasLasActasAnteriores los materiales que fueron cubicados pero no fueron informados en la presente acta
        //Esta condicion debe darse solo para la primera acta que se genere por cada ot
        for(ObjetoCoreOT itemCubicadorServicio : listadoCubicadorServicios){
            CubicadorServicios cubicadorServicio = (CubicadorServicios)itemCubicadorServicio;
            Servicios servicio = cubicadorServicio.getServicio();
            if(!mapaTotalServiciosOt.containsKey(servicio.getId())){
                //Si el servicio NO ha sido informado entonces se agrega al mapa de mapaMaterialesCubicadosNoInformados
                DetalleActa newItem = new DetalleActa(servicio.getId(),cubicadorServicio.getCantidad(),0,0,cubicadorServicio.getTotal(),0,0,false);
                mapaTotalServiciosOt.put(newItem.id,newItem);
            }else{
                DetalleActa detalleActa = mapaTotalServiciosOt.get(servicio.getId());
                detalleActa.totalCubicado = cubicadorServicio.getCantidad();
                detalleActa.montoCubicado = cubicadorServicio.getTotal();
            }
        }
        return mapaTotalServiciosOt;
    }

    private void setearDetallesServiciosActa(Actas acta, Map<Integer, DetalleActa> mapaServicios)throws Exception{
        Set<DetalleServiciosActas> setDetalleServiciosActas = new HashSet<>();
        Set<DetalleServiciosAdicionalesActas> setDetalleServiciosAdicionalesActa = new HashSet<>();

        for(Map.Entry<Integer, DetalleActa> entry : mapaServicios.entrySet()){
            DetalleActa detalleActa = entry.getValue();

            double sumaUnidadesTotalesServicioUtilizado = detalleActa.totalHistorico + detalleActa.totalEjecutadoActa;
            double diferenciaUnidadesCubicadoMenosTotalUtilizado = detalleActa.totalCubicado - sumaUnidadesTotalesServicioUtilizado;
            double diferenciaUnidadesCubicadoMenosTotalHistorico = detalleActa.totalCubicado - detalleActa.totalHistorico;

            Servicios servicio = new Servicios(detalleActa.id);

            //Solo lo que ha sido cubicado y esta dentro de rango (total o parcialmente)
            if(detalleActa.totalCubicado > 0 && diferenciaUnidadesCubicadoMenosTotalHistorico > 0 ){
                DetalleServiciosActas detalleMaterialActa = retornarDetalleServicioActa(acta, detalleActa, diferenciaUnidadesCubicadoMenosTotalUtilizado,
                        diferenciaUnidadesCubicadoMenosTotalHistorico, servicio);
                setDetalleServiciosActas.add(detalleMaterialActa);
            }

            //Lo que fue cubicado pero excede y lo que NO fue cubicado
            if(diferenciaUnidadesCubicadoMenosTotalUtilizado < 0){
                DetalleServiciosAdicionalesActas detalleMaterialesAdicionalesActa = retornarDetalleServicioAdicionalActa(acta, detalleActa,
                        diferenciaUnidadesCubicadoMenosTotalHistorico, servicio);
                setDetalleServiciosAdicionalesActa.add(detalleMaterialesAdicionalesActa);
            }

        }
        acta.setDetalleServiciosActas(setDetalleServiciosActas);
        acta.setDetalleServiciosAdicionalesActas(setDetalleServiciosAdicionalesActa);
    }

    private DetalleServiciosActas retornarDetalleServicioActa(Actas acta, DetalleActa detalleActa, double diferenciaUnidadesCubicadasMenosTotalUtilizado,
                                                              double diferenciaUnidadesCubicadasMenosTotalHistorico, Servicios servicio) {
        DetalleServiciosActas detalleServicioActa = new DetalleServiciosActas();
        DetalleServiciosActasId detalleId = new DetalleServiciosActasId(acta.getId(), detalleActa.id);
        detalleServicioActa.setId(detalleId);
        detalleServicioActa.setActas(acta);
        detalleServicioActa.setServicio(servicio);
        detalleServicioActa.setTotalUnidadesCubicadas(detalleActa.totalCubicado);
        detalleServicioActa.setTotalUnidadesEjecucionOt(detalleActa.totalHistorico);
        detalleServicioActa.setTotalMontoCubicado(detalleActa.montoCubicado);
        detalleServicioActa.setTotalHistoricoActa(detalleActa.montoHistorico);
        if(diferenciaUnidadesCubicadasMenosTotalUtilizado >= 0){
            //Si diferencia es positiva entonces lo informado en el presente acta esta dentro de lo estimado
            detalleServicioActa.setTotalUnidadesEjecucionActa(detalleActa.totalEjecutadoActa);
            detalleServicioActa.setTotalMontoEjecucionActa(detalleActa.precioPorUnidadItem * detalleActa.totalEjecutadoActa);
        }else{
            //Si ingresa aqui es por que el servicio SI fue cubicado pero la cantidad utilizada excede a la estimada
            // no obstante se toma la diferencia del total cubicado menos el total historico(actas anteriores sumadas)
            Double totalUnidadesEjecucionActa = Math.abs(diferenciaUnidadesCubicadasMenosTotalHistorico);
            detalleServicioActa.setTotalUnidadesEjecucionActa(totalUnidadesEjecucionActa);
            detalleServicioActa.setTotalMontoEjecucionActa(detalleActa.precioPorUnidadItem * totalUnidadesEjecucionActa);
            detalleServicioActa.setValidacionSistema(Boolean.FALSE);
        }
        return detalleServicioActa;
    }

    private DetalleServiciosAdicionalesActas retornarDetalleServicioAdicionalActa(Actas acta, DetalleActa detalleActa, double diferenciaUnidadesCubicadasMenosTotalHistorico, Servicios servicio) {
        DetalleServiciosAdicionalesActasId id = new DetalleServiciosAdicionalesActasId(acta.getId(), detalleActa.id);
        DetalleServiciosAdicionalesActas detalleServicioAdicionalActa = new DetalleServiciosAdicionalesActas();
        detalleServicioAdicionalActa.setId(id);
        detalleServicioAdicionalActa.setServicio(servicio);
        detalleServicioAdicionalActa.setTotalUnidadesCubicadas(detalleActa.totalCubicado);
        detalleServicioAdicionalActa.setTotalUnidadesEjecucionOt(detalleActa.totalHistorico);

        double unidadesTotalesServicioPresenteActa = 0;
        double montoTotalServicioPresenteActa = 0;
        if(detalleActa.totalCubicado == 0){
            //Ingresa solo si servicio NO fue cubicado
            unidadesTotalesServicioPresenteActa = detalleActa.totalEjecutadoActa;

        }else{
            //Ingresa solo si servicio SI fue cubicado
            //Si fue cubicado se debe calcular cuanto es la cantidad que excede a lo estimado
            unidadesTotalesServicioPresenteActa = detalleActa.totalEjecutadoActa - diferenciaUnidadesCubicadasMenosTotalHistorico;
        }
        montoTotalServicioPresenteActa = detalleActa.precioPorUnidadItem * unidadesTotalesServicioPresenteActa;
        detalleServicioAdicionalActa.setTotalUnidadesEjecucionActa(unidadesTotalesServicioPresenteActa);
        detalleServicioAdicionalActa.setTotalMontoCubicado(detalleActa.montoCubicado);
        detalleServicioAdicionalActa.setTotalHistoricoActa(detalleActa.montoHistorico);
        detalleServicioAdicionalActa.setTotalMontoEjecucionActa(montoTotalServicioPresenteActa);
        return detalleServicioAdicionalActa;
    }


    private Map<Integer,DetalleActa> retornarMapaConTotalServiciosTodasLasActas(List<ObjetoCoreOT> listadoActasAnteriores) throws Exception {
        Map<Integer,DetalleActa> detalleActa = new HashMap<>();
        //Se obtienen todas las actas anteriores

        for(ObjetoCoreOT item : listadoActasAnteriores){
            Actas actaAntigua = (Actas)item;
            //Se consideran solo las actas que han sido aprobadas por los usuarios
            if (actaAntigua.getValidacionUsuarios()) {
                for(Object itemMaterial : actaAntigua.getDetalleServiciosActas()){
                    DetalleServiciosActas detalleServiciosActas = (DetalleServiciosActas)itemMaterial;
                    int idServicio = detalleServiciosActas.getServicio().getId();
                    //Se verifica si el item ya se encuentra en el mapa y si existe se reemplaza la entrada con un nuevo valor
                    double nuevoTotal = 0;
                    double nuevoMontoTotal = 0;
                    double totalItemActual = detalleServiciosActas.getTotalUnidadesEjecucionActa() == null?0:detalleServiciosActas.getTotalUnidadesEjecucionActa();
                    double montoItemActual = detalleServiciosActas.getTotalMontoEjecucionActa()==null?0:detalleServiciosActas.getTotalMontoEjecucionActa();
                    if(detalleActa.containsKey(idServicio)){
                        DetalleActa varAux = detalleActa.get(idServicio);
                        double totalAnterior = varAux.totalHistorico;
                        nuevoTotal = totalAnterior + totalItemActual;
                        nuevoMontoTotal = varAux.montoHistorico + montoItemActual;
                        detalleActa.remove(idServicio);
                    }else {
                        nuevoTotal = totalItemActual;
                        nuevoMontoTotal = montoItemActual;
                    }
                    double montoCubicado = detalleServiciosActas.getTotalMontoCubicado();
                    DetalleActa newDetalleActa = new DetalleActa(idServicio,detalleServiciosActas.getTotalUnidadesCubicadas(),nuevoTotal,0,montoCubicado,nuevoMontoTotal,0,false);
                    detalleActa.put(idServicio, newDetalleActa);
                }
            }
        }
        return detalleActa;
    }


    private static final Logger log = Logger.getLogger(AccionGenerarActaLLave.class);

}
