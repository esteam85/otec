package com.skydream.eventmanager;

import com.skydream.coreot.dao.Gateway;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by mcj on 26-10-15.
 */
public class AccionGenerarActaOrdinaria extends Command{

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {

        Map mapaOT = (Map) params.get("datosOt");
        int otId = (int) mapaOT.get("id");
        int usuarioId = (int) params.get("usuarioId");
        Ot ot = new Ot();
        ot.setId(otId);
        boolean existe = false;

        List<ObjetoCoreOT> listadoActasAnteriores = getGateway().listarConFiltro(Actas.class, null, "ot.id", ot.getId(), "id");
        List<Actas> listadoActasValidadas = getGateway().obtenerListadoActasValidadas(ot.getId());

        Actas acta = new Actas();
        acta.setOt(ot);
        acta.setFecha(new Date());

        Number idActa = 0;

        try {
            getGateway().persistirObjeto(acta);
            idActa = acta.getId();
        } finally {
            contAccionesBBDD.incrementAndGet();
        }
        acta.setId(idActa.intValue());
        Cubicador cubicador = (Cubicador)getGateway().obtenerRegistroConFiltro(Cubicador.class, null, "ot.id", otId);
        CubicadorOrdinario cubiAux = new CubicadorOrdinario();
        // obtengo listado de item de cubicacion orginal ordinaria
        List<CubicadorOrdinario> mapaConsolidadoMaterialesOt = null;
        mapaConsolidadoMaterialesOt = getGateway().obtenerListaCubicacionOrdinariaPorIdOtAcciones(cubicador.getId());
        double montoTotalRealInformadoHistorico = 0.0;
        double montoTotalInformadoActa = 0.0;
        double cantidadCubicadaTotal = 0.0;
        double cantidadHistoricaOt = 0.0;
        //obtengo el listado de item con cantidad informada desde el front
        List<LinkedHashMap> arrayMaterialesUtilizadosActa = (List)params.get("materiales");
        // recorro el listado y creo el objeto
        List<ItemOrdinarioAux> listadoItemInformado = new ArrayList<>();

        int posUltiActaValidada = listadoActasValidadas.size();
        Actas ultimaActaValidada = new Actas();
        int montoHistoricoAprobado = 0;
        if(posUltiActaValidada > 0){
            ultimaActaValidada = (Actas) listadoActasValidadas.get((posUltiActaValidada - 1));
        }

        for (LinkedHashMap itemInformadoAux: arrayMaterialesUtilizadosActa){

            Set set = itemInformadoAux.entrySet();
            Iterator i = set.iterator();
            ItemOrdinarioAux itemOrdinarioAux = new ItemOrdinarioAux();
            while(i.hasNext()) {
                Map.Entry me = (Map.Entry)i.next();
                boolean inserto = false;
                switch ((String)me.getKey()) {
                    case "id":
                        itemOrdinarioAux.setId((Integer) me.getValue());
                        break;
                    case "cantidadInformada":
                        try {
                            itemOrdinarioAux.setCantidadInformadaHistorica((Integer) me.getValue());
                            inserto = true;
                        }catch (Exception e){
                            // no lanzo excepcion porq puede ser double el valor
                        }
                        if(!inserto){
                            itemOrdinarioAux.setCantidadInformadaHistorica((Double) me.getValue());
                        }
                        break;
                    case "cantidadReal":
                        try {
                            itemOrdinarioAux.setCantidadInformada((Integer) me.getValue());
                            inserto = true;
                        }catch (Exception e){
                            // no lanzo excepcion porq puede ser double el valor
                        }
                        if(!inserto){
                            itemOrdinarioAux.setCantidadInformada((Double) me.getValue());
                        }
                        break;
                }
            }
            cubiAux = getGateway().obtenerCubicacionOrdinariaPorId(itemOrdinarioAux.getId());

            montoTotalRealInformadoHistorico = montoTotalRealInformadoHistorico + (cubiAux.getPrecio() * itemOrdinarioAux.getCantidadInformadaHistorica());
            montoTotalInformadoActa = montoTotalInformadoActa + (cubiAux.getPrecio() * itemOrdinarioAux.getCantidadInformada());
            listadoItemInformado.add(itemOrdinarioAux);
        }

        boolean validacionSistemaTotal = true;

        for (CubicadorOrdinario itemOriginal: mapaConsolidadoMaterialesOt){
            cantidadCubicadaTotal = cantidadCubicadaTotal + itemOriginal.getTotal();
            int idItemCubicado = itemOriginal.getId();
            for(ItemOrdinarioAux itemInformado: listadoItemInformado){
                // busco el item cubicado en informado
                if(itemInformado.getId() ==  idItemCubicado){
                    // Instancio el objeto con los datos correspondientes
                    DetalleCubicadorOrdinarioActas detalleCubicadorOrdinarioActasUltimo = new DetalleCubicadorOrdinarioActas();
                    DetalleCubicadorOrdinarioActasId detalleCubicadorOrdinarioActasIdUltimo = new DetalleCubicadorOrdinarioActasId();

                    DetalleCubicadorOrdinarioActas detalleCubicadorOrdinarioActas = new DetalleCubicadorOrdinarioActas();
                    DetalleCubicadorOrdinarioActasId detalleCubicadorOrdinarioActasId = new DetalleCubicadorOrdinarioActasId();

                    if(posUltiActaValidada > 0) {
                        detalleCubicadorOrdinarioActasIdUltimo.setActaId(ultimaActaValidada.getId());
                        detalleCubicadorOrdinarioActasIdUltimo.setCubicadorOrdinarioId(itemInformado.getId());
                        detalleCubicadorOrdinarioActasUltimo = getGateway().obtieneDetalleCubicadorOrdinarioActas(detalleCubicadorOrdinarioActasIdUltimo);
                        detalleCubicadorOrdinarioActas.setTotalUnidadesEjecucionOt(detalleCubicadorOrdinarioActasUltimo.getTotalUnidadesEjecucionOt());
                    } else {
                        detalleCubicadorOrdinarioActas.setTotalUnidadesEjecucionOt(0.0);
                    }

                    detalleCubicadorOrdinarioActasId.setActaId(acta.getId());
                    detalleCubicadorOrdinarioActasId.setCubicadorOrdinarioId(idItemCubicado);
                    detalleCubicadorOrdinarioActas.setId(detalleCubicadorOrdinarioActasId);

                    detalleCubicadorOrdinarioActas.setTotalMontoCubicado(itemOriginal.getTotal());
                    double montoInformado = itemOriginal.getPrecio() * itemInformado.getCantidadInformada();
                    detalleCubicadorOrdinarioActas.setTotalMontoEjecucion(montoInformado);
                    detalleCubicadorOrdinarioActas.setTotalUnidadesCubicadas(itemOriginal.getCantidad());
                    detalleCubicadorOrdinarioActas.setTotalUnidadesEjecucionActa(itemInformado.getCantidadInformada());


                    boolean validacionSistema = false;
                    if(itemOriginal.getCantidad() == itemInformado.getCantidadInformada()){
                        validacionSistema = true;
                    }else{
                        validacionSistemaTotal = false;
                    }
                    detalleCubicadorOrdinarioActas.setValidacionSistema(validacionSistema);

                    // inserto el registro con los datos correspondientes
                    boolean inserto = getGateway().registrarDetalleCubicacionOrdinaria(detalleCubicadorOrdinarioActas);
                }
            }
        }
        acta.setMontoTotalActaCubicado(montoTotalInformadoActa);
        acta.setMontoCubicado(cantidadCubicadaTotal);
        acta.setMontoHistorico(montoTotalRealInformadoHistorico);
        acta.setValidacionSistema(validacionSistemaTotal);
        acta.setMontoTotalActa(montoTotalInformadoActa);
        acta.setMontoTotalAPagar(montoTotalInformadoActa);


        String obs = params.get("obs").toString();
        Date fecha = new Date();
        String fechaFormat = obtienefechaSegunFormato(fecha, "dd-MM-yyyy");
        Usuarios usuario = null;
        usuario = (Usuarios) getGateway().retornarObjetoCoreOT(Usuarios.class, usuarioId);

        String observaciones = "{\n" +
                "  \"usuario\": "  + "\"" + usuario.getNombres() + " " + usuario.getApellidos() + "\"" + ",\n" +
                "  \"fecha\": "  + "\"" + fechaFormat + "\"" + ",\n" +
                "  \"observaciones\": "  + "\"" + obs + "\""  +
                "  }\n";
        int posUltiActa = listadoActasAnteriores.size();
        Actas ultimaActa = new Actas();
        String obsOld = "";
        if(posUltiActa > 0){
            ultimaActa = (Actas) listadoActasAnteriores.get((posUltiActa - 1));
            obsOld = ultimaActa.getObservaciones();
        } else {
            obsOld = null;
        }

        String observacionNueva = "";
        if(obsOld == null){
             observacionNueva = "[" + observaciones + "]";
        }else{
            obsOld = obsOld.replace("]",",");
            observacionNueva = obsOld + observaciones + "]";
        }
        acta.setObservaciones(observacionNueva);
//        throw new Exception("gsgsg");

        getGateway().actualizarWEE(otId);

    }

    private String obtienefechaSegunFormato(Date fecha, String formatoSalida) {
        String fechaString = "";
        if (fecha.equals("null")) {
            return fecha.toString();
        } else {
            try {
                DateFormat formato = new SimpleDateFormat(formatoSalida);
                fechaString = formato.format(fecha);
                //System.out.println("====================>  Fecha de salida :  "+fechaString);
                return fechaString;
            } catch (Exception e) {
                fechaString = "error: "+e;
                return fechaString;
            }
        }
    }

    private static final Logger log = Logger.getLogger(AccionGenerarActaOrdinaria.class);

}
