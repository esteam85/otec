package com.skydream.eventmanager;


import com.itextpdf.text.*;
import com.skydream.coreot.dao.OtDAO;
import com.skydream.coreot.pojos.*;
import com.skydream.coreot.util.Config;
import com.skydream.coreot.util.RenderPDF;
import org.apache.fop.apps.FOPException;
import org.apache.log4j.Logger;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfPTable;

import javax.xml.transform.TransformerException;

/**
 * Created by jonathanRamirez on 07-03-16.
 */
public class AccionGenerarPdf extends Command {

    String nombrePdf;
    String nombreXsl;
    int tipo;

    AccionGenerarPdf(int tipo){

        setTipo(tipo);

    }

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {

        String xmlString = "";
        //int repositorioId = 5;

        try {

            Map mapaOT = (Map) params.get("datosOt");
            int otId = (int)mapaOT.get("id");
            int usuarioId = (int) params.get("usuarioId");
            int contrato;

            switch (getTipo()){
                case 1:
                    contrato = (int)((Map)mapaOT.get("contrato")).get("id");
                    this.setNombrePdf("pdf_ot_"+otId);
                    this.setNombreXsl("plantilla_ot_contrato_"+contrato);
                    xmlString = getGateway().obtenerXmlOt(otId);
                    break;
                case 2:
                    contrato = ((Ot) OtDAO.getINSTANCE().obtenerDetalleOt(otId)).getContrato().getId();
                    int actaId = (int) params.get("idActa");
                    this.setNombrePdf("pdf_acta_"+otId+"_"+actaId);
                    this.setNombreXsl("plantilla_acta_contrato_"+contrato);
                    xmlString = getGateway().obtenerXmlActa(actaId);
                    break;
            }

            if(xmlString!= null && !xmlString.isEmpty()){
                Usuarios usuario = (Usuarios) getGateway().retornarObjetoCoreOT(Usuarios.class, usuarioId);
                Config config = Config.getINSTANCE();
                Repositorios repoBase = (Repositorios) getGateway().obtenerRepositorio(config.getNombreRepositorioBase());

                RenderPDF render = new RenderPDF(repoBase);
                render.generarPDF_From_XmlString_XslFTP(xmlString, usuario.getRepositorio(), getNombrePdf(), getNombreXsl());
            }

            //Repositorios xslRepo = GenericDAO.getINSTANCE().obtenerRepositorio(repositorioId);
            //RenderPDF.getINSTANCE().generarPDF_From_XmlString_XslFTP(xmlString, usuario.getRepositorio(), getNombrePdf(), getNombreXsl());

        } catch (FOPException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (TransformerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            contAccionesBBDD.getAndIncrement();
        }

    }

    public String getNombrePdf() {
        return nombrePdf;
    }

    public void setNombrePdf(String nombrePdf) {
        this.nombrePdf = nombrePdf;
    }

    public String getNombreXsl() {
        return nombreXsl;
    }

    public void setNombreXsl(String nombreXsl) {
        this.nombreXsl = nombreXsl;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    private static final Logger log = Logger.getLogger(AccionGenerarPdf.class);
}
