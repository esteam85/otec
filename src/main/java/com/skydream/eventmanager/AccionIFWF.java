/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.pojos.*;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.log4j.Logger;

/**
 *
 * @author mcj
 */
public class AccionIFWF extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        //
        int usuarioEjecutor = (Integer) params.get("usuarioEjecutor");

        Map mapaOT = (Map) params.get("datosOt");
        Ot ot = (Ot) this.getGateway().retornarObjetoCoreOT(Ot.class, (Integer) mapaOT.get("id"));

        Map contratoVarAux = (Map)mapaOT.get("contrato");
        int idContrato = (int)contratoVarAux.get("id");
        Contratos contrato = (Contratos)this.getGateway().obtenerRegistroConFiltro(Contratos.class, null, "id", idContrato);

        int idWorkflow = contrato.getWorkflow().getId();
        List arrayWE = (List)params.get("workflow");
        Map<Integer, WorkflowEvento> mapaWE = retornarMapaWorkFlowEvento(arrayWE, this.getGateway(), ot, contrato, usuarioEjecutor);
        
        Map cubicador = (Map)mapaOT.get("cubicador");
        int idCubicador = (Integer)cubicador.get("id");
        List<WorkflowEventos> workflowEventos = this.getGateway().listarConFiltro33(idWorkflow);
        int ctd = 0;
        if (workflowEventos.size() > 0) {

                final WorkflowEvento workflowEventoProximo = new WorkflowEvento();
                List listadoWEE = new ArrayList<>();
                Date fechaCreacion = new Date();
                for (ObjetoCoreOT workflowkEvento : workflowEventos) {
                    WorkflowEventos we = (WorkflowEventos) workflowkEvento;
//                    System.out.println("************ EVENTO ID : " + we.getEvento().getId() + " ************");
                    WorkflowEventosEjecucion wee = new WorkflowEventosEjecucion();
                    listadoWEE.add(wee);
                    setearWorkflowEventosEjecucion(idCubicador, we, listadoWEE, ot, mapaWE, ctd, workflowEventoProximo, workflowEventos, fechaCreacion);

                    Serializable registro = getGateway().registrarWee(wee);
                    if (wee.getEjecutado() == 1) {
                        WorkflowEventosEjecucionHistorico weeHistorico = new WorkflowEventosEjecucionHistorico();
                        weeHistorico.setEventoPadre(wee.getEventoPadre());
                        weeHistorico.setFechaCreacion(wee.getFechaCreacion());
                        weeHistorico.setFechaInicioEstimada(wee.getFechaInicioReal());
                        weeHistorico.setFechaInicioReal(wee.getFechaInicioReal());
                        weeHistorico.setFechaTerminoEstimada(wee.getFechaTerminoEstimada());
                        weeHistorico.setFechaTerminoReal(wee.getFechaTerminoReal());
                        weeHistorico.setProveedorId(wee.getProveedorId());
                        weeHistorico.setProximo(wee.getProximo());
                        weeHistorico.setUsuarioEjecutorId(wee.getUsuarioEjecutorId());
                        weeHistorico.setUsuarioId(wee.getUsuarioId());
                        Ot orden = wee.getOt();
                        weeHistorico.setOt(orden);
                        WorkflowEventosEjecucionId weeId = wee.getId();
                        WorkflowEventosEjecucionHistoricoId weeHistoricoId = new WorkflowEventosEjecucionHistoricoId();
                        weeHistoricoId.setCubicadorId(idCubicador);
                        weeHistoricoId.setDecisionId(weeId.getDecisionId());
                        weeHistoricoId.setEventoId(weeId.getEventoId());
                        weeHistoricoId.setRolId(weeId.getRolId());
                        weeHistoricoId.setWorkflowId(weeId.getWorkflowId());
                        weeHistoricoId.setEjecutado(wee.getEjecutado());
                        weeHistorico.setId(weeHistoricoId);
                        getGateway().registrarWeeHistorico(weeHistorico);
                    }
                    ctd++;
            }

            try {
                getGateway().flush();
            } finally {
                contAccionesBBDD.getAndIncrement();
            }
        }
    }

    private void setearWorkflowEventosEjecucion(int idCubicador, WorkflowEventos we, List<WorkflowEventosEjecucion> listadoWEE, Ot ot, Map<Integer, WorkflowEvento> mapaWE, 
        int ctd, WorkflowEvento workflowEventoProximo, List listadoWorkflowEventos, Date fechaCreacion) throws Exception{
        WorkflowEventosEjecucionId weeId = new WorkflowEventosEjecucionId();
        weeId.setCubicadorId(idCubicador);
        weeId.setEventoId(we.getEvento().getId());
        weeId.setRolId(we.getRol().getId());
        weeId.setWorkflowId(we.getWorkflow().getId());
        weeId.setDecisionId(we.getDecision().getId());
        WorkflowEventosEjecucion wee = listadoWEE.get(ctd);
        
        wee.setId(weeId);
        wee.setOt(ot);
        
        wee.setProximo(we.getProximo());
        wee.setEventoPadre(we.getEventoPadre());
        WorkflowEvento varAux = mapaWE.get(we.getEvento().getId());
        int idUsuario = varAux.idUsuario;
        
        WorkflowEventosEjecucion weeAnterior = null;
        if(wee.getId().getDecisionId()==0 && ctd>0){
            weeAnterior = listadoWEE.get(ctd-1);
        }else if(wee.getId().getDecisionId()>0 && ctd>0){
            weeAnterior = listadoWEE.get(ctd-1);
            if(weeAnterior.getId().getDecisionId()>0){
                weeAnterior = listadoWEE.get(ctd-2);
            }
        }

        if(ctd == 0) {
            setearRegistroInicial(wee, we, idUsuario, mapaWE, workflowEventoProximo, fechaCreacion);
        }else{
            if (we.getEvento() != null && we.getEvento().getId() == workflowEventoProximo.idEvento) {
                wee.setEjecutado(9999);
            } else {
                wee.setEjecutado(0);
            }
            Facade facade = new Facade();
            Date fechaInicioEstimada = facade.sumarDiasHabiles(weeAnterior.getFechaTerminoEstimada(), 1);
            Date fechaFinalEstimada = facade.sumarDiasHabiles(fechaInicioEstimada, varAux.tiempo);
            setearFechas(wee, fechaInicioEstimada, fechaFinalEstimada, null, null, fechaCreacion);
        }
        
        if (we.getEvento().getEsTelefonica()) {
            wee.setUsuarioEjecutorId(idUsuario);
        }
        
        wee.setUsuarioId(idUsuario);
    }

    private void setearRegistroInicial(WorkflowEventosEjecucion wee, WorkflowEventos we, int idUsuario, 
                                                Map<Integer, WorkflowEvento> mapaWE, WorkflowEvento workflowEventoProximo, Date fechaCreacion) {
        setearFechas(wee, fechaCreacion, fechaCreacion, fechaCreacion, fechaCreacion, fechaCreacion);
        wee.setEjecutado(1);
        WorkflowEvento workflowEventoProx = mapaWE.get(we.getProximo());
        workflowEventoProximo.idEvento = workflowEventoProx.idEvento;
        workflowEventoProximo.idUsuario = workflowEventoProx.idUsuario;
    }
    
    private void setearFechas(WorkflowEventosEjecucion wee, Date fechaInicioEstimada, Date fechaTerminoEstimada, 
                                                        Date fechaInicioReal, Date fechaTerminoReal, Date fechaCreacion) {
        wee.setFechaInicioEstimada(fechaInicioEstimada);
        wee.setFechaTerminoEstimada(fechaTerminoEstimada);
        wee.setFechaInicioReal(fechaInicioReal);
        wee.setFechaTerminoReal(fechaTerminoReal);
        wee.setFechaCreacion(fechaCreacion);
    }

    private Map<Integer, WorkflowEvento> retornarMapaWorkFlowEvento(List<Map> listaDeParametros, GatewayAcciones gateway, Ot ot, Contratos contrato, int usuarioEjecutor) throws Exception {
        Map<Integer, WorkflowEvento> mapaRetorno = new HashMap();
        for (Map mapa : listaDeParametros) {
            WorkflowEvento we = new WorkflowEvento();
            we.setearObjetosDeInstancia(mapa, gateway, ot, contrato, usuarioEjecutor);
            mapaRetorno.put(we.idEvento, we);
        }
        return mapaRetorno;
    }


    private class WorkflowEvento implements Comparable<WorkflowEvento>{
        int idEvento;
        int idUsuario;
        int tiempo;
        
        public void setearObjetosDeInstancia(Map params, GatewayAcciones gateway, Ot ot, Contratos contrato, int usuarioEjecutor) throws Exception {
            if(params.containsKey("evento")){
                Map mapaEvento = (Map)params.get("evento");
                idEvento = (int)mapaEvento.get("id");
            }
            boolean decision = false;
            if(params.containsKey("decision")){
                decision = (boolean)params.get("decision");
            }

            if(params.containsKey("usuario")){
                if(decision){
                    List arrayUsuarios = (List)params.get("usuario");
                    int cantidadUsuarios = arrayUsuarios.size();
                    int orden = 0;
                    for(int i=0; i<cantidadUsuarios; i++){
                        orden++;
                        Map mapaUsuario = (Map)arrayUsuarios.get(i);
                        int id = (Integer) mapaUsuario.get("id");
//                        int orden = (int)mapaUsuario.get("orden");
                        if(i==0){
                            idUsuario = id;
                        }
                        // Se agrega logica para que almacene el usuario en la nueva tabla
                        boolean almacenaUsuario = gateway.almacenaUsuarioOt(ot, contrato, usuarioEjecutor, id, idEvento );

                        //almacenar en la tabla de usuarios_validadores
                        UsuariosValidadores usuarioValidador = new UsuariosValidadores();
                        Eventos evento = new Eventos();
                        evento.setId(idEvento);
                        usuarioValidador.setEventos(evento);
                        Usuarios usuario = new Usuarios();
                        usuario.setId(id);
                        usuarioValidador.setUsuarios(usuario);
                        usuarioValidador.setOrden(orden);
                        usuarioValidador.setOt(ot);
                        Conceptos concepto = new Conceptos();
                        try {
                            concepto = gateway.obtenerConceptoPorIdEvento(idEvento);
                            usuarioValidador.setConceptos(concepto);
                            boolean registro = gateway.registrarUsuariosValidadores(usuarioValidador);
                        } catch (Exception e) {
                            // retu
                            e.printStackTrace();
                            throw e;
                        }
                    }
                }else if(idEvento==16 || idEvento==17){

                    List arrayUsuarios = (List)params.get("usuario");
                    for(int i=0; i<arrayUsuarios.size(); i++){
                        Map mapaUsuario = (Map)arrayUsuarios.get(i);
                        int id = (Integer) mapaUsuario.get("id");
                        idUsuario = id;
                        // Se agrega logica para que almacene el usuario en la nueva tabla
                        boolean almacenaUsuario = gateway.almacenaUsuarioOt(ot, contrato, usuarioEjecutor, idUsuario, idEvento );

                        //almacenar en la tabla de usuarios_validadores
                        UsuariosValidadores usuarioValidador = new UsuariosValidadores();
                        Eventos evento = new Eventos();
                        evento.setId(idEvento);
                        usuarioValidador.setEventos(evento);
                        Usuarios usuario = new Usuarios();
                        usuario.setId(id);
                        usuarioValidador.setUsuarios(usuario);
                        usuarioValidador.setOrden(1);
                        usuarioValidador.setOt(ot);
                        Conceptos concepto = null;
                        try {
                            concepto = gateway.obtenerConceptoPorIdEvento(idEvento);
                            usuarioValidador.setConceptos(concepto);
                            gateway.registrarUsuariosValidadores(usuarioValidador);
                        } catch (Exception e) {
                            e.printStackTrace();
                            throw e;
                        }
                    }

                }else{
                    List arrayUsuarios = (List)params.get("usuario");
                    for(int i=0; i<arrayUsuarios.size(); i++) {
                        Map mapaUsuario = (Map) arrayUsuarios.get(i);
                        int id = (Integer) mapaUsuario.get("id");
                        if(i==0){
                            idUsuario = id;
                        }
                        int idUserOt = id;
                        // Se agrega logica para que almacene el usuario en la nueva tabla
                        boolean almacenaUsuario = gateway.almacenaUsuarioOt(ot, contrato, usuarioEjecutor, idUserOt, idEvento );
                        if(idEvento == 51 ){
                            // Aca se agregan 2 eventos de calidad que estan asociados al evento 51 de lavidar el acta en nuevo flujo calidad para efectos de validacion de usuarios eventos
                            almacenaUsuario = gateway.almacenaUsuarioOt(ot, contrato, usuarioEjecutor, idUserOt, 59 );
                        }
                    }

                }

            }
            if(params.containsKey("tiempo")){
                tiempo = (int)params.get("tiempo");
            }
        }

        @Override
        public int compareTo(WorkflowEvento we) {
            return Integer.compare(idEvento, we.idEvento);
        }
    
    }


//    private boolean almacenaUsuarioOt(GatewayAcciones gateway, Ot ot, Contratos contrato, int gestor, int usuario, int idEvento) throws Exception {
//
//        boolean registro = false;
//
//        UsuariosOt usuariosOt = new UsuariosOt();
//
//
//        usuariosOt.setOt(ot);
//        List<String> joinObjCoreOT = new ArrayList<>();
//        Eventos eventos = (Eventos) gateway.obtenerRegistroConFiltro(Eventos.class,joinObjCoreOT, "id", idEvento);
//        usuariosOt.setEventos(eventos);
//        Usuarios usuarios = (Usuarios) gateway.obtenerRegistroConFiltro(Usuarios.class,joinObjCoreOT, "id", usuario);
//        usuariosOt.setUsuarios(usuarios);
//        usuariosOt.setContratoId(contrato.getId());
//        usuariosOt.setGestorTelefonicaId(gestor);
//        usuariosOt.setParticipacion(eventos.getNombre());
//        Set<Roles> roles = usuarios.getUsuariosRoles();
//        Roles rolUsado = new Roles();
//        for (Roles rol: roles){
//            rolUsado = rol;
//        }
//        usuariosOt.setRoles(rolUsado);
//
//        UsuariosOtId usuariosOtId = new UsuariosOtId();
//        usuariosOtId.setEventoId(idEvento);
//        usuariosOtId.setOtId(ot.getId());
//        usuariosOtId.setUsuarioId(usuario);
//        usuariosOtId.setRolId(rolUsado.getId());
//
//        usuariosOt.setId(usuariosOtId);
//
//        try {
//            registro = gateway.registrarUsuariosOt(usuariosOt);
//        } catch (Exception e) {
//            // retu
//            e.printStackTrace();
//            throw e;
//        }
//
//        return registro;
//    }

    
    private static final Logger log = Logger.getLogger(AccionIFWF.class);
    
}
