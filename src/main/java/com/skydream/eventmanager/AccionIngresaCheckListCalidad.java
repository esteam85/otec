/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionIngresaCheckListCalidad extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        int idOt = 0;
        try {
            DetalleParametros detalleParametro = retornarDetalleParametros(accion);
            CoreOtFactory coreOtFactory = new CoreOtFactory();

            Map mapaOT = (Map) params.get("datosOt");
            int otId = (int)mapaOT.get("id");


            int eventoId = (int) params.get("idEvento");
            int eventoIdNuevo = Integer.parseInt(params.get("eventoId").toString());

            String jsonDatos = detalleParametro.getValor();


            String[] aux = jsonDatos.split("-");
            String llaveJson = aux[1];

            String[] campos = llaveJson.split(",");
            String campo1 = campos[0];
            String campo2 = campos[1];

            int idUsuario = buscarData(campo1, campo2,params);
            
            try {
//                this.getGateway().actualizarWorkfloEventoEjecucion(otId, eventoId, idUsuario);
//                getGateway().actualizarWEE(otId);
                Ot ot = (Ot) getGateway().obtenerRegistroConFiltro(Ot.class,null, "id", otId);
                Contratos contrato = new Contratos();
                contrato = (Contratos)this.getGateway().obtenerRegistroConFiltro(Contratos.class, null, "id", ot.getContrato().getId());


            } finally {
                contAccionesBBDD.getAndIncrement();
            }

            
        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }

    private int buscarData(String campo1, String campo2, Map params) {
        Map padre = (Map) params.get(campo1);
        String valor = (String) padre.get(campo2);
        return Integer.parseInt(valor);
    }

    private DetalleParametros retornarDetalleParametros(Acciones accion) {
        TreeSet<AccionesParametros> accionesParametros = (TreeSet) accion.getAccionesParametros();
        AccionesParametros accionParam = (AccionesParametros) accionesParametros.first();
        Parametros parametro = accionParam.getParametros();
        List<DetalleParametros> listaDetalleParametros = new ArrayList<>(parametro.getDetalleParametros());
        return listaDetalleParametros.get(0);
    }
    
    private static final Logger log = Logger.getLogger(AccionIngresaCheckListCalidad.class);
    
}
