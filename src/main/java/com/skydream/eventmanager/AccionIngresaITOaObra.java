/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.apache.xalan.xsltc.util.IntegerArray;
import org.hibernate.HibernateException;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionIngresaITOaObra extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        Map datosOt = new HashMap();
        datosOt = (HashMap) params.get("datosOt");
        int otId = Integer.parseInt(datosOt.get("id").toString());
        int eventoIto = Integer.parseInt(params.get("idEvento").toString());
        try {

            
            try {
                int eventoIdActual = 0;
                eventoIdActual = this.getGateway().obtieneWorflowEventoActualEjecucion(otId);
                this.getGateway().continuarFlujoWorkflowEventoEjecucionManual(otId, eventoIdActual, eventoIto);

            } finally {
                contAccionesBBDD.incrementAndGet();
            }

            
        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }

    
    private static final Logger log = Logger.getLogger(AccionIngresaITOaObra.class);
    
}
