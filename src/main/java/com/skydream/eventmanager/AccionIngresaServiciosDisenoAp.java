/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionIngresaServiciosDisenoAp extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        int idOt = 0;
        try {
            Map actividadMAp = (Map) params.get("actividadDisenoSeleccionada");
            List<LinkedHashMap> listaServCubSeleccionados = (List<LinkedHashMap>) params.get("serviciosCubicadosSeleccionados");
            List<LinkedHashMap> listaServCubAdicSeleccionados = (List<LinkedHashMap>) params.get("serviciosAdicionalesSeleccionados");
            int otId = (int)params.get("otId");
            int cubicadorId = (int)params.get("cubicadorId");
            int eventoId = Integer.parseInt(params.get("eventoId").toString());
            int usuarioId = (int) params.get("usuarioId");
            int eventoproximo = 0;
            contAccionesBBDD.incrementAndGet();
//                    Actualiza cubicacion borrando servicios anteriores y dejando solo de diseño
            boolean actualizaCubicacion = this.getGateway().actualizaCubicacionParePagoDisenoAp(otId, cubicadorId, listaServCubSeleccionados, listaServCubAdicSeleccionados);
            eventoproximo = this.getGateway().obtieneProximoEventoWee(eventoId, otId, false);
            this.getGateway().continuarFlujoWorkflowEventoEjecucionManual(otId, eventoId, eventoproximo);
//            throw new Exception("pruebas");
        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }

    }
    private String obtienefechaSegunFormato(Date fecha, String formatoSalida) {
        String fechaString = "";
        if (fecha.equals("null")) {
            return fecha.toString();
        } else {
            try {
                DateFormat formato = new SimpleDateFormat(formatoSalida);
                fechaString = formato.format(fecha);
                //System.out.println("====================>  Fecha de salida :  "+fechaString);
                return fechaString;
            } catch (Exception e) {
                fechaString = "error: "+e;
                return fechaString;
            }
        }
    }
    private int buscarData(String campo1, String campo2, Map params) {
        Map padre = (Map) params.get(campo1);
        String valor = (String) padre.get(campo2);
        return Integer.parseInt(valor);
    }

    private DetalleParametros retornarDetalleParametros(Acciones accion) {
        TreeSet<AccionesParametros> accionesParametros = (TreeSet) accion.getAccionesParametros();
        AccionesParametros accionParam = (AccionesParametros) accionesParametros.first();
        Parametros parametro = accionParam.getParametros();
        List<DetalleParametros> listaDetalleParametros = new ArrayList<>(parametro.getDetalleParametros());
        return listaDetalleParametros.get(0);
    }
    
    private static final Logger log = Logger.getLogger(AccionIngresaServiciosDisenoAp.class);
    
}
