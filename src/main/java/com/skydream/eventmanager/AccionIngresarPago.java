/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.HibernateException;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionIngresarPago extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        int idOt = 0;
        try {
            DetalleParametros detalleParametro = retornarDetalleParametros(accion);
            CoreOtFactory coreOtFactory = new CoreOtFactory();

            Map mapaOT = (Map) params.get("datosOt");
            int otId = (int)mapaOT.get("id");
            int idEventoPadre = (int) params.get("idEvento");
            int usuarioId = (int) params.get("usuarioId");

            Long hemId = Long.parseLong(String.valueOf(params.get("hemId")));
//            int facturaId = (int) params.get("facturaId");

            String fechaDerivadaAux = params.get("fechaContable").toString();
            SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
            Date fechaDerivada =  formatoDelTexto.parse(fechaDerivadaAux);

//            String fechaFacturaAux = params.get("fechaFactura").toString();
//            Date fechaFactura =  formatoDelTexto.parse(fechaFacturaAux);

//            String fechaHemAux = params.get("fechaHem").toString();
//            Date fechaHem =  formatoDelTexto.parse(fechaHemAux);


            try {
                // tengo q saber si es pago parcial o total
                double montoTotalActaPago = 0;
                List<Actas> actasTotales = new ArrayList<Actas>();
                actasTotales = this.getGateway().obtenerListadoActasValidadas(otId);
                for(Actas actasValidada : actasTotales){
//                    montoTotalActaPago = montoTotalActaPago + actasValidada.getMontoTotalAPagar();
                    montoTotalActaPago = montoTotalActaPago + actasValidada.getMontoTotalActa();
                }
                Actas acta = this.getGateway().obtenerActaValida(otId);
                boolean pagoTotal = acta.getEsTotal();
                double montoTotalCubicado = acta.getMontoCubicado();

                Pagos pago = new Pagos();
                pago.setEstado('A');
                pago.setHemId(hemId);
//                pago.setFacturaId(facturaId);
                pago.setFechaDerivada(fechaDerivada);
                pago.setFechaFactura(fechaDerivada);
                pago.setFechaHem(fechaDerivada);

                boolean registroPago = this.getGateway().registrarPago(pago);

                if(registroPago){
                    Ot ot = getGateway().obtieneOtPorId(otId);
                    if(ot.getContrato().getId() == 2) {
                        double numeroPedido = 0;
                        double numeroCarta = 0;

                        JSONObject jsonDetalleOtAux = new JSONObject((String) ot.getJsonDetalleOt());

                        numeroPedido = (double) Double.parseDouble(jsonDetalleOtAux.get("numeroPedido").toString());
                        numeroCarta = (double) Double.parseDouble(jsonDetalleOtAux.get("numeroCarta").toString());

                        if(ot.getContrato().getId() == 2){
                            getGateway().registrarValidacionGestionEconomicaPagos("detalle_bolsas_validaciones_pag", acta.getId(), otId,       true,           usuarioId,                 null,                               true,          pago.getId(),   usuarioId,            numeroCarta,   hemId);
                            //                                                                                         acta_id,    ot_id, validacion_acta, usuario_validador_id, motivo_rechazo_gestion_economica_id, validacion_parcial_acta, pagos_id, usuario_pagador_id, numero_contrato
                        }

                    }



                   if((montoTotalCubicado ==  montoTotalActaPago) || (montoTotalCubicado <  montoTotalActaPago)){
                        // evento 11
                        // tengo q avanzar la ot al evento 11
                        this.getGateway().continuarFlujoWorkfloEventoEjecucionManual(otId, idEventoPadre, false ,11);

                    }else if (montoTotalActaPago < montoTotalCubicado){
                        // evento 12
                        // tengo q avanzar la ot al evento 12
                        this.getGateway().continuarFlujoWorkfloEventoEjecucionDecision(otId, 12, false);
                    }
                }else{
                    throw new Exception();
                }
            } finally {
                contAccionesBBDD.getAndIncrement();
            }


        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }

    private int buscarData(String campo1, String campo2, Map params) {
        Map padre = (Map) params.get(campo1);
        String valor = (String) padre.get(campo2);
        return Integer.parseInt(valor);
    }

    private DetalleParametros retornarDetalleParametros(Acciones accion) {
        TreeSet<AccionesParametros> accionesParametros = (TreeSet) accion.getAccionesParametros();
        AccionesParametros accionParam = (AccionesParametros) accionesParametros.first();
        Parametros parametro = accionParam.getParametros();
        List<DetalleParametros> listaDetalleParametros = new ArrayList<>(parametro.getDetalleParametros());
        return listaDetalleParametros.get(0);
    }
    
    private static final Logger log = Logger.getLogger(AccionIngresarPago.class);
    
}
