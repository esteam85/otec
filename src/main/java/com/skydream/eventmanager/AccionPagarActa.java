package com.skydream.eventmanager;

import com.skydream.coreot.pojos.Acciones;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import com.skydream.coreot.pojos.WorkflowEventosEjecucion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by mcj on 03-11-15.
 */
public class AccionPagarActa extends Command{


    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        Map mapaOT = (Map) params.get("datosOt");
        int otId = (int) mapaOT.get("id");
        Integer idActa = (Integer)params.get("actaId");



        List<ObjetoCoreOT> listadoWEE = getGateway().listarConFiltro(WorkflowEventosEjecucion.class, null, "ot.id", otId, "ejecutado");
        Map<Integer, WorkflowEventosEjecucion> mapaWEE = new HashMap();

        int idEjecucionEventoAnterior = 0;
        WorkflowEventosEjecucion weeActual = null;
        for(ObjetoCoreOT itemWEE : listadoWEE){
            WorkflowEventosEjecucion wee = (WorkflowEventosEjecucion)itemWEE;
            mapaWEE.put(wee.getId().getEventoId(),wee);
            if(wee.getEjecutado()>0 && wee.getEjecutado()!=9999){
                idEjecucionEventoAnterior = wee.getEjecutado();
            }else if(wee.getEjecutado()==9999){
                weeActual = wee;
                break;
            }
        }

    }
}
