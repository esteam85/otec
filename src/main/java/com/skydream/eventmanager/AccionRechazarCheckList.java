/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionRechazarCheckList extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        int idOt = 0;
        try {

            this.getGateway().abrirSesion();
            this.getGateway().iniciarTransaccion();

            Map mapaOT = (Map) params.get("datosOt");
            int otId = (int)mapaOT.get("id");
            Ot ot = (Ot)getGateway().retornarObjetoCoreOT(Ot.class,otId);
            ot.setFechaInicioReal(new Date());
            int usuarioId = (int) params.get("usuarioId");
            Usuarios usuario = (Usuarios) getGateway().retornarObjetoCoreOT(Usuarios.class, usuarioId);
            String obs = (String) params.get("obs");
            int idActa = (int) params.get("idActa");
            Date fecha = new Date();
            String fechaFormat = obtienefechaSegunFormato(fecha, "dd-MM-yyyy");
            String observaciones = "{\n" +
                    "  \"usuario\": "  + "\"" + usuario.getNombres() + " " + usuario.getApellidos() + "\"" + ",\n" +
                    "  \"fecha\": "  + "\"" + fechaFormat + "\"" + ",\n" +
                    "  \"observaciones\": "  + "\"" + obs + "\"" +
                    "  }\n";

            this.getGateway().continuarFlujoWorkfloEventoEjecucionRechazoCheckList(idActa, observaciones, otId, 51, 58, 57);
            this.getGateway().commit();


        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }

    private int buscarData(String campo1, String campo2, Map params) {
        Map padre = (Map) params.get(campo1);
        String valor = (String) padre.get(campo2);
        return Integer.parseInt(valor);
    }

    private DetalleParametros retornarDetalleParametros(Acciones accion) {
        TreeSet<AccionesParametros> accionesParametros = (TreeSet) accion.getAccionesParametros();
        AccionesParametros accionParam = (AccionesParametros) accionesParametros.first();
        Parametros parametro = accionParam.getParametros();
        List<DetalleParametros> listaDetalleParametros = new ArrayList<>(parametro.getDetalleParametros());
        return listaDetalleParametros.get(0);
    }
    
    private static final Logger log = Logger.getLogger(AccionRechazarCheckList.class);
    private String obtienefechaSegunFormato(Date fecha, String formatoSalida) {
        String fechaString = "";
        if (fecha.equals("null")) {
            return fecha.toString();
        } else {
            try {
                DateFormat formato = new SimpleDateFormat(formatoSalida);
                fechaString = formato.format(fecha);
                //System.out.println("====================>  Fecha de salida :  "+fechaString);
                return fechaString;
            } catch (Exception e) {
                fechaString = "error: "+e;
                return fechaString;
            }
        }
    }
}
