/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.pojos.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionRegistrarNotificacion extends Command{

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {

        Map mapaOT = (Map) params.get("datosOt");
        int otId = (int)mapaOT.get("id");
        
        EjecutaAccionesPorEvento ejecAcciones = new EjecutaAccionesPorEvento();

        int eventoId = (int) params.get("idEvento");
//        WorkflowEventosEjecucion proximoEventoAEjecutar = ejecAcciones.retornarProximoEventoOT(getGateway(), otId);
        List<WorkflowEventosEjecucion> listadoWEE = ejecAcciones.retornarProximoEventoOT2(getGateway(), otId);
        int idUsuarioActual = 0;
        int idUsuarioProximo = 0;
        int idProximoInicial = 0;
        for (WorkflowEventosEjecucion wee : listadoWEE) {
            if(eventoId == wee.getId().getEventoId()){
                idUsuarioActual = wee.getUsuarioId();
                idProximoInicial = wee.getProximo();

            }
            if(wee.getEjecutado() == 9999 || idProximoInicial == wee.getId().getEventoId()){
                idUsuarioProximo = wee.getUsuarioId();
            }

        }
        if(idProximoInicial == 0){
            idUsuarioProximo = getGateway().obtieneProximoUsuarioDecision(eventoId, otId, listadoWEE);
        }

        List<String> join = new ArrayList<>();
        join.add("mensajeEvento");
        
        Eventos evento = (Eventos)getGateway().obtenerRegistroConFiltro(Eventos.class, join, "id", eventoId);
        
        System.out.println("idUsuario a notificar : " + idUsuarioProximo);
        Usuarios proxUsrEjecutor = (Usuarios)getGateway().obtenerRegistroConFiltro(Usuarios.class, join, "id", idUsuarioProximo);
        String mensajeNotificacion = evento.getMensajeEvento().getMensajeNotificacion() + " " + otId;
//        Notificaciones notificacion = new Notificaciones(0,varAux.getOt(),varAux.getId().getEventoId(),proxUsrEjecutor.getId(),evento.getMensajeEvento().getMensajeNotificacion(),new Date(),proxUsrEjecutor.getEmail());
        Ot ot = new Ot();
        ot.setId(otId);
        Notificaciones notificacion = new Notificaciones(0,ot,eventoId,idUsuarioProximo,mensajeNotificacion,new Date(),proxUsrEjecutor.getEmail(),false);


        try {
            getGateway().registrarConId(notificacion);
        } finally {
            contAccionesBBDD.getAndIncrement();
        }
        
    }
    
    
    
}
