/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author christian
 */
public class AccionRegistrarOTSupervision extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        try {
            DetalleParametros detalleParametro = retornarDetalleParametros(accion);
            CoreOtFactory coreOtFactory = new CoreOtFactory();
            Ot ot = (Ot)coreOtFactory.getObjetoCoreOt("Ot");

            Date fechaCreacion = new Date();
            ot.setFechaCreacion(fechaCreacion);
            
            String jsonDatos = detalleParametro.getValor();
            Map mapaOT = null;
            
            String[] aux = jsonDatos.split("-");
            String llaveJson = aux[1];
            mapaOT = (Map) params.get(llaveJson);
            
            ObjetoCoreOT objetoCoreOT = ot;
            objetoCoreOT.setearObjetoDesdeMap(mapaOT,0);
            TipoEstadoOt tipoEstadoOt = new TipoEstadoOt();
            tipoEstadoOt.setId(1);
            ot.setTipoEstadoOt(tipoEstadoOt);
            if(ot.getTipoOt().getId() == 2){
//                ot.setTipoEstadoOt(null);
                ot.setNumeroAp(0);
            }

            try{
                Serializable idRegistro = this.getGateway().registrarConId(objetoCoreOT);
                Integer idObjetoCreado = Integer.parseInt(idRegistro.toString());
                mapaOT.put("id", idObjetoCreado);
                Map cubicacion = (Map) mapaOT.get("cubicador");
                int idCubicacion = (int) cubicacion.get("id");
                boolean actualizo = this.getGateway().actualizarCubicacion(idObjetoCreado, idCubicacion);

            }finally{
                contAccionesBBDD.getAndIncrement();
            }
            
        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }
    
    private DetalleParametros retornarDetalleParametros(Acciones accion) {
        TreeSet<AccionesParametros> accionesParametros = (TreeSet) accion.getAccionesParametros();
        AccionesParametros accionParam = (AccionesParametros) accionesParametros.first();
        Parametros parametro = accionParam.getParametros();
        List<DetalleParametros> listaDetalleParametros = new ArrayList<>(parametro.getDetalleParametros());
        return listaDetalleParametros.get(0);
    }
    
    private static final Logger log = Logger.getLogger(AccionRegistrarOTSupervision.class);
    
}
