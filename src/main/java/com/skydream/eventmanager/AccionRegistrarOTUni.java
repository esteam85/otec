/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionRegistrarOTUni extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        try {
            DetalleParametros detalleParametro = retornarDetalleParametros(accion);
            CoreOtFactory coreOtFactory = new CoreOtFactory();
            Ot ot = (Ot)coreOtFactory.getObjetoCoreOt("Ot");

            Date fechaCreacion = new Date();
            ot.setFechaCreacion(fechaCreacion);
            
            String jsonDatos = detalleParametro.getValor();
            Map mapaOT = null;
            
            String[] aux = jsonDatos.split("-");
            String llaveJson = aux[1];
            mapaOT = (Map) params.get(llaveJson);
            
            ObjetoCoreOT objetoCoreOT = ot;
            objetoCoreOT.setearObjetoDesdeMap(mapaOT,0);
            TipoEstadoOt tipoEstadoOt = new TipoEstadoOt();
            tipoEstadoOt.setId(1);
            ot.setTipoEstadoOt(tipoEstadoOt);
            if(ot.getTipoOt().getId() == 2){
//                ot.setTipoEstadoOt(null);
                ot.setNumeroAp(0);
            }
            Map datosOt = (Map) params.get("datosOt");
            Map cubicadorMap = (Map) datosOt.get("cubicador");
            try{
                Serializable idRegistro = this.getGateway().registrarConId(objetoCoreOT);
                guardarAtributosGeoServicios(cubicadorMap);
                Integer idObjetoCreado = Integer.parseInt(idRegistro.toString());
                mapaOT.put("id", idObjetoCreado);
                Map cubicacion = (Map) mapaOT.get("cubicador");
                int idCubicacion = (int) cubicacion.get("id");
                boolean actualizo = this.getGateway().actualizarCubicacion(idObjetoCreado, idCubicacion);

            }finally{
                contAccionesBBDD.getAndIncrement();
            }
            
        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }
    private void guardarAtributosGeoServicios(Map cubicacion){
        int idServicio = 0;
        int idAgencia = 0;
        int idCentral = 0;
        List<Map> serviciosMap = (ArrayList<Map>) cubicacion.get("serviciosUnificado");
        for (Map servicio : serviciosMap){
            idServicio = Integer.parseInt(servicio.get("id").toString());

            Map agencia = (HashMap) servicio.get("agencia");
            Map central = (HashMap) servicio.get("central");

            idAgencia = Integer.parseInt(agencia.get("id").toString());
            idCentral = Integer.parseInt(central.get("id").toString());
            try{
                this.getGateway().ingresarLocalizacionAServiciosCubicados(idServicio, idAgencia, idCentral);
            } catch (Exception e){
                log.error("****** Error al actualizar cubicador detalle al crear OT, error: "+ e.toString());
            }

        };
    };
    private DetalleParametros retornarDetalleParametros(Acciones accion) {
        TreeSet<AccionesParametros> accionesParametros = (TreeSet) accion.getAccionesParametros();
        AccionesParametros accionParam = (AccionesParametros) accionesParametros.first();
        Parametros parametro = accionParam.getParametros();
        List<DetalleParametros> listaDetalleParametros = new ArrayList<>(parametro.getDetalleParametros());
        return listaDetalleParametros.get(0);
    }
    
    private static final Logger log = Logger.getLogger(AccionRegistrarOTUni.class);
    
}
