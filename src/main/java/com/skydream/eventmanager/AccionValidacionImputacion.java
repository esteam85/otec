package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by mcj on 25-11-15.
 */
public class AccionValidacionImputacion extends Command {



    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {

        try {
            Map mapaOT = (Map) params.get("datosOt");
            int otId = (int) mapaOT.get("id");

            int eventoId = (int) params.get("idEvento");
            int usuarioId = (int) params.get("usuarioId");
            int actaId = (int)params.get("idActa");
            String pep2 = "";
            try{
                pep2  = (String)params.get("pep2");
            }catch (Exception e){
                Map mapaPep2 = (HashMap) params.get("pep2");
                pep2 = (String)mapaPep2.get("id");
            }



            UsuariosValidadores usuarioValidador = (UsuariosValidadores)getGateway().obtenerUsuarioValidador(otId,eventoId,usuarioId);
            ValidacionesOtec validacionOtec = new ValidacionesOtec();
            validacionOtec.setUsuariosValidadores(usuarioValidador);
            validacionOtec.setFecha(new Date());
            validacionOtec.setValidacion(true);

            try {
                getGateway().registrarValidacionUsuario(validacionOtec);
                getGateway().registrarValidacionGestionEconomica("detalle_bolsas_validaciones_imp", actaId, otId, true, usuarioId, null,true);
//                GestionEconomica_old gestionEconomica = (GestionEconomica_old)getGateway().obtenerGestionEconomica(actaId);
//                gestionEconomica.setPep2(pep2);
//                getGateway().actualizarGestionEconomica(gestionEconomica);
                getGateway().actualizarWEE(otId);
            } finally {
                contAccionesBBDD.getAndIncrement();
            }



        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }

    }


    private static final Logger log = Logger.getLogger(AccionIngresarPago.class);

}
