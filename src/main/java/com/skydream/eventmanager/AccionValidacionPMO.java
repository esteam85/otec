package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by mcj on 25-11-15.
 */
public class AccionValidacionPMO extends Command {



    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {

        try {
            DetalleParametros detalleParametro = retornarDetalleParametros(accion);
            CoreOtFactory coreOtFactory = new CoreOtFactory();

            Map mapaOT = (Map) params.get("datosOt");
            int otId = (int) mapaOT.get("id");
            int usuarioId = (int) params.get("usuarioId");
            int actaId = (int)params.get("idActa");
            int lineaPresupuestariaId = Integer.parseInt(params.get("lp").toString());
            int pmoId = Integer.parseInt(params.get("idPmo").toString());
            int eventoId = (int) params.get("idEvento");

            UsuariosValidadores usuarioValidador = (UsuariosValidadores)getGateway().obtenerUsuarioValidador(otId,eventoId,usuarioId);
            ValidacionesOtec validacionOtec = new ValidacionesOtec();
            validacionOtec.setUsuariosValidadores(usuarioValidador);
            validacionOtec.setFecha(new Date());
            validacionOtec.setValidacion(true);
            Actas acta = (Actas)getGateway().retornarObjetoCoreOT(Actas.class,actaId);
            try {
                getGateway().registrarValidacionUsuario(validacionOtec);
                getGateway().registrarValidacionGestionEconomica("detalle_bolsas_validaciones_pmo", actaId, otId, true, usuarioId, null,true);
                getGateway().actualizarWEE(otId);
            } finally {
                contAccionesBBDD.getAndIncrement();
            }
        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }

    }


    private DetalleParametros retornarDetalleParametros(Acciones accion) {
        TreeSet<AccionesParametros> accionesParametros = (TreeSet) accion.getAccionesParametros();
        AccionesParametros accionParam = (AccionesParametros) accionesParametros.first();
        Parametros parametro = accionParam.getParametros();
        List<DetalleParametros> listaDetalleParametros = new ArrayList<>(parametro.getDetalleParametros());
        return listaDetalleParametros.get(0);
    }


    private static final Logger log = Logger.getLogger(AccionIngresarPago.class);

}
