/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.apache.xalan.xsltc.util.IntegerArray;
import org.hibernate.HibernateException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionValidarActa extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        int idOt = 0;
        try {
            DetalleParametros detalleParametro = retornarDetalleParametros(accion);
            CoreOtFactory coreOtFactory = new CoreOtFactory();

            Map mapaOT = (Map) params.get("datosOt");
            int otId = (int)mapaOT.get("id");
            int eventoId = (int) params.get("idEvento");
            int usuarioId = (int) params.get("usuarioId");
            String obs = (String) params.get("obs");
            int idActa = (int) params.get("idActa");

            boolean validacionUsuario = (boolean) params.get("validacionActa");
            Map otConstruccion = (Map) params.get("crearOtContruccion");
            int eventoOtConstruccion = 0;
            boolean crearOt = false;
            if(otConstruccion != null) {
                crearOt = (boolean) otConstruccion.get("crearOt");

                if (crearOt) {
                    eventoOtConstruccion = (int) otConstruccion.get("eventoId");
                }
            }

            Date fecha = new Date();
            String fechaFormat = obtienefechaSegunFormato(fecha, "dd-MM-yyyy");
            Usuarios usuario = (Usuarios) getGateway().retornarObjetoCoreOT(Usuarios.class, usuarioId);
            String observaciones = "{\n" +
                    "  \"usuario\": "  + "\"" + usuario.getNombres() + " " + usuario.getApellidos() + "\"" + ",\n" +
                    "  \"fecha\": "  + "\"" + fechaFormat + "\"" + ",\n" +
                    "  \"observaciones\": "  + "\"" + obs + "\"" +
                    "  }\n";

            try {
                if(validacionUsuario){
                    Object obj = params.get("montoDiponiblePagoPorcentage");
                    int porcentagePagoMap = 0;
                    if(obj instanceof Double){
                        porcentagePagoMap = ((Double) obj).intValue();
                    }else if(obj instanceof Integer){
                        porcentagePagoMap = (Integer)obj;
                    }
//                    int porcentagePagoMap = (int) params.get("montoDiponiblePagoPorcentage");
                    
                    String opcionPago = (String) params.get("opcionPago");

//                    int montoPagar = (int) params.get("montoDiponiblePago");
                    //Obtener listado de usuarios
                    int proximoValidador = 0;
                    boolean registroValidacion = false;
                    List<UsuariosValidadores> usuariosValidadores = this.getGateway().obtenerUsuariosValidadoresd(otId, eventoId);
                    for (UsuariosValidadores usuarios : usuariosValidadores){
                        if(usuarios.getUsuarios().getId() == usuarioId){
                            // registrar Validacion
                            proximoValidador = usuarios.getOrden() + 1;
                            // registrar la validacion
                            registroValidacion = this.getGateway().registrarUsuarioValidador(otId, eventoId, usuarioId, validacionUsuario, obs);
                        }
                    }
                    boolean proximoUsuarioValidador = false;
                    int usuarioIdProximo = 0;
                    for (UsuariosValidadores usuarios : usuariosValidadores){
                        if(usuarios.getOrden() == proximoValidador){
                            usuarioIdProximo = usuarios.getUsuarios().getId();
                            proximoUsuarioValidador = true;
                        }
                    }
                    if(registroValidacion){
                        if(proximoUsuarioValidador){
                            // actualizo el registro de wee para notificar al proximo usuario que debe validar
                            boolean actualizo = this.getGateway().actualizarUsuarioWorkflowEventoEjecucion(otId, eventoId, usuarioId, usuarioIdProximo);
                        }else{
                            // si no hay mas usuario validadores avanzo el evento al proximo y actualizo al acta a true en el campo validacion
                            boolean pagoTotal = false;
                            int porcentagePago = 100;
                            if(opcionPago.equalsIgnoreCase("pagoTotal")){
                                pagoTotal = true;
                            } else {
                                porcentagePago = porcentagePagoMap;
                            }
                            Ot ot = new Ot();
                            ot = getGateway().obtieneOtPorId(otId);
                            if(ot.getContrato().getId() == 1){
                                this.getGateway().actualizarValidacionActaSbe(idActa, validacionUsuario, pagoTotal, observaciones, otId, porcentagePago);
                            } else {
                                this.getGateway().actualizarValidacionActa(idActa, validacionUsuario, pagoTotal, observaciones);
                            }
//                            this.getGateway().actualizarValidacionActa(otId, validacionUsuario);
//                        getGateway().actualizarWEE(otId);
                            if(ot.getTipoOt().getId() == 2){
                                if(crearOt){
                                    this.getGateway().continuarFlujoWorkfloEventoEjecucionManualOtConstruccion(otId, eventoId, 5, eventoOtConstruccion, usuarioId, true, ot.getContrato().getId());
                                } else {
                                    eventoOtConstruccion = this.getGateway().obtieneProximoEventoPagarAp(eventoId, otId, false);
                                    this.getGateway().continuarFlujoWorkfloEventoEjecucionDecision(otId, eventoId, crearOt);
                                    this.getGateway().continuarFlujoWorkflowEventoEjecucionManual(otId, eventoId, eventoOtConstruccion);
//                                this.getGateway().continuarFlujoWorkfloEventoEjecucionDecision(otId, eventoOtConstruccion, crearOt);
                                }
                            } else {
                                eventoOtConstruccion = this.getGateway().obtieneProximoEventoPagarAp(eventoId, otId, false);
                                if(eventoOtConstruccion != 0){
                                    eventoOtConstruccion = this.getGateway().obtieneProximoEventoWee(eventoOtConstruccion, otId, false);
                                    int eventoPosicionado = this.getGateway().obtieneEventoEventoPadreWee(eventoId, otId, false);

                                    this.getGateway().continuarFlujoWorkflowEventoEjecucionManual(otId, eventoPosicionado, eventoId);
                                    this.getGateway().continuarFlujoWorkflowEventoEjecucionManual(otId, eventoId, eventoOtConstruccion);
                                } else {
                                    this.getGateway().continuarFlujoWorkfloEventoEjecucionDecision(otId, eventoId, false);
                                }

                            }

//                            this.getGateway().continuarFlujoWorkfloEventoEjecucionDecision(otId, eventoId, false);
                        }
                    }else{
                        throw new Exception();
                    }

                    //Setear historico servicios y adicionales
                    List<DetalleServiciosActas> serviciosActa = this.getGateway().obtieneDetalleServiciosEjecutados(idActa);
                    List<DetalleServiciosAdicionalesActas> adicionalesActa = this.getGateway().obtieneDetalleServiciosAdicionalesEjecutados(idActa);

                    for(DetalleServiciosActas servicioActa: serviciosActa){
                        servicioActa.setTotalHistoricoActa(servicioActa.getTotalHistoricoActa() + servicioActa.getTotalMontoEjecucionActa());
                        servicioActa.setTotalUnidadesEjecucionOt(servicioActa.getTotalUnidadesEjecucionOt() + servicioActa.getTotalUnidadesEjecucionActa());
                    }

                    for(DetalleServiciosAdicionalesActas serviciosAdicionalActa: adicionalesActa){
                        serviciosAdicionalActa.setTotalHistoricoActa(serviciosAdicionalActa.getTotalHistoricoActa() + serviciosAdicionalActa.getTotalMontoEjecucionActa());
                        serviciosAdicionalActa.setTotalUnidadesEjecucionOt(serviciosAdicionalActa.getTotalUnidadesEjecucionOt() + serviciosAdicionalActa.getTotalUnidadesEjecucionActa());
                    }
                    //Fin Setear historico servicios y adicionales

                }else{

                    List listadoMateriales = (List) params.get("detalleMaterialesAdicionalesActa");
                    if(!listadoMateriales.isEmpty()){
                        for(Object obj: listadoMateriales){
                            Map mapaMaterial = (Map) obj;
                            String materialId = (String)mapaMaterial.get("id");
                            boolean valido = (boolean)mapaMaterial.get("validar");
                            this.getGateway().registrarValidacionUsuarioActaMateriaAdicional(idActa, Integer.parseInt(materialId), valido);

                        }
                    }

                    List listadoServicios = (List) params.get("detalleServiciosAdicionalesActa");
                    if(!listadoServicios.isEmpty()){
                        for(Object obj: listadoServicios){
                            Map mapaServicios = (Map) obj;
                            String servicioId = (String)mapaServicios.get("id");
                            boolean valido = (boolean)mapaServicios.get("validar");
                            this.getGateway().registrarValidacionUsuarioActaServicioAdicional(idActa, Integer.parseInt(servicioId), valido);
                        }
                    }
                    // actualizo el acta con la validacion del usuario

                    this.getGateway().actualizarValidacionActa(idActa, validacionUsuario, false, observaciones);
                    // avanzo el workflow segun decision
                    this.getGateway().continuarFlujoWorkfloEventoEjecucionDecision(otId, eventoId, true);

                }
            } finally {
//                throw new Exception();
                contAccionesBBDD.getAndIncrement();
            }


        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }

    }
    private String obtienefechaSegunFormato(Date fecha, String formatoSalida) {
        String fechaString = "";
        if (fecha.equals("null")) {
            return fecha.toString();
        } else {
            try {
                DateFormat formato = new SimpleDateFormat(formatoSalida);
                fechaString = formato.format(fecha);
                //System.out.println("====================>  Fecha de salida :  "+fechaString);
                return fechaString;
            } catch (Exception e) {
                fechaString = "error: "+e;
                return fechaString;
            }
        }
    }
    private int buscarData(String campo1, String campo2, Map params) {
        Map padre = (Map) params.get(campo1);
        String valor = (String) padre.get(campo2);
        return Integer.parseInt(valor);
    }

    private DetalleParametros retornarDetalleParametros(Acciones accion) {
        TreeSet<AccionesParametros> accionesParametros = (TreeSet) accion.getAccionesParametros();
        AccionesParametros accionParam = (AccionesParametros) accionesParametros.first();
        Parametros parametro = accionParam.getParametros();
        List<DetalleParametros> listaDetalleParametros = new ArrayList<>(parametro.getDetalleParametros());
        return listaDetalleParametros.get(0);
    }
    
    private static final Logger log = Logger.getLogger(AccionValidarActa.class);
    
}
