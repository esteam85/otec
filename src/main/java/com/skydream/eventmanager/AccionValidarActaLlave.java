/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.dao.CubicadorDAO;
import com.skydream.coreot.dao.Gateway;
import com.skydream.coreot.dao.OtDAO;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.HibernateException;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionValidarActaLlave extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        int idOt = 0;
        try {
            CoreOtFactory coreOtFactory = new CoreOtFactory();

//            Map cubicacion = (Map) params.get("cubicacion");
//            Map servicio = (Map) params.get("servicio");
            Map mapaOT = (Map) params.get("datosOt");
            int otId = (int)mapaOT.get("id");
//            int eventoId = (int) params.get("idEvento");
            int usuarioId = (int) params.get("usuarioId");
            Map gestorConstruccion = (Map) params.get("gestorConstruccion");
            int gestorConstruccionId = 0;
            try {
                gestorConstruccionId = (int) gestorConstruccion.get("id");
            }catch (Exception e){
                // no hago nada
            }

            String obs = (String) params.get("obs");
            int idActa = (int) params.get("idActa");


            List listadoServicios = (List) params.get("detalleServiciosAdicionalesActa");
            if(!listadoServicios.isEmpty()){
                for(Object obj: listadoServicios){
                    Map mapaServicios = (Map) obj;
                    String servicioId = (String)mapaServicios.get("id");
                    boolean valido = (boolean)mapaServicios.get("validar");
                    this.getGateway().registrarValidacionUsuarioActaServicioAdicional(idActa, Integer.parseInt(servicioId), valido);
                }
            }

            List listadoMateriales = (List) params.get("detalleMaterialesAdicionalesActa");
            if(!listadoMateriales.isEmpty()){
                for(Object obj: listadoMateriales){
                    Map mapaMaterial = (Map) obj;
                    String materialId = (String)mapaMaterial.get("id");
                    boolean valido = (boolean)mapaMaterial.get("validar");
                    this.getGateway().registrarValidacionUsuarioActaMateriaAdicional(idActa, Integer.parseInt(materialId), valido);

                }
            }

            this.getGateway().actualizarValidacionActaLLaveDiseño(idActa, obs);

            if(gestorConstruccionId == 0){
                this.getGateway().continuarFlujoWorkfloEventoEjecucionLlaveManual(otId, 51, 52, 6, usuarioId, false);
            }else{
                this.getGateway().continuarFlujoWorkfloEventoEjecucionLlaveManual(otId, 51, 52, 53, gestorConstruccionId, true);
            }
//            throw new Exception();
//            this.getGateway().commit();


        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        } finally {
            contAccionesBBDD.getAndIncrement();
        }
    }

    
    private static final Logger log = Logger.getLogger(AccionValidarActaLlave.class);

    private String obtienefechaSegunFormato(Date fecha, String formatoSalida) {
        String fechaString = "";
        if (fecha.equals("null")) {
            return fecha.toString();
        } else {
            try {
                DateFormat formato = new SimpleDateFormat(formatoSalida);
                fechaString = formato.format(fecha);
                //System.out.println("====================>  Fecha de salida :  "+fechaString);
                return fechaString;
            } catch (Exception e) {
                fechaString = "error: "+e;
                return fechaString;
            }
        }
    }
    
}
