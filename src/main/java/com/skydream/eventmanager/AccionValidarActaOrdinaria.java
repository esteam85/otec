/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.apache.poi.util.SystemOutLogger;
import org.apache.xmlbeans.impl.common.SystemCache;
import org.hibernate.HibernateException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class AccionValidarActaOrdinaria extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        int idOt = 0;
        try {
            DetalleParametros detalleParametro = retornarDetalleParametros(accion);
            CoreOtFactory coreOtFactory = new CoreOtFactory();

            Map mapaOT = (Map) params.get("datosOt");
            int otId = (int)mapaOT.get("id");
            int eventoId = (int) params.get("idEvento");
            int usuarioId = (int) params.get("usuarioId");
            String obs = (String) params.get("obs");
            int idActa = (int) params.get("idActa");

            boolean validacionUsuario = (boolean) params.get("validacionActa");

            Date fecha = new Date();
            String fechaFormat = obtienefechaSegunFormato(fecha, "dd-MM-yyyy");
            Usuarios usuario = (Usuarios) getGateway().retornarObjetoCoreOT(Usuarios.class, usuarioId);
            String observaciones = "{\n" +
                    "  \"usuario\": "  + "\"" + usuario.getNombres() + " " + usuario.getApellidos() + "\"" + ",\n" +
                    "  \"fecha\": "  + "\"" + fechaFormat + "\"" + ",\n" +
                    "  \"observaciones\": "  + "\"" + obs + "\"" +
                    "  }\n";

            try {
                if(validacionUsuario){
                    String opcionPago = (String) params.get("opcionPago");
                    //Obtener listado de usuarios
                    int proximoValidador = 0;
                    boolean registroValidacion = false;
                    List<UsuariosValidadores> usuariosValidadores = this.getGateway().obtenerUsuariosValidadoresd(otId, eventoId);
                    for (UsuariosValidadores usuarios : usuariosValidadores){
                        if(usuarios.getUsuarios().getId() == usuarioId){
                            // registrar Validacion
                            proximoValidador = usuarios.getOrden() + 1;
                            // registrar la validacion
                            registroValidacion = this.getGateway().registrarUsuarioValidador(otId, eventoId, usuarioId, validacionUsuario, obs);
                        }
                    }
                    boolean proximoUsuarioValidador = false;
                    int usuarioIdProximo = 0;
                    for (UsuariosValidadores usuarios : usuariosValidadores){
                        if(usuarios.getOrden() == proximoValidador){
                            usuarioIdProximo = usuarios.getUsuarios().getId();
                            proximoUsuarioValidador = true;
                        }
                    }
                    if(registroValidacion){
                        if(proximoUsuarioValidador){
                            // actualizo el registro de wee para notificar al proximo usuario que debe validar
                            boolean actualizo = this.getGateway().actualizarUsuarioWorkflowEventoEjecucion(otId, eventoId, usuarioId, usuarioIdProximo);
                        }else{
                            // si no hay mas usuario validadores avanzo el evento al proximo y actualizo al acta a true en el campo validacion
                            boolean pagoTotal = false;
                            if(opcionPago.equalsIgnoreCase("pagoTotal")){
                                pagoTotal = true;
                            }

                            this.getGateway().actualizarValidacionActa(idActa, validacionUsuario, pagoTotal, observaciones);
                            this.getGateway().continuarFlujoWorkfloEventoEjecucionDecision(otId, eventoId, false);
                        }
                        boolean validoMateriales = false;
                        int idMaterial = 0;
                        List<LinkedHashMap> arrayMaterialesUtilizadosActa = (List)params.get("detalleMaterialesActa");
                        for (LinkedHashMap itemInformadoAux: arrayMaterialesUtilizadosActa){
                            Set set = itemInformadoAux.entrySet();
                            Iterator i = set.iterator();
                            while(i.hasNext()) {
                                Map.Entry me = (Map.Entry)i.next();
                                boolean inserto = false;
                                switch ((String)me.getKey()) {
                                    case "id":
                                        idMaterial = Integer.parseInt(me.getValue().toString());
                                        validoMateriales = getGateway().validarCubicacionOrdinaria(idMaterial, idActa);
                                        break;
                                }
                            }
                        }
                        if (!validoMateriales){
                            throw new Exception("No se pudo validar materiales ordinarios");
                        }
                    }else{
                        throw new Exception();
                    }

                }else{
                    List<LinkedHashMap> arrayMaterialesUtilizadosActa = (List)params.get("detalleMaterialesActa");
                    boolean inserto = false;
                    double precioRechazado = 0;
                    for (LinkedHashMap itemInformadoAux: arrayMaterialesUtilizadosActa){
                        Set set = itemInformadoAux.entrySet();
                        Iterator i = set.iterator();
                        int idMaterial = 0;
                        while(i.hasNext()) {
                            Map.Entry me = (Map.Entry)i.next();
                            switch ((String)me.getKey()) {
                                case "id":
                                    idMaterial = Integer.parseInt(me.getValue().toString());
                                    break;
                                case "cantidadActualInformada":
                                    precioRechazado =  Integer.parseInt(me.getValue().toString()) * 1.0;
                            }
                        }
                        inserto = getGateway().actualizaActaRechazada(idMaterial, precioRechazado, idActa);
                    }
                    this.getGateway().actualizarValidacionActa(idActa, validacionUsuario, false, observaciones);
                    // avanzo el workflow segun decision
                    this.getGateway().continuarFlujoWorkfloEventoEjecucionDecision(otId, eventoId, true);

                }
            } finally {
                contAccionesBBDD.getAndIncrement();
            }

            
        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }

    private int buscarData(String campo1, String campo2, Map params) {
        Map padre = (Map) params.get(campo1);
        String valor = (String) padre.get(campo2);
        return Integer.parseInt(valor);
    }

    private DetalleParametros retornarDetalleParametros(Acciones accion) {
        TreeSet<AccionesParametros> accionesParametros = (TreeSet) accion.getAccionesParametros();
        AccionesParametros accionParam = (AccionesParametros) accionesParametros.first();
        Parametros parametro = accionParam.getParametros();
        List<DetalleParametros> listaDetalleParametros = new ArrayList<>(parametro.getDetalleParametros());
        return listaDetalleParametros.get(0);
    }
    private String obtienefechaSegunFormato(Date fecha, String formatoSalida) {
        String fechaString = "";
        if (fecha.equals("null")) {
            return fecha.toString();
        } else {
            try {
                DateFormat formato = new SimpleDateFormat(formatoSalida);
                fechaString = formato.format(fecha);
                //System.out.println("====================>  Fecha de salida :  "+fechaString);
                return fechaString;
            } catch (Exception e) {
                fechaString = "error: "+e;
                return fechaString;
            }
        }
    }
    private static final Logger log = Logger.getLogger(AccionValidarActaOrdinaria.class);
    
}
