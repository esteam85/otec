/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.*;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author JonathanRamirez
 */
public class AccionValidarOT extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        try {
            CoreOtFactory coreOtFactory = new CoreOtFactory();

            int otId = (int)params.get("otId");
            int eventoId = (int) params.get("eventoId");
            int usuarioId = (int) params.get("usuarioId");
            String obs = (String) params.get("obs");
            boolean validacionUsuario = false;
            int eventoPadre = 31;
            if(eventoId == 32){
                validacionUsuario = true;
            } else if(eventoId == 33) {
                validacionUsuario = false;
            }

            Date fecha = new Date();
            String fechaFormat = obtienefechaSegunFormato(fecha, "dd-MM-yyyy");
            Usuarios usuario = (Usuarios) getGateway().retornarObjetoCoreOT(Usuarios.class, usuarioId);

            boolean registroValidacion = false;
            boolean validacionOt = false;
            validacionOt = this.getGateway().determinaAprobacionGerarquica(otId);

            try {
                if(validacionUsuario){
                    //Obtener listado de usuarios
                    int proximoValidador = 0;
                    List<UsuariosValidadores> usuariosValidadores = this.getGateway().obtenerUsuariosValidadoresd(otId, eventoPadre);
                    for (UsuariosValidadores usuarios : usuariosValidadores){
                        if(usuarios.getUsuarios().getId() == usuarioId){
                            // registrar Validacion
                            proximoValidador = usuarios.getOrden() + 1;
                            // registrar la validacion
                            registroValidacion = this.getGateway().registrarUsuarioValidador(otId, eventoPadre, usuarioId, validacionUsuario, obs);
                        }
                    }
                    boolean proximoUsuarioValidador = false;
                    int usuarioIdProximo = 0;
                    for (UsuariosValidadores usuarios : usuariosValidadores){
                        if(usuarios.getOrden() == proximoValidador){
                            usuarioIdProximo = usuarios.getUsuarios().getId();
                            proximoUsuarioValidador = true;
                        }
                    }
                    if(registroValidacion){
                        if(proximoUsuarioValidador && validacionOt){
                            // actualizo el registro de wee para notificar al proximo usuario que debe validar
                            boolean actualizo = this.getGateway().actualizarUsuarioWorkflowEventoEjecucion(otId, eventoPadre, usuarioId, usuarioIdProximo);
                        }else{
                            // si no hay mas usuario validadores avanzo el evento al proximo
                            this.getGateway().continuarFlujoWorkfloEventoEjecucionDecision(otId, eventoId, false);
                        }
                    }else{
                        throw new Exception();
                    }

                }else{
                    boolean actualizo = false;
                    registroValidacion = this.getGateway().registrarAutorizacion(otId, eventoPadre, usuarioId, obs,  validacionUsuario);
                    actualizo = this.getGateway().actualizarEstadoOt(otId, 4, obs);
                    // avanzo el workflow segun decision
                    if(actualizo && registroValidacion){
                        this.getGateway().continuarFlujoWorkfloEventoEjecucionDecision(otId, eventoId, true);
                    }


                }
            } finally {
                contAccionesBBDD.getAndIncrement();
            }


        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }


 private String obtienefechaSegunFormato(Date fecha, String formatoSalida) {
        String fechaString = "";
        if (fecha.equals("null")) {
            return fecha.toString();
        } else {
            try {
                DateFormat formato = new SimpleDateFormat(formatoSalida);
                fechaString = formato.format(fecha);
                //System.out.println("====================>  Fecha de salida :  "+fechaString);
                return fechaString;
            } catch (Exception e) {
                fechaString = "error: "+e;
                return fechaString;
            }
        }
    }
    private static final Logger log = Logger.getLogger(AccionValidarActaUnificado.class);

}