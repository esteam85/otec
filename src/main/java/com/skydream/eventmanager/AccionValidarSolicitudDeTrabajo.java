/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.CoreOtFactory;
import com.skydream.coreot.pojos.Acciones;
import com.skydream.coreot.pojos.Ot;
import com.skydream.coreot.pojos.Usuarios;
import com.skydream.coreot.pojos.UsuariosValidadores;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author JonathanRamirez
 */
public class AccionValidarSolicitudDeTrabajo extends Command {

    @Override
    public void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD) throws Exception {
        try {
            CoreOtFactory coreOtFactory = new CoreOtFactory();

            int otId = (int)params.get("otId");
            int eventoId = (int) params.get("eventoId");
            int usuarioId = (int) params.get("usuarioId");
            String obs = (String) params.get("obs");
            boolean validacionUsuario = false;
            int eventoPadre = 45;
            if(eventoId == 46){
                validacionUsuario = true;
            } else if(eventoId == 47) {
                validacionUsuario = false;
            }
            boolean registroValidacion = false;
            List<Usuarios> usuarios = new ArrayList<Usuarios>();
            try {
                if(validacionUsuario){
                    //Obtener listado de usuarios
                    Ot ot = new Ot();
                    ot = this.getGateway().obtieneOtPorId(otId);
                    usuarios = this.getGateway().obtenerUsuariosPorRolProovedorGatewayAcciones(3, ot.getProveedor().getId(), ot.getContrato().getId());
                    this.getGateway().adjudicaOt(otId, 3, usuarios, usuarioId, eventoPadre, ot.getContrato().getId());
                    registroValidacion = this.getGateway().registrarUsuarioValidador(otId, eventoPadre, usuarioId, validacionUsuario, obs);
                    if(registroValidacion){
                        this.getGateway().continuarFlujoWorkfloEventoEjecucionDecision(otId, eventoId, false);
                    }else{
                        throw new Exception();
                    }

                }else{
                    registroValidacion = this.getGateway().registrarAutorizacion(otId, eventoPadre, usuarioId, obs,  validacionUsuario);
                    // avanzo el workflow segun decision
                    if(registroValidacion){
                        this.getGateway().continuarFlujoWorkfloEventoEjecucionDecision(otId, eventoId, true);
                    }


                }
            } finally {
                contAccionesBBDD.getAndIncrement();
            }


        } catch (HibernateException e) {
            log.fatal("Error al crear OT. ", e);
            throw e;
        }
    }


    private static final Logger log = Logger.getLogger(AccionValidarActaUnificado.class);

}