/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.pojos.Acciones;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class ActualizarWEE implements Chain{
    
    Chain nextInChain;

    @Override
    public void setNextChain(Chain nextChain) {
        this.nextInChain = nextChain;
    }

    @Override
    public void executeAction(GatewayAcciones gateway, AtomicInteger contAccionesBBDD,Acciones accion, Map parametros) throws Exception{
        if (accion.getTipoEjecucionBackEnd().equals("BBDD")) {
            actualizarWorkflowEventoEjecucion(gateway, contAccionesBBDD, accion, parametros);
        } else {
            nextInChain.executeAction(gateway, contAccionesBBDD, accion, parametros);
        }
    }

    private void actualizarWorkflowEventoEjecucion(GatewayAcciones gateway, AtomicInteger contAccionesBBDD, Acciones accion, Map params) throws Exception {
        FactoryAcciones accionAEjecutar = new FactoryAcciones();
        Command accionEjec = accionAEjecutar.getAccionBBDD(accion.getCodigo());
        if(accionEjec.getGateway()==null){
            accionEjec.setGateway(gateway);
        }
        accionEjec.execute(accion, params, contAccionesBBDD);
    }
    
}
