/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.pojos.Acciones;
import com.skydream.eventmanager.GatewayAcciones;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public interface Chain {
    
    void setNextChain(Chain nextChain);
    
    void executeAction(GatewayAcciones gateway, AtomicInteger contAccionesBBDD,Acciones eventoAccion, Map parametros) throws Exception;
    
}
