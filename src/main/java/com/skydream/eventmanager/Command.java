/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.pojos.Acciones;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public abstract class Command {
    
    private GatewayAcciones gateway;
    
    public abstract void execute(Acciones accion, Map params, AtomicInteger contAccionesBBDD)throws Exception;
    
    public void setGateway(GatewayAcciones gateway){
        this.gateway = gateway;
    }

    public GatewayAcciones getGateway() {
        return this.gateway;
    }
    
}
