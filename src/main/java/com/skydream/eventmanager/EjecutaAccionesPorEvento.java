/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.dao.EventoDAO;
import com.skydream.coreot.dao.Gateway;
import com.skydream.coreot.pojos.Eventos;
import com.skydream.coreot.pojos.EventosAcciones;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import com.skydream.coreot.pojos.WorkflowEventosEjecucion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class EjecutaAccionesPorEvento {
    
    public TreeSet<EventosAcciones> obtenerAcciones(int idEvento)throws Exception{
        Eventos evento = (Eventos)EventoDAO.getINSTANCE().retornarObjCoreOtPorId(idEvento);
        TreeSet listadoAcciones = (TreeSet)evento.getEventosAcciones();
        return listadoAcciones;
    }
    
    public void ejecutarAcciones(int idEvento, TreeSet<EventosAcciones> eventosAcciones, Map params) throws Exception{
        Chain chain1 = setearCadenaDeAcciones();
        GatewayAcciones gateway = null;
        int contadorAccionesBBDD = retornarCantidadAccionesEnBBDD(eventosAcciones);
        AtomicInteger contadorAccionesBBDDaux = new AtomicInteger();
        contadorAccionesBBDDaux.set(0);
        gateway = new GatewayAcciones();
        try {
            if (contadorAccionesBBDD > 0) {
                gateway.abrirSesion();
                gateway.iniciarTransaccion();
            }
            int contadorAccion = 0;
            params.put("idEvento" , idEvento);
            for (EventosAcciones eventoAccion : eventosAcciones) {
                System.out.println("Ejecutando accion " + contadorAccion + ", id(" + eventoAccion.getOrden() + ")");
                chain1.executeAction(gateway, contadorAccionesBBDDaux, eventoAccion.getAcciones(), params);
                contadorAccion++;
            }
            if (contadorAccionesBBDD > 0 && contadorAccionesBBDDaux.intValue() == contadorAccionesBBDD) {
                gateway.commit();
            }
        } catch (Exception ex) {
            if (contadorAccionesBBDDaux.intValue() > 0)
                gateway.rollback();
            throw ex;
        }finally{
            if (contadorAccionesBBDD > 0) {
                gateway.cerrarSesion();
            }
        }
        
    }
    
    public List retornarListadoFeriados() throws Exception{
        GatewayAcciones gate = new GatewayAcciones();
        try {
            gate.abrirSesion();
            List feriados = gate.listarFeriados();
            return feriados;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }finally{
            gate.cerrarSesion();
        }
    }
    
    protected WorkflowEventosEjecucion retornarProximoEventoOT(GatewayAcciones gate, int idOt) {
        WorkflowEventosEjecucion proximoEventoAEjecutar = null;
        try {
            List<ObjetoCoreOT> listadoWEE = gate.listarConFiltro(WorkflowEventosEjecucion.class, null, "ot.id", idOt, "id.eventoId");
            proximoEventoAEjecutar = null;
            for (ObjetoCoreOT objeto : listadoWEE) {
                WorkflowEventosEjecucion wee = (WorkflowEventosEjecucion) objeto;
                int idEjecucion = wee.getEjecutado();
                boolean eventoProximo = idEjecucion == 9999;
                if (eventoProximo) {
                    proximoEventoAEjecutar = wee;
                    break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return proximoEventoAEjecutar;
    }

    protected List<WorkflowEventosEjecucion> retornarProximoEventoOT2(GatewayAcciones gate, int idOt) {
        List<WorkflowEventosEjecucion> listadoWEE = new ArrayList<>();
        try {
            listadoWEE = gate.obtenerEventoEjecucion(idOt);

//            for (WorkflowEventosEjecucion wee : listadoWEE) {
//                if(idEvento == wee.getId().getEventoId()){
//                    proximoEventoAEjecutar = wee;
//                    break;
//                }
//            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return listadoWEE;
    }

    protected WorkflowEventosEjecucion obtenerEventoEjecucion2(GatewayAcciones gate, int idOt, int idEvento) {
        WorkflowEventosEjecucion proximoEventoAEjecutar = null;
        try {
            List<ObjetoCoreOT> listadoWEE = gate.listarConFiltro(WorkflowEventosEjecucion.class, null, "ot.id", idOt, "id.eventoId");
            proximoEventoAEjecutar = null;
            for (ObjetoCoreOT objeto : listadoWEE) {
                WorkflowEventosEjecucion wee = (WorkflowEventosEjecucion) objeto;
                int idEjecucion = wee.getEjecutado();
                boolean eventoProximo = idEjecucion == 9999;
                if (eventoProximo) {
                    proximoEventoAEjecutar = wee;
                    break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return proximoEventoAEjecutar;
    }


    public void ejecutarAcciones2(int idEvento, TreeSet<EventosAcciones> eventosAcciones, Map params) throws Exception{
        Chain chain1 = setearCadenaDeAcciones();
        GatewayAcciones gateway = null;
        int contadorAccionesBBDD = retornarCantidadAccionesEnBBDD(eventosAcciones);
        AtomicInteger contadorAccionesBBDDaux = new AtomicInteger();
        contadorAccionesBBDDaux.set(0);
        gateway = new GatewayAcciones();
        try {
            if (contadorAccionesBBDD > 0) {
                gateway.abrirSesion();
                gateway.iniciarTransaccion();
            }
            int contadorAccion = 0;
            for (EventosAcciones eventoAccion : eventosAcciones) {

                params.put("idEvento" , idEvento);
                System.out.println("Ejecutando accion " + contadorAccion + ", id(" + eventoAccion.getOrden() + ")");
                chain1.executeAction(gateway, contadorAccionesBBDDaux, eventoAccion.getAcciones(), params);
                contadorAccion++;
            }
            if (contadorAccionesBBDD > 0 && contadorAccionesBBDDaux.intValue() == contadorAccionesBBDD) {
                gateway.commit();
//                throw new Exception("pruebas");
            }
        } catch (Exception ex) {
            if (contadorAccionesBBDDaux.intValue() > 0)
                gateway.rollback();
            throw ex;
        }finally{

            if (contadorAccionesBBDD > 0) {
                gateway.cerrarSesion();
            }
        }

    }

    private Chain setearCadenaDeAcciones() {
        Chain chain1 = new RegistrarActualizarDatos();
//        Chain chain2 = new EnviarMail();
//        chain1.setNextChain(chain2);
        return chain1;
    }


    private int retornarCantidadAccionesEnBBDD(TreeSet<EventosAcciones> eventosAcciones){
        int contador=0;
        for(EventosAcciones eveAcc : eventosAcciones){
            if(eveAcc.getAcciones().getTipoEjecucionBackEnd().equals("BBDD")){
                contador++;
            }
        }
        return contador;
    }
    
}
