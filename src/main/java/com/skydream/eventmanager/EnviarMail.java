/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.pojos.*;
import com.skydream.coreot.util.SendMail;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author mcj
 */
public class EnviarMail implements Chain{
    
    Chain nextInChain;

    @Override
    public void setNextChain(Chain nextChain) {
        this.nextInChain = nextChain;
    }

    @Override
    public void executeAction(GatewayAcciones gateway, AtomicInteger contAccionesBBDD, Acciones accion, Map parametros) throws Exception{
        if(accion.getTipoEjecucionBackEnd().equals("MAIL")){
            enviarMail(gateway, accion, parametros);
        }else{
            nextInChain.executeAction(gateway, contAccionesBBDD, accion, parametros);
        }
    }
    
    
    /* Se obtiene el usuario al que se le debe enviar mail, si el usuario que ejecuta el evento no es
    ** un usuario de telefonica, y el proximo ejecutor no es un usuario de tmc, se debe enviar mail tambien al
    ** gestor de la ot
    */
    private void enviarMail(GatewayAcciones gate, Acciones accion, Map parametros)throws Exception{
        Map mapaOt = (Map)parametros.get("datosOt");
        Integer idOt = (Integer)mapaOt.get("id");

        WorkflowEventosEjecucion weeEnEjecucion = retornarUltimoEventoEjecutadoOT(gate, idOt);
        Usuarios usuarioGestor = (Usuarios)gate.obtenerRegistroConFiltro(Usuarios.class, null, "id", weeEnEjecucion.getOt().getGestor().getId());


        EjecutaAccionesPorEvento ejecAcc = new EjecutaAccionesPorEvento();
        WorkflowEventosEjecucion weeProximo = ejecAcc.retornarProximoEventoOT(gate, idOt);
        
        Usuarios usuarioProximaEjecucion = null;
        try {
            usuarioProximaEjecucion = (Usuarios)gate.obtenerRegistroConFiltro(Usuarios.class, null, "id", weeProximo.getUsuarioId());
        }catch(Exception e){
            e.printStackTrace();
            throw e;
        }
        
        List<Parametros> params = retornarDetalleParametros(accion);
        List<DetalleParametros> detParam1 = new ArrayList(params.get(0).getDetalleParametros());
        
        String asunto = detParam1.get(0).getValor();
        
//        List<DetalleParametros> detParam2 = new ArrayList(params.get(1).getDetalleParametros());
        int eventoId = (int) parametros.get("idEvento");
        List<String> join = new ArrayList<>();
        join.add("mensajeEvento");
        Eventos evento = (Eventos) gate.obtenerRegistroConFiltro(Eventos.class, join, "id", eventoId);


        String cuerpo = evento.getMensajeEvento().getMensajeNotificacion() + " " +  idOt;
//        String cuerpo = evento.getMensajeEvento().getMensajeNotificacion();
        
        List<String> listaDestinatariosCC = new ArrayList<>();
        int idEsTmc = 9999;
        if(usuarioGestor.getProveedor().getId()!=idEsTmc && usuarioProximaEjecucion.getProveedor().getId()!=idEsTmc){
            listaDestinatariosCC.add(usuarioGestor.getEmail());
        }
        SendMail.getINSTANCE().enviarMail(usuarioProximaEjecucion.getEmail(),asunto,cuerpo,listaDestinatariosCC);
    }
    
    
    /**
    ** Se obtiene el listado de eventos de la ot en la tabla workflow_eventos_ejecucion y
    ** se considera como ultimo evento ejecutado el que se encuentra antes del registro  
    ** con idEjecucion 9999
    */
    private WorkflowEventosEjecucion retornarUltimoEventoEjecutadoOT(GatewayAcciones gate,int idOt) {
        WorkflowEventosEjecucion proximoEventoAEjecutar = null;
        try {
            List<ObjetoCoreOT> listadoWEE = gate.listarConFiltro(WorkflowEventosEjecucion.class, null, "ot.id", idOt, "id.eventoId");
            proximoEventoAEjecutar = null;
            int ejecutado = 0;
            // Obtengo el valor de ejecutado
            for(ObjetoCoreOT objeto : listadoWEE){
                WorkflowEventosEjecucion wee = (WorkflowEventosEjecucion) objeto;
                int ejecutadoWee = wee.getEjecutado();
                if(ejecutadoWee > ejecutado && ejecutadoWee != 9999){
                    ejecutado = ejecutadoWee;
                    proximoEventoAEjecutar = wee;
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return proximoEventoAEjecutar;
    }

    private List<Parametros> retornarDetalleParametros(Acciones accion) {
        TreeSet<AccionesParametros> accionesParametros = (TreeSet) accion.getAccionesParametros();
        List<Parametros> parametros = new ArrayList<>();
        for(AccionesParametros accionesParams : accionesParametros){
            parametros.add(accionesParams.getParametros());
        }
        return parametros;
    }
    
}
