/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.dao.GenericDAO;
import com.skydream.coreot.dao.LoginLogoutDAO;
import com.skydream.coreot.pojos.*;
import com.skydream.coreot.util.SecureAccess;
import com.skydream.eventmanager.EjecutaAccionesPorEvento;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.hibernate.HibernateException;

import java.awt.*;
import java.io.*;
import java.math.BigInteger;
import java.util.*;
import java.util.List;

/**
 *
 * @author mcj
 */
public class Facade {
    
    EjecutaAccionesPorEvento ejecutor;
    
    public Facade(){
        ejecutor = new EjecutaAccionesPorEvento();
    }
    
    public void ejecutarEvento(int idEvento, Map params) throws Exception{
        log.debug("Ejecutando evento : " + idEvento);
        TreeSet<EventosAcciones> acciones = ejecutor.obtenerAcciones(idEvento);
        ejecutor.ejecutarAcciones(idEvento, acciones, params);
    }

    public void ejecutarEvento2(int idEvento, Map params) throws Exception{
        log.debug("Ejecutando evento : " + idEvento);
        TreeSet<EventosAcciones> acciones = ejecutor.obtenerAcciones(idEvento);
        ejecutor.ejecutarAcciones2(idEvento, acciones, params);
    }
    
    public Date sumarDiasHabiles(Date fecha, int dias) throws Exception {
        List<Feriados> listadoFeriados = null;
        EjecutaAccionesPorEvento executor = new EjecutaAccionesPorEvento();
        listadoFeriados = executor.retornarListadoFeriados();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe

        int cont = 1;
        while (cont <= dias) {
            calendar.add(Calendar.DATE, 1);
            if (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
                    && !esFeriado(calendar.getTime(), listadoFeriados)) {
                cont++;
            }
        }
        return calendar.getTime(); // Devuelve el objeto Date con las nuevas horas añadidas
    }
    
    private Boolean esFeriado(Date fecha, List<Feriados> listadoFeriados){
        Calendar fechaBuscada = Calendar.getInstance();
        fechaBuscada.setTime(fecha);
        Boolean esFeriado = false;
        for(Feriados feriado : listadoFeriados){
            Calendar calendario = Calendar.getInstance();
            calendario.setTime(feriado.getFecha()); // Configuramos la fecha que se recibe
            if(calendario.get(Calendar.DATE)==fechaBuscada.get(Calendar.DATE) && 
                    calendario.get(Calendar.MONTH)==fechaBuscada.get(Calendar.MONTH)){
                esFeriado = true;
                break;
            }
        }
        return esFeriado;
    }

    public byte[] retornarArchivoDescargaEnExcel(int usuarioId)throws Exception{
        byte[] retorno = null;
        List<Object[]> listadoOts = retornarResumenOtsAPagar(usuarioId);

        //Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();

        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Resumen Pagos");

        //This data needs to be written (Object[])
        Map<String, Object[]> datos = new TreeMap<>();
        //Seteando encabezados
        datos.put("1",new Object[]{"Tipo OT","Contrato","Proveedor","Gestor","ID PMO","LP","Sitio","Plan de Proyecto","PEP2","Numero OT","Nombre OT","Fecha Inicio", "Fecha Termino", "Monto Total"});

        Integer contador = 2;
        for(Object[] obj : listadoOts){
            log.debug(obj.getClass().toString());
            int ultimoItemObjeto = obj.length - 1;
            datos.put((contador++).toString(), obj);
        }

        //Iterate over data and write to sheet
        Set<String> keyset = datos.keySet();
        int rownum = 0;
        for (String key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object [] objArr = datos.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                XSSFCreationHelper createHelper = workbook.getCreationHelper();
                XSSFCellStyle cellStyle         = workbook.createCellStyle();
                XSSFDataFormat xssfDataFormat = createHelper.createDataFormat();
                String formato = "";
                if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
                else if(obj instanceof Date){
                    cell.setCellValue((Date)obj);
                    formato = "dd-MM-yyyy";
                }else if(obj instanceof  Double){
                    cell.setCellValue((Double) obj);
                    formato = "#,##0.000";
                }
                cellStyle.setDataFormat(xssfDataFormat.getFormat(formato));
                cell.setCellStyle(cellStyle);

            }
        }
        try {
            //Write the workbook in file system
            File archivoTemporal = new File("ArchivoPagosOTs.xlsx");
            FileOutputStream out = new FileOutputStream(archivoTemporal);
            workbook.write(out);
            out.close();
            ByteArrayOutputStream baos;
            baos = leerArchivo(archivoTemporal);
            retorno = baos.toByteArray();
            log.debug("ArchivoPagosOTs.xlsx written successfully on disk.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retorno;
    }

    public byte[] retornarArchivoReporteEnExcel(int usuarioId, int rolId)throws Exception{
        byte[] retorno = null;
        int reporteId;
        String reporteNombre;

        int hojaId;
        String hojaNombre;
        String hojaColumnas;
        String hojaQuery;

        SXSSFWorkbook workbook;

//        List<Object[]> reporteOts = GenericDAO.getINSTANCE().obtenerListaReporteOtExcel(usuarioId);
//        List<Object[]> reporteOtsTipoServicio = GenericDAO.getINSTANCE().obtenerListaReporteOtTipoServicioExcel(usuarioId);
//        List<Object[]> reporteOtsServicios = GenericDAO.getINSTANCE().obtenerListaReporteOtServicioExcel(usuarioId);
//        List<Object[]> reporteOtsActividadesPlazos = GenericDAO.getINSTANCE().obtenerListaReporteOtActividadesPlazosExcel(usuarioId);
//        List<Object[]> reporteOtsContratoActividad = GenericDAO.getINSTANCE().obtenerListaReporteOtContratoActividadExcel(usuarioId);
//
//        XSSFWorkbook workbook = new XSSFWorkbook();
//
//        XSSFSheet sheet1 = workbook.createSheet("1 - OTs");
//        XSSFSheet sheet2 = workbook.createSheet("2 - OTs - Tipo Servicios");
//        XSSFSheet sheet3 = workbook.createSheet("3 - OTs - Servicios");
//        XSSFSheet sheet4 = workbook.createSheet("4 - OTs - Actividad - Plazos");
//        XSSFSheet sheet5 = workbook.createSheet("5 - OTs - Contratos Actividad");
//
//
//
//
//        Object[] titulosHoja1 = {"id_ot","gestor","administrador_ec","coordinador_ec","trabajador_ec","tipo_ot","tipo","avba","contrato","proveedor","pep2","idPmo","lp","codigo_sitio","plan_proyecto","nombre_ot","tipo_estado_ot","fecha_creacion_ot","fecha_inicio_real_ot","fecha_termino_real_ot","fecha_inicio_estimada_ot","fecha_termino_estimada_ot","actividad_por_realizar","monto_total_inicial_ap","monto_total_cubicado","monto_total_a_pagar","monto_total_a_pagar_real","porcentaje_a_pagar", "acta","fecha_validacion_acta","hem","fecha_hem","derivada","fecha_derivada"};
//        Object[] titulosHoja2 = {"id_ot","gestor","tipo_ot","contrato","proveedor","monto_total","tipo_servicio_id","tipo_servicio"};
//        Object[] titulosHoja3 = {"ot_id","proveedor","tipo_ot","contrato_id","contrato","id","servicio_material","cantidad","precio","total","validacion","tipo","tipo_item"};
//        Object[] titulosHoja4 = {"id_ot","proveedor","contrato","actividad","fecha_inicio_estimada_actividad","fecha_inicio_real_actividad","fecha_termino_estimada_actividad","fecha_termino_real_actividad","cumplimiento_plazos_inicio_actividad","cumplimiento_plazos_termino_actividad","duracion_dias_actividad_real","duracion_dias_actividad_sla","cumplimiento_plazos_actividad"};
//        Object[] titulosHoja5 = {"contrato_id","contrato","actividad","cantidad_ordenes","monto_total_cubicado","monto_total_a_pagar"};
//
//        writeSheetOptimized(workbook, sheet1, titulosHoja1, reporteOts);
//        writeSheetOptimized(workbook, sheet2, titulosHoja2, reporteOtsTipoServicio);
//        writeSheetOptimized(workbook, sheet3, titulosHoja3, reporteOtsServicios);
//        writeSheetOptimized(workbook, sheet4, titulosHoja4, reporteOtsActividadesPlazos);
//        writeSheetOptimized(workbook, sheet5, titulosHoja5, reporteOtsContratoActividad);

        Object [] obj = GenericDAO.getINSTANCE().obtenerReportePorRol(rolId);

        reporteId = (Integer) obj[0];
        reporteNombre = (String) obj[1];
        workbook = new SXSSFWorkbook();

        List<Object[]> listaHojasReporte = GenericDAO.getINSTANCE().obtenerHojasPorReporte(reporteId);

        if(listaHojasReporte.size() > 0){

            for(Object[] obj2 : listaHojasReporte){ //rh.rep_hoja_id, h.nombre, h.columnas, h.query

                hojaId = (Integer) obj2[0];
                hojaNombre = (String) obj2[1];
                hojaColumnas = (String) obj2[2];
                hojaQuery = (String) obj2[3];

                List<Object[]> hojaReporte = GenericDAO.getINSTANCE().obtenerHojaReporte(hojaId, usuarioId);

                Sheet sheetGeneric = workbook.createSheet(hojaNombre);

                writeSheetOptimizedGeneric(workbook, sheetGeneric, hojaColumnas, hojaReporte);

            }

        }

        if(rolId == 6 ){ //pagos
            List<Object[]> reporteGEDash = GenericDAO.getINSTANCE().obtenerListaReporteGestionEconomicaExcelDash(usuarioId, rolId, true, true);
            Sheet sheet6 = workbook.createSheet("8 - Gestion Economica - PAG");
            Object[] titulosHoja6 = {"id_bolsa","sitio","numero_contrato","proveedor","id_pmo","lp","gestor","codigo_sitio","pep2","id_ot", "id_acta","aprobador","codigo_catalogo","valor_unitario","cantidad_pagar","valor_pagar","valor_total","moneda","centro", "grupo_articulo", "tipo_imputacion","codigo_aprobador","cuenta_mayor","contrato","numero_material","fecha_ingreso","estado","usuario_asignado","evento","motivo_rechazo","fecha_rechazo","fecha_pago","hem","hem_adj","derivada","fecha_contable"};
            writeSheetOptimizedNoNull2(workbook, sheet6, titulosHoja6, reporteGEDash);

        }else if (rolId == 8 ){ // PMO
            List<Object[]> reporteGEDash = GenericDAO.getINSTANCE().obtenerListaReporteGestionEconomicaExcelDash(usuarioId, rolId, false, false);
            Sheet sheet6 = workbook.createSheet("8 - Gestion Economica - PMO");
            Object[] titulosHoja6 = {"id_bolsa","sitio","numero_contrato","proveedor","id_pmo","lp","gestor","codigo_sitio","pep2","id_ot","id_acta","aprobador","codigo_catalogo","valor_unitario","cantidad_pagar","valor_pagar","valor_total","moneda","centro", "grupo_articulo", "tipo_imputacion","codigo_aprobador","cuenta_mayor","contrato","numero_material","fecha_ingreso","estado","usuario_asignado","evento","motivo_rechazo","fecha_rechazo","fecha_pago","hem", "hem_adj","derivada","fecha_contable"};
            writeSheetOptimizedNoNull2(workbook, sheet6, titulosHoja6, reporteGEDash);

        }else if (rolId == 9 ){ //IMP
            List<Object[]> reporteGEDash = GenericDAO.getINSTANCE().obtenerListaReporteGestionEconomicaExcelDash(usuarioId, rolId, false, false);
            Sheet sheet6 = workbook.createSheet("8 - Gestion Economica - IMP");
            Object[] titulosHoja6 = {"id_bolsa","sitio","numero_contrato","proveedor","id_pmo","lp","gestor","codigo_sitio","pep2","id_ot","id_acta","aprobador","codigo_catalogo","valor_unitario","cantidad_pagar","valor_pagar","valor_total","moneda","centro", "grupo_articulo", "tipo_imputacion","codigo_aprobador","cuenta_mayor","contrato","numero_material","fecha_ingreso","estado","usuario_asignado","evento","motivo_rechazo","fecha_rechazo","fecha_pago","hem", "hem_adj","derivada","fecha_contable"};
            writeSheetOptimizedNoNull2(workbook, sheet6, titulosHoja6, reporteGEDash);

        }

        try {

            File archivoTemporal = new File("ArchivoReporteOTs.xlsx");
            FileOutputStream out = new FileOutputStream(archivoTemporal);
            workbook.write(out);
            out.close();
            ByteArrayOutputStream baos;
            baos = leerArchivo(archivoTemporal);
            retorno = baos.toByteArray();
            log.debug("ArchivoReporteOTs.xlsx written successfully on disk.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retorno;
    }

    public byte[] retornarArchivoGestionEconomicaEnExcel(int usuarioId, int rolId, String estado)throws Exception{
        byte[] retorno = null;
        List<Object[]> reporteGE = null;

        if(estado.compareTo("pagadas") == 0 || estado.compareTo("aprobadas") == 0){
            reporteGE = GenericDAO.getINSTANCE().obtenerListaReporteGestionEconomicaExcelPagadas(usuarioId, rolId);
        }else{
            reporteGE = GenericDAO.getINSTANCE().obtenerListaReporteGestionEconomicaExcel(usuarioId, estado, rolId);
        }

        XSSFWorkbook workbook = new XSSFWorkbook();
        String tituloSheet = "GestionEconomica - ";

        switch (estado){
            case "asignadas": tituloSheet = tituloSheet + "Asignadas"; break;
            case "aprobadas": tituloSheet = tituloSheet + "Pagadas"; break;
            case "rechazadas": tituloSheet = tituloSheet + "Rechazadas"; break;
            case "APRO": tituloSheet = tituloSheet + "Aprobadas"; break;
            case "PEND": tituloSheet = tituloSheet + "Rechazadas"; break;
            case "pagadas": tituloSheet = tituloSheet + "Pagadas"; break;
            default: tituloSheet = tituloSheet + "Ejecutar"; break;
        }

        XSSFSheet sheet1 = workbook.createSheet(tituloSheet);
        Object[] titulosHoja1 = null;

        if(rolId == 6 ){ //pagos
            if(estado.compareTo("aprobadas") == 0){
                titulosHoja1 = new Object[]{"id_bolsa","sitio","numero_contrato","proveedor","id_pmo","lp","gestor","codigo_sitio","pep2","id_ot", "id_acta","aprobador","usuario_pmo","fecha_validacion_pmo","usuario_imp","fecha_validacion_imp","usuario_pag","fecha_validacion_pag","codigo_catalogo","valor_unitario","cantidad_pagar","valor_pagar","valor_total","moneda","centro", "grupo_articulo", "tipo_imputacion","codigo_aprobador","cuenta_mayor","contrato","numero_material", "fecha_ingreso", "monto_pago", "fecha_pago", "hem", "hem_adj", "derivada", "fecha_contable"};
            }else{
                titulosHoja1 = new Object[]{"id_bolsa","sitio","numero_contrato","proveedor","id_pmo","lp","gestor","codigo_sitio","pep2","id_ot", "id_acta","aprobador","usuario_pmo","fecha_validacion_pmo","usuario_imp","fecha_validacion_imp","codigo_catalogo","valor_unitario","cantidad_pagar","valor_pagar","valor_total","moneda","centro", "grupo_articulo", "tipo_imputacion","codigo_aprobador","cuenta_mayor","contrato","numero_material","fecha_ingreso","usuario_asignado","motivo_rechazo"};
            }

        }else if (rolId == 8 ){ //PMO
            if(estado.compareTo("pagadas") == 0){
                titulosHoja1 = new Object[]{"id_bolsa","sitio","numero_contrato","proveedor","id_pmo","lp","gestor","codigo_sitio","pep2","id_ot", "id_acta","aprobador","usuario_pmo","fecha_validacion_pmo","usuario_imp","fecha_validacion_imp","usuario_pag","fecha_validacion_pag","codigo_catalogo","valor_unitario","cantidad_pagar","valor_pagar","valor_total","moneda","centro", "grupo_articulo", "tipo_imputacion","codigo_aprobador","cuenta_mayor","contrato","numero_material", "fecha_ingreso", "monto_pago", "fecha_pago", "hem", "hem_adj", "derivada", "fecha_contable"};
            }else{
                titulosHoja1 = new Object[]{"sitio","numero_contrato","proveedor","id_pmo","lp","gestor","codigo_sitio","pep2","id_ot","id_acta","aprobador","codigo_catalogo","valor_unitario","cantidad_pagar","valor_pagar","valor_total","moneda","centro", "grupo_articulo", "tipo_imputacion","codigo_aprobador","cuenta_mayor","contrato","numero_material","fecha_ingreso","usuario_asignado","motivo_rechazo"};
            }

        }else if (rolId == 9 ){ // IMP
            if(estado.compareTo("pagadas") == 0){
                titulosHoja1 = new Object[]{"id_bolsa","sitio","numero_contrato","proveedor","id_pmo","lp","gestor","codigo_sitio","pep2","id_ot", "id_acta","aprobador","usuario_pmo","fecha_validacion_pmo","usuario_imp","fecha_validacion_imp","usuario_pag","fecha_validacion_pag","codigo_catalogo","valor_unitario","cantidad_pagar","valor_pagar","valor_total","moneda","centro", "grupo_articulo", "tipo_imputacion","codigo_aprobador","cuenta_mayor","contrato","numero_material", "fecha_ingreso", "monto_pago", "fecha_pago", "hem", "hem_adj", "derivada", "fecha_contable"};
            }else{
                titulosHoja1 = new Object[]{"sitio","numero_contrato","proveedor","id_pmo","lp","gestor","codigo_sitio","pep2","id_ot","id_acta","aprobador","usuario_pmo","fecha_validacion_pmo","codigo_catalogo","valor_unitario","cantidad_pagar","valor_pagar","valor_total","moneda","centro", "grupo_articulo", "tipo_imputacion","codigo_aprobador","cuenta_mayor","contrato","numero_material","fecha_ingreso","usuario_asignado","motivo_rechazo"};
            }
        }

        writeSheetOptimizedNoNull(workbook, sheet1, titulosHoja1, reporteGE);

        try {

            File archivoTemporal = new File("ArchivoReporteGestionEconomica.xlsx");
            FileOutputStream out = new FileOutputStream(archivoTemporal);
            workbook.write(out);
            out.close();
            ByteArrayOutputStream baos;
            baos = leerArchivo(archivoTemporal);
            retorno = baos.toByteArray();
            //System.out.println("ArchivoReporteGestionEconomica.xlsx written successfully on disk.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retorno;
    }

    public void writeSheetOptimized(XSSFWorkbook workbook, XSSFSheet sheet, Object[] titulos, List<Object[]> data){

        Row rowTitle = sheet.createRow(0);

        int cellnumTitle = 0;

        for(Object obj : titulos){
            Cell cell = rowTitle.createCell(cellnumTitle++);
            cell.setCellValue((String) obj);
        }

        int rownum = 1;

        XSSFCreationHelper createHelper = workbook.getCreationHelper();
        XSSFCellStyle cellStyle         = workbook.createCellStyle();
        XSSFDataFormat xssfDataFormat = createHelper.createDataFormat();

        for (Object[] objList : data) {
            Row row = sheet.createRow(rownum++);
            int cellnum = 0;
            for (Object obj : objList) {
                Cell cell = row.createCell(cellnum++);
                String formato = "";

                if(obj instanceof String){
                    cell.setCellValue((String)obj);
                }else if(obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }else if(obj instanceof Date){
                    //cell.setCellValue((Date)obj);
                    //formato = "yyyy-MM-dd";
                    cell.setCellValue(""+obj);
                }else if(obj instanceof  Double){
                    cell.setCellValue((Double) obj);
                    formato = "#,##0.000";
                }else if(obj instanceof BigInteger){
                    cell.setCellValue(((BigInteger) obj).longValue());
                }
                cellStyle.setDataFormat(xssfDataFormat.getFormat(formato));
                cell.setCellStyle(cellStyle);

            }
        }
    }

    public void writeSheetOptimizedNoNull(XSSFWorkbook workbook, XSSFSheet sheet, Object[] titulos, List<Object[]> data){

        Row rowTitle = sheet.createRow(0);

        int cellnumTitle = 0;

        for(Object obj : titulos){
            Cell cell = rowTitle.createCell(cellnumTitle++);
            cell.setCellValue((String) obj);
        }

        int rownum = 1;

        XSSFCreationHelper createHelper = workbook.getCreationHelper();
        XSSFCellStyle cellStyle         = workbook.createCellStyle();
        XSSFDataFormat xssfDataFormat = createHelper.createDataFormat();

        for (Object[] objList : data) {
            Row row = sheet.createRow(rownum++);
            int cellnum = 0;
            for (Object obj : objList) {
                if(obj != null){
                    Cell cell = row.createCell(cellnum++);
                    String formato = "";

                    if(obj instanceof String){
                        cell.setCellValue((String)obj);
                    }else if(obj instanceof Integer) {
                        cell.setCellValue((Integer) obj);
                    }else if(obj instanceof Date){
                        //cell.setCellValue((Date)obj);
                        //formato = "dd-MM-yyyy";
                        cell.setCellValue(""+obj);
                    }else if(obj instanceof  Double){
                        cell.setCellValue((Double) obj);
                        formato = "#,##0.000";
                    }else if(obj instanceof BigInteger){
                        cell.setCellValue(((BigInteger) obj).longValue());
                    }
                    cellStyle.setDataFormat(xssfDataFormat.getFormat(formato));
                    cell.setCellStyle(cellStyle);
                }
            }
        }
    }

    public void writeSheetOptimizedNoNull2(SXSSFWorkbook workbook, Sheet sheet, Object[] titulos, List<Object[]> data){

        Row rowTitle = sheet.createRow(0);

        int cellnumTitle = 0;

        for(Object obj : titulos){
            Cell cell = rowTitle.createCell(cellnumTitle++);
            cell.setCellValue((String) obj);
        }

        int rownum = 1;

        CreationHelper createHelper = workbook.getCreationHelper();
        CellStyle cellStyle         = workbook.createCellStyle();
        DataFormat xssfDataFormat = createHelper.createDataFormat();

        for (Object[] objList : data) {
            Row row = sheet.createRow(rownum++);
            int cellnum = 0;
            for (Object obj : objList) {
                if(obj != null){
                    Cell cell = row.createCell(cellnum++);
                    String formato = "";

                    if(obj instanceof String){
                        cell.setCellValue((String)obj);
                    }else if(obj instanceof Integer) {
                        cell.setCellValue((Integer) obj);
                    }else if(obj instanceof Date){
                        //cell.setCellValue((Date)obj);
                        //formato = "dd-MM-yyyy";
                        cell.setCellValue(""+obj);
                    }else if(obj instanceof  Double){
                        cell.setCellValue((Double) obj);
                        formato = "#,##0.000";
                    }else if(obj instanceof BigInteger){
                        cell.setCellValue(((BigInteger) obj).longValue());
                    }
                    cellStyle.setDataFormat(xssfDataFormat.getFormat(formato));
                    cell.setCellStyle(cellStyle);
                }
            }
        }
    }

    public void writeSheet(XSSFWorkbook workbook, XSSFSheet sheet, Object[] titulos, List<Object[]> data){
        Map<String, Object[]> datosHoja = new TreeMap<>();

        datosHoja.put("1", titulos);

        Integer contador = 2;
        for(Object[] obj : data){
            //System.out.println(obj.getClass().toString());
            int ultimoItemObjeto = obj.length - 1;
            datosHoja.put((contador++).toString(), obj);
        }

        Set<String> keyset1 = datosHoja.keySet();
        int rownum = 0;
        for (String key : keyset1) {
            Row row = sheet.createRow(rownum++);
            Object [] objArr = datosHoja.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                XSSFCreationHelper createHelper = workbook.getCreationHelper();
                XSSFCellStyle cellStyle         = workbook.createCellStyle();
                XSSFDataFormat xssfDataFormat = createHelper.createDataFormat();
                String formato = "";

                if(obj instanceof String){
                    cell.setCellValue((String)obj);
                }else if(obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }else if(obj instanceof Date){
                    cell.setCellValue((Date)obj);
                    formato = "dd-MM-yyyy";
                }else if(obj instanceof  Double){
                    cell.setCellValue((Double) obj);
                    formato = "#,##0.000";
                }
                cellStyle.setDataFormat(xssfDataFormat.getFormat(formato));
                cell.setCellStyle(cellStyle);

            }
        }
    }

    public void writeSheetOptimizedGeneric(SXSSFWorkbook workbook, Sheet sheet, String titulos, List<Object[]> data){

        CreationHelper createHelper = workbook.getCreationHelper();
        CellStyle cellStyleFecha         = workbook.createCellStyle();
        CellStyle cellStyleDouble         = workbook.createCellStyle();
        DataFormat xssfDataFormat = workbook.createDataFormat();

        String formatoFecha = "dd-mm-yyyy";
        cellStyleFecha.setDataFormat(xssfDataFormat.getFormat(formatoFecha));

        String formatoDouble = "#,##0;-#,##0";
        //formato = "$#.##0,00;-$#.##0,00";
        cellStyleDouble.setDataFormat(xssfDataFormat.getFormat(formatoDouble));

        Row rowTitle = sheet.createRow(0);
        int cellnumTitle = 0;

        String[] listaTitulos = titulos.split(";");

        for(String obj : listaTitulos){
            Cell cell = rowTitle.createCell(cellnumTitle++);
            cell.setCellValue(obj);
        }

        int rownum = 1;
        String formato = "";

        for (Object[] objList : data) {
            Row row = sheet.createRow(rownum++);
            int cellnum = 0;
            for (Object obj : objList) {
                Cell cell = row.createCell(cellnum++);

                if(obj instanceof String){
                    cell.setCellValue((String)obj);
                }else if(obj instanceof Date){
                    cell.setCellValue((Date)obj);
                    cell.setCellStyle(cellStyleFecha);
                }else if(obj instanceof Double){
                    cell.setCellValue((Double)obj);
                    cell.setCellStyle(cellStyleDouble);
                }else if(obj instanceof Integer){
                    cell.setCellValue((Integer)obj);
                }else {
                    if(!(""+obj).trim().equalsIgnoreCase("null")) cell.setCellValue(""+obj);
                }

            }
        }
    }

    private ByteArrayOutputStream leerArchivo(File archivo) throws IOException, FileNotFoundException {
        ByteArrayOutputStream baos;
        InputStream is = new FileInputStream(archivo);
        try (BufferedInputStream bis = new BufferedInputStream(is)) {
            baos = new ByteArrayOutputStream();
            int size = 2048;
            byte[] buffer = new byte[size];
            int readCount;
            while ((readCount = bis.read(buffer)) > 0) {
                baos.write(buffer, 0, readCount);
            }
        }
        return baos;
    }

    public List retornarResumenOtsAPagar(int usuarioId)throws Exception{
        GatewayAcciones gate = new GatewayAcciones();
        try{
            gate.abrirSesion();
            return gate.retornarResumenOtsAPagar(usuarioId);
        }finally{
            gate.cerrarSesion();
        }
    }

    private static final Logger log = Logger.getLogger(Facade.class);
    
}
