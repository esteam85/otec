/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.pojos.MesCantidadAux;
import org.apache.log4j.Logger;
import java.io.*;
import java.util.*;

/**
 *
 * @author Christian Espinoza
 */
public class FacadeDashboard {

    public FacadeDashboard(){

    }

    public List retornarApCerradasVsApEnCurso(int idUsuario)throws Exception{
        GatewayDashboard gatewayDashboard = new GatewayDashboard();
        try{
            gatewayDashboard.abrirSesion();
            return gatewayDashboard.retornarApCerradasVsApEnCurso(idUsuario);
        }finally{
            gatewayDashboard.cerrarSesion();
        }
    }

    public List<MesCantidadAux> retornarApFechaInicioRealVsFechaInicio(int idUsuario)throws Exception{
        List<MesCantidadAux> listado = new ArrayList<>();
        GatewayDashboard gatewayDashboard = new GatewayDashboard();
        try{
            gatewayDashboard.abrirSesion();
            listado = gatewayDashboard.retornarApFechaInicioRealVsFechaInicio(idUsuario);
        }finally{
            gatewayDashboard.cerrarSesion();
        }
        return listado;
    }

    private static final Logger log = Logger.getLogger(FacadeDashboard.class);
    
}
