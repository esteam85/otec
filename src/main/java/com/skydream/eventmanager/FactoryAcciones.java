/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

/**
 *
 * @author mcj
 */
public class FactoryAcciones {
    
    public Command getAccionBBDD(String nombreObjeto) throws Exception {
        Command accion = null;

        switch (nombreObjeto) {
            case "acc-5":
                accion = new AccionRegistrarOT();
                break;
            case "acc-6":
                accion = new AccionIFWF();
                break;
            case "acc-7":
                accion = new AccionRegistrarNotificacion();
                break;
            case "acc-10":
                accion = new AccionActualizarWEE();
                break;
            case "acc-12":
                accion = new AccionContinuarFlujoWEE();
                break;
            case "acc-13":
                accion = new AccionContinuarFlujoWEE();
                break;
            case "acc-14":
                accion = new AccionGenerarActa_SBE_RAN();
                break;
            case "acc-16":
                accion = new AccionValidarActa();
                break;
            case "acc-18":
                accion = new AccionAutorizarPago();
                break;
            case "acc-20":
                accion = new AccionIngresarPago();
                break;
            case "acc-21":
                accion = new AccionAsignarCoordinador();
                break;
            case "acc-22":
                accion = new AccionValidacionPMO();
                break;
            case "acc-23":
                accion = new AccionValidacionImputacion();
                break;
            case "acc-24":
                accion = new AccionAdjuntarCarta();
                break;
            case "acc-25":
                accion = new AccionGenerarActaOrdinaria();
                break;
            case "acc-26":
                accion = new AccionValidarActaOrdinaria();
                break;
            case "acc-27":
                accion = new AccionRegistrarOTRan();
                break;
            case "acc-28":
                accion = new AccionRegistrarOTUni();
                break;
            case "acc-29":
                accion = new AccionRegistrarOTLLave();
                break;
            case "acc-30":
                accion = new AccionGenerarActaUnificado();
                break;
            case "acc-31":
                accion = new AccionValidarActaUnificado();
                break;
            case "acc-34":
                accion = new AccionRegistrarOTConstruccionUni();
                break;
            case "acc-35":
                accion = new AccionRegistrarOTSalas();
                break;
            case "acc-36":
                accion = new AccionRegistrarOTSupervision();
                break;
            case "acc-37":
                accion = new AccionRegistrarOTSatelital();
                break;
            case "acc-38":
                accion = new AccionValidarOT();
                break;
            case "acc-39":
                accion = new AccionValidarSolicitudDeTrabajo();
                break;
            case "acc-40":
                accion = new AccionCambioProveedor();
                break;
            case "acc-41":
                accion = new AccionValidarActaLlave();
                break;
            case "acc-42":
                accion = new AccionValidarActaLlaveConstruccion();
                break;
            case "acc-43":
                accion = new AccionCrearOTConstruccionLLave();
                break;
            case "acc-44":
                accion = new AccionGenerarActaLLave();
                break;
            case "acc-45":
                accion = new AccionGenerarPdf(1);
                break;
            case "acc-46":
                accion = new AccionGenerarPdf(2);
                break;
            case "acc-47":
                accion = new AccionValidarActa();
                break;
            case "acc-48":
                accion = new AccionCrearOTConstruccionLLave();
                break;
            case "acc-49":
                accion = new AccionIngresaCheckListCalidad();
                break;
            case "acc-50":
                accion = new AccionRegistrarOTBucle();
                break;
            case "acc-51":
                accion = new AccionGenerarActaBucle();
                break;
            case "acc-52":
                accion = new AccionValidarActaBucle();
                break;
            case "acc-54":
                accion = new AccionRechazarCheckList();
                break;
            case "acc-55":
                accion = new AccionIngresaITOaObra();
                break;
            case "acc-56":
                accion = new AccionCerraOtYFinalizarFlujo();
                break;
            case "acc-57":
                accion = new AccionCrearOTConstruccionSBE();
                break;
            case "acc-58":
                accion = new AccionIngresaServiciosDisenoAp();
                break;
            case "acc-60":
                accion = new AccionRegistrarOTIndoor();
                break;
            case "acc-61":
                accion = new AccionRegistrarOTCarros();
                break;
            default:
                throw new Exception("Accion no encontrada en Factory");
        }

        return accion;
    }
    
}
