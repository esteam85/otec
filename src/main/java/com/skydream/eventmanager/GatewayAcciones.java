/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.dao.Gateway;
import com.skydream.coreot.pojos.*;
import com.skydream.coreot.util.NewHibernateUtil;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.*;
import org.hibernate.InstantiationException;
import org.hibernate.criterion.*;

/**
 *
 * @author mcj
 */
public class GatewayAcciones {

    protected List retornarResumenOtsAPagar(int usuarioId)throws Exception{
        List retorno = null;
        try{
            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * FROM listar_ots1(:usuarioId)");
            sqlQuery.setParameter("usuarioId",usuarioId);
            retorno = sqlQuery.list();
        }catch(Exception e){
            log.error("Error al obtener data desde Procedimiento Almacenado.",e);
        }
        return retorno;
    }
    
    protected Serializable registrarConId(ObjetoCoreOT obj) throws Exception {
        Serializable idRegistro = 0;
        try {
            idRegistro = sesion.save(obj);
            sesion.flush();
        } catch (HibernateException e) {
            log.error("No se pudo crear el objeto " + obj.getClass().toString());
            throw e;
        }
        return idRegistro;
    }
    protected boolean ingresarLocalizacionAServiciosCubicados(int idServicio, int idAgencia, int idCentral) throws Exception {
        boolean respuesta = false;
        CubicadorDetalle cubicadorDetalle = new CubicadorDetalle();
        try {
            Criteria criteria = sesion.createCriteria(CubicadorDetalle.class);
            criteria.add(Restrictions.eq("id", idServicio));
            cubicadorDetalle = (CubicadorDetalle) criteria.uniqueResult();

            cubicadorDetalle.setCentralId(idCentral);
            cubicadorDetalle.setAgenciaId(idAgencia);

            sesion.update(cubicadorDetalle);
            sesion.flush();
            respuesta = true;

        } catch (HibernateException e) {
            log.error("****** No se pudo actualizar cubicador_detalle, error:  " + e.toString());
            throw e;
        }
        return respuesta;
    }

    protected void persistirObjeto(ObjetoCoreOT obj) throws Exception {
        try {
            sesion.persist(obj);
            sesion.flush();
        } catch (HibernateException e) {
            log.error("No se pudo crear el objeto " + obj.getClass().toString());
            throw e;
        }
    }
    
    protected ObjetoCoreOT retornarObjetoCoreOT(Class claseObjetoCoreOT, int id) throws Exception {
        try {
            ObjetoCoreOT objetoRetorno = (ObjetoCoreOT) sesion.get(claseObjetoCoreOT, id);
            return objetoRetorno;
        } catch (Exception e) {
            log.error("No se pudo cargar el objeto " + claseObjetoCoreOT.toString());
            throw e;
        }
    }
    
    protected List<ObjetoCoreOT> listarConFiltro(Class claseObjeto, List<String> joinObjCoreOT, String filtro, int valorFiltro, String orden) throws Exception {
        List<ObjetoCoreOT> lista = new ArrayList();
        try {
            Criteria criteria = sesion.createCriteria(claseObjeto);
            if (joinObjCoreOT != null) {
                for (String join : joinObjCoreOT) {
                    criteria.setFetchMode(join, FetchMode.JOIN);
                }
            }
            if (orden!=null) {
                criteria.addOrder(Order.asc(orden));
            }
            criteria.add(Restrictions.eq(filtro, valorFiltro));
            lista = criteria.list();
        } catch (HibernateException e) {
            throw e;
        }
        return lista;
    }

    protected List<WorkflowEventos> listarConFiltro33(int id) throws Exception {
        List<WorkflowEventos> lista = new ArrayList();
        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventos.class);
            criteria.addOrder(Order.asc("orden"));
            criteria.add(Restrictions.eq("workflow.id", id));
            lista = criteria.list();
        } catch (HibernateException e) {
            throw e;
        }
        return lista;
    }

    protected Integer obtenerSumaRegistros(Class claseObjeto, Map<String,String> filtros, String nombreCampoSuma){
        Integer resultado;
        try {
            Criteria criteria = sesion.createCriteria(claseObjeto);
            criteria.setProjection(Projections.sum(nombreCampoSuma));
            for(Map.Entry<String,String> filtro : filtros.entrySet()){
                String nombreFiltro = filtro.getKey();
                String valorFiltro = filtro.getValue();
                criteria.add(Restrictions.eq(nombreFiltro, valorFiltro));
            }
            resultado = (Integer)criteria.uniqueResult();
        } catch (HibernateException e) {
            throw e;
        }
        return resultado;
    }


    protected boolean actualizarCubicacion(int otId, int cubicacionId) throws Exception {

        boolean actualizo = false;
        List<Cubicador> cubicaciones = new ArrayList<>();
        try {
            Criteria criteria = sesion.createCriteria(Cubicador.class);
            criteria.add(Restrictions.eq("id", cubicacionId));
            cubicaciones = criteria.list();
            Cubicador cubicacion = cubicaciones.get(0);
            Ot ot = new Ot();
            ot.setId(otId);
            cubicacion.setOt(ot);
            sesion.update(cubicacion);
            actualizo = true;

        } catch (HibernateException e) {
            throw e;
        }
        return actualizo;
    }

    protected boolean actualizarCubicacionServicios(int cubicacionId) throws Exception {

        boolean actualizo = false;
        List<CubicadorServicios> cubicaciones = new ArrayList<>();
        try {

            Criteria criteria = sesion.createCriteria(CubicadorServicios.class);
            criteria.add(Restrictions.eq("cubicador.id", cubicacionId));
            cubicaciones = criteria.list();
            CubicadorServicios cubicacion = cubicaciones.get(0);
            cubicacion.setCantidad(0);
            sesion.update(cubicacion);
            actualizo = true;

        } catch (HibernateException e) {
            throw e;
        }
        return actualizo;
    }
    protected boolean actualizarCubicacionyActaServiciosCrearOtAp(int cubicacionId, int actaId) throws Exception {

        boolean actualizo = false;
        List<CubicadorServicios> cubicaciones = new ArrayList<>();
        try {

            Criteria criteria = sesion.createCriteria(CubicadorServicios.class);
            criteria.add(Restrictions.eq("cubicador.id", cubicacionId));
            cubicaciones = criteria.list();
            for(CubicadorServicios cubSer : cubicaciones){
                CubicadorServicios cubicacion = cubSer;
                cubicacion.setCantidad(0);
                sesion.update(cubicacion);
            }
            Actas actas = new Actas();
            Criteria criteria2 = sesion.createCriteria(Actas.class);
            criteria2.add(Restrictions.eq("id", actaId));
            actas = (Actas) criteria2.uniqueResult();
            actas.setMontoTotalActa(0.0);
            actas.setMontoTotalActaAdicional(0.0);
            actas.setMontoTotalActaCubicado(0.0);
            actas.setMontoHistorico(0.0);
            actas.setMontoTotalAPagar(0.0);
            actas.setValidacionUsuarios(false);
            actas.setPagoAprobado(false);

            sesion.update(actas);

            actualizo = true;

        } catch (HibernateException e) {
            throw e;
        }
        return actualizo;
    }

    protected boolean cerratOt(int otId) throws Exception {

        boolean actualizo = false;
        List<Ot> ots = new ArrayList<>();
        try {

            Criteria criteria = sesion.createCriteria(Ot.class);
            criteria.add(Restrictions.eq("id", otId));
            ots = criteria.list();
            Ot ot = ots.get(0);
            TipoEstadoOt tipoEstadoOt = new TipoEstadoOt();
            tipoEstadoOt.setId(2);
            ot.setTipoEstadoOt(tipoEstadoOt);
            sesion.update(ot);
            actualizo = true;

        } catch (HibernateException e) {
            throw e;
        }
        return actualizo;
    }


    protected void actualizarWEE(int otId) throws Exception {
        List<ObjetoCoreOT> listadoWEE = listarConFiltro(WorkflowEventosEjecucion.class, null, "ot.id", otId, "ejecutado");
        //Se llena un mapa con el listadoWEE obtenido para lograr un acceso mas rapido por idEvento(el cual sera la key del map)
        Map<Integer, WorkflowEventosEjecucion> mapaWEE = new HashMap();
        int idEjecucionEventoAnterior = 0;
        WorkflowEventosEjecucion weeActual = null;
        for(ObjetoCoreOT itemWEE : listadoWEE){
            WorkflowEventosEjecucion wee = (WorkflowEventosEjecucion)itemWEE;
            mapaWEE.put(wee.getId().getEventoId(),wee);
            if(wee.getEjecutado()>0 && wee.getEjecutado()!=9999){
                idEjecucionEventoAnterior = wee.getEjecutado();
            }else if(wee.getEjecutado()==9999){
                weeActual = wee;
                break;
            }
        }

        AtomicInteger idEjecucion = new AtomicInteger();
        idEjecucion.set(idEjecucionEventoAnterior);
        idEjecucion.incrementAndGet();

        weeActual.setEjecutado(idEjecucion.intValue());
        Date fechaActual = new Date();
        weeActual.setFechaInicioReal(fechaActual);
        weeActual.setFechaTerminoReal(fechaActual);

        WorkflowEventosEjecucion weeProximo = mapaWEE.get(weeActual.getProximo());
        weeProximo.setEjecutado(9999);

        WorkflowEventosEjecucionHistoricoId weeHistoricoId = new WorkflowEventosEjecucionHistoricoId();
        weeHistoricoId.setCubicadorId(weeActual.getId().getCubicadorId());
        weeHistoricoId.setDecisionId(weeActual.getId().getDecisionId());
        weeHistoricoId.setEventoId(weeActual.getId().getEventoId());
        weeHistoricoId.setRolId(weeActual.getId().getRolId());
        weeHistoricoId.setWorkflowId(weeActual.getId().getWorkflowId());
        weeHistoricoId.setEjecutado(weeActual.getEjecutado());

        WorkflowEventosEjecucionHistorico weeHistorico = new WorkflowEventosEjecucionHistorico();
        weeHistorico.setId(weeHistoricoId);
        weeHistorico.setOt(weeActual.getOt());
        weeHistorico.setEventoPadre(weeActual.getEventoPadre());
        weeHistorico.setFechaCreacion(weeActual.getFechaCreacion());
        weeHistorico.setFechaInicioEstimada(weeActual.getFechaInicioEstimada());
        weeHistorico.setFechaInicioReal(weeActual.getFechaInicioReal());
        weeHistorico.setFechaTerminoEstimada(weeActual.getFechaTerminoEstimada());
        weeHistorico.setFechaTerminoReal(weeActual.getFechaTerminoReal());
        weeHistorico.setProveedorId(weeActual.getProveedorId());
        weeHistorico.setProximo(weeActual.getProximo());
        weeHistorico.setUsuarioEjecutorId(weeActual.getUsuarioEjecutorId());
        weeHistorico.setUsuarioId(weeActual.getUsuarioId());

        try {
            sesion.save(weeHistorico);
        } catch (HibernateException hibex) {
            hibex.printStackTrace();
            throw hibex;
        }

    }


    protected boolean actualizarWorkfloEventoEjecucionCierreOt(int otId, int eventoId, int usuarioEjecutor) throws Exception {
        List<WorkflowEventosEjecucion> lista = new ArrayList();
        List<WorkflowEventosEjecucionHistorico> listaWeeH = new ArrayList();
        boolean retorno = false;
        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            lista = criteria.list();
            Cubicador cubicador = new Cubicador();
            cubicador = obtieneCubicadorPorIdOt(otId);

            int contador = 0;
            Date fechaInicioReal = new Date();
            WorkflowEventosEjecucionHistoricoId workflowEventosEjecucionHistoricoId = new WorkflowEventosEjecucionHistoricoId();
            WorkflowEventosEjecucionHistorico workflowEventosEjecucionHistorico = new WorkflowEventosEjecucionHistorico();
            // Obtengo el valor de ejecutado
            for(WorkflowEventosEjecucion wee : lista){
                if(wee.getProximo() == eventoId){
                    contador = wee.getEjecutado();
                    fechaInicioReal = wee.getFechaTerminoReal();
                }
            }
            int contadorWeeH = 0;
            // Acualizo el registro

            for(WorkflowEventosEjecucion wee : lista){
                if(wee.getId().getEventoId() == eventoId){
                    wee.setFechaTerminoReal(new Date());
                    contador = contador + 1;
                    wee.setEjecutado(contador);
                    sesion.update(wee);


                    Criteria criteriaWeeH = sesion.createCriteria(WorkflowEventosEjecucionHistorico.class);
                    criteriaWeeH.add(Restrictions.eq("ot.id", otId));
                    listaWeeH = (List<WorkflowEventosEjecucionHistorico>)criteriaWeeH.list();

                    for(WorkflowEventosEjecucionHistorico weeH : listaWeeH){
                        if(contador < weeH.getId().getEjecutado()){
                            contador = weeH.getId().getEjecutado() + 1;
                        }
                    }
                    workflowEventosEjecucionHistoricoId.setEventoId(eventoId);
                    workflowEventosEjecucionHistoricoId.setCubicadorId(cubicador.getId());
                    workflowEventosEjecucionHistoricoId.setDecisionId(0);
                    workflowEventosEjecucionHistoricoId.setRolId(wee.getId().getRolId());
                    workflowEventosEjecucionHistoricoId.setWorkflowId(wee.getId().getWorkflowId());
                    workflowEventosEjecucionHistoricoId.setEjecutado(contador);

                    workflowEventosEjecucionHistorico.setId(workflowEventosEjecucionHistoricoId);
                    workflowEventosEjecucionHistorico.setFechaCreacion(wee.getFechaCreacion());
                    workflowEventosEjecucionHistorico.setFechaInicioEstimada(wee.getFechaInicioEstimada());
                    workflowEventosEjecucionHistorico.setFechaInicioReal(fechaInicioReal);
                    workflowEventosEjecucionHistorico.setFechaTerminoEstimada(wee.getFechaTerminoEstimada());
                    Date fechaReal = new Date();
                    workflowEventosEjecucionHistorico.setFechaTerminoReal(fechaReal);
                    workflowEventosEjecucionHistorico.setProveedorId(wee.getOt().getProveedor().getId());
                    workflowEventosEjecucionHistorico.setUsuarioEjecutorId(usuarioEjecutor);
                    workflowEventosEjecucionHistorico.setUsuarioId(wee.getUsuarioId());
                    workflowEventosEjecucionHistorico.setOt(wee.getOt());
                    workflowEventosEjecucionHistorico.setProximo(0);
                    workflowEventosEjecucionHistorico.setEventoPadre(0);

                    sesion.save(workflowEventosEjecucionHistorico);
                }
            }

            // Actualizo el proximo registro
            for(WorkflowEventosEjecucion wee : lista){
                if(wee.getId().getEventoId() == 100){
                    wee.setEjecutado(contador+1);
                    sesion.update(wee);
                }
            }

            retorno = true;

        } catch (HibernateException e) {
            throw e;
        }
        return retorno;
    }

    protected boolean actualizarWorkfloEventoEjecucionCierre(int otId, int eventoId) throws Exception {
        List<WorkflowEventosEjecucion> lista = new ArrayList();
        boolean respuesta = false;
        int idEventoProximo = 0;
        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            lista = criteria.list();

            int contador = 0;
            switch (lista.get(0).getId().getWorkflowId()) {
                case 5:
                // Obtengo el valor de ejecutado
                    for (WorkflowEventosEjecucion wee : lista) {
                        if (wee.getProximo() == eventoId) {
                            contador = wee.getEjecutado();
                        }
                    }

                    // Acualizo el registro
                    for (WorkflowEventosEjecucion wee : lista) {
                        if (wee.getId().getEventoId() == eventoId) {
                            wee.setFechaTerminoReal(new Date());
                            contador = contador + 1;
                            wee.setEjecutado(contador);
                            idEventoProximo = wee.getProximo();
                            sesion.update(wee);
                        }
                    }
                    break;
                case 2:
                    int eventoPadre = 0;
                    contador = 1;
                    for (WorkflowEventosEjecucion wee : lista) {
                        if (wee.getId().getEventoId() == eventoId) {
                            eventoPadre = wee.getEventoPadre();
                        }
                    }
                    for (WorkflowEventosEjecucion wee : lista) {
                        if (wee.getId().getEventoId() == eventoPadre) {
                            contador = contador + wee.getEjecutado();
                        }
                    }
                    // Acualizo el registro
                    for (WorkflowEventosEjecucion wee : lista) {
                        if (wee.getId().getEventoId() == eventoId) {
                            wee.setFechaTerminoReal(new Date());
                            contador = contador + 1;
                            wee.setEjecutado(contador);
                            idEventoProximo = wee.getProximo();
                            sesion.update(wee);
                        }
                    }
                    for (WorkflowEventosEjecucion wee : lista) {
                        if (wee.getId().getEventoId() == idEventoProximo) {
                            wee.setFechaTerminoReal(new Date());
                            wee.setEjecutado(9999);
                            sesion.update(wee);
                        }
                    }
                    break;
            }


            respuesta = true;

        } catch (HibernateException e) {
            throw e;
        }
        return respuesta;
    }


    protected boolean commpletarDataOt(int idOtAntigua, int idOtNueva, int cubicadorId, int gestorAntiguo, int gestorConstruccionId, int idProveedor) throws Exception {

        boolean respuesta = false;
        List<UsuariosOt> usuariosOt = new ArrayList<>();
        List<Usuarios> listJefeArea = new ArrayList<>();
        List<Usuarios> listSubGerente = new ArrayList<>();
        List<UsuariosValidadores> usuariosValidadores = new ArrayList<>();
        List<WorkflowEventosEjecucion> workflowEventosEjecucion = new ArrayList<>();

        try {

            Criteria criteria = sesion.createCriteria(UsuariosOt.class);
            criteria.add(Restrictions.eq("ot.id", idOtAntigua));
            usuariosOt = criteria.list();

            Criteria criteriaOrganigramaAntiguo = sesion.createCriteria(Organigrama.class);
            criteriaOrganigramaAntiguo.add(Restrictions.eq("id.gestorId",gestorAntiguo));
            criteriaOrganigramaAntiguo.add(Restrictions.eq("id.contratoId",5));

            Organigrama organigramaAntiguo = (Organigrama) criteriaOrganigramaAntiguo.uniqueResult();

            int idJefeAreaAntiguo = organigramaAntiguo.getId().getJefeDeAreaId();
            int idSubGerenteAntiguo = organigramaAntiguo.getId().getSubGerenteId();

            Criteria criteriaOrganigramaNuevo = sesion.createCriteria(Organigrama.class);
            criteriaOrganigramaNuevo.add(Restrictions.eq("id.gestorId",gestorConstruccionId));
            criteriaOrganigramaNuevo.add(Restrictions.eq("id.contratoId",5));

            Organigrama organigramaNuevo = (Organigrama) criteriaOrganigramaNuevo.uniqueResult();

            int idJefeAreaNuevo = organigramaNuevo.getId().getJefeDeAreaId();
            int idSubGerenteNuevo = organigramaNuevo.getId().getSubGerenteId();



//            Criteria jefeArea = sesion.createCriteria(Usuarios.class);
//            jefeArea.add(Restrictions.eq("nombreUsuario", "jorgelg"));
//            listJefeArea = jefeArea.list();
//            int idJefeArea = listJefeArea.get(0).getId();
//
//            Criteria subGerente = sesion.createCriteria(Usuarios.class);
//            subGerente.add(Restrictions.eq("nombreUsuario", "jorgevg"));
//            listSubGerente = subGerente.list();
//            int idSubGerente = listSubGerente.get(0).getId();


            for(UsuariosOt usuariosOtAntiguos: usuariosOt){

                Ot otNueva = new Ot();
                otNueva.setId(idOtNueva);
                UsuariosOtId usuariosOtId = new UsuariosOtId();
                usuariosOtId.setOtId(idOtNueva);
//                usuariosOtId.setUsuarioId(usuariosOtAntiguos.getId().getUsuarioId());
                usuariosOtId.setEventoId(usuariosOtAntiguos.getId().getEventoId());
                usuariosOtId.setRolId(usuariosOtAntiguos.getId().getRolId());
                UsuariosOt usuariosOtNuevos = new UsuariosOt();
                if(usuariosOtAntiguos.getId().getUsuarioId() == gestorAntiguo){
                    usuariosOtId.setUsuarioId(gestorConstruccionId);
                }else if(usuariosOtAntiguos.getId().getUsuarioId() == idJefeAreaAntiguo){
                    usuariosOtId.setUsuarioId(idJefeAreaNuevo);
                }else if(usuariosOtAntiguos.getId().getUsuarioId() == idSubGerenteAntiguo){
                    usuariosOtId.setUsuarioId(idSubGerenteNuevo);
                }else{
                    usuariosOtId.setUsuarioId(usuariosOtAntiguos.getId().getUsuarioId());
                }

                usuariosOtNuevos.setId(usuariosOtId);
                usuariosOtNuevos.setOt(otNueva);
                usuariosOtNuevos.setRoles(usuariosOtAntiguos.getRoles());
                usuariosOtNuevos.setParticipacion(usuariosOtAntiguos.getParticipacion());
                usuariosOtNuevos.setGestorTelefonicaId(gestorConstruccionId);
                usuariosOtNuevos.setContratoId(usuariosOtAntiguos.getContratoId());
                usuariosOtNuevos.setEventos(usuariosOtAntiguos.getEventos());
                usuariosOtNuevos.setUsuarios(usuariosOtAntiguos.getUsuarios());
                if(usuariosOtAntiguos.getEventos().getId() == 2  || usuariosOtAntiguos.getEventos().getId() == 53){
                    // no se almacena ya que esto se hace en asignar coordinador
                }else{
                    sesion.save(usuariosOtNuevos);
                }

            }

//            Criteria criteria2 = sesion.createCriteria(UsuariosValidadores.class);
//            criteria2.add(Restrictions.eq("ot.id", idOtAntigua));
//            usuariosValidadores = criteria2.list();
//
//            for(UsuariosValidadores usuariosValidadoresAntiguos: usuariosValidadores){
//                Ot otNueva = new Ot();
//                otNueva.setId(idOtNueva);
//                UsuariosValidadores usuariosValidadoresNuevos = new UsuariosValidadores();
//                usuariosValidadoresNuevos.setConceptos(usuariosValidadoresAntiguos.getConceptos());
//                usuariosValidadoresNuevos.setOt(otNueva);
//                usuariosValidadoresNuevos.setEventos(usuariosValidadoresAntiguos.getEventos());
//                Usuarios user = new Usuarios();
//                usuariosValidadoresNuevos.setUsuarios(usuariosValidadoresAntiguos.getUsuarios());
//                usuariosValidadoresNuevos.setOrden(usuariosValidadoresAntiguos.getOrden());
//
//                sesion.save(usuariosValidadoresNuevos);
//
//            }

            // inserto a mano los usuarios validadores ya que son diferentes de los originales

            Ot otNueva = new Ot();
            otNueva.setId(idOtNueva);

            UsuariosValidadores usuariosValidadoresNuevos = new UsuariosValidadores();
            Conceptos concepto2 = new Conceptos();
            concepto2.setId(2);
            usuariosValidadoresNuevos.setConceptos(concepto2);
            usuariosValidadoresNuevos.setOt(otNueva);
            Eventos evento6 = new Eventos();
            evento6.setId(6);
            usuariosValidadoresNuevos.setEventos(evento6);
            Usuarios henry = new Usuarios();
            henry.setId(idJefeAreaNuevo);
            usuariosValidadoresNuevos.setUsuarios(henry);
            usuariosValidadoresNuevos.setOrden(1);

            sesion.save(usuariosValidadoresNuevos);

            UsuariosValidadores usuariosValidadoresNuevos2 = new UsuariosValidadores();
            usuariosValidadoresNuevos2.setConceptos(concepto2);
            usuariosValidadoresNuevos2.setOt(otNueva);
            usuariosValidadoresNuevos2.setEventos(evento6);
            Usuarios cancino = new Usuarios();
            cancino.setId(idSubGerenteNuevo);
            usuariosValidadoresNuevos2.setUsuarios(cancino);
            usuariosValidadoresNuevos2.setOrden(2);

            sesion.save(usuariosValidadoresNuevos2);

            UsuariosValidadores usuariosValidadoresNuevos3 = new UsuariosValidadores();
            Conceptos concepto3 = new Conceptos();
            concepto3.setId(3);
            usuariosValidadoresNuevos3.setConceptos(concepto3);
            usuariosValidadoresNuevos3.setOt(otNueva);
            Eventos evento16 = new Eventos();
            evento16.setId(16);
            usuariosValidadoresNuevos3.setEventos(evento16);
            Usuarios user = new Usuarios();
            user.setId(48);
            usuariosValidadoresNuevos3.setUsuarios(user);
            usuariosValidadoresNuevos3.setOrden(1);

            sesion.save(usuariosValidadoresNuevos3);

            UsuariosValidadores usuariosValidadoresNuevos4 = new UsuariosValidadores();
            Conceptos concepto4 = new Conceptos();
            concepto4.setId(4);
            usuariosValidadoresNuevos4.setConceptos(concepto4);
            usuariosValidadoresNuevos4.setOt(otNueva);
            Eventos evento17 = new Eventos();
            evento17.setId(17);
            usuariosValidadoresNuevos4.setEventos(evento17);
            Usuarios user55 = new Usuarios();
            user55.setId(55);
            usuariosValidadoresNuevos4.setUsuarios(user55);
            usuariosValidadoresNuevos4.setOrden(1);

            sesion.save(usuariosValidadoresNuevos4);

            UsuariosValidadores usuariosValidadoresNuevos5 = new UsuariosValidadores();
            Conceptos concepto9 = new Conceptos();
            concepto9.setId(9);
            usuariosValidadoresNuevos5.setConceptos(concepto9);
            usuariosValidadoresNuevos5.setOt(otNueva);
            Eventos evento31 = new Eventos();
            evento31.setId(31);
            usuariosValidadoresNuevos5.setEventos(evento31);
            usuariosValidadoresNuevos5.setUsuarios(henry);
            usuariosValidadoresNuevos5.setOrden(1);

            sesion.save(usuariosValidadoresNuevos5);

            UsuariosValidadores usuariosValidadoresNuevos6 = new UsuariosValidadores();
            usuariosValidadoresNuevos6.setConceptos(concepto9);
            usuariosValidadoresNuevos6.setOt(otNueva);
            usuariosValidadoresNuevos6.setEventos(evento31);
            usuariosValidadoresNuevos6.setUsuarios(cancino);
            usuariosValidadoresNuevos6.setOrden(2);

            sesion.save(usuariosValidadoresNuevos6);

            UsuariosValidadores usuariosValidadoresNuevos7 = new UsuariosValidadores();
            Conceptos concepto11 = new Conceptos();
            concepto11.setId(10);
            usuariosValidadoresNuevos7.setConceptos(concepto11);
            usuariosValidadoresNuevos7.setOt(otNueva);
            Eventos evento52 = new Eventos();
            evento52.setId(52);
            usuariosValidadoresNuevos7.setEventos(evento52);
            usuariosValidadoresNuevos7.setUsuarios(henry);
            usuariosValidadoresNuevos7.setOrden(1);

            sesion.save(usuariosValidadoresNuevos7);

            // tengo que obtener el o los usuarios administradores de contrato de segun la empresa
            List<Usuarios> listadoUsuario = obtenerUsuariosPorRolProovedorGatewayAcciones(3,idProveedor,5);
            for(Usuarios admin : listadoUsuario){
                // este evento es el Solicitud de trabajo
                UsuariosValidadores usuariosValidadoresNuevos8 = new UsuariosValidadores();
                Conceptos concepto12 = new Conceptos();
                concepto12.setId(8);
                usuariosValidadoresNuevos8.setConceptos(concepto12);
                usuariosValidadoresNuevos8.setOt(otNueva);
                Eventos evento53 = new Eventos();
                evento53.setId(45);
                usuariosValidadoresNuevos8.setEventos(evento53);
                usuariosValidadoresNuevos8.setUsuarios(admin);
                usuariosValidadoresNuevos8.setOrden(1);
                sesion.save(usuariosValidadoresNuevos8);
            }

            Criteria criteria3 = sesion.createCriteria(WorkflowEventosEjecucion.class);
            criteria3.add(Restrictions.eq("ot.id", idOtAntigua));
            workflowEventosEjecucion = criteria3.list();

            for(WorkflowEventosEjecucion workflowEventosEjecucionAntiguos: workflowEventosEjecucion){

                WorkflowEventosEjecucionId workflowEventosEjecucionId = workflowEventosEjecucionAntiguos.getId();
                workflowEventosEjecucionId.setCubicadorId(cubicadorId);

                WorkflowEventosEjecucion workflowEventosEjecucionNuevos = new WorkflowEventosEjecucion();
                workflowEventosEjecucionNuevos.setId(workflowEventosEjecucionId);


                if(workflowEventosEjecucionAntiguos.getId().getEventoId() == 50){
                    workflowEventosEjecucionNuevos.setEjecutado(1);
                }else if(workflowEventosEjecucionAntiguos.getId().getEventoId() == 45){
                    workflowEventosEjecucionNuevos.setEjecutado(9999);
                }else {
                    workflowEventosEjecucionNuevos.setEjecutado(0);
                }

                workflowEventosEjecucionNuevos.setOt(otNueva);

                if(workflowEventosEjecucionAntiguos.getUsuarioId() == gestorAntiguo){
                    workflowEventosEjecucionNuevos.setUsuarioId(gestorConstruccionId);
                }else if (workflowEventosEjecucionAntiguos.getUsuarioId() == idJefeAreaAntiguo){
                    workflowEventosEjecucionNuevos.setUsuarioId(idJefeAreaNuevo);
                }else if (workflowEventosEjecucionAntiguos.getUsuarioId() == idSubGerenteAntiguo){
                    workflowEventosEjecucionNuevos.setUsuarioId(idSubGerenteNuevo);
                }else {
                    workflowEventosEjecucionNuevos.setUsuarioId(workflowEventosEjecucionAntiguos.getUsuarioId());
                }

                workflowEventosEjecucionNuevos.setEventoPadre(workflowEventosEjecucionAntiguos.getEventoPadre());
                workflowEventosEjecucionNuevos.setFechaCreacion(workflowEventosEjecucionAntiguos.getFechaCreacion());
                workflowEventosEjecucionNuevos.setFechaInicioEstimada(workflowEventosEjecucionAntiguos.getFechaInicioEstimada());
                workflowEventosEjecucionNuevos.setFechaInicioReal(workflowEventosEjecucionAntiguos.getFechaInicioReal());
                workflowEventosEjecucionNuevos.setFechaTerminoEstimada(workflowEventosEjecucionAntiguos.getFechaTerminoEstimada());
                workflowEventosEjecucionNuevos.setFechaTerminoReal(workflowEventosEjecucionAntiguos.getFechaTerminoReal());
                workflowEventosEjecucionNuevos.setProveedorId(workflowEventosEjecucionAntiguos.getProveedorId());
                workflowEventosEjecucionNuevos.setProximo(workflowEventosEjecucionAntiguos.getProximo());
                if(workflowEventosEjecucionAntiguos.getId().getEventoId() == 6 || workflowEventosEjecucionAntiguos.getId().getEventoId() == 13 || workflowEventosEjecucionAntiguos.getId().getEventoId() == 14){
                    Organigrama organigrama = new Organigrama();
                    Criteria criteriaOrga = sesion.createCriteria(Organigrama.class);
                    criteriaOrga.add(Restrictions.eq("id.gestorId",gestorConstruccionId));
                    criteriaOrga.add(Restrictions.eq("id.contratoId",5));
                    organigrama = (Organigrama) criteriaOrga.uniqueResult();
                    int jefeArea = organigrama.getId().getJefeDeAreaId();
                    workflowEventosEjecucionNuevos.setUsuarioId(jefeArea);
                }




                sesion.save(workflowEventosEjecucionNuevos);

            }

            respuesta = true;
        } catch (Exception e) {
            System.out.println("No se pudo obtener listado de usuarios." + e);
            throw e;
        }

        return respuesta;
    }

    protected boolean crearNuevoFlujoOtConstruccion(Ot otAntigua, int idOtNueva, int cubicadorId, int gestorAntiguo, int gestorConstruccionId, int idProveedor) throws Exception {

        boolean respuesta = false;
        List<UsuariosOt> usuariosOt = new ArrayList<>();
        List<Usuarios> listJefeArea = new ArrayList<>();
        List<Usuarios> listSubGerente = new ArrayList<>();
        List<UsuariosValidadores> usuariosValidadores = new ArrayList<>();
        List<WorkflowEventosEjecucion> weeAntiguo = new ArrayList<>();

        try {

            Criteria criteria = sesion.createCriteria(UsuariosOt.class);
            criteria.add(Restrictions.eq("ot.id", otAntigua.getId()));
            usuariosOt = criteria.list();

            Criteria jefeArea = sesion.createCriteria(Usuarios.class);
            jefeArea.add(Restrictions.eq("nombreUsuario", "jorgelg"));
            listJefeArea = jefeArea.list();
            int idJefeArea = listJefeArea.get(0).getId();
            int count = 0;
            for(UsuariosOt usuariosOtAntiguos: usuariosOt){

                Ot otNueva = new Ot();
                otNueva.setId(idOtNueva);
                UsuariosOtId usuariosOtId = new UsuariosOtId();
                usuariosOtId.setOtId(idOtNueva);
                usuariosOtId.setUsuarioId(usuariosOtAntiguos.getId().getUsuarioId());
                usuariosOtId.setEventoId(usuariosOtAntiguos.getId().getEventoId());
                usuariosOtId.setRolId(usuariosOtAntiguos.getId().getRolId());
                UsuariosOt usuariosOtNuevos = new UsuariosOt();


                usuariosOtNuevos.setOt(otNueva);
                usuariosOtNuevos.setRoles(usuariosOtAntiguos.getRoles());
                usuariosOtNuevos.setParticipacion(usuariosOtAntiguos.getParticipacion());
                usuariosOtNuevos.setGestorTelefonicaId(gestorConstruccionId);
                usuariosOtNuevos.setContratoId(usuariosOtAntiguos.getContratoId());
                usuariosOtNuevos.setEventos(usuariosOtAntiguos.getEventos());
                usuariosOtNuevos.setUsuarios(usuariosOtAntiguos.getUsuarios());
                if(usuariosOtAntiguos.getEventos().getId() == 2  || usuariosOtAntiguos.getEventos().getId() == 3){
                    // no se almacena ya que esto se hace en asignar coordinador
                    usuariosOtId.setUsuarioId(gestorConstruccionId);
                    usuariosOtNuevos.setId(usuariosOtId);
                    sesion.save(usuariosOtNuevos);
                }else{
                    usuariosOtNuevos.setId(usuariosOtId);
                    sesion.save(usuariosOtNuevos);
                }

            }
            Ot otNueva = new Ot();
            otNueva.setId(idOtNueva);
            otNueva.setIdAp(otAntigua.getId());
//            Selecciono los usuarios validadores de la AP
            List<UsuariosValidadores> usuariosValidadoresAntiguos = new ArrayList<UsuariosValidadores>();


            Criteria criteriaValidadores = sesion.createCriteria(UsuariosValidadores.class);
            criteriaValidadores.add(Restrictions.eq("ot.id", otAntigua.getId()));
            usuariosValidadoresAntiguos = (List<UsuariosValidadores>)criteriaValidadores.list();

//            Recorro los antiguos usuarios validadores para asignarlos a la nueva ot
            List<UsuariosValidadores> usuariosValidadoresNuevos = new ArrayList<UsuariosValidadores>();
            for(UsuariosValidadores userValidOld : usuariosValidadoresAntiguos){
                userValidOld.setOt(otNueva);
                usuariosValidadoresNuevos.add(userValidOld);
            }
            for(UsuariosValidadores userValidNew : usuariosValidadoresNuevos){
                sesion.save(userValidNew);
            }

            // tengo que obtener el o los usuarios administradores de contrato de segun la empresa
            List<Usuarios> listadoUsuario = obtenerUsuariosPorRolProovedorGatewayAcciones(3,idProveedor, otAntigua.getContrato().getId());


            Criteria criteria3 = sesion.createCriteria(WorkflowEventosEjecucion.class);
            criteria3.add(Restrictions.eq("ot.id", otAntigua.getId()));
            weeAntiguo = criteria3.list();

            for(WorkflowEventosEjecucion weeAntiguosRecor: weeAntiguo){
                WorkflowEventosEjecucion workflowEventosEjecucionNuevos = new WorkflowEventosEjecucion();
                WorkflowEventosEjecucionId workflowEventosEjecucionId = new WorkflowEventosEjecucionId();
//                seteo los mismos datos del worflow gestorAntiguo

                workflowEventosEjecucionId.setWorkflowId(weeAntiguosRecor.getId().getWorkflowId());
                workflowEventosEjecucionId.setDecisionId(weeAntiguosRecor.getId().getDecisionId());
                workflowEventosEjecucionId.setRolId(weeAntiguosRecor.getId().getRolId());
                workflowEventosEjecucionId.setEventoId(weeAntiguosRecor.getId().getEventoId());
                workflowEventosEjecucionId.setCubicadorId(cubicadorId);



                workflowEventosEjecucionNuevos.setEventoPadre(weeAntiguosRecor.getEventoPadre());
                workflowEventosEjecucionNuevos.setFechaCreacion(weeAntiguosRecor.getFechaCreacion());
                workflowEventosEjecucionNuevos.setFechaInicioEstimada(weeAntiguosRecor.getFechaInicioEstimada());
                workflowEventosEjecucionNuevos.setFechaTerminoEstimada(weeAntiguosRecor.getFechaTerminoEstimada());
                workflowEventosEjecucionNuevos.setFechaTerminoReal(weeAntiguosRecor.getFechaTerminoReal());
                workflowEventosEjecucionNuevos.setOt(otNueva);
                workflowEventosEjecucionNuevos.setProximo(weeAntiguosRecor.getProximo());
                workflowEventosEjecucionNuevos.setUsuarioEjecutorId(0);
                workflowEventosEjecucionNuevos.setUsuarioId(weeAntiguosRecor.getUsuarioId());
                workflowEventosEjecucionNuevos.setId(workflowEventosEjecucionId);


                if(weeAntiguosRecor.getId().getEventoId() == 30){
                    workflowEventosEjecucionNuevos.setEjecutado(1);
                }else if(weeAntiguosRecor.getId().getEventoId() == 45){
                    workflowEventosEjecucionNuevos.setEjecutado(9999);
                }else {
                    workflowEventosEjecucionNuevos.setEjecutado(0);
                }

                sesion.save(workflowEventosEjecucionNuevos);
                sesion.flush();

            }

            respuesta = true;
        } catch (Exception e) {
            System.out.println("No se pudo obtener listado de usuarios." + e);
            throw e;
        }

        return respuesta;
    }

    protected int actualizarWorkfloEventoEjecucion(int otId, int eventoId, int usuarioId) throws Exception {
        List<WorkflowEventosEjecucion> lista = new ArrayList();
        int retorno = 0;
        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            lista = criteria.list();

            int idEventoProximo = 0;
            // Acualizo el registro
            for(WorkflowEventosEjecucion wee : lista){
                if(wee.getId().getEventoId() == eventoId){
                    wee.setUsuarioEjecutorId(usuarioId);
                    idEventoProximo = wee.getProximo();
                    sesion.update(wee);

                    break;
                }
            }

            // Acualizo el proximo registro
            for(WorkflowEventosEjecucion wee : lista){
                int eventoActual = wee.getId().getEventoId();
                if(eventoActual == idEventoProximo){
                    wee.setUsuarioId(usuarioId);
                    sesion.update(wee);
                    break;
                }
            }
            if(eventoId == 2){
                int eventoCalidad = 0;
                for(WorkflowEventosEjecucion wee : lista){
                    int eventoActual = wee.getId().getEventoId();
                    if(wee.getId().getEventoId() == 57){
                        eventoCalidad = wee.getId().getEventoId();
                        wee.setUsuarioId(usuarioId);
                        sesion.update(wee);
                        break;
                    }
                }
                SQLQuery sqlQuery = sesion.createSQLQuery("UPDATE usuarios_ot set usuario_id = :newUser WHERE  evento_id = :eventoId and ot_id = :otId");
                sqlQuery.setParameter("newUser", usuarioId);
                sqlQuery.setParameter("eventoId", eventoCalidad);
                sqlQuery.setParameter("otId", otId);
                sqlQuery.executeUpdate();
                sesion.flush();
            }



            retorno = 1;

        } catch (HibernateException e) {
            throw e;
        }
        return retorno;
    }

    protected List<UsuariosValidadores> obtenerUsuariosValidadoresd(int otId, int eventoId) throws Exception {
        List<UsuariosValidadores> lista = new ArrayList();

        try {
            Criteria criteria = sesion.createCriteria(UsuariosValidadores.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            criteria.add(Restrictions.eq("eventos.id", eventoId));
            lista = criteria.list();

        } catch (HibernateException e) {
            throw e;
        }
        return lista;
    }


    protected UsuariosValidadores obtenerUsuarioValidador(int otId, int eventoId, int usuarioId) throws Exception {
        UsuariosValidadores usuario = new UsuariosValidadores();

        try {
            Criteria criteria = sesion.createCriteria(UsuariosValidadores.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            criteria.add(Restrictions.eq("usuarios.id", usuarioId));
            criteria.add(Restrictions.eq("eventos.id", eventoId));
            usuario = (UsuariosValidadores)criteria.uniqueResult();

        } catch (HibernateException e) {
            throw e;
        }
        return usuario;
    }


    protected boolean registrarUsuarioValidador(int otId, int eventoId, int usuarioId, boolean validacionUsuario, String comentario) throws Exception {
        List<UsuariosValidadores> lista = new ArrayList();
        boolean actualizo = false;
        try {
            Criteria criteria = sesion.createCriteria(UsuariosValidadores.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            criteria.add(Restrictions.eq("eventos.id", eventoId));
            criteria.add(Restrictions.eq("usuarios.id", usuarioId));
            lista = criteria.list();
            UsuariosValidadores usuariosValidadores = lista.get(0);

            ValidacionesOtec validacion = new ValidacionesOtec();
            validacion.setUsuariosValidadores(usuariosValidadores);
            validacion.setComentario(comentario);
            validacion.setFecha(new Date());
            validacion.setValidacion(validacionUsuario);
            sesion.save(validacion);
            actualizo = true;
        } catch (HibernateException e) {
            throw e;
        }
        return actualizo;
    }

    protected boolean registrarValidacionUsuario(ValidacionesOtec validacionOtec) throws Exception {
        List<UsuariosValidadores> lista = new ArrayList();
        boolean registro = false;
        try {
            sesion.save(validacionOtec);
            registro = true;
        } catch (HibernateException e) {
            throw e;
        }
        return registro;
    }

    protected boolean registrarGestionEconomica(GestionEconomica_old gestionEconomica) throws Exception {
        List<UsuariosValidadores> lista = new ArrayList();
        boolean registro = false;
        try {
            sesion.save(gestionEconomica);
            registro = true;
        } catch (HibernateException e) {
            throw e;
        }
        return registro;
    }

    protected boolean actualizarGestionEconomica(GestionEconomica_old gestionEconomica) throws Exception {
        List<UsuariosValidadores> lista = new ArrayList();
        boolean registro = false;
        try {
            sesion.update(gestionEconomica);
            registro = true;
        } catch (HibernateException e) {
            throw e;
        }
        return registro;
    }

    protected GestionEconomica_old obtenerGestionEconomica(int idActa) throws Exception {
        GestionEconomica_old gestionEconomica = new GestionEconomica_old();
        boolean registro = false;
        try {
            Criteria criteria = sesion.createCriteria(GestionEconomica_old.class);
            criteria.add(Restrictions.eq("actas.id", idActa));
            gestionEconomica = (GestionEconomica_old)criteria.uniqueResult();
            registro = true;
        } catch (HibernateException e) {
            throw e;
        }
        return gestionEconomica;
    }

    protected boolean registrarAutorizacion(int otId, int eventoId, int usuarioId, String comentario, boolean validacionUsuario) throws Exception {
        List<UsuariosValidadores> lista = new ArrayList();
        boolean actualizo = false;
        try {
            Criteria criteria = sesion.createCriteria(UsuariosValidadores.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            criteria.add(Restrictions.eq("eventos.id", eventoId));
            criteria.add(Restrictions.eq("usuarios.id", usuarioId));
            lista = criteria.list();
            UsuariosValidadores usuariosValidadores = lista.get(0);

            ValidacionesOtec validacion = new ValidacionesOtec();
            validacion.setUsuariosValidadores(usuariosValidadores);
            validacion.setComentario(comentario);
            validacion.setFecha(new Date());
            validacion.setValidacion(validacionUsuario);
            sesion.save(validacion);
            actualizo = true;
        } catch (HibernateException e) {
            throw e;
        }
        return actualizo;
    }
    protected boolean actualizarUsuarioWorkflowEventoEjecucion(int otId, int eventoId, int usuarioIdActual, int usuarioIdProximo) throws Exception {
        List<WorkflowEventosEjecucion> lista = new ArrayList();
        boolean actualizo = false;
        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            criteria.add(Restrictions.eq("usuarioId", usuarioIdActual));
            criteria.add(Restrictions.eq("id.eventoId", eventoId));
            lista = criteria.list();
            WorkflowEventosEjecucion wee = lista.get(0);
            wee.setUsuarioId(usuarioIdProximo);
            sesion.update(wee);
            actualizo = true;

        } catch (HibernateException e) {
            throw e;
        }
        return actualizo;
    }

    protected boolean actualizarValidacionActaSbe(int idActa, boolean validacionUsuario, boolean pagoTotal, String observaciones, int otId, int porcentagePago) throws Exception {
        List<Actas> lista = new ArrayList();
        boolean actualizo = false;
        double montoPendiente = 0;
        double montoAPagar = 0;
        try {
            Actas actaValidadas = new Actas();
            actaValidadas = obtenerActaValida(otId);

            Criteria criteria = sesion.createCriteria(Actas.class);
            criteria.add(Restrictions.eq("id", idActa));
            lista = criteria.list();
            Actas acta = lista.get(0);
            acta.setEsTotal(pagoTotal);
            acta.setValidacionUsuarios(validacionUsuario);

            if(actaValidadas != null ){
                montoPendiente = actaValidadas.getSaldoPendiente();
            } else if(!pagoTotal){
                montoAPagar = (porcentagePago * acta.getMontoTotalActa())/100;
                montoPendiente = acta.getMontoTotalActa() - montoAPagar;
            }

            if((pagoTotal) && (actaValidadas == null)) {
                acta.setSaldoPendiente(montoPendiente);
                acta.setMontoTotalAPagar(acta.getMontoTotalActa());
            } else if((pagoTotal) && (actaValidadas != null)) {
                double totalPago = Math.abs(acta.getMontoTotalActa() - actaValidadas.getMontoTotalActa()) + actaValidadas.getSaldoPendiente();
                acta.setSaldoPendiente(0.0);
                acta.setMontoTotalAPagar(totalPago);
            } else if((!pagoTotal) && (actaValidadas != null)) {
                double totalPago = Math.abs(acta.getMontoTotalActa() - actaValidadas.getMontoTotalActa()) + actaValidadas.getSaldoPendiente();
                acta.setSaldoPendiente(((100 - porcentagePago) * totalPago)/100);
                acta.setMontoTotalAPagar((porcentagePago * totalPago)/100);
            } else if(!pagoTotal){
                acta.setSaldoPendiente(montoPendiente);
                acta.setMontoTotalAPagar(montoAPagar);
            }

            String obsOld = acta.getObservaciones();
            obsOld = obsOld.replace("]",",");
            String observacionNueva = obsOld + observaciones + "]";

            acta.setObservaciones(observacionNueva);


            sesion.update(acta);
            sesion.flush();
            actualizo = true;

        } catch (HibernateException e) {
            throw e;
        }
        return actualizo;
    }
    protected boolean actualizarValidacionActa(int idActa, boolean validacionUsuario, boolean pagoTotal, String observaciones) throws Exception {
        List<Actas> lista = new ArrayList();
        boolean actualizo = false;
        try {
            Criteria criteria = sesion.createCriteria(Actas.class);
            criteria.add(Restrictions.eq("id", idActa));
            lista = criteria.list();
            Actas acta = lista.get(0);
            acta.setEsTotal(pagoTotal);
            acta.setValidacionUsuarios(validacionUsuario);

            String obsOld = acta.getObservaciones();
            obsOld = obsOld.replace("]",",");
            String observacionNueva = obsOld + observaciones + "]";

            acta.setObservaciones(observacionNueva);


            sesion.update(acta);
            actualizo = true;

        } catch (HibernateException e) {
            throw e;
        }
        return actualizo;
    }

    protected boolean clonarServiciosActaParcial(Actas actaParcial, Actas actaAnterior) throws Exception{

        boolean guardado = false;
        List <DetalleServiciosActas> serviciosClonados = this.obtieneDetalleServiciosEjecutados(actaAnterior.getId());
        List <DetalleServiciosAdicionalesActas> serviciosAdicionalesClonados = this.obtieneDetalleServiciosAdicionalesEjecutados(actaAnterior.getId());

        try {

            for(DetalleServiciosActas servicioClonado :serviciosClonados){

                DetalleServiciosActas nuevoServicioClonado = new DetalleServiciosActas();

                nuevoServicioClonado.setActas(actaParcial);
                nuevoServicioClonado.setTotalHistoricoActa(servicioClonado.getTotalHistoricoActa());
                nuevoServicioClonado.setTotalUnidadesEjecucionOt(servicioClonado.getTotalUnidadesEjecucionOt());
                nuevoServicioClonado.setTotalUnidadesEjecucionActa(servicioClonado.getTotalUnidadesEjecucionActa());
                nuevoServicioClonado.setServicio(servicioClonado.getServicio());
                nuevoServicioClonado.setTotalUnidadesCubicadas(servicioClonado.getTotalUnidadesCubicadas());
                nuevoServicioClonado.setTotalMontoCubicado(servicioClonado.getTotalMontoCubicado());
                nuevoServicioClonado.setValidacionSistema(servicioClonado.getValidacionSistema());
                nuevoServicioClonado.setTotalMontoEjecucionActa(servicioClonado.getTotalMontoEjecucionActa());

                DetalleServiciosActasId idActa = new DetalleServiciosActasId();
                idActa.setServicioId(servicioClonado.getId().getServicioId());
                idActa.setActaId(actaParcial.getId());
                nuevoServicioClonado.setId(idActa);

                sesion.save(nuevoServicioClonado);
            }

            for(DetalleServiciosAdicionalesActas servicioAdicionalClonado :serviciosAdicionalesClonados){

                DetalleServiciosAdicionalesActas nuevoServicioAdicionalClonado = new DetalleServiciosAdicionalesActas();

                nuevoServicioAdicionalClonado.setActas(actaParcial);
                nuevoServicioAdicionalClonado.setTotalHistoricoActa(servicioAdicionalClonado.getTotalHistoricoActa());
                nuevoServicioAdicionalClonado.setTotalUnidadesEjecucionOt(servicioAdicionalClonado.getTotalUnidadesEjecucionOt());
                nuevoServicioAdicionalClonado.setTotalUnidadesEjecucionActa(servicioAdicionalClonado.getTotalUnidadesEjecucionActa());
                nuevoServicioAdicionalClonado.setServicio(servicioAdicionalClonado.getServicio());
                nuevoServicioAdicionalClonado.setTotalUnidadesCubicadas(servicioAdicionalClonado.getTotalUnidadesCubicadas());
                nuevoServicioAdicionalClonado.setTotalMontoCubicado(servicioAdicionalClonado.getTotalMontoCubicado());
                nuevoServicioAdicionalClonado.setValidacionUsuarios(servicioAdicionalClonado.getValidacionUsuarios());
                nuevoServicioAdicionalClonado.setTotalMontoEjecucionActa(servicioAdicionalClonado.getTotalMontoEjecucionActa());

                DetalleServiciosAdicionalesActasId idActa = new DetalleServiciosAdicionalesActasId();
                idActa.setServicioId(servicioAdicionalClonado.getId().getServicioId());
                idActa.setActaId(actaParcial.getId());
                nuevoServicioAdicionalClonado.setId(idActa);

                sesion.save(nuevoServicioAdicionalClonado);
            }

            guardado = true;

        } catch (HibernateException e) {
            throw e;
        }

        return  guardado;

    }

    protected boolean actualizarValidacionActaLLaveDiseño(int idActa, String observaciones) throws Exception {
        List<Actas> lista = new ArrayList();
        boolean actualizo = false;
        try {
            Criteria criteria = sesion.createCriteria(Actas.class);
            criteria.add(Restrictions.eq("id", idActa));
            lista = criteria.list();
            Actas acta = lista.get(0);
            acta.setEsTotal(false);
            acta.setValidacionUsuarios(true);
//
//            String obsOld = acta.getObservaciones();
//            obsOld = obsOld.replace("}",",");
//            String observacionNueva = obsOld + observaciones + "]";

//            acta.setObservaciones(observacionNueva);


            sesion.update(acta);
            actualizo = true;

        } catch (HibernateException e) {
            throw e;
        }
        return actualizo;
    }

    protected List<WorkflowEventosEjecucion> obtenerEventoEjecucion(int otId) throws Exception {
        List<WorkflowEventosEjecucion> lista = new ArrayList();

        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            lista = criteria.list();

        } catch (HibernateException e) {
            log.error("=================== Error al listar Objeto WorkFlowEventoEjecucion ===============");
            log.error("Error: "+e.toString());
            throw e;
        }
        return lista;
    }

    protected boolean registrarValidacionUsuarioActaMateriaAdicional(int idActa, int materialId, boolean validacion) {
        try {

            List<DetalleMaterialesAdicionalesActas> listado = (List<DetalleMaterialesAdicionalesActas>) sesion.createCriteria(DetalleMaterialesAdicionalesActas.class)
                    .add(Restrictions.eq("actas.id", idActa))
                    .add(Restrictions.eq("materiales.id", materialId))
                    .list();
            DetalleMaterialesAdicionalesActas dma = new DetalleMaterialesAdicionalesActas();
            dma = listado.get(0);
            dma.setValidacionUsuarios(validacion);
            sesion.update(dma);

            return true;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones. ", ex);
            throw ex;
        }
    }

    protected boolean registrarValidacionUsuarioActaServicioAdicional(int idActa, int servicioId, boolean validacion) {
        try {

            List<DetalleServiciosAdicionalesActas> listado = (List<DetalleServiciosAdicionalesActas>) sesion.createCriteria(DetalleServiciosAdicionalesActas.class)
                    .add(Restrictions.eq("actas.id", idActa))
                    .add(Restrictions.eq("servicio.id", servicioId))
                    .list();
            DetalleServiciosAdicionalesActas dma = new DetalleServiciosAdicionalesActas();
            dma = listado.get(0);
            dma.setValidacionUsuarios(validacion);
            sesion.update(dma);

            return true;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones. ", ex);
            throw ex;
        }
    }

    protected int continuarFlujoWorkfloEventoEjecucionDecision(int otId, int eventoId, boolean reiniciarFechas) throws Exception {
        List<WorkflowEventosEjecucion> lista = new ArrayList();
        int retorno = 0;
        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            lista = criteria.list();

            int contador = 0;
            // Obtengo el valor de ejecutado
            int ejecutado = 0;
            for(WorkflowEventosEjecucion wee : lista){
                ejecutado = wee.getEjecutado();
                if(ejecutado > contador && ejecutado!=9999){
                    contador = ejecutado;
                }
            }
            int idEventoProximo = 0;
            int idEventoPadre = 0;

            for(WorkflowEventosEjecucion wee : lista){

                if(wee.getId().getEventoId() == eventoId){
                    Date fechaActual = new Date();
                    wee.setFechaInicioReal(fechaActual);
                    wee.setFechaTerminoReal(fechaActual);
                    contador = contador + 2;
                    wee.setEjecutado(contador);
                    idEventoProximo = wee.getProximo();
                    idEventoPadre = wee.getEventoPadre();
                    sesion.update(wee);
                    WorkflowEventosEjecucionHistoricoId weeHistoricoId = new WorkflowEventosEjecucionHistoricoId();
                    weeHistoricoId.setCubicadorId(wee.getId().getCubicadorId());
                    weeHistoricoId.setDecisionId(wee.getId().getDecisionId());
                    weeHistoricoId.setEventoId(wee.getId().getEventoId());
                    weeHistoricoId.setRolId(wee.getId().getRolId());
                    weeHistoricoId.setWorkflowId(wee.getId().getWorkflowId());
                    weeHistoricoId.setEjecutado(wee.getEjecutado());

                    WorkflowEventosEjecucionHistorico weeHistorico = new WorkflowEventosEjecucionHistorico();
                    weeHistorico.setId(weeHistoricoId);
                    weeHistorico.setOt(wee.getOt());
                    weeHistorico.setEventoPadre(wee.getEventoPadre());
                    weeHistorico.setFechaCreacion(wee.getFechaCreacion());
                    weeHistorico.setFechaInicioEstimada(wee.getFechaInicioEstimada());
                    weeHistorico.setFechaInicioReal(wee.getFechaInicioReal());
                    weeHistorico.setFechaTerminoEstimada(wee.getFechaTerminoEstimada());
                    weeHistorico.setFechaTerminoReal(wee.getFechaTerminoReal());
                    weeHistorico.setProveedorId(wee.getProveedorId());
                    weeHistorico.setProximo(wee.getProximo());
                    weeHistorico.setUsuarioEjecutorId(wee.getUsuarioEjecutorId());
                    weeHistorico.setUsuarioId(wee.getUsuarioId());

                    try {
                        sesion.save(weeHistorico);
                    } catch (HibernateException hibex) {
                        hibex.printStackTrace();
                        throw hibex;
                    }
                    break;
                }
            }
            for(WorkflowEventosEjecucion wee : lista){

                if(wee.getId().getEventoId() == idEventoPadre){
                    Date fechaActual = new Date();
                    wee.setFechaInicioReal(fechaActual);
                    wee.setFechaTerminoReal(fechaActual);
                    contador = contador - 1;
                    wee.setEjecutado(contador);
                    sesion.update(wee);
                    WorkflowEventosEjecucionHistoricoId weeHistoricoId = new WorkflowEventosEjecucionHistoricoId();
                    weeHistoricoId.setCubicadorId(wee.getId().getCubicadorId());
                    weeHistoricoId.setDecisionId(wee.getId().getDecisionId());
                    weeHistoricoId.setEventoId(wee.getId().getEventoId());
                    weeHistoricoId.setRolId(wee.getId().getRolId());
                    weeHistoricoId.setWorkflowId(wee.getId().getWorkflowId());
                    weeHistoricoId.setEjecutado(wee.getEjecutado());

                    WorkflowEventosEjecucionHistorico weeHistorico = new WorkflowEventosEjecucionHistorico();
                    weeHistorico.setId(weeHistoricoId);
                    weeHistorico.setOt(wee.getOt());
                    weeHistorico.setEventoPadre(wee.getEventoPadre());
                    weeHistorico.setFechaCreacion(wee.getFechaCreacion());
                    weeHistorico.setFechaInicioEstimada(wee.getFechaInicioEstimada());
                    weeHistorico.setFechaInicioReal(wee.getFechaInicioReal());
                    weeHistorico.setFechaTerminoEstimada(wee.getFechaTerminoEstimada());
                    weeHistorico.setFechaTerminoReal(wee.getFechaTerminoReal());
                    weeHistorico.setProveedorId(wee.getProveedorId());
                    weeHistorico.setProximo(wee.getProximo());
                    weeHistorico.setUsuarioEjecutorId(wee.getUsuarioEjecutorId());
                    weeHistorico.setUsuarioId(wee.getUsuarioId());

                    try {
                        sesion.save(weeHistorico);
                    } catch (HibernateException hibex) {
                        hibex.printStackTrace();
                        throw hibex;
                    }
                    break;
                }
            }
            // Acualizo el proximo registro
            for(WorkflowEventosEjecucion wee : lista){
                if(wee.getId().getEventoId() == idEventoProximo){
                    wee.setEjecutado(9999);
                    if(reiniciarFechas){
                        wee.setFechaInicioReal(null);
                        wee.setFechaTerminoReal(null);
                    }
                    sesion.update(wee);
                    break;
                }
            }

            retorno = 1;

        } catch (HibernateException e) {
            throw e;
        }
        return retorno;
    }

    protected int continuarFlujoWorkfloEventoEjecucionLlaveManual(int otId, int eventoActual, int eventoIdPadre, int eventoIdProximo, int gestorConstruccionId, boolean diseno) throws Exception {
        List<WorkflowEventosEjecucion> lista = new ArrayList();
        int retorno = 0;
        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            lista = criteria.list();

            int contador = 0;
            // Obtengo el valor de ejecutado
            int ejecutado = 0;
            for(WorkflowEventosEjecucion wee : lista){
                ejecutado = wee.getEjecutado();
                if(ejecutado > contador && ejecutado!=9999){
                    contador = ejecutado;
                }
            }
            int idEventoProximo = 0;
            int idEventoPadre = 0;

            for(WorkflowEventosEjecucion wee : lista){

                if(wee.getId().getEventoId() == eventoActual){
                    Date fechaActual = new Date();
                    wee.setFechaInicioReal(fechaActual);
                    wee.setFechaTerminoReal(fechaActual);
                    contador = contador + 1;
                    wee.setEjecutado(contador);
                    sesion.update(wee);
                    WorkflowEventosEjecucionHistoricoId weeHistoricoId = new WorkflowEventosEjecucionHistoricoId();
                    weeHistoricoId.setCubicadorId(wee.getId().getCubicadorId());
                    weeHistoricoId.setDecisionId(wee.getId().getDecisionId());
                    weeHistoricoId.setEventoId(wee.getId().getEventoId());
                    weeHistoricoId.setRolId(wee.getId().getRolId());
                    weeHistoricoId.setWorkflowId(wee.getId().getWorkflowId());
                    weeHistoricoId.setEjecutado(wee.getEjecutado());

                    WorkflowEventosEjecucionHistorico weeHistorico = new WorkflowEventosEjecucionHistorico();
                    weeHistorico.setId(weeHistoricoId);
                    weeHistorico.setOt(wee.getOt());
                    weeHistorico.setEventoPadre(wee.getEventoPadre());
                    weeHistorico.setFechaCreacion(wee.getFechaCreacion());
                    weeHistorico.setFechaInicioEstimada(wee.getFechaInicioEstimada());
                    weeHistorico.setFechaInicioReal(wee.getFechaInicioReal());
                    weeHistorico.setFechaTerminoEstimada(wee.getFechaTerminoEstimada());
                    weeHistorico.setFechaTerminoReal(wee.getFechaTerminoReal());
                    weeHistorico.setProveedorId(wee.getProveedorId());
                    weeHistorico.setProximo(wee.getProximo());
                    weeHistorico.setUsuarioEjecutorId(wee.getUsuarioEjecutorId());
                    weeHistorico.setUsuarioId(wee.getUsuarioId());

                    try {
                        sesion.save(weeHistorico);
                    } catch (HibernateException hibex) {
                        hibex.printStackTrace();
                        throw hibex;
                    }
                    break;
                }
            }


            for(WorkflowEventosEjecucion wee : lista){

                if(wee.getId().getEventoId() == eventoIdPadre){
                    Date fechaActual = new Date();
                    wee.setFechaInicioReal(fechaActual);
                    wee.setFechaTerminoReal(fechaActual);
                    contador = contador + 1;
                    wee.setEjecutado(contador);
                    sesion.update(wee);
                    WorkflowEventosEjecucionHistoricoId weeHistoricoId = new WorkflowEventosEjecucionHistoricoId();
                    weeHistoricoId.setCubicadorId(wee.getId().getCubicadorId());
                    weeHistoricoId.setDecisionId(wee.getId().getDecisionId());
                    weeHistoricoId.setEventoId(wee.getId().getEventoId());
                    weeHistoricoId.setRolId(wee.getId().getRolId());
                    weeHistoricoId.setWorkflowId(wee.getId().getWorkflowId());
                    weeHistoricoId.setEjecutado(wee.getEjecutado());

                    WorkflowEventosEjecucionHistorico weeHistorico = new WorkflowEventosEjecucionHistorico();
                    weeHistorico.setId(weeHistoricoId);
                    weeHistorico.setOt(wee.getOt());
                    weeHistorico.setEventoPadre(wee.getEventoPadre());
                    weeHistorico.setFechaCreacion(wee.getFechaCreacion());
                    weeHistorico.setFechaInicioEstimada(wee.getFechaInicioEstimada());
                    weeHistorico.setFechaInicioReal(wee.getFechaInicioReal());
                    weeHistorico.setFechaTerminoEstimada(wee.getFechaTerminoEstimada());
                    weeHistorico.setFechaTerminoReal(wee.getFechaTerminoReal());
                    weeHistorico.setProveedorId(wee.getProveedorId());
                    weeHistorico.setProximo(wee.getProximo());
                    weeHistorico.setUsuarioEjecutorId(wee.getUsuarioEjecutorId());
                    weeHistorico.setUsuarioId(wee.getUsuarioId());

                    try {
                        sesion.save(weeHistorico);
                    } catch (HibernateException hibex) {
                        hibex.printStackTrace();
                        throw hibex;
                    }
                    break;
                }
            }

            if(diseno){
                for(WorkflowEventosEjecucion wee : lista){
                    if(wee.getId().getEventoId() == eventoIdProximo){
                        wee.setEjecutado(9999);
                        wee.setUsuarioId(gestorConstruccionId);
                        sesion.update(wee);
                        UsuariosOt usuariosOt = new UsuariosOt();
                        UsuariosOtId usuariosOtId = new UsuariosOtId();
                        usuariosOtId.setRolId(2);
                        usuariosOtId.setOtId(otId);
                        usuariosOtId.setEventoId(53);
                        usuariosOtId.setUsuarioId(gestorConstruccionId);
                        usuariosOt.setId(usuariosOtId);
                        Ot otAux = new Ot();
                        otAux.setId(otId);
                        usuariosOt.setOt(otAux);
                        sesion.save(usuariosOt);
                        break;
                    }
                }
            }else{
                for(WorkflowEventosEjecucion wee : lista){
                    if(wee.getId().getEventoId() == eventoIdProximo){
                        wee.setEjecutado(9999);
                        sesion.update(wee);
                        break;
                    }
                }
            }



            retorno = 1;

        } catch (HibernateException e) {
            throw e;
        }
        return retorno;
    }


    protected int continuarFlujoWorkfloEventoEjecucionManualOtConstruccion(int otId, int eventoActual, int eventoIdPadre, int eventoIdProximo, int gestorConstruccionId, boolean diseno, int contratoId) throws Exception {
        List<WorkflowEventosEjecucion> lista = new ArrayList();
        int retorno = 0;
        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            lista = criteria.list();

            int contador = 0;
            // Obtengo el valor de ejecutado
            int ejecutado = 0;
            for(WorkflowEventosEjecucion wee : lista){
                ejecutado = wee.getEjecutado();
                if(ejecutado > contador && ejecutado!=9999){
                    contador = ejecutado;
                }
            }
            int idEventoProximo = 0;
            int idEventoPadre = 0;
            Ot otAux = new Ot();
            otAux.setId(otId);

            for(WorkflowEventosEjecucion wee : lista){

                if(wee.getId().getEventoId() == eventoActual){
                    Date fechaActual = new Date();
                    wee.setFechaInicioReal(fechaActual);
                    wee.setFechaTerminoReal(fechaActual);
                    contador = contador + 1;
                    wee.setEjecutado(contador);
                    sesion.update(wee);
                    WorkflowEventosEjecucionHistoricoId weeHistoricoId = new WorkflowEventosEjecucionHistoricoId();
                    weeHistoricoId.setCubicadorId(wee.getId().getCubicadorId());
                    weeHistoricoId.setDecisionId(wee.getId().getDecisionId());
                    weeHistoricoId.setEventoId(wee.getId().getEventoId());
                    weeHistoricoId.setRolId(wee.getId().getRolId());
                    weeHistoricoId.setWorkflowId(wee.getId().getWorkflowId());
                    weeHistoricoId.setEjecutado(wee.getEjecutado());

                    WorkflowEventosEjecucionHistorico weeHistorico = new WorkflowEventosEjecucionHistorico();
                    weeHistorico.setId(weeHistoricoId);
                    weeHistorico.setOt(wee.getOt());
                    weeHistorico.setEventoPadre(wee.getEventoPadre());
                    weeHistorico.setFechaCreacion(wee.getFechaCreacion());
                    weeHistorico.setFechaInicioEstimada(wee.getFechaInicioEstimada());
                    weeHistorico.setFechaInicioReal(wee.getFechaInicioReal());
                    weeHistorico.setFechaTerminoEstimada(wee.getFechaTerminoEstimada());
                    weeHistorico.setFechaTerminoReal(wee.getFechaTerminoReal());
                    weeHistorico.setProveedorId(wee.getProveedorId());
                    weeHistorico.setProximo(wee.getProximo());
                    weeHistorico.setUsuarioEjecutorId(wee.getUsuarioEjecutorId());
                    weeHistorico.setUsuarioId(wee.getUsuarioId());

                    try {
                        sesion.save(weeHistorico);
                    } catch (HibernateException hibex) {
                        hibex.printStackTrace();
                        throw hibex;
                    }
                    break;
                }
            }


            for(WorkflowEventosEjecucion wee : lista){

                if(wee.getId().getEventoId() == eventoIdPadre){
                    Date fechaActual = new Date();
                    wee.setFechaInicioReal(fechaActual);
                    wee.setFechaTerminoReal(fechaActual);
                    contador = contador + 1;
                    wee.setEjecutado(contador);
                    sesion.update(wee);
                    WorkflowEventosEjecucionHistoricoId weeHistoricoId = new WorkflowEventosEjecucionHistoricoId();
                    weeHistoricoId.setCubicadorId(wee.getId().getCubicadorId());
                    weeHistoricoId.setDecisionId(wee.getId().getDecisionId());
                    weeHistoricoId.setEventoId(wee.getId().getEventoId());
                    weeHistoricoId.setRolId(wee.getId().getRolId());
                    weeHistoricoId.setWorkflowId(wee.getId().getWorkflowId());
                    weeHistoricoId.setEjecutado(wee.getEjecutado());

                    WorkflowEventosEjecucionHistorico weeHistorico = new WorkflowEventosEjecucionHistorico();
                    weeHistorico.setId(weeHistoricoId);
                    weeHistorico.setOt(wee.getOt());
                    weeHistorico.setEventoPadre(wee.getEventoPadre());
                    weeHistorico.setFechaCreacion(wee.getFechaCreacion());
                    weeHistorico.setFechaInicioEstimada(wee.getFechaInicioEstimada());
                    weeHistorico.setFechaInicioReal(wee.getFechaInicioReal());
                    weeHistorico.setFechaTerminoEstimada(wee.getFechaTerminoEstimada());
                    weeHistorico.setFechaTerminoReal(wee.getFechaTerminoReal());
                    weeHistorico.setProveedorId(wee.getProveedorId());
                    weeHistorico.setProximo(wee.getProximo());
                    weeHistorico.setUsuarioEjecutorId(wee.getUsuarioEjecutorId());
                    weeHistorico.setUsuarioId(wee.getUsuarioId());

                    try {
                        sesion.save(weeHistorico);
                    } catch (HibernateException hibex) {
                        hibex.printStackTrace();
                        throw hibex;
                    }
                    break;
                }
            }

            if(diseno){
                for(WorkflowEventosEjecucion wee : lista){
                    if(wee.getId().getEventoId() == eventoIdProximo){
                        wee.setEjecutado(9999);
                        wee.setUsuarioId(gestorConstruccionId);
                        sesion.update(wee);
                        break;
                    }
                }
            }else{
                for(WorkflowEventosEjecucion wee : lista){
                    if(wee.getId().getEventoId() == eventoIdProximo){
                        wee.setEjecutado(9999);
                        sesion.update(wee);
                        break;
                    }
                }
            }



            retorno = 1;

        } catch (HibernateException e) {
            throw e;
        }
        return retorno;
    }


    protected int continuarFlujoWorkfloEventoEjecucionRechazoCheckList(int idActa, String observaciones, int otId, int eventoOriginal, int eventoEjecutar, int idEventoProximo) throws Exception {
        List<WorkflowEventosEjecucion> lista = new ArrayList();

        List<Actas> listaActas = new ArrayList();
        int retorno = 0;

        try {

            Criteria criteriaActa = sesion.createCriteria(Actas.class);
            criteriaActa.add(Restrictions.eq("id", idActa));
            listaActas = criteriaActa.list();
            Actas acta = listaActas.get(0);

            String obsOld = acta.getObservaciones();
            obsOld = obsOld.replace("]",",");
            String observacionNueva = obsOld + observaciones + "]";
            acta.setValidacionUsuarios(false);
            acta.setObservaciones(observacionNueva);
            sesion.update(acta);

            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            lista = criteria.list();

            int contador = 0;
            // Obtengo el valor de ejecutado
            int ejecutado = 0;
            for(WorkflowEventosEjecucion wee : lista){
                ejecutado = wee.getEjecutado();
                if(ejecutado > contador && ejecutado!=9999){
                    contador = ejecutado;
                    log.debug("contador : " + contador);
                }
            }


            for(WorkflowEventosEjecucion wee : lista){

                if(wee.getId().getEventoId() == eventoOriginal){
                    Date fechaActual = new Date();
                    wee.setFechaInicioReal(fechaActual);
                    wee.setFechaTerminoReal(fechaActual);
                    wee.setEjecutado(contador + 1);
                    log.debug("id evento ejecutado: " + wee.getId() + "  contador: " + wee.getEjecutado());
                    sesion.update(wee);
                    WorkflowEventosEjecucionHistoricoId weeHistoricoId = new WorkflowEventosEjecucionHistoricoId();
                    weeHistoricoId.setCubicadorId(wee.getId().getCubicadorId());
                    weeHistoricoId.setDecisionId(wee.getId().getDecisionId());
                    weeHistoricoId.setEventoId(wee.getId().getEventoId());
                    weeHistoricoId.setRolId(wee.getId().getRolId());
                    weeHistoricoId.setWorkflowId(wee.getId().getWorkflowId());
                    weeHistoricoId.setEjecutado(wee.getEjecutado());

                    WorkflowEventosEjecucionHistorico weeHistorico = new WorkflowEventosEjecucionHistorico();
                    weeHistorico.setId(weeHistoricoId);
                    weeHistorico.setOt(wee.getOt());
                    weeHistorico.setEventoPadre(wee.getEventoPadre());
                    weeHistorico.setFechaCreacion(wee.getFechaCreacion());
                    weeHistorico.setFechaInicioEstimada(wee.getFechaInicioEstimada());
                    weeHistorico.setFechaInicioReal(wee.getFechaInicioReal());
                    weeHistorico.setFechaTerminoEstimada(wee.getFechaTerminoEstimada());
                    weeHistorico.setFechaTerminoReal(wee.getFechaTerminoReal());
                    weeHistorico.setProveedorId(wee.getProveedorId());
                    weeHistorico.setProximo(wee.getProximo());
                    weeHistorico.setUsuarioEjecutorId(wee.getUsuarioEjecutorId());
                    weeHistorico.setUsuarioId(wee.getUsuarioId());

                    try {
                        sesion.save(weeHistorico);
                    } catch (HibernateException hibex) {
                        hibex.printStackTrace();
                        throw hibex;
                    }
                    break;
                }
            }

            for(WorkflowEventosEjecucion wee : lista){
                log.debug(wee.getId().getEventoId() + " == " + idEventoProximo);
                if(wee.getId().getEventoId() == idEventoProximo){
                    wee.setEjecutado(9999);
                    log.debug("id evento econtrado: " + wee.getId());
                    sesion.update(wee);
                    break;
                }
            }

            retorno = 1;

        } catch (HibernateException e) {
            throw e;
        }
        return retorno;
    }


    protected int continuarFlujoWorkfloEventoEjecucion(int otId, int eventoId) throws Exception {
        List<WorkflowEventosEjecucion> lista = new ArrayList();
        int retorno = 0;
        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            lista = criteria.list();

            int contador = 0;
            // Obtengo el valor de ejecutado
            int ejecutado = 0;
            for(WorkflowEventosEjecucion wee : lista){
//                if(wee.getProximo() == eventoId){
//                    contador = wee.getEjecutado();
//                }
                ejecutado = wee.getEjecutado();
                if(ejecutado>contador && ejecutado!=9999){
                    contador = ejecutado;
                }
            }

            int idEventoProximo = 0;
            // Acualizo el registro
            for(WorkflowEventosEjecucion wee : lista){
                if(wee.getId().getEventoId() == eventoId){
                    wee.setFechaTerminoReal(new Date());
                    contador = contador + 1;
                    wee.setEjecutado(contador);
                    idEventoProximo = wee.getProximo();
                    sesion.update(wee);
                }
            }

            // Acualizo el proximo registro
            for(WorkflowEventosEjecucion wee : lista){
                if(wee.getId().getEventoId() == idEventoProximo){
                    wee.setEjecutado(9999);
                    sesion.update(wee);
                }
            }

            retorno = 1;

        } catch (HibernateException e) {
            throw e;
        }
        return retorno;
    }



    protected int continuarFlujoWorkfloEventoEjecucionManual(int otId, int eventoId, boolean reiniciarFechas, int eventoDecicionId) throws Exception {
        List<WorkflowEventosEjecucion> lista = new ArrayList();
        int retorno = 0;
        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            lista = criteria.list();

            int contador = 0;
            // Obtengo el valor de ejecutado
            int ejecutado = 0;
            for(WorkflowEventosEjecucion wee : lista){
                ejecutado = wee.getEjecutado();
                if(ejecutado>contador && ejecutado!=9999){
                    contador = ejecutado;
                    break;
                }
            }
            int idEventoProximo = 0;

            for(WorkflowEventosEjecucion wee : lista){

                if(wee.getId().getEventoId() == eventoDecicionId){
                    Date fechaActual = new Date();
                    wee.setFechaInicioReal(fechaActual);
                    wee.setFechaTerminoReal(fechaActual);
                    contador = contador + 2;
                    wee.setEjecutado(contador);
                    idEventoProximo = wee.getProximo();

                    sesion.update(wee);
                    WorkflowEventosEjecucionHistoricoId weeHistoricoId = new WorkflowEventosEjecucionHistoricoId();
                    weeHistoricoId.setCubicadorId(wee.getId().getCubicadorId());
                    weeHistoricoId.setDecisionId(wee.getId().getDecisionId());
                    weeHistoricoId.setEventoId(wee.getId().getEventoId());
                    weeHistoricoId.setRolId(wee.getId().getRolId());
                    weeHistoricoId.setWorkflowId(wee.getId().getWorkflowId());
                    weeHistoricoId.setEjecutado(wee.getEjecutado());

                    WorkflowEventosEjecucionHistorico weeHistorico = new WorkflowEventosEjecucionHistorico();
                    weeHistorico.setId(weeHistoricoId);
                    weeHistorico.setOt(wee.getOt());
                    weeHistorico.setEventoPadre(wee.getEventoPadre());
                    weeHistorico.setFechaCreacion(wee.getFechaCreacion());
                    weeHistorico.setFechaInicioEstimada(wee.getFechaInicioEstimada());
                    weeHistorico.setFechaInicioReal(wee.getFechaInicioReal());
                    weeHistorico.setFechaTerminoEstimada(wee.getFechaTerminoEstimada());
                    weeHistorico.setFechaTerminoReal(wee.getFechaTerminoReal());
                    weeHistorico.setProveedorId(wee.getProveedorId());
                    weeHistorico.setProximo(wee.getProximo());
                    weeHistorico.setUsuarioEjecutorId(wee.getUsuarioEjecutorId());
                    weeHistorico.setUsuarioId(wee.getUsuarioId());

                    try {
                        sesion.save(weeHistorico);
                    } catch (HibernateException hibex) {
                        hibex.printStackTrace();
                        throw hibex;
                    }
                    break;
                }
            }
            for(WorkflowEventosEjecucion wee : lista){

                if(wee.getId().getEventoId() == eventoId){
                    Date fechaActual = new Date();
                    wee.setFechaInicioReal(fechaActual);
                    wee.setFechaTerminoReal(fechaActual);
                    contador = contador - 1;
                    wee.setEjecutado(contador);
                    sesion.update(wee);
                    WorkflowEventosEjecucionHistoricoId weeHistoricoId = new WorkflowEventosEjecucionHistoricoId();
                    weeHistoricoId.setCubicadorId(wee.getId().getCubicadorId());
                    weeHistoricoId.setDecisionId(wee.getId().getDecisionId());
                    weeHistoricoId.setEventoId(wee.getId().getEventoId());
                    weeHistoricoId.setRolId(wee.getId().getRolId());
                    weeHistoricoId.setWorkflowId(wee.getId().getWorkflowId());
                    weeHistoricoId.setEjecutado(wee.getEjecutado());

                    WorkflowEventosEjecucionHistorico weeHistorico = new WorkflowEventosEjecucionHistorico();
                    weeHistorico.setId(weeHistoricoId);
                    weeHistorico.setOt(wee.getOt());
                    weeHistorico.setEventoPadre(wee.getEventoPadre());
                    weeHistorico.setFechaCreacion(wee.getFechaCreacion());
                    weeHistorico.setFechaInicioEstimada(wee.getFechaInicioEstimada());
                    weeHistorico.setFechaInicioReal(wee.getFechaInicioReal());
                    weeHistorico.setFechaTerminoEstimada(wee.getFechaTerminoEstimada());
                    weeHistorico.setFechaTerminoReal(wee.getFechaTerminoReal());
                    weeHistorico.setProveedorId(wee.getProveedorId());
                    weeHistorico.setProximo(wee.getProximo());
                    weeHistorico.setUsuarioEjecutorId(wee.getUsuarioEjecutorId());
                    weeHistorico.setUsuarioId(wee.getUsuarioId());

                    try {
                        sesion.save(weeHistorico);
                    } catch (HibernateException hibex) {
                        hibex.printStackTrace();
                        throw hibex;
                    }
                    break;
                }
            }
            // Acualizo el proximo registro
            for(WorkflowEventosEjecucion wee : lista){
                if(wee.getId().getEventoId() == idEventoProximo){
                    wee.setEjecutado(9999);
                    if(reiniciarFechas){
                        wee.setFechaInicioReal(null);
                        wee.setFechaTerminoReal(null);
                    }
                    sesion.update(wee);
                    break;
                }
            }

            retorno = 1;

        } catch (HibernateException e) {
            throw e;
        }
        return retorno;
    }

    protected int continuarFlujoWorkflowEventoEjecucionManual(int otId, int eventoIdActual,int proximoEvento) throws Exception {
        List<WorkflowEventosEjecucion> lista = new ArrayList();
        int retorno = 0;
        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class);
            criteria.add(Restrictions.eq("ot.id", otId));
            lista = criteria.list();

            int contador = 0;
            // Obtengo el valor de ejecutado
            int ejecutado = 0;
            for(WorkflowEventosEjecucion wee : lista){
//                if(wee.getProximo() == eventoId){
//                    contador = wee.getEjecutado();
//                }
                ejecutado = wee.getEjecutado();
                if(ejecutado>contador && ejecutado!=9999){
                    contador = ejecutado;
                }
            }

            // Acualizo el registro
            for(WorkflowEventosEjecucion wee : lista){
                if(wee.getId().getEventoId() == eventoIdActual){
                    wee.setFechaTerminoReal(new Date());
                    contador = contador + 1;
                    wee.setEjecutado(contador);
                    sesion.update(wee);
                }
            }

            for(WorkflowEventosEjecucion wee : lista){

                if(wee.getId().getEventoId() == eventoIdActual){
                    Date fechaActual = new Date();
                    wee.setFechaInicioReal(fechaActual);
                    wee.setFechaTerminoReal(fechaActual);
                        sesion.update(wee);
                    WorkflowEventosEjecucionHistoricoId weeHistoricoId = new WorkflowEventosEjecucionHistoricoId();
                    weeHistoricoId.setCubicadorId(wee.getId().getCubicadorId());
                    weeHistoricoId.setDecisionId(wee.getId().getDecisionId());
                    weeHistoricoId.setEventoId(wee.getId().getEventoId());
                    weeHistoricoId.setRolId(wee.getId().getRolId());
                    weeHistoricoId.setWorkflowId(wee.getId().getWorkflowId());
                    weeHistoricoId.setEjecutado(wee.getEjecutado());

                    WorkflowEventosEjecucionHistorico weeHistorico = new WorkflowEventosEjecucionHistorico();
                    weeHistorico.setId(weeHistoricoId);
                    weeHistorico.setOt(wee.getOt());
                    weeHistorico.setEventoPadre(wee.getEventoPadre());
                    weeHistorico.setFechaCreacion(wee.getFechaCreacion());
                    weeHistorico.setFechaInicioEstimada(wee.getFechaInicioEstimada());
                    weeHistorico.setFechaInicioReal(wee.getFechaInicioReal());
                    weeHistorico.setFechaTerminoEstimada(wee.getFechaTerminoEstimada());
                    weeHistorico.setFechaTerminoReal(wee.getFechaTerminoReal());
                    weeHistorico.setProveedorId(wee.getProveedorId());
                    weeHistorico.setProximo(wee.getProximo());
                    weeHistorico.setUsuarioEjecutorId(wee.getUsuarioEjecutorId());
                    weeHistorico.setUsuarioId(wee.getUsuarioId());

                    try {
                        sesion.save(weeHistorico);
                    } catch (HibernateException hibex) {
                        hibex.printStackTrace();
                        throw hibex;
                    }
                    break;
                }
            }

            // Acualizo el proximo registro
            for(WorkflowEventosEjecucion wee : lista){
                if(wee.getId().getEventoId() == proximoEvento){
                    wee.setEjecutado(9999);
                    sesion.update(wee);
                }
            }

            retorno = 1;

        } catch (HibernateException e) {
            throw e;
        }
        return retorno;
    }

    
    protected ObjetoCoreOT obtenerRegistroConFiltro(Class claseObjeto, List<String> joinObjCoreOT, String filtro, int valorFiltro) throws Exception {
        ObjetoCoreOT registroRetorno = null;
        try {
            Criteria criteria = sesion.createCriteria(claseObjeto);
            if (joinObjCoreOT != null) {
                for (String join : joinObjCoreOT) {
                    criteria.setFetchMode(join, FetchMode.JOIN);
                }
            }
            criteria.add(Restrictions.eq(filtro, valorFiltro));
            registroRetorno = (ObjetoCoreOT)criteria.uniqueResult();
        } catch (HibernateException e) {
            throw e;
        }
        return registroRetorno;
    }
    
//    protected List<WorkflowEventos> obtenerWorkflowEventos(int idWorkflow) throws Exception {
//
//        try {
//            Query query = sesion.createSQLQuery(
//                    "SELECT * FROM WORKFLOW_EVENTOS WHERE ID IN"
//                    + "(SELECT DISTINCT(ROL_ID) FROM WORKFLOW_EVENTOS_EJECUCION WHERE OT_ID = :idWorkflow)"
//            )
//                    .addEntity(Roles.class)
//                    .setParameter("idWorkflow", idWorkflow);
//            List<Roles> result = query.list();
//            return result;
//        } catch (HibernateException hExcep) {
//            log.error("Error al obtener roles desde la entidad workflowEventosEjecucion");
//            throw hExcep;
//        }
//    }
    
    protected boolean registrarWee(WorkflowEventosEjecucion obj) throws Exception {
        boolean respuesta = false;
        try {
            Serializable id = sesion.save(obj);
            respuesta = true;
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + obj.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return respuesta;
    }

    protected boolean registrarWeeHistorico(WorkflowEventosEjecucionHistorico obj) throws Exception {
        boolean respuesta = false;
        try {
            Serializable id = sesion.save(obj);
            respuesta = true;
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + obj.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return respuesta;
    }

    protected Conceptos obtenerConceptoPorIdEvento(int idEvento) throws Exception {
        Conceptos concepto =  new Conceptos();
        try {
            List<Conceptos> lista = new ArrayList();
            Criteria criteria = sesion.createCriteria(Conceptos.class);
            criteria.add(Restrictions.eq("eventos.id", idEvento));
            lista = criteria.list();
            concepto = lista.get(0);

        } catch (HibernateException e) {
            throw e;
        }
        return concepto;
    }

    protected boolean registrarUsuariosValidadores(UsuariosValidadores usuarioValidador) throws Exception {
        boolean respuesta = false;
        try {
            Serializable id = sesion.save(usuarioValidador);
            respuesta = true;
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + usuarioValidador.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return respuesta;
    }

    public boolean eliminaUsuarioOtInicial(Ot ot, int idEvento) throws Exception {
        boolean registro = false;

        try {
            SQLQuery sqlQuery = sesion.createSQLQuery("DELETE FROM usuarios_ot WHERE evento_id = :eventoId  and ot_id = :otIdUpdate");
            sqlQuery.setParameter("eventoId", idEvento);
            sqlQuery.setParameter("otIdUpdate", ot.getId());
            sqlQuery.executeUpdate();
            sesion.flush();

            registro = true;

        } catch (Exception e) {
            // retu
            e.printStackTrace();
            throw e;
        }

        return registro;
    }

    public boolean almacenaUsuarioOt(Ot ot, Contratos contrato, int gestor, int usuario, int idEvento) throws Exception {

        boolean registro = false;

        UsuariosOt usuariosOt = new UsuariosOt();
        usuariosOt.setOt(ot);
        List<String> joinObjCoreOT = new ArrayList<>();
        Eventos eventos = (Eventos) obtenerRegistroConFiltro(Eventos.class,joinObjCoreOT, "id", idEvento);
        usuariosOt.setEventos(eventos);
        Usuarios usuarios = (Usuarios) obtenerRegistroConFiltro(Usuarios.class,joinObjCoreOT, "id", usuario);
        usuariosOt.setUsuarios(usuarios);
        usuariosOt.setContratoId(contrato.getId());
        usuariosOt.setGestorTelefonicaId(gestor);
        usuariosOt.setParticipacion(eventos.getNombre());
        WorkflowEventos we2 = new WorkflowEventos();
        Criteria criteria2 = sesion.createCriteria(WorkflowEventos.class)
                .add(Restrictions.eq("evento.id", idEvento))
                .add(Restrictions.eq("workflow.id", contrato.getWorkflow().getId()));
        we2 = (WorkflowEventos) criteria2.uniqueResult();
        Set<Roles> roles = usuarios.getUsuariosRoles();
        Roles rolUsado = new Roles();
        for (Roles rol: roles){
            if(we2.getRol().getId() == rol.getId()){
                rolUsado = rol;
//                Aca se deben agregar todos los evenetos con mas de un rol posible a ser el usuario ejecutor
            } else if(idEvento == 100 || idEvento == 2 || idEvento == 3 || idEvento == 6 || idEvento == 59){
                rolUsado = rol;
            }
        }
        usuariosOt.setRoles(rolUsado);

        UsuariosOtId usuariosOtId = new UsuariosOtId();
        usuariosOtId.setEventoId(idEvento);
        usuariosOtId.setOtId(ot.getId());
        usuariosOtId.setUsuarioId(usuario);
        usuariosOtId.setRolId(rolUsado.getId());

        usuariosOt.setId(usuariosOtId);

        try {
            if(rolUsado.getId() != 0){
                registro = registrarUsuariosOt(usuariosOt);
            }
        } catch (Exception e) {
            // retu
            e.printStackTrace();
            throw e;
        }

        return registro;
    }

    protected boolean registrarUsuariosOt(UsuariosOt usuariosOt) throws Exception {
        boolean respuesta = false;
        try {
            sesion.saveOrUpdate(usuariosOt);
            sesion.flush();
            respuesta = true;
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + usuariosOt.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return respuesta;
    }

    protected List<ProveedoresServicios> obtenerListadoProveedoresServicios(Integer[] vectorID, int contratoId, int regionId, int proveedorId)throws Exception{
        try{
            Criteria criteria = sesion.createCriteria(ProveedoresServicios.class);

            Disjunction objDisjunction = Restrictions.disjunction();
            for (int i = 0; i < vectorID.length; i++) {
                objDisjunction.add(
                        Restrictions.and(
                                Restrictions.eq("proveedores.id", proveedorId),
                                Restrictions.eq("contratos.id", contratoId),
                                Restrictions.eq("regiones.id", regionId),
                                Restrictions.eq("servicios.id", vectorID[i])
                        )
                );
            }
            criteria.add(objDisjunction);
            return criteria.list();
        }catch(HibernateException he){
            log.error(he);
            throw he;
        }
    }

    protected List<Materiales> obtenerListadoMateriales(Integer[] vectorID)throws Exception{
        try{
            Criteria criteria = sesion.createCriteria(Materiales.class);
            criteria.add(Property.forName("id").in(vectorID));
            return criteria.list();
        }catch(HibernateException he){
            log.error(he);
            throw he;
        }
    }
    
    public List<Feriados> listarFeriados() throws Exception {
        List<Feriados> lista = new ArrayList();
        try {
            Criteria criteria = sesion.createCriteria(Feriados.class);
            criteria.addOrder(Order.asc("fecha"));
            lista = criteria.list();
        } catch (HibernateException e) {
            throw e;
        }
        return lista;
    }

    public Actas obtenerActaValida(int otId) throws Exception {
        Actas acta = new Actas();
        try {
            Criteria criteria = sesion.createCriteria(Actas.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .add(Restrictions.eq("validacionUsuarios", true))
                    .addOrder(Order.desc("id"))
                    .setMaxResults(1);
            acta = (Actas) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw e;
        }
        return acta;
    }
    public List<Actas> obtenerListadoActasValidadas(int otId) throws Exception {
        List<Actas> actasValidadas = new ArrayList<Actas>();
        try {
            Criteria criteria = sesion.createCriteria(Actas.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .add(Restrictions.eq("validacionUsuarios", true));
            actasValidadas = (List<Actas>) criteria.list();
        } catch (HibernateException e) {
            throw e;
        }
        return actasValidadas;
    }
    protected boolean registrarPago(Pagos pago) throws Exception {
        Serializable idRegistro = 0;
        boolean registro = false;
        try {
            idRegistro = sesion.save(pago);
            sesion.flush();
            registro = true;
        } catch (HibernateException e) {
            log.error("No se pudo crear el pago");
            throw e;
        }
        return registro;
    }
    protected DetalleCubicadorOrdinarioActas obtieneDetalleCubicadorOrdinarioActas(DetalleCubicadorOrdinarioActasId detalleCubicadorOrdinarioActasId) throws Exception {
        Serializable idRegistro = 0;
        DetalleCubicadorOrdinarioActas detalleCubicadorOrdinarioActas = new DetalleCubicadorOrdinarioActas();
        try {
            Criteria criteria1 = sesion.createCriteria(DetalleCubicadorOrdinarioActas.class)
                    .add(Restrictions.eq("id", detalleCubicadorOrdinarioActasId));
            detalleCubicadorOrdinarioActas = (DetalleCubicadorOrdinarioActas) criteria1.uniqueResult();
        } catch (HibernateException e) {
            log.error("No se pudo crear el pago");
            throw e;
        }
        return detalleCubicadorOrdinarioActas;
    }
    protected CubicadorDetalleUnificadoActas obtieneCubicadorDetalleUnificadoActas(CubicadorDetalleUnificadoActasId cubicadorDetalleUnificadoActasId) throws Exception {
        Serializable idRegistro = 0;
        CubicadorDetalleUnificadoActas cubicadorDetalleUnificadoActas = new CubicadorDetalleUnificadoActas();
        try {
            Criteria criteria1 = sesion.createCriteria(CubicadorDetalleUnificadoActas.class)
                    .add(Restrictions.eq("id", cubicadorDetalleUnificadoActasId));
            cubicadorDetalleUnificadoActas = (CubicadorDetalleUnificadoActas) criteria1.uniqueResult();
        } catch (HibernateException e) {
            log.error("No se pudo crear el pago");
            throw e;
        }
        return cubicadorDetalleUnificadoActas;
    }
    protected boolean registrarDetalleCubicacionOrdinaria(DetalleCubicadorOrdinarioActas detalleCubicadorOrdinarioActas) throws Exception {
        Serializable idRegistro = 0;
        boolean registro = false;
        try {
            sesion.save(detalleCubicadorOrdinarioActas);
            sesion.flush();
            registro = true;
        } catch (HibernateException e) {
            log.error("No se pudo crear el detalleCubicadorOrdinarioActas");
            log.error("Error: "+ e.toString());
            throw e;
        }
        return registro;
    }
    protected boolean registrarDetalleCubicacionUnificado(CubicadorDetalleUnificadoActas cubicadorDetalleUnificadoActas) throws Exception {
        Serializable idRegistro = 0;
        boolean registro = false;
        try {
            sesion.save(cubicadorDetalleUnificadoActas);
            sesion.flush();
            registro = true;
        } catch (HibernateException e) {
            log.error("No se pudo crear el registrarDetalleCubicacionUnificado");
            log.error("Error: "+ e.toString());
            throw e;
        }
        return registro;
    }
    protected boolean registrarServiciosAdicionalesUnificado(CubicadorDetalleAdicionales cubicadorDetalleAdicionales) throws Exception {
        Serializable idRegistro = 0;
        boolean registro = false;
        try {
            sesion.save(cubicadorDetalleAdicionales);
            sesion.flush();
            registro = true;
        } catch (HibernateException e) {
            log.error("No se pudo crear el detalleCubicadorOrdinarioActas");
            log.error("Error: "+ e.toString());
            throw e;
        }
        return registro;
    }
    protected boolean actualizaActarechazoJefatura(Actas acta) throws Exception {
        boolean respuesta = false;
        int contratoId = acta.getOt().getContrato().getId();
        int actaId = acta.getId();
        double nuevoMonto = 0;
        try {
            switch (contratoId) {
                case 2:
                    List<DetalleCubicadorOrdinarioActas> listaDetalleCubicadorOrdinarioActas = new ArrayList<DetalleCubicadorOrdinarioActas>();
                    Criteria criteria = sesion.createCriteria(DetalleCubicadorOrdinarioActas.class)
                            .add(Restrictions.eq("id.actaId", actaId));
                    listaDetalleCubicadorOrdinarioActas = (List<DetalleCubicadorOrdinarioActas>) criteria.list();
                    for (DetalleCubicadorOrdinarioActas detalleCubicadorOrdinarioActas : listaDetalleCubicadorOrdinarioActas) {
                        nuevoMonto = detalleCubicadorOrdinarioActas.getTotalUnidadesEjecucionOt() - detalleCubicadorOrdinarioActas.getTotalUnidadesEjecucionActa();
                        detalleCubicadorOrdinarioActas.setTotalUnidadesEjecucionOt(nuevoMonto);
                        sesion.update(detalleCubicadorOrdinarioActas);
                    }
                    sesion.flush();

                    break;
                case 4:
                    List<CubicadorDetalleUnificadoActas> listaCubicadorDetalleUnificadoActas = new ArrayList<CubicadorDetalleUnificadoActas>();
                    Criteria criteria1 = sesion.createCriteria(DetalleCubicadorOrdinarioActas.class)
                            .add(Restrictions.eq("id.actaId", actaId));
                    listaCubicadorDetalleUnificadoActas = (List<CubicadorDetalleUnificadoActas>) criteria1.list();
                    for (CubicadorDetalleUnificadoActas cubicadorDetalleUnificadoActas : listaCubicadorDetalleUnificadoActas) {
                        nuevoMonto = cubicadorDetalleUnificadoActas.getTotalUnidadesEjecucionOt() - cubicadorDetalleUnificadoActas.getTotalUnidadesEjecucionActa();
                        cubicadorDetalleUnificadoActas.setTotalUnidadesEjecucionOt(nuevoMonto);
                        sesion.update(cubicadorDetalleUnificadoActas);
                    }
                    sesion.flush();
                    break;

            }
            respuesta = true;
        }catch (Exception e){
            log.error("Error al actualizar acta despues de haber sido rechazada por Jefe encargado aprobacion");
            throw new Exception("Error al actualizar acta despues de haber sido rechazada por Jefe encargado aprobacion,  error :  " + e.toString());
        }

        return  respuesta;
    }
    protected boolean apruebaActaParaPago(Actas acta) throws Exception {
        boolean actualizaActa = false;
        try{
            Date date = new Date();
            acta.setPagoAprobado(true);
            acta.setFechaPasoAGestionEconomica(date);
            sesion.update(acta);
            sesion.flush();
            actualizaActa = true;
        }catch(Exception e){
            log.error("Error al actualizar acta para validacion de pago, error: "+e.toString());
            throw e;
        }
        return actualizaActa;
    }
    protected boolean actualizaActaRechazada(int idMaterial, double montoRechazado, int idActa) throws Exception {
        Serializable idRegistro = 0;
        boolean registro = false;
        try {
            DetalleCubicadorOrdinarioActasId detalleCubicadorOrdinarioActasAuxId = new DetalleCubicadorOrdinarioActasId();
            DetalleCubicadorOrdinarioActas detalleCubicadorOrdinarioActasAux = new DetalleCubicadorOrdinarioActas();
            detalleCubicadorOrdinarioActasAuxId.setActaId(idActa);
            detalleCubicadorOrdinarioActasAuxId.setCubicadorOrdinarioId(idMaterial);
            Criteria criteria1 = sesion.createCriteria(DetalleCubicadorOrdinarioActas.class)
                    .add(Restrictions.eq("id", detalleCubicadorOrdinarioActasAuxId));
            detalleCubicadorOrdinarioActasAux = (DetalleCubicadorOrdinarioActas) criteria1.uniqueResult();

            detalleCubicadorOrdinarioActasAux.setValidacionSistema(false);
            sesion.update(detalleCubicadorOrdinarioActasAux);
            registro = true;
        } catch (HibernateException e) {
            log.error("No se pudo crear el detalleCubicadorOrdinarioActas");
            log.error("Error: "+ e.toString());
            throw e;
        }
        return registro;
    }
    protected boolean actualizaServicioRechazado(int idServicio, boolean validacion) throws Exception {
        Serializable idRegistro = 0;
        boolean registro = false;
        try {
            CubicadorDetalleAdicionales cubicadorDetalleAdicionales = new CubicadorDetalleAdicionales();

            Criteria criteria1 = sesion.createCriteria(CubicadorDetalleAdicionales.class)
                    .add(Restrictions.eq("id", idServicio));
            cubicadorDetalleAdicionales = (CubicadorDetalleAdicionales) criteria1.uniqueResult();

            cubicadorDetalleAdicionales.setValidacionSistema(validacion);
            sesion.update(cubicadorDetalleAdicionales);
            registro = true;
        } catch (HibernateException e) {
            log.error("No se pudo actualizar servicio al haber sido rechazado por gestor");
            log.error("Error: "+ e.toString());
            throw e;
        }
        return registro;
    }
    protected boolean validarCubicacionOrdinaria(int idItems, int idActa) throws Exception {
        Serializable idRegistro = 0;
        boolean registro = false;
        try {
            DetalleCubicadorOrdinarioActasId detalleCubicadorOrdinarioActasAuxId = new DetalleCubicadorOrdinarioActasId();
            detalleCubicadorOrdinarioActasAuxId.setActaId(idActa);
            detalleCubicadorOrdinarioActasAuxId.setCubicadorOrdinarioId(idItems);
            DetalleCubicadorOrdinarioActas detalleCubicadorOrdinarioActasAux = new DetalleCubicadorOrdinarioActas();
            Criteria criteria1 = sesion.createCriteria(DetalleCubicadorOrdinarioActas.class)
                    .add(Restrictions.eq("id", detalleCubicadorOrdinarioActasAuxId));
            detalleCubicadorOrdinarioActasAux = (DetalleCubicadorOrdinarioActas) criteria1.uniqueResult();
            double totalEjecucionOt = detalleCubicadorOrdinarioActasAux.getTotalUnidadesEjecucionOt() + detalleCubicadorOrdinarioActasAux.getTotalUnidadesEjecucionActa();
            if(totalEjecucionOt > detalleCubicadorOrdinarioActasAux.getTotalUnidadesCubicadas()){
                log.error("=============== Error al generar ACTA ==========");
                log.error("****   Intenta insertar cantidad de material en tabla detalle_cubicador_ordinario_actas que sobrepasa a lo cubicado  ***");
                throw new Exception("Intenta insertar cantidad de material en tabla detalle_cubicador_ordinario_actas que sobrepasa a lo cubicado");
            }
            detalleCubicadorOrdinarioActasAux.setTotalUnidadesEjecucionOt(totalEjecucionOt);
            detalleCubicadorOrdinarioActasAux.setValidacionSistema(true);
            sesion.update(detalleCubicadorOrdinarioActasAux);
            registro = true;
        } catch (HibernateException e) {
            log.error("No se pudo validar detalle Materiales acta contrato ordinario");
            log.error("Error: "+ e.toString());
            throw e;
        }
        return registro;
    }
    protected boolean validadorServiciosUnificado(int idServicios) throws Exception {
        Serializable idRegistro = 0;
        boolean registro = false;
        CubicadorDetalleAdicionales cubicadorDetalleAdicionales = new CubicadorDetalleAdicionales();
        try {
            Criteria criteria = sesion.createCriteria(CubicadorDetalleAdicionales.class)
                    .add(Restrictions.eq("id", idServicios));
            cubicadorDetalleAdicionales = (CubicadorDetalleAdicionales) criteria.uniqueResult();
            cubicadorDetalleAdicionales.setValidacionSistema(true);
            sesion.update(cubicadorDetalleAdicionales);
            sesion.flush();
        } catch (HibernateException e) {
            log.error("No se pudo validar Servicios acta contrato unificado");
            log.error("Error: "+ e.toString());
            throw e;
        }
        return registro;
    }
    protected List<CubicadorOrdinario> obtenerListaCubicacionOrdinariaPorIdOtAcciones(int cubicadorId) {
        try {
            List<CubicadorOrdinario> listado = new ArrayList<CubicadorOrdinario>();
            List<CubicadorOrdinario> listadoAux = (List<CubicadorOrdinario>) sesion.createCriteria(CubicadorOrdinario.class)
                    .add(Restrictions.eq("cubicador.id", cubicadorId))
                    .list();
            for(CubicadorOrdinario cubicadorOrdinario : listadoAux){
                CubicadorOrdinario cubicadorOrdinarioAux = new CubicadorOrdinario();
                TipoMoneda tipoMoneda = new TipoMoneda();
                TipoUnidadMedida tipoUnidadMedida = new TipoUnidadMedida();
                tipoMoneda = (TipoMoneda) sesion.createCriteria(TipoMoneda.class)
                        .add(Restrictions.eq("id", cubicadorOrdinario.getTipoMoneda().getId()))
                        .uniqueResult();
                tipoUnidadMedida = (TipoUnidadMedida) sesion.createCriteria(TipoUnidadMedida.class)
                        .add(Restrictions.eq("id", cubicadorOrdinario.getTipoUnidadMedida().getId()))
                        .uniqueResult();
                cubicadorOrdinarioAux = cubicadorOrdinario;
                cubicadorOrdinarioAux.setTipoMoneda(tipoMoneda);
                cubicadorOrdinarioAux.setTipoUnidadMedida(tipoUnidadMedida);
                listado.add(cubicadorOrdinarioAux);
            }
            return listado;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones ordinarias. ", ex);
            throw ex;
        }
    }
    protected List<CubicadorDetalle> obtenerCubicadorDetallePorIdCubicadorAcciones(int idCubicacion) {
        try {
            List<CubicadorDetalle> cubicadorDetalleListFinal = new ArrayList<CubicadorDetalle>();
            List<CubicadorDetalle> cubicadorDetalleList = (List<CubicadorDetalle>) sesion.createCriteria(CubicadorDetalle.class)
                    .add(Restrictions.eq("cubicadorId", idCubicacion))
                    .setFetchMode("servicios", FetchMode.JOIN)
                    .list();
            for(CubicadorDetalle cubicadorDetalleAux : cubicadorDetalleList){
                Regiones regiones = (Regiones) sesion.createCriteria(Regiones.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getRegionId()))
                        .uniqueResult();
                Agencias agencias = (Agencias) sesion.createCriteria(Agencias.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getAgenciaId()))
                        .uniqueResult();
                Centrales centrales = (Centrales) sesion.createCriteria(Centrales.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getCentralId()))
                        .uniqueResult();
                TipoMoneda tipoMoneda = (TipoMoneda) sesion.createCriteria(TipoMoneda.class)
                        .add(Restrictions.eq("id", cubicadorDetalleAux.getTipoMonedaId()))
                        .uniqueResult();

                cubicadorDetalleAux.setRegiones(regiones);
                cubicadorDetalleAux.setAgencia(agencias);
                cubicadorDetalleAux.setCentrales(centrales);
                cubicadorDetalleAux.setTipoMoneda(tipoMoneda);
                cubicadorDetalleListFinal.add(cubicadorDetalleAux);
            }
            return cubicadorDetalleList;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicador detalle. ", ex);
            throw ex;
        }
    }
    protected CubicadorOrdinario obtenerCubicacionOrdinariaPorId(int cubicadorId) {
        try {
            CubicadorOrdinario cubicador = new CubicadorOrdinario();
            cubicador = (CubicadorOrdinario) sesion.createCriteria(CubicadorOrdinario.class)
                    .add(Restrictions.eq("id", cubicadorId))
                    .uniqueResult();
            return cubicador;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones ordinarias. ", ex);
            throw ex;
        }
    }
    protected CubicadorDetalle obtenerCubicadorDetallePorId(int cubicadorId) {
        try {
            CubicadorDetalle cubicador = new CubicadorDetalle();
            cubicador = (CubicadorDetalle) sesion.createCriteria(CubicadorDetalle.class)
                    .add(Restrictions.eq("id", cubicadorId))
                    .uniqueResult();
            return cubicador;
        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones ordinarias. ", ex);
            throw ex;
        }
    }


    protected boolean actualizarEstadoOt(int idOt, int tipoEstado, String observaciones) throws Exception {
        Ot ot = new Ot();
        TipoEstadoOt tipoEstadoOt = new TipoEstadoOt();
        boolean actualizo = false;
        try {
            Criteria criteria = sesion.createCriteria(Ot.class);
            criteria.add(Restrictions.eq("id", idOt));
            ot = (Ot) criteria.uniqueResult();

            Criteria criteria2 = sesion.createCriteria(TipoEstadoOt.class);
            criteria2.add(Restrictions.eq("id", tipoEstado));
            tipoEstadoOt = (TipoEstadoOt) criteria2.uniqueResult();
            if(tipoEstadoOt != null){
                ot.setTipoEstadoOt(tipoEstadoOt);
                sesion.update(ot);
                sesion.flush();
                actualizo = true;
            } else {
                throw new Exception("******  No se pudo obtener tipo estado OT...");
            }

        } catch (HibernateException e) {
            throw e;
        }
        return actualizo;
    }

    protected Ot obtieneOtPorId(int otId) throws Exception {
        Ot ot = new Ot();
        try {
            Criteria criteria2 = sesion.createCriteria(Ot.class)
                    .add(Restrictions.eq("id", otId))
                    .setFetchMode("contrato", FetchMode.JOIN);
            ot = (Ot) criteria2.uniqueResult();
        } catch (HibernateException e) {
            throw e;
        }
        return ot;
    }
    protected Cubicador obtieneCubicadorPorIdOt(int cubicadorId) throws Exception {
        Cubicador cubicador = new Cubicador();
        try {
            Criteria criteria2 = sesion.createCriteria(Cubicador.class)
                    .add(Restrictions.eq("ot.id", cubicadorId));
            cubicador = (Cubicador) criteria2.uniqueResult();
        } catch (HibernateException e) {
            throw e;
        }
        return cubicador;
    }
    protected List<CubicadorServicios> obtieneCubicadorServiciosPorCubicadorId(int cubicadorId) throws Exception {
        List<CubicadorServicios> cubicadorServiciosList = new ArrayList<CubicadorServicios>();
        try {
            Criteria criteria = sesion.createCriteria(CubicadorServicios.class)
                    .add(Restrictions.eq("cubicador.id", cubicadorId));
            cubicadorServiciosList = (List<CubicadorServicios>) criteria.list();
        } catch (HibernateException e) {
            throw e;
        }
        return cubicadorServiciosList;
    }

    protected int obtieneTipoMonedaDesicionJerarquicaConServicios(int idContrato, int idProveedor, int idRegion, int idServicio) throws Exception {
        int tipoMoneda = 0;
        int montoValidacion = 0;
        ProveedoresServicios proveedoresServicios = new ProveedoresServicios();
        try {
            Criteria criteria = sesion.createCriteria(ProveedoresServicios.class)
                    .add(Restrictions.eq("contratos.id", idContrato))
                    .add(Restrictions.eq("proveedores.id", idProveedor))
                    .add(Restrictions.eq("regiones.id", idRegion))
                    .add(Restrictions.eq("servicios.id", idServicio))
                    .setFetchMode("tipoMoneda", FetchMode.JOIN);

            proveedoresServicios = (ProveedoresServicios) criteria.uniqueResult();

            if(proveedoresServicios != null){
                tipoMoneda = proveedoresServicios.getTipoMoneda().getId();
                RangoValidacionOtId rangoValidacionOtId = new RangoValidacionOtId();
                RangoValidacionOt rangoValidacionOt = new RangoValidacionOt();

                rangoValidacionOtId.setContratoId(idContrato);
                rangoValidacionOtId.setTipoMonedaId(tipoMoneda);

                Criteria criteria2 = sesion.createCriteria(RangoValidacionOt.class)
                        .add(Restrictions.eq("id", rangoValidacionOtId));

                rangoValidacionOt = (RangoValidacionOt) criteria2.uniqueResult();
                montoValidacion = rangoValidacionOt.getValidaMayorQue();

            } else {
                throw new Exception("********  Error al obtener proveedores servicios con los siguientes parametros: idContrato "+idContrato+" , idProveedor "+idProveedor+" , idRegion "+idRegion+", idServicio" +idServicio);
            }
        } catch (HibernateException e) {
            throw e;
        }
        return montoValidacion;
    }

    protected double obtieneMontoDesicionJerarquicaContipoMoneda(int idContrato, int tipoMoneda) throws Exception {
        double montoValidacion = 0;
        ProveedoresServicios proveedoresServicios = new ProveedoresServicios();
        try {
            RangoValidacionOtId rangoValidacionOtId = new RangoValidacionOtId();
            RangoValidacionOt rangoValidacionOt = new RangoValidacionOt();

            rangoValidacionOtId.setContratoId(idContrato);
            rangoValidacionOtId.setTipoMonedaId(tipoMoneda);

            Criteria criteria2 = sesion.createCriteria(RangoValidacionOt.class)
                    .add(Restrictions.eq("id", rangoValidacionOtId));

            rangoValidacionOt = (RangoValidacionOt) criteria2.uniqueResult();
            montoValidacion = rangoValidacionOt.getValidaMayorQue();
        } catch (HibernateException e) {
            throw e;
        }
        return montoValidacion;
    }

    protected boolean determinaAprobacionGerarquica (int otId) throws Exception{
        Ot ot = new Ot();
        Cubicador cubicador = new Cubicador();
        ot = obtieneOtPorId(otId);
        cubicador = obtieneCubicadorPorIdOt(otId);
        int tipoContrato = ot.getContrato().getId();
        boolean respuesta = false;
        double montoTotal = 0;
        double precioTotal = 0;
        int idServicio = 0;
        double montoValidacion = 0;
        int idProveedor = 0;
        switch (tipoContrato){
            case 1:
                List<CubicadorServicios> cubicadorServiciosListSBE = new ArrayList<CubicadorServicios>();
                cubicadorServiciosListSBE = obtieneCubicadorServiciosPorCubicadorId(cubicador.getId());
                idProveedor = cubicador.getProveedor().getId();
                if(cubicadorServiciosListSBE.size()>0){
                    int idRegion = cubicador.getRegion().getId();
                    for(CubicadorServicios serviciosMap : cubicadorServiciosListSBE){
                        idServicio = serviciosMap.getServicio().getId();
                        precioTotal = serviciosMap.getTotal();
                        montoTotal = montoTotal + precioTotal;
                    }
                    montoValidacion = obtieneTipoMonedaDesicionJerarquicaConServicios(tipoContrato, idProveedor, idRegion, idServicio);
                }
                break;
            case 2:
                List<CubicadorOrdinario> cubicadorOrdinarioList = new ArrayList<CubicadorOrdinario>();
                cubicadorOrdinarioList = obtenerListaCubicacionOrdinariaPorIdOtAcciones(cubicador.getId());
                if(cubicadorOrdinarioList.size()>0){
                    int tipoMoneda = 0;
                    for(CubicadorOrdinario cubiOrdi : cubicadorOrdinarioList){
                        precioTotal = cubiOrdi.getTotal();
                        tipoMoneda = cubiOrdi.getTipoMoneda().getId();
                        montoTotal = montoTotal + precioTotal;
                    }

                    montoValidacion = obtieneMontoDesicionJerarquicaContipoMoneda(tipoContrato, tipoMoneda);
                }
                break;
            case 3:
                List<CubicadorServicios> cubicadorServiciosListRan = new ArrayList<CubicadorServicios>();
                cubicadorServiciosListRan = obtieneCubicadorServiciosPorCubicadorId(cubicador.getId());
                idProveedor = cubicador.getProveedor().getId();
                if(cubicadorServiciosListRan.size()>0){
                    int idRegion = cubicador.getRegion().getId();
                    for(CubicadorServicios serviciosMap : cubicadorServiciosListRan){
                        idServicio = serviciosMap.getServicio().getId();
                        precioTotal = serviciosMap.getTotal();
                        montoTotal = montoTotal + precioTotal;
                    }
                    montoValidacion = obtieneTipoMonedaDesicionJerarquicaConServicios(tipoContrato, idProveedor, idRegion, idServicio);
                }
                break;
            case 4:
                List<CubicadorDetalle> cubicadorDetallesListUni = new ArrayList<CubicadorDetalle>();
                cubicadorDetallesListUni = obtenerCubicadorDetallePorIdCubicadorAcciones(cubicador.getId());
                if(cubicadorDetallesListUni.size()>0){
                    int idRegion = 0;
                    int tipoMoneda = 0;
                    for(CubicadorDetalle servicioUni : cubicadorDetallesListUni){
                        tipoMoneda = servicioUni.getTipoMonedaId();
                        precioTotal = servicioUni.getTotal();
                        montoTotal = montoTotal + precioTotal;
                    }
                    montoValidacion = obtieneMontoDesicionJerarquicaContipoMoneda(tipoContrato, tipoMoneda);
                }
                break;
            case 5:
                List<CubicadorServicios> cubicadorServiciosListLLave = new ArrayList<CubicadorServicios>();
                cubicadorServiciosListLLave = obtieneCubicadorServiciosPorCubicadorId(cubicador.getId());
                idProveedor = cubicador.getProveedor().getId();
                if(cubicadorServiciosListLLave.size()>0){
                    int idRegion = cubicador.getRegion().getId();
                    for(CubicadorServicios serviciosMap : cubicadorServiciosListLLave){
                        idServicio = serviciosMap.getServicio().getId();
                        precioTotal = serviciosMap.getTotal();
                        montoTotal = montoTotal + precioTotal;
                    }
                    montoValidacion = obtieneTipoMonedaDesicionJerarquicaConServicios(tipoContrato, idProveedor, idRegion, idServicio);
                }
                break;
            case 6:
                List<CubicadorServicios> cubicadorServiciosList = new ArrayList<CubicadorServicios>();
                cubicadorServiciosList = obtieneCubicadorServiciosPorCubicadorId(cubicador.getId());
                idProveedor = cubicador.getProveedor().getId();
                if(cubicadorServiciosList.size()>0){
                    int idRegion = cubicador.getRegion().getId();
                    for(CubicadorServicios serviciosMap : cubicadorServiciosList){
                        idServicio = serviciosMap.getServicio().getId();
                        precioTotal = serviciosMap.getTotal();
                        montoTotal = montoTotal + precioTotal;
                    }
                    montoValidacion = obtieneTipoMonedaDesicionJerarquicaConServicios(tipoContrato, idProveedor, idRegion, idServicio);
                }
                break;

        }

        if(montoTotal>montoValidacion && montoValidacion !=0){
            respuesta = true;
        }
        return respuesta;
    };
    public boolean cambioProveedor(int otId, int cubicacionId, int proveedorId) {
        boolean respuesta = false;
        Cubicador cubicador = new Cubicador();
        Cubicador cubicadorCompleto = new Cubicador();
        Ot ot = new Ot();
        Proveedores proveedores = new Proveedores();
        try{
            Criteria criteria = sesion.createCriteria(Cubicador.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .add(Restrictions.eq("id", cubicacionId));
            cubicador = (Cubicador) criteria.uniqueResult();

            Criteria criteria2 = sesion.createCriteria(Proveedores.class)
                    .add(Restrictions.eq("id", proveedorId));
            proveedores = (Proveedores) criteria2.uniqueResult();

            Criteria criteria3 = sesion.createCriteria(Ot.class)
                    .add(Restrictions.eq("id", otId));
            ot = (Ot) criteria3.uniqueResult();

            sesion.update(cubicador);
            sesion.update(ot);
            sesion.flush();

            cubicadorCompleto = obtenerDetalleCubicacionGatawaysAcciones(cubicacionId);
            cambioServiciosNuevoProveedor(cubicadorCompleto, proveedores);

            cubicador.setProveedor(proveedores);
            ot.setProveedor(proveedores);





            respuesta = true;
        } catch (Exception e){
            log.error("Error al actualizar cubicacion para cambio de proveedor :  " + e.toString());
            throw  e;
        }

        return respuesta;
    }

    private void cambioServiciosNuevoProveedor(Cubicador cubicador, Proveedores proveedores) {

        Set<CubicadorMateriales> listadoMateriales = cubicador.getCubicadorMateriales();
        Set<CubicadorServicios> listadoServicios = cubicador.getCubicadorServicios();
        Set<CubicadorDetalle> listadoServiciosUnificado = cubicador.getCubicadorServiciosUnificado();
        Set<CubicadorOrdinario> listadoMaterialesOrdinarios = cubicador.getCubicadorOrdinario();


        double total = 0;
        List<ProveedoresServicios> proveedoresServicios = new ArrayList<ProveedoresServicios>();
        TipoMoneda tipoMoneda = new TipoMoneda();
        switch (cubicador.getContrato().getId()){
            case 1:
//                contrato SBE

                for(CubicadorServicios servicios : listadoServicios){

                    proveedoresServicios = obtieneProveedorServiciosGatewaysAcciones(cubicador.getContrato().getId(),  cubicador.getRegion().getId(),  proveedores.getId(),  servicios.getServicio().getId());
                    if(proveedoresServicios.size() > 0) {
                        tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();
                        total = (servicios.getCantidad() * proveedoresServicios.get(0).getPrecio());
                        servicios.setPrecio(proveedoresServicios.get(0).getPrecio());
                        servicios.setTotal(total);
                        sesion.update(servicios);
                        sesion.flush();
                    }
                }

                break;
            case 3:
//                contrato RAN
                for(CubicadorServicios servicios : listadoServicios){

                    proveedoresServicios = obtieneProveedorServiciosGatewaysAcciones(cubicador.getContrato().getId(),  cubicador.getRegion().getId(),  proveedores.getId(),  servicios.getServicio().getId());
                    if(proveedoresServicios.size() > 0) {
                        tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();

                        total = (servicios.getCantidad() * proveedoresServicios.get(0).getPrecio());
                        servicios.setPrecio(proveedoresServicios.get(0).getPrecio());
                        servicios.setTotal(total);
                        sesion.update(servicios);
                        sesion.flush();
                    }
                }
//

                break;
            case 4:
//                contrato UNIFICADO
                for(CubicadorDetalle servicios : listadoServiciosUnificado){

                    proveedoresServicios = obtieneProveedorServiciosGatewaysAcciones(cubicador.getContrato().getId(),  servicios.getRegionId(),  proveedores.getId(),  servicios.getServicios().getId());

                    if(proveedoresServicios.size() > 0) {
                        tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();

                        total = (servicios.getCantidad() * proveedoresServicios.get(0).getPrecio());
                        servicios.setPrecio(proveedoresServicios.get(0).getPrecio());
                        servicios.setTotal(total);
                        sesion.update(servicios);
                        sesion.flush();
                    }
                }

                break;
            case 5:
//                Contrato LLave en Mano
                for(CubicadorServicios servicios : listadoServicios){

                    proveedoresServicios = obtieneProveedorServiciosGatewaysAcciones(cubicador.getContrato().getId(),  cubicador.getRegion().getId(),  proveedores.getId(),  servicios.getServicio().getId());
                    if(proveedoresServicios.size() > 0) {
                        tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();

                        total = (servicios.getCantidad() * proveedoresServicios.get(0).getPrecio());
                        servicios.setPrecio(proveedoresServicios.get(0).getPrecio());
                        servicios.setTotal(total);
                        sesion.update(servicios);
                        sesion.flush();
                    }
                }
                break;
            case 6:
//                Contrato Salas
                for(CubicadorServicios servicios : listadoServicios){

                    proveedoresServicios = obtieneProveedorServiciosGatewaysAcciones(cubicador.getContrato().getId(),  cubicador.getRegion().getId(),  proveedores.getId(),  servicios.getServicio().getId());
                    if(proveedoresServicios.size() > 0) {
                        tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();

                        total = (servicios.getCantidad() * proveedoresServicios.get(0).getPrecio());
                        servicios.setPrecio(proveedoresServicios.get(0).getPrecio());
                        servicios.setTotal(total);
                        sesion.update(servicios);
                        sesion.flush();
                    }
                }
                break;
            case 8:
//                Contrato Satelital
                for(CubicadorServicios servicios : listadoServicios){

                    proveedoresServicios = obtieneProveedorServiciosGatewaysAcciones(cubicador.getContrato().getId(),  cubicador.getRegion().getId(),  proveedores.getId(),  servicios.getServicio().getId());
                    if(proveedoresServicios.size() > 0) {
                        tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();

                        total = (servicios.getCantidad() * proveedoresServicios.get(0).getPrecio());
                        servicios.setPrecio(proveedoresServicios.get(0).getPrecio());
                        servicios.setTotal(total);
                        sesion.update(servicios);
                        sesion.flush();
                    }
                }
                break;
            case 10:
//                Contrato Carros moviles
                for(CubicadorServicios servicios : listadoServicios){

                    proveedoresServicios = obtieneProveedorServiciosGatewaysAcciones(cubicador.getContrato().getId(),  cubicador.getRegion().getId(),  proveedores.getId(),  servicios.getServicio().getId());
                    if(proveedoresServicios.size() > 0) {
                        tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();

                        total = (servicios.getCantidad() * proveedoresServicios.get(0).getPrecio());
                        servicios.setPrecio(proveedoresServicios.get(0).getPrecio());
                        servicios.setTotal(total);
                        sesion.update(servicios);
                        sesion.flush();
                    }
                }
                break;
            case 11:
//                Contrato Indoor
                for(CubicadorServicios servicios : listadoServicios){

                    proveedoresServicios = obtieneProveedorServiciosGatewaysAcciones(cubicador.getContrato().getId(),  cubicador.getRegion().getId(),  proveedores.getId(),  servicios.getServicio().getId());
                    if(proveedoresServicios.size() > 0) {
                        tipoMoneda = proveedoresServicios.get(0).getTipoMoneda();

                        total = (servicios.getCantidad() * proveedoresServicios.get(0).getPrecio());
                        servicios.setPrecio(proveedoresServicios.get(0).getPrecio());
                        servicios.setTotal(total);
                        sesion.update(servicios);
                        sesion.flush();
                    }
                }
                break;
        }
    }
    protected List<ProveedoresServicios> obtieneProveedorServiciosGatewaysAcciones(int contratoId, int regionId, int proveedorId, int servicioId) {
        List<ProveedoresServicios> lista = new ArrayList<ProveedoresServicios>();
        try {

            try {
                Criteria criteria = sesion.createCriteria(ProveedoresServicios.class);
                criteria.add(Restrictions.eq("contratos.id", contratoId));
                criteria.add(Restrictions.eq("regiones.id", regionId));
                criteria.add(Restrictions.eq("proveedores.id", proveedorId));
                criteria.add(Restrictions.eq("servicios.id", servicioId));
                lista =(List<ProveedoresServicios>) criteria.list();

            } catch (HibernateException he) {
                log.error(he);
                throw he;
            }

            return lista;
        } catch (HibernateException hExcep) {
            log.error("Error al obtener roles desde la entidad workflowEventosEjecucion");
            throw hExcep;
        }

    }

    protected Cubicador obtenerDetalleCubicacionGatawaysAcciones(int cubicacionId) {
        Cubicador cubicacion = new Cubicador();
        try {
            Cubicador cubi = (Cubicador) sesion.createCriteria(Cubicador.class)
                    .add(Restrictions.eq("id", cubicacionId))
                    .setFetchMode("proveedor", FetchMode.JOIN)
                    .setFetchMode("contrato", FetchMode.JOIN)
                    .setFetchMode("cubicadorServicios", FetchMode.JOIN)
                    .setFetchMode("cubicadorMateriales", FetchMode.JOIN)
                    .setFetchMode("cubicadorOrdinario", FetchMode.JOIN)
                    .setFetchMode("cubicadorUnificado", FetchMode.JOIN)
                    .uniqueResult();

            cubicacion.setId(cubi.getId());
            cubicacion.setNombre(cubi.getNombre());
            Proveedores proveAux = new Proveedores();
            Proveedores prove = (Proveedores) retornarObjetoCoreOT(Proveedores.class, cubi.getProveedor().getId());
            proveAux.setId(prove.getId());
            proveAux.setNombre(prove.getNombre());
            cubicacion.setProveedor(proveAux);
            cubicacion.setDescripcion(cubi.getDescripcion());
            Contratos contratoAux = new Contratos();
            Contratos contrato = (Contratos) retornarObjetoCoreOT(Contratos.class, cubi.getContrato().getId());
            contratoAux.setId(contrato.getId());
            contratoAux.setNombre(contrato.getNombre());
            cubicacion.setContrato(contratoAux);

            Set<CubicadorOrdinario> cubicacionOrdinario = new HashSet<>();
            Set<CubicadorServicios> cubicacionServicios = new HashSet<>();
            Set<CubicadorMateriales> cubicacionMateriales = new HashSet<>();

            if(cubi.getContrato().getId() == 1 || cubi.getContrato().getId() == 3 || cubi.getContrato().getId() == 5 || cubi.getContrato().getId() == 9 || cubi.getContrato().getId() == 10 || cubi.getContrato().getId() == 11){
                Regiones region = (Regiones)retornarObjetoCoreOT(Regiones.class, cubi.getRegion().getId());
                Regiones regionAxu = new Regiones(region.getId(),region.getCodigo(),region.getNombre());
                cubicacion.setRegion(regionAxu);
            }

            if (cubi.getContrato().getId() == 2) {
                Set<CubicadorOrdinario> cubiOrdinario = cubi.getCubicadorOrdinario();
                for (CubicadorOrdinario cubAuxOrdinario : cubiOrdinario) {
                    CubicadorOrdinario cubicacionOrdinaria = new CubicadorOrdinario();
                    cubicacionOrdinaria.setId(cubAuxOrdinario.getId());
                    cubicacionOrdinaria.setPrecio(cubAuxOrdinario.getPrecio());
                    cubicacionOrdinaria.setCantidad(cubAuxOrdinario.getCantidad());
                    cubicacionOrdinaria.setItem(cubAuxOrdinario.getItem());
                    TipoMoneda tipoMonedaAux = cubAuxOrdinario.getTipoMoneda();
                    TipoMoneda tipoMoneda = new TipoMoneda(tipoMonedaAux.getId(), tipoMonedaAux.getNombre(), tipoMonedaAux.getDescripcion(), tipoMonedaAux.getEstado());
                    cubicacionOrdinaria.setTipoMoneda(tipoMoneda);
                    cubicacionOrdinaria.setTotal(cubAuxOrdinario.getTotal());
                    cubicacionOrdinario.add(cubicacionOrdinaria);
                }
                cubicacion.setCubicadorOrdinario(cubicacionOrdinario);

            } else if (cubi.getContrato().getId() == 4){
                Set<CubicadorDetalle> cubicadorDetalleLista = new HashSet<>();
//                cubicadorDetalleLista = obtenerCubicadorDetallePorIdCubicador(cubicacionId);
                for (CubicadorDetalle cubicadorDetalle : cubi.getCubicadorServiciosUnificado()){
                    TipoMoneda tipoMoneda = (TipoMoneda) sesion.createCriteria(TipoMoneda.class)
                            .add(Restrictions.eq("id", cubicadorDetalle.getTipoMonedaId()))
                            .uniqueResult();
                    tipoMoneda.setMaterialeses(null);

                    Servicios servicios = (Servicios) sesion.createCriteria(Servicios.class)
                            .add(Restrictions.eq("id", cubicadorDetalle.getServicios().getId()))
                            .setFetchMode("tipoServicio", FetchMode.JOIN)
                            .setFetchMode("tipoUnidadMedida", FetchMode.JOIN)
                            .uniqueResult();

                    Regiones region = (Regiones) sesion.createCriteria(Regiones.class)
                            .add(Restrictions.eq("id", cubicadorDetalle.getRegionId()))
                            .uniqueResult();

                    Centrales centrales = (Centrales) sesion.createCriteria(Centrales.class)
                            .add(Restrictions.eq("id", cubicadorDetalle.getCentralId()))
                            .uniqueResult();

                    Agencias agencias = (Agencias) sesion.createCriteria(Agencias.class)
                            .add(Restrictions.eq("id", cubicadorDetalle.getAgenciaId()))
                            .uniqueResult();

                    TipoUnidadMedida tipoUnidadMedida = new TipoUnidadMedida();
                    TipoUnidadMedida tipoUnidadMedidaProx = new TipoUnidadMedida();
                    tipoUnidadMedidaProx = (TipoUnidadMedida) retornarObjetoCoreOTGatewayAcciones(TipoUnidadMedida.class, servicios.getTipoUnidadMedida().getId());
                    tipoUnidadMedida = new TipoUnidadMedida(tipoUnidadMedidaProx.getId(),tipoUnidadMedidaProx.getNombre(),tipoUnidadMedidaProx.getCodigo(),tipoUnidadMedidaProx.getEstado());

                    cubicadorDetalle.setTipoUnidadMedida(tipoUnidadMedida);
                    cubicadorDetalle.setRegiones(region);
                    cubicadorDetalle.setCentrales(centrales);
                    cubicadorDetalle.setAgencia(agencias);
                    cubicadorDetalle.setServicios(servicios);
                    cubicadorDetalle.setTipoMoneda(tipoMoneda);

                    if(cubicadorDetalle != null){
                        validarYSetearObjetoARetornarGatewayAcciones(cubicadorDetalle, null);
                    }

                    cubicadorDetalleLista.add(cubicadorDetalle);
                }
                cubicacion.setCubicadorServiciosUnificado(cubicadorDetalleLista);
            }else {
                Set<CubicadorServicios> cubiServicios = cubi.getCubicadorServicios();
                for (CubicadorServicios cubServiciosAux : cubiServicios) {
                    CubicadorServicios cubicacionServicio = new CubicadorServicios();
                    cubicacionServicio.setId(cubServiciosAux.getId());
                    cubicacionServicio.setCantidad(cubServiciosAux.getCantidad());
                    cubicacionServicio.setTotal(cubServiciosAux.getTotal());
                    cubicacionServicio.setPrecio(cubServiciosAux.getPrecio());
                    Servicios servicioAux = new Servicios();
                    Servicios servicio = (Servicios) retornarObjetoCoreOT(Servicios.class, cubServiciosAux.getId().getServicioId());
                    servicioAux.setId(servicio.getId());
                    servicioAux.setNombre(servicio.getNombre());
                    servicioAux.setPrecio(cubServiciosAux.getPrecio());
                    servicioAux.setDescripcion(servicio.getDescripcion());
                    cubicacionServicio.setServicio(servicioAux);
                    cubicacionServicios.add(cubicacionServicio);
                }
                cubicacion.setCubicadorServicios(cubicacionServicios);

                Set<CubicadorMateriales> cubiMateriales = cubi.getCubicadorMateriales();
                for (CubicadorMateriales cubMaterialAux : cubiMateriales) {
                    CubicadorMateriales cubicacionMaterial = new CubicadorMateriales();
                    cubicacionMaterial.setId(cubMaterialAux.getId());
                    cubicacionMaterial.setCantidad(cubMaterialAux.getCantidad());
                    cubicacionMaterial.setMontoTotal(cubMaterialAux.getMontoTotal());
                    Materiales materialesAux = new Materiales();
                    Materiales materiales = (Materiales) retornarObjetoCoreOT(Servicios.class, cubMaterialAux.getId().getMaterialId());
                    materialesAux.setId(materiales.getId());
                    materialesAux.setNombre(materiales.getNombre());
                    cubicacionMaterial.setMaterial(materialesAux);
                    cubicacionMateriales.add(cubicacionMaterial);
                }
                cubicacion.setCubicadorMateriales(cubicacionMateriales);

            }

        } catch (HibernateException ex) {
            log.error("Error al obtener listado de cubicaciones. ", ex);
            throw ex;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cubicacion;
    }


    protected void validarYSetearObjetoARetornarGatewayAcciones(ObjetoCoreOT objCoreOT, List<String> camposJoin) throws java.lang.InstantiationException {
        String nombreClaseObjeto = objCoreOT.getClass().getName();
        log.debug("Clase : " + nombreClaseObjeto);
        Field[] atributos = objCoreOT.getClass().getDeclaredFields();

        try {
            for (Field campo : atributos) {
                log.debug("Nombre atributo : " + campo.getName());
                Class claseCampo = campo.getType();
                campo.setAccessible(true);
                boolean isSet = Set.class.isAssignableFrom(claseCampo);
                boolean isJoinField = camposJoin != null ? camposJoin.contains(campo.getName()) : false;
                if (isSet && !isJoinField) {
                    campo.set(objCoreOT, null);
                } else if (ObjetoCoreOT.class.isAssignableFrom(claseCampo) && campo != null) {
                    ObjetoCoreOT attrObjCoreOT = (ObjetoCoreOT) campo.get(objCoreOT);
                    if (attrObjCoreOT != null) {
                        ObjetoCoreOT nuevoAttrObjCoreOT = setearYRetornarAtributoObjCoreOTGatewayAcciones(attrObjCoreOT, claseCampo);
                        campo.set(objCoreOT, nuevoAttrObjCoreOT);
                    }
                } else if (isSet && isJoinField) {
                    Set setObjetos = (Set) campo.get(objCoreOT);
                    Set setAux = new HashSet();
                    for (Object objSet : setObjetos) {
                        int cont = 0;
                        log.debug("**** Seteando coleccion del objeto ****");
                        log.debug("**** Seteando campo JOIN ****" + " " + cont++);
                        ObjetoCoreOT attrObjCoreOT = (ObjetoCoreOT) objSet;
                        validarYSetearObjetoARetornarGatewayAcciones(attrObjCoreOT, null);
                        setAux.add(attrObjCoreOT);
                    }
                    campo.set(objCoreOT, setAux);
                }
            }
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | InstantiationException ex) {
            log.error(ex);
        }
    }

    private ObjetoCoreOT setearYRetornarAtributoObjCoreOTGatewayAcciones(ObjetoCoreOT objCoreOT, Class claseTipoObjeto) throws NoSuchMethodException, InvocationTargetException, SecurityException, IllegalArgumentException, IllegalAccessException, InstantiationException, java.lang.InstantiationException {
        String nombreClaseObjeto = claseTipoObjeto.toString();

        log.debug("Clase : " + nombreClaseObjeto);
        Field[] atributos = claseTipoObjeto.getDeclaredFields();

        Map<String, Method> mapaGets = retornarMapaGetGatewayAcciones(claseTipoObjeto.getMethods(), atributos);
        try {
            Constructor constructor = claseTipoObjeto.getConstructor();
            ObjetoCoreOT objetoRetorno = (ObjetoCoreOT) constructor.newInstance();
            for (Field campo : atributos) {
                log.debug("Nombre atributo : " + campo.getName());
                Class claseCampo = campo.getType();
                Method metodo = mapaGets.get(campo.getName());
                campo.setAccessible(true);
                if (Set.class.isAssignableFrom(claseCampo) || ObjetoCoreOT.class.isAssignableFrom(claseCampo)) {
                    campo.set(objetoRetorno, null);
                } else {
                    Object objetoAux = metodo.invoke(objCoreOT);
                    campo.set(objetoRetorno, objetoAux);
                }
            }
            return objetoRetorno;
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | InstantiationException ex) {
            log.error("Error al setear nuevo atributo de objeto" + ex);
            throw ex;
        }
    }

    private Map<String, Method> retornarMapaGetGatewayAcciones(Method[] metodos, Field[] campos) {
        Map<String, Method> mapaRetorno = new HashMap<>();
        for (Method metodo : metodos) {
            String nombreMetodo = metodo.getName();
            if (isGetterGAtewayAcciones(metodo)) {
                for (Field obj : campos) {
                    int largoEsperadoCampoGet = "get".length() + obj.getName().length();
                    boolean contieneCampo = nombreMetodo.toLowerCase().contains(obj.getName().toLowerCase());
                    if (contieneCampo && (nombreMetodo.length() == largoEsperadoCampoGet)) {
                        mapaRetorno.put(obj.getName(), metodo);
                        break;
                    }
                }
            }
        }
        return mapaRetorno;
    }

    private static boolean isGetterGAtewayAcciones(Method method) {
        if (!method.getName().startsWith("get")) {
            return false;
        }
        if (method.getParameterTypes().length != 0) {
            return false;
        }
        if (void.class.equals(method.getReturnType())) {
            return false;
        }
        return true;
    }
    protected List<Usuarios> obtenerUsuariosPorRolProovedorGatewayAcciones(int rolId, int proveedorId, int contratoId) throws Exception {
        List<Usuarios> usuarios = new ArrayList<>();
        try {

//            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from usuarios WHERE id in (SELECT usuario_id from usuarios_roles WHERE rol_id = :rol) AND usuarios.proveedor_id = :proveedor");
            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from usuarios WHERE id in (SELECT usuario_id from usuarios_roles WHERE rol_id = :rol) AND  id in (SELECT usuario_id from usuarios_contratos WHERE contrato_id = :contrato) AND usuarios.proveedor_id = :proveedor");

            sqlQuery.setParameter("rol",rolId);
            sqlQuery.setParameter("proveedor",proveedorId);
            sqlQuery.setParameter("contrato",contratoId);
            List<Object[]> usuariosAux = sqlQuery.list();
            for(Object[] obj: usuariosAux){
                Usuarios usr = new Usuarios();
                int idAux = (Integer) obj[0];
                String nombreAux = (String) obj[8];
                String apellidoAux = (String) obj[9];
                usr.setId(idAux);
                usr.setNombres(nombreAux);
                usr.setApellidos(apellidoAux);
                usuarios.add(usr);
            }
            return usuarios;
        } catch (Exception e) {
            System.out.println("No se pudo obtener listado de usuarios." + e);
            throw e;
        }
    }
    public Map obtieneRolIdCambioProveedorWee(int eventoId, int otId) {
        Map respuesta = new HashMap();
        WorkflowEventosEjecucion wee = new WorkflowEventosEjecucion();
        WorkflowEventosEjecucion weeProximo = new WorkflowEventosEjecucion();
        try{
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .add(Restrictions.eq("id.eventoId", eventoId));
            wee = (WorkflowEventosEjecucion) criteria.uniqueResult();

            Criteria criteria2 = sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .add(Restrictions.eq("id.eventoId", wee.getProximo()));
            weeProximo = (WorkflowEventosEjecucion) criteria2.uniqueResult();

            respuesta.put("rolUsuario", weeProximo.getId().getRolId());
            respuesta.put("usuarioIdActual", weeProximo.getUsuarioId());

        }catch (Exception e){

        }



        return  respuesta;
    }
    public boolean adjudicaOt(int otId, int rolId, List<Usuarios> usuarios, int userQueSeAdjudicaOt, int eventoPadre, int contratoId) {
        boolean respuesta = false;
        List<UsuariosOt> usuariosOt = new ArrayList<UsuariosOt>();
        UsuariosValidadores usuariosValidadores = new UsuariosValidadores();
        try{

            for(Usuarios borrarUser : usuarios){
                Criteria criteria2 = sesion.createCriteria(UsuariosOt.class)
                        .add(Restrictions.eq("id.rolId", rolId))
                        .add(Restrictions.eq("id.usuarioId", borrarUser.getId()))
                        .add(Restrictions.eq("id.otId", otId));
                usuariosOt = (List<UsuariosOt>) criteria2.list();
                if(usuariosOt.size() > 0){
                    for(UsuariosOt borrarUO : usuariosOt){
                        if(borrarUO.getId().getUsuarioId() == userQueSeAdjudicaOt){
                            break;
                        } else {
                            usuariosValidadores = new UsuariosValidadores();
                            Criteria criteriaUsuariosVal = sesion.createCriteria(UsuariosValidadores.class)
                                    .add(Restrictions.eq("ot.id", otId))
                                    .add(Restrictions.eq("eventos.id", eventoPadre))
                                    .add(Restrictions.eq("usuarios.id", borrarUO.getId().getUsuarioId()));
                            usuariosValidadores = (UsuariosValidadores) criteriaUsuariosVal.uniqueResult();
                            sesion.delete(usuariosValidadores);

                            SQLQuery sqlQuery = sesion.createSQLQuery("DELETE FROM usuarios_ot WHERE rol_id = :rolId and usuario_id = :usuarioId and ot_id = :otIdUpdate");
                            sqlQuery.setParameter("rolId", rolId);
                            sqlQuery.setParameter("usuarioId", borrarUO.getId().getUsuarioId());
                            sqlQuery.setParameter("otIdUpdate", otId);
                            sqlQuery.executeUpdate();
                            sesion.flush();
                            break;
                        }
                    }

                }
            }
//            Estos son todos los eventos que ejecuta un rol de Administrador de contrato inicialmente cargados en la wee, estos varian segun contrato
            List listaEventosAdministradorContrato = new ArrayList();
            listaEventosAdministradorContrato.add(2);
            listaEventosAdministradorContrato.add(3);
            listaEventosAdministradorContrato.add(15);
             if(contratoId == 2){
                listaEventosAdministradorContrato.add(19);
            } else if(contratoId == 4){
                listaEventosAdministradorContrato.add(41);
            } else if(contratoId == 5){
                listaEventosAdministradorContrato.add(56);
            } else {
                 listaEventosAdministradorContrato.add(4);
             }
            listaEventosAdministradorContrato.add(45);
            listaEventosAdministradorContrato.add(46);
            listaEventosAdministradorContrato.add(47);
            for (int i = 0; listaEventosAdministradorContrato.size() > i; i++){
                SQLQuery sqlQuery = sesion.createSQLQuery("UPDATE workflow_eventos_ejecucion SET usuario_id = :nuevoUser WHERE evento_id = :eventoId and ot_id = :otId ");
                sqlQuery.setParameter("nuevoUser", userQueSeAdjudicaOt);
                sqlQuery.setParameter("eventoId", listaEventosAdministradorContrato.get(i));
                sqlQuery.setParameter("otId", otId);
                sqlQuery.executeUpdate();
                sesion.flush();
            }

            respuesta = true;
//            throw new Exception("prueba error para ");
            return  respuesta;
        } catch (Exception e){
            log.error("Error al eliminar usuarios distintos a usuario que se adjudico OT, error : "+ e.toString());
            throw e;
        }
    }
    public boolean actualizarUsuarioCambioProveedorWorkflowEventoEjecucion(int otId, int rolId, List<Usuarios> usuarios) {
        boolean respuesta = false;
        List<WorkflowEventosEjecucion> weeProveedorActual = new ArrayList<WorkflowEventosEjecucion>();
        List<UsuariosOt> usuariosOt = new ArrayList<UsuariosOt>();
        UsuariosOtId usuariosOtId = new UsuariosOtId();
        int eventovalidaUsuarios = 0;
        UsuariosValidadores usuariosValidadores = new UsuariosValidadores();
        int count = 0;
        try {
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .add(Restrictions.eq("id.rolId", rolId))
                    .add(Restrictions.eq("ot.id", otId));
            weeProveedorActual = (List<WorkflowEventosEjecucion>) criteria.list();

            for(Usuarios borrarUser : usuarios){
                Criteria criteria2 = sesion.createCriteria(UsuariosOt.class)
                        .add(Restrictions.eq("id.rolId", weeProveedorActual.get(0).getId().getRolId()))
                        .add(Restrictions.eq("id.usuarioId", borrarUser.getId()))
                        .add(Restrictions.eq("id.otId", otId));
                usuariosOt = (List<UsuariosOt>) criteria2.list();
                if(usuariosOt.size() > 0){
                    for(UsuariosOt borrarUO : usuariosOt){
                        sesion.delete(borrarUO);
                    }
                }
            }

            SQLQuery sqlQuery = sesion.createSQLQuery("UPDATE usuarios_ot set usuario_id = :newUser WHERE rol_id = :rolId and usuario_id = :usuarioId and ot_id = :otIdUpdate");
            sqlQuery.setParameter("newUser",usuarios.get(0).getId());
            sqlQuery.setParameter("rolId",weeProveedorActual.get(0).getId().getRolId());
            sqlQuery.setParameter("usuarioId",weeProveedorActual.get(0).getUsuarioId());
            sqlQuery.setParameter("otIdUpdate", otId);
            sqlQuery.executeUpdate();
            sesion.flush();
            for (WorkflowEventosEjecucion weeNuevo : weeProveedorActual) {
                if(weeNuevo.getEventoPadre() != 0 && count != weeNuevo.getEventoPadre()){

                    Criteria criteriaUsuariosVal = sesion.createCriteria(UsuariosValidadores.class)
                        .add(Restrictions.eq("ot.id", otId))
                        .add(Restrictions.eq("eventos.id", weeNuevo.getEventoPadre()))
                        .add(Restrictions.eq("usuarios.id", weeNuevo.getUsuarioId()));
                    usuariosValidadores = (UsuariosValidadores) criteriaUsuariosVal.uniqueResult();

                    usuariosValidadores.setUsuarios(usuarios.get(0));

                    sesion.update(usuariosValidadores);
                    sesion.flush();

                    count = weeNuevo.getEventoPadre();
                }

                weeNuevo.setUsuarioId(usuarios.get(0).getId());

                sesion.update(weeNuevo);
                sesion.flush();
            }
            respuesta = true;
        } catch (Exception e){
            log.error("Error al actualizar WorkFlowEventosEjecucion al cambiar proveedor: "+ e.toString());
            throw e;
        }

        return  respuesta;
    }
    protected void eliminaAdicionalesAnterioresParaActualizar(int cubicadorId, AtomicInteger contAccionesBBDD){
        List<CubicadorDetalleAdicionales> cubicadorDetalleAdicionales = new ArrayList<CubicadorDetalleAdicionales>();

        SQLQuery sqlQuery2 = sesion.createSQLQuery("SELECT * FROM cubicador_detalle_adicionales WHERE cubicador_id = :cubicadorId");
        sqlQuery2.setParameter("cubicadorId",cubicadorId);
        cubicadorDetalleAdicionales = sqlQuery2.list();

        if(cubicadorDetalleAdicionales.size() > 0 ){
            SQLQuery sqlQuery = sesion.createSQLQuery("DELETE FROM cubicador_detalle_adicionales WHERE cubicador_id = :cubicadorId");
            sqlQuery.setParameter("cubicadorId",cubicadorId);
            sqlQuery.executeUpdate();
            sesion.flush();
//            contAccionesBBDD.getAndIncrement();
        }
    }
    protected ServiciosAux obtieneServicioPorProveedorContratoRegion(int idServicio, int contratoId, int regionId, int proveedorId, int idTipoServicio) throws Exception{
        ServiciosAux serviciosAux = new ServiciosAux();
        TipoMoneda tipoMonedaProxy = null;

        try {
            Criteria criteria = sesion.createCriteria(ProveedoresServicios.class)
                    .setFetchMode("tipoMoneda", FetchMode.JOIN);
            criteria.add(
                    Restrictions.and(

                            Restrictions.eq("proveedores.id", proveedorId),
                            Restrictions.eq("contratos.id", contratoId),
                            Restrictions.eq("regiones.id", regionId),
                            Restrictions.eq("servicios.id", idServicio)
                    )
            );
            List<ProveedoresServicios> lista = criteria.list();
            for (ProveedoresServicios ps : lista) {
                tipoMonedaProxy = new TipoMoneda();
                if (idTipoServicio == ps.getServicios().getTipoServicio().getId()) {
                    ServiciosAux servicio = new ServiciosAux();
                    char estado = ps.getServicios().getEstado();

                    if(estado == 'A'){
                        servicio.setNombre(ps.getServicios().getNombre());
                        servicio.setDescripcion(ps.getServicios().getDescripcion());
                        servicio.setPrecio(ps.getPrecio());

                        tipoMonedaProxy = (TipoMoneda)retornarObjetoCoreOTGatewayAcciones(TipoMoneda.class,ps.getTipoMoneda().getId());
                        TipoMoneda tipoMoneda = new TipoMoneda(tipoMonedaProxy.getId(),tipoMonedaProxy.getNombre(),tipoMonedaProxy.getDescripcion(),tipoMonedaProxy.getEstado());

                        servicio.setTipoMoneda(tipoMoneda);
                        servicio.setId(ps.getServicios().getId());
                        servicio.setIsPackBasico(ps.getServicios().getIsPackBasico());
//                    servicio.setPackBasico(true);
                        Alcances alcances = new Alcances();

                        Criteria criteria3 = sesion.createCriteria(Alcances.class)
                                .add(Restrictions.eq("codigo", ps.getServicios().getCodAlcance()));
                        alcances = (Alcances) criteria3.uniqueResult();
                        if(alcances != null){
                            servicio.setAlcances(alcances);
                        }
                        serviciosAux = servicio;
                    }

                }
            }
        } catch (HibernateException he) {
            log.error(he);
            throw he;
        }
        return serviciosAux;
    }
    protected ObjetoCoreOT retornarObjetoCoreOTGatewayAcciones(Class claseObjetoCoreOT, int id) throws Exception {
        try {
            ObjetoCoreOT objetoRetorno = (ObjetoCoreOT) sesion.get(claseObjetoCoreOT, id);
            return objetoRetorno;
        } catch (Exception e) {
            System.out.println("No se pudo cargar el objeto " + claseObjetoCoreOT.toString());
            throw e;
        }
    }

    protected String obtenerXmlOt(int otId)throws Exception{

        String respXML ="";
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from retornar_xml_otec(:id_ot)");
            sqlQuery.setParameter("id_ot",otId);

            respXML = (String)sqlQuery.uniqueResult();

        }catch(Exception e){
            log.error("Error al obtener XML ot",e);
            throw e;
        }
        return respXML;
    }

    protected String obtenerXmlActa(int actaId)throws Exception{

        String respXML ="";
        try{

            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from retornar_xml_acta(:id_acta)");
            sqlQuery.setParameter("id_acta",actaId);

            respXML = (String)sqlQuery.uniqueResult();

        }catch(Exception e){
            log.error("Error al obtener XML acta",e);
            throw e;
        }
        return respXML;
    }

    public void registrarValidacionGestionEconomica(String nombreTabla, int actaId, int otId, boolean validacionActa, int usuarioValidador, String comentarioRechazo, boolean validacionParcial) throws Exception{
        try{
            Date date = new Date();
            SQLQuery sqlQuery = sesion.createSQLQuery("INSERT INTO "+nombreTabla+" (acta_id, ot_id, validacion_acta, usuario_validador_id, fecha_validacion_acta, motivo_rechazo_gestion_economica_id, validacion_parcial_acta) " +
                    "VALUES ("+actaId+", "+otId+", "+validacionActa+", "+usuarioValidador+", '"+date+"', "+comentarioRechazo+", "+validacionParcial+" ) ");
            sqlQuery.executeUpdate();
            sesion.flush();
        }catch (Exception e){
            log.error("Error al guardar datos en tabla "+nombreTabla+" para flujo Gestion economica validando solo de 1 OT en flujo normal inicial, esta OT es la numero: "+otId+" y el error que arroja es :"+ e.toString());
            throw new Exception("Error al guardar datos en tabla "+nombreTabla+" para flujo Gestion economica validando solo de 1 OT en flujo normal inicial, esta OT es la numero: "+otId+" y el error que arroja es :"+ e.toString());

        }
    }
    public void registrarValidacionGestionEconomicaPagos(String nombreTabla, int acta_id,    int ot_id, boolean validacion_acta, int usuario_validador_id, String motivo_rechazo_gestion_economica_id, boolean validacion_parcial_acta, int pagos_id, int usuario_pagador_id,  double numero_contrato, Long hemId) throws Exception{
        //                                                                       acta_id,    ot_id,           validacion_acta,       usuario_validador_id, motivo_rechazo_gestion_economica_id,       validacion_parcial_acta,               pagos_id, usuario_pagador_id       numero_contrato
        try{

            Date date = new Date();
            SQLQuery sqlQueryUpdate = sesion.createSQLQuery("UPDATE adjuntos_pagos SET pagos_id = "+pagos_id+" WHERE hem_id = "+hemId+" ");
            sqlQueryUpdate.executeUpdate();

            String sql1 = "select * from nextval('bolsas_pagos_id_seq')";
            Query query1 = sesion.createSQLQuery(sql1);
            int bolsa_pag_id = ((BigInteger) query1.uniqueResult()).intValue();


            String sql2 = "select buscar_moneda_id_acta("+acta_id+")";
            Query query2 = sesion.createSQLQuery(sql2);
            int tipo_moneda_id = (int) query2.uniqueResult();



            String sql3 = "SELECT count(acta_id) from detalle_cubicador_ordinario_actas where total_unidades_ejecucion_acta <> 0 and acta_id = "+acta_id+" ";
            Query query3 = sesion.createSQLQuery(sql3);
            int cantidad_items = ((BigInteger) query3.uniqueResult()).intValue();


            SQLQuery sqlQuery = sesion.createSQLQuery("INSERT INTO "+nombreTabla+" (acta_id, ot_id, validacion_acta, usuario_validador_id, fecha_validacion_acta, motivo_rechazo_gestion_economica_id, validacion_parcial_acta, pagos_id, usuario_pagador_id, bolsa_pag_id, numero_contrato, cantidad_items, tipo_moneda_id) " +
                    "VALUES ("+acta_id+", "+ot_id+", "+validacion_acta+", "+usuario_validador_id+", '"+date+"', "+motivo_rechazo_gestion_economica_id+", "+validacion_parcial_acta+", "+pagos_id+", "+usuario_pagador_id+", "+bolsa_pag_id+", "+numero_contrato+", "+cantidad_items+", "+tipo_moneda_id+" ) ");
            sqlQuery.executeUpdate();
            sesion.flush();
        }catch (Exception e){
            log.error("Error al guardar datos en tabla "+nombreTabla+" para flujo Gestion economica validando solo de 1 OT en flujo normal inicial, esta OT es la numero: "+ot_id+" y el error que arroja es :"+ e.toString());
            throw new Exception("Error al guardar datos en tabla "+nombreTabla+" para flujo Gestion economica validando solo de 1 OT en flujo normal inicial, esta OT es la numero: "+ot_id+" y el error que arroja es :"+ e.toString());

        }
    }


    public int obtieneWorflowEventoActualEjecucion(int otId)throws Exception {
        int eventoIdActual = 0;
        WorkflowEventosEjecucion wee = new WorkflowEventosEjecucion();
        try{

            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .add(Restrictions.eq("ot.id", otId))
                    .add(Restrictions.eq("ejecutado", 9999));
            wee = (WorkflowEventosEjecucion) criteria.uniqueResult();
            eventoIdActual = wee.getId().getEventoId();
        } catch (Exception e){
            throw e;
        }

        return eventoIdActual;

    }
    public int obtieneProximoUsuarioDecision(int eventoId, int otId, List<WorkflowEventosEjecucion> wee)throws Exception {
        int usuarioProximo = 0;
        int eventoAnterior = 0;
        int eventoPadre = 0;
        int proximoEvento = 0;
        String aprobacion = "";

        try{
           for(WorkflowEventosEjecucion wee2: wee){
               if(wee2.getProximo() == eventoId) {
                   eventoAnterior = wee2.getId().getEventoId();
                   break;
               }
            }
            for(WorkflowEventosEjecucion wee2: wee){
                if(eventoAnterior == wee2.getId().getEventoId()) {
                    eventoPadre = wee2.getEventoPadre();
                    break;
                }
            }
            for(WorkflowEventosEjecucion wee2: wee){
                if(eventoPadre == wee2.getId().getEventoId()) {
                    eventoPadre = wee2.getEventoPadre();
                    usuarioProximo = wee2.getUsuarioId();
                    break;
                }
            }
            if(eventoPadre != 0){
                for(WorkflowEventosEjecucion wee2: wee){
                    if(eventoPadre == wee2.getId().getEventoId()) {
                        eventoPadre = wee2.getEventoPadre();
                        usuarioProximo = wee2.getUsuarioId();
                        break;
                    }
                }
            } else if(usuarioProximo == 0 && eventoPadre == 0 && eventoId == 7){
                for(WorkflowEventosEjecucion wee2: wee){
                    if(wee2.getEjecutado() == 9999) {
                        usuarioProximo = wee2.getUsuarioId();
                        break;
                    }
                }
            }
            return usuarioProximo;
        }catch (Exception e){
            throw e;
        }
    }
    public List<DetalleServiciosActas> obtieneDetalleServiciosEjecutados(int actaId)throws Exception {
        List<DetalleServiciosActas> detalleServiciosActasList = new ArrayList<DetalleServiciosActas>();
        DetalleServiciosActasId detalleServiciosActasId = new DetalleServiciosActasId();
        detalleServiciosActasId.setActaId(actaId);

        try{
            Criteria criteria = sesion.createCriteria(DetalleServiciosActas.class)
                    .add(Restrictions.eq("id.actaId", actaId));
            detalleServiciosActasList = (List<DetalleServiciosActas>) criteria.list();
            return detalleServiciosActasList;
        } catch (Exception e){
            cerrarSesion();
            throw e;
        }
    }
    public List<DetalleServiciosAdicionalesActas> obtieneDetalleServiciosAdicionalesEjecutados(int actaId)throws Exception {
        List<DetalleServiciosAdicionalesActas> detalleServiciosAdicionalesActasList = new ArrayList<DetalleServiciosAdicionalesActas>();
        DetalleServiciosAdicionalesActasId detalleServiciosAdicionalesActasId = new DetalleServiciosAdicionalesActasId();
        detalleServiciosAdicionalesActasId.setActaId(actaId);

        try{
            Criteria criteria = sesion.createCriteria(DetalleServiciosAdicionalesActas.class)
                    .add(Restrictions.eq("id.actaId", actaId));
            detalleServiciosAdicionalesActasList = (List<DetalleServiciosAdicionalesActas>) criteria.list();
            return detalleServiciosAdicionalesActasList;
        } catch (Exception e){
            cerrarSesion();
            throw e;
        }
    }

    public void CerrarOtyFinalizar(int otId) throws Exception {
        try{
            Ot ot = new Ot();
            Criteria criteria = sesion.createCriteria(Ot.class)
                    .add(Restrictions.eq("id", otId));
            ot = (Ot) criteria.uniqueResult();
            TipoEstadoOt tipoEstadoOt = new TipoEstadoOt();
            tipoEstadoOt.setId(2);
            Date date = new Date();
            ot.setFechaTerminoReal(date);
            ot.setTipoEstadoOt(tipoEstadoOt);
            sesion.update(ot);
            sesion.flush();
        } catch (Exception e){
            throw e;
        }
    }

    protected Repositorios obtenerRepositorio(String nombre) throws Exception {

        Repositorios repo = null;

        try {
            Criteria criteria = sesion.createCriteria(Repositorios.class);
            criteria.add(Restrictions.eq("nombre", nombre));
            criteria.add(Restrictions.eq("estadoActivo", Boolean.TRUE));
            repo = (Repositorios) criteria.uniqueResult();

        } catch (HibernateException e) {
            throw e;
        }
        return repo;
    }


    protected void abrirSesion() throws Exception {
        try {
            SessionFactory sesionFactory = NewHibernateUtil.getSessionFactory();
            this.sesion = sesionFactory.openSession();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    protected void cerrarSesion() {
        this.sesion.close();
    }

    protected void rollback() {
        log.info("********* Realizando ROLLBACK *********");
        this.trx.rollback();
    }

    protected void commit() {
        log.info("********* Realizando COMMIT *********");
        this.trx.commit();
    }
    
    protected void flush() {
        log.info("********* Realizando FLUSH *********");
        this.sesion.flush();
    }

    protected void iniciarTransaccion() {
        this.trx = sesion.beginTransaction();
    }




    private Session sesion;
    private Transaction trx;
    private static final Logger log = Logger.getLogger(Gateway.class);

    public int obtieneProximoEventoWee(int eventoId, int otId, boolean desicion) {
        int eventoProximo = 0;
        try{
            WorkflowEventosEjecucion wee = new WorkflowEventosEjecucion();
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .add(Restrictions.eq("id.eventoId", eventoId))
                    .add(Restrictions.eq("ot.id", otId));
            wee = (WorkflowEventosEjecucion) criteria.uniqueResult();

            eventoProximo = wee.getProximo();

            return eventoProximo;

        } catch (Exception e){
            throw e;
        }
    }
    public int obtieneEventoEventoPadreWee(int eventoId, int otId, boolean desicion) {
        int eventoProximo = 0;
        try{
            WorkflowEventosEjecucion wee = new WorkflowEventosEjecucion();
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .add(Restrictions.eq("id.eventoId", eventoId))
                    .add(Restrictions.eq("ot.id", otId));
            wee = (WorkflowEventosEjecucion) criteria.uniqueResult();

            eventoProximo = wee.getEventoPadre();

            return eventoProximo;

        } catch (Exception e){
            throw e;
        }
    }
    public int obtieneProximoEventoPagarAp(int eventoId, int otId, boolean desicion) {
        int eventoProximo = 0;
        try{
            List<WorkflowEventosEjecucion> weeList = new ArrayList<WorkflowEventosEjecucion>();
            Criteria criteria = sesion.createCriteria(WorkflowEventosEjecucion.class)
                    .add(Restrictions.eq("eventoPadre", eventoId))
                    .add(Restrictions.eq("ot.id", otId));
            weeList = (List<WorkflowEventosEjecucion>) criteria.list();
            int desicionId = 0;
            if(desicion){
//                Se setea 2 de aprobacion tabla desiciones
                desicionId = 2;
            } else {
//                Se setea 3 de rechazo en tabla desiciones
                desicionId = 3;
            }
            for(WorkflowEventosEjecucion wee : weeList){
                if(wee.getId().getDecisionId() == desicionId){
                    eventoProximo = wee.getId().getEventoId();
                }
            }

            return eventoProximo;

        } catch (Exception e){
            throw e;
        }
    }

    public boolean actualizaCubicacionParePagoDisenoAp(int otId, int cubicadorId, List<LinkedHashMap> actividadesCubSeleccionadas, List<LinkedHashMap> actividadesAdiSeleccionadas)throws Exception {
        boolean respuesta = false;
        double montoActaNuevo = 0;
        double montoActaAdicionalNuevo = 0;

        try {

            Actas actas = new Actas();
            actas = obtenerActaValida(otId);
             List<DetalleServiciosActas> detalleServiciosActasList = new ArrayList<DetalleServiciosActas>();
            detalleServiciosActasList  = (List<DetalleServiciosActas>) sesion.createCriteria(DetalleServiciosActas.class)
                    .add(Restrictions.eq("id.actaId", actas.getId()))
                    .list();

            List<DetalleServiciosAdicionalesActas> detalleServiciosAdicionalesActasList = new ArrayList<DetalleServiciosAdicionalesActas>();
            detalleServiciosAdicionalesActasList  = (List<DetalleServiciosAdicionalesActas>) sesion.createCriteria(DetalleServiciosAdicionalesActas.class)
                    .add(Restrictions.eq("id.actaId", actas.getId()))
                    .list();
            boolean esta = false;
            int sevicioId = 0;
            if(detalleServiciosActasList.size() > 0){
                esta = false;
                for(DetalleServiciosActas detalleServiciosActas : detalleServiciosActasList){
                    esta = false;
                    for(int i = 0; i<actividadesCubSeleccionadas.size(); i++){
                        sevicioId = (int) actividadesCubSeleccionadas.get(i).get("idServicio");
                        if(detalleServiciosActas.getServicio().getId() == sevicioId){
                            montoActaNuevo = montoActaNuevo + detalleServiciosActas.getTotalMontoEjecucionActa();
                            esta = true;
                        }
                    }
                    if(!esta){
//                        System.out.println("prueba");
                        sesion.delete(detalleServiciosActas);
                    }
                }
            }
            if(detalleServiciosAdicionalesActasList.size() > 0){
                for(DetalleServiciosAdicionalesActas detalleServiciosAdicionalActa : detalleServiciosAdicionalesActasList){
                    esta = false;
                    for(int i = 0; i<actividadesAdiSeleccionadas.size(); i++){
                        sevicioId = (int) actividadesAdiSeleccionadas.get(i).get("idServicio");
                        if(detalleServiciosAdicionalActa.getServicio().getId() == sevicioId){
                            montoActaAdicionalNuevo = montoActaAdicionalNuevo + detalleServiciosAdicionalActa.getTotalMontoEjecucionActa();
                            esta = true;
                        }
                    }
                    if(!esta){
//                        System.out.println("prueba");
                        sesion.delete(detalleServiciosAdicionalActa);
                    }
                }
            }

            double total = (montoActaNuevo + montoActaAdicionalNuevo);
            double porcentage = (actas.getMontoTotalAPagar() * 100) / actas.getMontoTotalActa();
            if(porcentage > 99){
                actas.setSaldoPendiente(0.0);
                actas.setMontoTotalAPagar(total);
                actas.setEsTotal(true);
            } else {
                double saldoPendiente = (total * porcentage) / 100;
                actas.setSaldoPendiente(saldoPendiente);
                actas.setMontoTotalAPagar((total- saldoPendiente));
                actas.setEsTotal(false);
            }
            actas.setMontoTotalActaCubicado(montoActaNuevo);
            actas.setMontoTotalActaAdicional(montoActaAdicionalNuevo);
            actas.setMontoTotalActa(total);

            sesion.update(actas);
            respuesta = true;
//            throw new Exception();
            return respuesta;

        }catch (Exception e){
            throw e;
        }
    }

    public void guardaDetalleOt(Map datosOt)throws Exception {
        DetalleOt detalleOt = new DetalleOt();
        try {

            int otId = Integer.parseInt(datosOt.get("id").toString());

            String direccion = datosOt.get("direccion").toString();
            String latitud = datosOt.get("latitud").toString();
            String longitud = datosOt.get("longitud").toString();
            try {
                String comuna = datosOt.get("comuna").toString();
                Long avba = Long.parseLong(datosOt.get("avba").toString());
                Long derivada = Long.parseLong(datosOt.get("longitud").toString());

                detalleOt.setComuna(comuna);
                detalleOt.setAvba(avba);
                detalleOt.setDerivada(derivada);
            } catch (Exception e) {

            }

            detalleOt.setOtId(otId);
            detalleOt.setDireccion(direccion);
            detalleOt.setLatitud(latitud);
            detalleOt.setLongitud(longitud);

            sesion.save(detalleOt);
            sesion.flush();
        } catch (Exception e){
            throw e;
        }
    }
}
