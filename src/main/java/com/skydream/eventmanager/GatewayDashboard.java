/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.eventmanager;

import com.skydream.coreot.dao.Gateway;
import com.skydream.coreot.pojos.*;
import com.skydream.coreot.util.NewHibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.hibernate.criterion.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author Christian Espinoza
 */
public class GatewayDashboard {

    protected List retornarApCerradasVsApEnCurso(int usuarioId)throws Exception{
        List retorno = null;
        try{
            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * FROM ap_cerradas_vs_encurso(:usuarioId)");
            sqlQuery.setParameter("usuarioId",usuarioId);
            retorno = sqlQuery.list();
        }catch(Exception e){
            log.error("Error al obtener data desde Procedimiento Almacenado.",e);
        }
        return retorno;
    }

    protected List retornarApFechaInicioRealVsFechaInicio(int usuarioId)throws Exception{

        List<Object[]> listadoCantidad = new ArrayList<>();
        List<MesCantidadAux> listadoFinal = new ArrayList<>();
        try{

            // primero obtengo el año del sistema
            Calendar fechaActual = Calendar.getInstance();
            int mes = fechaActual.get(Calendar.MONTH);
            int ano = fechaActual.get(Calendar.YEAR);
            mes = mes +1;

            // obtengo la suma de ot de los ultimos 12 meses que la fecha de inicio estimada sea diferente a la fecha de inicio real
//            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT EXTRACT(MONTH FROM ot.fecha_inicio_estimada) AS mes, count(*) AS cantidad\n" +
//                    "FROM ot\n" +
//                    "      WHERE EXTRACT(MONTH FROM ot.fecha_inicio_estimada) <= :mes\n" +
//                    "            AND usuario_id =:usuarioId\n" +
//                    "      AND EXTRACT(YEAR FROM ot.fecha_inicio_estimada) = 2015\n" +
//                    "      AND EXTRACT(DAY FROM ot.fecha_inicio_estimada) != EXTRACT(DAY FROM ot.fecha_inicio_real)\n" +
//                    "      OR EXTRACT(MONTH FROM ot.fecha_inicio_estimada) <= :mes\n" +
//                    "         AND usuario_id =:usuarioId\n" +
//                    "      AND EXTRACT(YEAR FROM ot.fecha_inicio_estimada) = 2015\n" +
//                    "      AND EXTRACT(DAY FROM ot.fecha_inicio_real) IS NULL\n" +
//                    "GROUP BY EXTRACT(MONTH FROM ot.fecha_inicio_estimada) LIMIT 12");
            SQLQuery sqlQuery = sesion.createSQLQuery("SELECT * from obtener_total_ots_ultimos_12meses(:month,:year,:usuario_id)");
            sqlQuery.setParameter("month",mes);
            sqlQuery.setParameter("year",ano);
            sqlQuery.setParameter("usuario_id",usuarioId);

            listadoCantidad = sqlQuery.list();
            listadoFinal = prepararDataGrafico(listadoCantidad);

        }catch(Exception e){
            log.error("Error al obtener data para retornarApFechaInicioRealVsFechaInicio",e);
        }
        return listadoFinal;
    }

    private List<MesCantidadAux> prepararDataGrafico(List<Object[]> listadoCantidad) {
        List<MesCantidadAux> listado = new ArrayList<>();
        listado.add(new MesCantidadAux(1,0));
        listado.add(new MesCantidadAux(2,0));
        listado.add(new MesCantidadAux(3,0));
        listado.add(new MesCantidadAux(4,0));
        listado.add(new MesCantidadAux(5,0));
        listado.add(new MesCantidadAux(6,0));
        listado.add(new MesCantidadAux(7,0));
        listado.add(new MesCantidadAux(8,0));
        listado.add(new MesCantidadAux(9,0));
        listado.add(new MesCantidadAux(10,0));
        listado.add(new MesCantidadAux(11,0));
        listado.add(new MesCantidadAux(12,0));

        for(Object[] obj: listadoCantidad){
            int mes = (Integer) obj[0];
            int cantidad = (Integer) obj[1];
            listado = buscarMesColocarCantidad(listado,mes,cantidad);
        }

        return listado;
    }

    private List<MesCantidadAux> buscarMesColocarCantidad(List<MesCantidadAux> listado, int mes, int cantidad) {

        List<MesCantidadAux> listadoResultado = new ArrayList<>();

        for(MesCantidadAux mesCantidad: listado){
            int comparaMes = Double.compare(mesCantidad.getMes(), mes);
            if(comparaMes == 0){
                mesCantidad.setCantidad(cantidad);
            }
            listadoResultado.add(mesCantidad);
        }

        return listadoResultado;
    }

    protected void abrirSesion() throws Exception {
        try {
            SessionFactory sesionFactory = NewHibernateUtil.getSessionFactory();
            this.sesion = sesionFactory.openSession();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    protected void cerrarSesion() {
        this.sesion.close();
    }

    protected void rollback() {
        log.info("********* Realizando ROLLBACK *********");
        this.trx.rollback();
    }

    protected void commit() {
        log.info("********* Realizando COMMIT *********");
        this.trx.commit();
    }
    
    protected void flush() {
        log.info("********* Realizando FLUSH *********");
        this.sesion.flush();
    }

    protected void iniciarTransaccion() {
        this.trx = sesion.beginTransaction();
    }
    
    
    private Session sesion;
    private Transaction trx;
    private static final Logger log = Logger.getLogger(GatewayDashboard.class);
    
}
