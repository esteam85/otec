<%--
    Document   : SubirImagen
    Created on : 29-sep-2015, 13:24:16
    Author     : rrr
--%>

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
    <head>
        <title>Struts 2 download</title>
    </head>

    <body>
        <h2>Struts 2 download</h2>
        <s:actionerror />
        <s:url id="descargar" action="descargarArchivo?usuarioId=2&esDescargaExcel=1&nombreArchivo=ejemplo.xlsx" />

        <h2><s:a href="%{descargar}">Descargar Archivo</s:a></h2>

    </body>
</html>