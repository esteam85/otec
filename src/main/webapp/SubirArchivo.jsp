<%-- 
    Document   : SubirImagen
    Created on : 29-sep-2015, 13:24:16
    Author     : mcj
--%>

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Upload User Image</title>
</head>
 
<body>
<h2>Struts2 File Upload & Save Example</h2>
<s:actionerror />
<s:form action="informarAvanceOt" method="post" enctype="multipart/form-data">
    <input type="text" name="parametros" value='{"ot":{"id":35},"observaciones":"registro de prueba","fechaCreacion":"15-10-2015"}'>
    <input type="text" name="objCoreOT" value="LibroObra">
    <s:textfield name="usuarioId" value="40" label="id de usuario" />
    <s:textfield name="otId" label="OT" />
    <s:textfield name="roles" label="Rol 1" />
    <s:textfield name="roles" label="Rol 2" />
    <s:textfield name="observaciones" label="Observaciones" />
    <s:file label="Archivo 1" name="archivo" size="40" />
    <%--<s:textfield name="roles[1]" label="Rol 2" value="33"/>--%>
    <%--<s:file label="Archivo 2" name="archivo" size="40" />--%>
    <%--<s:file label="Archivo 3" name="archivo" size="40" />  --%>
    <%--<s:file label="Archivo 4" name="archivo" size="40" />--%>
    <s:submit value="Subir" align="center" />
</s:form>

</body>
</html>