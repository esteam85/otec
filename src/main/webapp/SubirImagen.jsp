<%--
    Document   : SubirImagen
    Created on : 29-sep-2015, 13:24:16
    Author     : mcj
--%>

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Upload User Image</title>
</head>

<body>
<h2>Struts2 File Upload & Save Example</h2>
<s:actionerror />
<s:form action="enviarImagen" method="post" enctype="multipart/form-data">
    <s:param name="nombre" value="avatar-181569832.pdf"/>
    <s:file name="image" label="User Image" />
    <s:submit value="Upload" align="center" />
</s:form>
<br/><br/><br/><br/>
<img src=" <s:url action='obtenerImagen?imageId=181569824.jpg' />"  />
<br/><br/><br/><br/>

</body>
</html>