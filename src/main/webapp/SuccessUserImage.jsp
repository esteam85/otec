<%-- 
    Document   : SuccessUserImage
    Created on : 29-sep-2015, 13:30:51
    Author     : mcj
--%>

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Success: Upload User Image</title>
</head>
<body>
    <h2>Struts2 File Upload Example</h2>
    User Image: <s:property value="imagen"/>
    <br/>
    Content Type: <s:property value="imageContentType"/>
    <br/>
    File Name: <s:property value="imageFileName"/>
    <br/>
    Uploaded Image:
    <br/>
    <img src="<s:property value="imageFileName"/>"/>
    <br/><br/>
    
</body>
</html>
