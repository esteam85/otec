/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.Agencias;
import com.skydream.coreot.pojos.Contratos;
import com.skydream.coreot.pojos.Eventos;
import com.skydream.coreot.pojos.MensajeEvento;
import com.skydream.coreot.pojos.ObjetoCoreOT;
import com.skydream.coreot.pojos.Opciones;
import com.skydream.coreot.pojos.OpcionesRoles;
import com.skydream.coreot.pojos.Perfiles;
import com.skydream.coreot.pojos.Proveedores;
import com.skydream.coreot.pojos.ProveedoresServicios;
import com.skydream.coreot.pojos.Roles;
import com.skydream.coreot.pojos.Servicios;
import com.skydream.coreot.pojos.TipoEvento;
import com.skydream.coreot.pojos.TipoProveedor;
import com.skydream.coreot.pojos.TipoServicio;
import com.skydream.coreot.pojos.Usuarios;
import com.skydream.coreot.pojos.Workflow;
import com.skydream.coreot.util.NewHibernateUtil;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mcj
 */
public class Gateway {
    
    protected void abrirSesion() throws Exception{
        try {
            SessionFactory sesionFactory = NewHibernateUtil.getSessionFactory();
            this.sesion = sesionFactory.openSession();
        } catch (Exception e) {
            System.out.println("No se pudo iniciar sesión. " + e.getStackTrace().toString());
            throw e;
        }
    }
    
    protected void cerrarSesion(){
        this.sesion.close();
    }
    
    protected void rollback(){
        this.trx.rollback();
    }
    
    protected void commit(){
        this.trx.commit();
    }
    
    protected void iniciarTransaccion(){
        this.trx = sesion.beginTransaction();
    }
    
    protected Integer registrar(ObjetoCoreOT obj) throws Exception {
        int registro = 0;
        try {
            registro = (Integer)sesion.save(obj);
        } catch (HibernateException e) {
            System.out.println("No se pudo crear el objeto " + obj.getClass().toString());
            System.out.println(e);
            throw e;
        }
        return registro;
    }

    protected List<ObjetoCoreOT> listarGenerico(ObjetoCoreOT obj, List<String> joinObjCoreOT) throws Exception {
        List<ObjetoCoreOT> lista = new ArrayList();
        try {
            Criteria criteria = sesion.createCriteria(obj.getClass());
            if (joinObjCoreOT != null) {
                for (String join : joinObjCoreOT) {
                    criteria.setFetchMode(join, FetchMode.JOIN);
                }
            }
            lista = criteria.list();
            if(lista!=null){
                for(ObjetoCoreOT objeto : lista){
                    validarYSetearObjetoARetornar(objeto, joinObjCoreOT);
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return lista;
    }
    
    protected List<ObjetoCoreOT> listar(ObjetoCoreOT obj, List<String> joinObjCoreOT) throws Exception {
        try {
            Criteria criteria = sesion.createCriteria(obj.getClass());
            for (String join : joinObjCoreOT) {
                criteria.setFetchMode(join, FetchMode.JOIN);
            }
            List<ObjetoCoreOT> lista = criteria.list();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
    protected ObjetoCoreOT obtenerDatoPorId(ObjetoCoreOT obj, String[] joinObjCoreOT) throws Exception {
        ObjetoCoreOT objetoRetorno = new Servicios();
        Class claseObj = obj.getClass();
        try {
            Criteria criteria = sesion.createCriteria(obj.getClass());
            for(String join : joinObjCoreOT){
                criteria.setFetchMode(join, FetchMode.JOIN);
            }
            Method method = claseObj.getMethod("getId");
            Integer id = (Integer)method.invoke(obj);
            criteria.add(Restrictions.eq("id", id));
            objetoRetorno = (ObjetoCoreOT)criteria.uniqueResult();
            if (objetoRetorno != null) {
                validarYSetearObjetoARetornar(objetoRetorno, null);
            }
        } catch (Exception e) {
            throw e;
        }
        return objetoRetorno;
    }
    
    protected boolean actualizar(ObjetoCoreOT objetoCoreOT) throws Exception {
        boolean actualizar = false;
        try {
            Class claseObj = objetoCoreOT.getClass();
            Method method = claseObj.getMethod("getId");
            Integer id = (Integer)method.invoke(objetoCoreOT);
            ObjetoCoreOT registroActual = (ObjetoCoreOT) sesion.get(claseObj, id);
            if (registroActual != null) {
                validarYSetearObjetoAPersistir(objetoCoreOT, registroActual);
                sesion.update(registroActual);
                actualizar = true;
            }else{
                throw new Exception("No se encontró el registro a actualizar");
            }
        } catch (Exception e) {
            System.out.println("No se pudo actualizar el objeto " + objetoCoreOT.getClass().toString());
            throw e;
        }
        return actualizar;
    }
    
    protected boolean actualizar2(ObjetoCoreOT objetoCoreOT) throws Exception {
        try {
            sesion.update(objetoCoreOT);
        } catch (Exception e) {
            System.out.println("No se pudo actualizar el objeto " + objetoCoreOT.getClass().toString());
            throw e;
        }
        return true;
    }
    
    private void validarYSetearObjetoAPersistir(ObjetoCoreOT objetoEnviado, ObjetoCoreOT objetoBBDD) throws IllegalAccessException {
        String nombreClaseObjeto = objetoBBDD.getClass().getName();
        System.out.println("Clase : " + nombreClaseObjeto);
        Field[] atributos = objetoEnviado.getClass().getDeclaredFields();
        
        for (Field campo : atributos) {
            String nombreCampo = campo.getName();
            System.out.println("Nombre atributo : " + nombreCampo);
            Class claseCampo = campo.getType();

            if (!Set.class.isAssignableFrom(claseCampo)) {
                campo.setAccessible(true);
                Object valor = campo.get(objetoEnviado);
                campo.set(objetoBBDD, valor);
            }
        }
    }

    protected List<ObjetoCoreOT> listarConFiltro(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        List<ObjetoCoreOT> lista = new ArrayList();
        try {
            Criteria criteria = sesion.createCriteria(obj.getClass());
            criteria.setFetchMode(filtro, FetchMode.JOIN);
            criteria.add(Restrictions.eq(filtro, valorFiltro));
            lista = criteria.list();
            for(ObjetoCoreOT objeto : lista){
                validarYSetearObjetoARetornar(objeto, null);   
            }
        } catch (HibernateException e) {
            throw e;
        }
        return lista;
    }
    
    protected List<Perfiles> listarRolesPorPerfil() throws Exception {
        List<Perfiles> listaRetorno = new ArrayList<>();
        try {
            Criteria criteria = sesion.createCriteria(Roles.class);
            criteria.setProjection(Projections.projectionList()
                    .add(Projections.groupProperty("perfiles"))
            );
            List<Perfiles> lista = criteria.list();
            for(Perfiles p: lista){
                Perfiles perfil = new Perfiles(p.getId(),null,p.getNombre(),p.getDescripcion(),p.getEstado(),null,null);
                perfil.setId(p.getId());
                perfil.setNombre(p.getNombre());
                perfil.setEstado(p.getEstado());
                perfil.setDescripcion(p.getDescripcion());
                Set<Roles> roles = new HashSet<>(p.getRoleses());
                for(Roles r : roles){
                    r.setPerfiles(null);
                    r.setWorkflowEventosRoleses(null);
                }
                perfil.setRoleses(roles);
                listaRetorno.add(perfil);
            }
        } catch (Exception e) {
            throw e;
        }
        return listaRetorno;
    }
    
    
    protected List<Roles> listarRoles() throws Exception{
        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        List<Roles> listaRetorno = null;
        try {
            Criteria crit = session.createCriteria(Roles.class);
            crit.setFetchMode("perfiles", FetchMode.JOIN);
            listaRetorno = crit.list();
            for(Roles obj : listaRetorno){
                validarYSetearObjetoARetornar(obj, null);
            }
        } catch (HibernateException e) {
            System.out.println(e);
            throw e;
        } finally {
            session.close();
        }
        return listaRetorno;
    }
    
    protected void validarYSetearObjetoARetornar(ObjetoCoreOT objCoreOT, List<String> camposJoin){
        String nombreClaseObjeto = objCoreOT.getClass().getName();
        System.out.println("Clase : " + nombreClaseObjeto);
        Field[] atributos = objCoreOT.getClass().getDeclaredFields();

        try {
            for (Field campo : atributos) {
                System.out.println("Nombre atributo : " + campo.getName());
                Class claseCampo = campo.getType();
                campo.setAccessible(true);
                boolean isSet = Set.class.isAssignableFrom(claseCampo);
                boolean isJoinField = camposJoin!=null?camposJoin.contains(campo.getName()):false;
                if (isSet && !isJoinField) {
                        campo.set(objCoreOT, null);
                } else if (ObjetoCoreOT.class.isAssignableFrom(claseCampo)) {
                    ObjetoCoreOT attrObjCoreOT = (ObjetoCoreOT)campo.get(objCoreOT);
                    ObjetoCoreOT nuevoAttrObjCoreOT = setearYRetornarAtributoObjCoreOT(attrObjCoreOT, claseCampo);
                    campo.set(objCoreOT, nuevoAttrObjCoreOT);
                }else if(isSet && isJoinField){
                    Set setObjetos = (Set)campo.get(objCoreOT);
                    for(Object objSet : setObjetos){
                        ObjetoCoreOT attrObjCoreOT = (ObjetoCoreOT)objSet;
                        validarYSetearObjetoARetornar(attrObjCoreOT, null);
                    }
                }
            }
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | InstantiationException ex) {
            System.out.println(ex);
        }
    }
    
    private ObjetoCoreOT setearYRetornarAtributoObjCoreOT(ObjetoCoreOT objCoreOT, Class claseTipoObjeto) throws NoSuchMethodException, InvocationTargetException, SecurityException, IllegalArgumentException, IllegalAccessException, InstantiationException{
        String nombreClaseObjeto = claseTipoObjeto.toString();
        System.out.println("Clase : " + nombreClaseObjeto);
        Field[] atributos = claseTipoObjeto.getDeclaredFields();
        
        Map<String, Method> mapaGets = retornarMapaGet(claseTipoObjeto.getMethods(), atributos);
        try {
            Constructor constructor = claseTipoObjeto.getConstructor();
            ObjetoCoreOT objetoRetorno = (ObjetoCoreOT) constructor.newInstance();
            for (Field campo : atributos) {
                System.out.println("Nombre atributo : " + campo.getName());
                Class claseCampo = campo.getType();
                Method metodo = mapaGets.get(campo.getName());
                campo.setAccessible(true);
                if (Set.class.isAssignableFrom(claseCampo) || ObjetoCoreOT.class.isAssignableFrom(claseCampo)) {
                    campo.set(objetoRetorno, null);
                }else{
                    Object objetoAux = metodo.invoke(objCoreOT);
                    campo.set(objetoRetorno, objetoAux);
                }
            }
            return objetoRetorno;
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | InstantiationException ex) {
            System.out.println("Error al setear nuevo atributo de objeto" + ex);
            throw ex;
        }
    }
    
    private Map<String, Method> retornarMapaGet(Method[] metodos, Field[] campos) {
        Map<String, Method> mapaRetorno = new HashMap<>();
        for (Method metodo : metodos) {
            String nombreMetodo = metodo.getName();
            if (isGetter(metodo)) {
                for(Field obj : campos){
                    int largoEsperadoCampoGet = "get".length() + obj.getName().length();
                    boolean contieneCampo = nombreMetodo.toLowerCase().contains(obj.getName().toLowerCase());
                    if(contieneCampo && (nombreMetodo.length()==largoEsperadoCampoGet)){
                        mapaRetorno.put(obj.getName(), metodo);
                        break;
                    }
                }
            }
        }
        return mapaRetorno;
    }
    
    private static boolean isGetter(Method method){
        if(!method.getName().startsWith("get"))return false;
        if(method.getParameterTypes().length != 0)return false;  
        if(void.class.equals(method.getReturnType()))return false;
        return true;
}


    protected List<ObjetoCoreOT> listar2(ObjetoCoreOT obj) throws Exception {

        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        String hql = retornarConsultaListarTodos(obj);
        Session session = sesion.openSession();
        List<ObjetoCoreOT> listaRetorno;
        try {
            Query query = session.createQuery(hql);
            listaRetorno = query.list();
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        } finally {
            session.close();
        }
        return listaRetorno;
    }

    protected List<Perfiles> listarPerfiles() throws Exception{
        try {
            Criteria criteria = sesion.createCriteria(Perfiles.class)
                                        .setFetchMode("proveedores", FetchMode.JOIN);
            List<Perfiles> lista = criteria.list();
            if (lista != null) {
                for (Perfiles p : lista) {
                    validarYSetearObjetoARetornar(p, null);
                }
            } else throw new Exception("Busqueda sin resultados");
            
            return lista;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

    protected List<Usuarios> listarUsuarios() throws Exception{
        try {
            String hql = "SELECT NEW Usuarios(id, nombreUsuario, rut, dv, nombres, apellidos, email, celular, estado) "
                                                                                                    + "FROM Usuarios";
            Query query = sesion.createQuery(hql);
            List<Usuarios> listaRetorno = query.list();
            return listaRetorno;
        } catch (Exception e) {
            System.out.println("No se pudo obtener listado de usuarios." + e);
            throw e;
        }
    }

//    protected List<TipoProveedor> listarTiposProveedor() {
//
//        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
//        Session session = sesion.openSession();
//        try {
//            Criteria criteria = session.createCriteria(TipoProveedor.class);
//            List<TipoProveedor> listaRetorno = criteria.list();
//            return listaRetorno;
//        } catch (Exception e) {
//            System.out.println(e);
//            throw e;
//        } finally {
//            session.close();
//        }
//    }

    protected List<Proveedores> listarProveedores() throws Exception{
        try {
            Criteria criteria = sesion.createCriteria(ProveedoresServicios.class)
                                .setFetchMode("tipoProveedor", FetchMode.JOIN);
            List<Proveedores> lista = criteria.list();
            return lista;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }

//    protected List<Workflow> listarWorkflow() {
//
//        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
//        Session session = sesion.openSession();
//        try {
//            Criteria criteria = session.createCriteria(Workflow.class);
//            List<Workflow> lista = criteria.list();
//            return lista;
//        } catch (Exception e) {
//            System.out.println(e);
//            throw e;
//        } finally {
//            session.close();
//        }
//    }

//    protected List<Contratos> listarContratos() {
//
//        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
//        Session session = sesion.openSession();
//        try {
//            Criteria criteria = session.createCriteria(Contratos.class);
//            List<Contratos> lista = criteria.list();
//            for (Contratos contrato : lista) {
//                Workflow workflow = new Workflow();
//                workflow.setId(contrato.getWorkflow().getId());
//                contrato.setWorkflow(workflow);
//                contrato.setContratosProveedoreses(null);
//                contrato.setOts(null);
//                contrato.setUsuariosPerfileses(null);
//            }
//            return lista;
//        } catch (Exception e) {
//            System.out.println(e);
//            throw e;
//        } finally {
//            session.close();
//        }
//    }

    protected List<TipoEvento> listarTiposEvento() {

        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        try {
            Criteria criteria = session.createCriteria(TipoEvento.class);
            List<TipoEvento> listaRetorno = criteria.list();
            return listaRetorno;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        } finally {
            session.close();
        }
    }
    
//    protected List<TipoServicio> listarTiposServicio() {
//
//        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
//        Session session = sesion.openSession();
//        try {
//            Criteria criteria = session.createCriteria(TipoServicio.class);
//            List<TipoServicio> listaRetorno = criteria.list();
//            return listaRetorno;
//        } catch (Exception e) {
//            System.out.println(e);
//            throw e;
//        } finally {
//            session.close();
//        }
//    }  
    
    protected List<Servicios> listarServicios() {

        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        try {
            Criteria criteria = session.createCriteria(Servicios.class);
            List<Servicios> lista = criteria.list();
            for (Servicios servicio : lista) {
                TipoServicio tipoServicio = new TipoServicio();
                tipoServicio.setId(servicio.getTipoServicio().getId());
                servicio.setTipoServicio(tipoServicio);
            }
            return lista;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        } finally {
            session.close();
        }
    }    

    protected List<Eventos> listarEventos() {
        try {
            Criteria criteria = sesion.createCriteria(Eventos.class);
            List<Eventos> lista = criteria.list();
            for (Eventos evento : lista) {
                MensajeEvento msjeEvento = new MensajeEvento();
                msjeEvento.setId(evento.getMensajeEvento().getId());
                evento.setMensajeEvento(msjeEvento);
                TipoEvento tipoEvento = new TipoEvento();
                tipoEvento.setId(evento.getTipoEvento().getId());
                evento.setTipoEvento(tipoEvento);
            }
            return lista;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        }
    }
    
    protected List<Opciones> listarOpciones() throws Exception {
        List<Opciones> listaRetorno = null;
        try {
            Criteria crit = sesion.createCriteria(Opciones.class);
            listaRetorno = crit.list();
            for(Opciones obj : listaRetorno){
                validarYSetearObjetoARetornar(obj, null);
            }
        } catch (HibernateException e) {
            System.out.println(e);
            throw e;
        }
        return listaRetorno;
    }
    
    protected List<OpcionesRoles> listarOpcionesRoles() throws Exception {
        List<OpcionesRoles> listaRetorno = null;
        try {
            Criteria crit = sesion.createCriteria(OpcionesRoles.class);
            listaRetorno = crit.list();
            for(OpcionesRoles obj : listaRetorno){
                validarYSetearObjetoARetornar(obj, null);
            }
        } catch (HibernateException e) {
            System.out.println(e);
            throw e;
        }
        return listaRetorno;
    }

    private String retornarConsultaListarTodos(ObjetoCoreOT objCoreOT) {

        String nombreClaseObjeto = objCoreOT.getClass().getName();
        String cadena1 = "SELECT ";
        String alias = "objCoreOT";
        String cadena2 = "FROM " + nombreClaseObjeto + " as " + alias;

        Field[] atributos = objCoreOT.getClass().getDeclaredFields();
        int contador = 0;
        for (Field campo : atributos) {
            Class claseCampo = campo.getType();

            if (!Set.class.isAssignableFrom(claseCampo)) {
                if (contador > 0) {
                    cadena1 = cadena1 + ",";
                }
                System.out.println("nombreCampo : " + campo.getName());
                String nombreCampo = alias + "." + campo.getName();
                if (ObjetoCoreOT.class.isAssignableFrom(claseCampo)) {
                    nombreCampo = nombreCampo + ".id";
                    cadena2 = cadena2 + " LEFT JOIN FETCH " + alias + "." + campo.getName();
//                System.out.println("El atributo " + campo.getName() + " es un objetoRetorno de tipo ObjetoCoreOT !");
                }
                cadena1 = cadena1 + nombreCampo;
            }
            contador++;
        }
        String cadenaRetorno = cadena2;
        System.out.println(cadenaRetorno);

        return cadenaRetorno;
    }
    
    protected List<Agencias> listarAgencias() {

        SessionFactory sesion = NewHibernateUtil.getSessionFactory();
        Session session = sesion.openSession();
        try {
            Criteria criteria = session.createCriteria(Agencias.class);
            List<Agencias> listaRetorno = criteria.list();
            return listaRetorno;
        } catch (Exception e) {
            System.out.println(e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    
    private Session sesion;
    private Transaction trx;
}
