/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skydream.coreot.dao;

import com.skydream.coreot.pojos.*;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mcj
 */
public class GenericDAO implements GatewayDAO{
    
    protected List<ObjetoCoreOT> listarObjCoreOT(ObjetoCoreOT obj, Map params) throws Exception {
        Gateway gP = new Gateway();
        return gP.listarGenerico(obj, null);
    }
    
    protected List<Usuarios> listarUsuarios(Map datosSesion)throws Exception{
        Gateway gate = new Gateway();
        try{
            gate.abrirSesion();
            return gate.listarUsuarios();
        }finally{
            gate.cerrarSesion();
        }
    }

    protected boolean actualizar(ObjetoCoreOT obj) throws Exception {
        Gateway gP = new Gateway();
        try {
            gP.abrirSesion();
            gP.iniciarTransaccion();
            try {
                gP.actualizar(obj);
                gP.commit();
            } catch (Exception e) {
                gP.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        }finally{
            gP.cerrarSesion();
        }
    }

    protected List<ObjetoCoreOT> listarConFiltro(ObjetoCoreOT obj, String filtro, int valorFiltro) throws Exception {
        Gateway gate = new Gateway();
        return gate.listarConFiltro(obj, filtro, valorFiltro);
    }
    
    protected boolean eliminarGenerico(){
        return false;
    }
    
    public static GenericDAO getINSTANCE(){
        return INSTANCE;
    }
    
    private static final GenericDAO INSTANCE = new GenericDAO();

    @Override
    public boolean crear(ObjetoCoreOT obj, Map params) throws Exception {
        Gateway gateway = new Gateway();
        try {
            gateway.abrirSesion();
            gateway.iniciarTransaccion();
            try {
                gateway.registrar(obj);
                gateway.commit();
            } catch (Exception e) {
                gateway.rollback();
                throw e;
            }
            return true;
        } catch (Exception e) {
            throw e;
        }finally{
            gateway.cerrarSesion();
        }
    }

    @Override
    public boolean actualizar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listar(ObjetoCoreOT obj, Map params) throws Exception {
        Gateway gate = new Gateway();
        try {
            gate.abrirSesion();
            return gate.listarGenerico(obj, null);
        } catch (Exception e) {
            throw e;
        } finally {
            gate.cerrarSesion();
        }
    }

    @Override
    public boolean eliminar(ObjetoCoreOT obj, Map params) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
