
import com.lambdaworks.redis.*;
import com.skydream.coreot.dao.*;
import com.skydream.coreot.pojos.*;
import com.skydream.coreot.util.RenderPDF;
import com.skydream.eventmanager.Facade;
import com.skydream.eventmanager.GatewayAcciones;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.xssf.usermodel.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Test;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */

//import com.skydream.coreot.CoreOtFacade;

/**
 *
 * @author christian
 */
public class TestCoreOT {

    public TestCoreOT() {
    }

//    @Test
//    public void deberiaRegistrarUsuario() {
//
//        Usuarios usuario = new Usuarios();
//        boolean registro = false;
//        try {
//
//            usuario.setApellidos("Oliva Henriquez");
//            usuario.setCelular("97590320");
//            usuario.setEmail("francisca.oliva@skydream.cl");
//            char estado = 'T';
//            usuario.setEstado(estado);
//            usuario.setNombres("Christian");
//            usuario.setClave("dsfjgsd");
//            Integer rut = 16017623;
//            usuario.setRut(rut);
//            Character dv = '7';
//            usuario.setDv(dv);
//            usuario.setNombreUsuario("lafran");
//
//            registro = facade.registrar(usuario);
//
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertTrue("No se pudo registrar el Usuario" , registro);
//    }
//
//
//    @Test
//    public void deberiaRegistrarPerfil() {
//        Perfiles perfil = new Perfiles();
//        perfil.setNombre("Perfil ZX");
//        perfil.setDescripcion("Perfil de pruebas");
//        perfil.setEstado('A');
//        perfil.setProveedores(null);
//        boolean registro = false;
//        try{
//            registro = facade.registrar(perfil);
//        }catch(Exception e){
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, e);
//        }
//        assertTrue("No se pudo registrar el Usuario" , registro);
//    }
//
//    @Test
//    public void deberiaRegistrarContrato() {
//
//    }
//
//    @Test
//    public void deberiaRegistrarUsuarioPerfilContrato() {
//
//    }
//
//    @Test
//    public void deberiaRegistrarRol() {
//
//    }
//
//    @Test
//    public void deberiaRegistrarWorkFlow() {
//
//    }
//
//    @Test
//    public void deberiaCrearTipoEvento() {
//
//        boolean registro = false;
//        TipoEvento tipoEvento = new TipoEvento();
//        tipoEvento.setNombre("Evento desicion");
//        tipoEvento.setDescripcion("Evento........");
//
//        try {
//            registro = facade.registrar(tipoEvento);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertTrue("No se pudo registrar el Tipo Evento" , registro);
//    }
//
//    @Test
//    public void deberiaListarTipoEvento() {
//
//        TipoEvento tipoEvento = new TipoEvento();
//        List<ObjetoCoreOT> list = new ArrayList();
//        try {
//            list = facade.listarGenerico(tipoEvento);
//            tipoEvento = (TipoEvento)list.get(0);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertNotNull("No se pudo obtener el listado de eventos" , tipoEvento);
//    }
//
//
//    @Test
//    public void deberiaActualizarTipoEvento() {
//        boolean actualizar = false;
//        TipoEvento tipoEvento = new TipoEvento();
//        tipoEvento.setId(2);
//        tipoEvento.setDescripcion("descripcionnnnn");
//        tipoEvento.setNombre("Evento 1");
//
//         try {
//            actualizar = facade.actualizar(tipoEvento);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertTrue("No se pudo registrar el Tipo Evento" , actualizar);
//    }
//
//    @Test
//    public void deberiaCrearMensajeEvento() {
//
//        boolean registro = false;
//        MensajeEvento mensajeEvento = new MensajeEvento();
//        mensajeEvento.setMensajeNotificacion("Se ha generado una nueva OT");
//
//        try {
//            registro = facade.registrar(mensajeEvento);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertTrue("No se pudo registrar el Tipo Evento" , registro);
//    }
//
//    @Test
//    public void deberiaListarMensajeEvento() {
//
//        MensajeEvento mensajeEvento = new MensajeEvento();
//        List<ObjetoCoreOT> list = new ArrayList();
//        try {
//            list = facade.listarGenerico(mensajeEvento);
//            mensajeEvento = (MensajeEvento)list.get(0);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertNotNull("No se pudo obtener el listado de eventos" , mensajeEvento);
//    }
//
//    @Test
//    public void deberiaActualizarMensajeEvento() {
//        boolean actualizar = false;
//        MensajeEvento mensajeEvento = new MensajeEvento();
//        mensajeEvento.setId(2);
//        mensajeEvento.setMensajeNotificacion("lalalallalala");
//
//         try {
//            actualizar = facade.actualizar(mensajeEvento);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertTrue("No se pudo registrar el Tipo Evento" , actualizar);
//    }
//
//    @Test
//    public void deberiaCrearEvento() {
//
//        boolean registro = false;
//        Eventos evento = new Eventos();
//        evento.setDuracion(45);
//        MensajeEvento mensajeEvento = new MensajeEvento();
//        mensajeEvento.setId(2);
//        evento.setMensajeEvento(mensajeEvento);
//        evento.setNombre("evento prueba 1");
//        TipoEvento tipoEvento = new TipoEvento();
//        tipoEvento.setId(2);
//        evento.setTipoEvento(tipoEvento);
//
//        try {
//            registro = facade.registrar(evento);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertTrue("No se pudo registrar el Tipo Evento" , registro);
//    }
//
//    @Test
//    public void deberiaListarEventos() {
//
//        Eventos evento = new Eventos();
//        List<ObjetoCoreOT> list = new ArrayList();
//        try {
//            list = facade.listarGenerico(evento);
//            evento = (Eventos)list.get(0);
//            MensajeEvento mensajeEvento = new MensajeEvento();
//            mensajeEvento = (MensajeEvento) evento.getMensajeEvento();
//            String mensaje = mensajeEvento.getMensajeNotificacion();
//
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertNotNull("No se pudo obtener el listado de eventos" , evento);
//    }
//
//    @Test
//    public void deberiaListarEventosPorTipo() {
//
//        Eventos evento = new Eventos();
//        List<Object> list = new ArrayList();
//
//        try {
//            /*
//                Los parametros que se deben enviar son:
//                1.- El objeto que se desea listar.
//                2.- El objeto con el cual se desea filtrar seguido del campo especifico
//                3.- El valor del filtro
//            */
//
//            String filtro = "tipoEvento.id";
//            int valorFiltro = 12;
//
//            list = facade.listarConFiltro(evento, filtro, valorFiltro);
//            evento = (Eventos)list.get(0);
//            MensajeEvento mensajeEvento = new MensajeEvento();
//            mensajeEvento = (MensajeEvento) evento.getMensajeEvento();
//            String mensaje = mensajeEvento.getMensajeNotificacion();
//            int id = mensajeEvento.getId();
//
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertNotNull("No se pudo obtener el listado de eventos" , evento);
//    }
//
//
//    @Test
//    public void deberiaActualizarEvento() {
//        boolean actualizar = false;
//        Eventos evento = new Eventos();
//        evento.setId(12);
//        evento.setDuracion(20);
//        MensajeEvento mensajeEvento = new MensajeEvento();
//        mensajeEvento.setId(2);
//        evento.setMensajeEvento(mensajeEvento);
//        evento.setNombre("evento 333333");
//        TipoEvento tipoEvento = new TipoEvento();
//        tipoEvento.setId(2);
//        evento.setTipoEvento(tipoEvento);
//
//         try {
//            actualizar = facade.actualizar(evento);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertTrue("No se pudo registrar el Tipo Evento" , actualizar);
//    }
//
//
//    @Test
//    public void deberiaCrearTipoAccion() {
//
//        boolean registro = false;
//        TipoAccion tipoAccion = new TipoAccion();
//        tipoAccion.setNombre("TIPO Accion Z");
//        tipoAccion.setDescripcion(":::: registro de prueba ::::");
//        tipoAccion.setConfigurable(1);
//
//        try {
//            registro = facade.registrar(tipoAccion);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertTrue("No se pudo registrar el Tipo de Accion" , registro);
//    }
//
//    @Test
//    public void deberiaListarTipoAcciones() {
//
//        TipoAccion tipoAccion = new TipoAccion();
//        List<ObjetoCoreOT> list = new ArrayList();
//        try {
//            list = facade.listarGenerico(tipoAccion);
//            tipoAccion = (TipoAccion)list.get(0);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertNotNull("No se pudo obtener el listado de eventos" , tipoAccion);
//    }
//
//    @Test
//    public void deberiaCrearAcciones() {
//
//        boolean registro = false;
//        Acciones acciones = new Acciones();
//        acciones.setNombre("Accion de prueba1111");
//        TipoAccion tipoAccion = new TipoAccion();
//        tipoAccion.setId(2);
//        acciones.setTipoAccion(tipoAccion);
//        try {
//            registro = facade.registrar(acciones);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertTrue("No se pudo registrar el Tipo Evento" , registro);
//    }
//
//    @Test
//    public void deberiaCrearEventosAcciones() {
//
//        boolean registro = false;
//        EventosAcciones eventosAcciones = new EventosAcciones();
//
//        Acciones acciones = new Acciones();
//        acciones.setId(2);
//        eventosAcciones.setAcciones(acciones);
//        Eventos evento = new Eventos();
//        evento.setId(2);
//        eventosAcciones.setEventos(evento);
//
//        try {
//            registro = facade.registrar(eventosAcciones);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertTrue("No se pudo registrar el" , registro);
//    }
//
//
////    @Test
////    public void deberiaListarAccionesPorEvento() {
////
////        Acciones acciones = new Acciones();
////        List<Object> list = new ArrayList();
////
////        try {
////            /*
////                Los parametros que se deben enviar son:
////                1.- El objeto que se desea listar.
////                2.- El objeto con el cual se desea filtrar seguido del campo especifico
////                3.- El valor del filtro
////            */
////
////            String filtro = "tipoEvento.id";
////            int valorFiltro = 12;
////
////            list = facade.listarConFiltro(evento, filtro, valorFiltro);
////            evento = (Eventos)list.get(0);
////            MensajeEvento mensajeEvento = new MensajeEvento();
////            mensajeEvento = (MensajeEvento) evento.getMensajeEvento();
////            String mensaje = mensajeEvento.getMensajeNotificacion();
////            int id = mensajeEvento.getId();
////
////        } catch (Exception ex) {
////            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
////        }
////
////        assertNotNull("No se pudo obtener el listado de eventos" , evento);
////    }
//
////26.-
////27.-
////28.-	listarAccionesPorTipo
//
////    @Test
////    public void deberiaRegistrarEvento() {
////
////    }
////
////     @Test
////    public void deberiaRegistrarTipoAccion() {
////
////    }
////
////    @Test
////    public void deberiaRegistrarAccion() {
////
////    }
////
////    @Test
////    public void deberiaRegistrarEventoAccion() {
////
////    }
////
////    @Test
////    public void deberiaRegistrarTipoParametro() {
////
////    }
////
////    @Test
////    public void deberiaRegistrarParametro() {
////
////    }
////
////    @Test
////    public void deberiaRegistrarAccionParametro() {
////
////    }
////
////    @Test
////    public void deberiaRegistrarContratoProveedor() {
////
////    }
////
//    @Test
//    public void deberiaRegistrarTipoProveedor() {
//        
//        TipoProveedor tipoProveedor = new TipoProveedor();
//        tipoProveedor.setNombre("telefonica");
//        tipoProveedor.setDescripcion("lalallalala");
//        boolean registro = false;
//        try {
//            registro = facade.registrar(tipoProveedor);
//        } catch (Exception e) {
//        }
//        
//        assertTrue("No se pudo registrar el tipo de proveedor" , registro);
//
//    }
////
////    @Test
////    public void deberiaRegistrarProveedor() {
////
////    }
////
////    @Test
////    public void deberiaRegistrarProveedorServicio() {
////
////    }
////
////    @Test
////    public void deberiaRegistrarTipoServicio() {
////
////    }
////
////    @Test
////    public void deberiaRegistrarServicio() {
////
////    }

//    @Test
//    public void deberiaListarAcciones() {
//
//        String registros = "";
//        try {
//            Acciones acciones = new Acciones();
//            List<ObjetoCoreOT> listaRegistros = facade.listar2(acciones);
//            System.out.println("******************** Lista de registros ********************");            
//            for (ObjetoCoreOT objeto : listaRegistros) {
//                int id = ((Acciones) objeto).getTipoAccion().getId();
//                System.out.print("ID Accion : " + ((Acciones) objeto).getId() + " | ");
//                System.out.println("idTipoAccion : " + id);
//            }
//            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
//            registros = ow.writeValueAsString(listaRegistros);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        assertTrue("No se pudo obtener listado generico", registros!=null);
//    }
    
//    @Test
//    public void deberiaListarPerfiles() {
//
//        String registros = "";
//        try {
//            Perfiles perfil = new Perfiles();
//            List<Perfiles> listaRegistros = facade.listarPerfiles();
//            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
//            registros = ow.writeValueAsString(listaRegistros);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        assertTrue("No se pudo obtener listado generico", registros!=null);
//    }
    
//    @Test
//    public void deberiaListarUsuarios() {
//
//        String registros = "";
//        try {
//            List<Usuarios> listaRegistros = facade.listarUsuarios();
//            System.out.println("******************** Lista de registros ********************");
//            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
//            registros = ow.writeValueAsString(listaRegistros);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        assertTrue("No se pudo obtener listado generico", registros != null);
//    }
    
//    @Test
//    public void deberiaListarTipoProveedores() {
//
//        String registros = "";
//        try {
//            List<TipoProveedor> listaRegistros = facade.listarTiposProveedor();
//            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
//            registros = ow.writeValueAsString(listaRegistros);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        assertTrue("No se pudo obtener listado generico", registros != null);
//    }
    
//    @Test
//    public void deberiaListarProveedores() {
//
//        String registros = "";
//        try {
//            List<Proveedores> listaRegistros = facade.listarProveedores();
//            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
//            registros = ow.writeValueAsString(listaRegistros);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        assertTrue("No se pudo obtener listado generico", registros != null);
//    }
    

//    @Test
//    public void deberiaActualizarUsuario() {
//
//        Usuarios usuario = new Usuarios();
//        boolean registro = false;
//        try {
//            usuario.setId(2);
//            usuario.setApellidos("Oliva Henriquez");
//            usuario.setCelular("68995587");
//            usuario.setEmail("francisca.oliva@skydream.cl");
//            usuario.setEstado('A');
//            usuario.setNombres("Christian");
//            usuario.setClave("dsfjgsd");
//            Integer rut = 16017623;
//            usuario.setRut(rut);
//            usuario.setDv('2');
//            usuario.setNombreUsuario("lafran");
//            registro = facade.actualizar(usuario);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        assertTrue("No se pudo registrar el Usuario" , registro);
//    }

//    @Test
//    public void deberiaListarRoles() {
//
//        List<Roles> list = new ArrayList();
//        try {
////            list = facade.listarRolesPorPerfil();
//            list = facade.listarRoles();
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        assertNotNull("No se pudo obtener el listado de eventos" , list);
//    }
    
//    @Test
//    public void deberiaActualizarPerfil() {
//        Perfiles perfil = new Perfiles();
//        perfil.setId(9);
//        perfil.setNombre("Perfil ZX");
//        perfil.setDescripcion("Perfil de pruebas");
//        perfil.setEstado('I');
//        Proveedores proveedor = new Proveedores();
//        proveedor.setId(3);
//        perfil.setProveedores(null);
//        boolean registro = false;
//        try{
//            registro = facade.actualizar(perfil);
//        }catch(Exception e){
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, e);
//        }
//        assertTrue("No se pudo registrar el Usuario" , registro);
//    }
    
//    @Test
//    public void deberiaListarRolesPorIdPerfil() {
//        Roles rol = new Roles();
//        rol.setPerfiles(new Perfiles());
//        List<ObjetoCoreOT> list = new ArrayList();
//        try {
//            list = facade.listarConFiltro(rol, "perfiles.id", 4);
//        } catch (Exception ex) {
//            Logger.getLogger(TestCoreOT.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        assertNotNull("No se pudo obtener el listado de eventos" , list);
//    }
    
//    @Test 
//    public void registrarPassword(){
//            try{
//            // Print out 10 hashes
//            for(int i = 0; i < 10; i++)
////                System.out.println(SecureAccess.createHash("p\r\nassw0Rd!"));
//                System.out.println(SecureAccess.createHash("supercalifragilistico"));
//            // Test password validation
//            boolean failure = false;
//            System.out.println("Running tests...");
//            for(int i = 0; i < 20; i++)
//            {
//                String password = ""+i;
//                long startTime = System.nanoTime();
//                System.out.println("**************** START TIME : " + startTime + " ****************");
//                String hash = SecureAccess.createHash(password);
//                long stopTime = System.nanoTime();
//                System.out.println("**************** STOP  TIME : " + stopTime + " ****************");
//                long estimatedTime = stopTime - startTime;
//                System.out.println("Tiempo de respuesta : " + estimatedTime);
//            }
//            if(failure)
//                System.out.println("TESTS FAILED!");
//            else
//                System.out.println("TESTS PASSED!");
//        }
//        catch(NoSuchAlgorithmException | InvalidKeySpecException ex)
//        {
//            System.out.println("ERROR: " + ex);
//        }
//    }

//    @Test
//    public void deberiaEnviarMail() {
//
//        try {
//            SendMail sendMail = SendMail.getINSTANCE();
//            Config config = Config.getINSTANCE();
//            String asunto = config.getAsuntoMailNuevoUsuario();
//            String cuerpo = config.getCuerpoMailNuevoUsuario();
//            String mailDestino = "mauro.osmar@gmail.com";
//            boolean envioOK = sendMail.enviarMail(mailDestino,asunto, cuerpo);
//            assertTrue("No se pudo enviar mail.", envioOK);
//        } catch (Exception ex) {
//            System.out.println(ex);
//        }
//    }
    
//    @Test
//    public void deberiaGenerarBase64() {
//
//        try {
//            String cad2 = "";
//            for (int i=0; i < 50; i++) {
//                SecureRandom random = new SecureRandom();
//                byte[] arrBytes = new byte[16];
//                random.nextBytes(arrBytes);
//                Base64 base64 = new Base64(true);
//                String cad = new String(base64.encode(arrBytes));
//                cad2 = cad.replaceAll("\r\n", "");
//                System.out.println(cad2);
//            }
//            assertTrue("No se pudo enviar mail.", cad2!=null);
//
//        } catch (Exception ex) {
//            System.out.println(ex);
//        }
//    }

//    @Test
//    public void deberiaGuardarImagen() {
//
//        try {
//            String cad = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2MBERISGBUYLxoaL2NCOEJjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY//AABEIAWgB4AMBIgACEQEDEQH/xAAbAAEAAwEBAQEAAAAAAAAAAAAAAQIDBAUGB//EADkQAAICAQMCAggGAgEEAgMAAAABAhEDBBIhMUETUQUUFSJSYZHRMkJxcoGxBjShIyQzYhbhQ5Lx/8QAGQEBAQEBAQEAAAAAAAAAAAAAAAECAwQF/8QAKhEBAQEAAwAAAwYHAQAAAAAAAAERAhIhEyIxA0FRYZHBI0NxgaGx8AT/2gAMAwEAAhEDEQA/APz8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHor0LqWk9+Ln5v7D2NqfjxfV/YuDzgej7G1Hx4vq/sR7H1Hx4vq/sMHng9D2PqPjxfV/YeyNR8eL6v7DB54PR9jaj48X1f2C9C6l/nxfV/YmDzgel7E1Px4vq/sPYmp+PF9X9hg80HpP0LqV+fF9X9ivsfUfHi+r+wweeD0PY+o+PF9X9h7H1Hx4vq/sMHng7/ZOf48f1f2HsnP8eP6v7AcAO/2Tn+PH9X9h7Jz/AB4/q/sBwA9D2RqPjxfV/Yex9R8eL6v7AeeD0PY+o+PF9X9iy9C6l/nxfV/YDzQei/QupX58X1f2I9j6j48X1f2A88Hf7I1Hx4/q/sPZOf48f1f2A4Ad3srP8WP6v7EP0ZmX5sf1f2A4gdns3N8UPq/sR7OzfFD6v7AcgOv2dm+KH1f2Hs7N8UPq/sByA6/Z2b4ofV/Yezs3xQ+r+wHIDr9nZvih9X9h7OzfFD6v7AcgOv2fl+KH1f2Hs/L8UPq/sByA6/Z+X4ofVkeoZfih9WByg6vUcvxQ+rHqOXzh9WByg6vUcvxQ+rI9SyecfqBzA6fUsnnH6j1LL5x+oHMDp9SyfFD6kep5POP1A5wdHqeTzj9R6pk84/UDnBv6pk84j1WfnEDAG3q0/OI9Wn5xAxBt6tPziR6vPziBkDX1eXmh4EvNAZA18CXmiHhku6A+tiv+nH9CKNMcbxR/REONG2WbRGxvoi9cl4oKw2PuSoqjdxsKIRiok9DXaVaAoLLUVkQCrSZG4iwD4KMsyrQVABNARRIJSAmKstREUaAVUS3QIt2AoyriXoUBTaQ4mqRDQGDiUcUbyRlIDNpFWi7RFMgpRBfaxtAzRJbYSoAZg02oigM6ZKiaUQwM2iGi4aApSIovVEMKrRXaXKsCKIZJFoCpDLNlbAUQxZDYAqSQBBBYWBWiKLWCCjQLEMCpEuhJWT4A+v08LwxvyRMsdjTtPFC30SOjanymbZcmzklQVnRsrmijXPAGUolY9ao170Q+Aqko0Uao1l0Mm+wRSXBR8m3VFJRIMqFFytgRRBZlQqGgkTRNARRKiWii1AEhRJJBSiUWolQKIirJ2l0qAFVEhrgtZSTAzkZtGrRXbZBnSFGu0hoDHaHE0aKtAUFEtoq5xXcCKFEPLEq8oFmQZubZRzYGroq2jPcVcgNXJFdxk5EbgNdxFmVi2FaNlGRZFgAVckVcgL2RaKORWwNXIrZRMWQXsWUIsC+4hyKbkQ5AX3EORm5EOQF9xDlwZtkOQH3WDEpYY9ntRpGO1dS2nf8A0oL/ANUXeNPqbZZObRVO3yi2THXQx3NOgLSxpu0yk1XNi2LtUwrNybISRdwsJUEU20xRo0NpBhOJnRvMzaAzoOJehQVWiUi1FlECEidrLKJagMlGyyiaqHkKoCuwWkXKSlFdyBdirMpZoozlqijeSooznlqWZyzt9wOttLqUeWC7nHLI33KOTIOqWoiuhnLUHOyrA2edlHmb7mYCrObZVuyLIcgJBRyKuQGjkVcijZARZyI3EEOSCrJk2Z70Q5kGlkNme9kOTAu2yrZX3mRTAtZDK0xtYE2RZDS8xx5gTuRG8rwOPIBuIcmB/IENsgnjzIAECxaAEENh0B+hYYNYoNfCjoq48lsEU8MP2ozzOUXw+DbLHJ7vfk5Zys3lJvqjKUUwM1JounZVRLwxtgK54FGiSiUnwBHCIu0R1CRBWSKOJvtZGzzA56LbS7ikNr8gK7S6SLLGHtj3ClWTtS7mcs9dDGeo46gdLnGPcxlnS6HJPPbMnlYHVPO33MJZvmYuTZQgvKbZVtgWkA5YrzIcyjmFXdEWjJyfYryBq5pFHkKDgCzmyrkwQQRuZFsnn5D+QIsiw2vMWvICNzI5Jt+QdgRXmKXmQyLAtwRfyK2HICW38hfzKNkWBe/mVv5lSAL38yHXmVshsC1oi0VAE2iNzIdkfyBLZDIFACBQoCAKAAgAD9LWSePDj4X4UUlkeTqaYfewY1Ln3UUnjUZcG2XPKDRRRdnS0FADnUTWlFF9tEPngDNq+TKSN/kPD3EGCxt9i21RZ0KvwmUo8gQnZGxtl9qiveMp51FcAS8cY8sznmS6GOTO2YSnYGs9RJ9zGWV9ykpmMp2FaTymTm2UbIsCWyLKkkCyCWRYUIbDfkVbIDZHAIbQD9CKZDbI/VgS2kRu+RViwJbZF/MhlQLEbkVBRLkiNzBADcyG2SQAtkMkUBUhotQoCtAtRDQFQTQIKkUXoUUUoUWoEFaIaL0RQFKDLURQEFWXohoCgLUKAqQWoNAfpOJNYcaXwotfmbxjHwIdvdRm1G+p0ZZshIlzSf4SrytcKJBMlyZOrNIvdZVwbZBVRtmjagrFqC94wyzUgpJ73aD91deTN5FFdaM5Zd36BFcsmzmnI0y5ElRyTyWwqZTMpSIkyhAbZWySLCopiiWyGwIIFkORBJVtFXIq5AXbKuRW2RVhEuSIciNoooWQTQoCoLUNoFBRahQFaIoukTtCs6FF6JAzohouyKIK0Eie5JRXaKJsEEFWSyAIoULAChQ4IcgJogi2OQDIFEdAAbRFACLAIsBYIbG4CCX0IshtgfqcOMGJv4UZZ2kuKLaeaeDHF8raicuL3eOUbZcsWS7b4HhtM1UVHlgVhGuWRlm0uCMmVI4s2o8mQaOTb5ZhkydomEs0mUll2x+YF5SrmTMMmfsjOcnJ9SKIqJTbKlnRVsKjkq3ZLZW0QCrDZVyANlW2LbIKIfJFFqFAV2ii+0soBGNDabONFXSCs9ootdkxVhFdo20XlwUbsiodECmQEH1CIJTCpSEuA5fIpKQEXyRZFWTVAKsh2SmkhYERVktUQRYEkArygLRW6VPgv4ca6mKbsltsCzgl1ZTck6SG1kOLsCXRVtEvG/MjZ5sCNxG4s4oh433AjcRuJeNoeG2BVsbiXjonZx1Ap1IaZegBnQou6IYEKJFE2QwP0vT45eDCl+VHZBbcdyPTho4R0WOUVyoL+jxNXOak+eDbKMuWMW6XJw5NQ3LljJn4pnFknbA0y5r7nNJ2Q2yk5cECU6M22yUr6h0iKqQ2S2UdeYUbItENlWQJNFLZZ0QUVaILUSogUoUaqAryAook7TRqkTFWBSMRLgSkkzKU7IIlJlEm3yy1+ZNoA0kuCqlRLXmUsA5EWiVXchxQBN9i/h+bK7q6BzbAOKT6jhdiNz7DcyCG0yKj3Db8iH8uoE0mRSXcrTsNV0KLNJorXyIp9w3LswJ2vyI2/qSt1daG5rvZBUJtEuV9iY0gM3Jy7FeTe4t1QaS8gMtr8yKou4kbfMApeZFonahtAhuN2R4nyJpENgS5WRuoq2yrTb6gX3EORQi2UWbIbRApARZBbb5DawKckFqIaA/dMUd2hxrzxr+j5rXp45Siz6fTf6mL9i/o8H03hW9yjymaiV87kdyZi0bZY0zKSKjKRSrZdoiiKq+OhRsvIoRVGVZdlWgKkFqsAUoUWUWzSMOeQM1C+pdRSRpKPHBnKTqqIKt30J2OKJxqCdvqaOmuOgGHZqjKUmulm84uijxy2gZWurREpJ9ETKEiu1pgVdsJ0WcWQl8gIcmXUE4ptpFaLKNx46gJbVXdmU0Wkm2VpgVpENLsWpjbaAouCerIdoXXcC9beepDn8ijd9yUmyBuvsVD4ZBRL5JtLuVpF1FdiCN9drG5PsW6C0RVVT6Db8w2hce/IFXH50TtS7jfHyG5MBSIdE2RdlENryK7g4sbZPsEQ/1K2WeOfdMnYl+LgCFHcupV435k9W0kyHKVVXAEbfmQ0l8w5OiL80UG12RFk2vIRi5SoAlask6FBJJNURkxxT/EiaMKKSR0KEUuZoPFFr8XA0ftmn50eJf+i/o8rXR/FGSPW0v+ri/Yv6KajTQzRdrlmoj4rUwSk6OOaPoNd6NWOT5PLyadLuio89xIa4OieKimwK5pIo0byiUkiKyaIo0orXIFKpcEVybbCu2mAjGiWmmTNPihKVLkgiUlRkyZSso2rAVfYuu1Ff5F/Mgs5LzslTM277EKrugOjba5RlOFdjfTyhPG05VJeZaeKMqe5JE3FxwtchY+Lo63iwt0pKzSEIy91NKkOxjznilf4WUlGUezPVenpf8AlMvVd3MZ2TsvVyYsE5q6N/UpfJMtkxZktsZ/wVWPOl782kTauK+oO/ekqHqMY87+DGc80pbY7qNcUNQnzJ/oNp4yy6Zq3FJxMJaaTi3fK7HqKGRqpxjRFY4ulFDsdXmrT3VJ33LOEca5pvyPQeGMui/lMzyaaK5cb/RjsY8/JG1cYGccUn+WjtlNY+I4n/LMvWJydSx1H5F2s45nie7joR7y4XJtLxHfhwaj+hWOHM+aZdMZuddSryeSNZad9WVWHnhNjwZvJfZEbn5HXHSOXZlnpa4Y7QxxORW32O16W3STKSwKHVWNhjkqT7imvM6dr7RKuEu6LqYzT87Zbc+xooquSXFJcE1WW6T/ADCUW+nJOx2NrXcBFV2IlBefUdEVk2+wCWCXVLgzeKS7GqnNqkQ4zfmVGKRMU07XU1x46vcmabUuhNXGDnka7mb3PqmdcpRXSRHjLysaY5VGTfQu45Iqr4NvH+RSUk0/eGj9u0v+ri/Yv6LtFNL/AKuL9i/o0fQ0jzNblq4zimuzPE1UMck2lTPop6LxN2+Vpnl6/wBFTxrdjluj5GtZfP5OHRjLg7cunlF+9Ey8EK5JKzJxOyWKjGUOSDDbwUceTocaKNBUQ5XQpkSUrNYPbdqw4QkuOH8yDHddK6E266WWlgafyGxJVYGDjfYq4PyOhQ8uUS8dY6+YHJymWUbLuFdi0Y9OAMZY2lZVOup25cdQVnO8TpvsTVwxY97tM2jpW03JmOK4J8MmeZtUrM+rF4YXHJd3E6IQxpurb+ZzYnOT4dG+Sc4xS3cszbWpIiSxJpTj7z8mXjixwTlvaOZN3wrZE8m38cL/AJJg1lq8UY1zN+ZMdRgmlu3foZqOPIulFo6W3w/4Hi+r+LiXGPH/ACzOWfHGXRs0no8s3S4XyJxej8qXkvmTYeslqY3at/IZMk9trEjrh6PSak5KzSfuy2RimTYuOGEsklbil/BnLNNSfSj0pYpT4caRk9HjUncx2iY5cahnlTh/J0PDjXCgi6goLbH6kpcktXFVjivyoPHHyNaIaJo5M2mU4tJckYtNHGuls66KNF0xk4pdikkvI1kZyRRkzNwT6o2aK0VGG2O6qKzwv8qOnaiS6mPOlhm+iKPDPuepQeNSXKHYx5MsfzKeFb6npz0sX04KPSP4i9kxweAl1kPCp2mdnqaT5ZLwKH4VbHYxyeG/Kg4NG88Upq1wzGWLIu7Y0xRrjuUkl8RaUJ+TKLHOT4RUQ9i7WV/SJstPPujZY2lxQ0xyqM+0Q8Mmn7prkjkXcpHdJu+lDR+1aX/VxfsX9Ghnpf8AVxfsX9Gp0ZQcWvx5pR9x2juIA+Zzqd+9Gv1OSS+R9dPDjyfigmc2T0Zpp/kr9C6mPkp/tMZQPqsnoTDJe7Jo5MnoHIvwSTA+blAzlBnuZfROeHXG/wCDlnopx6xf0IrzFESgrpnXPTfKirxc8oDnXPBnNNPodMsPPumU8d/qBnFbmlXLNfD2KuTNJxdpuy0ss+zII8JTboh4skOyS+ZpHUNLmKZLzwydU4kutRlPJ8VMRkuyiTshL8//AAXWPH8ZjFRF88pP+CfDxO3KK+hK2J8ckN89qIuo2wXGOMU35lVpMkvxSSRpvXaiqyJ9ZA1K0cF1yfQn1LHJ252hcV+YePDH8yersaR9HQ67qReOPBidue75I55a3j3YmcdTzco2Z62mx3vPaeyNV0OPJqsspO3XyIWplK0lSM3FvkSYtqyyyck5SbN1nxr8u5nPGDNY4rLcF56mcn7vCM1FvlmixM0WOkZGUYltptHGyVj5JqqbSrib7SriFYuJRo2kjNlRjJGbRtJFGioycSNppRNFGVEOPBrtDiBkol1EtGJfaQZ7SHE1oJWBg4mc4+R1uA8NNDR5ziFfc7ngRWWFLsXTHHx3ihcFwkjq8BPsPVV5DUccowl+Yr4MF0f0O71K4tqDdFsWhlkXEKrzGjznHjp9SjhFRlUex60PR2Wc9qxujTN6IyQxyckklFjtDH6Jpf8AVxfsX9Gplpf9XF+xf0anocgAAAABAJAEUVlihJe9FP8AguAPP1fo/BKLahT+R5Wb0fBdHR9FOtrs8vO1ufBB4uTSODfRnPLAu6PTzK3wjmlDnoTVcTwJ/wD2c+TBTdHp+HzdCWGM+3BNXHjSxOhHFxyenLSrdwmRLAl2ZNMeZLE+yIUJHovC+kf6IemyVwv+CauOHw5dg4RS5s7PVZdyfVaXcauOF/8AquDPbJuj0Hid9GQsDa6Mmpjg2O+XwXlCL6M6npn5Mvj0sW+Wxq489YW2ax002/wnqrBHj3N38GsNM5fhhyZvNqcXl49JK+UarSvsmevDST+FHRHSV1RzvNrq8SGjk+xtHSNLoewtMT6vRjuuR5K0z8iXpmer4FFXh4HZceasFEvFR2yx0ZTgNMcM4pGUjfLxZzSZuM1R9SkizZXqUZTRRmsupmyoigiewKBVFgiAkWK9SaAJWXUSYRNVHgmjNQtlvDNIQ5NlCyDm8JslYG+x2RxnRjxKugHnR0mRq9peOklfKPWhCkTsV9Bo5semhjjyjRQjxSN9tqiHjoCIpdimrr1XJ+1luhjqn/2+T9rEH0ml/wBXF+xf0amWl/1cX7F/RqexwAAAAAAAgCQQSBDVmU9Njn1RsAOHJ6Pg+jObL6PlHlKz1g1Zm8V189LTNdivhNdj3cmnjLoYvRGLK1seP4TGxnsepD1FGcq7Hj+GyfCb6nr+oxD0SQymx5C06f5UXWmXken6rRPq9Gbq+PN9VvsvoStDB9UeisRZYjPq7Hnr0fjfWJrDQY0vwncsZpGAnC1LzccdDj+E2jpMcex0bSTpPsZ97N51j6vBO6J8KPkag38Lj+DPasXiXkPCXkbURZL9nxXtXNLGvIxnFI6pyOXLM8vOSOnC2uedeRzTrkvmzcHFkzWzMjsxz9zimzpyzs5MjOsc6q2RZVsruNC0nyVsiTK7giwSIvk0XQCnQNiVplLCtIljOLotuIOjErOhJUcuKZup8EGqRpBGMZG8HyB0QgbRSMISs3jLgguhyVZPYCV1LFUXQGUuphqV/wBvk/azoyGGb/XyftZYPotL/q4v2L+jUy0r/wC1xfsX9GtnsedIIAEgiwBJWUtvYsZ54qWDIpfhcWmBnp9Ss+PeotK2joOD0NCOP0ZhjBNRSdX+p3gACG6AkEEgCCQBBIAAgkARRFIsCYK7SaJAyCASCgAAIAspKdGbykFzOcqRSWWkcuXP15OHP7Wfc3x4Wr5cnU4dRmS4srl1HDPPz5jz+13k6py5evJyzyGeTKYSmdZE1rKZhOVlXMzcjWMrWUcqZVyKORcRdyI3GbkRuLg3Ujq0eGOryRxSz+Db/EedvCyuPRmbx2NcOd43Y1yKSzZovM34c3FPz5MnJvJW5pFFLmTfdkdZHPPlfYvOfG5S/Scfwn5Nscm5Sju4rqMeRqEpSbfkZwntdCPMHEjXLjx+a2fL8vv7t4RyyhuWSn2RrHUSnpJt8Tjwc8M+2NNO0TG46ed9Zcj+jHLjf5kk9mfq6dBqZbtmV/i5i2Tiy5Jw1f8A1JLbzHnp1OdR3aaFcSjyidNk24NS5dZL7k9dOvDlefLjPdkz+/7u/TRz5vRcskc0/Fttc+XYpHX59VjwabDKUczfvyXyM9NrPV/RtR/HbSXzKwhk0Dw6pNtv8a/UizhO3LtJ9b1/78HfrMmXHq8OnlqJYcO3nJ5v9TX0Vkz5/WcfiueOPGPK0cmpzY8muhk1UZz0u24UuL+Zt6NzPDk1OXFDJ6olcYvz+Q31y5cP4P09yfrv+1M2TPpdZghg1ktRKbqUOGfQVTPmdRPBl1GKXozFkhn3XJpNH0Hi0uetFjh/6+OTjv1/yvJKzDUJeBk/azz/AEr6W9Vw/wDQcZTk66/hPEfp/U48WeDfiva+Wuh048bXgtkfpGlf/a4v2L+jWz4vS/5qvAxw8BJxik+Tp/8AmMIpN44yvsj1vO+r3HP6Q1uPQ6WWWb6LhHzv/wA0wr8WnaVeZ8z6Y9Ny1udyk57Oyb4og+r0Hp7XanLtlhg3KSSjHsvM+mXQ/K3/AJDLSqEfR8pQ496UlyztX+da7wfDkobl+euSo/SDPUy26XK/KD/o+K0H+eJQUdVgcmvzR7nBrf8AMdbnlkjiahjlxXyIP0D0fHbocK/9bOk+W9Gf5dpMmPFiywljdJN9UdfpD/KdDooqt2ST7ID3rObVJOeFec0fNL/OtNt502RS/U5Z/wCXLU67BSljjGV/JhX2/QiORSuux5no/wBM4NXGTnkxpx8meXq/8h8P0tDBiyweJupf/wBGj6mxZxy1+lxY1KeoxxXS3I0w6vT547sWaE15pjTHRYKeJD4l9Rvj8S+o1GhBzT12lx7t+ohHb1uXQxXpj0e1xrMX/wCxdHeRuV1as4PbPo+UW4azE2v/AGPk9XrXL0nOeD0ltlKdr3vdSJeWLj7wHyOq/wAl9V0/hQ1Cy5Gv/IuiPIf+QelIZ8alqHNzd1HyJ3Xq/RbFnFo9bHVaWGW1Ftcxb6M1eaPxr6kvOHV0WVcqOaWqguPEj9TKWddpL6nPl9qs4V1TypHLlzpHHm12JRlLxY1Hq76Hnz9JYJzcI54OXlZxt5cnXjxkelk1Ndzizaq75OHNq4Ru8sV/JyS1cJK45Yv+RODWu3Jn46nJkzX3OeWdP86+pzZNXji/eyRX8nScWLXTPIZvIeZn9JVLbjSl8zF+k59PDRucWbyeq8hRzPKfpKfwIe0OOY8murPaPT3lHI832g/gLx12OXeh1XY7XMrvMt98lXOhhrfeRvOaOZSV8ol5Ki32Qw10bxvOCWuhFcW2Y+uzyS91NfIYmvV3l1kPGWpm8lKT57HVPVLHHqpSXYdTXorIXjkPIXpFU7jyF6Qld7Pd8ydauvajlN8eY+e9fybrSSiuqZfUa/xMCWKTi31J0Oz6jFmTr3kdHjY8UN+WahHzbPhcWvyY9sbbUXfU69d6QlrYLG1tjjVpvuyfDurOcfY6fWafPKsWeEn5WdEs8cd7pxVK3yfneGMlF58M2pw5M4y1OXJJb8jk173PYfC/Ne79ExekdNlaUM8G/Ky+py7MMmpxjJr3dzrk/PtLPHp9RGXvT288di/pbVZtVqt1yjjaW1brJ8L07+NvSWrlHLJT8Oc7duPQ8/Hlyvh5GovryWhhnPHcaW3v5m8Y446KSlCMXJfifLb+R2mRz21xxzyi+GarUT45fBzTlum5bVG+y6DczWMO31hyVNjxLXS0ji3snxJfImUdjnF8uNEcN24o5PFl8ifGl8iZR2JqKpJDckvwnJ48/kPWJ/IZR2RzbHatF563xGvEts4PHl5Ih5m+qiXKruWbE/NDet3us4PFfki3jyrpEmVHq4NbmwJ+G1z5MzyZ5ZMm/wDC7PP9Zn5RC1M0+iGVXp582TP1yNLyJ0+p1GmTWLNKKfWnR5q1k1+SH0ZWepnN8qK/QZT19d6K9O5cmSOKcpSjH8UrO/0hrcu+tLlk41362fEaf0jk08ax48X6tO/7Nvbmq+HF9H9ydWt8d08OaWScsmbc5O+WZyw5FK0rVdLOWfprPNVLDgvz2u/7M/auf4Mf0f3L1qOycM1c43/CMKk7cvd+Rl7Vz/Bj+j+49q5/hx/R/cZUa0n7scnK630IjKWCSm/frokZP0llfXFhf6x/+yj10n/+DD/Cf3GUevi1+SEajkcV+prD0pk/C8kv1s8F6uT6Qgv0v7lfWsnyM9F7V7/r9u3P/kjJ6Ql4ctuWSddVI8NazIndR+gesyN3UR0p2ruxPJkxS/60op9r6mawSct7l355OR6rI+yI9ZyVXBrKmvUkk/zqv1IUIv8AOl/J50dXOPSMfoHrMj7R+gymvS8CN/8AljX7is8MF+aLf6nnPV5H2j9CPWZvtH6DKa9NYce1vdG68zD1dVzNfU4/WZ/DEn1qXww/5GUdHhNXTTCxOuWjnWqmvyx/5HrU/hiX0avDK+vBDxNLhcmfrU/KJHrM/kPRupZ10k1x0J8TO+5z+sz8kPWZ+SGU2t3HI00pPnqVaz9HddDL1rJ8g9TkdW1wPRfwpVymI42r91/qU9ayfIes5PkPRZYpJN1ZXwp/Cx6zk+Q9Yn8i+i3hO7rtyHGWykinrE/kR48/kPRolXUidVUSnjS8kPGl5RJ6IUZLsXhKUevK7rzI8eXlEjx5eSL6OyGr20ljUY7aaX9muj1GDFkySz43k3KkzzvGl5IeNL5Geq9q6YZMUNTuUKhfKKRnw1dLda4MfGl5IeNLyRcNd2HU4oYZpprK3w64Mc+onklV3BdFVI5/Gl5IeNKq4GGswAaQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/Z";
//            String nombreImagen = "IMAGENDEPRUEBAS.jpg";
//            byte[] imgByteArr = Base64.decodeBase64(cad);
//            String SFTPHost = "192.168.1.189";
//            String SFTPUser = "mcj";
//            String SFTPPassword = "otiq53jg";
//            String SFTPDir = "/Users/mcj/Desktop/temporal_mcj";
//            SecureTransmission.sendFile(SFTPHost,SFTPUser,SFTPPassword,SFTPDir,nombreImagen,imgByteArr);
//            File file = new File(nombreImagen);
//            file.delete();
//
//            assertTrue("No se pudo enviar mail.", true);
//        } catch (Exception ex) {
//            System.out.println(ex);
//        }
//    }

//    @Test
//    public void deberiaObtenerImagen() {
//
//        try {
//            String SFTPHost = "192.168.1.189";
//            String SFTPUser = "mcj";
//            String SFTPPassword = "otiq53jg";
//            String SFTPDir = "/Users/mcj/Desktop/temporal_mcj";
//            String nombreArchivo = "IMAGENDEPRUEBAS.jpg";
//            SecureTransmission st = new SecureTransmission();
//            byte[] byteArr = st.getFile(SFTPHost, SFTPUser, SFTPPassword, SFTPDir, nombreArchivo);
//            String imagen = Base64.encodeBase64URLSafeString(byteArr);
//            FileOutputStream imageOutFile = new FileOutputStream("/Users/mcj/" + nombreArchivo);
//            imageOutFile.write(byteArr);
//            imageOutFile.close();
//            assertTrue("No se pudo enviar mail.", true);
//        } catch (Exception ex) {
//            System.out.println(ex);
//        }
//    }
    
//    @Test
//    public void deberiaListarAccionesPorEvento() {
//        int idEvento = 1;
//        EjecutaAccionesPorEvento ejAccPorEvento = new EjecutaAccionesPorEvento();
//        Set<Acciones> list = null;
//        try {
//            list = ejAccPorEvento.obtenerAcciones(idEvento);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        assertNotNull("No se pudo obtener el listado de eventos" , list);
//    }
    
//    @Test
//    public void deberiaCalcularFecha() {
//        Date fechaInicial = new Date();
//        int diasADesplazarse = 5;
//        System.out.println("Fecha inicial : " + fechaInicial);
//        List<Feriados> listadoFeriados = null;
//        Date fechaFinal = null;
//        try {
//            Facade facade = new Facade();
//            fechaFinal = facade.sumarDiasHabiles(fechaInicial, diasADesplazarse);
//            System.out.println("*********************" + " Fecha final : " + fechaFinal + " *********************");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        assertNotNull("No se pudo obtener el listado de feriados" , fechaFinal);
//    }


//    @Test
//    public void deberiaObtenerRolPorUsuarioId() {
//        int usuarioId = 2;
//
//        try {
//            Roles rolGestor = GenericDAO.getINSTANCE().obtenerRolPorUsuarioId(usuarioId);
//            int rol = rolGestor.getId();
//
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        assertNotNull("No se pudo obtener el listado de feriados" , usuarioId);
//    }

//    @Test
//    public void obtenerUsuariosSegunProveedorId() {
//        int proveedorId = 6;
//        int rolId = 4;
//
//        try {
//            List<Usuarios> listaRetorno = UsuarioDAO.getINSTANCE().obtenerUsuariosSegunProveedorId(proveedorId, 4);
//            for (Usuarios usr : listaRetorno) {
//                System.out.println(usr.getNombres());
//
//            }
//
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        assertNotNull("No se pudo obtener el listado de feriados", rolId);
//    }


//    @Test
//    public void deberiaCrearLibroEnExcel() {
//
//        Integer usuarioId = 2;
//        List<Object[]> listadoOts = obtenerDatosDesdePL(usuarioId);
//
//        //Blank workbook
//        XSSFWorkbook workbook = new XSSFWorkbook();
//
//        //Create a blank sheet
//        XSSFSheet sheet = workbook.createSheet("Resumen Pagos");
//
//        //This data needs to be written (Object[])
//        Map<String, Object[]> datos = new TreeMap<>();
//        //Seteando encabezados
//        datos.put("1",new Object[]{"Tipo OT","Contrato","Proveedor","Gestor","ID PMO","LP","Sitio","Plan de Proyecto","PEP2","Numero OT","Nombre OT","Fecha Inicio", "Fecha Termino", "Monto Total"});
//
//        Integer contador = 2;
//        for(Object[] obj : listadoOts){
//            System.out.println(obj.getClass().toString());
//            int ultimoItemObjeto = obj.length - 1;
//            datos.put((contador++).toString(), obj);
//        }
//
////        //This data needs to be written (Object[])
////        Map<String, Object[]> data = new TreeMap<>();
////        data.put("1", new Object[] {"ID", "NAME", "LASTNAME"});
////        data.put("2", new Object[] {1, "Amit", "Shukla"});
////        data.put("3", new Object[] {2, "Lokesh", "Gupta"});
////        data.put("4", new Object[] {3, "John", "Adwards"});
////        data.put("5", new Object[] {4, "Brian", "Schultz"});
//
//        //Iterate over data and write to sheet
//        Set<String> keyset = datos.keySet();
//        int rownum = 0;
//        for (String key : keyset) {
//            Row row = sheet.createRow(rownum++);
//            Object [] objArr = datos.get(key);
//            int cellnum = 0;
//            for (Object obj : objArr) {
//                Cell cell = row.createCell(cellnum++);
//                XSSFCreationHelper createHelper = workbook.getCreationHelper();
//                XSSFCellStyle cellStyle         = workbook.createCellStyle();
//                XSSFDataFormat xssfDataFormat = createHelper.createDataFormat();
//                String formato = "";
//                if(obj instanceof String)
//                    cell.setCellValue((String)obj);
//                else if(obj instanceof Integer)
//                    cell.setCellValue((Integer)obj);
//                else if(obj instanceof Date){
//                    cell.setCellValue((Date)obj);
//                    formato = "dd-MM-yyyy";
//                }else if(obj instanceof  Double){
//                    cell.setCellValue((Double) obj);
//                    formato = "#,##0.000";
//                }
//                cellStyle.setDataFormat(xssfDataFormat.getFormat(formato));
//                cell.setCellStyle(cellStyle);
//
//            }
//        }
//        try {
//            //Write the workbook in file system
//            FileOutputStream out = new FileOutputStream(new File("ArchivoPagosOTs.xlsx"));
//            workbook.write(out);
//            out.close();
//
//            System.out.println("ArchivoPagosOTs.xlsx written successfully on disk.");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        assertNotNull("No se pudo crear el archivo de descarga.", new String("OK!"));
//    }
//
//    private List obtenerDatosDesdePL(int usuarioId){
//        Facade facade = new Facade();
//        List listaDeOts = null;
//        try {
//            listaDeOts = facade.retornarResumenOtsAPagar(usuarioId);
//        }catch (Exception e){
//            System.out.println("Error al obtener listado de OT's");
//        }
//        return listaDeOts;
//    }


//    @Test
//    public void deberiaObtenerContratosPorProveedor() {
//        List<Proveedores> proveedores = null;
//        int contratoId = 4;
//        try {
//            proveedores = GenericDAO.getINSTANCE().listarProveedoresPorContrato(contratoId);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        assertNotNull("No se pudo obtener el listado de proveedores", proveedores);
//    }


//    @Test
//    public void deberiaObtenerRegionesPorProveedor() {
//        int contratoId = 4;
//        int proveedorId = 7;
//        List<Regiones> regiones = null;
//        try {
//            regiones = GenericDAO.getINSTANCE().listarRegionesPorProveedorYContrato(contratoId,proveedorId);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        assertNotNull("No se pudo obtener el listado de regiones", regiones);
//    }


//    @Test
//    public void deberiaGuardarJournal(){
//        RedisClient redisClient = new RedisClient("localhost",6379);
//        System.out.println("Iniciando tarea..");
//        long time_start, time_end;
//        time_start = System.currentTimeMillis();
//
//        try{
//            RedisConnection<String, String> conn = (RedisConnection<String, String>) redisClient.connect();
//            System.out.println("Connected to Redis");
//        String value = conn.get("key");
//
//            for(int i=1;i<100000;i++){
//            System.out.println("Valor --> " + i);
//            conn.hset("journal:1","","");
//            Map<String,String> mapa = new HashMap();
//            mapa.put("A" + retornarStringAleatorio(),"1");
//            mapa.put("B" + retornarStringAleatorio(),"1");
//            mapa.put("C" + retornarStringAleatorio(),"1");
//            mapa.put("D" + retornarStringAleatorio(),"1");
//            conn.hmset("A"+i,mapa);
//            conn.del("A"+i);
//            }
//
//        String valor = conn.hget("clientes","Mauro");
//            conn.close();
//            time_end = System.currentTimeMillis();
//            System.out.println("the task has taken " + (time_end - time_start) + " milliseconds");
//            redisClient.shutdown();
//        }catch(Exception ex){
//            System.out.println(ex);
//        }
//    }

//    private String retornarStringAleatorio() {
//        SecureRandom random = new SecureRandom();
//        byte[] arrBytes = new byte[32];
//        random.nextBytes(arrBytes);
//        return new String(Base64.encodeBase64URLSafe(arrBytes));
//    }

//    @Test
//    public void deberiaObtenerYGuardarValoresDeIndicadores() {
//        try {
//            SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
//            Date fecha = formatoDelTexto.parse("25-07-2016");
//            for(int i = 0;i<17;i++){
//                String uri = "http://mindicador.cl/api/euro/" + formatoDelTexto.format(fecha);
//                System.out.println(uri);
//                URL url = new URL(uri);
//                String jsonRetorno = retornarJsonServicio(url);
//                ObjectMapper mapper = new ObjectMapper();
//                Map mapRetorno = mapper.readValue(jsonRetorno, new TypeReference<HashMap<String,Object>>() {
//                });
//                String codigoTipoMoneda = (String)mapRetorno.get("codigo");
//                String nombreTipoMoneda = null;
//                switch(codigoTipoMoneda){
//                    case "dolar":
//                        nombreTipoMoneda = "USD";
//                        break;
//                    case "uf":
//                        nombreTipoMoneda = "UF";
//                        break;
//                    case "euro":
//                        nombreTipoMoneda = "EUR";
//                        break;
//                }
//                Map filtros = new HashMap();
//                filtros.put("nombre", nombreTipoMoneda);
//                List listTipoMoneda = GenericDAO.getINSTANCE().listarGenerico(TipoMoneda.class,filtros,null);
//                int tipoMonedaId = ((TipoMoneda)listTipoMoneda.get(0)).getId();
//                List<LinkedHashMap> registros = (List)mapRetorno.get("serie");
//                List<TipoMonedaValores> listadoTipoMonedaValores = retornarListadoTipoMonedaValores(tipoMonedaId, registros);
//                GenericDAO.getINSTANCE().registrarValoresTipoCambio(listadoTipoMonedaValores);
//                fecha = addDays(fecha, 1);
//            }
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch(Exception ex){
//            ex.printStackTrace();
//        }
//    }
//
//    private String retornarJsonServicio(URL url) throws IOException {
//        String jsonRetorno = "";
//        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        conn.setRequestMethod("GET");
//        conn.setRequestProperty("Accept", "application/json");
//        if (conn.getResponseCode() != 200) {
//            throw new RuntimeException("Failed : HTTP error code : "
//                    + conn.getResponseCode());
//        }
//        BufferedReader br = new BufferedReader(new InputStreamReader(
//                (conn.getInputStream())));
//
//        String output;
//        System.out.println("Output from Server .... \n");
//        while ((output = br.readLine()) != null) {
//            jsonRetorno = output;
//            System.out.println(output);
//        }
//        conn.disconnect();
//        return jsonRetorno;
//    }
//
//    private List<TipoMonedaValores> retornarListadoTipoMonedaValores(int tipoMonedaId, List<LinkedHashMap> registros)   throws ParseException {
//        List<TipoMonedaValores> listadoTipoMonedaValores = new ArrayList<>();
//        for(LinkedHashMap item : registros){
//            SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
//            Date fecha = formatoDelTexto.parse(((String)item.get("fecha")).replace("Z","").replace("T"," "));
////                System.out.print("Fecha : " + fecha);
////                System.out.println(" | " + "Valor : " + item.get("valor"));
//            TipoMonedaValoresId tipoMonedaValoresId = new TipoMonedaValoresId(tipoMonedaId,fecha);
//            Object objValor = (Number)item.get("valor");
//            Double valor = null;
//            if(objValor instanceof Integer){
//                valor = ((Integer)objValor).doubleValue();
//            }else valor = (Double)objValor;
//            TipoMonedaValores tipoMonedaValores = new TipoMonedaValores(tipoMonedaValoresId,valor);
//            listadoTipoMonedaValores.add(tipoMonedaValores);
//        }
//        Collections.sort(listadoTipoMonedaValores);
//        System.out.println("*****************************************************");
//        for(TipoMonedaValores item : listadoTipoMonedaValores){
//            System.out.print("Fecha : " + item.getId().getFecha());
//            System.out.println(" | " + "Valor : " + item.getValor());
//        }
//        return listadoTipoMonedaValores;
//    }
//
//    static Date addDays(Date date, int days) {
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(date);
//        cal.add(Calendar.DATE, days); //minus number would decrement the days
//        return cal.getTime();
//    }

//    @Test
//    public void deberiaObtenerValorTipoCambio(){
//        int tipoMonedaID = 1;
//        double monto = 326.0;
//        double valor = 0;
//        try {
//            valor = GenericDAO.getINSTANCE().retornarPrecioEnPesosChilenos(monto, tipoMonedaID);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        assertNotNull("No se pudo obtener el valor tipo de cambio", valor);
//    }

    /*@Test
    public void generarPdfActa(){
        boolean generar = false;
        String nombrePdf = "";
        String nombreXsl = "";
        String xmlString = "";
        int otId = 569;
        int actaId = 313;
        try {

            nombrePdf = "pdf_acta_"+otId+"_" + actaId ;
            nombreXsl = "plantilla_acta_contrato_" + 1;
            Gateway gateway = new Gateway();
            xmlString = gateway.obtenerXmlActaDummy(actaId);

            Repositorios repo = new Repositorios();
            repo.setServidor("localhost");
            repo.setUsuario("Christian");
            repo.setPassword("chelonko");
            repo.setPuerto(22);
            repo.setUrl("/Users/christian/Desktop/FTP-OTEC/Usuarios");

            String pathXsl = "/Users/FParedes/Desktop/FTP-OTEC/plantilla_acta_contrato_1.xsl";
            String pathXml = "/Users/FParedes/Desktop/FTP-OTEC/xml_entrada.xml";
            String pathPdf = "/Users/FParedes/Desktop/FTP-OTEC/"+nombrePdf+".pdf";

            RenderPDF.getINSTANCE().convertToPDF_FromXmlString(pathPdf,xmlString,pathXsl);
            //RenderPDF.getINSTANCE().generarPDF_From_XmlString_XslFTP(xmlString, repo, nombrePdf, nombreXsl);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //assertTrue(generar);
    }
*/

//    @Test
//    public void generarPdfActaMasiva(){
//        List<int[]> listaPdfs = new ArrayList<>();
//        try {
//            listaPdfs.add(new int[] {569,313});
////            listaPdfs.add(new int[] {221,371});
////            listaPdfs.add(new int[] {163,216});
////            listaPdfs.add(new int[] {229,361});
////            listaPdfs.add(new int[] {169,217});
////            listaPdfs.add(new int[] {164,218});
////            listaPdfs.add(new int[] {241,182});
////            listaPdfs.add(new int[] {255,199});
////            listaPdfs.add(new int[] {329,310});
//
//            for(int [] pdf:listaPdfs){
//                generarPdfActa2(pdf[0],pdf[1]);
//            }
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//        //assertTrue(generar);
//    }
//
//
//
//    public void generarPdfActa2(int _otId, int _actaId){
//        boolean generar = false;
//        String nombrePdf = "";
//        String nombreXsl = "";
//        String xmlString = "";
//        int otId = _otId;
//        int actaId = _actaId;
//        try {
//
//            nombrePdf = "pdf_acta_"+otId+"_" + actaId ;
//            nombreXsl = "plantilla_acta_contrato_" + 1;
//            Gateway gateway = new Gateway();
//            xmlString = gateway.obtenerXmlActaDummy(actaId);
//
//            Repositorios repo = new Repositorios();
//            repo.setServidor("localhost");
//            repo.setUsuario("Christian");
//            repo.setPassword("chelonko");
//            repo.setPuerto(22);
//            repo.setUrl("/Users/christian/Desktop/FTP-OTEC/Usuarios");
//
//            String pathXsl = "/Users/Christian/Desktop/FTP-OTEC/Usuarios/PDFs/Plantillas/plantilla_acta_contrato_1.xsl";
//            String pathPdf = "/Users/Christian/Desktop/FTP-OTEC/Usuarios/PDFs/Pdfs/"+nombrePdf+".pdf";
//
//            RenderPDF.getINSTANCE().convertToPDF_FromXmlString(pathPdf,xmlString,pathXsl);
//            //RenderPDF.getINSTANCE().generarPDF_From_XmlString_XslFTP(xmlString, repo, nombrePdf, nombreXsl);
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//        //assertTrue(generar);
//    }

//    @Test
//    public void deberiaObtenerCheckListPorContrato() {
//        int contratoId = 5;
//        CheckList checkList = new CheckList();
//        try {
//            checkList = GenericDAO.getINSTANCE().obtenerCheckList(contratoId);
//            String json = checkList.getJsonCheckList();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        assertNotNull("No se pudo obtener el checkListConfiguracion", checkList);
//    }
//
//    @Test
//    public void deberiaRegistrarCheckListOt() {
//
//        CheckListOt checkListOt = new CheckListOt();
//
//        checkListOt.setCheckListDetalleOt("\n" +
//                "{\n" +
//                "  \"servicioPrivado\": \"\",\n" +
//                "  \"fecha\": \"12-03-2016\",\n" +
//                "  \"checkList\": [\n" +
//                "    {\n" +
//                "      \"item\": \"FO: etiquetado, plano unilineal, reflectometriaViñeta: N° AVBA; N° Cubicación; Poligonos/ Zona/ AAD; etc\",\n" +
//                "      \"puntuacion\": 3,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 2,\n" +
//                "        \"nombre\": \"No OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Simbología: Existente y cambio de cuentas, etc\",\n" +
//                "      \"puntuacion\": 1,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 1,\n" +
//                "        \"nombre\": \"OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Cartografía; casas, Edificios GPON, Edificios VTR exclusivos, Edificios Movistar\",\n" +
//                "      \"puntuacion\": 1,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 3,\n" +
//                "        \"nombre\": \"No Aplica\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Nombres de calles, soleras, en plano CAD\",\n" +
//                "      \"puntuacion\": 1,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 1,\n" +
//                "        \"nombre\": \"OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Tabla con HP objetivos; casas y departamentos\",\n" +
//                "      \"puntuacion\": 2,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 1,\n" +
//                "        \"nombre\": \"OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Cálculo  de atenuación\",\n" +
//                "      \"puntuacion\": 1,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 1,\n" +
//                "        \"nombre\": \"OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Inclusión de notas\",\n" +
//                "      \"puntuacion\": 3,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 2,\n" +
//                "        \"nombre\": \"No OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Vertical de edificio; Shaft, medidas de ductos, cajas, cantidad de departamentos y números\",\n" +
//                "      \"puntuacion\": 1,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 2,\n" +
//                "        \"nombre\": \"No OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Fibras ópticas reservadas\",\n" +
//                "      \"puntuacion\": 2,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 2,\n" +
//                "        \"nombre\": \"No OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Resumen en tabla de OC, CAP, pelos,  pon a ocupar, para CTO rojas y verdes\",\n" +
//                "      \"puntuacion\": 2,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 2,\n" +
//                "        \"nombre\": \"No OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Planes de derivación; número Mufa/dirección/ tipo de Mufa\",\n" +
//                "      \"puntuacion\": 1,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 2,\n" +
//                "        \"nombre\": \"No OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Acometida planeada en plano CAD con criterio 50% CTO y 100% BAF y STB\",\n" +
//                "      \"puntuacion\": 1,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 2,\n" +
//                "        \"nombre\": \"No OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Excel con distribución calles, número, CTO con fusiones, BAF, STB y VTR\",\n" +
//                "      \"puntuacion\": 2,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 1,\n" +
//                "        \"nombre\": \"OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Metrajes de cables nuevos\",\n" +
//                "      \"puntuacion\": 2,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 1,\n" +
//                "        \"nombre\": \"OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Cuentas en cables y CTO por instalar y futuras\",\n" +
//                "      \"puntuacion\": 1,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 1,\n" +
//                "        \"nombre\": \"OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Banderas, cables, Splitter, CAS, CDPE, CAI referidos al CAP\",\n" +
//                "      \"puntuacion\": 1,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 1,\n" +
//                "        \"nombre\": \"OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Etiquetas con nueva norma\",\n" +
//                "      \"puntuacion\": 2,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 1,\n" +
//                "        \"nombre\": \"OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Ingreso a OSP, Cartografía, infraestrucura y red con CAD de diseño\",\n" +
//                "      \"puntuacion\": 1,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 1,\n" +
//                "        \"nombre\": \"OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Conectividad a OSP con avances Asbuilt\",\n" +
//                "      \"puntuacion\": 1,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 1,\n" +
//                "        \"nombre\": \"OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Planos de permisos si requiere; Eléctricos, Serviu\",\n" +
//                "      \"puntuacion\": 2,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 1,\n" +
//                "        \"nombre\": \"OK\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Cubicador en excel\",\n" +
//                "      \"puntuacion\": 3,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 3,\n" +
//                "        \"nombre\": \"No Aplica\"\n" +
//                "      }\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"item\": \"Consitencia de Unilineal \",\n" +
//                "      \"puntuacion\": 2,\n" +
//                "      \"observacion\": \"\",\n" +
//                "      \"opcion\": {\n" +
//                "        \"id\": 3,\n" +
//                "        \"nombre\": \"No Aplica\"\n" +
//                "      }\n" +
//                "    }\n" +
//                "    \n" +
//                "  ]\n" +
//                "}");
//        checkListOt.setComentario("cometario");
//        checkListOt.setEmpresacolaboradora(true);
//        checkListOt.setFechaCreacion(new Date());
//        Ot ot = new Ot();
//        ot.setId(1381);
//        checkListOt.setOt(ot);
//        Usuarios usuario = new Usuarios();
//        usuario.setId(160);
//        checkListOt.setUsuario(usuario);
////        checkListOt.setPuntuacionCheckList(25);
//
//        boolean inserto = false;
//
//        try {
//            inserto = GenericDAO.getINSTANCE().registrarCheckListOt(checkListOt);
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        assertTrue("No se pudo obtener el checkListConfiguracion", inserto);
//    }

//
//    @Test
//    public void deberiaObtenerCheckListOtPorId() {
//
//        int idCheckList = 4;
//
//        CheckListOt checkListOt = new CheckListOt();
//        String mensaje = "";
//        try {
//            checkListOt = GenericDAO.getINSTANCE().obtenerCheckListOtPorId(idCheckList);
//
//            String jsonCheckListOt = checkListOt.getCheckListDetalleOt();
//
//            ObjectMapper mapper = new ObjectMapper();
//            Map mapaParams = mapper.readValue(jsonCheckListOt, new TypeReference<HashMap<String,Object>>() {
//            });
//
//            if(mapaParams.containsKey("checkList")){
//
//                ArrayList<LinkedHashMap> list =  (ArrayList<LinkedHashMap>) mapaParams.get("checkList");
//
//                Iterator iterator = list.iterator();
//
//                int sumaPuntuacionCheckList;
//
//                while(iterator.hasNext()) {
//                    LinkedHashMap item = (LinkedHashMap)iterator.next();
//
//                    String nombreItem = item.get("item").toString();
//                    int puntuacionItem = (int)item.get("puntuacion");
//                    String observacionItem = item.get("observacion").toString();
//                    Map opcionItem = (Map)item.get("opcion");
//                    int idOpcion = (int)opcionItem.get("id");
//                    String nombreOpcion = opcionItem.get("id").toString();
//
//                    // si la puntuacion es 1 significa que esta opcion es obligatoria por lo que la opcion marcada tiene que ser ok
//                    // de lo contrario no cumple con la calidad minima
////                    if(puntuacionItem == 1){
////                        if(idOpcion != 1){
////                            throw new Exception("Item");
////                        }
////                    }
//
//                    CheckListOtDetalle checkListOtDetalle = new CheckListOtDetalle();
//
//                    checkListOtDetalle.setCheckListOt(checkListOt);
//                    checkListOtDetalle.setItem(nombreItem);
//                    checkListOtDetalle.setPuntuacion(puntuacionItem);
//                    checkListOtDetalle.setObservacion(observacionItem);
//                    checkListOtDetalle.setOpcionId(idOpcion);
//                    checkListOtDetalle.setOpcionNombre(nombreOpcion);
//
//                    boolean registro = GenericDAO.getINSTANCE().registrarCheckListOtDetalle(checkListOtDetalle);
//
//                }
//
//
//            }
//
//
//
//        } catch (Exception ex) {
//
//            String mensajeError = ex.getMessage();
//
//            if(ex.getMessage().equalsIgnoreCase("Item")){
//                mensaje = "El item que se desea";
//            }
//            ex.printStackTrace();
//        }
//        assertNotNull("No se pudo obtener el checkListConfiguracion", checkListOt);
//    }
//

//
//    @Test
//    public void obtenerActividadTest() throws Exception{
//        List<Actividades> listaRetorno = CubicadorDAO.getINSTANCE().listarActividades();
//        assertNotNull(listaRetorno + "Error al obtener actividades");
//    }
//
//    @Test
//    public void obtenerEspecialidades() throws Exception{
//        List<Especialidades> listaRetorno = CubicadorDAO.getINSTANCE().listarEspecialidades();
//        assertNotNull(listaRetorno + "Error al obtener especialidades");
//    }
//    @Test
//    public void obtenerServiciosPorEspecialidadTest() throws Exception{
//
//        List<Servicios> servicios = ProveedorServiciosDAO.getInstance().listarServiciosPorEspecialidad(7, 9);
//        assertNotNull(servicios + "Error al obtener especialidades");
//    }
//    @Test
//    public void listarServiciosUnidadPorActividadTest() throws Exception{
//        List serviciosList =  new ArrayList();
//        serviciosList.add(7021);
//        List<HashMap> servicios = ProveedorServiciosDAO.getInstance().listarServiciosUnidadPorActividad(3, 9, serviciosList);
//        assertNotNull(servicios + "Error al obtener especialidades");
//    }
//
//
//    @Test
//    public void deberiaRegistrarCubicadorBucle() throws Exception{
//
////        String codigoManoDeObra = "R103";
////
////        List<Servicios> servicios = ProveedorServiciosDAO.getInstance().obtenerServiciosUnidadPorActividadYManoDeObra(3,codigoManoDeObra);
////        assertNotNull(servicios + "Error al obtener especialidades");
//    }
//
//

//SELECT * from organigrama_eecc WHERE contrato_id = 11 and gestor_id = 92 and proveedor_id = 46;
//        @Test
//    public void listarUsuariosOrganigramaEECC() throws Exception{
//        List<Usuarios> listadoUsuario =  new ArrayList();
//        listadoUsuario = GenericDAO.getINSTANCE().obtenerUsuariosOrganigramaEECC(92, 46, 11);
//
//        assertNotNull(listadoUsuario + "Error al obtener especialidades");
//    }
//
@Test
public void reemplazaTabBd() throws Exception{
    int contratoId = 5;

    boolean respuesta = GenericDAO.getINSTANCE().reemplazaTabsBd(contratoId);
    assertTrue(respuesta);
}


    @Test
    public void visualizarOT() throws Exception{

        int usuarioVisualizador = 90;
        List<Integer> idGestores = new ArrayList<>();
        idGestores.add(93);
        idGestores.add(92);
        idGestores.add(95);
        idGestores.add(87);
        idGestores.add(88);
        idGestores.add(91);
        idGestores.add(1013);
        idGestores.add(96);
        idGestores.add(1014);
        idGestores.add(89);
        idGestores.add(94);

        List<Ot> listadoOt = GenericDAO.getINSTANCE().obtenerOtDeUsuarios(idGestores);


        for (Ot ot: listadoOt){
            System.out.println("INSERT INTO usuarios_ot (usuario_id, rol_id, ot_id, evento_id, contrato_id, gestor_telefonica_id, participacion) VALUES (90,2,"+ot.getId()+",100,1,"+ot.getGestor().getId()+",'Visualizar OT');");

        }

        assertNotNull(listadoOt + "Error al obtener especialidades");
    }






        @Test
    public void regularizarRegistros() throws Exception{

        boolean inserta = false;
        int usuarioId = 158;
//        System.out.println("asda");
        List<WorkflowEventosEjecucion> listadoWEE = GenericDAO.getINSTANCE().obtenerWeePorUsuario(usuarioId);
        List<UsuariosOt> listadousuariosOt = GenericDAO.getINSTANCE().obtenerusuariosOTPorUsuario(usuarioId);

        List<UsuariosOt> listadoResumen = new ArrayList<>();
        System.out.println("----------------------------------------INICIO----------------------------------------");
        int cont = 0;

        for (WorkflowEventosEjecucion wee: listadoWEE){

            int otId =  wee.getOt().getId();

            boolean esta = false;
            for (UsuariosOt usuarioOt: listadousuariosOt){
                int otId2 = usuarioOt.getId().getOtId();

                if(otId2 == otId){
                    esta = true;
                }
            }

            if(!esta){

                Ot ot = OtDAO.getINSTANCE().obtenerDetalleOt(otId);
                cont++;

                System.out.println("INSERT INTO usuarios_ot (usuario_id, rol_id, ot_id, evento_id, contrato_id, gestor_telefonica_id, participacion) VALUES ("+usuarioId+",10,"+otId+",6,"+ot.getContrato().getId()+","+ot.getGestor().getId()+",'Autorizar Pago');");


            }

        }



        System.out.println("----------------------------------------FIN----------------------------------------");
        System.out.println("Cantidad a insertar: " + cont);
        assertNotNull(inserta + "Error al obtener especialidades");
    }



//    @Test
//    public void cargarCatalogoMineria() throws Exception{
//
//
//        String csvFile = "/Users/christian/Desktop/FTP-OTEC/catalogo/ebco-mineria.csv";
//
//        BufferedReader br = null;
//        String line = "";
//        String cvsSplitBy = ",";
//
//        List<ServiciosMineria> listadoServiciosMineria = new ArrayList<>();
//
//        try {
//
//            br = new BufferedReader(new FileReader(csvFile));
//
//            int pos = 0;
//            Contratos mineria = new Contratos();
//            mineria.setId(12);
//
//            Proveedores ebco = new Proveedores();
//            ebco.setId(49);
//
//            TipoMoneda pesos = new TipoMoneda();
//            pesos.setId(2);
//
//            TipoUnidadMedida unidad = new TipoUnidadMedida();
//            unidad.setId(3);
//
//            TipoServicio tipoMineria = new TipoServicio();
//            tipoMineria.setId(72);
//
//            while ((line = br.readLine()) != null) {
//
////                String[] archivo = line.split(cvsSplitBy);
//                String archivo = line;
//                ServiciosMineria servicioMineria = new ServiciosMineria();
//
//                try {
//                    String[] data = line.split(";");
//
//                    String nombreEmpresa = data[6];
//                    Servicios servicio = new Servicios();
//
//                    String nombre = obtenerNombreServicio(nombreEmpresa);
//                    List<Regiones> regiones = obtenerRegionesServicios(nombreEmpresa);
//
//                    servicio.setTipoServicio(tipoMineria);
//                    servicio.setTipoUnidadMedida(unidad);
//                    servicio.setNombre(nombre);
//                    servicio.setDescripcion(nombre);
//                    servicio.setCodigo(null);
//                    servicio.setEstado('A');
//                    servicio.setIsPackBasico(false);
//                    servicio.setCantidadDefault(1);
//                    servicio.setCodAlcance("SIN-ALCANCE");
//                    servicio.setPuntosBaremos(null);
//                    servicio.setEspecialidadId(null);
//                    servicio.setContratoId(null);
//
//                    servicioMineria.setServicios(servicio);
//
//
//                    List<ProveedoresServicios> listadoProveedoresServicio = new ArrayList<>();
//
//                    for(Regiones region: regiones){
//                        ProveedoresServicios provedorServicio = new ProveedoresServicios();
//                        provedorServicio.setContratos(mineria);
//                        provedorServicio.setProveedores(ebco);
//                        provedorServicio.setRegiones(region);
//                        provedorServicio.setServicios(servicio);
////                        provedorServicio.setPrecio(precioServicio);
//                        provedorServicio.setTipoMoneda(pesos);
//                        listadoProveedoresServicio.add(provedorServicio);
//                    }
//
//                    servicioMineria.setProveedoresServicios(listadoProveedoresServicio);
//
//                    listadoServiciosMineria.add(servicioMineria);
//
//                }catch (Exception e){
//                    System.out.println("Error al leer un registro");
//                }
//            }
//
//
//// TRAS E INST DE BANCO DE BATERIAS  PARA SER IMPLEMENTADOS EN LA FUENTE DE PODER XXI XV
//
////            System.out.println("Listado empresas" + listadoEmpresas.size());
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (br != null) {
//                try {
//                    br.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        assertNotNull(listadoServiciosMineria + "Error al obtener especialidades");
//    }

    private String obtenerNombreServicio(String nombreServicioCompleto) {

        String nombre[] = nombreServicioCompleto.split("\\.");

        String nombreServicioConRegion = nombre[1];
        nombreServicioConRegion = nombreServicioConRegion.replace("METROPOLITANA", "");
        nombreServicioConRegion = nombreServicioConRegion.replace("I,", "");
        nombreServicioConRegion =  nombreServicioConRegion.replace("II,", "");
        nombreServicioConRegion =  nombreServicioConRegion.replace("III,", "");
        nombreServicioConRegion =  nombreServicioConRegion.replace("IV,", "");
        nombreServicioConRegion =  nombreServicioConRegion.replace("V,", "");
        nombreServicioConRegion = nombreServicioConRegion.replace("VI,", "");
        nombreServicioConRegion = nombreServicioConRegion.replace("VII,", "");
        nombreServicioConRegion = nombreServicioConRegion.replace("VIII,", "");
        nombreServicioConRegion = nombreServicioConRegion.replace("IX,", "");
        nombreServicioConRegion = nombreServicioConRegion.replace("X,", "");
        nombreServicioConRegion = nombreServicioConRegion.replace("XI,", "");
        nombreServicioConRegion = nombreServicioConRegion.replace("XII,", "");
        nombreServicioConRegion = nombreServicioConRegion.replace("XIII,", "");
        nombreServicioConRegion = nombreServicioConRegion.replace("XIV,", "");
        nombreServicioConRegion = nombreServicioConRegion.replace("XV,", "");

        return nombreServicioConRegion;
    }

    private List<Regiones> obtenerRegionesServicios(String nombreServicioCompleto) {

        List<Regiones> listadoRegiones = new ArrayList<>();

        if(nombreServicioCompleto.contains("METROPOLITANA")){
            Regiones region = new Regiones();
            region.setId(13);
            listadoRegiones.add(region);
        }
        if(nombreServicioCompleto.contains("I,")){
            Regiones region = new Regiones();
            region.setId(1);
            listadoRegiones.add(region);
        }
        if(nombreServicioCompleto.contains("II")){
            Regiones region = new Regiones();
            region.setId(2);
            listadoRegiones.add(region);
        }
        if(nombreServicioCompleto.contains("III")){
            Regiones region = new Regiones();
            region.setId(3);
            listadoRegiones.add(region);
        }
        if(nombreServicioCompleto.contains("IV,")){
            Regiones region = new Regiones();
            region.setId(4);
            listadoRegiones.add(region);
        }
        if(nombreServicioCompleto.contains("V,")){
            Regiones region = new Regiones();
            region.setId(5);
            listadoRegiones.add(region);
        }
        if(nombreServicioCompleto.contains("VI,")){
            Regiones region = new Regiones();
            region.setId(6);
            listadoRegiones.add(region);
        }
        if(nombreServicioCompleto.contains("VII")){
            Regiones region = new Regiones();
            region.setId(7);
            listadoRegiones.add(region);
        }
        if(nombreServicioCompleto.contains("VIII")){
            Regiones region = new Regiones();
            region.setId(8);
            listadoRegiones.add(region);
        }
        if(nombreServicioCompleto.contains("IX")){
            Regiones region = new Regiones();
            region.setId(9);
            listadoRegiones.add(region);
        }
        if(nombreServicioCompleto.contains("X,")){
            Regiones region = new Regiones();
            region.setId(10);
            listadoRegiones.add(region);
        }
        if(nombreServicioCompleto.contains("XI")){
            Regiones region = new Regiones();
            region.setId(11);
            listadoRegiones.add(region);
        }
        if(nombreServicioCompleto.contains("XII")){
            Regiones region = new Regiones();
            region.setId(12);
            listadoRegiones.add(region);
        }
        if(nombreServicioCompleto.contains("XIV")){
            Regiones region = new Regiones();
            region.setId(14);
            listadoRegiones.add(region);
        }
        if(nombreServicioCompleto.contains("XV")){
            Regiones region = new Regiones();
            region.setId(15);
            listadoRegiones.add(region);
        }

        return listadoRegiones;
    }

//    @Test
//    public void obtieneFormulariosCalidad() throws Exception{
//        List formularioCalidad = new ArrayList();
//        int usuarioId = 1;
//        int otId = 6309;
//        String tipoFormulario = "OOEE";
//
//        formularioCalidad = OtDAO.getINSTANCE().obtieneFormularioCalidad(otId, usuarioId, tipoFormulario);
//        assertNotNull(formularioCalidad);
//    }


    @Test
    public void registraFormularioCalidad() throws Exception{
        boolean formularioCalidad = false;

        int usuarioId = 1;
        int otId = 6315;
        String tipoFormulario = "OOEE";

        String formulario = "{\"retorno\":[{\"nombre\":\"MALLA A TIERRA\",\"itemsSeguimiento\":[{\"item\":\"ATERRAMIENTOS EN LAS 4 CARAS DE CERCO PERIMETRAL\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"ATERRAMIENTO PARARRAYO\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"ATERRAMIENTO PARA TG\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"ATERRAMIENTO TDA Y F\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"ATERRAMIENTOS TORRE\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"ATERRAMIENTO PUERTAS DE ACCESO\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"ATERRAMIENTO GABINETE DE EQUIPO\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"2 BDT CON AISLADORES RESPECTIVOS\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"ATERRAMIENTO VIENTOS CON BARRA COPPERWELD\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"}]},{\"nombre\":\"CANALIZACIONES\",\"itemsSeguimiento\":[{\"item\":\"CANALIZACIÓN SUBTERRANEA DEBE SER TIPO PVC\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"CANALIZACIÓN (c.a.g.) A LA VISTA EN EL EXTERIOR\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"CANALIZACIÓN BAJADA (c.a.g 1 1/4'') DE POSTE TG Y TDA Y F\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"EN CASO DE LÍNEA CONSIDERAR 3 DUCTOS DE TDA Y F A EQUIPO\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"EN CASO DE ENERGÍA ALTERNATIVA 2 DUCTOS DE TDA Y F A EQUIPO\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"CANALIZACIONES DISPONIBLES\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"}]},{\"nombre\":\"ELEMENTOS ELÉCTRICOS EN SITIO\",\"itemsSeguimiento\":[{\"item\":\"POSTES METALICO 6m LIBRES, DE TG Y TDA Y F\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"BALIZA\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"HALOGENO\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"TDA Y F (QUE CONTENGA TODOS LOS ELEMENTOS)\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"TG\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"GABINETE EQUIPO\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"RADIER EQUIPO\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"EPC\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"PARARRAYO\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"MALLA A TIERRA\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"PLANTA DE ENERGÍA\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"BANCO DE BATERIA\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"ALIMENTADORES CON INSTALACION CORRESPONDIENTE\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"}]},{\"nombre\":\"LÍNEA ELÉCTRICA EN MT\",\"itemsSeguimiento\":[{\"item\":\"POSTACIÓN\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"S/E\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"CONDUCTORES\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"E.C.M.\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"CRUCETAS\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"ACOMETIDA\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"FERRETERIA EN GENERAL\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"LÍNEAS RURALES DE CATEGORÍA B, DISTANCIA ENTRE CONDUCTORES Y ARBOLES DEBE SER POR LO MENOS 5m\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"}]},{\"nombre\":\"LÍNEA ELÉCTRICA EN BT\",\"itemsSeguimiento\":[{\"item\":\"POSTACIÓN\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"CONDUCTORES\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"ACOMETIDA\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"},{\"item\":\"FERRETERIA EN GENERAL\",\"ok\":null,\"nok\":null,\"n/a\":null,\"porcentage\":0,\"observaciones\":\"\"}]}]}";
        HashMap<String,Object> mapaParametros = new ObjectMapper().readValue(formulario, HashMap.class);

//        formularioCalidad = OtDAO.getINSTANCE().registraFormularioSeguimientoCalidad(otId, usuarioId, mapaParametros);
//        assertTrue(formularioCalidad);
    }




}


